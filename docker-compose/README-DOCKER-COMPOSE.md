# JHipster generated Docker-Compose configuration

## Usage

Launch all your infrastructure by running: `docker-compose up -d`.

## Configured Docker services

### Service registry and configuration server:

- [JHipster Registry](http://localhost:8761)

### Applications and dependencies:

- gateway (gateway application)
- gateway's postgresql database
- manilla (microservice application)
- manilla's postgresql database
- pacientes (microservice application)
- pacientes's postgresql database
- biologico (microservice application)
- biologico's postgresql database
- diadema (microservice application)
- diadema's postgresql database
- expedientemanual (microservice application)
- expedientemanual's postgresql database

### Additional Services:

- [Keycloak server](http://localhost:9080)
