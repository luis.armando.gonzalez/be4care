package com.be4tech.be4care.pacientes.repository;

import com.be4tech.be4care.pacientes.domain.CuestionarioEstado;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the CuestionarioEstado entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CuestionarioEstadoRepository extends R2dbcRepository<CuestionarioEstado, Long>, CuestionarioEstadoRepositoryInternal {
    Flux<CuestionarioEstado> findAllBy(Pageable pageable);

    @Query("SELECT * FROM cuestionario_estado entity WHERE entity.pregunta_id = :id")
    Flux<CuestionarioEstado> findByPregunta(Long id);

    @Query("SELECT * FROM cuestionario_estado entity WHERE entity.pregunta_id IS NULL")
    Flux<CuestionarioEstado> findAllWherePreguntaIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<CuestionarioEstado> findAll();

    @Override
    Mono<CuestionarioEstado> findById(Long id);

    @Override
    <S extends CuestionarioEstado> Mono<S> save(S entity);
}

interface CuestionarioEstadoRepositoryInternal {
    <S extends CuestionarioEstado> Mono<S> insert(S entity);
    <S extends CuestionarioEstado> Mono<S> save(S entity);
    Mono<Integer> update(CuestionarioEstado entity);

    Flux<CuestionarioEstado> findAll();
    Mono<CuestionarioEstado> findById(Long id);
    Flux<CuestionarioEstado> findAllBy(Pageable pageable);
    Flux<CuestionarioEstado> findAllBy(Pageable pageable, Criteria criteria);
}
