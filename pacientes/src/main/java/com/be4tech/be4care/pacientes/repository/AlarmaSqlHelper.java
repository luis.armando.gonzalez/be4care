package com.be4tech.be4care.pacientes.repository;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Table;

public class AlarmaSqlHelper {

    public static List<Expression> getColumns(Table table, String columnPrefix) {
        List<Expression> columns = new ArrayList<>();
        columns.add(Column.aliased("id", table, columnPrefix + "_id"));
        columns.add(Column.aliased("time_instant", table, columnPrefix + "_time_instant"));
        columns.add(Column.aliased("descripcion", table, columnPrefix + "_descripcion"));
        columns.add(Column.aliased("procedimiento", table, columnPrefix + "_procedimiento"));
        columns.add(Column.aliased("titulo", table, columnPrefix + "_titulo"));
        columns.add(Column.aliased("verificar", table, columnPrefix + "_verificar"));
        columns.add(Column.aliased("observaciones", table, columnPrefix + "_observaciones"));
        columns.add(Column.aliased("prioridad", table, columnPrefix + "_prioridad"));

        columns.add(Column.aliased("user_id", table, columnPrefix + "_user_id"));
        return columns;
    }
}
