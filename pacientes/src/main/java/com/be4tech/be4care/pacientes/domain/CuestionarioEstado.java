package com.be4tech.be4care.pacientes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A CuestionarioEstado.
 */
@Table("cuestionario_estado")
public class CuestionarioEstado implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("valor")
    private Integer valor;

    @Column("valoracion")
    private String valoracion;

    @Transient
    @JsonIgnoreProperties(value = { "condicion" }, allowSetters = true)
    private Pregunta pregunta;

    @Column("pregunta_id")
    private Long preguntaId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public CuestionarioEstado id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getValor() {
        return this.valor;
    }

    public CuestionarioEstado valor(Integer valor) {
        this.setValor(valor);
        return this;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public String getValoracion() {
        return this.valoracion;
    }

    public CuestionarioEstado valoracion(String valoracion) {
        this.setValoracion(valoracion);
        return this;
    }

    public void setValoracion(String valoracion) {
        this.valoracion = valoracion;
    }

    public Pregunta getPregunta() {
        return this.pregunta;
    }

    public void setPregunta(Pregunta pregunta) {
        this.pregunta = pregunta;
        this.preguntaId = pregunta != null ? pregunta.getId() : null;
    }

    public CuestionarioEstado pregunta(Pregunta pregunta) {
        this.setPregunta(pregunta);
        return this;
    }

    public Long getPreguntaId() {
        return this.preguntaId;
    }

    public void setPreguntaId(Long pregunta) {
        this.preguntaId = pregunta;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CuestionarioEstado)) {
            return false;
        }
        return id != null && id.equals(((CuestionarioEstado) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CuestionarioEstado{" +
            "id=" + getId() +
            ", valor=" + getValor() +
            ", valoracion='" + getValoracion() + "'" +
            "}";
    }
}
