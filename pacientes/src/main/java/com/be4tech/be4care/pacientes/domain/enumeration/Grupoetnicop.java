package com.be4tech.be4care.pacientes.domain.enumeration;

/**
 * The Grupoetnicop enumeration.
 */
public enum Grupoetnicop {
    Afrocolombiano,
    Palenquero,
    Raizal,
    Indigena,
    RomGitano,
    Mestizo,
    Ninguno,
    Otro,
}
