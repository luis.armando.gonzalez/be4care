package com.be4tech.be4care.pacientes.repository;

import com.be4tech.be4care.pacientes.domain.TratamientoMedicamento;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the TratamientoMedicamento entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TratamientoMedicamentoRepository
    extends R2dbcRepository<TratamientoMedicamento, Long>, TratamientoMedicamentoRepositoryInternal {
    Flux<TratamientoMedicamento> findAllBy(Pageable pageable);

    @Query("SELECT * FROM tratamiento_medicamento entity WHERE entity.tratamiento_id = :id")
    Flux<TratamientoMedicamento> findByTratamiento(Long id);

    @Query("SELECT * FROM tratamiento_medicamento entity WHERE entity.tratamiento_id IS NULL")
    Flux<TratamientoMedicamento> findAllWhereTratamientoIsNull();

    @Query("SELECT * FROM tratamiento_medicamento entity WHERE entity.medicamento_id = :id")
    Flux<TratamientoMedicamento> findByMedicamento(Long id);

    @Query("SELECT * FROM tratamiento_medicamento entity WHERE entity.medicamento_id IS NULL")
    Flux<TratamientoMedicamento> findAllWhereMedicamentoIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<TratamientoMedicamento> findAll();

    @Override
    Mono<TratamientoMedicamento> findById(Long id);

    @Override
    <S extends TratamientoMedicamento> Mono<S> save(S entity);
}

interface TratamientoMedicamentoRepositoryInternal {
    <S extends TratamientoMedicamento> Mono<S> insert(S entity);
    <S extends TratamientoMedicamento> Mono<S> save(S entity);
    Mono<Integer> update(TratamientoMedicamento entity);

    Flux<TratamientoMedicamento> findAll();
    Mono<TratamientoMedicamento> findById(Long id);
    Flux<TratamientoMedicamento> findAllBy(Pageable pageable);
    Flux<TratamientoMedicamento> findAllBy(Pageable pageable, Criteria criteria);
}
