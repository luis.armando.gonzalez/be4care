package com.be4tech.be4care.pacientes.domain.enumeration;

/**
 * The Estratop enumeration.
 */
public enum Estratop {
    Estrato_1,
    Estrato_2,
    Estrato_3,
    Estrato_4,
    Estrato_5,
    Estrato_especial,
}
