package com.be4tech.be4care.pacientes.web.rest;

import com.be4tech.be4care.pacientes.domain.IPS;
import com.be4tech.be4care.pacientes.repository.IPSRepository;
import com.be4tech.be4care.pacientes.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.pacientes.domain.IPS}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class IPSResource {

    private final Logger log = LoggerFactory.getLogger(IPSResource.class);

    private static final String ENTITY_NAME = "pacientesIps";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IPSRepository iPSRepository;

    public IPSResource(IPSRepository iPSRepository) {
        this.iPSRepository = iPSRepository;
    }

    /**
     * {@code POST  /ips} : Create a new iPS.
     *
     * @param iPS the iPS to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new iPS, or with status {@code 400 (Bad Request)} if the iPS has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ips")
    public Mono<ResponseEntity<IPS>> createIPS(@RequestBody IPS iPS) throws URISyntaxException {
        log.debug("REST request to save IPS : {}", iPS);
        if (iPS.getId() != null) {
            throw new BadRequestAlertException("A new iPS cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return iPSRepository
            .save(iPS)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/ips/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /ips/:id} : Updates an existing iPS.
     *
     * @param id the id of the iPS to save.
     * @param iPS the iPS to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated iPS,
     * or with status {@code 400 (Bad Request)} if the iPS is not valid,
     * or with status {@code 500 (Internal Server Error)} if the iPS couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ips/{id}")
    public Mono<ResponseEntity<IPS>> updateIPS(@PathVariable(value = "id", required = false) final Long id, @RequestBody IPS iPS)
        throws URISyntaxException {
        log.debug("REST request to update IPS : {}, {}", id, iPS);
        if (iPS.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, iPS.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return iPSRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                return iPSRepository
                    .save(iPS)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /ips/:id} : Partial updates given fields of an existing iPS, field will ignore if it is null
     *
     * @param id the id of the iPS to save.
     * @param iPS the iPS to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated iPS,
     * or with status {@code 400 (Bad Request)} if the iPS is not valid,
     * or with status {@code 404 (Not Found)} if the iPS is not found,
     * or with status {@code 500 (Internal Server Error)} if the iPS couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/ips/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<IPS>> partialUpdateIPS(@PathVariable(value = "id", required = false) final Long id, @RequestBody IPS iPS)
        throws URISyntaxException {
        log.debug("REST request to partial update IPS partially : {}, {}", id, iPS);
        if (iPS.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, iPS.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return iPSRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                Mono<IPS> result = iPSRepository
                    .findById(iPS.getId())
                    .map(existingIPS -> {
                        if (iPS.getNombre() != null) {
                            existingIPS.setNombre(iPS.getNombre());
                        }
                        if (iPS.getNit() != null) {
                            existingIPS.setNit(iPS.getNit());
                        }
                        if (iPS.getDireccion() != null) {
                            existingIPS.setDireccion(iPS.getDireccion());
                        }
                        if (iPS.getTelefono() != null) {
                            existingIPS.setTelefono(iPS.getTelefono());
                        }
                        if (iPS.getCorreoElectronico() != null) {
                            existingIPS.setCorreoElectronico(iPS.getCorreoElectronico());
                        }

                        return existingIPS;
                    })
                    .flatMap(iPSRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /ips} : get all the iPS.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of iPS in body.
     */
    @GetMapping("/ips")
    public Mono<ResponseEntity<List<IPS>>> getAllIPS(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of IPS");
        return iPSRepository
            .count()
            .zipWith(iPSRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /ips/:id} : get the "id" iPS.
     *
     * @param id the id of the iPS to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the iPS, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ips/{id}")
    public Mono<ResponseEntity<IPS>> getIPS(@PathVariable Long id) {
        log.debug("REST request to get IPS : {}", id);
        Mono<IPS> iPS = iPSRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(iPS);
    }

    /**
     * {@code DELETE  /ips/:id} : delete the "id" iPS.
     *
     * @param id the id of the iPS to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ips/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteIPS(@PathVariable Long id) {
        log.debug("REST request to delete IPS : {}", id);
        return iPSRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
