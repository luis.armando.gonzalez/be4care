package com.be4tech.be4care.pacientes.web.rest;

import com.be4tech.be4care.pacientes.domain.Pregunta;
import com.be4tech.be4care.pacientes.repository.PreguntaRepository;
import com.be4tech.be4care.pacientes.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.pacientes.domain.Pregunta}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class PreguntaResource {

    private final Logger log = LoggerFactory.getLogger(PreguntaResource.class);

    private static final String ENTITY_NAME = "pacientesPregunta";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PreguntaRepository preguntaRepository;

    public PreguntaResource(PreguntaRepository preguntaRepository) {
        this.preguntaRepository = preguntaRepository;
    }

    /**
     * {@code POST  /preguntas} : Create a new pregunta.
     *
     * @param pregunta the pregunta to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new pregunta, or with status {@code 400 (Bad Request)} if the pregunta has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/preguntas")
    public Mono<ResponseEntity<Pregunta>> createPregunta(@RequestBody Pregunta pregunta) throws URISyntaxException {
        log.debug("REST request to save Pregunta : {}", pregunta);
        if (pregunta.getId() != null) {
            throw new BadRequestAlertException("A new pregunta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return preguntaRepository
            .save(pregunta)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/preguntas/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /preguntas/:id} : Updates an existing pregunta.
     *
     * @param id the id of the pregunta to save.
     * @param pregunta the pregunta to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pregunta,
     * or with status {@code 400 (Bad Request)} if the pregunta is not valid,
     * or with status {@code 500 (Internal Server Error)} if the pregunta couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/preguntas/{id}")
    public Mono<ResponseEntity<Pregunta>> updatePregunta(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Pregunta pregunta
    ) throws URISyntaxException {
        log.debug("REST request to update Pregunta : {}, {}", id, pregunta);
        if (pregunta.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, pregunta.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return preguntaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                return preguntaRepository
                    .save(pregunta)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /preguntas/:id} : Partial updates given fields of an existing pregunta, field will ignore if it is null
     *
     * @param id the id of the pregunta to save.
     * @param pregunta the pregunta to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pregunta,
     * or with status {@code 400 (Bad Request)} if the pregunta is not valid,
     * or with status {@code 404 (Not Found)} if the pregunta is not found,
     * or with status {@code 500 (Internal Server Error)} if the pregunta couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/preguntas/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Pregunta>> partialUpdatePregunta(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Pregunta pregunta
    ) throws URISyntaxException {
        log.debug("REST request to partial update Pregunta partially : {}, {}", id, pregunta);
        if (pregunta.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, pregunta.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return preguntaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                Mono<Pregunta> result = preguntaRepository
                    .findById(pregunta.getId())
                    .map(existingPregunta -> {
                        if (pregunta.getPregunta() != null) {
                            existingPregunta.setPregunta(pregunta.getPregunta());
                        }

                        return existingPregunta;
                    })
                    .flatMap(preguntaRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /preguntas} : get all the preguntas.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of preguntas in body.
     */
    @GetMapping("/preguntas")
    public Mono<ResponseEntity<List<Pregunta>>> getAllPreguntas(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Preguntas");
        return preguntaRepository
            .count()
            .zipWith(preguntaRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /preguntas/:id} : get the "id" pregunta.
     *
     * @param id the id of the pregunta to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the pregunta, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/preguntas/{id}")
    public Mono<ResponseEntity<Pregunta>> getPregunta(@PathVariable Long id) {
        log.debug("REST request to get Pregunta : {}", id);
        Mono<Pregunta> pregunta = preguntaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(pregunta);
    }

    /**
     * {@code DELETE  /preguntas/:id} : delete the "id" pregunta.
     *
     * @param id the id of the pregunta to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/preguntas/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deletePregunta(@PathVariable Long id) {
        log.debug("REST request to delete Pregunta : {}", id);
        return preguntaRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
