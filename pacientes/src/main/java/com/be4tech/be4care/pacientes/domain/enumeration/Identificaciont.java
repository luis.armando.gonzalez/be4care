package com.be4tech.be4care.pacientes.domain.enumeration;

/**
 * The Identificaciont enumeration.
 */
public enum Identificaciont {
    CEDULA,
    TARJETA,
    IDENTIDAD,
    CEDULA_EXTRANJERIA,
    PASAPORTE,
    OTRO,
}
