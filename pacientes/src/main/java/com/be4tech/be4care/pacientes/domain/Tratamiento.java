package com.be4tech.be4care.pacientes.domain;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZonedDateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Tratamiento.
 */
@Table("tratamiento")
public class Tratamiento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("descripcion_tratamiento")
    private String descripcionTratamiento;

    @Column("fecha_inicio")
    private Instant fechaInicio;

    @Column("fecha_fin")
    private ZonedDateTime fechaFin;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Tratamiento id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcionTratamiento() {
        return this.descripcionTratamiento;
    }

    public Tratamiento descripcionTratamiento(String descripcionTratamiento) {
        this.setDescripcionTratamiento(descripcionTratamiento);
        return this;
    }

    public void setDescripcionTratamiento(String descripcionTratamiento) {
        this.descripcionTratamiento = descripcionTratamiento;
    }

    public Instant getFechaInicio() {
        return this.fechaInicio;
    }

    public Tratamiento fechaInicio(Instant fechaInicio) {
        this.setFechaInicio(fechaInicio);
        return this;
    }

    public void setFechaInicio(Instant fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public ZonedDateTime getFechaFin() {
        return this.fechaFin;
    }

    public Tratamiento fechaFin(ZonedDateTime fechaFin) {
        this.setFechaFin(fechaFin);
        return this;
    }

    public void setFechaFin(ZonedDateTime fechaFin) {
        this.fechaFin = fechaFin;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tratamiento)) {
            return false;
        }
        return id != null && id.equals(((Tratamiento) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Tratamiento{" +
            "id=" + getId() +
            ", descripcionTratamiento='" + getDescripcionTratamiento() + "'" +
            ", fechaInicio='" + getFechaInicio() + "'" +
            ", fechaFin='" + getFechaFin() + "'" +
            "}";
    }
}
