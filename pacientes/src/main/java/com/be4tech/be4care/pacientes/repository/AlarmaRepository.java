package com.be4tech.be4care.pacientes.repository;

import com.be4tech.be4care.pacientes.domain.Alarma;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Alarma entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AlarmaRepository extends R2dbcRepository<Alarma, Long>, AlarmaRepositoryInternal {
    Flux<Alarma> findAllBy(Pageable pageable);

    @Query("SELECT * FROM alarma entity WHERE entity.user_id = :id")
    Flux<Alarma> findByUser(Long id);

    @Query("SELECT * FROM alarma entity WHERE entity.user_id IS NULL")
    Flux<Alarma> findAllWhereUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Alarma> findAll();

    @Override
    Mono<Alarma> findById(Long id);

    @Override
    <S extends Alarma> Mono<S> save(S entity);
}

interface AlarmaRepositoryInternal {
    <S extends Alarma> Mono<S> insert(S entity);
    <S extends Alarma> Mono<S> save(S entity);
    Mono<Integer> update(Alarma entity);

    Flux<Alarma> findAll();
    Mono<Alarma> findById(Long id);
    Flux<Alarma> findAllBy(Pageable pageable);
    Flux<Alarma> findAllBy(Pageable pageable, Criteria criteria);
}
