package com.be4tech.be4care.pacientes.repository.rowmapper;

import com.be4tech.be4care.pacientes.domain.Farmaceutica;
import com.be4tech.be4care.pacientes.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Farmaceutica}, with proper type conversions.
 */
@Service
public class FarmaceuticaRowMapper implements BiFunction<Row, String, Farmaceutica> {

    private final ColumnConverter converter;

    public FarmaceuticaRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Farmaceutica} stored in the database.
     */
    @Override
    public Farmaceutica apply(Row row, String prefix) {
        Farmaceutica entity = new Farmaceutica();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setNombre(converter.fromRow(row, prefix + "_nombre", String.class));
        entity.setDireccion(converter.fromRow(row, prefix + "_direccion", String.class));
        entity.setPropietario(converter.fromRow(row, prefix + "_propietario", String.class));
        return entity;
    }
}
