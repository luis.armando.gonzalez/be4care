package com.be4tech.be4care.pacientes.repository;

import com.be4tech.be4care.pacientes.domain.Ciudad;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Ciudad entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CiudadRepository extends R2dbcRepository<Ciudad, Long>, CiudadRepositoryInternal {
    Flux<Ciudad> findAllBy(Pageable pageable);

    @Query("SELECT * FROM ciudad entity WHERE entity.pais_id = :id")
    Flux<Ciudad> findByPais(Long id);

    @Query("SELECT * FROM ciudad entity WHERE entity.pais_id IS NULL")
    Flux<Ciudad> findAllWherePaisIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Ciudad> findAll();

    @Override
    Mono<Ciudad> findById(Long id);

    @Override
    <S extends Ciudad> Mono<S> save(S entity);
}

interface CiudadRepositoryInternal {
    <S extends Ciudad> Mono<S> insert(S entity);
    <S extends Ciudad> Mono<S> save(S entity);
    Mono<Integer> update(Ciudad entity);

    Flux<Ciudad> findAll();
    Mono<Ciudad> findById(Long id);
    Flux<Ciudad> findAllBy(Pageable pageable);
    Flux<Ciudad> findAllBy(Pageable pageable, Criteria criteria);
}
