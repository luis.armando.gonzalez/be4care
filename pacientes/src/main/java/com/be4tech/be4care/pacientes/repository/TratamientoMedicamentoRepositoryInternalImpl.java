package com.be4tech.be4care.pacientes.repository;

import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

import com.be4tech.be4care.pacientes.domain.TratamientoMedicamento;
import com.be4tech.be4care.pacientes.repository.rowmapper.MedicamentoRowMapper;
import com.be4tech.be4care.pacientes.repository.rowmapper.TratamientoMedicamentoRowMapper;
import com.be4tech.be4care.pacientes.repository.rowmapper.TratamientoRowMapper;
import com.be4tech.be4care.pacientes.service.EntityManager;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoinCondition;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive custom repository implementation for the TratamientoMedicamento entity.
 */
@SuppressWarnings("unused")
class TratamientoMedicamentoRepositoryInternalImpl implements TratamientoMedicamentoRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final TratamientoRowMapper tratamientoMapper;
    private final MedicamentoRowMapper medicamentoMapper;
    private final TratamientoMedicamentoRowMapper tratamientomedicamentoMapper;

    private static final Table entityTable = Table.aliased("tratamiento_medicamento", EntityManager.ENTITY_ALIAS);
    private static final Table tratamientoTable = Table.aliased("tratamiento", "tratamiento");
    private static final Table medicamentoTable = Table.aliased("medicamento", "medicamento");

    public TratamientoMedicamentoRepositoryInternalImpl(
        R2dbcEntityTemplate template,
        EntityManager entityManager,
        TratamientoRowMapper tratamientoMapper,
        MedicamentoRowMapper medicamentoMapper,
        TratamientoMedicamentoRowMapper tratamientomedicamentoMapper
    ) {
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.tratamientoMapper = tratamientoMapper;
        this.medicamentoMapper = medicamentoMapper;
        this.tratamientomedicamentoMapper = tratamientomedicamentoMapper;
    }

    @Override
    public Flux<TratamientoMedicamento> findAllBy(Pageable pageable) {
        return findAllBy(pageable, null);
    }

    @Override
    public Flux<TratamientoMedicamento> findAllBy(Pageable pageable, Criteria criteria) {
        return createQuery(pageable, criteria).all();
    }

    RowsFetchSpec<TratamientoMedicamento> createQuery(Pageable pageable, Criteria criteria) {
        List<Expression> columns = TratamientoMedicamentoSqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        columns.addAll(TratamientoSqlHelper.getColumns(tratamientoTable, "tratamiento"));
        columns.addAll(MedicamentoSqlHelper.getColumns(medicamentoTable, "medicamento"));
        SelectFromAndJoinCondition selectFrom = Select
            .builder()
            .select(columns)
            .from(entityTable)
            .leftOuterJoin(tratamientoTable)
            .on(Column.create("tratamiento_id", entityTable))
            .equals(Column.create("id", tratamientoTable))
            .leftOuterJoin(medicamentoTable)
            .on(Column.create("medicamento_id", entityTable))
            .equals(Column.create("id", medicamentoTable));

        String select = entityManager.createSelect(selectFrom, TratamientoMedicamento.class, pageable, criteria);
        String alias = entityTable.getReferenceName().getReference();
        String selectWhere = Optional
            .ofNullable(criteria)
            .map(crit ->
                new StringBuilder(select)
                    .append(" ")
                    .append("WHERE")
                    .append(" ")
                    .append(alias)
                    .append(".")
                    .append(crit.toString())
                    .toString()
            )
            .orElse(select); // TODO remove once https://github.com/spring-projects/spring-data-jdbc/issues/907 will be fixed
        return db.sql(selectWhere).map(this::process);
    }

    @Override
    public Flux<TratamientoMedicamento> findAll() {
        return findAllBy(null, null);
    }

    @Override
    public Mono<TratamientoMedicamento> findById(Long id) {
        return createQuery(null, where("id").is(id)).one();
    }

    private TratamientoMedicamento process(Row row, RowMetadata metadata) {
        TratamientoMedicamento entity = tratamientomedicamentoMapper.apply(row, "e");
        entity.setTratamiento(tratamientoMapper.apply(row, "tratamiento"));
        entity.setMedicamento(medicamentoMapper.apply(row, "medicamento"));
        return entity;
    }

    @Override
    public <S extends TratamientoMedicamento> Mono<S> insert(S entity) {
        return entityManager.insert(entity);
    }

    @Override
    public <S extends TratamientoMedicamento> Mono<S> save(S entity) {
        if (entity.getId() == null) {
            return insert(entity);
        } else {
            return update(entity)
                .map(numberOfUpdates -> {
                    if (numberOfUpdates.intValue() <= 0) {
                        throw new IllegalStateException("Unable to update TratamientoMedicamento with id = " + entity.getId());
                    }
                    return entity;
                });
        }
    }

    @Override
    public Mono<Integer> update(TratamientoMedicamento entity) {
        //fixme is this the proper way?
        return r2dbcEntityTemplate.update(entity).thenReturn(1);
    }
}
