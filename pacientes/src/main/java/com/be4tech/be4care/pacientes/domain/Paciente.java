package com.be4tech.be4care.pacientes.domain;

import com.be4tech.be4care.pacientes.domain.enumeration.Estratop;
import com.be4tech.be4care.pacientes.domain.enumeration.Grupoetnicop;
import com.be4tech.be4care.pacientes.domain.enumeration.Identificaciont;
import com.be4tech.be4care.pacientes.domain.enumeration.Sexop;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Paciente.
 */
@Table("paciente")
public class Paciente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("nombre")
    private String nombre;

    @Column("tipo_identificacion")
    private Identificaciont tipoIdentificacion;

    @Column("identificacion")
    private Integer identificacion;

    @Column("edad")
    private Integer edad;

    @Column("sexo")
    private Sexop sexo;

    @Column("telefono")
    private Integer telefono;

    @Column("direccion")
    private String direccion;

    @Column("peso_kg")
    private Float pesoKG;

    @Column("estatura_cm")
    private Integer estaturaCM;

    @Column("grupo_etnico")
    private Grupoetnicop grupoEtnico;

    @Column("estrato_socioeconomico")
    private Estratop estratoSocioeconomico;

    @Column("oximetria_referencia")
    private Integer oximetriaReferencia;

    @Column("ritmo_cardiaco_referencia")
    private Integer ritmoCardiacoReferencia;

    @Column("presion_sistolica_referencia")
    private Integer presionSistolicaReferencia;

    @Column("presion_distolica_referencia")
    private Integer presionDistolicaReferencia;

    @Column("comentarios")
    private String comentarios;

    @Column("pasos_referencia")
    private Integer pasosReferencia;

    @Column("calorias_referencia")
    private Integer caloriasReferencia;

    @Column("meta_referencia")
    private String metaReferencia;

    @Transient
    private User user;

    @Transient
    @JsonIgnoreProperties(value = { "pais" }, allowSetters = true)
    private Ciudad ciudad;

    @Transient
    private Condicion condicion;

    @Transient
    private IPS ips;

    @Transient
    private Tratamiento tratamiento;

    @Transient
    private Farmaceutica farmaceutica;

    @Column("user_id")
    private String userId;

    @Column("ciudad_id")
    private Long ciudadId;

    @Column("condicion_id")
    private Long condicionId;

    @Column("ips_id")
    private Long ipsId;

    @Column("tratamiento_id")
    private Long tratamientoId;

    @Column("farmaceutica_id")
    private Long farmaceuticaId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Paciente id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public Paciente nombre(String nombre) {
        this.setNombre(nombre);
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Identificaciont getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    public Paciente tipoIdentificacion(Identificaciont tipoIdentificacion) {
        this.setTipoIdentificacion(tipoIdentificacion);
        return this;
    }

    public void setTipoIdentificacion(Identificaciont tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public Integer getIdentificacion() {
        return this.identificacion;
    }

    public Paciente identificacion(Integer identificacion) {
        this.setIdentificacion(identificacion);
        return this;
    }

    public void setIdentificacion(Integer identificacion) {
        this.identificacion = identificacion;
    }

    public Integer getEdad() {
        return this.edad;
    }

    public Paciente edad(Integer edad) {
        this.setEdad(edad);
        return this;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public Sexop getSexo() {
        return this.sexo;
    }

    public Paciente sexo(Sexop sexo) {
        this.setSexo(sexo);
        return this;
    }

    public void setSexo(Sexop sexo) {
        this.sexo = sexo;
    }

    public Integer getTelefono() {
        return this.telefono;
    }

    public Paciente telefono(Integer telefono) {
        this.setTelefono(telefono);
        return this;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public Paciente direccion(String direccion) {
        this.setDireccion(direccion);
        return this;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Float getPesoKG() {
        return this.pesoKG;
    }

    public Paciente pesoKG(Float pesoKG) {
        this.setPesoKG(pesoKG);
        return this;
    }

    public void setPesoKG(Float pesoKG) {
        this.pesoKG = pesoKG;
    }

    public Integer getEstaturaCM() {
        return this.estaturaCM;
    }

    public Paciente estaturaCM(Integer estaturaCM) {
        this.setEstaturaCM(estaturaCM);
        return this;
    }

    public void setEstaturaCM(Integer estaturaCM) {
        this.estaturaCM = estaturaCM;
    }

    public Grupoetnicop getGrupoEtnico() {
        return this.grupoEtnico;
    }

    public Paciente grupoEtnico(Grupoetnicop grupoEtnico) {
        this.setGrupoEtnico(grupoEtnico);
        return this;
    }

    public void setGrupoEtnico(Grupoetnicop grupoEtnico) {
        this.grupoEtnico = grupoEtnico;
    }

    public Estratop getEstratoSocioeconomico() {
        return this.estratoSocioeconomico;
    }

    public Paciente estratoSocioeconomico(Estratop estratoSocioeconomico) {
        this.setEstratoSocioeconomico(estratoSocioeconomico);
        return this;
    }

    public void setEstratoSocioeconomico(Estratop estratoSocioeconomico) {
        this.estratoSocioeconomico = estratoSocioeconomico;
    }

    public Integer getOximetriaReferencia() {
        return this.oximetriaReferencia;
    }

    public Paciente oximetriaReferencia(Integer oximetriaReferencia) {
        this.setOximetriaReferencia(oximetriaReferencia);
        return this;
    }

    public void setOximetriaReferencia(Integer oximetriaReferencia) {
        this.oximetriaReferencia = oximetriaReferencia;
    }

    public Integer getRitmoCardiacoReferencia() {
        return this.ritmoCardiacoReferencia;
    }

    public Paciente ritmoCardiacoReferencia(Integer ritmoCardiacoReferencia) {
        this.setRitmoCardiacoReferencia(ritmoCardiacoReferencia);
        return this;
    }

    public void setRitmoCardiacoReferencia(Integer ritmoCardiacoReferencia) {
        this.ritmoCardiacoReferencia = ritmoCardiacoReferencia;
    }

    public Integer getPresionSistolicaReferencia() {
        return this.presionSistolicaReferencia;
    }

    public Paciente presionSistolicaReferencia(Integer presionSistolicaReferencia) {
        this.setPresionSistolicaReferencia(presionSistolicaReferencia);
        return this;
    }

    public void setPresionSistolicaReferencia(Integer presionSistolicaReferencia) {
        this.presionSistolicaReferencia = presionSistolicaReferencia;
    }

    public Integer getPresionDistolicaReferencia() {
        return this.presionDistolicaReferencia;
    }

    public Paciente presionDistolicaReferencia(Integer presionDistolicaReferencia) {
        this.setPresionDistolicaReferencia(presionDistolicaReferencia);
        return this;
    }

    public void setPresionDistolicaReferencia(Integer presionDistolicaReferencia) {
        this.presionDistolicaReferencia = presionDistolicaReferencia;
    }

    public String getComentarios() {
        return this.comentarios;
    }

    public Paciente comentarios(String comentarios) {
        this.setComentarios(comentarios);
        return this;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public Integer getPasosReferencia() {
        return this.pasosReferencia;
    }

    public Paciente pasosReferencia(Integer pasosReferencia) {
        this.setPasosReferencia(pasosReferencia);
        return this;
    }

    public void setPasosReferencia(Integer pasosReferencia) {
        this.pasosReferencia = pasosReferencia;
    }

    public Integer getCaloriasReferencia() {
        return this.caloriasReferencia;
    }

    public Paciente caloriasReferencia(Integer caloriasReferencia) {
        this.setCaloriasReferencia(caloriasReferencia);
        return this;
    }

    public void setCaloriasReferencia(Integer caloriasReferencia) {
        this.caloriasReferencia = caloriasReferencia;
    }

    public String getMetaReferencia() {
        return this.metaReferencia;
    }

    public Paciente metaReferencia(String metaReferencia) {
        this.setMetaReferencia(metaReferencia);
        return this;
    }

    public void setMetaReferencia(String metaReferencia) {
        this.metaReferencia = metaReferencia;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user != null ? user.getId() : null;
    }

    public Paciente user(User user) {
        this.setUser(user);
        return this;
    }

    public Ciudad getCiudad() {
        return this.ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
        this.ciudadId = ciudad != null ? ciudad.getId() : null;
    }

    public Paciente ciudad(Ciudad ciudad) {
        this.setCiudad(ciudad);
        return this;
    }

    public Condicion getCondicion() {
        return this.condicion;
    }

    public void setCondicion(Condicion condicion) {
        this.condicion = condicion;
        this.condicionId = condicion != null ? condicion.getId() : null;
    }

    public Paciente condicion(Condicion condicion) {
        this.setCondicion(condicion);
        return this;
    }

    public IPS getIps() {
        return this.ips;
    }

    public void setIps(IPS iPS) {
        this.ips = iPS;
        this.ipsId = iPS != null ? iPS.getId() : null;
    }

    public Paciente ips(IPS iPS) {
        this.setIps(iPS);
        return this;
    }

    public Tratamiento getTratamiento() {
        return this.tratamiento;
    }

    public void setTratamiento(Tratamiento tratamiento) {
        this.tratamiento = tratamiento;
        this.tratamientoId = tratamiento != null ? tratamiento.getId() : null;
    }

    public Paciente tratamiento(Tratamiento tratamiento) {
        this.setTratamiento(tratamiento);
        return this;
    }

    public Farmaceutica getFarmaceutica() {
        return this.farmaceutica;
    }

    public void setFarmaceutica(Farmaceutica farmaceutica) {
        this.farmaceutica = farmaceutica;
        this.farmaceuticaId = farmaceutica != null ? farmaceutica.getId() : null;
    }

    public Paciente farmaceutica(Farmaceutica farmaceutica) {
        this.setFarmaceutica(farmaceutica);
        return this;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String user) {
        this.userId = user;
    }

    public Long getCiudadId() {
        return this.ciudadId;
    }

    public void setCiudadId(Long ciudad) {
        this.ciudadId = ciudad;
    }

    public Long getCondicionId() {
        return this.condicionId;
    }

    public void setCondicionId(Long condicion) {
        this.condicionId = condicion;
    }

    public Long getIpsId() {
        return this.ipsId;
    }

    public void setIpsId(Long iPS) {
        this.ipsId = iPS;
    }

    public Long getTratamientoId() {
        return this.tratamientoId;
    }

    public void setTratamientoId(Long tratamiento) {
        this.tratamientoId = tratamiento;
    }

    public Long getFarmaceuticaId() {
        return this.farmaceuticaId;
    }

    public void setFarmaceuticaId(Long farmaceutica) {
        this.farmaceuticaId = farmaceutica;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Paciente)) {
            return false;
        }
        return id != null && id.equals(((Paciente) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Paciente{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", tipoIdentificacion='" + getTipoIdentificacion() + "'" +
            ", identificacion=" + getIdentificacion() +
            ", edad=" + getEdad() +
            ", sexo='" + getSexo() + "'" +
            ", telefono=" + getTelefono() +
            ", direccion='" + getDireccion() + "'" +
            ", pesoKG=" + getPesoKG() +
            ", estaturaCM=" + getEstaturaCM() +
            ", grupoEtnico='" + getGrupoEtnico() + "'" +
            ", estratoSocioeconomico='" + getEstratoSocioeconomico() + "'" +
            ", oximetriaReferencia=" + getOximetriaReferencia() +
            ", ritmoCardiacoReferencia=" + getRitmoCardiacoReferencia() +
            ", presionSistolicaReferencia=" + getPresionSistolicaReferencia() +
            ", presionDistolicaReferencia=" + getPresionDistolicaReferencia() +
            ", comentarios='" + getComentarios() + "'" +
            ", pasosReferencia=" + getPasosReferencia() +
            ", caloriasReferencia=" + getCaloriasReferencia() +
            ", metaReferencia='" + getMetaReferencia() + "'" +
            "}";
    }
}
