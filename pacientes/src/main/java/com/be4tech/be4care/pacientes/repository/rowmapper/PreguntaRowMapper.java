package com.be4tech.be4care.pacientes.repository.rowmapper;

import com.be4tech.be4care.pacientes.domain.Pregunta;
import com.be4tech.be4care.pacientes.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Pregunta}, with proper type conversions.
 */
@Service
public class PreguntaRowMapper implements BiFunction<Row, String, Pregunta> {

    private final ColumnConverter converter;

    public PreguntaRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Pregunta} stored in the database.
     */
    @Override
    public Pregunta apply(Row row, String prefix) {
        Pregunta entity = new Pregunta();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setPregunta(converter.fromRow(row, prefix + "_pregunta", String.class));
        entity.setCondicionId(converter.fromRow(row, prefix + "_condicion_id", Long.class));
        return entity;
    }
}
