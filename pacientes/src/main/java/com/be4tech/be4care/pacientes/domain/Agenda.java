package com.be4tech.be4care.pacientes.domain;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Agenda.
 */
@Table("agenda")
public class Agenda implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("hora_medicamento")
    private Integer horaMedicamento;

    @Transient
    private Medicamento medicamento;

    @Column("medicamento_id")
    private Long medicamentoId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Agenda id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getHoraMedicamento() {
        return this.horaMedicamento;
    }

    public Agenda horaMedicamento(Integer horaMedicamento) {
        this.setHoraMedicamento(horaMedicamento);
        return this;
    }

    public void setHoraMedicamento(Integer horaMedicamento) {
        this.horaMedicamento = horaMedicamento;
    }

    public Medicamento getMedicamento() {
        return this.medicamento;
    }

    public void setMedicamento(Medicamento medicamento) {
        this.medicamento = medicamento;
        this.medicamentoId = medicamento != null ? medicamento.getId() : null;
    }

    public Agenda medicamento(Medicamento medicamento) {
        this.setMedicamento(medicamento);
        return this;
    }

    public Long getMedicamentoId() {
        return this.medicamentoId;
    }

    public void setMedicamentoId(Long medicamento) {
        this.medicamentoId = medicamento;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Agenda)) {
            return false;
        }
        return id != null && id.equals(((Agenda) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Agenda{" +
            "id=" + getId() +
            ", horaMedicamento=" + getHoraMedicamento() +
            "}";
    }
}
