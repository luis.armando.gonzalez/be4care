package com.be4tech.be4care.pacientes.repository;

import com.be4tech.be4care.pacientes.domain.Adherencia;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Adherencia entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdherenciaRepository extends R2dbcRepository<Adherencia, Long>, AdherenciaRepositoryInternal {
    Flux<Adherencia> findAllBy(Pageable pageable);

    @Query("SELECT * FROM adherencia entity WHERE entity.medicamento_id = :id")
    Flux<Adherencia> findByMedicamento(Long id);

    @Query("SELECT * FROM adherencia entity WHERE entity.medicamento_id IS NULL")
    Flux<Adherencia> findAllWhereMedicamentoIsNull();

    @Query("SELECT * FROM adherencia entity WHERE entity.paciente_id = :id")
    Flux<Adherencia> findByPaciente(Long id);

    @Query("SELECT * FROM adherencia entity WHERE entity.paciente_id IS NULL")
    Flux<Adherencia> findAllWherePacienteIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Adherencia> findAll();

    @Override
    Mono<Adherencia> findById(Long id);

    @Override
    <S extends Adherencia> Mono<S> save(S entity);
}

interface AdherenciaRepositoryInternal {
    <S extends Adherencia> Mono<S> insert(S entity);
    <S extends Adherencia> Mono<S> save(S entity);
    Mono<Integer> update(Adherencia entity);

    Flux<Adherencia> findAll();
    Mono<Adherencia> findById(Long id);
    Flux<Adherencia> findAllBy(Pageable pageable);
    Flux<Adherencia> findAllBy(Pageable pageable, Criteria criteria);
}
