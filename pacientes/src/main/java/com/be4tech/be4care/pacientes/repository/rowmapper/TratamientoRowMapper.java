package com.be4tech.be4care.pacientes.repository.rowmapper;

import com.be4tech.be4care.pacientes.domain.Tratamiento;
import com.be4tech.be4care.pacientes.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Tratamiento}, with proper type conversions.
 */
@Service
public class TratamientoRowMapper implements BiFunction<Row, String, Tratamiento> {

    private final ColumnConverter converter;

    public TratamientoRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Tratamiento} stored in the database.
     */
    @Override
    public Tratamiento apply(Row row, String prefix) {
        Tratamiento entity = new Tratamiento();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setDescripcionTratamiento(converter.fromRow(row, prefix + "_descripcion_tratamiento", String.class));
        entity.setFechaInicio(converter.fromRow(row, prefix + "_fecha_inicio", Instant.class));
        entity.setFechaFin(converter.fromRow(row, prefix + "_fecha_fin", ZonedDateTime.class));
        return entity;
    }
}
