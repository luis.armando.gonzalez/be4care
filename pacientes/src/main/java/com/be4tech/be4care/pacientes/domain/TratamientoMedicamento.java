package com.be4tech.be4care.pacientes.domain;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A TratamientoMedicamento.
 */
@Table("tratamiento_medicamento")
public class TratamientoMedicamento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("dosis")
    private String dosis;

    @Column("intensidad")
    private String intensidad;

    @Transient
    private Tratamiento tratamiento;

    @Transient
    private Medicamento medicamento;

    @Column("tratamiento_id")
    private Long tratamientoId;

    @Column("medicamento_id")
    private Long medicamentoId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public TratamientoMedicamento id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDosis() {
        return this.dosis;
    }

    public TratamientoMedicamento dosis(String dosis) {
        this.setDosis(dosis);
        return this;
    }

    public void setDosis(String dosis) {
        this.dosis = dosis;
    }

    public String getIntensidad() {
        return this.intensidad;
    }

    public TratamientoMedicamento intensidad(String intensidad) {
        this.setIntensidad(intensidad);
        return this;
    }

    public void setIntensidad(String intensidad) {
        this.intensidad = intensidad;
    }

    public Tratamiento getTratamiento() {
        return this.tratamiento;
    }

    public void setTratamiento(Tratamiento tratamiento) {
        this.tratamiento = tratamiento;
        this.tratamientoId = tratamiento != null ? tratamiento.getId() : null;
    }

    public TratamientoMedicamento tratamiento(Tratamiento tratamiento) {
        this.setTratamiento(tratamiento);
        return this;
    }

    public Medicamento getMedicamento() {
        return this.medicamento;
    }

    public void setMedicamento(Medicamento medicamento) {
        this.medicamento = medicamento;
        this.medicamentoId = medicamento != null ? medicamento.getId() : null;
    }

    public TratamientoMedicamento medicamento(Medicamento medicamento) {
        this.setMedicamento(medicamento);
        return this;
    }

    public Long getTratamientoId() {
        return this.tratamientoId;
    }

    public void setTratamientoId(Long tratamiento) {
        this.tratamientoId = tratamiento;
    }

    public Long getMedicamentoId() {
        return this.medicamentoId;
    }

    public void setMedicamentoId(Long medicamento) {
        this.medicamentoId = medicamento;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TratamientoMedicamento)) {
            return false;
        }
        return id != null && id.equals(((TratamientoMedicamento) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TratamientoMedicamento{" +
            "id=" + getId() +
            ", dosis='" + getDosis() + "'" +
            ", intensidad='" + getIntensidad() + "'" +
            "}";
    }
}
