package com.be4tech.be4care.pacientes.repository.rowmapper;

import com.be4tech.be4care.pacientes.domain.Condicion;
import com.be4tech.be4care.pacientes.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Condicion}, with proper type conversions.
 */
@Service
public class CondicionRowMapper implements BiFunction<Row, String, Condicion> {

    private final ColumnConverter converter;

    public CondicionRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Condicion} stored in the database.
     */
    @Override
    public Condicion apply(Row row, String prefix) {
        Condicion entity = new Condicion();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setCondicion(converter.fromRow(row, prefix + "_condicion", String.class));
        entity.setDescripcion(converter.fromRow(row, prefix + "_descripcion", String.class));
        return entity;
    }
}
