package com.be4tech.be4care.pacientes.web.rest;

import com.be4tech.be4care.pacientes.domain.TratamientoMedicamento;
import com.be4tech.be4care.pacientes.repository.TratamientoMedicamentoRepository;
import com.be4tech.be4care.pacientes.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.pacientes.domain.TratamientoMedicamento}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class TratamientoMedicamentoResource {

    private final Logger log = LoggerFactory.getLogger(TratamientoMedicamentoResource.class);

    private static final String ENTITY_NAME = "pacientesTratamientoMedicamento";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TratamientoMedicamentoRepository tratamientoMedicamentoRepository;

    public TratamientoMedicamentoResource(TratamientoMedicamentoRepository tratamientoMedicamentoRepository) {
        this.tratamientoMedicamentoRepository = tratamientoMedicamentoRepository;
    }

    /**
     * {@code POST  /tratamiento-medicamentos} : Create a new tratamientoMedicamento.
     *
     * @param tratamientoMedicamento the tratamientoMedicamento to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tratamientoMedicamento, or with status {@code 400 (Bad Request)} if the tratamientoMedicamento has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tratamiento-medicamentos")
    public Mono<ResponseEntity<TratamientoMedicamento>> createTratamientoMedicamento(
        @RequestBody TratamientoMedicamento tratamientoMedicamento
    ) throws URISyntaxException {
        log.debug("REST request to save TratamientoMedicamento : {}", tratamientoMedicamento);
        if (tratamientoMedicamento.getId() != null) {
            throw new BadRequestAlertException("A new tratamientoMedicamento cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return tratamientoMedicamentoRepository
            .save(tratamientoMedicamento)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/tratamiento-medicamentos/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /tratamiento-medicamentos/:id} : Updates an existing tratamientoMedicamento.
     *
     * @param id the id of the tratamientoMedicamento to save.
     * @param tratamientoMedicamento the tratamientoMedicamento to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tratamientoMedicamento,
     * or with status {@code 400 (Bad Request)} if the tratamientoMedicamento is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tratamientoMedicamento couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tratamiento-medicamentos/{id}")
    public Mono<ResponseEntity<TratamientoMedicamento>> updateTratamientoMedicamento(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody TratamientoMedicamento tratamientoMedicamento
    ) throws URISyntaxException {
        log.debug("REST request to update TratamientoMedicamento : {}, {}", id, tratamientoMedicamento);
        if (tratamientoMedicamento.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, tratamientoMedicamento.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return tratamientoMedicamentoRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                return tratamientoMedicamentoRepository
                    .save(tratamientoMedicamento)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /tratamiento-medicamentos/:id} : Partial updates given fields of an existing tratamientoMedicamento, field will ignore if it is null
     *
     * @param id the id of the tratamientoMedicamento to save.
     * @param tratamientoMedicamento the tratamientoMedicamento to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tratamientoMedicamento,
     * or with status {@code 400 (Bad Request)} if the tratamientoMedicamento is not valid,
     * or with status {@code 404 (Not Found)} if the tratamientoMedicamento is not found,
     * or with status {@code 500 (Internal Server Error)} if the tratamientoMedicamento couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/tratamiento-medicamentos/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<TratamientoMedicamento>> partialUpdateTratamientoMedicamento(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody TratamientoMedicamento tratamientoMedicamento
    ) throws URISyntaxException {
        log.debug("REST request to partial update TratamientoMedicamento partially : {}, {}", id, tratamientoMedicamento);
        if (tratamientoMedicamento.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, tratamientoMedicamento.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return tratamientoMedicamentoRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                Mono<TratamientoMedicamento> result = tratamientoMedicamentoRepository
                    .findById(tratamientoMedicamento.getId())
                    .map(existingTratamientoMedicamento -> {
                        if (tratamientoMedicamento.getDosis() != null) {
                            existingTratamientoMedicamento.setDosis(tratamientoMedicamento.getDosis());
                        }
                        if (tratamientoMedicamento.getIntensidad() != null) {
                            existingTratamientoMedicamento.setIntensidad(tratamientoMedicamento.getIntensidad());
                        }

                        return existingTratamientoMedicamento;
                    })
                    .flatMap(tratamientoMedicamentoRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /tratamiento-medicamentos} : get all the tratamientoMedicamentos.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tratamientoMedicamentos in body.
     */
    @GetMapping("/tratamiento-medicamentos")
    public Mono<ResponseEntity<List<TratamientoMedicamento>>> getAllTratamientoMedicamentos(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of TratamientoMedicamentos");
        return tratamientoMedicamentoRepository
            .count()
            .zipWith(tratamientoMedicamentoRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /tratamiento-medicamentos/:id} : get the "id" tratamientoMedicamento.
     *
     * @param id the id of the tratamientoMedicamento to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tratamientoMedicamento, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tratamiento-medicamentos/{id}")
    public Mono<ResponseEntity<TratamientoMedicamento>> getTratamientoMedicamento(@PathVariable Long id) {
        log.debug("REST request to get TratamientoMedicamento : {}", id);
        Mono<TratamientoMedicamento> tratamientoMedicamento = tratamientoMedicamentoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(tratamientoMedicamento);
    }

    /**
     * {@code DELETE  /tratamiento-medicamentos/:id} : delete the "id" tratamientoMedicamento.
     *
     * @param id the id of the tratamientoMedicamento to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tratamiento-medicamentos/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteTratamientoMedicamento(@PathVariable Long id) {
        log.debug("REST request to delete TratamientoMedicamento : {}", id);
        return tratamientoMedicamentoRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
