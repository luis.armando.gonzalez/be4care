package com.be4tech.be4care.pacientes.repository.rowmapper;

import com.be4tech.be4care.pacientes.domain.Paciente;
import com.be4tech.be4care.pacientes.domain.enumeration.Estratop;
import com.be4tech.be4care.pacientes.domain.enumeration.Grupoetnicop;
import com.be4tech.be4care.pacientes.domain.enumeration.Identificaciont;
import com.be4tech.be4care.pacientes.domain.enumeration.Sexop;
import com.be4tech.be4care.pacientes.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Paciente}, with proper type conversions.
 */
@Service
public class PacienteRowMapper implements BiFunction<Row, String, Paciente> {

    private final ColumnConverter converter;

    public PacienteRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Paciente} stored in the database.
     */
    @Override
    public Paciente apply(Row row, String prefix) {
        Paciente entity = new Paciente();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setNombre(converter.fromRow(row, prefix + "_nombre", String.class));
        entity.setTipoIdentificacion(converter.fromRow(row, prefix + "_tipo_identificacion", Identificaciont.class));
        entity.setIdentificacion(converter.fromRow(row, prefix + "_identificacion", Integer.class));
        entity.setEdad(converter.fromRow(row, prefix + "_edad", Integer.class));
        entity.setSexo(converter.fromRow(row, prefix + "_sexo", Sexop.class));
        entity.setTelefono(converter.fromRow(row, prefix + "_telefono", Integer.class));
        entity.setDireccion(converter.fromRow(row, prefix + "_direccion", String.class));
        entity.setPesoKG(converter.fromRow(row, prefix + "_peso_kg", Float.class));
        entity.setEstaturaCM(converter.fromRow(row, prefix + "_estatura_cm", Integer.class));
        entity.setGrupoEtnico(converter.fromRow(row, prefix + "_grupo_etnico", Grupoetnicop.class));
        entity.setEstratoSocioeconomico(converter.fromRow(row, prefix + "_estrato_socioeconomico", Estratop.class));
        entity.setOximetriaReferencia(converter.fromRow(row, prefix + "_oximetria_referencia", Integer.class));
        entity.setRitmoCardiacoReferencia(converter.fromRow(row, prefix + "_ritmo_cardiaco_referencia", Integer.class));
        entity.setPresionSistolicaReferencia(converter.fromRow(row, prefix + "_presion_sistolica_referencia", Integer.class));
        entity.setPresionDistolicaReferencia(converter.fromRow(row, prefix + "_presion_distolica_referencia", Integer.class));
        entity.setComentarios(converter.fromRow(row, prefix + "_comentarios", String.class));
        entity.setPasosReferencia(converter.fromRow(row, prefix + "_pasos_referencia", Integer.class));
        entity.setCaloriasReferencia(converter.fromRow(row, prefix + "_calorias_referencia", Integer.class));
        entity.setMetaReferencia(converter.fromRow(row, prefix + "_meta_referencia", String.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        entity.setCiudadId(converter.fromRow(row, prefix + "_ciudad_id", Long.class));
        entity.setCondicionId(converter.fromRow(row, prefix + "_condicion_id", Long.class));
        entity.setIpsId(converter.fromRow(row, prefix + "_ips_id", Long.class));
        entity.setTratamientoId(converter.fromRow(row, prefix + "_tratamiento_id", Long.class));
        entity.setFarmaceuticaId(converter.fromRow(row, prefix + "_farmaceutica_id", Long.class));
        return entity;
    }
}
