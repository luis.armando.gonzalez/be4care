package com.be4tech.be4care.pacientes.repository;

import com.be4tech.be4care.pacientes.domain.Pais;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Pais entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaisRepository extends R2dbcRepository<Pais, Long>, PaisRepositoryInternal {
    Flux<Pais> findAllBy(Pageable pageable);

    // just to avoid having unambigous methods
    @Override
    Flux<Pais> findAll();

    @Override
    Mono<Pais> findById(Long id);

    @Override
    <S extends Pais> Mono<S> save(S entity);
}

interface PaisRepositoryInternal {
    <S extends Pais> Mono<S> insert(S entity);
    <S extends Pais> Mono<S> save(S entity);
    Mono<Integer> update(Pais entity);

    Flux<Pais> findAll();
    Mono<Pais> findById(Long id);
    Flux<Pais> findAllBy(Pageable pageable);
    Flux<Pais> findAllBy(Pageable pageable, Criteria criteria);
}
