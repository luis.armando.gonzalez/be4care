package com.be4tech.be4care.pacientes.repository;

import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

import com.be4tech.be4care.pacientes.domain.Pregunta;
import com.be4tech.be4care.pacientes.repository.rowmapper.CondicionRowMapper;
import com.be4tech.be4care.pacientes.repository.rowmapper.PreguntaRowMapper;
import com.be4tech.be4care.pacientes.service.EntityManager;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoinCondition;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive custom repository implementation for the Pregunta entity.
 */
@SuppressWarnings("unused")
class PreguntaRepositoryInternalImpl implements PreguntaRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final CondicionRowMapper condicionMapper;
    private final PreguntaRowMapper preguntaMapper;

    private static final Table entityTable = Table.aliased("pregunta", EntityManager.ENTITY_ALIAS);
    private static final Table condicionTable = Table.aliased("condicion", "condicion");

    public PreguntaRepositoryInternalImpl(
        R2dbcEntityTemplate template,
        EntityManager entityManager,
        CondicionRowMapper condicionMapper,
        PreguntaRowMapper preguntaMapper
    ) {
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.condicionMapper = condicionMapper;
        this.preguntaMapper = preguntaMapper;
    }

    @Override
    public Flux<Pregunta> findAllBy(Pageable pageable) {
        return findAllBy(pageable, null);
    }

    @Override
    public Flux<Pregunta> findAllBy(Pageable pageable, Criteria criteria) {
        return createQuery(pageable, criteria).all();
    }

    RowsFetchSpec<Pregunta> createQuery(Pageable pageable, Criteria criteria) {
        List<Expression> columns = PreguntaSqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        columns.addAll(CondicionSqlHelper.getColumns(condicionTable, "condicion"));
        SelectFromAndJoinCondition selectFrom = Select
            .builder()
            .select(columns)
            .from(entityTable)
            .leftOuterJoin(condicionTable)
            .on(Column.create("condicion_id", entityTable))
            .equals(Column.create("id", condicionTable));

        String select = entityManager.createSelect(selectFrom, Pregunta.class, pageable, criteria);
        String alias = entityTable.getReferenceName().getReference();
        String selectWhere = Optional
            .ofNullable(criteria)
            .map(crit ->
                new StringBuilder(select)
                    .append(" ")
                    .append("WHERE")
                    .append(" ")
                    .append(alias)
                    .append(".")
                    .append(crit.toString())
                    .toString()
            )
            .orElse(select); // TODO remove once https://github.com/spring-projects/spring-data-jdbc/issues/907 will be fixed
        return db.sql(selectWhere).map(this::process);
    }

    @Override
    public Flux<Pregunta> findAll() {
        return findAllBy(null, null);
    }

    @Override
    public Mono<Pregunta> findById(Long id) {
        return createQuery(null, where("id").is(id)).one();
    }

    private Pregunta process(Row row, RowMetadata metadata) {
        Pregunta entity = preguntaMapper.apply(row, "e");
        entity.setCondicion(condicionMapper.apply(row, "condicion"));
        return entity;
    }

    @Override
    public <S extends Pregunta> Mono<S> insert(S entity) {
        return entityManager.insert(entity);
    }

    @Override
    public <S extends Pregunta> Mono<S> save(S entity) {
        if (entity.getId() == null) {
            return insert(entity);
        } else {
            return update(entity)
                .map(numberOfUpdates -> {
                    if (numberOfUpdates.intValue() <= 0) {
                        throw new IllegalStateException("Unable to update Pregunta with id = " + entity.getId());
                    }
                    return entity;
                });
        }
    }

    @Override
    public Mono<Integer> update(Pregunta entity) {
        //fixme is this the proper way?
        return r2dbcEntityTemplate.update(entity).thenReturn(1);
    }
}
