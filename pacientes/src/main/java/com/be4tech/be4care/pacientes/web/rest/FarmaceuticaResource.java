package com.be4tech.be4care.pacientes.web.rest;

import com.be4tech.be4care.pacientes.domain.Farmaceutica;
import com.be4tech.be4care.pacientes.repository.FarmaceuticaRepository;
import com.be4tech.be4care.pacientes.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.pacientes.domain.Farmaceutica}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class FarmaceuticaResource {

    private final Logger log = LoggerFactory.getLogger(FarmaceuticaResource.class);

    private static final String ENTITY_NAME = "pacientesFarmaceutica";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FarmaceuticaRepository farmaceuticaRepository;

    public FarmaceuticaResource(FarmaceuticaRepository farmaceuticaRepository) {
        this.farmaceuticaRepository = farmaceuticaRepository;
    }

    /**
     * {@code POST  /farmaceuticas} : Create a new farmaceutica.
     *
     * @param farmaceutica the farmaceutica to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new farmaceutica, or with status {@code 400 (Bad Request)} if the farmaceutica has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/farmaceuticas")
    public Mono<ResponseEntity<Farmaceutica>> createFarmaceutica(@RequestBody Farmaceutica farmaceutica) throws URISyntaxException {
        log.debug("REST request to save Farmaceutica : {}", farmaceutica);
        if (farmaceutica.getId() != null) {
            throw new BadRequestAlertException("A new farmaceutica cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return farmaceuticaRepository
            .save(farmaceutica)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/farmaceuticas/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /farmaceuticas/:id} : Updates an existing farmaceutica.
     *
     * @param id the id of the farmaceutica to save.
     * @param farmaceutica the farmaceutica to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated farmaceutica,
     * or with status {@code 400 (Bad Request)} if the farmaceutica is not valid,
     * or with status {@code 500 (Internal Server Error)} if the farmaceutica couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/farmaceuticas/{id}")
    public Mono<ResponseEntity<Farmaceutica>> updateFarmaceutica(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Farmaceutica farmaceutica
    ) throws URISyntaxException {
        log.debug("REST request to update Farmaceutica : {}, {}", id, farmaceutica);
        if (farmaceutica.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, farmaceutica.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return farmaceuticaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                return farmaceuticaRepository
                    .save(farmaceutica)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /farmaceuticas/:id} : Partial updates given fields of an existing farmaceutica, field will ignore if it is null
     *
     * @param id the id of the farmaceutica to save.
     * @param farmaceutica the farmaceutica to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated farmaceutica,
     * or with status {@code 400 (Bad Request)} if the farmaceutica is not valid,
     * or with status {@code 404 (Not Found)} if the farmaceutica is not found,
     * or with status {@code 500 (Internal Server Error)} if the farmaceutica couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/farmaceuticas/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Farmaceutica>> partialUpdateFarmaceutica(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Farmaceutica farmaceutica
    ) throws URISyntaxException {
        log.debug("REST request to partial update Farmaceutica partially : {}, {}", id, farmaceutica);
        if (farmaceutica.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, farmaceutica.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return farmaceuticaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                Mono<Farmaceutica> result = farmaceuticaRepository
                    .findById(farmaceutica.getId())
                    .map(existingFarmaceutica -> {
                        if (farmaceutica.getNombre() != null) {
                            existingFarmaceutica.setNombre(farmaceutica.getNombre());
                        }
                        if (farmaceutica.getDireccion() != null) {
                            existingFarmaceutica.setDireccion(farmaceutica.getDireccion());
                        }
                        if (farmaceutica.getPropietario() != null) {
                            existingFarmaceutica.setPropietario(farmaceutica.getPropietario());
                        }

                        return existingFarmaceutica;
                    })
                    .flatMap(farmaceuticaRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /farmaceuticas} : get all the farmaceuticas.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of farmaceuticas in body.
     */
    @GetMapping("/farmaceuticas")
    public Mono<ResponseEntity<List<Farmaceutica>>> getAllFarmaceuticas(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Farmaceuticas");
        return farmaceuticaRepository
            .count()
            .zipWith(farmaceuticaRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /farmaceuticas/:id} : get the "id" farmaceutica.
     *
     * @param id the id of the farmaceutica to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the farmaceutica, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/farmaceuticas/{id}")
    public Mono<ResponseEntity<Farmaceutica>> getFarmaceutica(@PathVariable Long id) {
        log.debug("REST request to get Farmaceutica : {}", id);
        Mono<Farmaceutica> farmaceutica = farmaceuticaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(farmaceutica);
    }

    /**
     * {@code DELETE  /farmaceuticas/:id} : delete the "id" farmaceutica.
     *
     * @param id the id of the farmaceutica to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/farmaceuticas/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteFarmaceutica(@PathVariable Long id) {
        log.debug("REST request to delete Farmaceutica : {}", id);
        return farmaceuticaRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
