package com.be4tech.be4care.pacientes.repository;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Table;

public class CuestionarioEstadoSqlHelper {

    public static List<Expression> getColumns(Table table, String columnPrefix) {
        List<Expression> columns = new ArrayList<>();
        columns.add(Column.aliased("id", table, columnPrefix + "_id"));
        columns.add(Column.aliased("valor", table, columnPrefix + "_valor"));
        columns.add(Column.aliased("valoracion", table, columnPrefix + "_valoracion"));

        columns.add(Column.aliased("pregunta_id", table, columnPrefix + "_pregunta_id"));
        return columns;
    }
}
