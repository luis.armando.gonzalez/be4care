package com.be4tech.be4care.pacientes.web.rest;

import com.be4tech.be4care.pacientes.domain.Alarma;
import com.be4tech.be4care.pacientes.repository.AlarmaRepository;
import com.be4tech.be4care.pacientes.repository.UserRepository;
import com.be4tech.be4care.pacientes.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.pacientes.domain.Alarma}.
 */
@RestController
@RequestMapping("/api")
public class AlarmaResource {

    private final Logger log = LoggerFactory.getLogger(AlarmaResource.class);

    private static final String ENTITY_NAME = "pacientesAlarma";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AlarmaRepository alarmaRepository;

    private final UserRepository userRepository;

    public AlarmaResource(AlarmaRepository alarmaRepository, UserRepository userRepository) {
        this.alarmaRepository = alarmaRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /alarmas} : Create a new alarma.
     *
     * @param alarma the alarma to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new alarma, or with status {@code 400 (Bad Request)} if the alarma has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/alarmas")
    public Mono<ResponseEntity<Alarma>> createAlarma(@RequestBody Alarma alarma) throws URISyntaxException {
        log.debug("REST request to save Alarma : {}", alarma);
        if (alarma.getId() != null) {
            throw new BadRequestAlertException("A new alarma cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (alarma.getUser() != null) {
            // Save user in case it's new and only exists in gateway
            userRepository.save(alarma.getUser());
        }
        return alarmaRepository
            .save(alarma)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/alarmas/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /alarmas/:id} : Updates an existing alarma.
     *
     * @param id the id of the alarma to save.
     * @param alarma the alarma to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated alarma,
     * or with status {@code 400 (Bad Request)} if the alarma is not valid,
     * or with status {@code 500 (Internal Server Error)} if the alarma couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/alarmas/{id}")
    public Mono<ResponseEntity<Alarma>> updateAlarma(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Alarma alarma
    ) throws URISyntaxException {
        log.debug("REST request to update Alarma : {}, {}", id, alarma);
        if (alarma.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, alarma.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return alarmaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (alarma.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(alarma.getUser());
                }
                return alarmaRepository
                    .save(alarma)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /alarmas/:id} : Partial updates given fields of an existing alarma, field will ignore if it is null
     *
     * @param id the id of the alarma to save.
     * @param alarma the alarma to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated alarma,
     * or with status {@code 400 (Bad Request)} if the alarma is not valid,
     * or with status {@code 404 (Not Found)} if the alarma is not found,
     * or with status {@code 500 (Internal Server Error)} if the alarma couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/alarmas/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Alarma>> partialUpdateAlarma(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Alarma alarma
    ) throws URISyntaxException {
        log.debug("REST request to partial update Alarma partially : {}, {}", id, alarma);
        if (alarma.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, alarma.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return alarmaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (alarma.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(alarma.getUser());
                }

                Mono<Alarma> result = alarmaRepository
                    .findById(alarma.getId())
                    .map(existingAlarma -> {
                        if (alarma.getTimeInstant() != null) {
                            existingAlarma.setTimeInstant(alarma.getTimeInstant());
                        }
                        if (alarma.getDescripcion() != null) {
                            existingAlarma.setDescripcion(alarma.getDescripcion());
                        }
                        if (alarma.getProcedimiento() != null) {
                            existingAlarma.setProcedimiento(alarma.getProcedimiento());
                        }
                        if (alarma.getTitulo() != null) {
                            existingAlarma.setTitulo(alarma.getTitulo());
                        }
                        if (alarma.getVerificar() != null) {
                            existingAlarma.setVerificar(alarma.getVerificar());
                        }
                        if (alarma.getObservaciones() != null) {
                            existingAlarma.setObservaciones(alarma.getObservaciones());
                        }
                        if (alarma.getPrioridad() != null) {
                            existingAlarma.setPrioridad(alarma.getPrioridad());
                        }

                        return existingAlarma;
                    })
                    .flatMap(alarmaRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /alarmas} : get all the alarmas.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of alarmas in body.
     */
    @GetMapping("/alarmas")
    public Mono<ResponseEntity<List<Alarma>>> getAllAlarmas(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Alarmas");
        return alarmaRepository
            .count()
            .zipWith(alarmaRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /alarmas/:id} : get the "id" alarma.
     *
     * @param id the id of the alarma to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the alarma, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/alarmas/{id}")
    public Mono<ResponseEntity<Alarma>> getAlarma(@PathVariable Long id) {
        log.debug("REST request to get Alarma : {}", id);
        Mono<Alarma> alarma = alarmaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(alarma);
    }

    /**
     * {@code DELETE  /alarmas/:id} : delete the "id" alarma.
     *
     * @param id the id of the alarma to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/alarmas/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteAlarma(@PathVariable Long id) {
        log.debug("REST request to delete Alarma : {}", id);
        return alarmaRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
