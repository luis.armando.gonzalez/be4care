package com.be4tech.be4care.pacientes.repository.rowmapper;

import com.be4tech.be4care.pacientes.domain.IPS;
import com.be4tech.be4care.pacientes.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link IPS}, with proper type conversions.
 */
@Service
public class IPSRowMapper implements BiFunction<Row, String, IPS> {

    private final ColumnConverter converter;

    public IPSRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link IPS} stored in the database.
     */
    @Override
    public IPS apply(Row row, String prefix) {
        IPS entity = new IPS();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setNombre(converter.fromRow(row, prefix + "_nombre", String.class));
        entity.setNit(converter.fromRow(row, prefix + "_nit", String.class));
        entity.setDireccion(converter.fromRow(row, prefix + "_direccion", String.class));
        entity.setTelefono(converter.fromRow(row, prefix + "_telefono", String.class));
        entity.setCorreoElectronico(converter.fromRow(row, prefix + "_correo_electronico", String.class));
        return entity;
    }
}
