package com.be4tech.be4care.pacientes.repository;

import com.be4tech.be4care.pacientes.domain.IPS;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the IPS entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IPSRepository extends R2dbcRepository<IPS, Long>, IPSRepositoryInternal {
    Flux<IPS> findAllBy(Pageable pageable);

    // just to avoid having unambigous methods
    @Override
    Flux<IPS> findAll();

    @Override
    Mono<IPS> findById(Long id);

    @Override
    <S extends IPS> Mono<S> save(S entity);
}

interface IPSRepositoryInternal {
    <S extends IPS> Mono<S> insert(S entity);
    <S extends IPS> Mono<S> save(S entity);
    Mono<Integer> update(IPS entity);

    Flux<IPS> findAll();
    Mono<IPS> findById(Long id);
    Flux<IPS> findAllBy(Pageable pageable);
    Flux<IPS> findAllBy(Pageable pageable, Criteria criteria);
}
