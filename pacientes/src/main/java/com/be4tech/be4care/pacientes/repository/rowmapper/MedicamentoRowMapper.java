package com.be4tech.be4care.pacientes.repository.rowmapper;

import com.be4tech.be4care.pacientes.domain.Medicamento;
import com.be4tech.be4care.pacientes.domain.enumeration.Presentacion;
import com.be4tech.be4care.pacientes.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Medicamento}, with proper type conversions.
 */
@Service
public class MedicamentoRowMapper implements BiFunction<Row, String, Medicamento> {

    private final ColumnConverter converter;

    public MedicamentoRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Medicamento} stored in the database.
     */
    @Override
    public Medicamento apply(Row row, String prefix) {
        Medicamento entity = new Medicamento();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setNombre(converter.fromRow(row, prefix + "_nombre", String.class));
        entity.setDescripcion(converter.fromRow(row, prefix + "_descripcion", String.class));
        entity.setFechaIngreso(converter.fromRow(row, prefix + "_fecha_ingreso", Instant.class));
        entity.setPresentacion(converter.fromRow(row, prefix + "_presentacion", Presentacion.class));
        entity.setGenerico(converter.fromRow(row, prefix + "_generico", String.class));
        return entity;
    }
}
