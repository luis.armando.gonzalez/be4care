package com.be4tech.be4care.pacientes.repository;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Table;

public class AdherenciaSqlHelper {

    public static List<Expression> getColumns(Table table, String columnPrefix) {
        List<Expression> columns = new ArrayList<>();
        columns.add(Column.aliased("id", table, columnPrefix + "_id"));
        columns.add(Column.aliased("hora_toma", table, columnPrefix + "_hora_toma"));
        columns.add(Column.aliased("respuesta", table, columnPrefix + "_respuesta"));
        columns.add(Column.aliased("valor", table, columnPrefix + "_valor"));
        columns.add(Column.aliased("comentario", table, columnPrefix + "_comentario"));

        columns.add(Column.aliased("medicamento_id", table, columnPrefix + "_medicamento_id"));
        columns.add(Column.aliased("paciente_id", table, columnPrefix + "_paciente_id"));
        return columns;
    }
}
