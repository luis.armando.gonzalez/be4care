package com.be4tech.be4care.pacientes.repository;

import com.be4tech.be4care.pacientes.domain.Farmaceutica;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Farmaceutica entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FarmaceuticaRepository extends R2dbcRepository<Farmaceutica, Long>, FarmaceuticaRepositoryInternal {
    Flux<Farmaceutica> findAllBy(Pageable pageable);

    // just to avoid having unambigous methods
    @Override
    Flux<Farmaceutica> findAll();

    @Override
    Mono<Farmaceutica> findById(Long id);

    @Override
    <S extends Farmaceutica> Mono<S> save(S entity);
}

interface FarmaceuticaRepositoryInternal {
    <S extends Farmaceutica> Mono<S> insert(S entity);
    <S extends Farmaceutica> Mono<S> save(S entity);
    Mono<Integer> update(Farmaceutica entity);

    Flux<Farmaceutica> findAll();
    Mono<Farmaceutica> findById(Long id);
    Flux<Farmaceutica> findAllBy(Pageable pageable);
    Flux<Farmaceutica> findAllBy(Pageable pageable, Criteria criteria);
}
