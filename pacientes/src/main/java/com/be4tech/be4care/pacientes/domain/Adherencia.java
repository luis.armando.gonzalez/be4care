package com.be4tech.be4care.pacientes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Adherencia.
 */
@Table("adherencia")
public class Adherencia implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("hora_toma")
    private Instant horaToma;

    @Column("respuesta")
    private Boolean respuesta;

    @Column("valor")
    private Integer valor;

    @Column("comentario")
    private String comentario;

    @Transient
    private Medicamento medicamento;

    @Transient
    @JsonIgnoreProperties(value = { "user", "ciudad", "condicion", "ips", "tratamiento", "farmaceutica" }, allowSetters = true)
    private Paciente paciente;

    @Column("medicamento_id")
    private Long medicamentoId;

    @Column("paciente_id")
    private Long pacienteId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Adherencia id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getHoraToma() {
        return this.horaToma;
    }

    public Adherencia horaToma(Instant horaToma) {
        this.setHoraToma(horaToma);
        return this;
    }

    public void setHoraToma(Instant horaToma) {
        this.horaToma = horaToma;
    }

    public Boolean getRespuesta() {
        return this.respuesta;
    }

    public Adherencia respuesta(Boolean respuesta) {
        this.setRespuesta(respuesta);
        return this;
    }

    public void setRespuesta(Boolean respuesta) {
        this.respuesta = respuesta;
    }

    public Integer getValor() {
        return this.valor;
    }

    public Adherencia valor(Integer valor) {
        this.setValor(valor);
        return this;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public String getComentario() {
        return this.comentario;
    }

    public Adherencia comentario(String comentario) {
        this.setComentario(comentario);
        return this;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Medicamento getMedicamento() {
        return this.medicamento;
    }

    public void setMedicamento(Medicamento medicamento) {
        this.medicamento = medicamento;
        this.medicamentoId = medicamento != null ? medicamento.getId() : null;
    }

    public Adherencia medicamento(Medicamento medicamento) {
        this.setMedicamento(medicamento);
        return this;
    }

    public Paciente getPaciente() {
        return this.paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
        this.pacienteId = paciente != null ? paciente.getId() : null;
    }

    public Adherencia paciente(Paciente paciente) {
        this.setPaciente(paciente);
        return this;
    }

    public Long getMedicamentoId() {
        return this.medicamentoId;
    }

    public void setMedicamentoId(Long medicamento) {
        this.medicamentoId = medicamento;
    }

    public Long getPacienteId() {
        return this.pacienteId;
    }

    public void setPacienteId(Long paciente) {
        this.pacienteId = paciente;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Adherencia)) {
            return false;
        }
        return id != null && id.equals(((Adherencia) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Adherencia{" +
            "id=" + getId() +
            ", horaToma='" + getHoraToma() + "'" +
            ", respuesta='" + getRespuesta() + "'" +
            ", valor=" + getValor() +
            ", comentario='" + getComentario() + "'" +
            "}";
    }
}
