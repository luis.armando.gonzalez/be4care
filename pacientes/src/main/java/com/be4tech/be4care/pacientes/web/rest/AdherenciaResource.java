package com.be4tech.be4care.pacientes.web.rest;

import com.be4tech.be4care.pacientes.domain.Adherencia;
import com.be4tech.be4care.pacientes.repository.AdherenciaRepository;
import com.be4tech.be4care.pacientes.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.pacientes.domain.Adherencia}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AdherenciaResource {

    private final Logger log = LoggerFactory.getLogger(AdherenciaResource.class);

    private static final String ENTITY_NAME = "pacientesAdherencia";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AdherenciaRepository adherenciaRepository;

    public AdherenciaResource(AdherenciaRepository adherenciaRepository) {
        this.adherenciaRepository = adherenciaRepository;
    }

    /**
     * {@code POST  /adherencias} : Create a new adherencia.
     *
     * @param adherencia the adherencia to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new adherencia, or with status {@code 400 (Bad Request)} if the adherencia has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/adherencias")
    public Mono<ResponseEntity<Adherencia>> createAdherencia(@RequestBody Adherencia adherencia) throws URISyntaxException {
        log.debug("REST request to save Adherencia : {}", adherencia);
        if (adherencia.getId() != null) {
            throw new BadRequestAlertException("A new adherencia cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return adherenciaRepository
            .save(adherencia)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/adherencias/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /adherencias/:id} : Updates an existing adherencia.
     *
     * @param id the id of the adherencia to save.
     * @param adherencia the adherencia to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated adherencia,
     * or with status {@code 400 (Bad Request)} if the adherencia is not valid,
     * or with status {@code 500 (Internal Server Error)} if the adherencia couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/adherencias/{id}")
    public Mono<ResponseEntity<Adherencia>> updateAdherencia(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Adherencia adherencia
    ) throws URISyntaxException {
        log.debug("REST request to update Adherencia : {}, {}", id, adherencia);
        if (adherencia.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, adherencia.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return adherenciaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                return adherenciaRepository
                    .save(adherencia)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /adherencias/:id} : Partial updates given fields of an existing adherencia, field will ignore if it is null
     *
     * @param id the id of the adherencia to save.
     * @param adherencia the adherencia to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated adherencia,
     * or with status {@code 400 (Bad Request)} if the adherencia is not valid,
     * or with status {@code 404 (Not Found)} if the adherencia is not found,
     * or with status {@code 500 (Internal Server Error)} if the adherencia couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/adherencias/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Adherencia>> partialUpdateAdherencia(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Adherencia adherencia
    ) throws URISyntaxException {
        log.debug("REST request to partial update Adherencia partially : {}, {}", id, adherencia);
        if (adherencia.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, adherencia.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return adherenciaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                Mono<Adherencia> result = adherenciaRepository
                    .findById(adherencia.getId())
                    .map(existingAdherencia -> {
                        if (adherencia.getHoraToma() != null) {
                            existingAdherencia.setHoraToma(adherencia.getHoraToma());
                        }
                        if (adherencia.getRespuesta() != null) {
                            existingAdherencia.setRespuesta(adherencia.getRespuesta());
                        }
                        if (adherencia.getValor() != null) {
                            existingAdherencia.setValor(adherencia.getValor());
                        }
                        if (adherencia.getComentario() != null) {
                            existingAdherencia.setComentario(adherencia.getComentario());
                        }

                        return existingAdherencia;
                    })
                    .flatMap(adherenciaRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /adherencias} : get all the adherencias.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of adherencias in body.
     */
    @GetMapping("/adherencias")
    public Mono<ResponseEntity<List<Adherencia>>> getAllAdherencias(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Adherencias");
        return adherenciaRepository
            .count()
            .zipWith(adherenciaRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /adherencias/:id} : get the "id" adherencia.
     *
     * @param id the id of the adherencia to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the adherencia, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/adherencias/{id}")
    public Mono<ResponseEntity<Adherencia>> getAdherencia(@PathVariable Long id) {
        log.debug("REST request to get Adherencia : {}", id);
        Mono<Adherencia> adherencia = adherenciaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(adherencia);
    }

    /**
     * {@code DELETE  /adherencias/:id} : delete the "id" adherencia.
     *
     * @param id the id of the adherencia to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/adherencias/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteAdherencia(@PathVariable Long id) {
        log.debug("REST request to delete Adherencia : {}", id);
        return adherenciaRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
