package com.be4tech.be4care.pacientes.repository.rowmapper;

import com.be4tech.be4care.pacientes.domain.Agenda;
import com.be4tech.be4care.pacientes.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Agenda}, with proper type conversions.
 */
@Service
public class AgendaRowMapper implements BiFunction<Row, String, Agenda> {

    private final ColumnConverter converter;

    public AgendaRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Agenda} stored in the database.
     */
    @Override
    public Agenda apply(Row row, String prefix) {
        Agenda entity = new Agenda();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setHoraMedicamento(converter.fromRow(row, prefix + "_hora_medicamento", Integer.class));
        entity.setMedicamentoId(converter.fromRow(row, prefix + "_medicamento_id", Long.class));
        return entity;
    }
}
