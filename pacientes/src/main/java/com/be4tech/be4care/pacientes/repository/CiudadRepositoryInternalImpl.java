package com.be4tech.be4care.pacientes.repository;

import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

import com.be4tech.be4care.pacientes.domain.Ciudad;
import com.be4tech.be4care.pacientes.repository.rowmapper.CiudadRowMapper;
import com.be4tech.be4care.pacientes.repository.rowmapper.PaisRowMapper;
import com.be4tech.be4care.pacientes.service.EntityManager;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoinCondition;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive custom repository implementation for the Ciudad entity.
 */
@SuppressWarnings("unused")
class CiudadRepositoryInternalImpl implements CiudadRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final PaisRowMapper paisMapper;
    private final CiudadRowMapper ciudadMapper;

    private static final Table entityTable = Table.aliased("ciudad", EntityManager.ENTITY_ALIAS);
    private static final Table paisTable = Table.aliased("pais", "pais");

    public CiudadRepositoryInternalImpl(
        R2dbcEntityTemplate template,
        EntityManager entityManager,
        PaisRowMapper paisMapper,
        CiudadRowMapper ciudadMapper
    ) {
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.paisMapper = paisMapper;
        this.ciudadMapper = ciudadMapper;
    }

    @Override
    public Flux<Ciudad> findAllBy(Pageable pageable) {
        return findAllBy(pageable, null);
    }

    @Override
    public Flux<Ciudad> findAllBy(Pageable pageable, Criteria criteria) {
        return createQuery(pageable, criteria).all();
    }

    RowsFetchSpec<Ciudad> createQuery(Pageable pageable, Criteria criteria) {
        List<Expression> columns = CiudadSqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        columns.addAll(PaisSqlHelper.getColumns(paisTable, "pais"));
        SelectFromAndJoinCondition selectFrom = Select
            .builder()
            .select(columns)
            .from(entityTable)
            .leftOuterJoin(paisTable)
            .on(Column.create("pais_id", entityTable))
            .equals(Column.create("id", paisTable));

        String select = entityManager.createSelect(selectFrom, Ciudad.class, pageable, criteria);
        String alias = entityTable.getReferenceName().getReference();
        String selectWhere = Optional
            .ofNullable(criteria)
            .map(crit ->
                new StringBuilder(select)
                    .append(" ")
                    .append("WHERE")
                    .append(" ")
                    .append(alias)
                    .append(".")
                    .append(crit.toString())
                    .toString()
            )
            .orElse(select); // TODO remove once https://github.com/spring-projects/spring-data-jdbc/issues/907 will be fixed
        return db.sql(selectWhere).map(this::process);
    }

    @Override
    public Flux<Ciudad> findAll() {
        return findAllBy(null, null);
    }

    @Override
    public Mono<Ciudad> findById(Long id) {
        return createQuery(null, where("id").is(id)).one();
    }

    private Ciudad process(Row row, RowMetadata metadata) {
        Ciudad entity = ciudadMapper.apply(row, "e");
        entity.setPais(paisMapper.apply(row, "pais"));
        return entity;
    }

    @Override
    public <S extends Ciudad> Mono<S> insert(S entity) {
        return entityManager.insert(entity);
    }

    @Override
    public <S extends Ciudad> Mono<S> save(S entity) {
        if (entity.getId() == null) {
            return insert(entity);
        } else {
            return update(entity)
                .map(numberOfUpdates -> {
                    if (numberOfUpdates.intValue() <= 0) {
                        throw new IllegalStateException("Unable to update Ciudad with id = " + entity.getId());
                    }
                    return entity;
                });
        }
    }

    @Override
    public Mono<Integer> update(Ciudad entity) {
        //fixme is this the proper way?
        return r2dbcEntityTemplate.update(entity).thenReturn(1);
    }
}
