package com.be4tech.be4care.pacientes.repository.rowmapper;

import com.be4tech.be4care.pacientes.domain.Adherencia;
import com.be4tech.be4care.pacientes.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Adherencia}, with proper type conversions.
 */
@Service
public class AdherenciaRowMapper implements BiFunction<Row, String, Adherencia> {

    private final ColumnConverter converter;

    public AdherenciaRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Adherencia} stored in the database.
     */
    @Override
    public Adherencia apply(Row row, String prefix) {
        Adherencia entity = new Adherencia();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setHoraToma(converter.fromRow(row, prefix + "_hora_toma", Instant.class));
        entity.setRespuesta(converter.fromRow(row, prefix + "_respuesta", Boolean.class));
        entity.setValor(converter.fromRow(row, prefix + "_valor", Integer.class));
        entity.setComentario(converter.fromRow(row, prefix + "_comentario", String.class));
        entity.setMedicamentoId(converter.fromRow(row, prefix + "_medicamento_id", Long.class));
        entity.setPacienteId(converter.fromRow(row, prefix + "_paciente_id", Long.class));
        return entity;
    }
}
