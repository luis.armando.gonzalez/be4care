package com.be4tech.be4care.pacientes.repository;

import com.be4tech.be4care.pacientes.domain.Tratamiento;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Tratamiento entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TratamientoRepository extends R2dbcRepository<Tratamiento, Long>, TratamientoRepositoryInternal {
    Flux<Tratamiento> findAllBy(Pageable pageable);

    // just to avoid having unambigous methods
    @Override
    Flux<Tratamiento> findAll();

    @Override
    Mono<Tratamiento> findById(Long id);

    @Override
    <S extends Tratamiento> Mono<S> save(S entity);
}

interface TratamientoRepositoryInternal {
    <S extends Tratamiento> Mono<S> insert(S entity);
    <S extends Tratamiento> Mono<S> save(S entity);
    Mono<Integer> update(Tratamiento entity);

    Flux<Tratamiento> findAll();
    Mono<Tratamiento> findById(Long id);
    Flux<Tratamiento> findAllBy(Pageable pageable);
    Flux<Tratamiento> findAllBy(Pageable pageable, Criteria criteria);
}
