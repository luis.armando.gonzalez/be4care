package com.be4tech.be4care.pacientes.domain;

import java.io.Serializable;
import java.time.Instant;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Alarma.
 */
@Table("alarma")
public class Alarma implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("time_instant")
    private Instant timeInstant;

    @Column("descripcion")
    private String descripcion;

    @Column("procedimiento")
    private String procedimiento;

    @Column("titulo")
    private String titulo;

    @Column("verificar")
    private Boolean verificar;

    @Column("observaciones")
    private String observaciones;

    @Column("prioridad")
    private String prioridad;

    @Transient
    private User user;

    @Column("user_id")
    private String userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Alarma id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getTimeInstant() {
        return this.timeInstant;
    }

    public Alarma timeInstant(Instant timeInstant) {
        this.setTimeInstant(timeInstant);
        return this;
    }

    public void setTimeInstant(Instant timeInstant) {
        this.timeInstant = timeInstant;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public Alarma descripcion(String descripcion) {
        this.setDescripcion(descripcion);
        return this;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getProcedimiento() {
        return this.procedimiento;
    }

    public Alarma procedimiento(String procedimiento) {
        this.setProcedimiento(procedimiento);
        return this;
    }

    public void setProcedimiento(String procedimiento) {
        this.procedimiento = procedimiento;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public Alarma titulo(String titulo) {
        this.setTitulo(titulo);
        return this;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Boolean getVerificar() {
        return this.verificar;
    }

    public Alarma verificar(Boolean verificar) {
        this.setVerificar(verificar);
        return this;
    }

    public void setVerificar(Boolean verificar) {
        this.verificar = verificar;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public Alarma observaciones(String observaciones) {
        this.setObservaciones(observaciones);
        return this;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getPrioridad() {
        return this.prioridad;
    }

    public Alarma prioridad(String prioridad) {
        this.setPrioridad(prioridad);
        return this;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user != null ? user.getId() : null;
    }

    public Alarma user(User user) {
        this.setUser(user);
        return this;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String user) {
        this.userId = user;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Alarma)) {
            return false;
        }
        return id != null && id.equals(((Alarma) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Alarma{" +
            "id=" + getId() +
            ", timeInstant='" + getTimeInstant() + "'" +
            ", descripcion='" + getDescripcion() + "'" +
            ", procedimiento='" + getProcedimiento() + "'" +
            ", titulo='" + getTitulo() + "'" +
            ", verificar='" + getVerificar() + "'" +
            ", observaciones='" + getObservaciones() + "'" +
            ", prioridad='" + getPrioridad() + "'" +
            "}";
    }
}
