package com.be4tech.be4care.pacientes.repository;

import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

import com.be4tech.be4care.pacientes.domain.Adherencia;
import com.be4tech.be4care.pacientes.repository.rowmapper.AdherenciaRowMapper;
import com.be4tech.be4care.pacientes.repository.rowmapper.MedicamentoRowMapper;
import com.be4tech.be4care.pacientes.repository.rowmapper.PacienteRowMapper;
import com.be4tech.be4care.pacientes.service.EntityManager;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoinCondition;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive custom repository implementation for the Adherencia entity.
 */
@SuppressWarnings("unused")
class AdherenciaRepositoryInternalImpl implements AdherenciaRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final MedicamentoRowMapper medicamentoMapper;
    private final PacienteRowMapper pacienteMapper;
    private final AdherenciaRowMapper adherenciaMapper;

    private static final Table entityTable = Table.aliased("adherencia", EntityManager.ENTITY_ALIAS);
    private static final Table medicamentoTable = Table.aliased("medicamento", "medicamento");
    private static final Table pacienteTable = Table.aliased("paciente", "paciente");

    public AdherenciaRepositoryInternalImpl(
        R2dbcEntityTemplate template,
        EntityManager entityManager,
        MedicamentoRowMapper medicamentoMapper,
        PacienteRowMapper pacienteMapper,
        AdherenciaRowMapper adherenciaMapper
    ) {
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.medicamentoMapper = medicamentoMapper;
        this.pacienteMapper = pacienteMapper;
        this.adherenciaMapper = adherenciaMapper;
    }

    @Override
    public Flux<Adherencia> findAllBy(Pageable pageable) {
        return findAllBy(pageable, null);
    }

    @Override
    public Flux<Adherencia> findAllBy(Pageable pageable, Criteria criteria) {
        return createQuery(pageable, criteria).all();
    }

    RowsFetchSpec<Adherencia> createQuery(Pageable pageable, Criteria criteria) {
        List<Expression> columns = AdherenciaSqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        columns.addAll(MedicamentoSqlHelper.getColumns(medicamentoTable, "medicamento"));
        columns.addAll(PacienteSqlHelper.getColumns(pacienteTable, "paciente"));
        SelectFromAndJoinCondition selectFrom = Select
            .builder()
            .select(columns)
            .from(entityTable)
            .leftOuterJoin(medicamentoTable)
            .on(Column.create("medicamento_id", entityTable))
            .equals(Column.create("id", medicamentoTable))
            .leftOuterJoin(pacienteTable)
            .on(Column.create("paciente_id", entityTable))
            .equals(Column.create("id", pacienteTable));

        String select = entityManager.createSelect(selectFrom, Adherencia.class, pageable, criteria);
        String alias = entityTable.getReferenceName().getReference();
        String selectWhere = Optional
            .ofNullable(criteria)
            .map(crit ->
                new StringBuilder(select)
                    .append(" ")
                    .append("WHERE")
                    .append(" ")
                    .append(alias)
                    .append(".")
                    .append(crit.toString())
                    .toString()
            )
            .orElse(select); // TODO remove once https://github.com/spring-projects/spring-data-jdbc/issues/907 will be fixed
        return db.sql(selectWhere).map(this::process);
    }

    @Override
    public Flux<Adherencia> findAll() {
        return findAllBy(null, null);
    }

    @Override
    public Mono<Adherencia> findById(Long id) {
        return createQuery(null, where("id").is(id)).one();
    }

    private Adherencia process(Row row, RowMetadata metadata) {
        Adherencia entity = adherenciaMapper.apply(row, "e");
        entity.setMedicamento(medicamentoMapper.apply(row, "medicamento"));
        entity.setPaciente(pacienteMapper.apply(row, "paciente"));
        return entity;
    }

    @Override
    public <S extends Adherencia> Mono<S> insert(S entity) {
        return entityManager.insert(entity);
    }

    @Override
    public <S extends Adherencia> Mono<S> save(S entity) {
        if (entity.getId() == null) {
            return insert(entity);
        } else {
            return update(entity)
                .map(numberOfUpdates -> {
                    if (numberOfUpdates.intValue() <= 0) {
                        throw new IllegalStateException("Unable to update Adherencia with id = " + entity.getId());
                    }
                    return entity;
                });
        }
    }

    @Override
    public Mono<Integer> update(Adherencia entity) {
        //fixme is this the proper way?
        return r2dbcEntityTemplate.update(entity).thenReturn(1);
    }
}
