package com.be4tech.be4care.pacientes.repository;

import com.be4tech.be4care.pacientes.domain.Condicion;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Condicion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CondicionRepository extends R2dbcRepository<Condicion, Long>, CondicionRepositoryInternal {
    Flux<Condicion> findAllBy(Pageable pageable);

    // just to avoid having unambigous methods
    @Override
    Flux<Condicion> findAll();

    @Override
    Mono<Condicion> findById(Long id);

    @Override
    <S extends Condicion> Mono<S> save(S entity);
}

interface CondicionRepositoryInternal {
    <S extends Condicion> Mono<S> insert(S entity);
    <S extends Condicion> Mono<S> save(S entity);
    Mono<Integer> update(Condicion entity);

    Flux<Condicion> findAll();
    Mono<Condicion> findById(Long id);
    Flux<Condicion> findAllBy(Pageable pageable);
    Flux<Condicion> findAllBy(Pageable pageable, Criteria criteria);
}
