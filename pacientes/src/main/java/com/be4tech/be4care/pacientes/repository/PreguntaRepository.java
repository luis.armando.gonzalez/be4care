package com.be4tech.be4care.pacientes.repository;

import com.be4tech.be4care.pacientes.domain.Pregunta;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Pregunta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PreguntaRepository extends R2dbcRepository<Pregunta, Long>, PreguntaRepositoryInternal {
    Flux<Pregunta> findAllBy(Pageable pageable);

    @Query("SELECT * FROM pregunta entity WHERE entity.condicion_id = :id")
    Flux<Pregunta> findByCondicion(Long id);

    @Query("SELECT * FROM pregunta entity WHERE entity.condicion_id IS NULL")
    Flux<Pregunta> findAllWhereCondicionIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Pregunta> findAll();

    @Override
    Mono<Pregunta> findById(Long id);

    @Override
    <S extends Pregunta> Mono<S> save(S entity);
}

interface PreguntaRepositoryInternal {
    <S extends Pregunta> Mono<S> insert(S entity);
    <S extends Pregunta> Mono<S> save(S entity);
    Mono<Integer> update(Pregunta entity);

    Flux<Pregunta> findAll();
    Mono<Pregunta> findById(Long id);
    Flux<Pregunta> findAllBy(Pageable pageable);
    Flux<Pregunta> findAllBy(Pageable pageable, Criteria criteria);
}
