package com.be4tech.be4care.pacientes.repository;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Table;

public class PacienteSqlHelper {

    public static List<Expression> getColumns(Table table, String columnPrefix) {
        List<Expression> columns = new ArrayList<>();
        columns.add(Column.aliased("id", table, columnPrefix + "_id"));
        columns.add(Column.aliased("nombre", table, columnPrefix + "_nombre"));
        columns.add(Column.aliased("tipo_identificacion", table, columnPrefix + "_tipo_identificacion"));
        columns.add(Column.aliased("identificacion", table, columnPrefix + "_identificacion"));
        columns.add(Column.aliased("edad", table, columnPrefix + "_edad"));
        columns.add(Column.aliased("sexo", table, columnPrefix + "_sexo"));
        columns.add(Column.aliased("telefono", table, columnPrefix + "_telefono"));
        columns.add(Column.aliased("direccion", table, columnPrefix + "_direccion"));
        columns.add(Column.aliased("peso_kg", table, columnPrefix + "_peso_kg"));
        columns.add(Column.aliased("estatura_cm", table, columnPrefix + "_estatura_cm"));
        columns.add(Column.aliased("grupo_etnico", table, columnPrefix + "_grupo_etnico"));
        columns.add(Column.aliased("estrato_socioeconomico", table, columnPrefix + "_estrato_socioeconomico"));
        columns.add(Column.aliased("oximetria_referencia", table, columnPrefix + "_oximetria_referencia"));
        columns.add(Column.aliased("ritmo_cardiaco_referencia", table, columnPrefix + "_ritmo_cardiaco_referencia"));
        columns.add(Column.aliased("presion_sistolica_referencia", table, columnPrefix + "_presion_sistolica_referencia"));
        columns.add(Column.aliased("presion_distolica_referencia", table, columnPrefix + "_presion_distolica_referencia"));
        columns.add(Column.aliased("comentarios", table, columnPrefix + "_comentarios"));
        columns.add(Column.aliased("pasos_referencia", table, columnPrefix + "_pasos_referencia"));
        columns.add(Column.aliased("calorias_referencia", table, columnPrefix + "_calorias_referencia"));
        columns.add(Column.aliased("meta_referencia", table, columnPrefix + "_meta_referencia"));

        columns.add(Column.aliased("user_id", table, columnPrefix + "_user_id"));
        columns.add(Column.aliased("ciudad_id", table, columnPrefix + "_ciudad_id"));
        columns.add(Column.aliased("condicion_id", table, columnPrefix + "_condicion_id"));
        columns.add(Column.aliased("ips_id", table, columnPrefix + "_ips_id"));
        columns.add(Column.aliased("tratamiento_id", table, columnPrefix + "_tratamiento_id"));
        columns.add(Column.aliased("farmaceutica_id", table, columnPrefix + "_farmaceutica_id"));
        return columns;
    }
}
