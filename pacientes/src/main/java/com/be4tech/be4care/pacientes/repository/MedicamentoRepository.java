package com.be4tech.be4care.pacientes.repository;

import com.be4tech.be4care.pacientes.domain.Medicamento;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Medicamento entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MedicamentoRepository extends R2dbcRepository<Medicamento, Long>, MedicamentoRepositoryInternal {
    Flux<Medicamento> findAllBy(Pageable pageable);

    // just to avoid having unambigous methods
    @Override
    Flux<Medicamento> findAll();

    @Override
    Mono<Medicamento> findById(Long id);

    @Override
    <S extends Medicamento> Mono<S> save(S entity);
}

interface MedicamentoRepositoryInternal {
    <S extends Medicamento> Mono<S> insert(S entity);
    <S extends Medicamento> Mono<S> save(S entity);
    Mono<Integer> update(Medicamento entity);

    Flux<Medicamento> findAll();
    Mono<Medicamento> findById(Long id);
    Flux<Medicamento> findAllBy(Pageable pageable);
    Flux<Medicamento> findAllBy(Pageable pageable, Criteria criteria);
}
