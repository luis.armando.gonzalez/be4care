package com.be4tech.be4care.pacientes.repository;

import com.be4tech.be4care.pacientes.domain.Paciente;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Paciente entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PacienteRepository extends R2dbcRepository<Paciente, Long>, PacienteRepositoryInternal {
    Flux<Paciente> findAllBy(Pageable pageable);

    @Query("SELECT * FROM paciente entity WHERE entity.user_id = :id")
    Flux<Paciente> findByUser(Long id);

    @Query("SELECT * FROM paciente entity WHERE entity.user_id IS NULL")
    Flux<Paciente> findAllWhereUserIsNull();

    @Query("SELECT * FROM paciente entity WHERE entity.ciudad_id = :id")
    Flux<Paciente> findByCiudad(Long id);

    @Query("SELECT * FROM paciente entity WHERE entity.ciudad_id IS NULL")
    Flux<Paciente> findAllWhereCiudadIsNull();

    @Query("SELECT * FROM paciente entity WHERE entity.condicion_id = :id")
    Flux<Paciente> findByCondicion(Long id);

    @Query("SELECT * FROM paciente entity WHERE entity.condicion_id IS NULL")
    Flux<Paciente> findAllWhereCondicionIsNull();

    @Query("SELECT * FROM paciente entity WHERE entity.ips_id = :id")
    Flux<Paciente> findByIps(Long id);

    @Query("SELECT * FROM paciente entity WHERE entity.ips_id IS NULL")
    Flux<Paciente> findAllWhereIpsIsNull();

    @Query("SELECT * FROM paciente entity WHERE entity.tratamiento_id = :id")
    Flux<Paciente> findByTratamiento(Long id);

    @Query("SELECT * FROM paciente entity WHERE entity.tratamiento_id IS NULL")
    Flux<Paciente> findAllWhereTratamientoIsNull();

    @Query("SELECT * FROM paciente entity WHERE entity.farmaceutica_id = :id")
    Flux<Paciente> findByFarmaceutica(Long id);

    @Query("SELECT * FROM paciente entity WHERE entity.farmaceutica_id IS NULL")
    Flux<Paciente> findAllWhereFarmaceuticaIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Paciente> findAll();

    @Override
    Mono<Paciente> findById(Long id);

    @Override
    <S extends Paciente> Mono<S> save(S entity);
}

interface PacienteRepositoryInternal {
    <S extends Paciente> Mono<S> insert(S entity);
    <S extends Paciente> Mono<S> save(S entity);
    Mono<Integer> update(Paciente entity);

    Flux<Paciente> findAll();
    Mono<Paciente> findById(Long id);
    Flux<Paciente> findAllBy(Pageable pageable);
    Flux<Paciente> findAllBy(Pageable pageable, Criteria criteria);
}
