package com.be4tech.be4care.pacientes.web.rest;

import com.be4tech.be4care.pacientes.domain.Condicion;
import com.be4tech.be4care.pacientes.repository.CondicionRepository;
import com.be4tech.be4care.pacientes.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.pacientes.domain.Condicion}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CondicionResource {

    private final Logger log = LoggerFactory.getLogger(CondicionResource.class);

    private static final String ENTITY_NAME = "pacientesCondicion";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CondicionRepository condicionRepository;

    public CondicionResource(CondicionRepository condicionRepository) {
        this.condicionRepository = condicionRepository;
    }

    /**
     * {@code POST  /condicions} : Create a new condicion.
     *
     * @param condicion the condicion to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new condicion, or with status {@code 400 (Bad Request)} if the condicion has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/condicions")
    public Mono<ResponseEntity<Condicion>> createCondicion(@RequestBody Condicion condicion) throws URISyntaxException {
        log.debug("REST request to save Condicion : {}", condicion);
        if (condicion.getId() != null) {
            throw new BadRequestAlertException("A new condicion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return condicionRepository
            .save(condicion)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/condicions/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /condicions/:id} : Updates an existing condicion.
     *
     * @param id the id of the condicion to save.
     * @param condicion the condicion to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated condicion,
     * or with status {@code 400 (Bad Request)} if the condicion is not valid,
     * or with status {@code 500 (Internal Server Error)} if the condicion couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/condicions/{id}")
    public Mono<ResponseEntity<Condicion>> updateCondicion(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Condicion condicion
    ) throws URISyntaxException {
        log.debug("REST request to update Condicion : {}, {}", id, condicion);
        if (condicion.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, condicion.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return condicionRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                return condicionRepository
                    .save(condicion)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /condicions/:id} : Partial updates given fields of an existing condicion, field will ignore if it is null
     *
     * @param id the id of the condicion to save.
     * @param condicion the condicion to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated condicion,
     * or with status {@code 400 (Bad Request)} if the condicion is not valid,
     * or with status {@code 404 (Not Found)} if the condicion is not found,
     * or with status {@code 500 (Internal Server Error)} if the condicion couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/condicions/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Condicion>> partialUpdateCondicion(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Condicion condicion
    ) throws URISyntaxException {
        log.debug("REST request to partial update Condicion partially : {}, {}", id, condicion);
        if (condicion.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, condicion.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return condicionRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                Mono<Condicion> result = condicionRepository
                    .findById(condicion.getId())
                    .map(existingCondicion -> {
                        if (condicion.getCondicion() != null) {
                            existingCondicion.setCondicion(condicion.getCondicion());
                        }
                        if (condicion.getDescripcion() != null) {
                            existingCondicion.setDescripcion(condicion.getDescripcion());
                        }

                        return existingCondicion;
                    })
                    .flatMap(condicionRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /condicions} : get all the condicions.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of condicions in body.
     */
    @GetMapping("/condicions")
    public Mono<ResponseEntity<List<Condicion>>> getAllCondicions(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Condicions");
        return condicionRepository
            .count()
            .zipWith(condicionRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /condicions/:id} : get the "id" condicion.
     *
     * @param id the id of the condicion to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the condicion, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/condicions/{id}")
    public Mono<ResponseEntity<Condicion>> getCondicion(@PathVariable Long id) {
        log.debug("REST request to get Condicion : {}", id);
        Mono<Condicion> condicion = condicionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(condicion);
    }

    /**
     * {@code DELETE  /condicions/:id} : delete the "id" condicion.
     *
     * @param id the id of the condicion to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/condicions/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteCondicion(@PathVariable Long id) {
        log.debug("REST request to delete Condicion : {}", id);
        return condicionRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
