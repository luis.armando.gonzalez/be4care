package com.be4tech.be4care.pacientes.web.rest;

import com.be4tech.be4care.pacientes.domain.Tratamiento;
import com.be4tech.be4care.pacientes.repository.TratamientoRepository;
import com.be4tech.be4care.pacientes.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.pacientes.domain.Tratamiento}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class TratamientoResource {

    private final Logger log = LoggerFactory.getLogger(TratamientoResource.class);

    private static final String ENTITY_NAME = "pacientesTratamiento";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TratamientoRepository tratamientoRepository;

    public TratamientoResource(TratamientoRepository tratamientoRepository) {
        this.tratamientoRepository = tratamientoRepository;
    }

    /**
     * {@code POST  /tratamientos} : Create a new tratamiento.
     *
     * @param tratamiento the tratamiento to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tratamiento, or with status {@code 400 (Bad Request)} if the tratamiento has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tratamientos")
    public Mono<ResponseEntity<Tratamiento>> createTratamiento(@RequestBody Tratamiento tratamiento) throws URISyntaxException {
        log.debug("REST request to save Tratamiento : {}", tratamiento);
        if (tratamiento.getId() != null) {
            throw new BadRequestAlertException("A new tratamiento cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return tratamientoRepository
            .save(tratamiento)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/tratamientos/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /tratamientos/:id} : Updates an existing tratamiento.
     *
     * @param id the id of the tratamiento to save.
     * @param tratamiento the tratamiento to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tratamiento,
     * or with status {@code 400 (Bad Request)} if the tratamiento is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tratamiento couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tratamientos/{id}")
    public Mono<ResponseEntity<Tratamiento>> updateTratamiento(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Tratamiento tratamiento
    ) throws URISyntaxException {
        log.debug("REST request to update Tratamiento : {}, {}", id, tratamiento);
        if (tratamiento.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, tratamiento.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return tratamientoRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                return tratamientoRepository
                    .save(tratamiento)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /tratamientos/:id} : Partial updates given fields of an existing tratamiento, field will ignore if it is null
     *
     * @param id the id of the tratamiento to save.
     * @param tratamiento the tratamiento to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tratamiento,
     * or with status {@code 400 (Bad Request)} if the tratamiento is not valid,
     * or with status {@code 404 (Not Found)} if the tratamiento is not found,
     * or with status {@code 500 (Internal Server Error)} if the tratamiento couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/tratamientos/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Tratamiento>> partialUpdateTratamiento(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Tratamiento tratamiento
    ) throws URISyntaxException {
        log.debug("REST request to partial update Tratamiento partially : {}, {}", id, tratamiento);
        if (tratamiento.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, tratamiento.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return tratamientoRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                Mono<Tratamiento> result = tratamientoRepository
                    .findById(tratamiento.getId())
                    .map(existingTratamiento -> {
                        if (tratamiento.getDescripcionTratamiento() != null) {
                            existingTratamiento.setDescripcionTratamiento(tratamiento.getDescripcionTratamiento());
                        }
                        if (tratamiento.getFechaInicio() != null) {
                            existingTratamiento.setFechaInicio(tratamiento.getFechaInicio());
                        }
                        if (tratamiento.getFechaFin() != null) {
                            existingTratamiento.setFechaFin(tratamiento.getFechaFin());
                        }

                        return existingTratamiento;
                    })
                    .flatMap(tratamientoRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /tratamientos} : get all the tratamientos.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tratamientos in body.
     */
    @GetMapping("/tratamientos")
    public Mono<ResponseEntity<List<Tratamiento>>> getAllTratamientos(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Tratamientos");
        return tratamientoRepository
            .count()
            .zipWith(tratamientoRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /tratamientos/:id} : get the "id" tratamiento.
     *
     * @param id the id of the tratamiento to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tratamiento, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tratamientos/{id}")
    public Mono<ResponseEntity<Tratamiento>> getTratamiento(@PathVariable Long id) {
        log.debug("REST request to get Tratamiento : {}", id);
        Mono<Tratamiento> tratamiento = tratamientoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(tratamiento);
    }

    /**
     * {@code DELETE  /tratamientos/:id} : delete the "id" tratamiento.
     *
     * @param id the id of the tratamiento to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tratamientos/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteTratamiento(@PathVariable Long id) {
        log.debug("REST request to delete Tratamiento : {}", id);
        return tratamientoRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
