package com.be4tech.be4care.pacientes.repository.rowmapper;

import com.be4tech.be4care.pacientes.domain.CuestionarioEstado;
import com.be4tech.be4care.pacientes.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link CuestionarioEstado}, with proper type conversions.
 */
@Service
public class CuestionarioEstadoRowMapper implements BiFunction<Row, String, CuestionarioEstado> {

    private final ColumnConverter converter;

    public CuestionarioEstadoRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link CuestionarioEstado} stored in the database.
     */
    @Override
    public CuestionarioEstado apply(Row row, String prefix) {
        CuestionarioEstado entity = new CuestionarioEstado();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setValor(converter.fromRow(row, prefix + "_valor", Integer.class));
        entity.setValoracion(converter.fromRow(row, prefix + "_valoracion", String.class));
        entity.setPreguntaId(converter.fromRow(row, prefix + "_pregunta_id", Long.class));
        return entity;
    }
}
