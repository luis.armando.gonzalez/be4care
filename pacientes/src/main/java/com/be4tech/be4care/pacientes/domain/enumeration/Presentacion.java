package com.be4tech.be4care.pacientes.domain.enumeration;

/**
 * The Presentacion enumeration.
 */
public enum Presentacion {
    Comprimido,
    Gragea,
    Capsula,
    Polvo,
    Pildora,
    Unguento,
    Pomada,
    Crema,
    Pasta,
    Solucion,
    Jarabe,
    Suspension,
    Emulsion,
    Locion,
}
