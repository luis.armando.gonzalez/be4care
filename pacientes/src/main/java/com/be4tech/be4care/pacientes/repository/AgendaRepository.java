package com.be4tech.be4care.pacientes.repository;

import com.be4tech.be4care.pacientes.domain.Agenda;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Agenda entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgendaRepository extends R2dbcRepository<Agenda, Long>, AgendaRepositoryInternal {
    Flux<Agenda> findAllBy(Pageable pageable);

    @Query("SELECT * FROM agenda entity WHERE entity.medicamento_id = :id")
    Flux<Agenda> findByMedicamento(Long id);

    @Query("SELECT * FROM agenda entity WHERE entity.medicamento_id IS NULL")
    Flux<Agenda> findAllWhereMedicamentoIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Agenda> findAll();

    @Override
    Mono<Agenda> findById(Long id);

    @Override
    <S extends Agenda> Mono<S> save(S entity);
}

interface AgendaRepositoryInternal {
    <S extends Agenda> Mono<S> insert(S entity);
    <S extends Agenda> Mono<S> save(S entity);
    Mono<Integer> update(Agenda entity);

    Flux<Agenda> findAll();
    Mono<Agenda> findById(Long id);
    Flux<Agenda> findAllBy(Pageable pageable);
    Flux<Agenda> findAllBy(Pageable pageable, Criteria criteria);
}
