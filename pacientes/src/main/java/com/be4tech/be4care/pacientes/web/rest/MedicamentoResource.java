package com.be4tech.be4care.pacientes.web.rest;

import com.be4tech.be4care.pacientes.domain.Medicamento;
import com.be4tech.be4care.pacientes.repository.MedicamentoRepository;
import com.be4tech.be4care.pacientes.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.pacientes.domain.Medicamento}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class MedicamentoResource {

    private final Logger log = LoggerFactory.getLogger(MedicamentoResource.class);

    private static final String ENTITY_NAME = "pacientesMedicamento";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MedicamentoRepository medicamentoRepository;

    public MedicamentoResource(MedicamentoRepository medicamentoRepository) {
        this.medicamentoRepository = medicamentoRepository;
    }

    /**
     * {@code POST  /medicamentos} : Create a new medicamento.
     *
     * @param medicamento the medicamento to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new medicamento, or with status {@code 400 (Bad Request)} if the medicamento has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/medicamentos")
    public Mono<ResponseEntity<Medicamento>> createMedicamento(@RequestBody Medicamento medicamento) throws URISyntaxException {
        log.debug("REST request to save Medicamento : {}", medicamento);
        if (medicamento.getId() != null) {
            throw new BadRequestAlertException("A new medicamento cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return medicamentoRepository
            .save(medicamento)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/medicamentos/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /medicamentos/:id} : Updates an existing medicamento.
     *
     * @param id the id of the medicamento to save.
     * @param medicamento the medicamento to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated medicamento,
     * or with status {@code 400 (Bad Request)} if the medicamento is not valid,
     * or with status {@code 500 (Internal Server Error)} if the medicamento couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/medicamentos/{id}")
    public Mono<ResponseEntity<Medicamento>> updateMedicamento(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Medicamento medicamento
    ) throws URISyntaxException {
        log.debug("REST request to update Medicamento : {}, {}", id, medicamento);
        if (medicamento.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, medicamento.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return medicamentoRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                return medicamentoRepository
                    .save(medicamento)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /medicamentos/:id} : Partial updates given fields of an existing medicamento, field will ignore if it is null
     *
     * @param id the id of the medicamento to save.
     * @param medicamento the medicamento to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated medicamento,
     * or with status {@code 400 (Bad Request)} if the medicamento is not valid,
     * or with status {@code 404 (Not Found)} if the medicamento is not found,
     * or with status {@code 500 (Internal Server Error)} if the medicamento couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/medicamentos/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Medicamento>> partialUpdateMedicamento(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Medicamento medicamento
    ) throws URISyntaxException {
        log.debug("REST request to partial update Medicamento partially : {}, {}", id, medicamento);
        if (medicamento.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, medicamento.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return medicamentoRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                Mono<Medicamento> result = medicamentoRepository
                    .findById(medicamento.getId())
                    .map(existingMedicamento -> {
                        if (medicamento.getNombre() != null) {
                            existingMedicamento.setNombre(medicamento.getNombre());
                        }
                        if (medicamento.getDescripcion() != null) {
                            existingMedicamento.setDescripcion(medicamento.getDescripcion());
                        }
                        if (medicamento.getFechaIngreso() != null) {
                            existingMedicamento.setFechaIngreso(medicamento.getFechaIngreso());
                        }
                        if (medicamento.getPresentacion() != null) {
                            existingMedicamento.setPresentacion(medicamento.getPresentacion());
                        }
                        if (medicamento.getGenerico() != null) {
                            existingMedicamento.setGenerico(medicamento.getGenerico());
                        }

                        return existingMedicamento;
                    })
                    .flatMap(medicamentoRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /medicamentos} : get all the medicamentos.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of medicamentos in body.
     */
    @GetMapping("/medicamentos")
    public Mono<ResponseEntity<List<Medicamento>>> getAllMedicamentos(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Medicamentos");
        return medicamentoRepository
            .count()
            .zipWith(medicamentoRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /medicamentos/:id} : get the "id" medicamento.
     *
     * @param id the id of the medicamento to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the medicamento, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/medicamentos/{id}")
    public Mono<ResponseEntity<Medicamento>> getMedicamento(@PathVariable Long id) {
        log.debug("REST request to get Medicamento : {}", id);
        Mono<Medicamento> medicamento = medicamentoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(medicamento);
    }

    /**
     * {@code DELETE  /medicamentos/:id} : delete the "id" medicamento.
     *
     * @param id the id of the medicamento to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/medicamentos/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteMedicamento(@PathVariable Long id) {
        log.debug("REST request to delete Medicamento : {}", id);
        return medicamentoRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
