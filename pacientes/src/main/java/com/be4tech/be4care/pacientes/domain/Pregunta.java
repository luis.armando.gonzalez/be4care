package com.be4tech.be4care.pacientes.domain;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Pregunta.
 */
@Table("pregunta")
public class Pregunta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("pregunta")
    private String pregunta;

    @Transient
    private Condicion condicion;

    @Column("condicion_id")
    private Long condicionId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Pregunta id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPregunta() {
        return this.pregunta;
    }

    public Pregunta pregunta(String pregunta) {
        this.setPregunta(pregunta);
        return this;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public Condicion getCondicion() {
        return this.condicion;
    }

    public void setCondicion(Condicion condicion) {
        this.condicion = condicion;
        this.condicionId = condicion != null ? condicion.getId() : null;
    }

    public Pregunta condicion(Condicion condicion) {
        this.setCondicion(condicion);
        return this;
    }

    public Long getCondicionId() {
        return this.condicionId;
    }

    public void setCondicionId(Long condicion) {
        this.condicionId = condicion;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Pregunta)) {
            return false;
        }
        return id != null && id.equals(((Pregunta) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Pregunta{" +
            "id=" + getId() +
            ", pregunta='" + getPregunta() + "'" +
            "}";
    }
}
