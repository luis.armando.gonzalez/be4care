package com.be4tech.be4care.pacientes.repository;

import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

import com.be4tech.be4care.pacientes.domain.Agenda;
import com.be4tech.be4care.pacientes.repository.rowmapper.AgendaRowMapper;
import com.be4tech.be4care.pacientes.repository.rowmapper.MedicamentoRowMapper;
import com.be4tech.be4care.pacientes.service.EntityManager;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoinCondition;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive custom repository implementation for the Agenda entity.
 */
@SuppressWarnings("unused")
class AgendaRepositoryInternalImpl implements AgendaRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final MedicamentoRowMapper medicamentoMapper;
    private final AgendaRowMapper agendaMapper;

    private static final Table entityTable = Table.aliased("agenda", EntityManager.ENTITY_ALIAS);
    private static final Table medicamentoTable = Table.aliased("medicamento", "medicamento");

    public AgendaRepositoryInternalImpl(
        R2dbcEntityTemplate template,
        EntityManager entityManager,
        MedicamentoRowMapper medicamentoMapper,
        AgendaRowMapper agendaMapper
    ) {
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.medicamentoMapper = medicamentoMapper;
        this.agendaMapper = agendaMapper;
    }

    @Override
    public Flux<Agenda> findAllBy(Pageable pageable) {
        return findAllBy(pageable, null);
    }

    @Override
    public Flux<Agenda> findAllBy(Pageable pageable, Criteria criteria) {
        return createQuery(pageable, criteria).all();
    }

    RowsFetchSpec<Agenda> createQuery(Pageable pageable, Criteria criteria) {
        List<Expression> columns = AgendaSqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        columns.addAll(MedicamentoSqlHelper.getColumns(medicamentoTable, "medicamento"));
        SelectFromAndJoinCondition selectFrom = Select
            .builder()
            .select(columns)
            .from(entityTable)
            .leftOuterJoin(medicamentoTable)
            .on(Column.create("medicamento_id", entityTable))
            .equals(Column.create("id", medicamentoTable));

        String select = entityManager.createSelect(selectFrom, Agenda.class, pageable, criteria);
        String alias = entityTable.getReferenceName().getReference();
        String selectWhere = Optional
            .ofNullable(criteria)
            .map(crit ->
                new StringBuilder(select)
                    .append(" ")
                    .append("WHERE")
                    .append(" ")
                    .append(alias)
                    .append(".")
                    .append(crit.toString())
                    .toString()
            )
            .orElse(select); // TODO remove once https://github.com/spring-projects/spring-data-jdbc/issues/907 will be fixed
        return db.sql(selectWhere).map(this::process);
    }

    @Override
    public Flux<Agenda> findAll() {
        return findAllBy(null, null);
    }

    @Override
    public Mono<Agenda> findById(Long id) {
        return createQuery(null, where("id").is(id)).one();
    }

    private Agenda process(Row row, RowMetadata metadata) {
        Agenda entity = agendaMapper.apply(row, "e");
        entity.setMedicamento(medicamentoMapper.apply(row, "medicamento"));
        return entity;
    }

    @Override
    public <S extends Agenda> Mono<S> insert(S entity) {
        return entityManager.insert(entity);
    }

    @Override
    public <S extends Agenda> Mono<S> save(S entity) {
        if (entity.getId() == null) {
            return insert(entity);
        } else {
            return update(entity)
                .map(numberOfUpdates -> {
                    if (numberOfUpdates.intValue() <= 0) {
                        throw new IllegalStateException("Unable to update Agenda with id = " + entity.getId());
                    }
                    return entity;
                });
        }
    }

    @Override
    public Mono<Integer> update(Agenda entity) {
        //fixme is this the proper way?
        return r2dbcEntityTemplate.update(entity).thenReturn(1);
    }
}
