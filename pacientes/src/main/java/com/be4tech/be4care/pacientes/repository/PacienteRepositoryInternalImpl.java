package com.be4tech.be4care.pacientes.repository;

import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

import com.be4tech.be4care.pacientes.domain.Paciente;
import com.be4tech.be4care.pacientes.domain.enumeration.Estratop;
import com.be4tech.be4care.pacientes.domain.enumeration.Grupoetnicop;
import com.be4tech.be4care.pacientes.domain.enumeration.Identificaciont;
import com.be4tech.be4care.pacientes.domain.enumeration.Sexop;
import com.be4tech.be4care.pacientes.repository.rowmapper.CiudadRowMapper;
import com.be4tech.be4care.pacientes.repository.rowmapper.CondicionRowMapper;
import com.be4tech.be4care.pacientes.repository.rowmapper.FarmaceuticaRowMapper;
import com.be4tech.be4care.pacientes.repository.rowmapper.IPSRowMapper;
import com.be4tech.be4care.pacientes.repository.rowmapper.PacienteRowMapper;
import com.be4tech.be4care.pacientes.repository.rowmapper.TratamientoRowMapper;
import com.be4tech.be4care.pacientes.repository.rowmapper.UserRowMapper;
import com.be4tech.be4care.pacientes.service.EntityManager;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoinCondition;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive custom repository implementation for the Paciente entity.
 */
@SuppressWarnings("unused")
class PacienteRepositoryInternalImpl implements PacienteRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final UserRowMapper userMapper;
    private final CiudadRowMapper ciudadMapper;
    private final CondicionRowMapper condicionMapper;
    private final IPSRowMapper ipsMapper;
    private final TratamientoRowMapper tratamientoMapper;
    private final FarmaceuticaRowMapper farmaceuticaMapper;
    private final PacienteRowMapper pacienteMapper;

    private static final Table entityTable = Table.aliased("paciente", EntityManager.ENTITY_ALIAS);
    private static final Table userTable = Table.aliased("jhi_user", "e_user");
    private static final Table ciudadTable = Table.aliased("ciudad", "ciudad");
    private static final Table condicionTable = Table.aliased("condicion", "condicion");
    private static final Table ipsTable = Table.aliased("ips", "ips");
    private static final Table tratamientoTable = Table.aliased("tratamiento", "tratamiento");
    private static final Table farmaceuticaTable = Table.aliased("farmaceutica", "farmaceutica");

    public PacienteRepositoryInternalImpl(
        R2dbcEntityTemplate template,
        EntityManager entityManager,
        UserRowMapper userMapper,
        CiudadRowMapper ciudadMapper,
        CondicionRowMapper condicionMapper,
        IPSRowMapper ipsMapper,
        TratamientoRowMapper tratamientoMapper,
        FarmaceuticaRowMapper farmaceuticaMapper,
        PacienteRowMapper pacienteMapper
    ) {
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.userMapper = userMapper;
        this.ciudadMapper = ciudadMapper;
        this.condicionMapper = condicionMapper;
        this.ipsMapper = ipsMapper;
        this.tratamientoMapper = tratamientoMapper;
        this.farmaceuticaMapper = farmaceuticaMapper;
        this.pacienteMapper = pacienteMapper;
    }

    @Override
    public Flux<Paciente> findAllBy(Pageable pageable) {
        return findAllBy(pageable, null);
    }

    @Override
    public Flux<Paciente> findAllBy(Pageable pageable, Criteria criteria) {
        return createQuery(pageable, criteria).all();
    }

    RowsFetchSpec<Paciente> createQuery(Pageable pageable, Criteria criteria) {
        List<Expression> columns = PacienteSqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        columns.addAll(UserSqlHelper.getColumns(userTable, "user"));
        columns.addAll(CiudadSqlHelper.getColumns(ciudadTable, "ciudad"));
        columns.addAll(CondicionSqlHelper.getColumns(condicionTable, "condicion"));
        columns.addAll(IPSSqlHelper.getColumns(ipsTable, "ips"));
        columns.addAll(TratamientoSqlHelper.getColumns(tratamientoTable, "tratamiento"));
        columns.addAll(FarmaceuticaSqlHelper.getColumns(farmaceuticaTable, "farmaceutica"));
        SelectFromAndJoinCondition selectFrom = Select
            .builder()
            .select(columns)
            .from(entityTable)
            .leftOuterJoin(userTable)
            .on(Column.create("user_id", entityTable))
            .equals(Column.create("id", userTable))
            .leftOuterJoin(ciudadTable)
            .on(Column.create("ciudad_id", entityTable))
            .equals(Column.create("id", ciudadTable))
            .leftOuterJoin(condicionTable)
            .on(Column.create("condicion_id", entityTable))
            .equals(Column.create("id", condicionTable))
            .leftOuterJoin(ipsTable)
            .on(Column.create("ips_id", entityTable))
            .equals(Column.create("id", ipsTable))
            .leftOuterJoin(tratamientoTable)
            .on(Column.create("tratamiento_id", entityTable))
            .equals(Column.create("id", tratamientoTable))
            .leftOuterJoin(farmaceuticaTable)
            .on(Column.create("farmaceutica_id", entityTable))
            .equals(Column.create("id", farmaceuticaTable));

        String select = entityManager.createSelect(selectFrom, Paciente.class, pageable, criteria);
        String alias = entityTable.getReferenceName().getReference();
        String selectWhere = Optional
            .ofNullable(criteria)
            .map(crit ->
                new StringBuilder(select)
                    .append(" ")
                    .append("WHERE")
                    .append(" ")
                    .append(alias)
                    .append(".")
                    .append(crit.toString())
                    .toString()
            )
            .orElse(select); // TODO remove once https://github.com/spring-projects/spring-data-jdbc/issues/907 will be fixed
        return db.sql(selectWhere).map(this::process);
    }

    @Override
    public Flux<Paciente> findAll() {
        return findAllBy(null, null);
    }

    @Override
    public Mono<Paciente> findById(Long id) {
        return createQuery(null, where("id").is(id)).one();
    }

    private Paciente process(Row row, RowMetadata metadata) {
        Paciente entity = pacienteMapper.apply(row, "e");
        entity.setUser(userMapper.apply(row, "user"));
        entity.setCiudad(ciudadMapper.apply(row, "ciudad"));
        entity.setCondicion(condicionMapper.apply(row, "condicion"));
        entity.setIps(ipsMapper.apply(row, "ips"));
        entity.setTratamiento(tratamientoMapper.apply(row, "tratamiento"));
        entity.setFarmaceutica(farmaceuticaMapper.apply(row, "farmaceutica"));
        return entity;
    }

    @Override
    public <S extends Paciente> Mono<S> insert(S entity) {
        return entityManager.insert(entity);
    }

    @Override
    public <S extends Paciente> Mono<S> save(S entity) {
        if (entity.getId() == null) {
            return insert(entity);
        } else {
            return update(entity)
                .map(numberOfUpdates -> {
                    if (numberOfUpdates.intValue() <= 0) {
                        throw new IllegalStateException("Unable to update Paciente with id = " + entity.getId());
                    }
                    return entity;
                });
        }
    }

    @Override
    public Mono<Integer> update(Paciente entity) {
        //fixme is this the proper way?
        return r2dbcEntityTemplate.update(entity).thenReturn(1);
    }
}
