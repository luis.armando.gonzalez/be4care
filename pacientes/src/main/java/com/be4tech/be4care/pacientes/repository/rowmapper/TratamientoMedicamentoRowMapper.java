package com.be4tech.be4care.pacientes.repository.rowmapper;

import com.be4tech.be4care.pacientes.domain.TratamientoMedicamento;
import com.be4tech.be4care.pacientes.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link TratamientoMedicamento}, with proper type conversions.
 */
@Service
public class TratamientoMedicamentoRowMapper implements BiFunction<Row, String, TratamientoMedicamento> {

    private final ColumnConverter converter;

    public TratamientoMedicamentoRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link TratamientoMedicamento} stored in the database.
     */
    @Override
    public TratamientoMedicamento apply(Row row, String prefix) {
        TratamientoMedicamento entity = new TratamientoMedicamento();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setDosis(converter.fromRow(row, prefix + "_dosis", String.class));
        entity.setIntensidad(converter.fromRow(row, prefix + "_intensidad", String.class));
        entity.setTratamientoId(converter.fromRow(row, prefix + "_tratamiento_id", Long.class));
        entity.setMedicamentoId(converter.fromRow(row, prefix + "_medicamento_id", Long.class));
        return entity;
    }
}
