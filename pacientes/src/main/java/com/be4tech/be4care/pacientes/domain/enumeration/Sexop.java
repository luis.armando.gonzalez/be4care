package com.be4tech.be4care.pacientes.domain.enumeration;

/**
 * The Sexop enumeration.
 */
public enum Sexop {
    FEMENINO,
    MASCULINO,
    OTRO,
}
