package com.be4tech.be4care.pacientes.repository.rowmapper;

import com.be4tech.be4care.pacientes.domain.Alarma;
import com.be4tech.be4care.pacientes.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Alarma}, with proper type conversions.
 */
@Service
public class AlarmaRowMapper implements BiFunction<Row, String, Alarma> {

    private final ColumnConverter converter;

    public AlarmaRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Alarma} stored in the database.
     */
    @Override
    public Alarma apply(Row row, String prefix) {
        Alarma entity = new Alarma();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setTimeInstant(converter.fromRow(row, prefix + "_time_instant", Instant.class));
        entity.setDescripcion(converter.fromRow(row, prefix + "_descripcion", String.class));
        entity.setProcedimiento(converter.fromRow(row, prefix + "_procedimiento", String.class));
        entity.setTitulo(converter.fromRow(row, prefix + "_titulo", String.class));
        entity.setVerificar(converter.fromRow(row, prefix + "_verificar", Boolean.class));
        entity.setObservaciones(converter.fromRow(row, prefix + "_observaciones", String.class));
        entity.setPrioridad(converter.fromRow(row, prefix + "_prioridad", String.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        return entity;
    }
}
