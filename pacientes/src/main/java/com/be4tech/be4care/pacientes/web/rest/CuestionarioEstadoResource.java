package com.be4tech.be4care.pacientes.web.rest;

import com.be4tech.be4care.pacientes.domain.CuestionarioEstado;
import com.be4tech.be4care.pacientes.repository.CuestionarioEstadoRepository;
import com.be4tech.be4care.pacientes.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.pacientes.domain.CuestionarioEstado}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CuestionarioEstadoResource {

    private final Logger log = LoggerFactory.getLogger(CuestionarioEstadoResource.class);

    private static final String ENTITY_NAME = "pacientesCuestionarioEstado";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CuestionarioEstadoRepository cuestionarioEstadoRepository;

    public CuestionarioEstadoResource(CuestionarioEstadoRepository cuestionarioEstadoRepository) {
        this.cuestionarioEstadoRepository = cuestionarioEstadoRepository;
    }

    /**
     * {@code POST  /cuestionario-estados} : Create a new cuestionarioEstado.
     *
     * @param cuestionarioEstado the cuestionarioEstado to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cuestionarioEstado, or with status {@code 400 (Bad Request)} if the cuestionarioEstado has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cuestionario-estados")
    public Mono<ResponseEntity<CuestionarioEstado>> createCuestionarioEstado(@RequestBody CuestionarioEstado cuestionarioEstado)
        throws URISyntaxException {
        log.debug("REST request to save CuestionarioEstado : {}", cuestionarioEstado);
        if (cuestionarioEstado.getId() != null) {
            throw new BadRequestAlertException("A new cuestionarioEstado cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return cuestionarioEstadoRepository
            .save(cuestionarioEstado)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/cuestionario-estados/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /cuestionario-estados/:id} : Updates an existing cuestionarioEstado.
     *
     * @param id the id of the cuestionarioEstado to save.
     * @param cuestionarioEstado the cuestionarioEstado to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cuestionarioEstado,
     * or with status {@code 400 (Bad Request)} if the cuestionarioEstado is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cuestionarioEstado couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cuestionario-estados/{id}")
    public Mono<ResponseEntity<CuestionarioEstado>> updateCuestionarioEstado(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CuestionarioEstado cuestionarioEstado
    ) throws URISyntaxException {
        log.debug("REST request to update CuestionarioEstado : {}, {}", id, cuestionarioEstado);
        if (cuestionarioEstado.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, cuestionarioEstado.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return cuestionarioEstadoRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                return cuestionarioEstadoRepository
                    .save(cuestionarioEstado)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /cuestionario-estados/:id} : Partial updates given fields of an existing cuestionarioEstado, field will ignore if it is null
     *
     * @param id the id of the cuestionarioEstado to save.
     * @param cuestionarioEstado the cuestionarioEstado to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cuestionarioEstado,
     * or with status {@code 400 (Bad Request)} if the cuestionarioEstado is not valid,
     * or with status {@code 404 (Not Found)} if the cuestionarioEstado is not found,
     * or with status {@code 500 (Internal Server Error)} if the cuestionarioEstado couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/cuestionario-estados/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<CuestionarioEstado>> partialUpdateCuestionarioEstado(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CuestionarioEstado cuestionarioEstado
    ) throws URISyntaxException {
        log.debug("REST request to partial update CuestionarioEstado partially : {}, {}", id, cuestionarioEstado);
        if (cuestionarioEstado.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, cuestionarioEstado.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return cuestionarioEstadoRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                Mono<CuestionarioEstado> result = cuestionarioEstadoRepository
                    .findById(cuestionarioEstado.getId())
                    .map(existingCuestionarioEstado -> {
                        if (cuestionarioEstado.getValor() != null) {
                            existingCuestionarioEstado.setValor(cuestionarioEstado.getValor());
                        }
                        if (cuestionarioEstado.getValoracion() != null) {
                            existingCuestionarioEstado.setValoracion(cuestionarioEstado.getValoracion());
                        }

                        return existingCuestionarioEstado;
                    })
                    .flatMap(cuestionarioEstadoRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /cuestionario-estados} : get all the cuestionarioEstados.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cuestionarioEstados in body.
     */
    @GetMapping("/cuestionario-estados")
    public Mono<ResponseEntity<List<CuestionarioEstado>>> getAllCuestionarioEstados(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of CuestionarioEstados");
        return cuestionarioEstadoRepository
            .count()
            .zipWith(cuestionarioEstadoRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /cuestionario-estados/:id} : get the "id" cuestionarioEstado.
     *
     * @param id the id of the cuestionarioEstado to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cuestionarioEstado, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cuestionario-estados/{id}")
    public Mono<ResponseEntity<CuestionarioEstado>> getCuestionarioEstado(@PathVariable Long id) {
        log.debug("REST request to get CuestionarioEstado : {}", id);
        Mono<CuestionarioEstado> cuestionarioEstado = cuestionarioEstadoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(cuestionarioEstado);
    }

    /**
     * {@code DELETE  /cuestionario-estados/:id} : delete the "id" cuestionarioEstado.
     *
     * @param id the id of the cuestionarioEstado to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cuestionario-estados/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteCuestionarioEstado(@PathVariable Long id) {
        log.debug("REST request to delete CuestionarioEstado : {}", id);
        return cuestionarioEstadoRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
