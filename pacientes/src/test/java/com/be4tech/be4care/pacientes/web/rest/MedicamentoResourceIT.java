package com.be4tech.be4care.pacientes.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.pacientes.IntegrationTest;
import com.be4tech.be4care.pacientes.domain.Medicamento;
import com.be4tech.be4care.pacientes.domain.enumeration.Presentacion;
import com.be4tech.be4care.pacientes.repository.MedicamentoRepository;
import com.be4tech.be4care.pacientes.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link MedicamentoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class MedicamentoResourceIT {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPCION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPCION = "BBBBBBBBBB";

    private static final Instant DEFAULT_FECHA_INGRESO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_INGRESO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Presentacion DEFAULT_PRESENTACION = Presentacion.Comprimido;
    private static final Presentacion UPDATED_PRESENTACION = Presentacion.Gragea;

    private static final String DEFAULT_GENERICO = "AAAAAAAAAA";
    private static final String UPDATED_GENERICO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/medicamentos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MedicamentoRepository medicamentoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Medicamento medicamento;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Medicamento createEntity(EntityManager em) {
        Medicamento medicamento = new Medicamento()
            .nombre(DEFAULT_NOMBRE)
            .descripcion(DEFAULT_DESCRIPCION)
            .fechaIngreso(DEFAULT_FECHA_INGRESO)
            .presentacion(DEFAULT_PRESENTACION)
            .generico(DEFAULT_GENERICO);
        return medicamento;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Medicamento createUpdatedEntity(EntityManager em) {
        Medicamento medicamento = new Medicamento()
            .nombre(UPDATED_NOMBRE)
            .descripcion(UPDATED_DESCRIPCION)
            .fechaIngreso(UPDATED_FECHA_INGRESO)
            .presentacion(UPDATED_PRESENTACION)
            .generico(UPDATED_GENERICO);
        return medicamento;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Medicamento.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        medicamento = createEntity(em);
    }

    @Test
    void createMedicamento() throws Exception {
        int databaseSizeBeforeCreate = medicamentoRepository.findAll().collectList().block().size();
        // Create the Medicamento
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(medicamento))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Medicamento in the database
        List<Medicamento> medicamentoList = medicamentoRepository.findAll().collectList().block();
        assertThat(medicamentoList).hasSize(databaseSizeBeforeCreate + 1);
        Medicamento testMedicamento = medicamentoList.get(medicamentoList.size() - 1);
        assertThat(testMedicamento.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testMedicamento.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
        assertThat(testMedicamento.getFechaIngreso()).isEqualTo(DEFAULT_FECHA_INGRESO);
        assertThat(testMedicamento.getPresentacion()).isEqualTo(DEFAULT_PRESENTACION);
        assertThat(testMedicamento.getGenerico()).isEqualTo(DEFAULT_GENERICO);
    }

    @Test
    void createMedicamentoWithExistingId() throws Exception {
        // Create the Medicamento with an existing ID
        medicamento.setId(1L);

        int databaseSizeBeforeCreate = medicamentoRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(medicamento))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Medicamento in the database
        List<Medicamento> medicamentoList = medicamentoRepository.findAll().collectList().block();
        assertThat(medicamentoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllMedicamentos() {
        // Initialize the database
        medicamentoRepository.save(medicamento).block();

        // Get all the medicamentoList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(medicamento.getId().intValue()))
            .jsonPath("$.[*].nombre")
            .value(hasItem(DEFAULT_NOMBRE))
            .jsonPath("$.[*].descripcion")
            .value(hasItem(DEFAULT_DESCRIPCION.toString()))
            .jsonPath("$.[*].fechaIngreso")
            .value(hasItem(DEFAULT_FECHA_INGRESO.toString()))
            .jsonPath("$.[*].presentacion")
            .value(hasItem(DEFAULT_PRESENTACION.toString()))
            .jsonPath("$.[*].generico")
            .value(hasItem(DEFAULT_GENERICO.toString()));
    }

    @Test
    void getMedicamento() {
        // Initialize the database
        medicamentoRepository.save(medicamento).block();

        // Get the medicamento
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, medicamento.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(medicamento.getId().intValue()))
            .jsonPath("$.nombre")
            .value(is(DEFAULT_NOMBRE))
            .jsonPath("$.descripcion")
            .value(is(DEFAULT_DESCRIPCION.toString()))
            .jsonPath("$.fechaIngreso")
            .value(is(DEFAULT_FECHA_INGRESO.toString()))
            .jsonPath("$.presentacion")
            .value(is(DEFAULT_PRESENTACION.toString()))
            .jsonPath("$.generico")
            .value(is(DEFAULT_GENERICO.toString()));
    }

    @Test
    void getNonExistingMedicamento() {
        // Get the medicamento
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewMedicamento() throws Exception {
        // Initialize the database
        medicamentoRepository.save(medicamento).block();

        int databaseSizeBeforeUpdate = medicamentoRepository.findAll().collectList().block().size();

        // Update the medicamento
        Medicamento updatedMedicamento = medicamentoRepository.findById(medicamento.getId()).block();
        updatedMedicamento
            .nombre(UPDATED_NOMBRE)
            .descripcion(UPDATED_DESCRIPCION)
            .fechaIngreso(UPDATED_FECHA_INGRESO)
            .presentacion(UPDATED_PRESENTACION)
            .generico(UPDATED_GENERICO);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedMedicamento.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedMedicamento))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Medicamento in the database
        List<Medicamento> medicamentoList = medicamentoRepository.findAll().collectList().block();
        assertThat(medicamentoList).hasSize(databaseSizeBeforeUpdate);
        Medicamento testMedicamento = medicamentoList.get(medicamentoList.size() - 1);
        assertThat(testMedicamento.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testMedicamento.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testMedicamento.getFechaIngreso()).isEqualTo(UPDATED_FECHA_INGRESO);
        assertThat(testMedicamento.getPresentacion()).isEqualTo(UPDATED_PRESENTACION);
        assertThat(testMedicamento.getGenerico()).isEqualTo(UPDATED_GENERICO);
    }

    @Test
    void putNonExistingMedicamento() throws Exception {
        int databaseSizeBeforeUpdate = medicamentoRepository.findAll().collectList().block().size();
        medicamento.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, medicamento.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(medicamento))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Medicamento in the database
        List<Medicamento> medicamentoList = medicamentoRepository.findAll().collectList().block();
        assertThat(medicamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchMedicamento() throws Exception {
        int databaseSizeBeforeUpdate = medicamentoRepository.findAll().collectList().block().size();
        medicamento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(medicamento))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Medicamento in the database
        List<Medicamento> medicamentoList = medicamentoRepository.findAll().collectList().block();
        assertThat(medicamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamMedicamento() throws Exception {
        int databaseSizeBeforeUpdate = medicamentoRepository.findAll().collectList().block().size();
        medicamento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(medicamento))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Medicamento in the database
        List<Medicamento> medicamentoList = medicamentoRepository.findAll().collectList().block();
        assertThat(medicamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateMedicamentoWithPatch() throws Exception {
        // Initialize the database
        medicamentoRepository.save(medicamento).block();

        int databaseSizeBeforeUpdate = medicamentoRepository.findAll().collectList().block().size();

        // Update the medicamento using partial update
        Medicamento partialUpdatedMedicamento = new Medicamento();
        partialUpdatedMedicamento.setId(medicamento.getId());

        partialUpdatedMedicamento
            .nombre(UPDATED_NOMBRE)
            .descripcion(UPDATED_DESCRIPCION)
            .fechaIngreso(UPDATED_FECHA_INGRESO)
            .presentacion(UPDATED_PRESENTACION)
            .generico(UPDATED_GENERICO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedMedicamento.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedMedicamento))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Medicamento in the database
        List<Medicamento> medicamentoList = medicamentoRepository.findAll().collectList().block();
        assertThat(medicamentoList).hasSize(databaseSizeBeforeUpdate);
        Medicamento testMedicamento = medicamentoList.get(medicamentoList.size() - 1);
        assertThat(testMedicamento.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testMedicamento.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testMedicamento.getFechaIngreso()).isEqualTo(UPDATED_FECHA_INGRESO);
        assertThat(testMedicamento.getPresentacion()).isEqualTo(UPDATED_PRESENTACION);
        assertThat(testMedicamento.getGenerico()).isEqualTo(UPDATED_GENERICO);
    }

    @Test
    void fullUpdateMedicamentoWithPatch() throws Exception {
        // Initialize the database
        medicamentoRepository.save(medicamento).block();

        int databaseSizeBeforeUpdate = medicamentoRepository.findAll().collectList().block().size();

        // Update the medicamento using partial update
        Medicamento partialUpdatedMedicamento = new Medicamento();
        partialUpdatedMedicamento.setId(medicamento.getId());

        partialUpdatedMedicamento
            .nombre(UPDATED_NOMBRE)
            .descripcion(UPDATED_DESCRIPCION)
            .fechaIngreso(UPDATED_FECHA_INGRESO)
            .presentacion(UPDATED_PRESENTACION)
            .generico(UPDATED_GENERICO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedMedicamento.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedMedicamento))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Medicamento in the database
        List<Medicamento> medicamentoList = medicamentoRepository.findAll().collectList().block();
        assertThat(medicamentoList).hasSize(databaseSizeBeforeUpdate);
        Medicamento testMedicamento = medicamentoList.get(medicamentoList.size() - 1);
        assertThat(testMedicamento.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testMedicamento.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testMedicamento.getFechaIngreso()).isEqualTo(UPDATED_FECHA_INGRESO);
        assertThat(testMedicamento.getPresentacion()).isEqualTo(UPDATED_PRESENTACION);
        assertThat(testMedicamento.getGenerico()).isEqualTo(UPDATED_GENERICO);
    }

    @Test
    void patchNonExistingMedicamento() throws Exception {
        int databaseSizeBeforeUpdate = medicamentoRepository.findAll().collectList().block().size();
        medicamento.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, medicamento.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(medicamento))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Medicamento in the database
        List<Medicamento> medicamentoList = medicamentoRepository.findAll().collectList().block();
        assertThat(medicamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchMedicamento() throws Exception {
        int databaseSizeBeforeUpdate = medicamentoRepository.findAll().collectList().block().size();
        medicamento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(medicamento))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Medicamento in the database
        List<Medicamento> medicamentoList = medicamentoRepository.findAll().collectList().block();
        assertThat(medicamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamMedicamento() throws Exception {
        int databaseSizeBeforeUpdate = medicamentoRepository.findAll().collectList().block().size();
        medicamento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(medicamento))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Medicamento in the database
        List<Medicamento> medicamentoList = medicamentoRepository.findAll().collectList().block();
        assertThat(medicamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteMedicamento() {
        // Initialize the database
        medicamentoRepository.save(medicamento).block();

        int databaseSizeBeforeDelete = medicamentoRepository.findAll().collectList().block().size();

        // Delete the medicamento
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, medicamento.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Medicamento> medicamentoList = medicamentoRepository.findAll().collectList().block();
        assertThat(medicamentoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
