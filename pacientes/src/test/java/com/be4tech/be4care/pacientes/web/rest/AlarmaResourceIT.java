package com.be4tech.be4care.pacientes.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.pacientes.IntegrationTest;
import com.be4tech.be4care.pacientes.domain.Alarma;
import com.be4tech.be4care.pacientes.repository.AlarmaRepository;
import com.be4tech.be4care.pacientes.repository.UserRepository;
import com.be4tech.be4care.pacientes.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link AlarmaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class AlarmaResourceIT {

    private static final Instant DEFAULT_TIME_INSTANT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TIME_INSTANT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_DESCRIPCION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPCION = "BBBBBBBBBB";

    private static final String DEFAULT_PROCEDIMIENTO = "AAAAAAAAAA";
    private static final String UPDATED_PROCEDIMIENTO = "BBBBBBBBBB";

    private static final String DEFAULT_TITULO = "AAAAAAAAAA";
    private static final String UPDATED_TITULO = "BBBBBBBBBB";

    private static final Boolean DEFAULT_VERIFICAR = false;
    private static final Boolean UPDATED_VERIFICAR = true;

    private static final String DEFAULT_OBSERVACIONES = "AAAAAAAAAA";
    private static final String UPDATED_OBSERVACIONES = "BBBBBBBBBB";

    private static final String DEFAULT_PRIORIDAD = "AAAAAAAAAA";
    private static final String UPDATED_PRIORIDAD = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/alarmas";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private AlarmaRepository alarmaRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Alarma alarma;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Alarma createEntity(EntityManager em) {
        Alarma alarma = new Alarma()
            .timeInstant(DEFAULT_TIME_INSTANT)
            .descripcion(DEFAULT_DESCRIPCION)
            .procedimiento(DEFAULT_PROCEDIMIENTO)
            .titulo(DEFAULT_TITULO)
            .verificar(DEFAULT_VERIFICAR)
            .observaciones(DEFAULT_OBSERVACIONES)
            .prioridad(DEFAULT_PRIORIDAD);
        return alarma;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Alarma createUpdatedEntity(EntityManager em) {
        Alarma alarma = new Alarma()
            .timeInstant(UPDATED_TIME_INSTANT)
            .descripcion(UPDATED_DESCRIPCION)
            .procedimiento(UPDATED_PROCEDIMIENTO)
            .titulo(UPDATED_TITULO)
            .verificar(UPDATED_VERIFICAR)
            .observaciones(UPDATED_OBSERVACIONES)
            .prioridad(UPDATED_PRIORIDAD);
        return alarma;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Alarma.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        alarma = createEntity(em);
    }

    @Test
    void createAlarma() throws Exception {
        int databaseSizeBeforeCreate = alarmaRepository.findAll().collectList().block().size();
        // Create the Alarma
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(alarma))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Alarma in the database
        List<Alarma> alarmaList = alarmaRepository.findAll().collectList().block();
        assertThat(alarmaList).hasSize(databaseSizeBeforeCreate + 1);
        Alarma testAlarma = alarmaList.get(alarmaList.size() - 1);
        assertThat(testAlarma.getTimeInstant()).isEqualTo(DEFAULT_TIME_INSTANT);
        assertThat(testAlarma.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
        assertThat(testAlarma.getProcedimiento()).isEqualTo(DEFAULT_PROCEDIMIENTO);
        assertThat(testAlarma.getTitulo()).isEqualTo(DEFAULT_TITULO);
        assertThat(testAlarma.getVerificar()).isEqualTo(DEFAULT_VERIFICAR);
        assertThat(testAlarma.getObservaciones()).isEqualTo(DEFAULT_OBSERVACIONES);
        assertThat(testAlarma.getPrioridad()).isEqualTo(DEFAULT_PRIORIDAD);
    }

    @Test
    void createAlarmaWithExistingId() throws Exception {
        // Create the Alarma with an existing ID
        alarma.setId(1L);

        int databaseSizeBeforeCreate = alarmaRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(alarma))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Alarma in the database
        List<Alarma> alarmaList = alarmaRepository.findAll().collectList().block();
        assertThat(alarmaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllAlarmas() {
        // Initialize the database
        alarmaRepository.save(alarma).block();

        // Get all the alarmaList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(alarma.getId().intValue()))
            .jsonPath("$.[*].timeInstant")
            .value(hasItem(DEFAULT_TIME_INSTANT.toString()))
            .jsonPath("$.[*].descripcion")
            .value(hasItem(DEFAULT_DESCRIPCION))
            .jsonPath("$.[*].procedimiento")
            .value(hasItem(DEFAULT_PROCEDIMIENTO))
            .jsonPath("$.[*].titulo")
            .value(hasItem(DEFAULT_TITULO))
            .jsonPath("$.[*].verificar")
            .value(hasItem(DEFAULT_VERIFICAR.booleanValue()))
            .jsonPath("$.[*].observaciones")
            .value(hasItem(DEFAULT_OBSERVACIONES))
            .jsonPath("$.[*].prioridad")
            .value(hasItem(DEFAULT_PRIORIDAD));
    }

    @Test
    void getAlarma() {
        // Initialize the database
        alarmaRepository.save(alarma).block();

        // Get the alarma
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, alarma.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(alarma.getId().intValue()))
            .jsonPath("$.timeInstant")
            .value(is(DEFAULT_TIME_INSTANT.toString()))
            .jsonPath("$.descripcion")
            .value(is(DEFAULT_DESCRIPCION))
            .jsonPath("$.procedimiento")
            .value(is(DEFAULT_PROCEDIMIENTO))
            .jsonPath("$.titulo")
            .value(is(DEFAULT_TITULO))
            .jsonPath("$.verificar")
            .value(is(DEFAULT_VERIFICAR.booleanValue()))
            .jsonPath("$.observaciones")
            .value(is(DEFAULT_OBSERVACIONES))
            .jsonPath("$.prioridad")
            .value(is(DEFAULT_PRIORIDAD));
    }

    @Test
    void getNonExistingAlarma() {
        // Get the alarma
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewAlarma() throws Exception {
        // Initialize the database
        alarmaRepository.save(alarma).block();

        int databaseSizeBeforeUpdate = alarmaRepository.findAll().collectList().block().size();

        // Update the alarma
        Alarma updatedAlarma = alarmaRepository.findById(alarma.getId()).block();
        updatedAlarma
            .timeInstant(UPDATED_TIME_INSTANT)
            .descripcion(UPDATED_DESCRIPCION)
            .procedimiento(UPDATED_PROCEDIMIENTO)
            .titulo(UPDATED_TITULO)
            .verificar(UPDATED_VERIFICAR)
            .observaciones(UPDATED_OBSERVACIONES)
            .prioridad(UPDATED_PRIORIDAD);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedAlarma.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedAlarma))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Alarma in the database
        List<Alarma> alarmaList = alarmaRepository.findAll().collectList().block();
        assertThat(alarmaList).hasSize(databaseSizeBeforeUpdate);
        Alarma testAlarma = alarmaList.get(alarmaList.size() - 1);
        assertThat(testAlarma.getTimeInstant()).isEqualTo(UPDATED_TIME_INSTANT);
        assertThat(testAlarma.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testAlarma.getProcedimiento()).isEqualTo(UPDATED_PROCEDIMIENTO);
        assertThat(testAlarma.getTitulo()).isEqualTo(UPDATED_TITULO);
        assertThat(testAlarma.getVerificar()).isEqualTo(UPDATED_VERIFICAR);
        assertThat(testAlarma.getObservaciones()).isEqualTo(UPDATED_OBSERVACIONES);
        assertThat(testAlarma.getPrioridad()).isEqualTo(UPDATED_PRIORIDAD);
    }

    @Test
    void putNonExistingAlarma() throws Exception {
        int databaseSizeBeforeUpdate = alarmaRepository.findAll().collectList().block().size();
        alarma.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, alarma.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(alarma))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Alarma in the database
        List<Alarma> alarmaList = alarmaRepository.findAll().collectList().block();
        assertThat(alarmaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchAlarma() throws Exception {
        int databaseSizeBeforeUpdate = alarmaRepository.findAll().collectList().block().size();
        alarma.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(alarma))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Alarma in the database
        List<Alarma> alarmaList = alarmaRepository.findAll().collectList().block();
        assertThat(alarmaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamAlarma() throws Exception {
        int databaseSizeBeforeUpdate = alarmaRepository.findAll().collectList().block().size();
        alarma.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(alarma))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Alarma in the database
        List<Alarma> alarmaList = alarmaRepository.findAll().collectList().block();
        assertThat(alarmaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateAlarmaWithPatch() throws Exception {
        // Initialize the database
        alarmaRepository.save(alarma).block();

        int databaseSizeBeforeUpdate = alarmaRepository.findAll().collectList().block().size();

        // Update the alarma using partial update
        Alarma partialUpdatedAlarma = new Alarma();
        partialUpdatedAlarma.setId(alarma.getId());

        partialUpdatedAlarma
            .timeInstant(UPDATED_TIME_INSTANT)
            .descripcion(UPDATED_DESCRIPCION)
            .procedimiento(UPDATED_PROCEDIMIENTO)
            .titulo(UPDATED_TITULO)
            .verificar(UPDATED_VERIFICAR)
            .observaciones(UPDATED_OBSERVACIONES);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedAlarma.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedAlarma))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Alarma in the database
        List<Alarma> alarmaList = alarmaRepository.findAll().collectList().block();
        assertThat(alarmaList).hasSize(databaseSizeBeforeUpdate);
        Alarma testAlarma = alarmaList.get(alarmaList.size() - 1);
        assertThat(testAlarma.getTimeInstant()).isEqualTo(UPDATED_TIME_INSTANT);
        assertThat(testAlarma.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testAlarma.getProcedimiento()).isEqualTo(UPDATED_PROCEDIMIENTO);
        assertThat(testAlarma.getTitulo()).isEqualTo(UPDATED_TITULO);
        assertThat(testAlarma.getVerificar()).isEqualTo(UPDATED_VERIFICAR);
        assertThat(testAlarma.getObservaciones()).isEqualTo(UPDATED_OBSERVACIONES);
        assertThat(testAlarma.getPrioridad()).isEqualTo(DEFAULT_PRIORIDAD);
    }

    @Test
    void fullUpdateAlarmaWithPatch() throws Exception {
        // Initialize the database
        alarmaRepository.save(alarma).block();

        int databaseSizeBeforeUpdate = alarmaRepository.findAll().collectList().block().size();

        // Update the alarma using partial update
        Alarma partialUpdatedAlarma = new Alarma();
        partialUpdatedAlarma.setId(alarma.getId());

        partialUpdatedAlarma
            .timeInstant(UPDATED_TIME_INSTANT)
            .descripcion(UPDATED_DESCRIPCION)
            .procedimiento(UPDATED_PROCEDIMIENTO)
            .titulo(UPDATED_TITULO)
            .verificar(UPDATED_VERIFICAR)
            .observaciones(UPDATED_OBSERVACIONES)
            .prioridad(UPDATED_PRIORIDAD);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedAlarma.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedAlarma))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Alarma in the database
        List<Alarma> alarmaList = alarmaRepository.findAll().collectList().block();
        assertThat(alarmaList).hasSize(databaseSizeBeforeUpdate);
        Alarma testAlarma = alarmaList.get(alarmaList.size() - 1);
        assertThat(testAlarma.getTimeInstant()).isEqualTo(UPDATED_TIME_INSTANT);
        assertThat(testAlarma.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testAlarma.getProcedimiento()).isEqualTo(UPDATED_PROCEDIMIENTO);
        assertThat(testAlarma.getTitulo()).isEqualTo(UPDATED_TITULO);
        assertThat(testAlarma.getVerificar()).isEqualTo(UPDATED_VERIFICAR);
        assertThat(testAlarma.getObservaciones()).isEqualTo(UPDATED_OBSERVACIONES);
        assertThat(testAlarma.getPrioridad()).isEqualTo(UPDATED_PRIORIDAD);
    }

    @Test
    void patchNonExistingAlarma() throws Exception {
        int databaseSizeBeforeUpdate = alarmaRepository.findAll().collectList().block().size();
        alarma.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, alarma.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(alarma))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Alarma in the database
        List<Alarma> alarmaList = alarmaRepository.findAll().collectList().block();
        assertThat(alarmaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchAlarma() throws Exception {
        int databaseSizeBeforeUpdate = alarmaRepository.findAll().collectList().block().size();
        alarma.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(alarma))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Alarma in the database
        List<Alarma> alarmaList = alarmaRepository.findAll().collectList().block();
        assertThat(alarmaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamAlarma() throws Exception {
        int databaseSizeBeforeUpdate = alarmaRepository.findAll().collectList().block().size();
        alarma.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(alarma))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Alarma in the database
        List<Alarma> alarmaList = alarmaRepository.findAll().collectList().block();
        assertThat(alarmaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteAlarma() {
        // Initialize the database
        alarmaRepository.save(alarma).block();

        int databaseSizeBeforeDelete = alarmaRepository.findAll().collectList().block().size();

        // Delete the alarma
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, alarma.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Alarma> alarmaList = alarmaRepository.findAll().collectList().block();
        assertThat(alarmaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
