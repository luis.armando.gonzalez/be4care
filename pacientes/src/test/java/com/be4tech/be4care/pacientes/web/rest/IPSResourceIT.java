package com.be4tech.be4care.pacientes.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.pacientes.IntegrationTest;
import com.be4tech.be4care.pacientes.domain.IPS;
import com.be4tech.be4care.pacientes.repository.IPSRepository;
import com.be4tech.be4care.pacientes.service.EntityManager;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link IPSResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class IPSResourceIT {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    private static final String DEFAULT_NIT = "AAAAAAAAAA";
    private static final String UPDATED_NIT = "BBBBBBBBBB";

    private static final String DEFAULT_DIRECCION = "AAAAAAAAAA";
    private static final String UPDATED_DIRECCION = "BBBBBBBBBB";

    private static final String DEFAULT_TELEFONO = "AAAAAAAAAA";
    private static final String UPDATED_TELEFONO = "BBBBBBBBBB";

    private static final String DEFAULT_CORREO_ELECTRONICO = "AAAAAAAAAA";
    private static final String UPDATED_CORREO_ELECTRONICO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/ips";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private IPSRepository iPSRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private IPS iPS;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IPS createEntity(EntityManager em) {
        IPS iPS = new IPS()
            .nombre(DEFAULT_NOMBRE)
            .nit(DEFAULT_NIT)
            .direccion(DEFAULT_DIRECCION)
            .telefono(DEFAULT_TELEFONO)
            .correoElectronico(DEFAULT_CORREO_ELECTRONICO);
        return iPS;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IPS createUpdatedEntity(EntityManager em) {
        IPS iPS = new IPS()
            .nombre(UPDATED_NOMBRE)
            .nit(UPDATED_NIT)
            .direccion(UPDATED_DIRECCION)
            .telefono(UPDATED_TELEFONO)
            .correoElectronico(UPDATED_CORREO_ELECTRONICO);
        return iPS;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(IPS.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        iPS = createEntity(em);
    }

    @Test
    void createIPS() throws Exception {
        int databaseSizeBeforeCreate = iPSRepository.findAll().collectList().block().size();
        // Create the IPS
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(iPS))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the IPS in the database
        List<IPS> iPSList = iPSRepository.findAll().collectList().block();
        assertThat(iPSList).hasSize(databaseSizeBeforeCreate + 1);
        IPS testIPS = iPSList.get(iPSList.size() - 1);
        assertThat(testIPS.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testIPS.getNit()).isEqualTo(DEFAULT_NIT);
        assertThat(testIPS.getDireccion()).isEqualTo(DEFAULT_DIRECCION);
        assertThat(testIPS.getTelefono()).isEqualTo(DEFAULT_TELEFONO);
        assertThat(testIPS.getCorreoElectronico()).isEqualTo(DEFAULT_CORREO_ELECTRONICO);
    }

    @Test
    void createIPSWithExistingId() throws Exception {
        // Create the IPS with an existing ID
        iPS.setId(1L);

        int databaseSizeBeforeCreate = iPSRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(iPS))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the IPS in the database
        List<IPS> iPSList = iPSRepository.findAll().collectList().block();
        assertThat(iPSList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllIPS() {
        // Initialize the database
        iPSRepository.save(iPS).block();

        // Get all the iPSList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(iPS.getId().intValue()))
            .jsonPath("$.[*].nombre")
            .value(hasItem(DEFAULT_NOMBRE))
            .jsonPath("$.[*].nit")
            .value(hasItem(DEFAULT_NIT))
            .jsonPath("$.[*].direccion")
            .value(hasItem(DEFAULT_DIRECCION))
            .jsonPath("$.[*].telefono")
            .value(hasItem(DEFAULT_TELEFONO))
            .jsonPath("$.[*].correoElectronico")
            .value(hasItem(DEFAULT_CORREO_ELECTRONICO));
    }

    @Test
    void getIPS() {
        // Initialize the database
        iPSRepository.save(iPS).block();

        // Get the iPS
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, iPS.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(iPS.getId().intValue()))
            .jsonPath("$.nombre")
            .value(is(DEFAULT_NOMBRE))
            .jsonPath("$.nit")
            .value(is(DEFAULT_NIT))
            .jsonPath("$.direccion")
            .value(is(DEFAULT_DIRECCION))
            .jsonPath("$.telefono")
            .value(is(DEFAULT_TELEFONO))
            .jsonPath("$.correoElectronico")
            .value(is(DEFAULT_CORREO_ELECTRONICO));
    }

    @Test
    void getNonExistingIPS() {
        // Get the iPS
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewIPS() throws Exception {
        // Initialize the database
        iPSRepository.save(iPS).block();

        int databaseSizeBeforeUpdate = iPSRepository.findAll().collectList().block().size();

        // Update the iPS
        IPS updatedIPS = iPSRepository.findById(iPS.getId()).block();
        updatedIPS
            .nombre(UPDATED_NOMBRE)
            .nit(UPDATED_NIT)
            .direccion(UPDATED_DIRECCION)
            .telefono(UPDATED_TELEFONO)
            .correoElectronico(UPDATED_CORREO_ELECTRONICO);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedIPS.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedIPS))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the IPS in the database
        List<IPS> iPSList = iPSRepository.findAll().collectList().block();
        assertThat(iPSList).hasSize(databaseSizeBeforeUpdate);
        IPS testIPS = iPSList.get(iPSList.size() - 1);
        assertThat(testIPS.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testIPS.getNit()).isEqualTo(UPDATED_NIT);
        assertThat(testIPS.getDireccion()).isEqualTo(UPDATED_DIRECCION);
        assertThat(testIPS.getTelefono()).isEqualTo(UPDATED_TELEFONO);
        assertThat(testIPS.getCorreoElectronico()).isEqualTo(UPDATED_CORREO_ELECTRONICO);
    }

    @Test
    void putNonExistingIPS() throws Exception {
        int databaseSizeBeforeUpdate = iPSRepository.findAll().collectList().block().size();
        iPS.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, iPS.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(iPS))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the IPS in the database
        List<IPS> iPSList = iPSRepository.findAll().collectList().block();
        assertThat(iPSList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchIPS() throws Exception {
        int databaseSizeBeforeUpdate = iPSRepository.findAll().collectList().block().size();
        iPS.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(iPS))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the IPS in the database
        List<IPS> iPSList = iPSRepository.findAll().collectList().block();
        assertThat(iPSList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamIPS() throws Exception {
        int databaseSizeBeforeUpdate = iPSRepository.findAll().collectList().block().size();
        iPS.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(iPS))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the IPS in the database
        List<IPS> iPSList = iPSRepository.findAll().collectList().block();
        assertThat(iPSList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateIPSWithPatch() throws Exception {
        // Initialize the database
        iPSRepository.save(iPS).block();

        int databaseSizeBeforeUpdate = iPSRepository.findAll().collectList().block().size();

        // Update the iPS using partial update
        IPS partialUpdatedIPS = new IPS();
        partialUpdatedIPS.setId(iPS.getId());

        partialUpdatedIPS.nombre(UPDATED_NOMBRE).correoElectronico(UPDATED_CORREO_ELECTRONICO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedIPS.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedIPS))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the IPS in the database
        List<IPS> iPSList = iPSRepository.findAll().collectList().block();
        assertThat(iPSList).hasSize(databaseSizeBeforeUpdate);
        IPS testIPS = iPSList.get(iPSList.size() - 1);
        assertThat(testIPS.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testIPS.getNit()).isEqualTo(DEFAULT_NIT);
        assertThat(testIPS.getDireccion()).isEqualTo(DEFAULT_DIRECCION);
        assertThat(testIPS.getTelefono()).isEqualTo(DEFAULT_TELEFONO);
        assertThat(testIPS.getCorreoElectronico()).isEqualTo(UPDATED_CORREO_ELECTRONICO);
    }

    @Test
    void fullUpdateIPSWithPatch() throws Exception {
        // Initialize the database
        iPSRepository.save(iPS).block();

        int databaseSizeBeforeUpdate = iPSRepository.findAll().collectList().block().size();

        // Update the iPS using partial update
        IPS partialUpdatedIPS = new IPS();
        partialUpdatedIPS.setId(iPS.getId());

        partialUpdatedIPS
            .nombre(UPDATED_NOMBRE)
            .nit(UPDATED_NIT)
            .direccion(UPDATED_DIRECCION)
            .telefono(UPDATED_TELEFONO)
            .correoElectronico(UPDATED_CORREO_ELECTRONICO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedIPS.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedIPS))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the IPS in the database
        List<IPS> iPSList = iPSRepository.findAll().collectList().block();
        assertThat(iPSList).hasSize(databaseSizeBeforeUpdate);
        IPS testIPS = iPSList.get(iPSList.size() - 1);
        assertThat(testIPS.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testIPS.getNit()).isEqualTo(UPDATED_NIT);
        assertThat(testIPS.getDireccion()).isEqualTo(UPDATED_DIRECCION);
        assertThat(testIPS.getTelefono()).isEqualTo(UPDATED_TELEFONO);
        assertThat(testIPS.getCorreoElectronico()).isEqualTo(UPDATED_CORREO_ELECTRONICO);
    }

    @Test
    void patchNonExistingIPS() throws Exception {
        int databaseSizeBeforeUpdate = iPSRepository.findAll().collectList().block().size();
        iPS.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, iPS.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(iPS))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the IPS in the database
        List<IPS> iPSList = iPSRepository.findAll().collectList().block();
        assertThat(iPSList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchIPS() throws Exception {
        int databaseSizeBeforeUpdate = iPSRepository.findAll().collectList().block().size();
        iPS.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(iPS))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the IPS in the database
        List<IPS> iPSList = iPSRepository.findAll().collectList().block();
        assertThat(iPSList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamIPS() throws Exception {
        int databaseSizeBeforeUpdate = iPSRepository.findAll().collectList().block().size();
        iPS.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(iPS))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the IPS in the database
        List<IPS> iPSList = iPSRepository.findAll().collectList().block();
        assertThat(iPSList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteIPS() {
        // Initialize the database
        iPSRepository.save(iPS).block();

        int databaseSizeBeforeDelete = iPSRepository.findAll().collectList().block().size();

        // Delete the iPS
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, iPS.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<IPS> iPSList = iPSRepository.findAll().collectList().block();
        assertThat(iPSList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
