package com.be4tech.be4care.pacientes.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.pacientes.IntegrationTest;
import com.be4tech.be4care.pacientes.domain.Paciente;
import com.be4tech.be4care.pacientes.domain.enumeration.Estratop;
import com.be4tech.be4care.pacientes.domain.enumeration.Grupoetnicop;
import com.be4tech.be4care.pacientes.domain.enumeration.Identificaciont;
import com.be4tech.be4care.pacientes.domain.enumeration.Sexop;
import com.be4tech.be4care.pacientes.repository.PacienteRepository;
import com.be4tech.be4care.pacientes.repository.UserRepository;
import com.be4tech.be4care.pacientes.service.EntityManager;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link PacienteResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class PacienteResourceIT {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    private static final Identificaciont DEFAULT_TIPO_IDENTIFICACION = Identificaciont.CEDULA;
    private static final Identificaciont UPDATED_TIPO_IDENTIFICACION = Identificaciont.TARJETA;

    private static final Integer DEFAULT_IDENTIFICACION = 1;
    private static final Integer UPDATED_IDENTIFICACION = 2;

    private static final Integer DEFAULT_EDAD = 1;
    private static final Integer UPDATED_EDAD = 2;

    private static final Sexop DEFAULT_SEXO = Sexop.FEMENINO;
    private static final Sexop UPDATED_SEXO = Sexop.MASCULINO;

    private static final Integer DEFAULT_TELEFONO = 1;
    private static final Integer UPDATED_TELEFONO = 2;

    private static final String DEFAULT_DIRECCION = "AAAAAAAAAA";
    private static final String UPDATED_DIRECCION = "BBBBBBBBBB";

    private static final Float DEFAULT_PESO_KG = 1F;
    private static final Float UPDATED_PESO_KG = 2F;

    private static final Integer DEFAULT_ESTATURA_CM = 1;
    private static final Integer UPDATED_ESTATURA_CM = 2;

    private static final Grupoetnicop DEFAULT_GRUPO_ETNICO = Grupoetnicop.Afrocolombiano;
    private static final Grupoetnicop UPDATED_GRUPO_ETNICO = Grupoetnicop.Palenquero;

    private static final Estratop DEFAULT_ESTRATO_SOCIOECONOMICO = Estratop.Estrato_1;
    private static final Estratop UPDATED_ESTRATO_SOCIOECONOMICO = Estratop.Estrato_2;

    private static final Integer DEFAULT_OXIMETRIA_REFERENCIA = 1;
    private static final Integer UPDATED_OXIMETRIA_REFERENCIA = 2;

    private static final Integer DEFAULT_RITMO_CARDIACO_REFERENCIA = 1;
    private static final Integer UPDATED_RITMO_CARDIACO_REFERENCIA = 2;

    private static final Integer DEFAULT_PRESION_SISTOLICA_REFERENCIA = 1;
    private static final Integer UPDATED_PRESION_SISTOLICA_REFERENCIA = 2;

    private static final Integer DEFAULT_PRESION_DISTOLICA_REFERENCIA = 1;
    private static final Integer UPDATED_PRESION_DISTOLICA_REFERENCIA = 2;

    private static final String DEFAULT_COMENTARIOS = "AAAAAAAAAA";
    private static final String UPDATED_COMENTARIOS = "BBBBBBBBBB";

    private static final Integer DEFAULT_PASOS_REFERENCIA = 1;
    private static final Integer UPDATED_PASOS_REFERENCIA = 2;

    private static final Integer DEFAULT_CALORIAS_REFERENCIA = 1;
    private static final Integer UPDATED_CALORIAS_REFERENCIA = 2;

    private static final String DEFAULT_META_REFERENCIA = "AAAAAAAAAA";
    private static final String UPDATED_META_REFERENCIA = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/pacientes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PacienteRepository pacienteRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Paciente paciente;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Paciente createEntity(EntityManager em) {
        Paciente paciente = new Paciente()
            .nombre(DEFAULT_NOMBRE)
            .tipoIdentificacion(DEFAULT_TIPO_IDENTIFICACION)
            .identificacion(DEFAULT_IDENTIFICACION)
            .edad(DEFAULT_EDAD)
            .sexo(DEFAULT_SEXO)
            .telefono(DEFAULT_TELEFONO)
            .direccion(DEFAULT_DIRECCION)
            .pesoKG(DEFAULT_PESO_KG)
            .estaturaCM(DEFAULT_ESTATURA_CM)
            .grupoEtnico(DEFAULT_GRUPO_ETNICO)
            .estratoSocioeconomico(DEFAULT_ESTRATO_SOCIOECONOMICO)
            .oximetriaReferencia(DEFAULT_OXIMETRIA_REFERENCIA)
            .ritmoCardiacoReferencia(DEFAULT_RITMO_CARDIACO_REFERENCIA)
            .presionSistolicaReferencia(DEFAULT_PRESION_SISTOLICA_REFERENCIA)
            .presionDistolicaReferencia(DEFAULT_PRESION_DISTOLICA_REFERENCIA)
            .comentarios(DEFAULT_COMENTARIOS)
            .pasosReferencia(DEFAULT_PASOS_REFERENCIA)
            .caloriasReferencia(DEFAULT_CALORIAS_REFERENCIA)
            .metaReferencia(DEFAULT_META_REFERENCIA);
        return paciente;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Paciente createUpdatedEntity(EntityManager em) {
        Paciente paciente = new Paciente()
            .nombre(UPDATED_NOMBRE)
            .tipoIdentificacion(UPDATED_TIPO_IDENTIFICACION)
            .identificacion(UPDATED_IDENTIFICACION)
            .edad(UPDATED_EDAD)
            .sexo(UPDATED_SEXO)
            .telefono(UPDATED_TELEFONO)
            .direccion(UPDATED_DIRECCION)
            .pesoKG(UPDATED_PESO_KG)
            .estaturaCM(UPDATED_ESTATURA_CM)
            .grupoEtnico(UPDATED_GRUPO_ETNICO)
            .estratoSocioeconomico(UPDATED_ESTRATO_SOCIOECONOMICO)
            .oximetriaReferencia(UPDATED_OXIMETRIA_REFERENCIA)
            .ritmoCardiacoReferencia(UPDATED_RITMO_CARDIACO_REFERENCIA)
            .presionSistolicaReferencia(UPDATED_PRESION_SISTOLICA_REFERENCIA)
            .presionDistolicaReferencia(UPDATED_PRESION_DISTOLICA_REFERENCIA)
            .comentarios(UPDATED_COMENTARIOS)
            .pasosReferencia(UPDATED_PASOS_REFERENCIA)
            .caloriasReferencia(UPDATED_CALORIAS_REFERENCIA)
            .metaReferencia(UPDATED_META_REFERENCIA);
        return paciente;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Paciente.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        paciente = createEntity(em);
    }

    @Test
    void createPaciente() throws Exception {
        int databaseSizeBeforeCreate = pacienteRepository.findAll().collectList().block().size();
        // Create the Paciente
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(paciente))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Paciente in the database
        List<Paciente> pacienteList = pacienteRepository.findAll().collectList().block();
        assertThat(pacienteList).hasSize(databaseSizeBeforeCreate + 1);
        Paciente testPaciente = pacienteList.get(pacienteList.size() - 1);
        assertThat(testPaciente.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testPaciente.getTipoIdentificacion()).isEqualTo(DEFAULT_TIPO_IDENTIFICACION);
        assertThat(testPaciente.getIdentificacion()).isEqualTo(DEFAULT_IDENTIFICACION);
        assertThat(testPaciente.getEdad()).isEqualTo(DEFAULT_EDAD);
        assertThat(testPaciente.getSexo()).isEqualTo(DEFAULT_SEXO);
        assertThat(testPaciente.getTelefono()).isEqualTo(DEFAULT_TELEFONO);
        assertThat(testPaciente.getDireccion()).isEqualTo(DEFAULT_DIRECCION);
        assertThat(testPaciente.getPesoKG()).isEqualTo(DEFAULT_PESO_KG);
        assertThat(testPaciente.getEstaturaCM()).isEqualTo(DEFAULT_ESTATURA_CM);
        assertThat(testPaciente.getGrupoEtnico()).isEqualTo(DEFAULT_GRUPO_ETNICO);
        assertThat(testPaciente.getEstratoSocioeconomico()).isEqualTo(DEFAULT_ESTRATO_SOCIOECONOMICO);
        assertThat(testPaciente.getOximetriaReferencia()).isEqualTo(DEFAULT_OXIMETRIA_REFERENCIA);
        assertThat(testPaciente.getRitmoCardiacoReferencia()).isEqualTo(DEFAULT_RITMO_CARDIACO_REFERENCIA);
        assertThat(testPaciente.getPresionSistolicaReferencia()).isEqualTo(DEFAULT_PRESION_SISTOLICA_REFERENCIA);
        assertThat(testPaciente.getPresionDistolicaReferencia()).isEqualTo(DEFAULT_PRESION_DISTOLICA_REFERENCIA);
        assertThat(testPaciente.getComentarios()).isEqualTo(DEFAULT_COMENTARIOS);
        assertThat(testPaciente.getPasosReferencia()).isEqualTo(DEFAULT_PASOS_REFERENCIA);
        assertThat(testPaciente.getCaloriasReferencia()).isEqualTo(DEFAULT_CALORIAS_REFERENCIA);
        assertThat(testPaciente.getMetaReferencia()).isEqualTo(DEFAULT_META_REFERENCIA);
    }

    @Test
    void createPacienteWithExistingId() throws Exception {
        // Create the Paciente with an existing ID
        paciente.setId(1L);

        int databaseSizeBeforeCreate = pacienteRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(paciente))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Paciente in the database
        List<Paciente> pacienteList = pacienteRepository.findAll().collectList().block();
        assertThat(pacienteList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllPacientes() {
        // Initialize the database
        pacienteRepository.save(paciente).block();

        // Get all the pacienteList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(paciente.getId().intValue()))
            .jsonPath("$.[*].nombre")
            .value(hasItem(DEFAULT_NOMBRE))
            .jsonPath("$.[*].tipoIdentificacion")
            .value(hasItem(DEFAULT_TIPO_IDENTIFICACION.toString()))
            .jsonPath("$.[*].identificacion")
            .value(hasItem(DEFAULT_IDENTIFICACION))
            .jsonPath("$.[*].edad")
            .value(hasItem(DEFAULT_EDAD))
            .jsonPath("$.[*].sexo")
            .value(hasItem(DEFAULT_SEXO.toString()))
            .jsonPath("$.[*].telefono")
            .value(hasItem(DEFAULT_TELEFONO))
            .jsonPath("$.[*].direccion")
            .value(hasItem(DEFAULT_DIRECCION))
            .jsonPath("$.[*].pesoKG")
            .value(hasItem(DEFAULT_PESO_KG.doubleValue()))
            .jsonPath("$.[*].estaturaCM")
            .value(hasItem(DEFAULT_ESTATURA_CM))
            .jsonPath("$.[*].grupoEtnico")
            .value(hasItem(DEFAULT_GRUPO_ETNICO.toString()))
            .jsonPath("$.[*].estratoSocioeconomico")
            .value(hasItem(DEFAULT_ESTRATO_SOCIOECONOMICO.toString()))
            .jsonPath("$.[*].oximetriaReferencia")
            .value(hasItem(DEFAULT_OXIMETRIA_REFERENCIA))
            .jsonPath("$.[*].ritmoCardiacoReferencia")
            .value(hasItem(DEFAULT_RITMO_CARDIACO_REFERENCIA))
            .jsonPath("$.[*].presionSistolicaReferencia")
            .value(hasItem(DEFAULT_PRESION_SISTOLICA_REFERENCIA))
            .jsonPath("$.[*].presionDistolicaReferencia")
            .value(hasItem(DEFAULT_PRESION_DISTOLICA_REFERENCIA))
            .jsonPath("$.[*].comentarios")
            .value(hasItem(DEFAULT_COMENTARIOS.toString()))
            .jsonPath("$.[*].pasosReferencia")
            .value(hasItem(DEFAULT_PASOS_REFERENCIA))
            .jsonPath("$.[*].caloriasReferencia")
            .value(hasItem(DEFAULT_CALORIAS_REFERENCIA))
            .jsonPath("$.[*].metaReferencia")
            .value(hasItem(DEFAULT_META_REFERENCIA));
    }

    @Test
    void getPaciente() {
        // Initialize the database
        pacienteRepository.save(paciente).block();

        // Get the paciente
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, paciente.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(paciente.getId().intValue()))
            .jsonPath("$.nombre")
            .value(is(DEFAULT_NOMBRE))
            .jsonPath("$.tipoIdentificacion")
            .value(is(DEFAULT_TIPO_IDENTIFICACION.toString()))
            .jsonPath("$.identificacion")
            .value(is(DEFAULT_IDENTIFICACION))
            .jsonPath("$.edad")
            .value(is(DEFAULT_EDAD))
            .jsonPath("$.sexo")
            .value(is(DEFAULT_SEXO.toString()))
            .jsonPath("$.telefono")
            .value(is(DEFAULT_TELEFONO))
            .jsonPath("$.direccion")
            .value(is(DEFAULT_DIRECCION))
            .jsonPath("$.pesoKG")
            .value(is(DEFAULT_PESO_KG.doubleValue()))
            .jsonPath("$.estaturaCM")
            .value(is(DEFAULT_ESTATURA_CM))
            .jsonPath("$.grupoEtnico")
            .value(is(DEFAULT_GRUPO_ETNICO.toString()))
            .jsonPath("$.estratoSocioeconomico")
            .value(is(DEFAULT_ESTRATO_SOCIOECONOMICO.toString()))
            .jsonPath("$.oximetriaReferencia")
            .value(is(DEFAULT_OXIMETRIA_REFERENCIA))
            .jsonPath("$.ritmoCardiacoReferencia")
            .value(is(DEFAULT_RITMO_CARDIACO_REFERENCIA))
            .jsonPath("$.presionSistolicaReferencia")
            .value(is(DEFAULT_PRESION_SISTOLICA_REFERENCIA))
            .jsonPath("$.presionDistolicaReferencia")
            .value(is(DEFAULT_PRESION_DISTOLICA_REFERENCIA))
            .jsonPath("$.comentarios")
            .value(is(DEFAULT_COMENTARIOS.toString()))
            .jsonPath("$.pasosReferencia")
            .value(is(DEFAULT_PASOS_REFERENCIA))
            .jsonPath("$.caloriasReferencia")
            .value(is(DEFAULT_CALORIAS_REFERENCIA))
            .jsonPath("$.metaReferencia")
            .value(is(DEFAULT_META_REFERENCIA));
    }

    @Test
    void getNonExistingPaciente() {
        // Get the paciente
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewPaciente() throws Exception {
        // Initialize the database
        pacienteRepository.save(paciente).block();

        int databaseSizeBeforeUpdate = pacienteRepository.findAll().collectList().block().size();

        // Update the paciente
        Paciente updatedPaciente = pacienteRepository.findById(paciente.getId()).block();
        updatedPaciente
            .nombre(UPDATED_NOMBRE)
            .tipoIdentificacion(UPDATED_TIPO_IDENTIFICACION)
            .identificacion(UPDATED_IDENTIFICACION)
            .edad(UPDATED_EDAD)
            .sexo(UPDATED_SEXO)
            .telefono(UPDATED_TELEFONO)
            .direccion(UPDATED_DIRECCION)
            .pesoKG(UPDATED_PESO_KG)
            .estaturaCM(UPDATED_ESTATURA_CM)
            .grupoEtnico(UPDATED_GRUPO_ETNICO)
            .estratoSocioeconomico(UPDATED_ESTRATO_SOCIOECONOMICO)
            .oximetriaReferencia(UPDATED_OXIMETRIA_REFERENCIA)
            .ritmoCardiacoReferencia(UPDATED_RITMO_CARDIACO_REFERENCIA)
            .presionSistolicaReferencia(UPDATED_PRESION_SISTOLICA_REFERENCIA)
            .presionDistolicaReferencia(UPDATED_PRESION_DISTOLICA_REFERENCIA)
            .comentarios(UPDATED_COMENTARIOS)
            .pasosReferencia(UPDATED_PASOS_REFERENCIA)
            .caloriasReferencia(UPDATED_CALORIAS_REFERENCIA)
            .metaReferencia(UPDATED_META_REFERENCIA);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedPaciente.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedPaciente))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Paciente in the database
        List<Paciente> pacienteList = pacienteRepository.findAll().collectList().block();
        assertThat(pacienteList).hasSize(databaseSizeBeforeUpdate);
        Paciente testPaciente = pacienteList.get(pacienteList.size() - 1);
        assertThat(testPaciente.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testPaciente.getTipoIdentificacion()).isEqualTo(UPDATED_TIPO_IDENTIFICACION);
        assertThat(testPaciente.getIdentificacion()).isEqualTo(UPDATED_IDENTIFICACION);
        assertThat(testPaciente.getEdad()).isEqualTo(UPDATED_EDAD);
        assertThat(testPaciente.getSexo()).isEqualTo(UPDATED_SEXO);
        assertThat(testPaciente.getTelefono()).isEqualTo(UPDATED_TELEFONO);
        assertThat(testPaciente.getDireccion()).isEqualTo(UPDATED_DIRECCION);
        assertThat(testPaciente.getPesoKG()).isEqualTo(UPDATED_PESO_KG);
        assertThat(testPaciente.getEstaturaCM()).isEqualTo(UPDATED_ESTATURA_CM);
        assertThat(testPaciente.getGrupoEtnico()).isEqualTo(UPDATED_GRUPO_ETNICO);
        assertThat(testPaciente.getEstratoSocioeconomico()).isEqualTo(UPDATED_ESTRATO_SOCIOECONOMICO);
        assertThat(testPaciente.getOximetriaReferencia()).isEqualTo(UPDATED_OXIMETRIA_REFERENCIA);
        assertThat(testPaciente.getRitmoCardiacoReferencia()).isEqualTo(UPDATED_RITMO_CARDIACO_REFERENCIA);
        assertThat(testPaciente.getPresionSistolicaReferencia()).isEqualTo(UPDATED_PRESION_SISTOLICA_REFERENCIA);
        assertThat(testPaciente.getPresionDistolicaReferencia()).isEqualTo(UPDATED_PRESION_DISTOLICA_REFERENCIA);
        assertThat(testPaciente.getComentarios()).isEqualTo(UPDATED_COMENTARIOS);
        assertThat(testPaciente.getPasosReferencia()).isEqualTo(UPDATED_PASOS_REFERENCIA);
        assertThat(testPaciente.getCaloriasReferencia()).isEqualTo(UPDATED_CALORIAS_REFERENCIA);
        assertThat(testPaciente.getMetaReferencia()).isEqualTo(UPDATED_META_REFERENCIA);
    }

    @Test
    void putNonExistingPaciente() throws Exception {
        int databaseSizeBeforeUpdate = pacienteRepository.findAll().collectList().block().size();
        paciente.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, paciente.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(paciente))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Paciente in the database
        List<Paciente> pacienteList = pacienteRepository.findAll().collectList().block();
        assertThat(pacienteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchPaciente() throws Exception {
        int databaseSizeBeforeUpdate = pacienteRepository.findAll().collectList().block().size();
        paciente.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(paciente))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Paciente in the database
        List<Paciente> pacienteList = pacienteRepository.findAll().collectList().block();
        assertThat(pacienteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamPaciente() throws Exception {
        int databaseSizeBeforeUpdate = pacienteRepository.findAll().collectList().block().size();
        paciente.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(paciente))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Paciente in the database
        List<Paciente> pacienteList = pacienteRepository.findAll().collectList().block();
        assertThat(pacienteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdatePacienteWithPatch() throws Exception {
        // Initialize the database
        pacienteRepository.save(paciente).block();

        int databaseSizeBeforeUpdate = pacienteRepository.findAll().collectList().block().size();

        // Update the paciente using partial update
        Paciente partialUpdatedPaciente = new Paciente();
        partialUpdatedPaciente.setId(paciente.getId());

        partialUpdatedPaciente
            .nombre(UPDATED_NOMBRE)
            .identificacion(UPDATED_IDENTIFICACION)
            .sexo(UPDATED_SEXO)
            .telefono(UPDATED_TELEFONO)
            .direccion(UPDATED_DIRECCION)
            .pesoKG(UPDATED_PESO_KG)
            .grupoEtnico(UPDATED_GRUPO_ETNICO)
            .oximetriaReferencia(UPDATED_OXIMETRIA_REFERENCIA)
            .ritmoCardiacoReferencia(UPDATED_RITMO_CARDIACO_REFERENCIA)
            .presionDistolicaReferencia(UPDATED_PRESION_DISTOLICA_REFERENCIA)
            .pasosReferencia(UPDATED_PASOS_REFERENCIA);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPaciente.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPaciente))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Paciente in the database
        List<Paciente> pacienteList = pacienteRepository.findAll().collectList().block();
        assertThat(pacienteList).hasSize(databaseSizeBeforeUpdate);
        Paciente testPaciente = pacienteList.get(pacienteList.size() - 1);
        assertThat(testPaciente.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testPaciente.getTipoIdentificacion()).isEqualTo(DEFAULT_TIPO_IDENTIFICACION);
        assertThat(testPaciente.getIdentificacion()).isEqualTo(UPDATED_IDENTIFICACION);
        assertThat(testPaciente.getEdad()).isEqualTo(DEFAULT_EDAD);
        assertThat(testPaciente.getSexo()).isEqualTo(UPDATED_SEXO);
        assertThat(testPaciente.getTelefono()).isEqualTo(UPDATED_TELEFONO);
        assertThat(testPaciente.getDireccion()).isEqualTo(UPDATED_DIRECCION);
        assertThat(testPaciente.getPesoKG()).isEqualTo(UPDATED_PESO_KG);
        assertThat(testPaciente.getEstaturaCM()).isEqualTo(DEFAULT_ESTATURA_CM);
        assertThat(testPaciente.getGrupoEtnico()).isEqualTo(UPDATED_GRUPO_ETNICO);
        assertThat(testPaciente.getEstratoSocioeconomico()).isEqualTo(DEFAULT_ESTRATO_SOCIOECONOMICO);
        assertThat(testPaciente.getOximetriaReferencia()).isEqualTo(UPDATED_OXIMETRIA_REFERENCIA);
        assertThat(testPaciente.getRitmoCardiacoReferencia()).isEqualTo(UPDATED_RITMO_CARDIACO_REFERENCIA);
        assertThat(testPaciente.getPresionSistolicaReferencia()).isEqualTo(DEFAULT_PRESION_SISTOLICA_REFERENCIA);
        assertThat(testPaciente.getPresionDistolicaReferencia()).isEqualTo(UPDATED_PRESION_DISTOLICA_REFERENCIA);
        assertThat(testPaciente.getComentarios()).isEqualTo(DEFAULT_COMENTARIOS);
        assertThat(testPaciente.getPasosReferencia()).isEqualTo(UPDATED_PASOS_REFERENCIA);
        assertThat(testPaciente.getCaloriasReferencia()).isEqualTo(DEFAULT_CALORIAS_REFERENCIA);
        assertThat(testPaciente.getMetaReferencia()).isEqualTo(DEFAULT_META_REFERENCIA);
    }

    @Test
    void fullUpdatePacienteWithPatch() throws Exception {
        // Initialize the database
        pacienteRepository.save(paciente).block();

        int databaseSizeBeforeUpdate = pacienteRepository.findAll().collectList().block().size();

        // Update the paciente using partial update
        Paciente partialUpdatedPaciente = new Paciente();
        partialUpdatedPaciente.setId(paciente.getId());

        partialUpdatedPaciente
            .nombre(UPDATED_NOMBRE)
            .tipoIdentificacion(UPDATED_TIPO_IDENTIFICACION)
            .identificacion(UPDATED_IDENTIFICACION)
            .edad(UPDATED_EDAD)
            .sexo(UPDATED_SEXO)
            .telefono(UPDATED_TELEFONO)
            .direccion(UPDATED_DIRECCION)
            .pesoKG(UPDATED_PESO_KG)
            .estaturaCM(UPDATED_ESTATURA_CM)
            .grupoEtnico(UPDATED_GRUPO_ETNICO)
            .estratoSocioeconomico(UPDATED_ESTRATO_SOCIOECONOMICO)
            .oximetriaReferencia(UPDATED_OXIMETRIA_REFERENCIA)
            .ritmoCardiacoReferencia(UPDATED_RITMO_CARDIACO_REFERENCIA)
            .presionSistolicaReferencia(UPDATED_PRESION_SISTOLICA_REFERENCIA)
            .presionDistolicaReferencia(UPDATED_PRESION_DISTOLICA_REFERENCIA)
            .comentarios(UPDATED_COMENTARIOS)
            .pasosReferencia(UPDATED_PASOS_REFERENCIA)
            .caloriasReferencia(UPDATED_CALORIAS_REFERENCIA)
            .metaReferencia(UPDATED_META_REFERENCIA);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPaciente.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPaciente))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Paciente in the database
        List<Paciente> pacienteList = pacienteRepository.findAll().collectList().block();
        assertThat(pacienteList).hasSize(databaseSizeBeforeUpdate);
        Paciente testPaciente = pacienteList.get(pacienteList.size() - 1);
        assertThat(testPaciente.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testPaciente.getTipoIdentificacion()).isEqualTo(UPDATED_TIPO_IDENTIFICACION);
        assertThat(testPaciente.getIdentificacion()).isEqualTo(UPDATED_IDENTIFICACION);
        assertThat(testPaciente.getEdad()).isEqualTo(UPDATED_EDAD);
        assertThat(testPaciente.getSexo()).isEqualTo(UPDATED_SEXO);
        assertThat(testPaciente.getTelefono()).isEqualTo(UPDATED_TELEFONO);
        assertThat(testPaciente.getDireccion()).isEqualTo(UPDATED_DIRECCION);
        assertThat(testPaciente.getPesoKG()).isEqualTo(UPDATED_PESO_KG);
        assertThat(testPaciente.getEstaturaCM()).isEqualTo(UPDATED_ESTATURA_CM);
        assertThat(testPaciente.getGrupoEtnico()).isEqualTo(UPDATED_GRUPO_ETNICO);
        assertThat(testPaciente.getEstratoSocioeconomico()).isEqualTo(UPDATED_ESTRATO_SOCIOECONOMICO);
        assertThat(testPaciente.getOximetriaReferencia()).isEqualTo(UPDATED_OXIMETRIA_REFERENCIA);
        assertThat(testPaciente.getRitmoCardiacoReferencia()).isEqualTo(UPDATED_RITMO_CARDIACO_REFERENCIA);
        assertThat(testPaciente.getPresionSistolicaReferencia()).isEqualTo(UPDATED_PRESION_SISTOLICA_REFERENCIA);
        assertThat(testPaciente.getPresionDistolicaReferencia()).isEqualTo(UPDATED_PRESION_DISTOLICA_REFERENCIA);
        assertThat(testPaciente.getComentarios()).isEqualTo(UPDATED_COMENTARIOS);
        assertThat(testPaciente.getPasosReferencia()).isEqualTo(UPDATED_PASOS_REFERENCIA);
        assertThat(testPaciente.getCaloriasReferencia()).isEqualTo(UPDATED_CALORIAS_REFERENCIA);
        assertThat(testPaciente.getMetaReferencia()).isEqualTo(UPDATED_META_REFERENCIA);
    }

    @Test
    void patchNonExistingPaciente() throws Exception {
        int databaseSizeBeforeUpdate = pacienteRepository.findAll().collectList().block().size();
        paciente.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, paciente.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(paciente))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Paciente in the database
        List<Paciente> pacienteList = pacienteRepository.findAll().collectList().block();
        assertThat(pacienteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchPaciente() throws Exception {
        int databaseSizeBeforeUpdate = pacienteRepository.findAll().collectList().block().size();
        paciente.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(paciente))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Paciente in the database
        List<Paciente> pacienteList = pacienteRepository.findAll().collectList().block();
        assertThat(pacienteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamPaciente() throws Exception {
        int databaseSizeBeforeUpdate = pacienteRepository.findAll().collectList().block().size();
        paciente.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(paciente))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Paciente in the database
        List<Paciente> pacienteList = pacienteRepository.findAll().collectList().block();
        assertThat(pacienteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deletePaciente() {
        // Initialize the database
        pacienteRepository.save(paciente).block();

        int databaseSizeBeforeDelete = pacienteRepository.findAll().collectList().block().size();

        // Delete the paciente
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, paciente.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Paciente> pacienteList = pacienteRepository.findAll().collectList().block();
        assertThat(pacienteList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
