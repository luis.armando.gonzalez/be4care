package com.be4tech.be4care.pacientes.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.pacientes.IntegrationTest;
import com.be4tech.be4care.pacientes.domain.Condicion;
import com.be4tech.be4care.pacientes.repository.CondicionRepository;
import com.be4tech.be4care.pacientes.service.EntityManager;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link CondicionResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class CondicionResourceIT {

    private static final String DEFAULT_CONDICION = "AAAAAAAAAA";
    private static final String UPDATED_CONDICION = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPCION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPCION = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/condicions";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CondicionRepository condicionRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Condicion condicion;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Condicion createEntity(EntityManager em) {
        Condicion condicion = new Condicion().condicion(DEFAULT_CONDICION).descripcion(DEFAULT_DESCRIPCION);
        return condicion;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Condicion createUpdatedEntity(EntityManager em) {
        Condicion condicion = new Condicion().condicion(UPDATED_CONDICION).descripcion(UPDATED_DESCRIPCION);
        return condicion;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Condicion.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        condicion = createEntity(em);
    }

    @Test
    void createCondicion() throws Exception {
        int databaseSizeBeforeCreate = condicionRepository.findAll().collectList().block().size();
        // Create the Condicion
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(condicion))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Condicion in the database
        List<Condicion> condicionList = condicionRepository.findAll().collectList().block();
        assertThat(condicionList).hasSize(databaseSizeBeforeCreate + 1);
        Condicion testCondicion = condicionList.get(condicionList.size() - 1);
        assertThat(testCondicion.getCondicion()).isEqualTo(DEFAULT_CONDICION);
        assertThat(testCondicion.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
    }

    @Test
    void createCondicionWithExistingId() throws Exception {
        // Create the Condicion with an existing ID
        condicion.setId(1L);

        int databaseSizeBeforeCreate = condicionRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(condicion))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Condicion in the database
        List<Condicion> condicionList = condicionRepository.findAll().collectList().block();
        assertThat(condicionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllCondicions() {
        // Initialize the database
        condicionRepository.save(condicion).block();

        // Get all the condicionList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(condicion.getId().intValue()))
            .jsonPath("$.[*].condicion")
            .value(hasItem(DEFAULT_CONDICION))
            .jsonPath("$.[*].descripcion")
            .value(hasItem(DEFAULT_DESCRIPCION.toString()));
    }

    @Test
    void getCondicion() {
        // Initialize the database
        condicionRepository.save(condicion).block();

        // Get the condicion
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, condicion.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(condicion.getId().intValue()))
            .jsonPath("$.condicion")
            .value(is(DEFAULT_CONDICION))
            .jsonPath("$.descripcion")
            .value(is(DEFAULT_DESCRIPCION.toString()));
    }

    @Test
    void getNonExistingCondicion() {
        // Get the condicion
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewCondicion() throws Exception {
        // Initialize the database
        condicionRepository.save(condicion).block();

        int databaseSizeBeforeUpdate = condicionRepository.findAll().collectList().block().size();

        // Update the condicion
        Condicion updatedCondicion = condicionRepository.findById(condicion.getId()).block();
        updatedCondicion.condicion(UPDATED_CONDICION).descripcion(UPDATED_DESCRIPCION);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedCondicion.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedCondicion))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Condicion in the database
        List<Condicion> condicionList = condicionRepository.findAll().collectList().block();
        assertThat(condicionList).hasSize(databaseSizeBeforeUpdate);
        Condicion testCondicion = condicionList.get(condicionList.size() - 1);
        assertThat(testCondicion.getCondicion()).isEqualTo(UPDATED_CONDICION);
        assertThat(testCondicion.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
    }

    @Test
    void putNonExistingCondicion() throws Exception {
        int databaseSizeBeforeUpdate = condicionRepository.findAll().collectList().block().size();
        condicion.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, condicion.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(condicion))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Condicion in the database
        List<Condicion> condicionList = condicionRepository.findAll().collectList().block();
        assertThat(condicionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchCondicion() throws Exception {
        int databaseSizeBeforeUpdate = condicionRepository.findAll().collectList().block().size();
        condicion.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(condicion))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Condicion in the database
        List<Condicion> condicionList = condicionRepository.findAll().collectList().block();
        assertThat(condicionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamCondicion() throws Exception {
        int databaseSizeBeforeUpdate = condicionRepository.findAll().collectList().block().size();
        condicion.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(condicion))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Condicion in the database
        List<Condicion> condicionList = condicionRepository.findAll().collectList().block();
        assertThat(condicionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateCondicionWithPatch() throws Exception {
        // Initialize the database
        condicionRepository.save(condicion).block();

        int databaseSizeBeforeUpdate = condicionRepository.findAll().collectList().block().size();

        // Update the condicion using partial update
        Condicion partialUpdatedCondicion = new Condicion();
        partialUpdatedCondicion.setId(condicion.getId());

        partialUpdatedCondicion.condicion(UPDATED_CONDICION).descripcion(UPDATED_DESCRIPCION);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedCondicion.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedCondicion))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Condicion in the database
        List<Condicion> condicionList = condicionRepository.findAll().collectList().block();
        assertThat(condicionList).hasSize(databaseSizeBeforeUpdate);
        Condicion testCondicion = condicionList.get(condicionList.size() - 1);
        assertThat(testCondicion.getCondicion()).isEqualTo(UPDATED_CONDICION);
        assertThat(testCondicion.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
    }

    @Test
    void fullUpdateCondicionWithPatch() throws Exception {
        // Initialize the database
        condicionRepository.save(condicion).block();

        int databaseSizeBeforeUpdate = condicionRepository.findAll().collectList().block().size();

        // Update the condicion using partial update
        Condicion partialUpdatedCondicion = new Condicion();
        partialUpdatedCondicion.setId(condicion.getId());

        partialUpdatedCondicion.condicion(UPDATED_CONDICION).descripcion(UPDATED_DESCRIPCION);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedCondicion.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedCondicion))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Condicion in the database
        List<Condicion> condicionList = condicionRepository.findAll().collectList().block();
        assertThat(condicionList).hasSize(databaseSizeBeforeUpdate);
        Condicion testCondicion = condicionList.get(condicionList.size() - 1);
        assertThat(testCondicion.getCondicion()).isEqualTo(UPDATED_CONDICION);
        assertThat(testCondicion.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
    }

    @Test
    void patchNonExistingCondicion() throws Exception {
        int databaseSizeBeforeUpdate = condicionRepository.findAll().collectList().block().size();
        condicion.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, condicion.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(condicion))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Condicion in the database
        List<Condicion> condicionList = condicionRepository.findAll().collectList().block();
        assertThat(condicionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchCondicion() throws Exception {
        int databaseSizeBeforeUpdate = condicionRepository.findAll().collectList().block().size();
        condicion.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(condicion))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Condicion in the database
        List<Condicion> condicionList = condicionRepository.findAll().collectList().block();
        assertThat(condicionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamCondicion() throws Exception {
        int databaseSizeBeforeUpdate = condicionRepository.findAll().collectList().block().size();
        condicion.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(condicion))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Condicion in the database
        List<Condicion> condicionList = condicionRepository.findAll().collectList().block();
        assertThat(condicionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteCondicion() {
        // Initialize the database
        condicionRepository.save(condicion).block();

        int databaseSizeBeforeDelete = condicionRepository.findAll().collectList().block().size();

        // Delete the condicion
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, condicion.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Condicion> condicionList = condicionRepository.findAll().collectList().block();
        assertThat(condicionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
