package com.be4tech.be4care.pacientes.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.pacientes.IntegrationTest;
import com.be4tech.be4care.pacientes.domain.TratamientoMedicamento;
import com.be4tech.be4care.pacientes.repository.TratamientoMedicamentoRepository;
import com.be4tech.be4care.pacientes.service.EntityManager;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link TratamientoMedicamentoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class TratamientoMedicamentoResourceIT {

    private static final String DEFAULT_DOSIS = "AAAAAAAAAA";
    private static final String UPDATED_DOSIS = "BBBBBBBBBB";

    private static final String DEFAULT_INTENSIDAD = "AAAAAAAAAA";
    private static final String UPDATED_INTENSIDAD = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/tratamiento-medicamentos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private TratamientoMedicamentoRepository tratamientoMedicamentoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private TratamientoMedicamento tratamientoMedicamento;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TratamientoMedicamento createEntity(EntityManager em) {
        TratamientoMedicamento tratamientoMedicamento = new TratamientoMedicamento().dosis(DEFAULT_DOSIS).intensidad(DEFAULT_INTENSIDAD);
        return tratamientoMedicamento;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TratamientoMedicamento createUpdatedEntity(EntityManager em) {
        TratamientoMedicamento tratamientoMedicamento = new TratamientoMedicamento().dosis(UPDATED_DOSIS).intensidad(UPDATED_INTENSIDAD);
        return tratamientoMedicamento;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(TratamientoMedicamento.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        tratamientoMedicamento = createEntity(em);
    }

    @Test
    void createTratamientoMedicamento() throws Exception {
        int databaseSizeBeforeCreate = tratamientoMedicamentoRepository.findAll().collectList().block().size();
        // Create the TratamientoMedicamento
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(tratamientoMedicamento))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the TratamientoMedicamento in the database
        List<TratamientoMedicamento> tratamientoMedicamentoList = tratamientoMedicamentoRepository.findAll().collectList().block();
        assertThat(tratamientoMedicamentoList).hasSize(databaseSizeBeforeCreate + 1);
        TratamientoMedicamento testTratamientoMedicamento = tratamientoMedicamentoList.get(tratamientoMedicamentoList.size() - 1);
        assertThat(testTratamientoMedicamento.getDosis()).isEqualTo(DEFAULT_DOSIS);
        assertThat(testTratamientoMedicamento.getIntensidad()).isEqualTo(DEFAULT_INTENSIDAD);
    }

    @Test
    void createTratamientoMedicamentoWithExistingId() throws Exception {
        // Create the TratamientoMedicamento with an existing ID
        tratamientoMedicamento.setId(1L);

        int databaseSizeBeforeCreate = tratamientoMedicamentoRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(tratamientoMedicamento))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the TratamientoMedicamento in the database
        List<TratamientoMedicamento> tratamientoMedicamentoList = tratamientoMedicamentoRepository.findAll().collectList().block();
        assertThat(tratamientoMedicamentoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllTratamientoMedicamentos() {
        // Initialize the database
        tratamientoMedicamentoRepository.save(tratamientoMedicamento).block();

        // Get all the tratamientoMedicamentoList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(tratamientoMedicamento.getId().intValue()))
            .jsonPath("$.[*].dosis")
            .value(hasItem(DEFAULT_DOSIS))
            .jsonPath("$.[*].intensidad")
            .value(hasItem(DEFAULT_INTENSIDAD));
    }

    @Test
    void getTratamientoMedicamento() {
        // Initialize the database
        tratamientoMedicamentoRepository.save(tratamientoMedicamento).block();

        // Get the tratamientoMedicamento
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, tratamientoMedicamento.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(tratamientoMedicamento.getId().intValue()))
            .jsonPath("$.dosis")
            .value(is(DEFAULT_DOSIS))
            .jsonPath("$.intensidad")
            .value(is(DEFAULT_INTENSIDAD));
    }

    @Test
    void getNonExistingTratamientoMedicamento() {
        // Get the tratamientoMedicamento
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewTratamientoMedicamento() throws Exception {
        // Initialize the database
        tratamientoMedicamentoRepository.save(tratamientoMedicamento).block();

        int databaseSizeBeforeUpdate = tratamientoMedicamentoRepository.findAll().collectList().block().size();

        // Update the tratamientoMedicamento
        TratamientoMedicamento updatedTratamientoMedicamento = tratamientoMedicamentoRepository
            .findById(tratamientoMedicamento.getId())
            .block();
        updatedTratamientoMedicamento.dosis(UPDATED_DOSIS).intensidad(UPDATED_INTENSIDAD);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedTratamientoMedicamento.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedTratamientoMedicamento))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the TratamientoMedicamento in the database
        List<TratamientoMedicamento> tratamientoMedicamentoList = tratamientoMedicamentoRepository.findAll().collectList().block();
        assertThat(tratamientoMedicamentoList).hasSize(databaseSizeBeforeUpdate);
        TratamientoMedicamento testTratamientoMedicamento = tratamientoMedicamentoList.get(tratamientoMedicamentoList.size() - 1);
        assertThat(testTratamientoMedicamento.getDosis()).isEqualTo(UPDATED_DOSIS);
        assertThat(testTratamientoMedicamento.getIntensidad()).isEqualTo(UPDATED_INTENSIDAD);
    }

    @Test
    void putNonExistingTratamientoMedicamento() throws Exception {
        int databaseSizeBeforeUpdate = tratamientoMedicamentoRepository.findAll().collectList().block().size();
        tratamientoMedicamento.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, tratamientoMedicamento.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(tratamientoMedicamento))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the TratamientoMedicamento in the database
        List<TratamientoMedicamento> tratamientoMedicamentoList = tratamientoMedicamentoRepository.findAll().collectList().block();
        assertThat(tratamientoMedicamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchTratamientoMedicamento() throws Exception {
        int databaseSizeBeforeUpdate = tratamientoMedicamentoRepository.findAll().collectList().block().size();
        tratamientoMedicamento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(tratamientoMedicamento))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the TratamientoMedicamento in the database
        List<TratamientoMedicamento> tratamientoMedicamentoList = tratamientoMedicamentoRepository.findAll().collectList().block();
        assertThat(tratamientoMedicamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamTratamientoMedicamento() throws Exception {
        int databaseSizeBeforeUpdate = tratamientoMedicamentoRepository.findAll().collectList().block().size();
        tratamientoMedicamento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(tratamientoMedicamento))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the TratamientoMedicamento in the database
        List<TratamientoMedicamento> tratamientoMedicamentoList = tratamientoMedicamentoRepository.findAll().collectList().block();
        assertThat(tratamientoMedicamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateTratamientoMedicamentoWithPatch() throws Exception {
        // Initialize the database
        tratamientoMedicamentoRepository.save(tratamientoMedicamento).block();

        int databaseSizeBeforeUpdate = tratamientoMedicamentoRepository.findAll().collectList().block().size();

        // Update the tratamientoMedicamento using partial update
        TratamientoMedicamento partialUpdatedTratamientoMedicamento = new TratamientoMedicamento();
        partialUpdatedTratamientoMedicamento.setId(tratamientoMedicamento.getId());

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedTratamientoMedicamento.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedTratamientoMedicamento))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the TratamientoMedicamento in the database
        List<TratamientoMedicamento> tratamientoMedicamentoList = tratamientoMedicamentoRepository.findAll().collectList().block();
        assertThat(tratamientoMedicamentoList).hasSize(databaseSizeBeforeUpdate);
        TratamientoMedicamento testTratamientoMedicamento = tratamientoMedicamentoList.get(tratamientoMedicamentoList.size() - 1);
        assertThat(testTratamientoMedicamento.getDosis()).isEqualTo(DEFAULT_DOSIS);
        assertThat(testTratamientoMedicamento.getIntensidad()).isEqualTo(DEFAULT_INTENSIDAD);
    }

    @Test
    void fullUpdateTratamientoMedicamentoWithPatch() throws Exception {
        // Initialize the database
        tratamientoMedicamentoRepository.save(tratamientoMedicamento).block();

        int databaseSizeBeforeUpdate = tratamientoMedicamentoRepository.findAll().collectList().block().size();

        // Update the tratamientoMedicamento using partial update
        TratamientoMedicamento partialUpdatedTratamientoMedicamento = new TratamientoMedicamento();
        partialUpdatedTratamientoMedicamento.setId(tratamientoMedicamento.getId());

        partialUpdatedTratamientoMedicamento.dosis(UPDATED_DOSIS).intensidad(UPDATED_INTENSIDAD);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedTratamientoMedicamento.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedTratamientoMedicamento))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the TratamientoMedicamento in the database
        List<TratamientoMedicamento> tratamientoMedicamentoList = tratamientoMedicamentoRepository.findAll().collectList().block();
        assertThat(tratamientoMedicamentoList).hasSize(databaseSizeBeforeUpdate);
        TratamientoMedicamento testTratamientoMedicamento = tratamientoMedicamentoList.get(tratamientoMedicamentoList.size() - 1);
        assertThat(testTratamientoMedicamento.getDosis()).isEqualTo(UPDATED_DOSIS);
        assertThat(testTratamientoMedicamento.getIntensidad()).isEqualTo(UPDATED_INTENSIDAD);
    }

    @Test
    void patchNonExistingTratamientoMedicamento() throws Exception {
        int databaseSizeBeforeUpdate = tratamientoMedicamentoRepository.findAll().collectList().block().size();
        tratamientoMedicamento.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, tratamientoMedicamento.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(tratamientoMedicamento))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the TratamientoMedicamento in the database
        List<TratamientoMedicamento> tratamientoMedicamentoList = tratamientoMedicamentoRepository.findAll().collectList().block();
        assertThat(tratamientoMedicamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchTratamientoMedicamento() throws Exception {
        int databaseSizeBeforeUpdate = tratamientoMedicamentoRepository.findAll().collectList().block().size();
        tratamientoMedicamento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(tratamientoMedicamento))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the TratamientoMedicamento in the database
        List<TratamientoMedicamento> tratamientoMedicamentoList = tratamientoMedicamentoRepository.findAll().collectList().block();
        assertThat(tratamientoMedicamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamTratamientoMedicamento() throws Exception {
        int databaseSizeBeforeUpdate = tratamientoMedicamentoRepository.findAll().collectList().block().size();
        tratamientoMedicamento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(tratamientoMedicamento))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the TratamientoMedicamento in the database
        List<TratamientoMedicamento> tratamientoMedicamentoList = tratamientoMedicamentoRepository.findAll().collectList().block();
        assertThat(tratamientoMedicamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteTratamientoMedicamento() {
        // Initialize the database
        tratamientoMedicamentoRepository.save(tratamientoMedicamento).block();

        int databaseSizeBeforeDelete = tratamientoMedicamentoRepository.findAll().collectList().block().size();

        // Delete the tratamientoMedicamento
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, tratamientoMedicamento.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<TratamientoMedicamento> tratamientoMedicamentoList = tratamientoMedicamentoRepository.findAll().collectList().block();
        assertThat(tratamientoMedicamentoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
