package com.be4tech.be4care.pacientes.web.rest;

import static com.be4tech.be4care.pacientes.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.pacientes.IntegrationTest;
import com.be4tech.be4care.pacientes.domain.Tratamiento;
import com.be4tech.be4care.pacientes.repository.TratamientoRepository;
import com.be4tech.be4care.pacientes.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link TratamientoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class TratamientoResourceIT {

    private static final String DEFAULT_DESCRIPCION_TRATAMIENTO = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPCION_TRATAMIENTO = "BBBBBBBBBB";

    private static final Instant DEFAULT_FECHA_INICIO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_INICIO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final ZonedDateTime DEFAULT_FECHA_FIN = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FECHA_FIN = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/tratamientos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private TratamientoRepository tratamientoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Tratamiento tratamiento;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tratamiento createEntity(EntityManager em) {
        Tratamiento tratamiento = new Tratamiento()
            .descripcionTratamiento(DEFAULT_DESCRIPCION_TRATAMIENTO)
            .fechaInicio(DEFAULT_FECHA_INICIO)
            .fechaFin(DEFAULT_FECHA_FIN);
        return tratamiento;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tratamiento createUpdatedEntity(EntityManager em) {
        Tratamiento tratamiento = new Tratamiento()
            .descripcionTratamiento(UPDATED_DESCRIPCION_TRATAMIENTO)
            .fechaInicio(UPDATED_FECHA_INICIO)
            .fechaFin(UPDATED_FECHA_FIN);
        return tratamiento;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Tratamiento.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        tratamiento = createEntity(em);
    }

    @Test
    void createTratamiento() throws Exception {
        int databaseSizeBeforeCreate = tratamientoRepository.findAll().collectList().block().size();
        // Create the Tratamiento
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(tratamiento))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Tratamiento in the database
        List<Tratamiento> tratamientoList = tratamientoRepository.findAll().collectList().block();
        assertThat(tratamientoList).hasSize(databaseSizeBeforeCreate + 1);
        Tratamiento testTratamiento = tratamientoList.get(tratamientoList.size() - 1);
        assertThat(testTratamiento.getDescripcionTratamiento()).isEqualTo(DEFAULT_DESCRIPCION_TRATAMIENTO);
        assertThat(testTratamiento.getFechaInicio()).isEqualTo(DEFAULT_FECHA_INICIO);
        assertThat(testTratamiento.getFechaFin()).isEqualTo(DEFAULT_FECHA_FIN);
    }

    @Test
    void createTratamientoWithExistingId() throws Exception {
        // Create the Tratamiento with an existing ID
        tratamiento.setId(1L);

        int databaseSizeBeforeCreate = tratamientoRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(tratamiento))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Tratamiento in the database
        List<Tratamiento> tratamientoList = tratamientoRepository.findAll().collectList().block();
        assertThat(tratamientoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllTratamientos() {
        // Initialize the database
        tratamientoRepository.save(tratamiento).block();

        // Get all the tratamientoList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(tratamiento.getId().intValue()))
            .jsonPath("$.[*].descripcionTratamiento")
            .value(hasItem(DEFAULT_DESCRIPCION_TRATAMIENTO.toString()))
            .jsonPath("$.[*].fechaInicio")
            .value(hasItem(DEFAULT_FECHA_INICIO.toString()))
            .jsonPath("$.[*].fechaFin")
            .value(hasItem(sameInstant(DEFAULT_FECHA_FIN)));
    }

    @Test
    void getTratamiento() {
        // Initialize the database
        tratamientoRepository.save(tratamiento).block();

        // Get the tratamiento
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, tratamiento.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(tratamiento.getId().intValue()))
            .jsonPath("$.descripcionTratamiento")
            .value(is(DEFAULT_DESCRIPCION_TRATAMIENTO.toString()))
            .jsonPath("$.fechaInicio")
            .value(is(DEFAULT_FECHA_INICIO.toString()))
            .jsonPath("$.fechaFin")
            .value(is(sameInstant(DEFAULT_FECHA_FIN)));
    }

    @Test
    void getNonExistingTratamiento() {
        // Get the tratamiento
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewTratamiento() throws Exception {
        // Initialize the database
        tratamientoRepository.save(tratamiento).block();

        int databaseSizeBeforeUpdate = tratamientoRepository.findAll().collectList().block().size();

        // Update the tratamiento
        Tratamiento updatedTratamiento = tratamientoRepository.findById(tratamiento.getId()).block();
        updatedTratamiento
            .descripcionTratamiento(UPDATED_DESCRIPCION_TRATAMIENTO)
            .fechaInicio(UPDATED_FECHA_INICIO)
            .fechaFin(UPDATED_FECHA_FIN);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedTratamiento.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedTratamiento))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Tratamiento in the database
        List<Tratamiento> tratamientoList = tratamientoRepository.findAll().collectList().block();
        assertThat(tratamientoList).hasSize(databaseSizeBeforeUpdate);
        Tratamiento testTratamiento = tratamientoList.get(tratamientoList.size() - 1);
        assertThat(testTratamiento.getDescripcionTratamiento()).isEqualTo(UPDATED_DESCRIPCION_TRATAMIENTO);
        assertThat(testTratamiento.getFechaInicio()).isEqualTo(UPDATED_FECHA_INICIO);
        assertThat(testTratamiento.getFechaFin()).isEqualTo(UPDATED_FECHA_FIN);
    }

    @Test
    void putNonExistingTratamiento() throws Exception {
        int databaseSizeBeforeUpdate = tratamientoRepository.findAll().collectList().block().size();
        tratamiento.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, tratamiento.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(tratamiento))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Tratamiento in the database
        List<Tratamiento> tratamientoList = tratamientoRepository.findAll().collectList().block();
        assertThat(tratamientoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchTratamiento() throws Exception {
        int databaseSizeBeforeUpdate = tratamientoRepository.findAll().collectList().block().size();
        tratamiento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(tratamiento))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Tratamiento in the database
        List<Tratamiento> tratamientoList = tratamientoRepository.findAll().collectList().block();
        assertThat(tratamientoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamTratamiento() throws Exception {
        int databaseSizeBeforeUpdate = tratamientoRepository.findAll().collectList().block().size();
        tratamiento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(tratamiento))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Tratamiento in the database
        List<Tratamiento> tratamientoList = tratamientoRepository.findAll().collectList().block();
        assertThat(tratamientoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateTratamientoWithPatch() throws Exception {
        // Initialize the database
        tratamientoRepository.save(tratamiento).block();

        int databaseSizeBeforeUpdate = tratamientoRepository.findAll().collectList().block().size();

        // Update the tratamiento using partial update
        Tratamiento partialUpdatedTratamiento = new Tratamiento();
        partialUpdatedTratamiento.setId(tratamiento.getId());

        partialUpdatedTratamiento.descripcionTratamiento(UPDATED_DESCRIPCION_TRATAMIENTO).fechaInicio(UPDATED_FECHA_INICIO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedTratamiento.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedTratamiento))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Tratamiento in the database
        List<Tratamiento> tratamientoList = tratamientoRepository.findAll().collectList().block();
        assertThat(tratamientoList).hasSize(databaseSizeBeforeUpdate);
        Tratamiento testTratamiento = tratamientoList.get(tratamientoList.size() - 1);
        assertThat(testTratamiento.getDescripcionTratamiento()).isEqualTo(UPDATED_DESCRIPCION_TRATAMIENTO);
        assertThat(testTratamiento.getFechaInicio()).isEqualTo(UPDATED_FECHA_INICIO);
        assertThat(testTratamiento.getFechaFin()).isEqualTo(DEFAULT_FECHA_FIN);
    }

    @Test
    void fullUpdateTratamientoWithPatch() throws Exception {
        // Initialize the database
        tratamientoRepository.save(tratamiento).block();

        int databaseSizeBeforeUpdate = tratamientoRepository.findAll().collectList().block().size();

        // Update the tratamiento using partial update
        Tratamiento partialUpdatedTratamiento = new Tratamiento();
        partialUpdatedTratamiento.setId(tratamiento.getId());

        partialUpdatedTratamiento
            .descripcionTratamiento(UPDATED_DESCRIPCION_TRATAMIENTO)
            .fechaInicio(UPDATED_FECHA_INICIO)
            .fechaFin(UPDATED_FECHA_FIN);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedTratamiento.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedTratamiento))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Tratamiento in the database
        List<Tratamiento> tratamientoList = tratamientoRepository.findAll().collectList().block();
        assertThat(tratamientoList).hasSize(databaseSizeBeforeUpdate);
        Tratamiento testTratamiento = tratamientoList.get(tratamientoList.size() - 1);
        assertThat(testTratamiento.getDescripcionTratamiento()).isEqualTo(UPDATED_DESCRIPCION_TRATAMIENTO);
        assertThat(testTratamiento.getFechaInicio()).isEqualTo(UPDATED_FECHA_INICIO);
        assertThat(testTratamiento.getFechaFin()).isEqualTo(UPDATED_FECHA_FIN);
    }

    @Test
    void patchNonExistingTratamiento() throws Exception {
        int databaseSizeBeforeUpdate = tratamientoRepository.findAll().collectList().block().size();
        tratamiento.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, tratamiento.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(tratamiento))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Tratamiento in the database
        List<Tratamiento> tratamientoList = tratamientoRepository.findAll().collectList().block();
        assertThat(tratamientoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchTratamiento() throws Exception {
        int databaseSizeBeforeUpdate = tratamientoRepository.findAll().collectList().block().size();
        tratamiento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(tratamiento))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Tratamiento in the database
        List<Tratamiento> tratamientoList = tratamientoRepository.findAll().collectList().block();
        assertThat(tratamientoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamTratamiento() throws Exception {
        int databaseSizeBeforeUpdate = tratamientoRepository.findAll().collectList().block().size();
        tratamiento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(tratamiento))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Tratamiento in the database
        List<Tratamiento> tratamientoList = tratamientoRepository.findAll().collectList().block();
        assertThat(tratamientoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteTratamiento() {
        // Initialize the database
        tratamientoRepository.save(tratamiento).block();

        int databaseSizeBeforeDelete = tratamientoRepository.findAll().collectList().block().size();

        // Delete the tratamiento
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, tratamiento.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Tratamiento> tratamientoList = tratamientoRepository.findAll().collectList().block();
        assertThat(tratamientoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
