package com.be4tech.be4care.pacientes.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.pacientes.IntegrationTest;
import com.be4tech.be4care.pacientes.domain.Farmaceutica;
import com.be4tech.be4care.pacientes.repository.FarmaceuticaRepository;
import com.be4tech.be4care.pacientes.service.EntityManager;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link FarmaceuticaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class FarmaceuticaResourceIT {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    private static final String DEFAULT_DIRECCION = "AAAAAAAAAA";
    private static final String UPDATED_DIRECCION = "BBBBBBBBBB";

    private static final String DEFAULT_PROPIETARIO = "AAAAAAAAAA";
    private static final String UPDATED_PROPIETARIO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/farmaceuticas";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private FarmaceuticaRepository farmaceuticaRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Farmaceutica farmaceutica;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Farmaceutica createEntity(EntityManager em) {
        Farmaceutica farmaceutica = new Farmaceutica().nombre(DEFAULT_NOMBRE).direccion(DEFAULT_DIRECCION).propietario(DEFAULT_PROPIETARIO);
        return farmaceutica;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Farmaceutica createUpdatedEntity(EntityManager em) {
        Farmaceutica farmaceutica = new Farmaceutica().nombre(UPDATED_NOMBRE).direccion(UPDATED_DIRECCION).propietario(UPDATED_PROPIETARIO);
        return farmaceutica;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Farmaceutica.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        farmaceutica = createEntity(em);
    }

    @Test
    void createFarmaceutica() throws Exception {
        int databaseSizeBeforeCreate = farmaceuticaRepository.findAll().collectList().block().size();
        // Create the Farmaceutica
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(farmaceutica))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Farmaceutica in the database
        List<Farmaceutica> farmaceuticaList = farmaceuticaRepository.findAll().collectList().block();
        assertThat(farmaceuticaList).hasSize(databaseSizeBeforeCreate + 1);
        Farmaceutica testFarmaceutica = farmaceuticaList.get(farmaceuticaList.size() - 1);
        assertThat(testFarmaceutica.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testFarmaceutica.getDireccion()).isEqualTo(DEFAULT_DIRECCION);
        assertThat(testFarmaceutica.getPropietario()).isEqualTo(DEFAULT_PROPIETARIO);
    }

    @Test
    void createFarmaceuticaWithExistingId() throws Exception {
        // Create the Farmaceutica with an existing ID
        farmaceutica.setId(1L);

        int databaseSizeBeforeCreate = farmaceuticaRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(farmaceutica))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Farmaceutica in the database
        List<Farmaceutica> farmaceuticaList = farmaceuticaRepository.findAll().collectList().block();
        assertThat(farmaceuticaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllFarmaceuticas() {
        // Initialize the database
        farmaceuticaRepository.save(farmaceutica).block();

        // Get all the farmaceuticaList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(farmaceutica.getId().intValue()))
            .jsonPath("$.[*].nombre")
            .value(hasItem(DEFAULT_NOMBRE))
            .jsonPath("$.[*].direccion")
            .value(hasItem(DEFAULT_DIRECCION))
            .jsonPath("$.[*].propietario")
            .value(hasItem(DEFAULT_PROPIETARIO));
    }

    @Test
    void getFarmaceutica() {
        // Initialize the database
        farmaceuticaRepository.save(farmaceutica).block();

        // Get the farmaceutica
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, farmaceutica.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(farmaceutica.getId().intValue()))
            .jsonPath("$.nombre")
            .value(is(DEFAULT_NOMBRE))
            .jsonPath("$.direccion")
            .value(is(DEFAULT_DIRECCION))
            .jsonPath("$.propietario")
            .value(is(DEFAULT_PROPIETARIO));
    }

    @Test
    void getNonExistingFarmaceutica() {
        // Get the farmaceutica
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewFarmaceutica() throws Exception {
        // Initialize the database
        farmaceuticaRepository.save(farmaceutica).block();

        int databaseSizeBeforeUpdate = farmaceuticaRepository.findAll().collectList().block().size();

        // Update the farmaceutica
        Farmaceutica updatedFarmaceutica = farmaceuticaRepository.findById(farmaceutica.getId()).block();
        updatedFarmaceutica.nombre(UPDATED_NOMBRE).direccion(UPDATED_DIRECCION).propietario(UPDATED_PROPIETARIO);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedFarmaceutica.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedFarmaceutica))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Farmaceutica in the database
        List<Farmaceutica> farmaceuticaList = farmaceuticaRepository.findAll().collectList().block();
        assertThat(farmaceuticaList).hasSize(databaseSizeBeforeUpdate);
        Farmaceutica testFarmaceutica = farmaceuticaList.get(farmaceuticaList.size() - 1);
        assertThat(testFarmaceutica.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testFarmaceutica.getDireccion()).isEqualTo(UPDATED_DIRECCION);
        assertThat(testFarmaceutica.getPropietario()).isEqualTo(UPDATED_PROPIETARIO);
    }

    @Test
    void putNonExistingFarmaceutica() throws Exception {
        int databaseSizeBeforeUpdate = farmaceuticaRepository.findAll().collectList().block().size();
        farmaceutica.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, farmaceutica.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(farmaceutica))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Farmaceutica in the database
        List<Farmaceutica> farmaceuticaList = farmaceuticaRepository.findAll().collectList().block();
        assertThat(farmaceuticaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchFarmaceutica() throws Exception {
        int databaseSizeBeforeUpdate = farmaceuticaRepository.findAll().collectList().block().size();
        farmaceutica.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(farmaceutica))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Farmaceutica in the database
        List<Farmaceutica> farmaceuticaList = farmaceuticaRepository.findAll().collectList().block();
        assertThat(farmaceuticaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamFarmaceutica() throws Exception {
        int databaseSizeBeforeUpdate = farmaceuticaRepository.findAll().collectList().block().size();
        farmaceutica.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(farmaceutica))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Farmaceutica in the database
        List<Farmaceutica> farmaceuticaList = farmaceuticaRepository.findAll().collectList().block();
        assertThat(farmaceuticaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateFarmaceuticaWithPatch() throws Exception {
        // Initialize the database
        farmaceuticaRepository.save(farmaceutica).block();

        int databaseSizeBeforeUpdate = farmaceuticaRepository.findAll().collectList().block().size();

        // Update the farmaceutica using partial update
        Farmaceutica partialUpdatedFarmaceutica = new Farmaceutica();
        partialUpdatedFarmaceutica.setId(farmaceutica.getId());

        partialUpdatedFarmaceutica.direccion(UPDATED_DIRECCION);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedFarmaceutica.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedFarmaceutica))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Farmaceutica in the database
        List<Farmaceutica> farmaceuticaList = farmaceuticaRepository.findAll().collectList().block();
        assertThat(farmaceuticaList).hasSize(databaseSizeBeforeUpdate);
        Farmaceutica testFarmaceutica = farmaceuticaList.get(farmaceuticaList.size() - 1);
        assertThat(testFarmaceutica.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testFarmaceutica.getDireccion()).isEqualTo(UPDATED_DIRECCION);
        assertThat(testFarmaceutica.getPropietario()).isEqualTo(DEFAULT_PROPIETARIO);
    }

    @Test
    void fullUpdateFarmaceuticaWithPatch() throws Exception {
        // Initialize the database
        farmaceuticaRepository.save(farmaceutica).block();

        int databaseSizeBeforeUpdate = farmaceuticaRepository.findAll().collectList().block().size();

        // Update the farmaceutica using partial update
        Farmaceutica partialUpdatedFarmaceutica = new Farmaceutica();
        partialUpdatedFarmaceutica.setId(farmaceutica.getId());

        partialUpdatedFarmaceutica.nombre(UPDATED_NOMBRE).direccion(UPDATED_DIRECCION).propietario(UPDATED_PROPIETARIO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedFarmaceutica.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedFarmaceutica))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Farmaceutica in the database
        List<Farmaceutica> farmaceuticaList = farmaceuticaRepository.findAll().collectList().block();
        assertThat(farmaceuticaList).hasSize(databaseSizeBeforeUpdate);
        Farmaceutica testFarmaceutica = farmaceuticaList.get(farmaceuticaList.size() - 1);
        assertThat(testFarmaceutica.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testFarmaceutica.getDireccion()).isEqualTo(UPDATED_DIRECCION);
        assertThat(testFarmaceutica.getPropietario()).isEqualTo(UPDATED_PROPIETARIO);
    }

    @Test
    void patchNonExistingFarmaceutica() throws Exception {
        int databaseSizeBeforeUpdate = farmaceuticaRepository.findAll().collectList().block().size();
        farmaceutica.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, farmaceutica.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(farmaceutica))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Farmaceutica in the database
        List<Farmaceutica> farmaceuticaList = farmaceuticaRepository.findAll().collectList().block();
        assertThat(farmaceuticaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchFarmaceutica() throws Exception {
        int databaseSizeBeforeUpdate = farmaceuticaRepository.findAll().collectList().block().size();
        farmaceutica.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(farmaceutica))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Farmaceutica in the database
        List<Farmaceutica> farmaceuticaList = farmaceuticaRepository.findAll().collectList().block();
        assertThat(farmaceuticaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamFarmaceutica() throws Exception {
        int databaseSizeBeforeUpdate = farmaceuticaRepository.findAll().collectList().block().size();
        farmaceutica.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(farmaceutica))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Farmaceutica in the database
        List<Farmaceutica> farmaceuticaList = farmaceuticaRepository.findAll().collectList().block();
        assertThat(farmaceuticaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteFarmaceutica() {
        // Initialize the database
        farmaceuticaRepository.save(farmaceutica).block();

        int databaseSizeBeforeDelete = farmaceuticaRepository.findAll().collectList().block().size();

        // Delete the farmaceutica
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, farmaceutica.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Farmaceutica> farmaceuticaList = farmaceuticaRepository.findAll().collectList().block();
        assertThat(farmaceuticaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
