package com.be4tech.be4care.pacientes.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.pacientes.IntegrationTest;
import com.be4tech.be4care.pacientes.domain.Pregunta;
import com.be4tech.be4care.pacientes.repository.PreguntaRepository;
import com.be4tech.be4care.pacientes.service.EntityManager;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link PreguntaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class PreguntaResourceIT {

    private static final String DEFAULT_PREGUNTA = "AAAAAAAAAA";
    private static final String UPDATED_PREGUNTA = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/preguntas";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PreguntaRepository preguntaRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Pregunta pregunta;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pregunta createEntity(EntityManager em) {
        Pregunta pregunta = new Pregunta().pregunta(DEFAULT_PREGUNTA);
        return pregunta;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pregunta createUpdatedEntity(EntityManager em) {
        Pregunta pregunta = new Pregunta().pregunta(UPDATED_PREGUNTA);
        return pregunta;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Pregunta.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        pregunta = createEntity(em);
    }

    @Test
    void createPregunta() throws Exception {
        int databaseSizeBeforeCreate = preguntaRepository.findAll().collectList().block().size();
        // Create the Pregunta
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(pregunta))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Pregunta in the database
        List<Pregunta> preguntaList = preguntaRepository.findAll().collectList().block();
        assertThat(preguntaList).hasSize(databaseSizeBeforeCreate + 1);
        Pregunta testPregunta = preguntaList.get(preguntaList.size() - 1);
        assertThat(testPregunta.getPregunta()).isEqualTo(DEFAULT_PREGUNTA);
    }

    @Test
    void createPreguntaWithExistingId() throws Exception {
        // Create the Pregunta with an existing ID
        pregunta.setId(1L);

        int databaseSizeBeforeCreate = preguntaRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(pregunta))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Pregunta in the database
        List<Pregunta> preguntaList = preguntaRepository.findAll().collectList().block();
        assertThat(preguntaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllPreguntas() {
        // Initialize the database
        preguntaRepository.save(pregunta).block();

        // Get all the preguntaList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(pregunta.getId().intValue()))
            .jsonPath("$.[*].pregunta")
            .value(hasItem(DEFAULT_PREGUNTA));
    }

    @Test
    void getPregunta() {
        // Initialize the database
        preguntaRepository.save(pregunta).block();

        // Get the pregunta
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, pregunta.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(pregunta.getId().intValue()))
            .jsonPath("$.pregunta")
            .value(is(DEFAULT_PREGUNTA));
    }

    @Test
    void getNonExistingPregunta() {
        // Get the pregunta
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewPregunta() throws Exception {
        // Initialize the database
        preguntaRepository.save(pregunta).block();

        int databaseSizeBeforeUpdate = preguntaRepository.findAll().collectList().block().size();

        // Update the pregunta
        Pregunta updatedPregunta = preguntaRepository.findById(pregunta.getId()).block();
        updatedPregunta.pregunta(UPDATED_PREGUNTA);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedPregunta.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedPregunta))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Pregunta in the database
        List<Pregunta> preguntaList = preguntaRepository.findAll().collectList().block();
        assertThat(preguntaList).hasSize(databaseSizeBeforeUpdate);
        Pregunta testPregunta = preguntaList.get(preguntaList.size() - 1);
        assertThat(testPregunta.getPregunta()).isEqualTo(UPDATED_PREGUNTA);
    }

    @Test
    void putNonExistingPregunta() throws Exception {
        int databaseSizeBeforeUpdate = preguntaRepository.findAll().collectList().block().size();
        pregunta.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, pregunta.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(pregunta))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Pregunta in the database
        List<Pregunta> preguntaList = preguntaRepository.findAll().collectList().block();
        assertThat(preguntaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchPregunta() throws Exception {
        int databaseSizeBeforeUpdate = preguntaRepository.findAll().collectList().block().size();
        pregunta.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(pregunta))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Pregunta in the database
        List<Pregunta> preguntaList = preguntaRepository.findAll().collectList().block();
        assertThat(preguntaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamPregunta() throws Exception {
        int databaseSizeBeforeUpdate = preguntaRepository.findAll().collectList().block().size();
        pregunta.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(pregunta))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Pregunta in the database
        List<Pregunta> preguntaList = preguntaRepository.findAll().collectList().block();
        assertThat(preguntaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdatePreguntaWithPatch() throws Exception {
        // Initialize the database
        preguntaRepository.save(pregunta).block();

        int databaseSizeBeforeUpdate = preguntaRepository.findAll().collectList().block().size();

        // Update the pregunta using partial update
        Pregunta partialUpdatedPregunta = new Pregunta();
        partialUpdatedPregunta.setId(pregunta.getId());

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPregunta.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPregunta))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Pregunta in the database
        List<Pregunta> preguntaList = preguntaRepository.findAll().collectList().block();
        assertThat(preguntaList).hasSize(databaseSizeBeforeUpdate);
        Pregunta testPregunta = preguntaList.get(preguntaList.size() - 1);
        assertThat(testPregunta.getPregunta()).isEqualTo(DEFAULT_PREGUNTA);
    }

    @Test
    void fullUpdatePreguntaWithPatch() throws Exception {
        // Initialize the database
        preguntaRepository.save(pregunta).block();

        int databaseSizeBeforeUpdate = preguntaRepository.findAll().collectList().block().size();

        // Update the pregunta using partial update
        Pregunta partialUpdatedPregunta = new Pregunta();
        partialUpdatedPregunta.setId(pregunta.getId());

        partialUpdatedPregunta.pregunta(UPDATED_PREGUNTA);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPregunta.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPregunta))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Pregunta in the database
        List<Pregunta> preguntaList = preguntaRepository.findAll().collectList().block();
        assertThat(preguntaList).hasSize(databaseSizeBeforeUpdate);
        Pregunta testPregunta = preguntaList.get(preguntaList.size() - 1);
        assertThat(testPregunta.getPregunta()).isEqualTo(UPDATED_PREGUNTA);
    }

    @Test
    void patchNonExistingPregunta() throws Exception {
        int databaseSizeBeforeUpdate = preguntaRepository.findAll().collectList().block().size();
        pregunta.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, pregunta.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(pregunta))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Pregunta in the database
        List<Pregunta> preguntaList = preguntaRepository.findAll().collectList().block();
        assertThat(preguntaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchPregunta() throws Exception {
        int databaseSizeBeforeUpdate = preguntaRepository.findAll().collectList().block().size();
        pregunta.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(pregunta))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Pregunta in the database
        List<Pregunta> preguntaList = preguntaRepository.findAll().collectList().block();
        assertThat(preguntaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamPregunta() throws Exception {
        int databaseSizeBeforeUpdate = preguntaRepository.findAll().collectList().block().size();
        pregunta.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(pregunta))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Pregunta in the database
        List<Pregunta> preguntaList = preguntaRepository.findAll().collectList().block();
        assertThat(preguntaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deletePregunta() {
        // Initialize the database
        preguntaRepository.save(pregunta).block();

        int databaseSizeBeforeDelete = preguntaRepository.findAll().collectList().block().size();

        // Delete the pregunta
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, pregunta.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Pregunta> preguntaList = preguntaRepository.findAll().collectList().block();
        assertThat(preguntaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
