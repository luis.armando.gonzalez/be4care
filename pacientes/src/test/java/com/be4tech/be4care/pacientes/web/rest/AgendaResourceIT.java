package com.be4tech.be4care.pacientes.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.pacientes.IntegrationTest;
import com.be4tech.be4care.pacientes.domain.Agenda;
import com.be4tech.be4care.pacientes.repository.AgendaRepository;
import com.be4tech.be4care.pacientes.service.EntityManager;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link AgendaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class AgendaResourceIT {

    private static final Integer DEFAULT_HORA_MEDICAMENTO = 1;
    private static final Integer UPDATED_HORA_MEDICAMENTO = 2;

    private static final String ENTITY_API_URL = "/api/agenda";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private AgendaRepository agendaRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Agenda agenda;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Agenda createEntity(EntityManager em) {
        Agenda agenda = new Agenda().horaMedicamento(DEFAULT_HORA_MEDICAMENTO);
        return agenda;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Agenda createUpdatedEntity(EntityManager em) {
        Agenda agenda = new Agenda().horaMedicamento(UPDATED_HORA_MEDICAMENTO);
        return agenda;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Agenda.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        agenda = createEntity(em);
    }

    @Test
    void createAgenda() throws Exception {
        int databaseSizeBeforeCreate = agendaRepository.findAll().collectList().block().size();
        // Create the Agenda
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(agenda))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll().collectList().block();
        assertThat(agendaList).hasSize(databaseSizeBeforeCreate + 1);
        Agenda testAgenda = agendaList.get(agendaList.size() - 1);
        assertThat(testAgenda.getHoraMedicamento()).isEqualTo(DEFAULT_HORA_MEDICAMENTO);
    }

    @Test
    void createAgendaWithExistingId() throws Exception {
        // Create the Agenda with an existing ID
        agenda.setId(1L);

        int databaseSizeBeforeCreate = agendaRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(agenda))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll().collectList().block();
        assertThat(agendaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllAgenda() {
        // Initialize the database
        agendaRepository.save(agenda).block();

        // Get all the agendaList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(agenda.getId().intValue()))
            .jsonPath("$.[*].horaMedicamento")
            .value(hasItem(DEFAULT_HORA_MEDICAMENTO));
    }

    @Test
    void getAgenda() {
        // Initialize the database
        agendaRepository.save(agenda).block();

        // Get the agenda
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, agenda.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(agenda.getId().intValue()))
            .jsonPath("$.horaMedicamento")
            .value(is(DEFAULT_HORA_MEDICAMENTO));
    }

    @Test
    void getNonExistingAgenda() {
        // Get the agenda
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewAgenda() throws Exception {
        // Initialize the database
        agendaRepository.save(agenda).block();

        int databaseSizeBeforeUpdate = agendaRepository.findAll().collectList().block().size();

        // Update the agenda
        Agenda updatedAgenda = agendaRepository.findById(agenda.getId()).block();
        updatedAgenda.horaMedicamento(UPDATED_HORA_MEDICAMENTO);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedAgenda.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedAgenda))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll().collectList().block();
        assertThat(agendaList).hasSize(databaseSizeBeforeUpdate);
        Agenda testAgenda = agendaList.get(agendaList.size() - 1);
        assertThat(testAgenda.getHoraMedicamento()).isEqualTo(UPDATED_HORA_MEDICAMENTO);
    }

    @Test
    void putNonExistingAgenda() throws Exception {
        int databaseSizeBeforeUpdate = agendaRepository.findAll().collectList().block().size();
        agenda.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, agenda.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(agenda))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll().collectList().block();
        assertThat(agendaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchAgenda() throws Exception {
        int databaseSizeBeforeUpdate = agendaRepository.findAll().collectList().block().size();
        agenda.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(agenda))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll().collectList().block();
        assertThat(agendaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamAgenda() throws Exception {
        int databaseSizeBeforeUpdate = agendaRepository.findAll().collectList().block().size();
        agenda.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(agenda))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll().collectList().block();
        assertThat(agendaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateAgendaWithPatch() throws Exception {
        // Initialize the database
        agendaRepository.save(agenda).block();

        int databaseSizeBeforeUpdate = agendaRepository.findAll().collectList().block().size();

        // Update the agenda using partial update
        Agenda partialUpdatedAgenda = new Agenda();
        partialUpdatedAgenda.setId(agenda.getId());

        partialUpdatedAgenda.horaMedicamento(UPDATED_HORA_MEDICAMENTO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedAgenda.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedAgenda))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll().collectList().block();
        assertThat(agendaList).hasSize(databaseSizeBeforeUpdate);
        Agenda testAgenda = agendaList.get(agendaList.size() - 1);
        assertThat(testAgenda.getHoraMedicamento()).isEqualTo(UPDATED_HORA_MEDICAMENTO);
    }

    @Test
    void fullUpdateAgendaWithPatch() throws Exception {
        // Initialize the database
        agendaRepository.save(agenda).block();

        int databaseSizeBeforeUpdate = agendaRepository.findAll().collectList().block().size();

        // Update the agenda using partial update
        Agenda partialUpdatedAgenda = new Agenda();
        partialUpdatedAgenda.setId(agenda.getId());

        partialUpdatedAgenda.horaMedicamento(UPDATED_HORA_MEDICAMENTO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedAgenda.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedAgenda))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll().collectList().block();
        assertThat(agendaList).hasSize(databaseSizeBeforeUpdate);
        Agenda testAgenda = agendaList.get(agendaList.size() - 1);
        assertThat(testAgenda.getHoraMedicamento()).isEqualTo(UPDATED_HORA_MEDICAMENTO);
    }

    @Test
    void patchNonExistingAgenda() throws Exception {
        int databaseSizeBeforeUpdate = agendaRepository.findAll().collectList().block().size();
        agenda.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, agenda.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(agenda))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll().collectList().block();
        assertThat(agendaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchAgenda() throws Exception {
        int databaseSizeBeforeUpdate = agendaRepository.findAll().collectList().block().size();
        agenda.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(agenda))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll().collectList().block();
        assertThat(agendaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamAgenda() throws Exception {
        int databaseSizeBeforeUpdate = agendaRepository.findAll().collectList().block().size();
        agenda.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(agenda))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll().collectList().block();
        assertThat(agendaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteAgenda() {
        // Initialize the database
        agendaRepository.save(agenda).block();

        int databaseSizeBeforeDelete = agendaRepository.findAll().collectList().block().size();

        // Delete the agenda
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, agenda.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Agenda> agendaList = agendaRepository.findAll().collectList().block();
        assertThat(agendaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
