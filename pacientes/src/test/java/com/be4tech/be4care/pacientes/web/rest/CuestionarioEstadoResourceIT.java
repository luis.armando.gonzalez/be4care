package com.be4tech.be4care.pacientes.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.pacientes.IntegrationTest;
import com.be4tech.be4care.pacientes.domain.CuestionarioEstado;
import com.be4tech.be4care.pacientes.repository.CuestionarioEstadoRepository;
import com.be4tech.be4care.pacientes.service.EntityManager;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link CuestionarioEstadoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class CuestionarioEstadoResourceIT {

    private static final Integer DEFAULT_VALOR = 1;
    private static final Integer UPDATED_VALOR = 2;

    private static final String DEFAULT_VALORACION = "AAAAAAAAAA";
    private static final String UPDATED_VALORACION = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/cuestionario-estados";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CuestionarioEstadoRepository cuestionarioEstadoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private CuestionarioEstado cuestionarioEstado;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CuestionarioEstado createEntity(EntityManager em) {
        CuestionarioEstado cuestionarioEstado = new CuestionarioEstado().valor(DEFAULT_VALOR).valoracion(DEFAULT_VALORACION);
        return cuestionarioEstado;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CuestionarioEstado createUpdatedEntity(EntityManager em) {
        CuestionarioEstado cuestionarioEstado = new CuestionarioEstado().valor(UPDATED_VALOR).valoracion(UPDATED_VALORACION);
        return cuestionarioEstado;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(CuestionarioEstado.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        cuestionarioEstado = createEntity(em);
    }

    @Test
    void createCuestionarioEstado() throws Exception {
        int databaseSizeBeforeCreate = cuestionarioEstadoRepository.findAll().collectList().block().size();
        // Create the CuestionarioEstado
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(cuestionarioEstado))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the CuestionarioEstado in the database
        List<CuestionarioEstado> cuestionarioEstadoList = cuestionarioEstadoRepository.findAll().collectList().block();
        assertThat(cuestionarioEstadoList).hasSize(databaseSizeBeforeCreate + 1);
        CuestionarioEstado testCuestionarioEstado = cuestionarioEstadoList.get(cuestionarioEstadoList.size() - 1);
        assertThat(testCuestionarioEstado.getValor()).isEqualTo(DEFAULT_VALOR);
        assertThat(testCuestionarioEstado.getValoracion()).isEqualTo(DEFAULT_VALORACION);
    }

    @Test
    void createCuestionarioEstadoWithExistingId() throws Exception {
        // Create the CuestionarioEstado with an existing ID
        cuestionarioEstado.setId(1L);

        int databaseSizeBeforeCreate = cuestionarioEstadoRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(cuestionarioEstado))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the CuestionarioEstado in the database
        List<CuestionarioEstado> cuestionarioEstadoList = cuestionarioEstadoRepository.findAll().collectList().block();
        assertThat(cuestionarioEstadoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllCuestionarioEstados() {
        // Initialize the database
        cuestionarioEstadoRepository.save(cuestionarioEstado).block();

        // Get all the cuestionarioEstadoList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(cuestionarioEstado.getId().intValue()))
            .jsonPath("$.[*].valor")
            .value(hasItem(DEFAULT_VALOR))
            .jsonPath("$.[*].valoracion")
            .value(hasItem(DEFAULT_VALORACION));
    }

    @Test
    void getCuestionarioEstado() {
        // Initialize the database
        cuestionarioEstadoRepository.save(cuestionarioEstado).block();

        // Get the cuestionarioEstado
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, cuestionarioEstado.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(cuestionarioEstado.getId().intValue()))
            .jsonPath("$.valor")
            .value(is(DEFAULT_VALOR))
            .jsonPath("$.valoracion")
            .value(is(DEFAULT_VALORACION));
    }

    @Test
    void getNonExistingCuestionarioEstado() {
        // Get the cuestionarioEstado
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewCuestionarioEstado() throws Exception {
        // Initialize the database
        cuestionarioEstadoRepository.save(cuestionarioEstado).block();

        int databaseSizeBeforeUpdate = cuestionarioEstadoRepository.findAll().collectList().block().size();

        // Update the cuestionarioEstado
        CuestionarioEstado updatedCuestionarioEstado = cuestionarioEstadoRepository.findById(cuestionarioEstado.getId()).block();
        updatedCuestionarioEstado.valor(UPDATED_VALOR).valoracion(UPDATED_VALORACION);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedCuestionarioEstado.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedCuestionarioEstado))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the CuestionarioEstado in the database
        List<CuestionarioEstado> cuestionarioEstadoList = cuestionarioEstadoRepository.findAll().collectList().block();
        assertThat(cuestionarioEstadoList).hasSize(databaseSizeBeforeUpdate);
        CuestionarioEstado testCuestionarioEstado = cuestionarioEstadoList.get(cuestionarioEstadoList.size() - 1);
        assertThat(testCuestionarioEstado.getValor()).isEqualTo(UPDATED_VALOR);
        assertThat(testCuestionarioEstado.getValoracion()).isEqualTo(UPDATED_VALORACION);
    }

    @Test
    void putNonExistingCuestionarioEstado() throws Exception {
        int databaseSizeBeforeUpdate = cuestionarioEstadoRepository.findAll().collectList().block().size();
        cuestionarioEstado.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, cuestionarioEstado.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(cuestionarioEstado))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the CuestionarioEstado in the database
        List<CuestionarioEstado> cuestionarioEstadoList = cuestionarioEstadoRepository.findAll().collectList().block();
        assertThat(cuestionarioEstadoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchCuestionarioEstado() throws Exception {
        int databaseSizeBeforeUpdate = cuestionarioEstadoRepository.findAll().collectList().block().size();
        cuestionarioEstado.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(cuestionarioEstado))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the CuestionarioEstado in the database
        List<CuestionarioEstado> cuestionarioEstadoList = cuestionarioEstadoRepository.findAll().collectList().block();
        assertThat(cuestionarioEstadoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamCuestionarioEstado() throws Exception {
        int databaseSizeBeforeUpdate = cuestionarioEstadoRepository.findAll().collectList().block().size();
        cuestionarioEstado.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(cuestionarioEstado))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the CuestionarioEstado in the database
        List<CuestionarioEstado> cuestionarioEstadoList = cuestionarioEstadoRepository.findAll().collectList().block();
        assertThat(cuestionarioEstadoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateCuestionarioEstadoWithPatch() throws Exception {
        // Initialize the database
        cuestionarioEstadoRepository.save(cuestionarioEstado).block();

        int databaseSizeBeforeUpdate = cuestionarioEstadoRepository.findAll().collectList().block().size();

        // Update the cuestionarioEstado using partial update
        CuestionarioEstado partialUpdatedCuestionarioEstado = new CuestionarioEstado();
        partialUpdatedCuestionarioEstado.setId(cuestionarioEstado.getId());

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedCuestionarioEstado.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedCuestionarioEstado))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the CuestionarioEstado in the database
        List<CuestionarioEstado> cuestionarioEstadoList = cuestionarioEstadoRepository.findAll().collectList().block();
        assertThat(cuestionarioEstadoList).hasSize(databaseSizeBeforeUpdate);
        CuestionarioEstado testCuestionarioEstado = cuestionarioEstadoList.get(cuestionarioEstadoList.size() - 1);
        assertThat(testCuestionarioEstado.getValor()).isEqualTo(DEFAULT_VALOR);
        assertThat(testCuestionarioEstado.getValoracion()).isEqualTo(DEFAULT_VALORACION);
    }

    @Test
    void fullUpdateCuestionarioEstadoWithPatch() throws Exception {
        // Initialize the database
        cuestionarioEstadoRepository.save(cuestionarioEstado).block();

        int databaseSizeBeforeUpdate = cuestionarioEstadoRepository.findAll().collectList().block().size();

        // Update the cuestionarioEstado using partial update
        CuestionarioEstado partialUpdatedCuestionarioEstado = new CuestionarioEstado();
        partialUpdatedCuestionarioEstado.setId(cuestionarioEstado.getId());

        partialUpdatedCuestionarioEstado.valor(UPDATED_VALOR).valoracion(UPDATED_VALORACION);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedCuestionarioEstado.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedCuestionarioEstado))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the CuestionarioEstado in the database
        List<CuestionarioEstado> cuestionarioEstadoList = cuestionarioEstadoRepository.findAll().collectList().block();
        assertThat(cuestionarioEstadoList).hasSize(databaseSizeBeforeUpdate);
        CuestionarioEstado testCuestionarioEstado = cuestionarioEstadoList.get(cuestionarioEstadoList.size() - 1);
        assertThat(testCuestionarioEstado.getValor()).isEqualTo(UPDATED_VALOR);
        assertThat(testCuestionarioEstado.getValoracion()).isEqualTo(UPDATED_VALORACION);
    }

    @Test
    void patchNonExistingCuestionarioEstado() throws Exception {
        int databaseSizeBeforeUpdate = cuestionarioEstadoRepository.findAll().collectList().block().size();
        cuestionarioEstado.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, cuestionarioEstado.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(cuestionarioEstado))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the CuestionarioEstado in the database
        List<CuestionarioEstado> cuestionarioEstadoList = cuestionarioEstadoRepository.findAll().collectList().block();
        assertThat(cuestionarioEstadoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchCuestionarioEstado() throws Exception {
        int databaseSizeBeforeUpdate = cuestionarioEstadoRepository.findAll().collectList().block().size();
        cuestionarioEstado.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(cuestionarioEstado))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the CuestionarioEstado in the database
        List<CuestionarioEstado> cuestionarioEstadoList = cuestionarioEstadoRepository.findAll().collectList().block();
        assertThat(cuestionarioEstadoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamCuestionarioEstado() throws Exception {
        int databaseSizeBeforeUpdate = cuestionarioEstadoRepository.findAll().collectList().block().size();
        cuestionarioEstado.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(cuestionarioEstado))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the CuestionarioEstado in the database
        List<CuestionarioEstado> cuestionarioEstadoList = cuestionarioEstadoRepository.findAll().collectList().block();
        assertThat(cuestionarioEstadoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteCuestionarioEstado() {
        // Initialize the database
        cuestionarioEstadoRepository.save(cuestionarioEstado).block();

        int databaseSizeBeforeDelete = cuestionarioEstadoRepository.findAll().collectList().block().size();

        // Delete the cuestionarioEstado
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, cuestionarioEstado.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<CuestionarioEstado> cuestionarioEstadoList = cuestionarioEstadoRepository.findAll().collectList().block();
        assertThat(cuestionarioEstadoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
