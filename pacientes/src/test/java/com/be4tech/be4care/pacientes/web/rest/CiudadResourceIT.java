package com.be4tech.be4care.pacientes.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.pacientes.IntegrationTest;
import com.be4tech.be4care.pacientes.domain.Ciudad;
import com.be4tech.be4care.pacientes.repository.CiudadRepository;
import com.be4tech.be4care.pacientes.service.EntityManager;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link CiudadResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class CiudadResourceIT {

    private static final String DEFAULT_CIUDAD = "AAAAAAAAAA";
    private static final String UPDATED_CIUDAD = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/ciudads";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CiudadRepository ciudadRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Ciudad ciudad;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Ciudad createEntity(EntityManager em) {
        Ciudad ciudad = new Ciudad().ciudad(DEFAULT_CIUDAD);
        return ciudad;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Ciudad createUpdatedEntity(EntityManager em) {
        Ciudad ciudad = new Ciudad().ciudad(UPDATED_CIUDAD);
        return ciudad;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Ciudad.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        ciudad = createEntity(em);
    }

    @Test
    void createCiudad() throws Exception {
        int databaseSizeBeforeCreate = ciudadRepository.findAll().collectList().block().size();
        // Create the Ciudad
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(ciudad))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Ciudad in the database
        List<Ciudad> ciudadList = ciudadRepository.findAll().collectList().block();
        assertThat(ciudadList).hasSize(databaseSizeBeforeCreate + 1);
        Ciudad testCiudad = ciudadList.get(ciudadList.size() - 1);
        assertThat(testCiudad.getCiudad()).isEqualTo(DEFAULT_CIUDAD);
    }

    @Test
    void createCiudadWithExistingId() throws Exception {
        // Create the Ciudad with an existing ID
        ciudad.setId(1L);

        int databaseSizeBeforeCreate = ciudadRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(ciudad))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Ciudad in the database
        List<Ciudad> ciudadList = ciudadRepository.findAll().collectList().block();
        assertThat(ciudadList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllCiudads() {
        // Initialize the database
        ciudadRepository.save(ciudad).block();

        // Get all the ciudadList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(ciudad.getId().intValue()))
            .jsonPath("$.[*].ciudad")
            .value(hasItem(DEFAULT_CIUDAD));
    }

    @Test
    void getCiudad() {
        // Initialize the database
        ciudadRepository.save(ciudad).block();

        // Get the ciudad
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, ciudad.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(ciudad.getId().intValue()))
            .jsonPath("$.ciudad")
            .value(is(DEFAULT_CIUDAD));
    }

    @Test
    void getNonExistingCiudad() {
        // Get the ciudad
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewCiudad() throws Exception {
        // Initialize the database
        ciudadRepository.save(ciudad).block();

        int databaseSizeBeforeUpdate = ciudadRepository.findAll().collectList().block().size();

        // Update the ciudad
        Ciudad updatedCiudad = ciudadRepository.findById(ciudad.getId()).block();
        updatedCiudad.ciudad(UPDATED_CIUDAD);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedCiudad.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedCiudad))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Ciudad in the database
        List<Ciudad> ciudadList = ciudadRepository.findAll().collectList().block();
        assertThat(ciudadList).hasSize(databaseSizeBeforeUpdate);
        Ciudad testCiudad = ciudadList.get(ciudadList.size() - 1);
        assertThat(testCiudad.getCiudad()).isEqualTo(UPDATED_CIUDAD);
    }

    @Test
    void putNonExistingCiudad() throws Exception {
        int databaseSizeBeforeUpdate = ciudadRepository.findAll().collectList().block().size();
        ciudad.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, ciudad.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(ciudad))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Ciudad in the database
        List<Ciudad> ciudadList = ciudadRepository.findAll().collectList().block();
        assertThat(ciudadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchCiudad() throws Exception {
        int databaseSizeBeforeUpdate = ciudadRepository.findAll().collectList().block().size();
        ciudad.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(ciudad))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Ciudad in the database
        List<Ciudad> ciudadList = ciudadRepository.findAll().collectList().block();
        assertThat(ciudadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamCiudad() throws Exception {
        int databaseSizeBeforeUpdate = ciudadRepository.findAll().collectList().block().size();
        ciudad.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(ciudad))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Ciudad in the database
        List<Ciudad> ciudadList = ciudadRepository.findAll().collectList().block();
        assertThat(ciudadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateCiudadWithPatch() throws Exception {
        // Initialize the database
        ciudadRepository.save(ciudad).block();

        int databaseSizeBeforeUpdate = ciudadRepository.findAll().collectList().block().size();

        // Update the ciudad using partial update
        Ciudad partialUpdatedCiudad = new Ciudad();
        partialUpdatedCiudad.setId(ciudad.getId());

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedCiudad.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedCiudad))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Ciudad in the database
        List<Ciudad> ciudadList = ciudadRepository.findAll().collectList().block();
        assertThat(ciudadList).hasSize(databaseSizeBeforeUpdate);
        Ciudad testCiudad = ciudadList.get(ciudadList.size() - 1);
        assertThat(testCiudad.getCiudad()).isEqualTo(DEFAULT_CIUDAD);
    }

    @Test
    void fullUpdateCiudadWithPatch() throws Exception {
        // Initialize the database
        ciudadRepository.save(ciudad).block();

        int databaseSizeBeforeUpdate = ciudadRepository.findAll().collectList().block().size();

        // Update the ciudad using partial update
        Ciudad partialUpdatedCiudad = new Ciudad();
        partialUpdatedCiudad.setId(ciudad.getId());

        partialUpdatedCiudad.ciudad(UPDATED_CIUDAD);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedCiudad.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedCiudad))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Ciudad in the database
        List<Ciudad> ciudadList = ciudadRepository.findAll().collectList().block();
        assertThat(ciudadList).hasSize(databaseSizeBeforeUpdate);
        Ciudad testCiudad = ciudadList.get(ciudadList.size() - 1);
        assertThat(testCiudad.getCiudad()).isEqualTo(UPDATED_CIUDAD);
    }

    @Test
    void patchNonExistingCiudad() throws Exception {
        int databaseSizeBeforeUpdate = ciudadRepository.findAll().collectList().block().size();
        ciudad.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, ciudad.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(ciudad))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Ciudad in the database
        List<Ciudad> ciudadList = ciudadRepository.findAll().collectList().block();
        assertThat(ciudadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchCiudad() throws Exception {
        int databaseSizeBeforeUpdate = ciudadRepository.findAll().collectList().block().size();
        ciudad.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(ciudad))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Ciudad in the database
        List<Ciudad> ciudadList = ciudadRepository.findAll().collectList().block();
        assertThat(ciudadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamCiudad() throws Exception {
        int databaseSizeBeforeUpdate = ciudadRepository.findAll().collectList().block().size();
        ciudad.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(ciudad))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Ciudad in the database
        List<Ciudad> ciudadList = ciudadRepository.findAll().collectList().block();
        assertThat(ciudadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteCiudad() {
        // Initialize the database
        ciudadRepository.save(ciudad).block();

        int databaseSizeBeforeDelete = ciudadRepository.findAll().collectList().block().size();

        // Delete the ciudad
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, ciudad.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Ciudad> ciudadList = ciudadRepository.findAll().collectList().block();
        assertThat(ciudadList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
