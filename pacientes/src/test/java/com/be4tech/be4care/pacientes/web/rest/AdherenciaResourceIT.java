package com.be4tech.be4care.pacientes.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.pacientes.IntegrationTest;
import com.be4tech.be4care.pacientes.domain.Adherencia;
import com.be4tech.be4care.pacientes.repository.AdherenciaRepository;
import com.be4tech.be4care.pacientes.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link AdherenciaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class AdherenciaResourceIT {

    private static final Instant DEFAULT_HORA_TOMA = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_HORA_TOMA = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_RESPUESTA = false;
    private static final Boolean UPDATED_RESPUESTA = true;

    private static final Integer DEFAULT_VALOR = 1;
    private static final Integer UPDATED_VALOR = 2;

    private static final String DEFAULT_COMENTARIO = "AAAAAAAAAA";
    private static final String UPDATED_COMENTARIO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/adherencias";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private AdherenciaRepository adherenciaRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Adherencia adherencia;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Adherencia createEntity(EntityManager em) {
        Adherencia adherencia = new Adherencia()
            .horaToma(DEFAULT_HORA_TOMA)
            .respuesta(DEFAULT_RESPUESTA)
            .valor(DEFAULT_VALOR)
            .comentario(DEFAULT_COMENTARIO);
        return adherencia;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Adherencia createUpdatedEntity(EntityManager em) {
        Adherencia adherencia = new Adherencia()
            .horaToma(UPDATED_HORA_TOMA)
            .respuesta(UPDATED_RESPUESTA)
            .valor(UPDATED_VALOR)
            .comentario(UPDATED_COMENTARIO);
        return adherencia;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Adherencia.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        adherencia = createEntity(em);
    }

    @Test
    void createAdherencia() throws Exception {
        int databaseSizeBeforeCreate = adherenciaRepository.findAll().collectList().block().size();
        // Create the Adherencia
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(adherencia))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Adherencia in the database
        List<Adherencia> adherenciaList = adherenciaRepository.findAll().collectList().block();
        assertThat(adherenciaList).hasSize(databaseSizeBeforeCreate + 1);
        Adherencia testAdherencia = adherenciaList.get(adherenciaList.size() - 1);
        assertThat(testAdherencia.getHoraToma()).isEqualTo(DEFAULT_HORA_TOMA);
        assertThat(testAdherencia.getRespuesta()).isEqualTo(DEFAULT_RESPUESTA);
        assertThat(testAdherencia.getValor()).isEqualTo(DEFAULT_VALOR);
        assertThat(testAdherencia.getComentario()).isEqualTo(DEFAULT_COMENTARIO);
    }

    @Test
    void createAdherenciaWithExistingId() throws Exception {
        // Create the Adherencia with an existing ID
        adherencia.setId(1L);

        int databaseSizeBeforeCreate = adherenciaRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(adherencia))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Adherencia in the database
        List<Adherencia> adherenciaList = adherenciaRepository.findAll().collectList().block();
        assertThat(adherenciaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllAdherencias() {
        // Initialize the database
        adherenciaRepository.save(adherencia).block();

        // Get all the adherenciaList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(adherencia.getId().intValue()))
            .jsonPath("$.[*].horaToma")
            .value(hasItem(DEFAULT_HORA_TOMA.toString()))
            .jsonPath("$.[*].respuesta")
            .value(hasItem(DEFAULT_RESPUESTA.booleanValue()))
            .jsonPath("$.[*].valor")
            .value(hasItem(DEFAULT_VALOR))
            .jsonPath("$.[*].comentario")
            .value(hasItem(DEFAULT_COMENTARIO));
    }

    @Test
    void getAdherencia() {
        // Initialize the database
        adherenciaRepository.save(adherencia).block();

        // Get the adherencia
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, adherencia.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(adherencia.getId().intValue()))
            .jsonPath("$.horaToma")
            .value(is(DEFAULT_HORA_TOMA.toString()))
            .jsonPath("$.respuesta")
            .value(is(DEFAULT_RESPUESTA.booleanValue()))
            .jsonPath("$.valor")
            .value(is(DEFAULT_VALOR))
            .jsonPath("$.comentario")
            .value(is(DEFAULT_COMENTARIO));
    }

    @Test
    void getNonExistingAdherencia() {
        // Get the adherencia
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewAdherencia() throws Exception {
        // Initialize the database
        adherenciaRepository.save(adherencia).block();

        int databaseSizeBeforeUpdate = adherenciaRepository.findAll().collectList().block().size();

        // Update the adherencia
        Adherencia updatedAdherencia = adherenciaRepository.findById(adherencia.getId()).block();
        updatedAdherencia.horaToma(UPDATED_HORA_TOMA).respuesta(UPDATED_RESPUESTA).valor(UPDATED_VALOR).comentario(UPDATED_COMENTARIO);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedAdherencia.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedAdherencia))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Adherencia in the database
        List<Adherencia> adherenciaList = adherenciaRepository.findAll().collectList().block();
        assertThat(adherenciaList).hasSize(databaseSizeBeforeUpdate);
        Adherencia testAdherencia = adherenciaList.get(adherenciaList.size() - 1);
        assertThat(testAdherencia.getHoraToma()).isEqualTo(UPDATED_HORA_TOMA);
        assertThat(testAdherencia.getRespuesta()).isEqualTo(UPDATED_RESPUESTA);
        assertThat(testAdherencia.getValor()).isEqualTo(UPDATED_VALOR);
        assertThat(testAdherencia.getComentario()).isEqualTo(UPDATED_COMENTARIO);
    }

    @Test
    void putNonExistingAdherencia() throws Exception {
        int databaseSizeBeforeUpdate = adherenciaRepository.findAll().collectList().block().size();
        adherencia.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, adherencia.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(adherencia))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Adherencia in the database
        List<Adherencia> adherenciaList = adherenciaRepository.findAll().collectList().block();
        assertThat(adherenciaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchAdherencia() throws Exception {
        int databaseSizeBeforeUpdate = adherenciaRepository.findAll().collectList().block().size();
        adherencia.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(adherencia))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Adherencia in the database
        List<Adherencia> adherenciaList = adherenciaRepository.findAll().collectList().block();
        assertThat(adherenciaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamAdherencia() throws Exception {
        int databaseSizeBeforeUpdate = adherenciaRepository.findAll().collectList().block().size();
        adherencia.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(adherencia))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Adherencia in the database
        List<Adherencia> adherenciaList = adherenciaRepository.findAll().collectList().block();
        assertThat(adherenciaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateAdherenciaWithPatch() throws Exception {
        // Initialize the database
        adherenciaRepository.save(adherencia).block();

        int databaseSizeBeforeUpdate = adherenciaRepository.findAll().collectList().block().size();

        // Update the adherencia using partial update
        Adherencia partialUpdatedAdherencia = new Adherencia();
        partialUpdatedAdherencia.setId(adherencia.getId());

        partialUpdatedAdherencia.horaToma(UPDATED_HORA_TOMA).comentario(UPDATED_COMENTARIO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedAdherencia.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedAdherencia))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Adherencia in the database
        List<Adherencia> adherenciaList = adherenciaRepository.findAll().collectList().block();
        assertThat(adherenciaList).hasSize(databaseSizeBeforeUpdate);
        Adherencia testAdherencia = adherenciaList.get(adherenciaList.size() - 1);
        assertThat(testAdherencia.getHoraToma()).isEqualTo(UPDATED_HORA_TOMA);
        assertThat(testAdherencia.getRespuesta()).isEqualTo(DEFAULT_RESPUESTA);
        assertThat(testAdherencia.getValor()).isEqualTo(DEFAULT_VALOR);
        assertThat(testAdherencia.getComentario()).isEqualTo(UPDATED_COMENTARIO);
    }

    @Test
    void fullUpdateAdherenciaWithPatch() throws Exception {
        // Initialize the database
        adherenciaRepository.save(adherencia).block();

        int databaseSizeBeforeUpdate = adherenciaRepository.findAll().collectList().block().size();

        // Update the adherencia using partial update
        Adherencia partialUpdatedAdherencia = new Adherencia();
        partialUpdatedAdherencia.setId(adherencia.getId());

        partialUpdatedAdherencia
            .horaToma(UPDATED_HORA_TOMA)
            .respuesta(UPDATED_RESPUESTA)
            .valor(UPDATED_VALOR)
            .comentario(UPDATED_COMENTARIO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedAdherencia.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedAdherencia))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Adherencia in the database
        List<Adherencia> adherenciaList = adherenciaRepository.findAll().collectList().block();
        assertThat(adherenciaList).hasSize(databaseSizeBeforeUpdate);
        Adherencia testAdherencia = adherenciaList.get(adherenciaList.size() - 1);
        assertThat(testAdherencia.getHoraToma()).isEqualTo(UPDATED_HORA_TOMA);
        assertThat(testAdherencia.getRespuesta()).isEqualTo(UPDATED_RESPUESTA);
        assertThat(testAdherencia.getValor()).isEqualTo(UPDATED_VALOR);
        assertThat(testAdherencia.getComentario()).isEqualTo(UPDATED_COMENTARIO);
    }

    @Test
    void patchNonExistingAdherencia() throws Exception {
        int databaseSizeBeforeUpdate = adherenciaRepository.findAll().collectList().block().size();
        adherencia.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, adherencia.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(adherencia))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Adherencia in the database
        List<Adherencia> adherenciaList = adherenciaRepository.findAll().collectList().block();
        assertThat(adherenciaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchAdherencia() throws Exception {
        int databaseSizeBeforeUpdate = adherenciaRepository.findAll().collectList().block().size();
        adherencia.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(adherencia))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Adherencia in the database
        List<Adherencia> adherenciaList = adherenciaRepository.findAll().collectList().block();
        assertThat(adherenciaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamAdherencia() throws Exception {
        int databaseSizeBeforeUpdate = adherenciaRepository.findAll().collectList().block().size();
        adherencia.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(adherencia))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Adherencia in the database
        List<Adherencia> adherenciaList = adherenciaRepository.findAll().collectList().block();
        assertThat(adherenciaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteAdherencia() {
        // Initialize the database
        adherenciaRepository.save(adherencia).block();

        int databaseSizeBeforeDelete = adherenciaRepository.findAll().collectList().block().size();

        // Delete the adherencia
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, adherencia.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Adherencia> adherenciaList = adherenciaRepository.findAll().collectList().block();
        assertThat(adherenciaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
