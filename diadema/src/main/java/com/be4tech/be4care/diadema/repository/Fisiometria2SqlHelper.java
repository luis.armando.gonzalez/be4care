package com.be4tech.be4care.diadema.repository;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Table;

public class Fisiometria2SqlHelper {

    public static List<Expression> getColumns(Table table, String columnPrefix) {
        List<Expression> columns = new ArrayList<>();
        columns.add(Column.aliased("id", table, columnPrefix + "_id"));
        columns.add(Column.aliased("time_instant", table, columnPrefix + "_time_instant"));
        columns.add(Column.aliased("meditacion", table, columnPrefix + "_meditacion"));
        columns.add(Column.aliased("attention", table, columnPrefix + "_attention"));
        columns.add(Column.aliased("delta", table, columnPrefix + "_delta"));
        columns.add(Column.aliased("theta", table, columnPrefix + "_theta"));
        columns.add(Column.aliased("low_alpha", table, columnPrefix + "_low_alpha"));
        columns.add(Column.aliased("high_alpha", table, columnPrefix + "_high_alpha"));
        columns.add(Column.aliased("low_beta", table, columnPrefix + "_low_beta"));
        columns.add(Column.aliased("high_beta", table, columnPrefix + "_high_beta"));
        columns.add(Column.aliased("low_gamma", table, columnPrefix + "_low_gamma"));
        columns.add(Column.aliased("mid_gamma", table, columnPrefix + "_mid_gamma"));
        columns.add(Column.aliased("pulso", table, columnPrefix + "_pulso"));
        columns.add(Column.aliased("respiracion", table, columnPrefix + "_respiracion"));
        columns.add(Column.aliased("acelerometro", table, columnPrefix + "_acelerometro"));
        columns.add(Column.aliased("estado_funcional", table, columnPrefix + "_estado_funcional"));

        columns.add(Column.aliased("user_id", table, columnPrefix + "_user_id"));
        return columns;
    }
}
