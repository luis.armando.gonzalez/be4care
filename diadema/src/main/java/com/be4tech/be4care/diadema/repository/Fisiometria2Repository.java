package com.be4tech.be4care.diadema.repository;

import com.be4tech.be4care.diadema.domain.Fisiometria2;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Fisiometria2 entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Fisiometria2Repository extends R2dbcRepository<Fisiometria2, Long>, Fisiometria2RepositoryInternal {
    Flux<Fisiometria2> findAllBy(Pageable pageable);

    @Query("SELECT * FROM fisiometria_2 entity WHERE entity.user_id = :id")
    Flux<Fisiometria2> findByUser(Long id);

    @Query("SELECT * FROM fisiometria_2 entity WHERE entity.user_id IS NULL")
    Flux<Fisiometria2> findAllWhereUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Fisiometria2> findAll();

    @Override
    Mono<Fisiometria2> findById(Long id);

    @Override
    <S extends Fisiometria2> Mono<S> save(S entity);
}

interface Fisiometria2RepositoryInternal {
    <S extends Fisiometria2> Mono<S> insert(S entity);
    <S extends Fisiometria2> Mono<S> save(S entity);
    Mono<Integer> update(Fisiometria2 entity);

    Flux<Fisiometria2> findAll();
    Mono<Fisiometria2> findById(Long id);
    Flux<Fisiometria2> findAllBy(Pageable pageable);
    Flux<Fisiometria2> findAllBy(Pageable pageable, Criteria criteria);
}
