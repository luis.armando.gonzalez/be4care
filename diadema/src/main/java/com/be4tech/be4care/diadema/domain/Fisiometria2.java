package com.be4tech.be4care.diadema.domain;

import java.io.Serializable;
import java.time.Instant;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Fisiometria2.
 */
@Table("fisiometria_2")
public class Fisiometria2 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("time_instant")
    private Instant timeInstant;

    @Column("meditacion")
    private Integer meditacion;

    @Column("attention")
    private Integer attention;

    @Column("delta")
    private Float delta;

    @Column("theta")
    private Float theta;

    @Column("low_alpha")
    private Float lowAlpha;

    @Column("high_alpha")
    private Float highAlpha;

    @Column("low_beta")
    private Float lowBeta;

    @Column("high_beta")
    private Float highBeta;

    @Column("low_gamma")
    private Float lowGamma;

    @Column("mid_gamma")
    private Float midGamma;

    @Column("pulso")
    private Integer pulso;

    @Column("respiracion")
    private Integer respiracion;

    @Column("acelerometro")
    private Float acelerometro;

    @Column("estado_funcional")
    private Integer estadoFuncional;

    @Transient
    private User user;

    @Column("user_id")
    private String userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Fisiometria2 id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getTimeInstant() {
        return this.timeInstant;
    }

    public Fisiometria2 timeInstant(Instant timeInstant) {
        this.setTimeInstant(timeInstant);
        return this;
    }

    public void setTimeInstant(Instant timeInstant) {
        this.timeInstant = timeInstant;
    }

    public Integer getMeditacion() {
        return this.meditacion;
    }

    public Fisiometria2 meditacion(Integer meditacion) {
        this.setMeditacion(meditacion);
        return this;
    }

    public void setMeditacion(Integer meditacion) {
        this.meditacion = meditacion;
    }

    public Integer getAttention() {
        return this.attention;
    }

    public Fisiometria2 attention(Integer attention) {
        this.setAttention(attention);
        return this;
    }

    public void setAttention(Integer attention) {
        this.attention = attention;
    }

    public Float getDelta() {
        return this.delta;
    }

    public Fisiometria2 delta(Float delta) {
        this.setDelta(delta);
        return this;
    }

    public void setDelta(Float delta) {
        this.delta = delta;
    }

    public Float getTheta() {
        return this.theta;
    }

    public Fisiometria2 theta(Float theta) {
        this.setTheta(theta);
        return this;
    }

    public void setTheta(Float theta) {
        this.theta = theta;
    }

    public Float getLowAlpha() {
        return this.lowAlpha;
    }

    public Fisiometria2 lowAlpha(Float lowAlpha) {
        this.setLowAlpha(lowAlpha);
        return this;
    }

    public void setLowAlpha(Float lowAlpha) {
        this.lowAlpha = lowAlpha;
    }

    public Float getHighAlpha() {
        return this.highAlpha;
    }

    public Fisiometria2 highAlpha(Float highAlpha) {
        this.setHighAlpha(highAlpha);
        return this;
    }

    public void setHighAlpha(Float highAlpha) {
        this.highAlpha = highAlpha;
    }

    public Float getLowBeta() {
        return this.lowBeta;
    }

    public Fisiometria2 lowBeta(Float lowBeta) {
        this.setLowBeta(lowBeta);
        return this;
    }

    public void setLowBeta(Float lowBeta) {
        this.lowBeta = lowBeta;
    }

    public Float getHighBeta() {
        return this.highBeta;
    }

    public Fisiometria2 highBeta(Float highBeta) {
        this.setHighBeta(highBeta);
        return this;
    }

    public void setHighBeta(Float highBeta) {
        this.highBeta = highBeta;
    }

    public Float getLowGamma() {
        return this.lowGamma;
    }

    public Fisiometria2 lowGamma(Float lowGamma) {
        this.setLowGamma(lowGamma);
        return this;
    }

    public void setLowGamma(Float lowGamma) {
        this.lowGamma = lowGamma;
    }

    public Float getMidGamma() {
        return this.midGamma;
    }

    public Fisiometria2 midGamma(Float midGamma) {
        this.setMidGamma(midGamma);
        return this;
    }

    public void setMidGamma(Float midGamma) {
        this.midGamma = midGamma;
    }

    public Integer getPulso() {
        return this.pulso;
    }

    public Fisiometria2 pulso(Integer pulso) {
        this.setPulso(pulso);
        return this;
    }

    public void setPulso(Integer pulso) {
        this.pulso = pulso;
    }

    public Integer getRespiracion() {
        return this.respiracion;
    }

    public Fisiometria2 respiracion(Integer respiracion) {
        this.setRespiracion(respiracion);
        return this;
    }

    public void setRespiracion(Integer respiracion) {
        this.respiracion = respiracion;
    }

    public Float getAcelerometro() {
        return this.acelerometro;
    }

    public Fisiometria2 acelerometro(Float acelerometro) {
        this.setAcelerometro(acelerometro);
        return this;
    }

    public void setAcelerometro(Float acelerometro) {
        this.acelerometro = acelerometro;
    }

    public Integer getEstadoFuncional() {
        return this.estadoFuncional;
    }

    public Fisiometria2 estadoFuncional(Integer estadoFuncional) {
        this.setEstadoFuncional(estadoFuncional);
        return this;
    }

    public void setEstadoFuncional(Integer estadoFuncional) {
        this.estadoFuncional = estadoFuncional;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user != null ? user.getId() : null;
    }

    public Fisiometria2 user(User user) {
        this.setUser(user);
        return this;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String user) {
        this.userId = user;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Fisiometria2)) {
            return false;
        }
        return id != null && id.equals(((Fisiometria2) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Fisiometria2{" +
            "id=" + getId() +
            ", timeInstant='" + getTimeInstant() + "'" +
            ", meditacion=" + getMeditacion() +
            ", attention=" + getAttention() +
            ", delta=" + getDelta() +
            ", theta=" + getTheta() +
            ", lowAlpha=" + getLowAlpha() +
            ", highAlpha=" + getHighAlpha() +
            ", lowBeta=" + getLowBeta() +
            ", highBeta=" + getHighBeta() +
            ", lowGamma=" + getLowGamma() +
            ", midGamma=" + getMidGamma() +
            ", pulso=" + getPulso() +
            ", respiracion=" + getRespiracion() +
            ", acelerometro=" + getAcelerometro() +
            ", estadoFuncional=" + getEstadoFuncional() +
            "}";
    }
}
