package com.be4tech.be4care.diadema.web.rest;

import com.be4tech.be4care.diadema.domain.Fisiometria2;
import com.be4tech.be4care.diadema.repository.Fisiometria2Repository;
import com.be4tech.be4care.diadema.repository.UserRepository;
import com.be4tech.be4care.diadema.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.diadema.domain.Fisiometria2}.
 */
@RestController
@RequestMapping("/api")
public class Fisiometria2Resource {

    private final Logger log = LoggerFactory.getLogger(Fisiometria2Resource.class);

    private static final String ENTITY_NAME = "diademaFisiometria2";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final Fisiometria2Repository fisiometria2Repository;

    private final UserRepository userRepository;

    public Fisiometria2Resource(Fisiometria2Repository fisiometria2Repository, UserRepository userRepository) {
        this.fisiometria2Repository = fisiometria2Repository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /fisiometria-2-s} : Create a new fisiometria2.
     *
     * @param fisiometria2 the fisiometria2 to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fisiometria2, or with status {@code 400 (Bad Request)} if the fisiometria2 has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fisiometria-2-s")
    public Mono<ResponseEntity<Fisiometria2>> createFisiometria2(@RequestBody Fisiometria2 fisiometria2) throws URISyntaxException {
        log.debug("REST request to save Fisiometria2 : {}", fisiometria2);
        if (fisiometria2.getId() != null) {
            throw new BadRequestAlertException("A new fisiometria2 cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (fisiometria2.getUser() != null) {
            // Save user in case it's new and only exists in gateway
            userRepository.save(fisiometria2.getUser());
        }
        return fisiometria2Repository
            .save(fisiometria2)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/fisiometria-2-s/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /fisiometria-2-s/:id} : Updates an existing fisiometria2.
     *
     * @param id the id of the fisiometria2 to save.
     * @param fisiometria2 the fisiometria2 to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fisiometria2,
     * or with status {@code 400 (Bad Request)} if the fisiometria2 is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fisiometria2 couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fisiometria-2-s/{id}")
    public Mono<ResponseEntity<Fisiometria2>> updateFisiometria2(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Fisiometria2 fisiometria2
    ) throws URISyntaxException {
        log.debug("REST request to update Fisiometria2 : {}, {}", id, fisiometria2);
        if (fisiometria2.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, fisiometria2.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return fisiometria2Repository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (fisiometria2.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(fisiometria2.getUser());
                }
                return fisiometria2Repository
                    .save(fisiometria2)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /fisiometria-2-s/:id} : Partial updates given fields of an existing fisiometria2, field will ignore if it is null
     *
     * @param id the id of the fisiometria2 to save.
     * @param fisiometria2 the fisiometria2 to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fisiometria2,
     * or with status {@code 400 (Bad Request)} if the fisiometria2 is not valid,
     * or with status {@code 404 (Not Found)} if the fisiometria2 is not found,
     * or with status {@code 500 (Internal Server Error)} if the fisiometria2 couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/fisiometria-2-s/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Fisiometria2>> partialUpdateFisiometria2(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Fisiometria2 fisiometria2
    ) throws URISyntaxException {
        log.debug("REST request to partial update Fisiometria2 partially : {}, {}", id, fisiometria2);
        if (fisiometria2.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, fisiometria2.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return fisiometria2Repository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (fisiometria2.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(fisiometria2.getUser());
                }

                Mono<Fisiometria2> result = fisiometria2Repository
                    .findById(fisiometria2.getId())
                    .map(existingFisiometria2 -> {
                        if (fisiometria2.getTimeInstant() != null) {
                            existingFisiometria2.setTimeInstant(fisiometria2.getTimeInstant());
                        }
                        if (fisiometria2.getMeditacion() != null) {
                            existingFisiometria2.setMeditacion(fisiometria2.getMeditacion());
                        }
                        if (fisiometria2.getAttention() != null) {
                            existingFisiometria2.setAttention(fisiometria2.getAttention());
                        }
                        if (fisiometria2.getDelta() != null) {
                            existingFisiometria2.setDelta(fisiometria2.getDelta());
                        }
                        if (fisiometria2.getTheta() != null) {
                            existingFisiometria2.setTheta(fisiometria2.getTheta());
                        }
                        if (fisiometria2.getLowAlpha() != null) {
                            existingFisiometria2.setLowAlpha(fisiometria2.getLowAlpha());
                        }
                        if (fisiometria2.getHighAlpha() != null) {
                            existingFisiometria2.setHighAlpha(fisiometria2.getHighAlpha());
                        }
                        if (fisiometria2.getLowBeta() != null) {
                            existingFisiometria2.setLowBeta(fisiometria2.getLowBeta());
                        }
                        if (fisiometria2.getHighBeta() != null) {
                            existingFisiometria2.setHighBeta(fisiometria2.getHighBeta());
                        }
                        if (fisiometria2.getLowGamma() != null) {
                            existingFisiometria2.setLowGamma(fisiometria2.getLowGamma());
                        }
                        if (fisiometria2.getMidGamma() != null) {
                            existingFisiometria2.setMidGamma(fisiometria2.getMidGamma());
                        }
                        if (fisiometria2.getPulso() != null) {
                            existingFisiometria2.setPulso(fisiometria2.getPulso());
                        }
                        if (fisiometria2.getRespiracion() != null) {
                            existingFisiometria2.setRespiracion(fisiometria2.getRespiracion());
                        }
                        if (fisiometria2.getAcelerometro() != null) {
                            existingFisiometria2.setAcelerometro(fisiometria2.getAcelerometro());
                        }
                        if (fisiometria2.getEstadoFuncional() != null) {
                            existingFisiometria2.setEstadoFuncional(fisiometria2.getEstadoFuncional());
                        }

                        return existingFisiometria2;
                    })
                    .flatMap(fisiometria2Repository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /fisiometria-2-s} : get all the fisiometria2s.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fisiometria2s in body.
     */
    @GetMapping("/fisiometria-2-s")
    public Mono<ResponseEntity<List<Fisiometria2>>> getAllFisiometria2s(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Fisiometria2s");
        return fisiometria2Repository
            .count()
            .zipWith(fisiometria2Repository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /fisiometria-2-s/:id} : get the "id" fisiometria2.
     *
     * @param id the id of the fisiometria2 to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fisiometria2, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fisiometria-2-s/{id}")
    public Mono<ResponseEntity<Fisiometria2>> getFisiometria2(@PathVariable Long id) {
        log.debug("REST request to get Fisiometria2 : {}", id);
        Mono<Fisiometria2> fisiometria2 = fisiometria2Repository.findById(id);
        return ResponseUtil.wrapOrNotFound(fisiometria2);
    }

    /**
     * {@code DELETE  /fisiometria-2-s/:id} : delete the "id" fisiometria2.
     *
     * @param id the id of the fisiometria2 to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fisiometria-2-s/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteFisiometria2(@PathVariable Long id) {
        log.debug("REST request to delete Fisiometria2 : {}", id);
        return fisiometria2Repository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
