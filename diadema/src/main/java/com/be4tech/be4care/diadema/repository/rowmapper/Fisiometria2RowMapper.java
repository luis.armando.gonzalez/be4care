package com.be4tech.be4care.diadema.repository.rowmapper;

import com.be4tech.be4care.diadema.domain.Fisiometria2;
import com.be4tech.be4care.diadema.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Fisiometria2}, with proper type conversions.
 */
@Service
public class Fisiometria2RowMapper implements BiFunction<Row, String, Fisiometria2> {

    private final ColumnConverter converter;

    public Fisiometria2RowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Fisiometria2} stored in the database.
     */
    @Override
    public Fisiometria2 apply(Row row, String prefix) {
        Fisiometria2 entity = new Fisiometria2();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setTimeInstant(converter.fromRow(row, prefix + "_time_instant", Instant.class));
        entity.setMeditacion(converter.fromRow(row, prefix + "_meditacion", Integer.class));
        entity.setAttention(converter.fromRow(row, prefix + "_attention", Integer.class));
        entity.setDelta(converter.fromRow(row, prefix + "_delta", Float.class));
        entity.setTheta(converter.fromRow(row, prefix + "_theta", Float.class));
        entity.setLowAlpha(converter.fromRow(row, prefix + "_low_alpha", Float.class));
        entity.setHighAlpha(converter.fromRow(row, prefix + "_high_alpha", Float.class));
        entity.setLowBeta(converter.fromRow(row, prefix + "_low_beta", Float.class));
        entity.setHighBeta(converter.fromRow(row, prefix + "_high_beta", Float.class));
        entity.setLowGamma(converter.fromRow(row, prefix + "_low_gamma", Float.class));
        entity.setMidGamma(converter.fromRow(row, prefix + "_mid_gamma", Float.class));
        entity.setPulso(converter.fromRow(row, prefix + "_pulso", Integer.class));
        entity.setRespiracion(converter.fromRow(row, prefix + "_respiracion", Integer.class));
        entity.setAcelerometro(converter.fromRow(row, prefix + "_acelerometro", Float.class));
        entity.setEstadoFuncional(converter.fromRow(row, prefix + "_estado_funcional", Integer.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        return entity;
    }
}
