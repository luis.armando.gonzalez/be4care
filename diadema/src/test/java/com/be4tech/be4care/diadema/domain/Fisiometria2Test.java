package com.be4tech.be4care.diadema.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.be4tech.be4care.diadema.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class Fisiometria2Test {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Fisiometria2.class);
        Fisiometria2 fisiometria21 = new Fisiometria2();
        fisiometria21.setId(1L);
        Fisiometria2 fisiometria22 = new Fisiometria2();
        fisiometria22.setId(fisiometria21.getId());
        assertThat(fisiometria21).isEqualTo(fisiometria22);
        fisiometria22.setId(2L);
        assertThat(fisiometria21).isNotEqualTo(fisiometria22);
        fisiometria21.setId(null);
        assertThat(fisiometria21).isNotEqualTo(fisiometria22);
    }
}
