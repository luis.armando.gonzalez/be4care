package com.be4tech.be4care.diadema.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.diadema.IntegrationTest;
import com.be4tech.be4care.diadema.domain.Fisiometria2;
import com.be4tech.be4care.diadema.repository.Fisiometria2Repository;
import com.be4tech.be4care.diadema.repository.UserRepository;
import com.be4tech.be4care.diadema.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link Fisiometria2Resource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class Fisiometria2ResourceIT {

    private static final Instant DEFAULT_TIME_INSTANT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TIME_INSTANT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_MEDITACION = 1;
    private static final Integer UPDATED_MEDITACION = 2;

    private static final Integer DEFAULT_ATTENTION = 1;
    private static final Integer UPDATED_ATTENTION = 2;

    private static final Float DEFAULT_DELTA = 1F;
    private static final Float UPDATED_DELTA = 2F;

    private static final Float DEFAULT_THETA = 1F;
    private static final Float UPDATED_THETA = 2F;

    private static final Float DEFAULT_LOW_ALPHA = 1F;
    private static final Float UPDATED_LOW_ALPHA = 2F;

    private static final Float DEFAULT_HIGH_ALPHA = 1F;
    private static final Float UPDATED_HIGH_ALPHA = 2F;

    private static final Float DEFAULT_LOW_BETA = 1F;
    private static final Float UPDATED_LOW_BETA = 2F;

    private static final Float DEFAULT_HIGH_BETA = 1F;
    private static final Float UPDATED_HIGH_BETA = 2F;

    private static final Float DEFAULT_LOW_GAMMA = 1F;
    private static final Float UPDATED_LOW_GAMMA = 2F;

    private static final Float DEFAULT_MID_GAMMA = 1F;
    private static final Float UPDATED_MID_GAMMA = 2F;

    private static final Integer DEFAULT_PULSO = 1;
    private static final Integer UPDATED_PULSO = 2;

    private static final Integer DEFAULT_RESPIRACION = 1;
    private static final Integer UPDATED_RESPIRACION = 2;

    private static final Float DEFAULT_ACELEROMETRO = 1F;
    private static final Float UPDATED_ACELEROMETRO = 2F;

    private static final Integer DEFAULT_ESTADO_FUNCIONAL = 1;
    private static final Integer UPDATED_ESTADO_FUNCIONAL = 2;

    private static final String ENTITY_API_URL = "/api/fisiometria-2-s";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private Fisiometria2Repository fisiometria2Repository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Fisiometria2 fisiometria2;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fisiometria2 createEntity(EntityManager em) {
        Fisiometria2 fisiometria2 = new Fisiometria2()
            .timeInstant(DEFAULT_TIME_INSTANT)
            .meditacion(DEFAULT_MEDITACION)
            .attention(DEFAULT_ATTENTION)
            .delta(DEFAULT_DELTA)
            .theta(DEFAULT_THETA)
            .lowAlpha(DEFAULT_LOW_ALPHA)
            .highAlpha(DEFAULT_HIGH_ALPHA)
            .lowBeta(DEFAULT_LOW_BETA)
            .highBeta(DEFAULT_HIGH_BETA)
            .lowGamma(DEFAULT_LOW_GAMMA)
            .midGamma(DEFAULT_MID_GAMMA)
            .pulso(DEFAULT_PULSO)
            .respiracion(DEFAULT_RESPIRACION)
            .acelerometro(DEFAULT_ACELEROMETRO)
            .estadoFuncional(DEFAULT_ESTADO_FUNCIONAL);
        return fisiometria2;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fisiometria2 createUpdatedEntity(EntityManager em) {
        Fisiometria2 fisiometria2 = new Fisiometria2()
            .timeInstant(UPDATED_TIME_INSTANT)
            .meditacion(UPDATED_MEDITACION)
            .attention(UPDATED_ATTENTION)
            .delta(UPDATED_DELTA)
            .theta(UPDATED_THETA)
            .lowAlpha(UPDATED_LOW_ALPHA)
            .highAlpha(UPDATED_HIGH_ALPHA)
            .lowBeta(UPDATED_LOW_BETA)
            .highBeta(UPDATED_HIGH_BETA)
            .lowGamma(UPDATED_LOW_GAMMA)
            .midGamma(UPDATED_MID_GAMMA)
            .pulso(UPDATED_PULSO)
            .respiracion(UPDATED_RESPIRACION)
            .acelerometro(UPDATED_ACELEROMETRO)
            .estadoFuncional(UPDATED_ESTADO_FUNCIONAL);
        return fisiometria2;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Fisiometria2.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        fisiometria2 = createEntity(em);
    }

    @Test
    void createFisiometria2() throws Exception {
        int databaseSizeBeforeCreate = fisiometria2Repository.findAll().collectList().block().size();
        // Create the Fisiometria2
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(fisiometria2))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Fisiometria2 in the database
        List<Fisiometria2> fisiometria2List = fisiometria2Repository.findAll().collectList().block();
        assertThat(fisiometria2List).hasSize(databaseSizeBeforeCreate + 1);
        Fisiometria2 testFisiometria2 = fisiometria2List.get(fisiometria2List.size() - 1);
        assertThat(testFisiometria2.getTimeInstant()).isEqualTo(DEFAULT_TIME_INSTANT);
        assertThat(testFisiometria2.getMeditacion()).isEqualTo(DEFAULT_MEDITACION);
        assertThat(testFisiometria2.getAttention()).isEqualTo(DEFAULT_ATTENTION);
        assertThat(testFisiometria2.getDelta()).isEqualTo(DEFAULT_DELTA);
        assertThat(testFisiometria2.getTheta()).isEqualTo(DEFAULT_THETA);
        assertThat(testFisiometria2.getLowAlpha()).isEqualTo(DEFAULT_LOW_ALPHA);
        assertThat(testFisiometria2.getHighAlpha()).isEqualTo(DEFAULT_HIGH_ALPHA);
        assertThat(testFisiometria2.getLowBeta()).isEqualTo(DEFAULT_LOW_BETA);
        assertThat(testFisiometria2.getHighBeta()).isEqualTo(DEFAULT_HIGH_BETA);
        assertThat(testFisiometria2.getLowGamma()).isEqualTo(DEFAULT_LOW_GAMMA);
        assertThat(testFisiometria2.getMidGamma()).isEqualTo(DEFAULT_MID_GAMMA);
        assertThat(testFisiometria2.getPulso()).isEqualTo(DEFAULT_PULSO);
        assertThat(testFisiometria2.getRespiracion()).isEqualTo(DEFAULT_RESPIRACION);
        assertThat(testFisiometria2.getAcelerometro()).isEqualTo(DEFAULT_ACELEROMETRO);
        assertThat(testFisiometria2.getEstadoFuncional()).isEqualTo(DEFAULT_ESTADO_FUNCIONAL);
    }

    @Test
    void createFisiometria2WithExistingId() throws Exception {
        // Create the Fisiometria2 with an existing ID
        fisiometria2.setId(1L);

        int databaseSizeBeforeCreate = fisiometria2Repository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(fisiometria2))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Fisiometria2 in the database
        List<Fisiometria2> fisiometria2List = fisiometria2Repository.findAll().collectList().block();
        assertThat(fisiometria2List).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllFisiometria2s() {
        // Initialize the database
        fisiometria2Repository.save(fisiometria2).block();

        // Get all the fisiometria2List
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(fisiometria2.getId().intValue()))
            .jsonPath("$.[*].timeInstant")
            .value(hasItem(DEFAULT_TIME_INSTANT.toString()))
            .jsonPath("$.[*].meditacion")
            .value(hasItem(DEFAULT_MEDITACION))
            .jsonPath("$.[*].attention")
            .value(hasItem(DEFAULT_ATTENTION))
            .jsonPath("$.[*].delta")
            .value(hasItem(DEFAULT_DELTA.doubleValue()))
            .jsonPath("$.[*].theta")
            .value(hasItem(DEFAULT_THETA.doubleValue()))
            .jsonPath("$.[*].lowAlpha")
            .value(hasItem(DEFAULT_LOW_ALPHA.doubleValue()))
            .jsonPath("$.[*].highAlpha")
            .value(hasItem(DEFAULT_HIGH_ALPHA.doubleValue()))
            .jsonPath("$.[*].lowBeta")
            .value(hasItem(DEFAULT_LOW_BETA.doubleValue()))
            .jsonPath("$.[*].highBeta")
            .value(hasItem(DEFAULT_HIGH_BETA.doubleValue()))
            .jsonPath("$.[*].lowGamma")
            .value(hasItem(DEFAULT_LOW_GAMMA.doubleValue()))
            .jsonPath("$.[*].midGamma")
            .value(hasItem(DEFAULT_MID_GAMMA.doubleValue()))
            .jsonPath("$.[*].pulso")
            .value(hasItem(DEFAULT_PULSO))
            .jsonPath("$.[*].respiracion")
            .value(hasItem(DEFAULT_RESPIRACION))
            .jsonPath("$.[*].acelerometro")
            .value(hasItem(DEFAULT_ACELEROMETRO.doubleValue()))
            .jsonPath("$.[*].estadoFuncional")
            .value(hasItem(DEFAULT_ESTADO_FUNCIONAL));
    }

    @Test
    void getFisiometria2() {
        // Initialize the database
        fisiometria2Repository.save(fisiometria2).block();

        // Get the fisiometria2
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, fisiometria2.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(fisiometria2.getId().intValue()))
            .jsonPath("$.timeInstant")
            .value(is(DEFAULT_TIME_INSTANT.toString()))
            .jsonPath("$.meditacion")
            .value(is(DEFAULT_MEDITACION))
            .jsonPath("$.attention")
            .value(is(DEFAULT_ATTENTION))
            .jsonPath("$.delta")
            .value(is(DEFAULT_DELTA.doubleValue()))
            .jsonPath("$.theta")
            .value(is(DEFAULT_THETA.doubleValue()))
            .jsonPath("$.lowAlpha")
            .value(is(DEFAULT_LOW_ALPHA.doubleValue()))
            .jsonPath("$.highAlpha")
            .value(is(DEFAULT_HIGH_ALPHA.doubleValue()))
            .jsonPath("$.lowBeta")
            .value(is(DEFAULT_LOW_BETA.doubleValue()))
            .jsonPath("$.highBeta")
            .value(is(DEFAULT_HIGH_BETA.doubleValue()))
            .jsonPath("$.lowGamma")
            .value(is(DEFAULT_LOW_GAMMA.doubleValue()))
            .jsonPath("$.midGamma")
            .value(is(DEFAULT_MID_GAMMA.doubleValue()))
            .jsonPath("$.pulso")
            .value(is(DEFAULT_PULSO))
            .jsonPath("$.respiracion")
            .value(is(DEFAULT_RESPIRACION))
            .jsonPath("$.acelerometro")
            .value(is(DEFAULT_ACELEROMETRO.doubleValue()))
            .jsonPath("$.estadoFuncional")
            .value(is(DEFAULT_ESTADO_FUNCIONAL));
    }

    @Test
    void getNonExistingFisiometria2() {
        // Get the fisiometria2
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewFisiometria2() throws Exception {
        // Initialize the database
        fisiometria2Repository.save(fisiometria2).block();

        int databaseSizeBeforeUpdate = fisiometria2Repository.findAll().collectList().block().size();

        // Update the fisiometria2
        Fisiometria2 updatedFisiometria2 = fisiometria2Repository.findById(fisiometria2.getId()).block();
        updatedFisiometria2
            .timeInstant(UPDATED_TIME_INSTANT)
            .meditacion(UPDATED_MEDITACION)
            .attention(UPDATED_ATTENTION)
            .delta(UPDATED_DELTA)
            .theta(UPDATED_THETA)
            .lowAlpha(UPDATED_LOW_ALPHA)
            .highAlpha(UPDATED_HIGH_ALPHA)
            .lowBeta(UPDATED_LOW_BETA)
            .highBeta(UPDATED_HIGH_BETA)
            .lowGamma(UPDATED_LOW_GAMMA)
            .midGamma(UPDATED_MID_GAMMA)
            .pulso(UPDATED_PULSO)
            .respiracion(UPDATED_RESPIRACION)
            .acelerometro(UPDATED_ACELEROMETRO)
            .estadoFuncional(UPDATED_ESTADO_FUNCIONAL);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedFisiometria2.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedFisiometria2))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Fisiometria2 in the database
        List<Fisiometria2> fisiometria2List = fisiometria2Repository.findAll().collectList().block();
        assertThat(fisiometria2List).hasSize(databaseSizeBeforeUpdate);
        Fisiometria2 testFisiometria2 = fisiometria2List.get(fisiometria2List.size() - 1);
        assertThat(testFisiometria2.getTimeInstant()).isEqualTo(UPDATED_TIME_INSTANT);
        assertThat(testFisiometria2.getMeditacion()).isEqualTo(UPDATED_MEDITACION);
        assertThat(testFisiometria2.getAttention()).isEqualTo(UPDATED_ATTENTION);
        assertThat(testFisiometria2.getDelta()).isEqualTo(UPDATED_DELTA);
        assertThat(testFisiometria2.getTheta()).isEqualTo(UPDATED_THETA);
        assertThat(testFisiometria2.getLowAlpha()).isEqualTo(UPDATED_LOW_ALPHA);
        assertThat(testFisiometria2.getHighAlpha()).isEqualTo(UPDATED_HIGH_ALPHA);
        assertThat(testFisiometria2.getLowBeta()).isEqualTo(UPDATED_LOW_BETA);
        assertThat(testFisiometria2.getHighBeta()).isEqualTo(UPDATED_HIGH_BETA);
        assertThat(testFisiometria2.getLowGamma()).isEqualTo(UPDATED_LOW_GAMMA);
        assertThat(testFisiometria2.getMidGamma()).isEqualTo(UPDATED_MID_GAMMA);
        assertThat(testFisiometria2.getPulso()).isEqualTo(UPDATED_PULSO);
        assertThat(testFisiometria2.getRespiracion()).isEqualTo(UPDATED_RESPIRACION);
        assertThat(testFisiometria2.getAcelerometro()).isEqualTo(UPDATED_ACELEROMETRO);
        assertThat(testFisiometria2.getEstadoFuncional()).isEqualTo(UPDATED_ESTADO_FUNCIONAL);
    }

    @Test
    void putNonExistingFisiometria2() throws Exception {
        int databaseSizeBeforeUpdate = fisiometria2Repository.findAll().collectList().block().size();
        fisiometria2.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, fisiometria2.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(fisiometria2))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Fisiometria2 in the database
        List<Fisiometria2> fisiometria2List = fisiometria2Repository.findAll().collectList().block();
        assertThat(fisiometria2List).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchFisiometria2() throws Exception {
        int databaseSizeBeforeUpdate = fisiometria2Repository.findAll().collectList().block().size();
        fisiometria2.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(fisiometria2))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Fisiometria2 in the database
        List<Fisiometria2> fisiometria2List = fisiometria2Repository.findAll().collectList().block();
        assertThat(fisiometria2List).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamFisiometria2() throws Exception {
        int databaseSizeBeforeUpdate = fisiometria2Repository.findAll().collectList().block().size();
        fisiometria2.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(fisiometria2))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Fisiometria2 in the database
        List<Fisiometria2> fisiometria2List = fisiometria2Repository.findAll().collectList().block();
        assertThat(fisiometria2List).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateFisiometria2WithPatch() throws Exception {
        // Initialize the database
        fisiometria2Repository.save(fisiometria2).block();

        int databaseSizeBeforeUpdate = fisiometria2Repository.findAll().collectList().block().size();

        // Update the fisiometria2 using partial update
        Fisiometria2 partialUpdatedFisiometria2 = new Fisiometria2();
        partialUpdatedFisiometria2.setId(fisiometria2.getId());

        partialUpdatedFisiometria2
            .timeInstant(UPDATED_TIME_INSTANT)
            .meditacion(UPDATED_MEDITACION)
            .attention(UPDATED_ATTENTION)
            .theta(UPDATED_THETA)
            .highAlpha(UPDATED_HIGH_ALPHA)
            .lowBeta(UPDATED_LOW_BETA)
            .highBeta(UPDATED_HIGH_BETA)
            .midGamma(UPDATED_MID_GAMMA)
            .acelerometro(UPDATED_ACELEROMETRO)
            .estadoFuncional(UPDATED_ESTADO_FUNCIONAL);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedFisiometria2.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedFisiometria2))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Fisiometria2 in the database
        List<Fisiometria2> fisiometria2List = fisiometria2Repository.findAll().collectList().block();
        assertThat(fisiometria2List).hasSize(databaseSizeBeforeUpdate);
        Fisiometria2 testFisiometria2 = fisiometria2List.get(fisiometria2List.size() - 1);
        assertThat(testFisiometria2.getTimeInstant()).isEqualTo(UPDATED_TIME_INSTANT);
        assertThat(testFisiometria2.getMeditacion()).isEqualTo(UPDATED_MEDITACION);
        assertThat(testFisiometria2.getAttention()).isEqualTo(UPDATED_ATTENTION);
        assertThat(testFisiometria2.getDelta()).isEqualTo(DEFAULT_DELTA);
        assertThat(testFisiometria2.getTheta()).isEqualTo(UPDATED_THETA);
        assertThat(testFisiometria2.getLowAlpha()).isEqualTo(DEFAULT_LOW_ALPHA);
        assertThat(testFisiometria2.getHighAlpha()).isEqualTo(UPDATED_HIGH_ALPHA);
        assertThat(testFisiometria2.getLowBeta()).isEqualTo(UPDATED_LOW_BETA);
        assertThat(testFisiometria2.getHighBeta()).isEqualTo(UPDATED_HIGH_BETA);
        assertThat(testFisiometria2.getLowGamma()).isEqualTo(DEFAULT_LOW_GAMMA);
        assertThat(testFisiometria2.getMidGamma()).isEqualTo(UPDATED_MID_GAMMA);
        assertThat(testFisiometria2.getPulso()).isEqualTo(DEFAULT_PULSO);
        assertThat(testFisiometria2.getRespiracion()).isEqualTo(DEFAULT_RESPIRACION);
        assertThat(testFisiometria2.getAcelerometro()).isEqualTo(UPDATED_ACELEROMETRO);
        assertThat(testFisiometria2.getEstadoFuncional()).isEqualTo(UPDATED_ESTADO_FUNCIONAL);
    }

    @Test
    void fullUpdateFisiometria2WithPatch() throws Exception {
        // Initialize the database
        fisiometria2Repository.save(fisiometria2).block();

        int databaseSizeBeforeUpdate = fisiometria2Repository.findAll().collectList().block().size();

        // Update the fisiometria2 using partial update
        Fisiometria2 partialUpdatedFisiometria2 = new Fisiometria2();
        partialUpdatedFisiometria2.setId(fisiometria2.getId());

        partialUpdatedFisiometria2
            .timeInstant(UPDATED_TIME_INSTANT)
            .meditacion(UPDATED_MEDITACION)
            .attention(UPDATED_ATTENTION)
            .delta(UPDATED_DELTA)
            .theta(UPDATED_THETA)
            .lowAlpha(UPDATED_LOW_ALPHA)
            .highAlpha(UPDATED_HIGH_ALPHA)
            .lowBeta(UPDATED_LOW_BETA)
            .highBeta(UPDATED_HIGH_BETA)
            .lowGamma(UPDATED_LOW_GAMMA)
            .midGamma(UPDATED_MID_GAMMA)
            .pulso(UPDATED_PULSO)
            .respiracion(UPDATED_RESPIRACION)
            .acelerometro(UPDATED_ACELEROMETRO)
            .estadoFuncional(UPDATED_ESTADO_FUNCIONAL);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedFisiometria2.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedFisiometria2))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Fisiometria2 in the database
        List<Fisiometria2> fisiometria2List = fisiometria2Repository.findAll().collectList().block();
        assertThat(fisiometria2List).hasSize(databaseSizeBeforeUpdate);
        Fisiometria2 testFisiometria2 = fisiometria2List.get(fisiometria2List.size() - 1);
        assertThat(testFisiometria2.getTimeInstant()).isEqualTo(UPDATED_TIME_INSTANT);
        assertThat(testFisiometria2.getMeditacion()).isEqualTo(UPDATED_MEDITACION);
        assertThat(testFisiometria2.getAttention()).isEqualTo(UPDATED_ATTENTION);
        assertThat(testFisiometria2.getDelta()).isEqualTo(UPDATED_DELTA);
        assertThat(testFisiometria2.getTheta()).isEqualTo(UPDATED_THETA);
        assertThat(testFisiometria2.getLowAlpha()).isEqualTo(UPDATED_LOW_ALPHA);
        assertThat(testFisiometria2.getHighAlpha()).isEqualTo(UPDATED_HIGH_ALPHA);
        assertThat(testFisiometria2.getLowBeta()).isEqualTo(UPDATED_LOW_BETA);
        assertThat(testFisiometria2.getHighBeta()).isEqualTo(UPDATED_HIGH_BETA);
        assertThat(testFisiometria2.getLowGamma()).isEqualTo(UPDATED_LOW_GAMMA);
        assertThat(testFisiometria2.getMidGamma()).isEqualTo(UPDATED_MID_GAMMA);
        assertThat(testFisiometria2.getPulso()).isEqualTo(UPDATED_PULSO);
        assertThat(testFisiometria2.getRespiracion()).isEqualTo(UPDATED_RESPIRACION);
        assertThat(testFisiometria2.getAcelerometro()).isEqualTo(UPDATED_ACELEROMETRO);
        assertThat(testFisiometria2.getEstadoFuncional()).isEqualTo(UPDATED_ESTADO_FUNCIONAL);
    }

    @Test
    void patchNonExistingFisiometria2() throws Exception {
        int databaseSizeBeforeUpdate = fisiometria2Repository.findAll().collectList().block().size();
        fisiometria2.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, fisiometria2.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(fisiometria2))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Fisiometria2 in the database
        List<Fisiometria2> fisiometria2List = fisiometria2Repository.findAll().collectList().block();
        assertThat(fisiometria2List).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchFisiometria2() throws Exception {
        int databaseSizeBeforeUpdate = fisiometria2Repository.findAll().collectList().block().size();
        fisiometria2.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(fisiometria2))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Fisiometria2 in the database
        List<Fisiometria2> fisiometria2List = fisiometria2Repository.findAll().collectList().block();
        assertThat(fisiometria2List).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamFisiometria2() throws Exception {
        int databaseSizeBeforeUpdate = fisiometria2Repository.findAll().collectList().block().size();
        fisiometria2.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(fisiometria2))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Fisiometria2 in the database
        List<Fisiometria2> fisiometria2List = fisiometria2Repository.findAll().collectList().block();
        assertThat(fisiometria2List).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteFisiometria2() {
        // Initialize the database
        fisiometria2Repository.save(fisiometria2).block();

        int databaseSizeBeforeDelete = fisiometria2Repository.findAll().collectList().block().size();

        // Delete the fisiometria2
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, fisiometria2.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Fisiometria2> fisiometria2List = fisiometria2Repository.findAll().collectList().block();
        assertThat(fisiometria2List).hasSize(databaseSizeBeforeDelete - 1);
    }
}
