package com.be4tech.be4care.manilla.web.rest;

import static com.be4tech.be4care.manilla.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.manilla.IntegrationTest;
import com.be4tech.be4care.manilla.domain.Notificacion;
import com.be4tech.be4care.manilla.repository.NotificacionRepository;
import com.be4tech.be4care.manilla.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link NotificacionResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class NotificacionResourceIT {

    private static final Instant DEFAULT_FECHA_INICIO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_INICIO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final ZonedDateTime DEFAULT_FECHA_ACTUALIZACION = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FECHA_ACTUALIZACION = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Integer DEFAULT_ESTADO = 1;
    private static final Integer UPDATED_ESTADO = 2;

    private static final Integer DEFAULT_TIPO_NOTIFICACION = 1;
    private static final Integer UPDATED_TIPO_NOTIFICACION = 2;

    private static final String ENTITY_API_URL = "/api/notificacions";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private NotificacionRepository notificacionRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Notificacion notificacion;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Notificacion createEntity(EntityManager em) {
        Notificacion notificacion = new Notificacion()
            .fechaInicio(DEFAULT_FECHA_INICIO)
            .fechaActualizacion(DEFAULT_FECHA_ACTUALIZACION)
            .estado(DEFAULT_ESTADO)
            .tipoNotificacion(DEFAULT_TIPO_NOTIFICACION);
        return notificacion;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Notificacion createUpdatedEntity(EntityManager em) {
        Notificacion notificacion = new Notificacion()
            .fechaInicio(UPDATED_FECHA_INICIO)
            .fechaActualizacion(UPDATED_FECHA_ACTUALIZACION)
            .estado(UPDATED_ESTADO)
            .tipoNotificacion(UPDATED_TIPO_NOTIFICACION);
        return notificacion;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Notificacion.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        notificacion = createEntity(em);
    }

    @Test
    void createNotificacion() throws Exception {
        int databaseSizeBeforeCreate = notificacionRepository.findAll().collectList().block().size();
        // Create the Notificacion
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(notificacion))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Notificacion in the database
        List<Notificacion> notificacionList = notificacionRepository.findAll().collectList().block();
        assertThat(notificacionList).hasSize(databaseSizeBeforeCreate + 1);
        Notificacion testNotificacion = notificacionList.get(notificacionList.size() - 1);
        assertThat(testNotificacion.getFechaInicio()).isEqualTo(DEFAULT_FECHA_INICIO);
        assertThat(testNotificacion.getFechaActualizacion()).isEqualTo(DEFAULT_FECHA_ACTUALIZACION);
        assertThat(testNotificacion.getEstado()).isEqualTo(DEFAULT_ESTADO);
        assertThat(testNotificacion.getTipoNotificacion()).isEqualTo(DEFAULT_TIPO_NOTIFICACION);
    }

    @Test
    void createNotificacionWithExistingId() throws Exception {
        // Create the Notificacion with an existing ID
        notificacion.setId(1L);

        int databaseSizeBeforeCreate = notificacionRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(notificacion))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Notificacion in the database
        List<Notificacion> notificacionList = notificacionRepository.findAll().collectList().block();
        assertThat(notificacionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllNotificacions() {
        // Initialize the database
        notificacionRepository.save(notificacion).block();

        // Get all the notificacionList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(notificacion.getId().intValue()))
            .jsonPath("$.[*].fechaInicio")
            .value(hasItem(DEFAULT_FECHA_INICIO.toString()))
            .jsonPath("$.[*].fechaActualizacion")
            .value(hasItem(sameInstant(DEFAULT_FECHA_ACTUALIZACION)))
            .jsonPath("$.[*].estado")
            .value(hasItem(DEFAULT_ESTADO))
            .jsonPath("$.[*].tipoNotificacion")
            .value(hasItem(DEFAULT_TIPO_NOTIFICACION));
    }

    @Test
    void getNotificacion() {
        // Initialize the database
        notificacionRepository.save(notificacion).block();

        // Get the notificacion
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, notificacion.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(notificacion.getId().intValue()))
            .jsonPath("$.fechaInicio")
            .value(is(DEFAULT_FECHA_INICIO.toString()))
            .jsonPath("$.fechaActualizacion")
            .value(is(sameInstant(DEFAULT_FECHA_ACTUALIZACION)))
            .jsonPath("$.estado")
            .value(is(DEFAULT_ESTADO))
            .jsonPath("$.tipoNotificacion")
            .value(is(DEFAULT_TIPO_NOTIFICACION));
    }

    @Test
    void getNonExistingNotificacion() {
        // Get the notificacion
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewNotificacion() throws Exception {
        // Initialize the database
        notificacionRepository.save(notificacion).block();

        int databaseSizeBeforeUpdate = notificacionRepository.findAll().collectList().block().size();

        // Update the notificacion
        Notificacion updatedNotificacion = notificacionRepository.findById(notificacion.getId()).block();
        updatedNotificacion
            .fechaInicio(UPDATED_FECHA_INICIO)
            .fechaActualizacion(UPDATED_FECHA_ACTUALIZACION)
            .estado(UPDATED_ESTADO)
            .tipoNotificacion(UPDATED_TIPO_NOTIFICACION);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedNotificacion.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedNotificacion))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Notificacion in the database
        List<Notificacion> notificacionList = notificacionRepository.findAll().collectList().block();
        assertThat(notificacionList).hasSize(databaseSizeBeforeUpdate);
        Notificacion testNotificacion = notificacionList.get(notificacionList.size() - 1);
        assertThat(testNotificacion.getFechaInicio()).isEqualTo(UPDATED_FECHA_INICIO);
        assertThat(testNotificacion.getFechaActualizacion()).isEqualTo(UPDATED_FECHA_ACTUALIZACION);
        assertThat(testNotificacion.getEstado()).isEqualTo(UPDATED_ESTADO);
        assertThat(testNotificacion.getTipoNotificacion()).isEqualTo(UPDATED_TIPO_NOTIFICACION);
    }

    @Test
    void putNonExistingNotificacion() throws Exception {
        int databaseSizeBeforeUpdate = notificacionRepository.findAll().collectList().block().size();
        notificacion.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, notificacion.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(notificacion))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Notificacion in the database
        List<Notificacion> notificacionList = notificacionRepository.findAll().collectList().block();
        assertThat(notificacionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchNotificacion() throws Exception {
        int databaseSizeBeforeUpdate = notificacionRepository.findAll().collectList().block().size();
        notificacion.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(notificacion))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Notificacion in the database
        List<Notificacion> notificacionList = notificacionRepository.findAll().collectList().block();
        assertThat(notificacionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamNotificacion() throws Exception {
        int databaseSizeBeforeUpdate = notificacionRepository.findAll().collectList().block().size();
        notificacion.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(notificacion))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Notificacion in the database
        List<Notificacion> notificacionList = notificacionRepository.findAll().collectList().block();
        assertThat(notificacionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateNotificacionWithPatch() throws Exception {
        // Initialize the database
        notificacionRepository.save(notificacion).block();

        int databaseSizeBeforeUpdate = notificacionRepository.findAll().collectList().block().size();

        // Update the notificacion using partial update
        Notificacion partialUpdatedNotificacion = new Notificacion();
        partialUpdatedNotificacion.setId(notificacion.getId());

        partialUpdatedNotificacion.estado(UPDATED_ESTADO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedNotificacion.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedNotificacion))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Notificacion in the database
        List<Notificacion> notificacionList = notificacionRepository.findAll().collectList().block();
        assertThat(notificacionList).hasSize(databaseSizeBeforeUpdate);
        Notificacion testNotificacion = notificacionList.get(notificacionList.size() - 1);
        assertThat(testNotificacion.getFechaInicio()).isEqualTo(DEFAULT_FECHA_INICIO);
        assertThat(testNotificacion.getFechaActualizacion()).isEqualTo(DEFAULT_FECHA_ACTUALIZACION);
        assertThat(testNotificacion.getEstado()).isEqualTo(UPDATED_ESTADO);
        assertThat(testNotificacion.getTipoNotificacion()).isEqualTo(DEFAULT_TIPO_NOTIFICACION);
    }

    @Test
    void fullUpdateNotificacionWithPatch() throws Exception {
        // Initialize the database
        notificacionRepository.save(notificacion).block();

        int databaseSizeBeforeUpdate = notificacionRepository.findAll().collectList().block().size();

        // Update the notificacion using partial update
        Notificacion partialUpdatedNotificacion = new Notificacion();
        partialUpdatedNotificacion.setId(notificacion.getId());

        partialUpdatedNotificacion
            .fechaInicio(UPDATED_FECHA_INICIO)
            .fechaActualizacion(UPDATED_FECHA_ACTUALIZACION)
            .estado(UPDATED_ESTADO)
            .tipoNotificacion(UPDATED_TIPO_NOTIFICACION);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedNotificacion.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedNotificacion))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Notificacion in the database
        List<Notificacion> notificacionList = notificacionRepository.findAll().collectList().block();
        assertThat(notificacionList).hasSize(databaseSizeBeforeUpdate);
        Notificacion testNotificacion = notificacionList.get(notificacionList.size() - 1);
        assertThat(testNotificacion.getFechaInicio()).isEqualTo(UPDATED_FECHA_INICIO);
        assertThat(testNotificacion.getFechaActualizacion()).isEqualTo(UPDATED_FECHA_ACTUALIZACION);
        assertThat(testNotificacion.getEstado()).isEqualTo(UPDATED_ESTADO);
        assertThat(testNotificacion.getTipoNotificacion()).isEqualTo(UPDATED_TIPO_NOTIFICACION);
    }

    @Test
    void patchNonExistingNotificacion() throws Exception {
        int databaseSizeBeforeUpdate = notificacionRepository.findAll().collectList().block().size();
        notificacion.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, notificacion.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(notificacion))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Notificacion in the database
        List<Notificacion> notificacionList = notificacionRepository.findAll().collectList().block();
        assertThat(notificacionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchNotificacion() throws Exception {
        int databaseSizeBeforeUpdate = notificacionRepository.findAll().collectList().block().size();
        notificacion.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(notificacion))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Notificacion in the database
        List<Notificacion> notificacionList = notificacionRepository.findAll().collectList().block();
        assertThat(notificacionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamNotificacion() throws Exception {
        int databaseSizeBeforeUpdate = notificacionRepository.findAll().collectList().block().size();
        notificacion.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(notificacion))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Notificacion in the database
        List<Notificacion> notificacionList = notificacionRepository.findAll().collectList().block();
        assertThat(notificacionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteNotificacion() {
        // Initialize the database
        notificacionRepository.save(notificacion).block();

        int databaseSizeBeforeDelete = notificacionRepository.findAll().collectList().block().size();

        // Delete the notificacion
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, notificacion.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Notificacion> notificacionList = notificacionRepository.findAll().collectList().block();
        assertThat(notificacionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
