package com.be4tech.be4care.manilla.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.manilla.IntegrationTest;
import com.be4tech.be4care.manilla.domain.Sueno;
import com.be4tech.be4care.manilla.repository.SuenoRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link SuenoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class SuenoResourceIT {

    private static final Integer DEFAULT_SUPERFICIAL = 1;
    private static final Integer UPDATED_SUPERFICIAL = 2;

    private static final Integer DEFAULT_PROFUNDO = 1;
    private static final Integer UPDATED_PROFUNDO = 2;

    private static final Integer DEFAULT_DESPIERTO = 1;
    private static final Integer UPDATED_DESPIERTO = 2;

    private static final Instant DEFAULT_TIME_INSTANT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TIME_INSTANT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/suenos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private SuenoRepository suenoRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Sueno sueno;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sueno createEntity(EntityManager em) {
        Sueno sueno = new Sueno()
            .superficial(DEFAULT_SUPERFICIAL)
            .profundo(DEFAULT_PROFUNDO)
            .despierto(DEFAULT_DESPIERTO)
            .timeInstant(DEFAULT_TIME_INSTANT);
        return sueno;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sueno createUpdatedEntity(EntityManager em) {
        Sueno sueno = new Sueno()
            .superficial(UPDATED_SUPERFICIAL)
            .profundo(UPDATED_PROFUNDO)
            .despierto(UPDATED_DESPIERTO)
            .timeInstant(UPDATED_TIME_INSTANT);
        return sueno;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Sueno.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        sueno = createEntity(em);
    }

    @Test
    void createSueno() throws Exception {
        int databaseSizeBeforeCreate = suenoRepository.findAll().collectList().block().size();
        // Create the Sueno
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(sueno))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Sueno in the database
        List<Sueno> suenoList = suenoRepository.findAll().collectList().block();
        assertThat(suenoList).hasSize(databaseSizeBeforeCreate + 1);
        Sueno testSueno = suenoList.get(suenoList.size() - 1);
        assertThat(testSueno.getSuperficial()).isEqualTo(DEFAULT_SUPERFICIAL);
        assertThat(testSueno.getProfundo()).isEqualTo(DEFAULT_PROFUNDO);
        assertThat(testSueno.getDespierto()).isEqualTo(DEFAULT_DESPIERTO);
        assertThat(testSueno.getTimeInstant()).isEqualTo(DEFAULT_TIME_INSTANT);
    }

    @Test
    void createSuenoWithExistingId() throws Exception {
        // Create the Sueno with an existing ID
        sueno.setId(1L);

        int databaseSizeBeforeCreate = suenoRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(sueno))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Sueno in the database
        List<Sueno> suenoList = suenoRepository.findAll().collectList().block();
        assertThat(suenoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllSuenos() {
        // Initialize the database
        suenoRepository.save(sueno).block();

        // Get all the suenoList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(sueno.getId().intValue()))
            .jsonPath("$.[*].superficial")
            .value(hasItem(DEFAULT_SUPERFICIAL))
            .jsonPath("$.[*].profundo")
            .value(hasItem(DEFAULT_PROFUNDO))
            .jsonPath("$.[*].despierto")
            .value(hasItem(DEFAULT_DESPIERTO))
            .jsonPath("$.[*].timeInstant")
            .value(hasItem(DEFAULT_TIME_INSTANT.toString()));
    }

    @Test
    void getSueno() {
        // Initialize the database
        suenoRepository.save(sueno).block();

        // Get the sueno
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, sueno.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(sueno.getId().intValue()))
            .jsonPath("$.superficial")
            .value(is(DEFAULT_SUPERFICIAL))
            .jsonPath("$.profundo")
            .value(is(DEFAULT_PROFUNDO))
            .jsonPath("$.despierto")
            .value(is(DEFAULT_DESPIERTO))
            .jsonPath("$.timeInstant")
            .value(is(DEFAULT_TIME_INSTANT.toString()));
    }

    @Test
    void getNonExistingSueno() {
        // Get the sueno
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewSueno() throws Exception {
        // Initialize the database
        suenoRepository.save(sueno).block();

        int databaseSizeBeforeUpdate = suenoRepository.findAll().collectList().block().size();

        // Update the sueno
        Sueno updatedSueno = suenoRepository.findById(sueno.getId()).block();
        updatedSueno
            .superficial(UPDATED_SUPERFICIAL)
            .profundo(UPDATED_PROFUNDO)
            .despierto(UPDATED_DESPIERTO)
            .timeInstant(UPDATED_TIME_INSTANT);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedSueno.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedSueno))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Sueno in the database
        List<Sueno> suenoList = suenoRepository.findAll().collectList().block();
        assertThat(suenoList).hasSize(databaseSizeBeforeUpdate);
        Sueno testSueno = suenoList.get(suenoList.size() - 1);
        assertThat(testSueno.getSuperficial()).isEqualTo(UPDATED_SUPERFICIAL);
        assertThat(testSueno.getProfundo()).isEqualTo(UPDATED_PROFUNDO);
        assertThat(testSueno.getDespierto()).isEqualTo(UPDATED_DESPIERTO);
        assertThat(testSueno.getTimeInstant()).isEqualTo(UPDATED_TIME_INSTANT);
    }

    @Test
    void putNonExistingSueno() throws Exception {
        int databaseSizeBeforeUpdate = suenoRepository.findAll().collectList().block().size();
        sueno.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, sueno.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(sueno))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Sueno in the database
        List<Sueno> suenoList = suenoRepository.findAll().collectList().block();
        assertThat(suenoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchSueno() throws Exception {
        int databaseSizeBeforeUpdate = suenoRepository.findAll().collectList().block().size();
        sueno.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(sueno))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Sueno in the database
        List<Sueno> suenoList = suenoRepository.findAll().collectList().block();
        assertThat(suenoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamSueno() throws Exception {
        int databaseSizeBeforeUpdate = suenoRepository.findAll().collectList().block().size();
        sueno.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(sueno))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Sueno in the database
        List<Sueno> suenoList = suenoRepository.findAll().collectList().block();
        assertThat(suenoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateSuenoWithPatch() throws Exception {
        // Initialize the database
        suenoRepository.save(sueno).block();

        int databaseSizeBeforeUpdate = suenoRepository.findAll().collectList().block().size();

        // Update the sueno using partial update
        Sueno partialUpdatedSueno = new Sueno();
        partialUpdatedSueno.setId(sueno.getId());

        partialUpdatedSueno.despierto(UPDATED_DESPIERTO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedSueno.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedSueno))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Sueno in the database
        List<Sueno> suenoList = suenoRepository.findAll().collectList().block();
        assertThat(suenoList).hasSize(databaseSizeBeforeUpdate);
        Sueno testSueno = suenoList.get(suenoList.size() - 1);
        assertThat(testSueno.getSuperficial()).isEqualTo(DEFAULT_SUPERFICIAL);
        assertThat(testSueno.getProfundo()).isEqualTo(DEFAULT_PROFUNDO);
        assertThat(testSueno.getDespierto()).isEqualTo(UPDATED_DESPIERTO);
        assertThat(testSueno.getTimeInstant()).isEqualTo(DEFAULT_TIME_INSTANT);
    }

    @Test
    void fullUpdateSuenoWithPatch() throws Exception {
        // Initialize the database
        suenoRepository.save(sueno).block();

        int databaseSizeBeforeUpdate = suenoRepository.findAll().collectList().block().size();

        // Update the sueno using partial update
        Sueno partialUpdatedSueno = new Sueno();
        partialUpdatedSueno.setId(sueno.getId());

        partialUpdatedSueno
            .superficial(UPDATED_SUPERFICIAL)
            .profundo(UPDATED_PROFUNDO)
            .despierto(UPDATED_DESPIERTO)
            .timeInstant(UPDATED_TIME_INSTANT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedSueno.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedSueno))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Sueno in the database
        List<Sueno> suenoList = suenoRepository.findAll().collectList().block();
        assertThat(suenoList).hasSize(databaseSizeBeforeUpdate);
        Sueno testSueno = suenoList.get(suenoList.size() - 1);
        assertThat(testSueno.getSuperficial()).isEqualTo(UPDATED_SUPERFICIAL);
        assertThat(testSueno.getProfundo()).isEqualTo(UPDATED_PROFUNDO);
        assertThat(testSueno.getDespierto()).isEqualTo(UPDATED_DESPIERTO);
        assertThat(testSueno.getTimeInstant()).isEqualTo(UPDATED_TIME_INSTANT);
    }

    @Test
    void patchNonExistingSueno() throws Exception {
        int databaseSizeBeforeUpdate = suenoRepository.findAll().collectList().block().size();
        sueno.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, sueno.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(sueno))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Sueno in the database
        List<Sueno> suenoList = suenoRepository.findAll().collectList().block();
        assertThat(suenoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchSueno() throws Exception {
        int databaseSizeBeforeUpdate = suenoRepository.findAll().collectList().block().size();
        sueno.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(sueno))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Sueno in the database
        List<Sueno> suenoList = suenoRepository.findAll().collectList().block();
        assertThat(suenoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamSueno() throws Exception {
        int databaseSizeBeforeUpdate = suenoRepository.findAll().collectList().block().size();
        sueno.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(sueno))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Sueno in the database
        List<Sueno> suenoList = suenoRepository.findAll().collectList().block();
        assertThat(suenoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteSueno() {
        // Initialize the database
        suenoRepository.save(sueno).block();

        int databaseSizeBeforeDelete = suenoRepository.findAll().collectList().block().size();

        // Delete the sueno
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, sueno.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Sueno> suenoList = suenoRepository.findAll().collectList().block();
        assertThat(suenoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
