package com.be4tech.be4care.manilla.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.manilla.IntegrationTest;
import com.be4tech.be4care.manilla.domain.Temperatura;
import com.be4tech.be4care.manilla.repository.TemperaturaRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link TemperaturaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class TemperaturaResourceIT {

    private static final Float DEFAULT_TEMPERATURA = 1F;
    private static final Float UPDATED_TEMPERATURA = 2F;

    private static final Instant DEFAULT_FECHA_REGISTRO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_REGISTRO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/temperaturas";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private TemperaturaRepository temperaturaRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Temperatura temperatura;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Temperatura createEntity(EntityManager em) {
        Temperatura temperatura = new Temperatura().temperatura(DEFAULT_TEMPERATURA).fechaRegistro(DEFAULT_FECHA_REGISTRO);
        return temperatura;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Temperatura createUpdatedEntity(EntityManager em) {
        Temperatura temperatura = new Temperatura().temperatura(UPDATED_TEMPERATURA).fechaRegistro(UPDATED_FECHA_REGISTRO);
        return temperatura;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Temperatura.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        temperatura = createEntity(em);
    }

    @Test
    void createTemperatura() throws Exception {
        int databaseSizeBeforeCreate = temperaturaRepository.findAll().collectList().block().size();
        // Create the Temperatura
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(temperatura))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Temperatura in the database
        List<Temperatura> temperaturaList = temperaturaRepository.findAll().collectList().block();
        assertThat(temperaturaList).hasSize(databaseSizeBeforeCreate + 1);
        Temperatura testTemperatura = temperaturaList.get(temperaturaList.size() - 1);
        assertThat(testTemperatura.getTemperatura()).isEqualTo(DEFAULT_TEMPERATURA);
        assertThat(testTemperatura.getFechaRegistro()).isEqualTo(DEFAULT_FECHA_REGISTRO);
    }

    @Test
    void createTemperaturaWithExistingId() throws Exception {
        // Create the Temperatura with an existing ID
        temperatura.setId(1L);

        int databaseSizeBeforeCreate = temperaturaRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(temperatura))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Temperatura in the database
        List<Temperatura> temperaturaList = temperaturaRepository.findAll().collectList().block();
        assertThat(temperaturaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllTemperaturas() {
        // Initialize the database
        temperaturaRepository.save(temperatura).block();

        // Get all the temperaturaList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(temperatura.getId().intValue()))
            .jsonPath("$.[*].temperatura")
            .value(hasItem(DEFAULT_TEMPERATURA.doubleValue()))
            .jsonPath("$.[*].fechaRegistro")
            .value(hasItem(DEFAULT_FECHA_REGISTRO.toString()));
    }

    @Test
    void getTemperatura() {
        // Initialize the database
        temperaturaRepository.save(temperatura).block();

        // Get the temperatura
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, temperatura.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(temperatura.getId().intValue()))
            .jsonPath("$.temperatura")
            .value(is(DEFAULT_TEMPERATURA.doubleValue()))
            .jsonPath("$.fechaRegistro")
            .value(is(DEFAULT_FECHA_REGISTRO.toString()));
    }

    @Test
    void getNonExistingTemperatura() {
        // Get the temperatura
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewTemperatura() throws Exception {
        // Initialize the database
        temperaturaRepository.save(temperatura).block();

        int databaseSizeBeforeUpdate = temperaturaRepository.findAll().collectList().block().size();

        // Update the temperatura
        Temperatura updatedTemperatura = temperaturaRepository.findById(temperatura.getId()).block();
        updatedTemperatura.temperatura(UPDATED_TEMPERATURA).fechaRegistro(UPDATED_FECHA_REGISTRO);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedTemperatura.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedTemperatura))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Temperatura in the database
        List<Temperatura> temperaturaList = temperaturaRepository.findAll().collectList().block();
        assertThat(temperaturaList).hasSize(databaseSizeBeforeUpdate);
        Temperatura testTemperatura = temperaturaList.get(temperaturaList.size() - 1);
        assertThat(testTemperatura.getTemperatura()).isEqualTo(UPDATED_TEMPERATURA);
        assertThat(testTemperatura.getFechaRegistro()).isEqualTo(UPDATED_FECHA_REGISTRO);
    }

    @Test
    void putNonExistingTemperatura() throws Exception {
        int databaseSizeBeforeUpdate = temperaturaRepository.findAll().collectList().block().size();
        temperatura.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, temperatura.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(temperatura))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Temperatura in the database
        List<Temperatura> temperaturaList = temperaturaRepository.findAll().collectList().block();
        assertThat(temperaturaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchTemperatura() throws Exception {
        int databaseSizeBeforeUpdate = temperaturaRepository.findAll().collectList().block().size();
        temperatura.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(temperatura))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Temperatura in the database
        List<Temperatura> temperaturaList = temperaturaRepository.findAll().collectList().block();
        assertThat(temperaturaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamTemperatura() throws Exception {
        int databaseSizeBeforeUpdate = temperaturaRepository.findAll().collectList().block().size();
        temperatura.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(temperatura))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Temperatura in the database
        List<Temperatura> temperaturaList = temperaturaRepository.findAll().collectList().block();
        assertThat(temperaturaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateTemperaturaWithPatch() throws Exception {
        // Initialize the database
        temperaturaRepository.save(temperatura).block();

        int databaseSizeBeforeUpdate = temperaturaRepository.findAll().collectList().block().size();

        // Update the temperatura using partial update
        Temperatura partialUpdatedTemperatura = new Temperatura();
        partialUpdatedTemperatura.setId(temperatura.getId());

        partialUpdatedTemperatura.temperatura(UPDATED_TEMPERATURA);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedTemperatura.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedTemperatura))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Temperatura in the database
        List<Temperatura> temperaturaList = temperaturaRepository.findAll().collectList().block();
        assertThat(temperaturaList).hasSize(databaseSizeBeforeUpdate);
        Temperatura testTemperatura = temperaturaList.get(temperaturaList.size() - 1);
        assertThat(testTemperatura.getTemperatura()).isEqualTo(UPDATED_TEMPERATURA);
        assertThat(testTemperatura.getFechaRegistro()).isEqualTo(DEFAULT_FECHA_REGISTRO);
    }

    @Test
    void fullUpdateTemperaturaWithPatch() throws Exception {
        // Initialize the database
        temperaturaRepository.save(temperatura).block();

        int databaseSizeBeforeUpdate = temperaturaRepository.findAll().collectList().block().size();

        // Update the temperatura using partial update
        Temperatura partialUpdatedTemperatura = new Temperatura();
        partialUpdatedTemperatura.setId(temperatura.getId());

        partialUpdatedTemperatura.temperatura(UPDATED_TEMPERATURA).fechaRegistro(UPDATED_FECHA_REGISTRO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedTemperatura.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedTemperatura))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Temperatura in the database
        List<Temperatura> temperaturaList = temperaturaRepository.findAll().collectList().block();
        assertThat(temperaturaList).hasSize(databaseSizeBeforeUpdate);
        Temperatura testTemperatura = temperaturaList.get(temperaturaList.size() - 1);
        assertThat(testTemperatura.getTemperatura()).isEqualTo(UPDATED_TEMPERATURA);
        assertThat(testTemperatura.getFechaRegistro()).isEqualTo(UPDATED_FECHA_REGISTRO);
    }

    @Test
    void patchNonExistingTemperatura() throws Exception {
        int databaseSizeBeforeUpdate = temperaturaRepository.findAll().collectList().block().size();
        temperatura.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, temperatura.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(temperatura))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Temperatura in the database
        List<Temperatura> temperaturaList = temperaturaRepository.findAll().collectList().block();
        assertThat(temperaturaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchTemperatura() throws Exception {
        int databaseSizeBeforeUpdate = temperaturaRepository.findAll().collectList().block().size();
        temperatura.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(temperatura))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Temperatura in the database
        List<Temperatura> temperaturaList = temperaturaRepository.findAll().collectList().block();
        assertThat(temperaturaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamTemperatura() throws Exception {
        int databaseSizeBeforeUpdate = temperaturaRepository.findAll().collectList().block().size();
        temperatura.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(temperatura))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Temperatura in the database
        List<Temperatura> temperaturaList = temperaturaRepository.findAll().collectList().block();
        assertThat(temperaturaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteTemperatura() {
        // Initialize the database
        temperaturaRepository.save(temperatura).block();

        int databaseSizeBeforeDelete = temperaturaRepository.findAll().collectList().block().size();

        // Delete the temperatura
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, temperatura.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Temperatura> temperaturaList = temperaturaRepository.findAll().collectList().block();
        assertThat(temperaturaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
