package com.be4tech.be4care.manilla.web.rest;

import static com.be4tech.be4care.manilla.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.manilla.IntegrationTest;
import com.be4tech.be4care.manilla.domain.TokenDisp;
import com.be4tech.be4care.manilla.repository.TokenDispRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link TokenDispResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class TokenDispResourceIT {

    private static final String DEFAULT_TOKEN_CONEXION = "AAAAAAAAAA";
    private static final String UPDATED_TOKEN_CONEXION = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVO = false;
    private static final Boolean UPDATED_ACTIVO = true;

    private static final Instant DEFAULT_FECHA_INICIO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_INICIO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final ZonedDateTime DEFAULT_FECHA_FIN = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FECHA_FIN = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/token-disps";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private TokenDispRepository tokenDispRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private TokenDisp tokenDisp;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TokenDisp createEntity(EntityManager em) {
        TokenDisp tokenDisp = new TokenDisp()
            .tokenConexion(DEFAULT_TOKEN_CONEXION)
            .activo(DEFAULT_ACTIVO)
            .fechaInicio(DEFAULT_FECHA_INICIO)
            .fechaFin(DEFAULT_FECHA_FIN);
        return tokenDisp;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TokenDisp createUpdatedEntity(EntityManager em) {
        TokenDisp tokenDisp = new TokenDisp()
            .tokenConexion(UPDATED_TOKEN_CONEXION)
            .activo(UPDATED_ACTIVO)
            .fechaInicio(UPDATED_FECHA_INICIO)
            .fechaFin(UPDATED_FECHA_FIN);
        return tokenDisp;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(TokenDisp.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        tokenDisp = createEntity(em);
    }

    @Test
    void createTokenDisp() throws Exception {
        int databaseSizeBeforeCreate = tokenDispRepository.findAll().collectList().block().size();
        // Create the TokenDisp
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(tokenDisp))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the TokenDisp in the database
        List<TokenDisp> tokenDispList = tokenDispRepository.findAll().collectList().block();
        assertThat(tokenDispList).hasSize(databaseSizeBeforeCreate + 1);
        TokenDisp testTokenDisp = tokenDispList.get(tokenDispList.size() - 1);
        assertThat(testTokenDisp.getTokenConexion()).isEqualTo(DEFAULT_TOKEN_CONEXION);
        assertThat(testTokenDisp.getActivo()).isEqualTo(DEFAULT_ACTIVO);
        assertThat(testTokenDisp.getFechaInicio()).isEqualTo(DEFAULT_FECHA_INICIO);
        assertThat(testTokenDisp.getFechaFin()).isEqualTo(DEFAULT_FECHA_FIN);
    }

    @Test
    void createTokenDispWithExistingId() throws Exception {
        // Create the TokenDisp with an existing ID
        tokenDisp.setId(1L);

        int databaseSizeBeforeCreate = tokenDispRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(tokenDisp))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the TokenDisp in the database
        List<TokenDisp> tokenDispList = tokenDispRepository.findAll().collectList().block();
        assertThat(tokenDispList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllTokenDisps() {
        // Initialize the database
        tokenDispRepository.save(tokenDisp).block();

        // Get all the tokenDispList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(tokenDisp.getId().intValue()))
            .jsonPath("$.[*].tokenConexion")
            .value(hasItem(DEFAULT_TOKEN_CONEXION))
            .jsonPath("$.[*].activo")
            .value(hasItem(DEFAULT_ACTIVO.booleanValue()))
            .jsonPath("$.[*].fechaInicio")
            .value(hasItem(DEFAULT_FECHA_INICIO.toString()))
            .jsonPath("$.[*].fechaFin")
            .value(hasItem(sameInstant(DEFAULT_FECHA_FIN)));
    }

    @Test
    void getTokenDisp() {
        // Initialize the database
        tokenDispRepository.save(tokenDisp).block();

        // Get the tokenDisp
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, tokenDisp.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(tokenDisp.getId().intValue()))
            .jsonPath("$.tokenConexion")
            .value(is(DEFAULT_TOKEN_CONEXION))
            .jsonPath("$.activo")
            .value(is(DEFAULT_ACTIVO.booleanValue()))
            .jsonPath("$.fechaInicio")
            .value(is(DEFAULT_FECHA_INICIO.toString()))
            .jsonPath("$.fechaFin")
            .value(is(sameInstant(DEFAULT_FECHA_FIN)));
    }

    @Test
    void getNonExistingTokenDisp() {
        // Get the tokenDisp
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewTokenDisp() throws Exception {
        // Initialize the database
        tokenDispRepository.save(tokenDisp).block();

        int databaseSizeBeforeUpdate = tokenDispRepository.findAll().collectList().block().size();

        // Update the tokenDisp
        TokenDisp updatedTokenDisp = tokenDispRepository.findById(tokenDisp.getId()).block();
        updatedTokenDisp
            .tokenConexion(UPDATED_TOKEN_CONEXION)
            .activo(UPDATED_ACTIVO)
            .fechaInicio(UPDATED_FECHA_INICIO)
            .fechaFin(UPDATED_FECHA_FIN);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedTokenDisp.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedTokenDisp))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the TokenDisp in the database
        List<TokenDisp> tokenDispList = tokenDispRepository.findAll().collectList().block();
        assertThat(tokenDispList).hasSize(databaseSizeBeforeUpdate);
        TokenDisp testTokenDisp = tokenDispList.get(tokenDispList.size() - 1);
        assertThat(testTokenDisp.getTokenConexion()).isEqualTo(UPDATED_TOKEN_CONEXION);
        assertThat(testTokenDisp.getActivo()).isEqualTo(UPDATED_ACTIVO);
        assertThat(testTokenDisp.getFechaInicio()).isEqualTo(UPDATED_FECHA_INICIO);
        assertThat(testTokenDisp.getFechaFin()).isEqualTo(UPDATED_FECHA_FIN);
    }

    @Test
    void putNonExistingTokenDisp() throws Exception {
        int databaseSizeBeforeUpdate = tokenDispRepository.findAll().collectList().block().size();
        tokenDisp.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, tokenDisp.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(tokenDisp))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the TokenDisp in the database
        List<TokenDisp> tokenDispList = tokenDispRepository.findAll().collectList().block();
        assertThat(tokenDispList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchTokenDisp() throws Exception {
        int databaseSizeBeforeUpdate = tokenDispRepository.findAll().collectList().block().size();
        tokenDisp.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(tokenDisp))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the TokenDisp in the database
        List<TokenDisp> tokenDispList = tokenDispRepository.findAll().collectList().block();
        assertThat(tokenDispList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamTokenDisp() throws Exception {
        int databaseSizeBeforeUpdate = tokenDispRepository.findAll().collectList().block().size();
        tokenDisp.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(tokenDisp))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the TokenDisp in the database
        List<TokenDisp> tokenDispList = tokenDispRepository.findAll().collectList().block();
        assertThat(tokenDispList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateTokenDispWithPatch() throws Exception {
        // Initialize the database
        tokenDispRepository.save(tokenDisp).block();

        int databaseSizeBeforeUpdate = tokenDispRepository.findAll().collectList().block().size();

        // Update the tokenDisp using partial update
        TokenDisp partialUpdatedTokenDisp = new TokenDisp();
        partialUpdatedTokenDisp.setId(tokenDisp.getId());

        partialUpdatedTokenDisp.tokenConexion(UPDATED_TOKEN_CONEXION).fechaInicio(UPDATED_FECHA_INICIO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedTokenDisp.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedTokenDisp))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the TokenDisp in the database
        List<TokenDisp> tokenDispList = tokenDispRepository.findAll().collectList().block();
        assertThat(tokenDispList).hasSize(databaseSizeBeforeUpdate);
        TokenDisp testTokenDisp = tokenDispList.get(tokenDispList.size() - 1);
        assertThat(testTokenDisp.getTokenConexion()).isEqualTo(UPDATED_TOKEN_CONEXION);
        assertThat(testTokenDisp.getActivo()).isEqualTo(DEFAULT_ACTIVO);
        assertThat(testTokenDisp.getFechaInicio()).isEqualTo(UPDATED_FECHA_INICIO);
        assertThat(testTokenDisp.getFechaFin()).isEqualTo(DEFAULT_FECHA_FIN);
    }

    @Test
    void fullUpdateTokenDispWithPatch() throws Exception {
        // Initialize the database
        tokenDispRepository.save(tokenDisp).block();

        int databaseSizeBeforeUpdate = tokenDispRepository.findAll().collectList().block().size();

        // Update the tokenDisp using partial update
        TokenDisp partialUpdatedTokenDisp = new TokenDisp();
        partialUpdatedTokenDisp.setId(tokenDisp.getId());

        partialUpdatedTokenDisp
            .tokenConexion(UPDATED_TOKEN_CONEXION)
            .activo(UPDATED_ACTIVO)
            .fechaInicio(UPDATED_FECHA_INICIO)
            .fechaFin(UPDATED_FECHA_FIN);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedTokenDisp.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedTokenDisp))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the TokenDisp in the database
        List<TokenDisp> tokenDispList = tokenDispRepository.findAll().collectList().block();
        assertThat(tokenDispList).hasSize(databaseSizeBeforeUpdate);
        TokenDisp testTokenDisp = tokenDispList.get(tokenDispList.size() - 1);
        assertThat(testTokenDisp.getTokenConexion()).isEqualTo(UPDATED_TOKEN_CONEXION);
        assertThat(testTokenDisp.getActivo()).isEqualTo(UPDATED_ACTIVO);
        assertThat(testTokenDisp.getFechaInicio()).isEqualTo(UPDATED_FECHA_INICIO);
        assertThat(testTokenDisp.getFechaFin()).isEqualTo(UPDATED_FECHA_FIN);
    }

    @Test
    void patchNonExistingTokenDisp() throws Exception {
        int databaseSizeBeforeUpdate = tokenDispRepository.findAll().collectList().block().size();
        tokenDisp.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, tokenDisp.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(tokenDisp))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the TokenDisp in the database
        List<TokenDisp> tokenDispList = tokenDispRepository.findAll().collectList().block();
        assertThat(tokenDispList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchTokenDisp() throws Exception {
        int databaseSizeBeforeUpdate = tokenDispRepository.findAll().collectList().block().size();
        tokenDisp.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(tokenDisp))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the TokenDisp in the database
        List<TokenDisp> tokenDispList = tokenDispRepository.findAll().collectList().block();
        assertThat(tokenDispList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamTokenDisp() throws Exception {
        int databaseSizeBeforeUpdate = tokenDispRepository.findAll().collectList().block().size();
        tokenDisp.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(tokenDisp))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the TokenDisp in the database
        List<TokenDisp> tokenDispList = tokenDispRepository.findAll().collectList().block();
        assertThat(tokenDispList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteTokenDisp() {
        // Initialize the database
        tokenDispRepository.save(tokenDisp).block();

        int databaseSizeBeforeDelete = tokenDispRepository.findAll().collectList().block().size();

        // Delete the tokenDisp
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, tokenDisp.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<TokenDisp> tokenDispList = tokenDispRepository.findAll().collectList().block();
        assertThat(tokenDispList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
