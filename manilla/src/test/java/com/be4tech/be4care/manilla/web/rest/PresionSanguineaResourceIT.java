package com.be4tech.be4care.manilla.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.manilla.IntegrationTest;
import com.be4tech.be4care.manilla.domain.PresionSanguinea;
import com.be4tech.be4care.manilla.repository.PresionSanguineaRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link PresionSanguineaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class PresionSanguineaResourceIT {

    private static final Integer DEFAULT_PRESION_SANGUINEA_SISTOLICA = 1;
    private static final Integer UPDATED_PRESION_SANGUINEA_SISTOLICA = 2;

    private static final Integer DEFAULT_PRESION_SANGUINEA_DIASTOLICA = 1;
    private static final Integer UPDATED_PRESION_SANGUINEA_DIASTOLICA = 2;

    private static final Instant DEFAULT_FECHA_REGISTRO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_REGISTRO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/presion-sanguineas";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PresionSanguineaRepository presionSanguineaRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private PresionSanguinea presionSanguinea;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PresionSanguinea createEntity(EntityManager em) {
        PresionSanguinea presionSanguinea = new PresionSanguinea()
            .presionSanguineaSistolica(DEFAULT_PRESION_SANGUINEA_SISTOLICA)
            .presionSanguineaDiastolica(DEFAULT_PRESION_SANGUINEA_DIASTOLICA)
            .fechaRegistro(DEFAULT_FECHA_REGISTRO);
        return presionSanguinea;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PresionSanguinea createUpdatedEntity(EntityManager em) {
        PresionSanguinea presionSanguinea = new PresionSanguinea()
            .presionSanguineaSistolica(UPDATED_PRESION_SANGUINEA_SISTOLICA)
            .presionSanguineaDiastolica(UPDATED_PRESION_SANGUINEA_DIASTOLICA)
            .fechaRegistro(UPDATED_FECHA_REGISTRO);
        return presionSanguinea;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(PresionSanguinea.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        presionSanguinea = createEntity(em);
    }

    @Test
    void createPresionSanguinea() throws Exception {
        int databaseSizeBeforeCreate = presionSanguineaRepository.findAll().collectList().block().size();
        // Create the PresionSanguinea
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(presionSanguinea))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the PresionSanguinea in the database
        List<PresionSanguinea> presionSanguineaList = presionSanguineaRepository.findAll().collectList().block();
        assertThat(presionSanguineaList).hasSize(databaseSizeBeforeCreate + 1);
        PresionSanguinea testPresionSanguinea = presionSanguineaList.get(presionSanguineaList.size() - 1);
        assertThat(testPresionSanguinea.getPresionSanguineaSistolica()).isEqualTo(DEFAULT_PRESION_SANGUINEA_SISTOLICA);
        assertThat(testPresionSanguinea.getPresionSanguineaDiastolica()).isEqualTo(DEFAULT_PRESION_SANGUINEA_DIASTOLICA);
        assertThat(testPresionSanguinea.getFechaRegistro()).isEqualTo(DEFAULT_FECHA_REGISTRO);
    }

    @Test
    void createPresionSanguineaWithExistingId() throws Exception {
        // Create the PresionSanguinea with an existing ID
        presionSanguinea.setId(1L);

        int databaseSizeBeforeCreate = presionSanguineaRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(presionSanguinea))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the PresionSanguinea in the database
        List<PresionSanguinea> presionSanguineaList = presionSanguineaRepository.findAll().collectList().block();
        assertThat(presionSanguineaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllPresionSanguineas() {
        // Initialize the database
        presionSanguineaRepository.save(presionSanguinea).block();

        // Get all the presionSanguineaList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(presionSanguinea.getId().intValue()))
            .jsonPath("$.[*].presionSanguineaSistolica")
            .value(hasItem(DEFAULT_PRESION_SANGUINEA_SISTOLICA))
            .jsonPath("$.[*].presionSanguineaDiastolica")
            .value(hasItem(DEFAULT_PRESION_SANGUINEA_DIASTOLICA))
            .jsonPath("$.[*].fechaRegistro")
            .value(hasItem(DEFAULT_FECHA_REGISTRO.toString()));
    }

    @Test
    void getPresionSanguinea() {
        // Initialize the database
        presionSanguineaRepository.save(presionSanguinea).block();

        // Get the presionSanguinea
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, presionSanguinea.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(presionSanguinea.getId().intValue()))
            .jsonPath("$.presionSanguineaSistolica")
            .value(is(DEFAULT_PRESION_SANGUINEA_SISTOLICA))
            .jsonPath("$.presionSanguineaDiastolica")
            .value(is(DEFAULT_PRESION_SANGUINEA_DIASTOLICA))
            .jsonPath("$.fechaRegistro")
            .value(is(DEFAULT_FECHA_REGISTRO.toString()));
    }

    @Test
    void getNonExistingPresionSanguinea() {
        // Get the presionSanguinea
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewPresionSanguinea() throws Exception {
        // Initialize the database
        presionSanguineaRepository.save(presionSanguinea).block();

        int databaseSizeBeforeUpdate = presionSanguineaRepository.findAll().collectList().block().size();

        // Update the presionSanguinea
        PresionSanguinea updatedPresionSanguinea = presionSanguineaRepository.findById(presionSanguinea.getId()).block();
        updatedPresionSanguinea
            .presionSanguineaSistolica(UPDATED_PRESION_SANGUINEA_SISTOLICA)
            .presionSanguineaDiastolica(UPDATED_PRESION_SANGUINEA_DIASTOLICA)
            .fechaRegistro(UPDATED_FECHA_REGISTRO);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedPresionSanguinea.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedPresionSanguinea))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the PresionSanguinea in the database
        List<PresionSanguinea> presionSanguineaList = presionSanguineaRepository.findAll().collectList().block();
        assertThat(presionSanguineaList).hasSize(databaseSizeBeforeUpdate);
        PresionSanguinea testPresionSanguinea = presionSanguineaList.get(presionSanguineaList.size() - 1);
        assertThat(testPresionSanguinea.getPresionSanguineaSistolica()).isEqualTo(UPDATED_PRESION_SANGUINEA_SISTOLICA);
        assertThat(testPresionSanguinea.getPresionSanguineaDiastolica()).isEqualTo(UPDATED_PRESION_SANGUINEA_DIASTOLICA);
        assertThat(testPresionSanguinea.getFechaRegistro()).isEqualTo(UPDATED_FECHA_REGISTRO);
    }

    @Test
    void putNonExistingPresionSanguinea() throws Exception {
        int databaseSizeBeforeUpdate = presionSanguineaRepository.findAll().collectList().block().size();
        presionSanguinea.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, presionSanguinea.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(presionSanguinea))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the PresionSanguinea in the database
        List<PresionSanguinea> presionSanguineaList = presionSanguineaRepository.findAll().collectList().block();
        assertThat(presionSanguineaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchPresionSanguinea() throws Exception {
        int databaseSizeBeforeUpdate = presionSanguineaRepository.findAll().collectList().block().size();
        presionSanguinea.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(presionSanguinea))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the PresionSanguinea in the database
        List<PresionSanguinea> presionSanguineaList = presionSanguineaRepository.findAll().collectList().block();
        assertThat(presionSanguineaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamPresionSanguinea() throws Exception {
        int databaseSizeBeforeUpdate = presionSanguineaRepository.findAll().collectList().block().size();
        presionSanguinea.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(presionSanguinea))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the PresionSanguinea in the database
        List<PresionSanguinea> presionSanguineaList = presionSanguineaRepository.findAll().collectList().block();
        assertThat(presionSanguineaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdatePresionSanguineaWithPatch() throws Exception {
        // Initialize the database
        presionSanguineaRepository.save(presionSanguinea).block();

        int databaseSizeBeforeUpdate = presionSanguineaRepository.findAll().collectList().block().size();

        // Update the presionSanguinea using partial update
        PresionSanguinea partialUpdatedPresionSanguinea = new PresionSanguinea();
        partialUpdatedPresionSanguinea.setId(presionSanguinea.getId());

        partialUpdatedPresionSanguinea.presionSanguineaSistolica(UPDATED_PRESION_SANGUINEA_SISTOLICA);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPresionSanguinea.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPresionSanguinea))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the PresionSanguinea in the database
        List<PresionSanguinea> presionSanguineaList = presionSanguineaRepository.findAll().collectList().block();
        assertThat(presionSanguineaList).hasSize(databaseSizeBeforeUpdate);
        PresionSanguinea testPresionSanguinea = presionSanguineaList.get(presionSanguineaList.size() - 1);
        assertThat(testPresionSanguinea.getPresionSanguineaSistolica()).isEqualTo(UPDATED_PRESION_SANGUINEA_SISTOLICA);
        assertThat(testPresionSanguinea.getPresionSanguineaDiastolica()).isEqualTo(DEFAULT_PRESION_SANGUINEA_DIASTOLICA);
        assertThat(testPresionSanguinea.getFechaRegistro()).isEqualTo(DEFAULT_FECHA_REGISTRO);
    }

    @Test
    void fullUpdatePresionSanguineaWithPatch() throws Exception {
        // Initialize the database
        presionSanguineaRepository.save(presionSanguinea).block();

        int databaseSizeBeforeUpdate = presionSanguineaRepository.findAll().collectList().block().size();

        // Update the presionSanguinea using partial update
        PresionSanguinea partialUpdatedPresionSanguinea = new PresionSanguinea();
        partialUpdatedPresionSanguinea.setId(presionSanguinea.getId());

        partialUpdatedPresionSanguinea
            .presionSanguineaSistolica(UPDATED_PRESION_SANGUINEA_SISTOLICA)
            .presionSanguineaDiastolica(UPDATED_PRESION_SANGUINEA_DIASTOLICA)
            .fechaRegistro(UPDATED_FECHA_REGISTRO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPresionSanguinea.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPresionSanguinea))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the PresionSanguinea in the database
        List<PresionSanguinea> presionSanguineaList = presionSanguineaRepository.findAll().collectList().block();
        assertThat(presionSanguineaList).hasSize(databaseSizeBeforeUpdate);
        PresionSanguinea testPresionSanguinea = presionSanguineaList.get(presionSanguineaList.size() - 1);
        assertThat(testPresionSanguinea.getPresionSanguineaSistolica()).isEqualTo(UPDATED_PRESION_SANGUINEA_SISTOLICA);
        assertThat(testPresionSanguinea.getPresionSanguineaDiastolica()).isEqualTo(UPDATED_PRESION_SANGUINEA_DIASTOLICA);
        assertThat(testPresionSanguinea.getFechaRegistro()).isEqualTo(UPDATED_FECHA_REGISTRO);
    }

    @Test
    void patchNonExistingPresionSanguinea() throws Exception {
        int databaseSizeBeforeUpdate = presionSanguineaRepository.findAll().collectList().block().size();
        presionSanguinea.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, presionSanguinea.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(presionSanguinea))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the PresionSanguinea in the database
        List<PresionSanguinea> presionSanguineaList = presionSanguineaRepository.findAll().collectList().block();
        assertThat(presionSanguineaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchPresionSanguinea() throws Exception {
        int databaseSizeBeforeUpdate = presionSanguineaRepository.findAll().collectList().block().size();
        presionSanguinea.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(presionSanguinea))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the PresionSanguinea in the database
        List<PresionSanguinea> presionSanguineaList = presionSanguineaRepository.findAll().collectList().block();
        assertThat(presionSanguineaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamPresionSanguinea() throws Exception {
        int databaseSizeBeforeUpdate = presionSanguineaRepository.findAll().collectList().block().size();
        presionSanguinea.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(presionSanguinea))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the PresionSanguinea in the database
        List<PresionSanguinea> presionSanguineaList = presionSanguineaRepository.findAll().collectList().block();
        assertThat(presionSanguineaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deletePresionSanguinea() {
        // Initialize the database
        presionSanguineaRepository.save(presionSanguinea).block();

        int databaseSizeBeforeDelete = presionSanguineaRepository.findAll().collectList().block().size();

        // Delete the presionSanguinea
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, presionSanguinea.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<PresionSanguinea> presionSanguineaList = presionSanguineaRepository.findAll().collectList().block();
        assertThat(presionSanguineaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
