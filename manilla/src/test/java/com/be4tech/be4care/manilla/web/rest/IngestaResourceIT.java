package com.be4tech.be4care.manilla.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.manilla.IntegrationTest;
import com.be4tech.be4care.manilla.domain.Ingesta;
import com.be4tech.be4care.manilla.repository.IngestaRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link IngestaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class IngestaResourceIT {

    private static final String DEFAULT_TIPO = "AAAAAAAAAA";
    private static final String UPDATED_TIPO = "BBBBBBBBBB";

    private static final Integer DEFAULT_CONSUMO_CALORIAS = 1;
    private static final Integer UPDATED_CONSUMO_CALORIAS = 2;

    private static final Instant DEFAULT_FECHA_REGISTRO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_REGISTRO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/ingestas";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private IngestaRepository ingestaRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Ingesta ingesta;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Ingesta createEntity(EntityManager em) {
        Ingesta ingesta = new Ingesta().tipo(DEFAULT_TIPO).consumoCalorias(DEFAULT_CONSUMO_CALORIAS).fechaRegistro(DEFAULT_FECHA_REGISTRO);
        return ingesta;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Ingesta createUpdatedEntity(EntityManager em) {
        Ingesta ingesta = new Ingesta().tipo(UPDATED_TIPO).consumoCalorias(UPDATED_CONSUMO_CALORIAS).fechaRegistro(UPDATED_FECHA_REGISTRO);
        return ingesta;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Ingesta.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        ingesta = createEntity(em);
    }

    @Test
    void createIngesta() throws Exception {
        int databaseSizeBeforeCreate = ingestaRepository.findAll().collectList().block().size();
        // Create the Ingesta
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(ingesta))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Ingesta in the database
        List<Ingesta> ingestaList = ingestaRepository.findAll().collectList().block();
        assertThat(ingestaList).hasSize(databaseSizeBeforeCreate + 1);
        Ingesta testIngesta = ingestaList.get(ingestaList.size() - 1);
        assertThat(testIngesta.getTipo()).isEqualTo(DEFAULT_TIPO);
        assertThat(testIngesta.getConsumoCalorias()).isEqualTo(DEFAULT_CONSUMO_CALORIAS);
        assertThat(testIngesta.getFechaRegistro()).isEqualTo(DEFAULT_FECHA_REGISTRO);
    }

    @Test
    void createIngestaWithExistingId() throws Exception {
        // Create the Ingesta with an existing ID
        ingesta.setId(1L);

        int databaseSizeBeforeCreate = ingestaRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(ingesta))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Ingesta in the database
        List<Ingesta> ingestaList = ingestaRepository.findAll().collectList().block();
        assertThat(ingestaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllIngestas() {
        // Initialize the database
        ingestaRepository.save(ingesta).block();

        // Get all the ingestaList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(ingesta.getId().intValue()))
            .jsonPath("$.[*].tipo")
            .value(hasItem(DEFAULT_TIPO))
            .jsonPath("$.[*].consumoCalorias")
            .value(hasItem(DEFAULT_CONSUMO_CALORIAS))
            .jsonPath("$.[*].fechaRegistro")
            .value(hasItem(DEFAULT_FECHA_REGISTRO.toString()));
    }

    @Test
    void getIngesta() {
        // Initialize the database
        ingestaRepository.save(ingesta).block();

        // Get the ingesta
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, ingesta.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(ingesta.getId().intValue()))
            .jsonPath("$.tipo")
            .value(is(DEFAULT_TIPO))
            .jsonPath("$.consumoCalorias")
            .value(is(DEFAULT_CONSUMO_CALORIAS))
            .jsonPath("$.fechaRegistro")
            .value(is(DEFAULT_FECHA_REGISTRO.toString()));
    }

    @Test
    void getNonExistingIngesta() {
        // Get the ingesta
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewIngesta() throws Exception {
        // Initialize the database
        ingestaRepository.save(ingesta).block();

        int databaseSizeBeforeUpdate = ingestaRepository.findAll().collectList().block().size();

        // Update the ingesta
        Ingesta updatedIngesta = ingestaRepository.findById(ingesta.getId()).block();
        updatedIngesta.tipo(UPDATED_TIPO).consumoCalorias(UPDATED_CONSUMO_CALORIAS).fechaRegistro(UPDATED_FECHA_REGISTRO);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedIngesta.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedIngesta))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Ingesta in the database
        List<Ingesta> ingestaList = ingestaRepository.findAll().collectList().block();
        assertThat(ingestaList).hasSize(databaseSizeBeforeUpdate);
        Ingesta testIngesta = ingestaList.get(ingestaList.size() - 1);
        assertThat(testIngesta.getTipo()).isEqualTo(UPDATED_TIPO);
        assertThat(testIngesta.getConsumoCalorias()).isEqualTo(UPDATED_CONSUMO_CALORIAS);
        assertThat(testIngesta.getFechaRegistro()).isEqualTo(UPDATED_FECHA_REGISTRO);
    }

    @Test
    void putNonExistingIngesta() throws Exception {
        int databaseSizeBeforeUpdate = ingestaRepository.findAll().collectList().block().size();
        ingesta.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, ingesta.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(ingesta))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Ingesta in the database
        List<Ingesta> ingestaList = ingestaRepository.findAll().collectList().block();
        assertThat(ingestaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchIngesta() throws Exception {
        int databaseSizeBeforeUpdate = ingestaRepository.findAll().collectList().block().size();
        ingesta.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(ingesta))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Ingesta in the database
        List<Ingesta> ingestaList = ingestaRepository.findAll().collectList().block();
        assertThat(ingestaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamIngesta() throws Exception {
        int databaseSizeBeforeUpdate = ingestaRepository.findAll().collectList().block().size();
        ingesta.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(ingesta))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Ingesta in the database
        List<Ingesta> ingestaList = ingestaRepository.findAll().collectList().block();
        assertThat(ingestaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateIngestaWithPatch() throws Exception {
        // Initialize the database
        ingestaRepository.save(ingesta).block();

        int databaseSizeBeforeUpdate = ingestaRepository.findAll().collectList().block().size();

        // Update the ingesta using partial update
        Ingesta partialUpdatedIngesta = new Ingesta();
        partialUpdatedIngesta.setId(ingesta.getId());

        partialUpdatedIngesta.tipo(UPDATED_TIPO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedIngesta.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedIngesta))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Ingesta in the database
        List<Ingesta> ingestaList = ingestaRepository.findAll().collectList().block();
        assertThat(ingestaList).hasSize(databaseSizeBeforeUpdate);
        Ingesta testIngesta = ingestaList.get(ingestaList.size() - 1);
        assertThat(testIngesta.getTipo()).isEqualTo(UPDATED_TIPO);
        assertThat(testIngesta.getConsumoCalorias()).isEqualTo(DEFAULT_CONSUMO_CALORIAS);
        assertThat(testIngesta.getFechaRegistro()).isEqualTo(DEFAULT_FECHA_REGISTRO);
    }

    @Test
    void fullUpdateIngestaWithPatch() throws Exception {
        // Initialize the database
        ingestaRepository.save(ingesta).block();

        int databaseSizeBeforeUpdate = ingestaRepository.findAll().collectList().block().size();

        // Update the ingesta using partial update
        Ingesta partialUpdatedIngesta = new Ingesta();
        partialUpdatedIngesta.setId(ingesta.getId());

        partialUpdatedIngesta.tipo(UPDATED_TIPO).consumoCalorias(UPDATED_CONSUMO_CALORIAS).fechaRegistro(UPDATED_FECHA_REGISTRO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedIngesta.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedIngesta))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Ingesta in the database
        List<Ingesta> ingestaList = ingestaRepository.findAll().collectList().block();
        assertThat(ingestaList).hasSize(databaseSizeBeforeUpdate);
        Ingesta testIngesta = ingestaList.get(ingestaList.size() - 1);
        assertThat(testIngesta.getTipo()).isEqualTo(UPDATED_TIPO);
        assertThat(testIngesta.getConsumoCalorias()).isEqualTo(UPDATED_CONSUMO_CALORIAS);
        assertThat(testIngesta.getFechaRegistro()).isEqualTo(UPDATED_FECHA_REGISTRO);
    }

    @Test
    void patchNonExistingIngesta() throws Exception {
        int databaseSizeBeforeUpdate = ingestaRepository.findAll().collectList().block().size();
        ingesta.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, ingesta.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(ingesta))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Ingesta in the database
        List<Ingesta> ingestaList = ingestaRepository.findAll().collectList().block();
        assertThat(ingestaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchIngesta() throws Exception {
        int databaseSizeBeforeUpdate = ingestaRepository.findAll().collectList().block().size();
        ingesta.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(ingesta))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Ingesta in the database
        List<Ingesta> ingestaList = ingestaRepository.findAll().collectList().block();
        assertThat(ingestaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamIngesta() throws Exception {
        int databaseSizeBeforeUpdate = ingestaRepository.findAll().collectList().block().size();
        ingesta.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(ingesta))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Ingesta in the database
        List<Ingesta> ingestaList = ingestaRepository.findAll().collectList().block();
        assertThat(ingestaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteIngesta() {
        // Initialize the database
        ingestaRepository.save(ingesta).block();

        int databaseSizeBeforeDelete = ingestaRepository.findAll().collectList().block().size();

        // Delete the ingesta
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, ingesta.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Ingesta> ingestaList = ingestaRepository.findAll().collectList().block();
        assertThat(ingestaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
