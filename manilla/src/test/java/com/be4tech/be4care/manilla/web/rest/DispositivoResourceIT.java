package com.be4tech.be4care.manilla.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.manilla.IntegrationTest;
import com.be4tech.be4care.manilla.domain.Dispositivo;
import com.be4tech.be4care.manilla.repository.DispositivoRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.service.EntityManager;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link DispositivoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class DispositivoResourceIT {

    private static final String DEFAULT_DISPOSITIVO = "AAAAAAAAAA";
    private static final String UPDATED_DISPOSITIVO = "BBBBBBBBBB";

    private static final String DEFAULT_MAC = "AAAAAAAAAA";
    private static final String UPDATED_MAC = "BBBBBBBBBB";

    private static final Boolean DEFAULT_CONECTADO = false;
    private static final Boolean UPDATED_CONECTADO = true;

    private static final String ENTITY_API_URL = "/api/dispositivos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private DispositivoRepository dispositivoRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Dispositivo dispositivo;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Dispositivo createEntity(EntityManager em) {
        Dispositivo dispositivo = new Dispositivo().dispositivo(DEFAULT_DISPOSITIVO).mac(DEFAULT_MAC).conectado(DEFAULT_CONECTADO);
        return dispositivo;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Dispositivo createUpdatedEntity(EntityManager em) {
        Dispositivo dispositivo = new Dispositivo().dispositivo(UPDATED_DISPOSITIVO).mac(UPDATED_MAC).conectado(UPDATED_CONECTADO);
        return dispositivo;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Dispositivo.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        dispositivo = createEntity(em);
    }

    @Test
    void createDispositivo() throws Exception {
        int databaseSizeBeforeCreate = dispositivoRepository.findAll().collectList().block().size();
        // Create the Dispositivo
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(dispositivo))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Dispositivo in the database
        List<Dispositivo> dispositivoList = dispositivoRepository.findAll().collectList().block();
        assertThat(dispositivoList).hasSize(databaseSizeBeforeCreate + 1);
        Dispositivo testDispositivo = dispositivoList.get(dispositivoList.size() - 1);
        assertThat(testDispositivo.getDispositivo()).isEqualTo(DEFAULT_DISPOSITIVO);
        assertThat(testDispositivo.getMac()).isEqualTo(DEFAULT_MAC);
        assertThat(testDispositivo.getConectado()).isEqualTo(DEFAULT_CONECTADO);
    }

    @Test
    void createDispositivoWithExistingId() throws Exception {
        // Create the Dispositivo with an existing ID
        dispositivo.setId(1L);

        int databaseSizeBeforeCreate = dispositivoRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(dispositivo))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Dispositivo in the database
        List<Dispositivo> dispositivoList = dispositivoRepository.findAll().collectList().block();
        assertThat(dispositivoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllDispositivos() {
        // Initialize the database
        dispositivoRepository.save(dispositivo).block();

        // Get all the dispositivoList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(dispositivo.getId().intValue()))
            .jsonPath("$.[*].dispositivo")
            .value(hasItem(DEFAULT_DISPOSITIVO))
            .jsonPath("$.[*].mac")
            .value(hasItem(DEFAULT_MAC))
            .jsonPath("$.[*].conectado")
            .value(hasItem(DEFAULT_CONECTADO.booleanValue()));
    }

    @Test
    void getDispositivo() {
        // Initialize the database
        dispositivoRepository.save(dispositivo).block();

        // Get the dispositivo
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, dispositivo.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(dispositivo.getId().intValue()))
            .jsonPath("$.dispositivo")
            .value(is(DEFAULT_DISPOSITIVO))
            .jsonPath("$.mac")
            .value(is(DEFAULT_MAC))
            .jsonPath("$.conectado")
            .value(is(DEFAULT_CONECTADO.booleanValue()));
    }

    @Test
    void getNonExistingDispositivo() {
        // Get the dispositivo
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewDispositivo() throws Exception {
        // Initialize the database
        dispositivoRepository.save(dispositivo).block();

        int databaseSizeBeforeUpdate = dispositivoRepository.findAll().collectList().block().size();

        // Update the dispositivo
        Dispositivo updatedDispositivo = dispositivoRepository.findById(dispositivo.getId()).block();
        updatedDispositivo.dispositivo(UPDATED_DISPOSITIVO).mac(UPDATED_MAC).conectado(UPDATED_CONECTADO);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedDispositivo.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedDispositivo))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Dispositivo in the database
        List<Dispositivo> dispositivoList = dispositivoRepository.findAll().collectList().block();
        assertThat(dispositivoList).hasSize(databaseSizeBeforeUpdate);
        Dispositivo testDispositivo = dispositivoList.get(dispositivoList.size() - 1);
        assertThat(testDispositivo.getDispositivo()).isEqualTo(UPDATED_DISPOSITIVO);
        assertThat(testDispositivo.getMac()).isEqualTo(UPDATED_MAC);
        assertThat(testDispositivo.getConectado()).isEqualTo(UPDATED_CONECTADO);
    }

    @Test
    void putNonExistingDispositivo() throws Exception {
        int databaseSizeBeforeUpdate = dispositivoRepository.findAll().collectList().block().size();
        dispositivo.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, dispositivo.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(dispositivo))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Dispositivo in the database
        List<Dispositivo> dispositivoList = dispositivoRepository.findAll().collectList().block();
        assertThat(dispositivoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchDispositivo() throws Exception {
        int databaseSizeBeforeUpdate = dispositivoRepository.findAll().collectList().block().size();
        dispositivo.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(dispositivo))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Dispositivo in the database
        List<Dispositivo> dispositivoList = dispositivoRepository.findAll().collectList().block();
        assertThat(dispositivoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamDispositivo() throws Exception {
        int databaseSizeBeforeUpdate = dispositivoRepository.findAll().collectList().block().size();
        dispositivo.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(dispositivo))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Dispositivo in the database
        List<Dispositivo> dispositivoList = dispositivoRepository.findAll().collectList().block();
        assertThat(dispositivoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateDispositivoWithPatch() throws Exception {
        // Initialize the database
        dispositivoRepository.save(dispositivo).block();

        int databaseSizeBeforeUpdate = dispositivoRepository.findAll().collectList().block().size();

        // Update the dispositivo using partial update
        Dispositivo partialUpdatedDispositivo = new Dispositivo();
        partialUpdatedDispositivo.setId(dispositivo.getId());

        partialUpdatedDispositivo.conectado(UPDATED_CONECTADO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedDispositivo.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedDispositivo))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Dispositivo in the database
        List<Dispositivo> dispositivoList = dispositivoRepository.findAll().collectList().block();
        assertThat(dispositivoList).hasSize(databaseSizeBeforeUpdate);
        Dispositivo testDispositivo = dispositivoList.get(dispositivoList.size() - 1);
        assertThat(testDispositivo.getDispositivo()).isEqualTo(DEFAULT_DISPOSITIVO);
        assertThat(testDispositivo.getMac()).isEqualTo(DEFAULT_MAC);
        assertThat(testDispositivo.getConectado()).isEqualTo(UPDATED_CONECTADO);
    }

    @Test
    void fullUpdateDispositivoWithPatch() throws Exception {
        // Initialize the database
        dispositivoRepository.save(dispositivo).block();

        int databaseSizeBeforeUpdate = dispositivoRepository.findAll().collectList().block().size();

        // Update the dispositivo using partial update
        Dispositivo partialUpdatedDispositivo = new Dispositivo();
        partialUpdatedDispositivo.setId(dispositivo.getId());

        partialUpdatedDispositivo.dispositivo(UPDATED_DISPOSITIVO).mac(UPDATED_MAC).conectado(UPDATED_CONECTADO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedDispositivo.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedDispositivo))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Dispositivo in the database
        List<Dispositivo> dispositivoList = dispositivoRepository.findAll().collectList().block();
        assertThat(dispositivoList).hasSize(databaseSizeBeforeUpdate);
        Dispositivo testDispositivo = dispositivoList.get(dispositivoList.size() - 1);
        assertThat(testDispositivo.getDispositivo()).isEqualTo(UPDATED_DISPOSITIVO);
        assertThat(testDispositivo.getMac()).isEqualTo(UPDATED_MAC);
        assertThat(testDispositivo.getConectado()).isEqualTo(UPDATED_CONECTADO);
    }

    @Test
    void patchNonExistingDispositivo() throws Exception {
        int databaseSizeBeforeUpdate = dispositivoRepository.findAll().collectList().block().size();
        dispositivo.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, dispositivo.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(dispositivo))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Dispositivo in the database
        List<Dispositivo> dispositivoList = dispositivoRepository.findAll().collectList().block();
        assertThat(dispositivoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchDispositivo() throws Exception {
        int databaseSizeBeforeUpdate = dispositivoRepository.findAll().collectList().block().size();
        dispositivo.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(dispositivo))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Dispositivo in the database
        List<Dispositivo> dispositivoList = dispositivoRepository.findAll().collectList().block();
        assertThat(dispositivoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamDispositivo() throws Exception {
        int databaseSizeBeforeUpdate = dispositivoRepository.findAll().collectList().block().size();
        dispositivo.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(dispositivo))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Dispositivo in the database
        List<Dispositivo> dispositivoList = dispositivoRepository.findAll().collectList().block();
        assertThat(dispositivoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteDispositivo() {
        // Initialize the database
        dispositivoRepository.save(dispositivo).block();

        int databaseSizeBeforeDelete = dispositivoRepository.findAll().collectList().block().size();

        // Delete the dispositivo
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, dispositivo.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Dispositivo> dispositivoList = dispositivoRepository.findAll().collectList().block();
        assertThat(dispositivoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
