package com.be4tech.be4care.manilla.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.manilla.IntegrationTest;
import com.be4tech.be4care.manilla.domain.Caloria;
import com.be4tech.be4care.manilla.repository.CaloriaRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link CaloriaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class CaloriaResourceIT {

    private static final Integer DEFAULT_CALORIAS_ACTIVAS = 1;
    private static final Integer UPDATED_CALORIAS_ACTIVAS = 2;

    private static final String DEFAULT_DESCRIPCION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPCION = "BBBBBBBBBB";

    private static final Instant DEFAULT_FECHA_REGISTRO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_REGISTRO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/calorias";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CaloriaRepository caloriaRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Caloria caloria;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Caloria createEntity(EntityManager em) {
        Caloria caloria = new Caloria()
            .caloriasActivas(DEFAULT_CALORIAS_ACTIVAS)
            .descripcion(DEFAULT_DESCRIPCION)
            .fechaRegistro(DEFAULT_FECHA_REGISTRO);
        return caloria;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Caloria createUpdatedEntity(EntityManager em) {
        Caloria caloria = new Caloria()
            .caloriasActivas(UPDATED_CALORIAS_ACTIVAS)
            .descripcion(UPDATED_DESCRIPCION)
            .fechaRegistro(UPDATED_FECHA_REGISTRO);
        return caloria;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Caloria.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        caloria = createEntity(em);
    }

    @Test
    void createCaloria() throws Exception {
        int databaseSizeBeforeCreate = caloriaRepository.findAll().collectList().block().size();
        // Create the Caloria
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(caloria))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Caloria in the database
        List<Caloria> caloriaList = caloriaRepository.findAll().collectList().block();
        assertThat(caloriaList).hasSize(databaseSizeBeforeCreate + 1);
        Caloria testCaloria = caloriaList.get(caloriaList.size() - 1);
        assertThat(testCaloria.getCaloriasActivas()).isEqualTo(DEFAULT_CALORIAS_ACTIVAS);
        assertThat(testCaloria.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
        assertThat(testCaloria.getFechaRegistro()).isEqualTo(DEFAULT_FECHA_REGISTRO);
    }

    @Test
    void createCaloriaWithExistingId() throws Exception {
        // Create the Caloria with an existing ID
        caloria.setId(1L);

        int databaseSizeBeforeCreate = caloriaRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(caloria))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Caloria in the database
        List<Caloria> caloriaList = caloriaRepository.findAll().collectList().block();
        assertThat(caloriaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllCalorias() {
        // Initialize the database
        caloriaRepository.save(caloria).block();

        // Get all the caloriaList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(caloria.getId().intValue()))
            .jsonPath("$.[*].caloriasActivas")
            .value(hasItem(DEFAULT_CALORIAS_ACTIVAS))
            .jsonPath("$.[*].descripcion")
            .value(hasItem(DEFAULT_DESCRIPCION))
            .jsonPath("$.[*].fechaRegistro")
            .value(hasItem(DEFAULT_FECHA_REGISTRO.toString()));
    }

    @Test
    void getCaloria() {
        // Initialize the database
        caloriaRepository.save(caloria).block();

        // Get the caloria
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, caloria.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(caloria.getId().intValue()))
            .jsonPath("$.caloriasActivas")
            .value(is(DEFAULT_CALORIAS_ACTIVAS))
            .jsonPath("$.descripcion")
            .value(is(DEFAULT_DESCRIPCION))
            .jsonPath("$.fechaRegistro")
            .value(is(DEFAULT_FECHA_REGISTRO.toString()));
    }

    @Test
    void getNonExistingCaloria() {
        // Get the caloria
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewCaloria() throws Exception {
        // Initialize the database
        caloriaRepository.save(caloria).block();

        int databaseSizeBeforeUpdate = caloriaRepository.findAll().collectList().block().size();

        // Update the caloria
        Caloria updatedCaloria = caloriaRepository.findById(caloria.getId()).block();
        updatedCaloria.caloriasActivas(UPDATED_CALORIAS_ACTIVAS).descripcion(UPDATED_DESCRIPCION).fechaRegistro(UPDATED_FECHA_REGISTRO);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedCaloria.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedCaloria))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Caloria in the database
        List<Caloria> caloriaList = caloriaRepository.findAll().collectList().block();
        assertThat(caloriaList).hasSize(databaseSizeBeforeUpdate);
        Caloria testCaloria = caloriaList.get(caloriaList.size() - 1);
        assertThat(testCaloria.getCaloriasActivas()).isEqualTo(UPDATED_CALORIAS_ACTIVAS);
        assertThat(testCaloria.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testCaloria.getFechaRegistro()).isEqualTo(UPDATED_FECHA_REGISTRO);
    }

    @Test
    void putNonExistingCaloria() throws Exception {
        int databaseSizeBeforeUpdate = caloriaRepository.findAll().collectList().block().size();
        caloria.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, caloria.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(caloria))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Caloria in the database
        List<Caloria> caloriaList = caloriaRepository.findAll().collectList().block();
        assertThat(caloriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchCaloria() throws Exception {
        int databaseSizeBeforeUpdate = caloriaRepository.findAll().collectList().block().size();
        caloria.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(caloria))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Caloria in the database
        List<Caloria> caloriaList = caloriaRepository.findAll().collectList().block();
        assertThat(caloriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamCaloria() throws Exception {
        int databaseSizeBeforeUpdate = caloriaRepository.findAll().collectList().block().size();
        caloria.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(caloria))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Caloria in the database
        List<Caloria> caloriaList = caloriaRepository.findAll().collectList().block();
        assertThat(caloriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateCaloriaWithPatch() throws Exception {
        // Initialize the database
        caloriaRepository.save(caloria).block();

        int databaseSizeBeforeUpdate = caloriaRepository.findAll().collectList().block().size();

        // Update the caloria using partial update
        Caloria partialUpdatedCaloria = new Caloria();
        partialUpdatedCaloria.setId(caloria.getId());

        partialUpdatedCaloria.descripcion(UPDATED_DESCRIPCION);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedCaloria.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedCaloria))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Caloria in the database
        List<Caloria> caloriaList = caloriaRepository.findAll().collectList().block();
        assertThat(caloriaList).hasSize(databaseSizeBeforeUpdate);
        Caloria testCaloria = caloriaList.get(caloriaList.size() - 1);
        assertThat(testCaloria.getCaloriasActivas()).isEqualTo(DEFAULT_CALORIAS_ACTIVAS);
        assertThat(testCaloria.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testCaloria.getFechaRegistro()).isEqualTo(DEFAULT_FECHA_REGISTRO);
    }

    @Test
    void fullUpdateCaloriaWithPatch() throws Exception {
        // Initialize the database
        caloriaRepository.save(caloria).block();

        int databaseSizeBeforeUpdate = caloriaRepository.findAll().collectList().block().size();

        // Update the caloria using partial update
        Caloria partialUpdatedCaloria = new Caloria();
        partialUpdatedCaloria.setId(caloria.getId());

        partialUpdatedCaloria
            .caloriasActivas(UPDATED_CALORIAS_ACTIVAS)
            .descripcion(UPDATED_DESCRIPCION)
            .fechaRegistro(UPDATED_FECHA_REGISTRO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedCaloria.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedCaloria))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Caloria in the database
        List<Caloria> caloriaList = caloriaRepository.findAll().collectList().block();
        assertThat(caloriaList).hasSize(databaseSizeBeforeUpdate);
        Caloria testCaloria = caloriaList.get(caloriaList.size() - 1);
        assertThat(testCaloria.getCaloriasActivas()).isEqualTo(UPDATED_CALORIAS_ACTIVAS);
        assertThat(testCaloria.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testCaloria.getFechaRegistro()).isEqualTo(UPDATED_FECHA_REGISTRO);
    }

    @Test
    void patchNonExistingCaloria() throws Exception {
        int databaseSizeBeforeUpdate = caloriaRepository.findAll().collectList().block().size();
        caloria.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, caloria.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(caloria))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Caloria in the database
        List<Caloria> caloriaList = caloriaRepository.findAll().collectList().block();
        assertThat(caloriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchCaloria() throws Exception {
        int databaseSizeBeforeUpdate = caloriaRepository.findAll().collectList().block().size();
        caloria.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(caloria))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Caloria in the database
        List<Caloria> caloriaList = caloriaRepository.findAll().collectList().block();
        assertThat(caloriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamCaloria() throws Exception {
        int databaseSizeBeforeUpdate = caloriaRepository.findAll().collectList().block().size();
        caloria.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(caloria))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Caloria in the database
        List<Caloria> caloriaList = caloriaRepository.findAll().collectList().block();
        assertThat(caloriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteCaloria() {
        // Initialize the database
        caloriaRepository.save(caloria).block();

        int databaseSizeBeforeDelete = caloriaRepository.findAll().collectList().block().size();

        // Delete the caloria
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, caloria.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Caloria> caloriaList = caloriaRepository.findAll().collectList().block();
        assertThat(caloriaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
