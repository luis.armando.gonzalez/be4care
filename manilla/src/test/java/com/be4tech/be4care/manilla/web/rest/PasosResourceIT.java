package com.be4tech.be4care.manilla.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.manilla.IntegrationTest;
import com.be4tech.be4care.manilla.domain.Pasos;
import com.be4tech.be4care.manilla.repository.PasosRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link PasosResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class PasosResourceIT {

    private static final Integer DEFAULT_NRO_PASOS = 1;
    private static final Integer UPDATED_NRO_PASOS = 2;

    private static final Instant DEFAULT_TIME_INSTANT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TIME_INSTANT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/pasos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PasosRepository pasosRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Pasos pasos;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pasos createEntity(EntityManager em) {
        Pasos pasos = new Pasos().nroPasos(DEFAULT_NRO_PASOS).timeInstant(DEFAULT_TIME_INSTANT);
        return pasos;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pasos createUpdatedEntity(EntityManager em) {
        Pasos pasos = new Pasos().nroPasos(UPDATED_NRO_PASOS).timeInstant(UPDATED_TIME_INSTANT);
        return pasos;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Pasos.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        pasos = createEntity(em);
    }

    @Test
    void createPasos() throws Exception {
        int databaseSizeBeforeCreate = pasosRepository.findAll().collectList().block().size();
        // Create the Pasos
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(pasos))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Pasos in the database
        List<Pasos> pasosList = pasosRepository.findAll().collectList().block();
        assertThat(pasosList).hasSize(databaseSizeBeforeCreate + 1);
        Pasos testPasos = pasosList.get(pasosList.size() - 1);
        assertThat(testPasos.getNroPasos()).isEqualTo(DEFAULT_NRO_PASOS);
        assertThat(testPasos.getTimeInstant()).isEqualTo(DEFAULT_TIME_INSTANT);
    }

    @Test
    void createPasosWithExistingId() throws Exception {
        // Create the Pasos with an existing ID
        pasos.setId(1L);

        int databaseSizeBeforeCreate = pasosRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(pasos))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Pasos in the database
        List<Pasos> pasosList = pasosRepository.findAll().collectList().block();
        assertThat(pasosList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllPasos() {
        // Initialize the database
        pasosRepository.save(pasos).block();

        // Get all the pasosList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(pasos.getId().intValue()))
            .jsonPath("$.[*].nroPasos")
            .value(hasItem(DEFAULT_NRO_PASOS))
            .jsonPath("$.[*].timeInstant")
            .value(hasItem(DEFAULT_TIME_INSTANT.toString()));
    }

    @Test
    void getPasos() {
        // Initialize the database
        pasosRepository.save(pasos).block();

        // Get the pasos
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, pasos.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(pasos.getId().intValue()))
            .jsonPath("$.nroPasos")
            .value(is(DEFAULT_NRO_PASOS))
            .jsonPath("$.timeInstant")
            .value(is(DEFAULT_TIME_INSTANT.toString()));
    }

    @Test
    void getNonExistingPasos() {
        // Get the pasos
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewPasos() throws Exception {
        // Initialize the database
        pasosRepository.save(pasos).block();

        int databaseSizeBeforeUpdate = pasosRepository.findAll().collectList().block().size();

        // Update the pasos
        Pasos updatedPasos = pasosRepository.findById(pasos.getId()).block();
        updatedPasos.nroPasos(UPDATED_NRO_PASOS).timeInstant(UPDATED_TIME_INSTANT);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedPasos.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedPasos))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Pasos in the database
        List<Pasos> pasosList = pasosRepository.findAll().collectList().block();
        assertThat(pasosList).hasSize(databaseSizeBeforeUpdate);
        Pasos testPasos = pasosList.get(pasosList.size() - 1);
        assertThat(testPasos.getNroPasos()).isEqualTo(UPDATED_NRO_PASOS);
        assertThat(testPasos.getTimeInstant()).isEqualTo(UPDATED_TIME_INSTANT);
    }

    @Test
    void putNonExistingPasos() throws Exception {
        int databaseSizeBeforeUpdate = pasosRepository.findAll().collectList().block().size();
        pasos.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, pasos.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(pasos))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Pasos in the database
        List<Pasos> pasosList = pasosRepository.findAll().collectList().block();
        assertThat(pasosList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchPasos() throws Exception {
        int databaseSizeBeforeUpdate = pasosRepository.findAll().collectList().block().size();
        pasos.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(pasos))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Pasos in the database
        List<Pasos> pasosList = pasosRepository.findAll().collectList().block();
        assertThat(pasosList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamPasos() throws Exception {
        int databaseSizeBeforeUpdate = pasosRepository.findAll().collectList().block().size();
        pasos.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(pasos))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Pasos in the database
        List<Pasos> pasosList = pasosRepository.findAll().collectList().block();
        assertThat(pasosList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdatePasosWithPatch() throws Exception {
        // Initialize the database
        pasosRepository.save(pasos).block();

        int databaseSizeBeforeUpdate = pasosRepository.findAll().collectList().block().size();

        // Update the pasos using partial update
        Pasos partialUpdatedPasos = new Pasos();
        partialUpdatedPasos.setId(pasos.getId());

        partialUpdatedPasos.nroPasos(UPDATED_NRO_PASOS).timeInstant(UPDATED_TIME_INSTANT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPasos.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPasos))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Pasos in the database
        List<Pasos> pasosList = pasosRepository.findAll().collectList().block();
        assertThat(pasosList).hasSize(databaseSizeBeforeUpdate);
        Pasos testPasos = pasosList.get(pasosList.size() - 1);
        assertThat(testPasos.getNroPasos()).isEqualTo(UPDATED_NRO_PASOS);
        assertThat(testPasos.getTimeInstant()).isEqualTo(UPDATED_TIME_INSTANT);
    }

    @Test
    void fullUpdatePasosWithPatch() throws Exception {
        // Initialize the database
        pasosRepository.save(pasos).block();

        int databaseSizeBeforeUpdate = pasosRepository.findAll().collectList().block().size();

        // Update the pasos using partial update
        Pasos partialUpdatedPasos = new Pasos();
        partialUpdatedPasos.setId(pasos.getId());

        partialUpdatedPasos.nroPasos(UPDATED_NRO_PASOS).timeInstant(UPDATED_TIME_INSTANT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPasos.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPasos))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Pasos in the database
        List<Pasos> pasosList = pasosRepository.findAll().collectList().block();
        assertThat(pasosList).hasSize(databaseSizeBeforeUpdate);
        Pasos testPasos = pasosList.get(pasosList.size() - 1);
        assertThat(testPasos.getNroPasos()).isEqualTo(UPDATED_NRO_PASOS);
        assertThat(testPasos.getTimeInstant()).isEqualTo(UPDATED_TIME_INSTANT);
    }

    @Test
    void patchNonExistingPasos() throws Exception {
        int databaseSizeBeforeUpdate = pasosRepository.findAll().collectList().block().size();
        pasos.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, pasos.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(pasos))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Pasos in the database
        List<Pasos> pasosList = pasosRepository.findAll().collectList().block();
        assertThat(pasosList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchPasos() throws Exception {
        int databaseSizeBeforeUpdate = pasosRepository.findAll().collectList().block().size();
        pasos.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(pasos))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Pasos in the database
        List<Pasos> pasosList = pasosRepository.findAll().collectList().block();
        assertThat(pasosList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamPasos() throws Exception {
        int databaseSizeBeforeUpdate = pasosRepository.findAll().collectList().block().size();
        pasos.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(pasos))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Pasos in the database
        List<Pasos> pasosList = pasosRepository.findAll().collectList().block();
        assertThat(pasosList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deletePasos() {
        // Initialize the database
        pasosRepository.save(pasos).block();

        int databaseSizeBeforeDelete = pasosRepository.findAll().collectList().block().size();

        // Delete the pasos
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, pasos.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Pasos> pasosList = pasosRepository.findAll().collectList().block();
        assertThat(pasosList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
