package com.be4tech.be4care.manilla.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.manilla.IntegrationTest;
import com.be4tech.be4care.manilla.domain.Encuesta;
import com.be4tech.be4care.manilla.repository.EncuestaRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link EncuestaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class EncuestaResourceIT {

    private static final Instant DEFAULT_FECHA = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_DEBILIDAD = false;
    private static final Boolean UPDATED_DEBILIDAD = true;

    private static final Boolean DEFAULT_CEFALEA = false;
    private static final Boolean UPDATED_CEFALEA = true;

    private static final Boolean DEFAULT_CALAMBRES = false;
    private static final Boolean UPDATED_CALAMBRES = true;

    private static final Boolean DEFAULT_NAUSEAS = false;
    private static final Boolean UPDATED_NAUSEAS = true;

    private static final Boolean DEFAULT_VOMITO = false;
    private static final Boolean UPDATED_VOMITO = true;

    private static final Boolean DEFAULT_MAREO = false;
    private static final Boolean UPDATED_MAREO = true;

    private static final Boolean DEFAULT_NINGUNA = false;
    private static final Boolean UPDATED_NINGUNA = true;

    private static final String ENTITY_API_URL = "/api/encuestas";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private EncuestaRepository encuestaRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Encuesta encuesta;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Encuesta createEntity(EntityManager em) {
        Encuesta encuesta = new Encuesta()
            .fecha(DEFAULT_FECHA)
            .debilidad(DEFAULT_DEBILIDAD)
            .cefalea(DEFAULT_CEFALEA)
            .calambres(DEFAULT_CALAMBRES)
            .nauseas(DEFAULT_NAUSEAS)
            .vomito(DEFAULT_VOMITO)
            .mareo(DEFAULT_MAREO)
            .ninguna(DEFAULT_NINGUNA);
        return encuesta;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Encuesta createUpdatedEntity(EntityManager em) {
        Encuesta encuesta = new Encuesta()
            .fecha(UPDATED_FECHA)
            .debilidad(UPDATED_DEBILIDAD)
            .cefalea(UPDATED_CEFALEA)
            .calambres(UPDATED_CALAMBRES)
            .nauseas(UPDATED_NAUSEAS)
            .vomito(UPDATED_VOMITO)
            .mareo(UPDATED_MAREO)
            .ninguna(UPDATED_NINGUNA);
        return encuesta;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Encuesta.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        encuesta = createEntity(em);
    }

    @Test
    void createEncuesta() throws Exception {
        int databaseSizeBeforeCreate = encuestaRepository.findAll().collectList().block().size();
        // Create the Encuesta
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(encuesta))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Encuesta in the database
        List<Encuesta> encuestaList = encuestaRepository.findAll().collectList().block();
        assertThat(encuestaList).hasSize(databaseSizeBeforeCreate + 1);
        Encuesta testEncuesta = encuestaList.get(encuestaList.size() - 1);
        assertThat(testEncuesta.getFecha()).isEqualTo(DEFAULT_FECHA);
        assertThat(testEncuesta.getDebilidad()).isEqualTo(DEFAULT_DEBILIDAD);
        assertThat(testEncuesta.getCefalea()).isEqualTo(DEFAULT_CEFALEA);
        assertThat(testEncuesta.getCalambres()).isEqualTo(DEFAULT_CALAMBRES);
        assertThat(testEncuesta.getNauseas()).isEqualTo(DEFAULT_NAUSEAS);
        assertThat(testEncuesta.getVomito()).isEqualTo(DEFAULT_VOMITO);
        assertThat(testEncuesta.getMareo()).isEqualTo(DEFAULT_MAREO);
        assertThat(testEncuesta.getNinguna()).isEqualTo(DEFAULT_NINGUNA);
    }

    @Test
    void createEncuestaWithExistingId() throws Exception {
        // Create the Encuesta with an existing ID
        encuesta.setId(1L);

        int databaseSizeBeforeCreate = encuestaRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(encuesta))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Encuesta in the database
        List<Encuesta> encuestaList = encuestaRepository.findAll().collectList().block();
        assertThat(encuestaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllEncuestas() {
        // Initialize the database
        encuestaRepository.save(encuesta).block();

        // Get all the encuestaList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(encuesta.getId().intValue()))
            .jsonPath("$.[*].fecha")
            .value(hasItem(DEFAULT_FECHA.toString()))
            .jsonPath("$.[*].debilidad")
            .value(hasItem(DEFAULT_DEBILIDAD.booleanValue()))
            .jsonPath("$.[*].cefalea")
            .value(hasItem(DEFAULT_CEFALEA.booleanValue()))
            .jsonPath("$.[*].calambres")
            .value(hasItem(DEFAULT_CALAMBRES.booleanValue()))
            .jsonPath("$.[*].nauseas")
            .value(hasItem(DEFAULT_NAUSEAS.booleanValue()))
            .jsonPath("$.[*].vomito")
            .value(hasItem(DEFAULT_VOMITO.booleanValue()))
            .jsonPath("$.[*].mareo")
            .value(hasItem(DEFAULT_MAREO.booleanValue()))
            .jsonPath("$.[*].ninguna")
            .value(hasItem(DEFAULT_NINGUNA.booleanValue()));
    }

    @Test
    void getEncuesta() {
        // Initialize the database
        encuestaRepository.save(encuesta).block();

        // Get the encuesta
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, encuesta.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(encuesta.getId().intValue()))
            .jsonPath("$.fecha")
            .value(is(DEFAULT_FECHA.toString()))
            .jsonPath("$.debilidad")
            .value(is(DEFAULT_DEBILIDAD.booleanValue()))
            .jsonPath("$.cefalea")
            .value(is(DEFAULT_CEFALEA.booleanValue()))
            .jsonPath("$.calambres")
            .value(is(DEFAULT_CALAMBRES.booleanValue()))
            .jsonPath("$.nauseas")
            .value(is(DEFAULT_NAUSEAS.booleanValue()))
            .jsonPath("$.vomito")
            .value(is(DEFAULT_VOMITO.booleanValue()))
            .jsonPath("$.mareo")
            .value(is(DEFAULT_MAREO.booleanValue()))
            .jsonPath("$.ninguna")
            .value(is(DEFAULT_NINGUNA.booleanValue()));
    }

    @Test
    void getNonExistingEncuesta() {
        // Get the encuesta
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewEncuesta() throws Exception {
        // Initialize the database
        encuestaRepository.save(encuesta).block();

        int databaseSizeBeforeUpdate = encuestaRepository.findAll().collectList().block().size();

        // Update the encuesta
        Encuesta updatedEncuesta = encuestaRepository.findById(encuesta.getId()).block();
        updatedEncuesta
            .fecha(UPDATED_FECHA)
            .debilidad(UPDATED_DEBILIDAD)
            .cefalea(UPDATED_CEFALEA)
            .calambres(UPDATED_CALAMBRES)
            .nauseas(UPDATED_NAUSEAS)
            .vomito(UPDATED_VOMITO)
            .mareo(UPDATED_MAREO)
            .ninguna(UPDATED_NINGUNA);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedEncuesta.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedEncuesta))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Encuesta in the database
        List<Encuesta> encuestaList = encuestaRepository.findAll().collectList().block();
        assertThat(encuestaList).hasSize(databaseSizeBeforeUpdate);
        Encuesta testEncuesta = encuestaList.get(encuestaList.size() - 1);
        assertThat(testEncuesta.getFecha()).isEqualTo(UPDATED_FECHA);
        assertThat(testEncuesta.getDebilidad()).isEqualTo(UPDATED_DEBILIDAD);
        assertThat(testEncuesta.getCefalea()).isEqualTo(UPDATED_CEFALEA);
        assertThat(testEncuesta.getCalambres()).isEqualTo(UPDATED_CALAMBRES);
        assertThat(testEncuesta.getNauseas()).isEqualTo(UPDATED_NAUSEAS);
        assertThat(testEncuesta.getVomito()).isEqualTo(UPDATED_VOMITO);
        assertThat(testEncuesta.getMareo()).isEqualTo(UPDATED_MAREO);
        assertThat(testEncuesta.getNinguna()).isEqualTo(UPDATED_NINGUNA);
    }

    @Test
    void putNonExistingEncuesta() throws Exception {
        int databaseSizeBeforeUpdate = encuestaRepository.findAll().collectList().block().size();
        encuesta.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, encuesta.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(encuesta))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Encuesta in the database
        List<Encuesta> encuestaList = encuestaRepository.findAll().collectList().block();
        assertThat(encuestaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchEncuesta() throws Exception {
        int databaseSizeBeforeUpdate = encuestaRepository.findAll().collectList().block().size();
        encuesta.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(encuesta))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Encuesta in the database
        List<Encuesta> encuestaList = encuestaRepository.findAll().collectList().block();
        assertThat(encuestaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamEncuesta() throws Exception {
        int databaseSizeBeforeUpdate = encuestaRepository.findAll().collectList().block().size();
        encuesta.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(encuesta))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Encuesta in the database
        List<Encuesta> encuestaList = encuestaRepository.findAll().collectList().block();
        assertThat(encuestaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateEncuestaWithPatch() throws Exception {
        // Initialize the database
        encuestaRepository.save(encuesta).block();

        int databaseSizeBeforeUpdate = encuestaRepository.findAll().collectList().block().size();

        // Update the encuesta using partial update
        Encuesta partialUpdatedEncuesta = new Encuesta();
        partialUpdatedEncuesta.setId(encuesta.getId());

        partialUpdatedEncuesta.calambres(UPDATED_CALAMBRES).nauseas(UPDATED_NAUSEAS).vomito(UPDATED_VOMITO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedEncuesta.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedEncuesta))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Encuesta in the database
        List<Encuesta> encuestaList = encuestaRepository.findAll().collectList().block();
        assertThat(encuestaList).hasSize(databaseSizeBeforeUpdate);
        Encuesta testEncuesta = encuestaList.get(encuestaList.size() - 1);
        assertThat(testEncuesta.getFecha()).isEqualTo(DEFAULT_FECHA);
        assertThat(testEncuesta.getDebilidad()).isEqualTo(DEFAULT_DEBILIDAD);
        assertThat(testEncuesta.getCefalea()).isEqualTo(DEFAULT_CEFALEA);
        assertThat(testEncuesta.getCalambres()).isEqualTo(UPDATED_CALAMBRES);
        assertThat(testEncuesta.getNauseas()).isEqualTo(UPDATED_NAUSEAS);
        assertThat(testEncuesta.getVomito()).isEqualTo(UPDATED_VOMITO);
        assertThat(testEncuesta.getMareo()).isEqualTo(DEFAULT_MAREO);
        assertThat(testEncuesta.getNinguna()).isEqualTo(DEFAULT_NINGUNA);
    }

    @Test
    void fullUpdateEncuestaWithPatch() throws Exception {
        // Initialize the database
        encuestaRepository.save(encuesta).block();

        int databaseSizeBeforeUpdate = encuestaRepository.findAll().collectList().block().size();

        // Update the encuesta using partial update
        Encuesta partialUpdatedEncuesta = new Encuesta();
        partialUpdatedEncuesta.setId(encuesta.getId());

        partialUpdatedEncuesta
            .fecha(UPDATED_FECHA)
            .debilidad(UPDATED_DEBILIDAD)
            .cefalea(UPDATED_CEFALEA)
            .calambres(UPDATED_CALAMBRES)
            .nauseas(UPDATED_NAUSEAS)
            .vomito(UPDATED_VOMITO)
            .mareo(UPDATED_MAREO)
            .ninguna(UPDATED_NINGUNA);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedEncuesta.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedEncuesta))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Encuesta in the database
        List<Encuesta> encuestaList = encuestaRepository.findAll().collectList().block();
        assertThat(encuestaList).hasSize(databaseSizeBeforeUpdate);
        Encuesta testEncuesta = encuestaList.get(encuestaList.size() - 1);
        assertThat(testEncuesta.getFecha()).isEqualTo(UPDATED_FECHA);
        assertThat(testEncuesta.getDebilidad()).isEqualTo(UPDATED_DEBILIDAD);
        assertThat(testEncuesta.getCefalea()).isEqualTo(UPDATED_CEFALEA);
        assertThat(testEncuesta.getCalambres()).isEqualTo(UPDATED_CALAMBRES);
        assertThat(testEncuesta.getNauseas()).isEqualTo(UPDATED_NAUSEAS);
        assertThat(testEncuesta.getVomito()).isEqualTo(UPDATED_VOMITO);
        assertThat(testEncuesta.getMareo()).isEqualTo(UPDATED_MAREO);
        assertThat(testEncuesta.getNinguna()).isEqualTo(UPDATED_NINGUNA);
    }

    @Test
    void patchNonExistingEncuesta() throws Exception {
        int databaseSizeBeforeUpdate = encuestaRepository.findAll().collectList().block().size();
        encuesta.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, encuesta.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(encuesta))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Encuesta in the database
        List<Encuesta> encuestaList = encuestaRepository.findAll().collectList().block();
        assertThat(encuestaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchEncuesta() throws Exception {
        int databaseSizeBeforeUpdate = encuestaRepository.findAll().collectList().block().size();
        encuesta.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(encuesta))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Encuesta in the database
        List<Encuesta> encuestaList = encuestaRepository.findAll().collectList().block();
        assertThat(encuestaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamEncuesta() throws Exception {
        int databaseSizeBeforeUpdate = encuestaRepository.findAll().collectList().block().size();
        encuesta.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(encuesta))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Encuesta in the database
        List<Encuesta> encuestaList = encuestaRepository.findAll().collectList().block();
        assertThat(encuestaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteEncuesta() {
        // Initialize the database
        encuestaRepository.save(encuesta).block();

        int databaseSizeBeforeDelete = encuestaRepository.findAll().collectList().block().size();

        // Delete the encuesta
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, encuesta.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Encuesta> encuestaList = encuestaRepository.findAll().collectList().block();
        assertThat(encuestaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
