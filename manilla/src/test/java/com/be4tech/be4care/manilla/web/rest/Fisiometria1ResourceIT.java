package com.be4tech.be4care.manilla.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.manilla.IntegrationTest;
import com.be4tech.be4care.manilla.domain.Fisiometria1;
import com.be4tech.be4care.manilla.repository.Fisiometria1Repository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link Fisiometria1Resource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class Fisiometria1ResourceIT {

    private static final Integer DEFAULT_RITMO_CARDIACO = 1;
    private static final Integer UPDATED_RITMO_CARDIACO = 2;

    private static final Integer DEFAULT_RITMO_RESPIRATORIO = 1;
    private static final Integer UPDATED_RITMO_RESPIRATORIO = 2;

    private static final Integer DEFAULT_OXIMETRIA = 1;
    private static final Integer UPDATED_OXIMETRIA = 2;

    private static final Integer DEFAULT_PRESION_ARTERIAL_SISTOLICA = 1;
    private static final Integer UPDATED_PRESION_ARTERIAL_SISTOLICA = 2;

    private static final Integer DEFAULT_PRESION_ARTERIAL_DIASTOLICA = 1;
    private static final Integer UPDATED_PRESION_ARTERIAL_DIASTOLICA = 2;

    private static final Float DEFAULT_TEMPERATURA = 1F;
    private static final Float UPDATED_TEMPERATURA = 2F;

    private static final Instant DEFAULT_FECHA_REGISTRO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_REGISTRO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_FECHA_TOMA = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_TOMA = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/fisiometria-1-s";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private Fisiometria1Repository fisiometria1Repository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Fisiometria1 fisiometria1;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fisiometria1 createEntity(EntityManager em) {
        Fisiometria1 fisiometria1 = new Fisiometria1()
            .ritmoCardiaco(DEFAULT_RITMO_CARDIACO)
            .ritmoRespiratorio(DEFAULT_RITMO_RESPIRATORIO)
            .oximetria(DEFAULT_OXIMETRIA)
            .presionArterialSistolica(DEFAULT_PRESION_ARTERIAL_SISTOLICA)
            .presionArterialDiastolica(DEFAULT_PRESION_ARTERIAL_DIASTOLICA)
            .temperatura(DEFAULT_TEMPERATURA)
            .fechaRegistro(DEFAULT_FECHA_REGISTRO)
            .fechaToma(DEFAULT_FECHA_TOMA);
        return fisiometria1;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fisiometria1 createUpdatedEntity(EntityManager em) {
        Fisiometria1 fisiometria1 = new Fisiometria1()
            .ritmoCardiaco(UPDATED_RITMO_CARDIACO)
            .ritmoRespiratorio(UPDATED_RITMO_RESPIRATORIO)
            .oximetria(UPDATED_OXIMETRIA)
            .presionArterialSistolica(UPDATED_PRESION_ARTERIAL_SISTOLICA)
            .presionArterialDiastolica(UPDATED_PRESION_ARTERIAL_DIASTOLICA)
            .temperatura(UPDATED_TEMPERATURA)
            .fechaRegistro(UPDATED_FECHA_REGISTRO)
            .fechaToma(UPDATED_FECHA_TOMA);
        return fisiometria1;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Fisiometria1.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        fisiometria1 = createEntity(em);
    }

    @Test
    void createFisiometria1() throws Exception {
        int databaseSizeBeforeCreate = fisiometria1Repository.findAll().collectList().block().size();
        // Create the Fisiometria1
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(fisiometria1))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Fisiometria1 in the database
        List<Fisiometria1> fisiometria1List = fisiometria1Repository.findAll().collectList().block();
        assertThat(fisiometria1List).hasSize(databaseSizeBeforeCreate + 1);
        Fisiometria1 testFisiometria1 = fisiometria1List.get(fisiometria1List.size() - 1);
        assertThat(testFisiometria1.getRitmoCardiaco()).isEqualTo(DEFAULT_RITMO_CARDIACO);
        assertThat(testFisiometria1.getRitmoRespiratorio()).isEqualTo(DEFAULT_RITMO_RESPIRATORIO);
        assertThat(testFisiometria1.getOximetria()).isEqualTo(DEFAULT_OXIMETRIA);
        assertThat(testFisiometria1.getPresionArterialSistolica()).isEqualTo(DEFAULT_PRESION_ARTERIAL_SISTOLICA);
        assertThat(testFisiometria1.getPresionArterialDiastolica()).isEqualTo(DEFAULT_PRESION_ARTERIAL_DIASTOLICA);
        assertThat(testFisiometria1.getTemperatura()).isEqualTo(DEFAULT_TEMPERATURA);
        assertThat(testFisiometria1.getFechaRegistro()).isEqualTo(DEFAULT_FECHA_REGISTRO);
        assertThat(testFisiometria1.getFechaToma()).isEqualTo(DEFAULT_FECHA_TOMA);
    }

    @Test
    void createFisiometria1WithExistingId() throws Exception {
        // Create the Fisiometria1 with an existing ID
        fisiometria1.setId(1L);

        int databaseSizeBeforeCreate = fisiometria1Repository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(fisiometria1))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Fisiometria1 in the database
        List<Fisiometria1> fisiometria1List = fisiometria1Repository.findAll().collectList().block();
        assertThat(fisiometria1List).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllFisiometria1s() {
        // Initialize the database
        fisiometria1Repository.save(fisiometria1).block();

        // Get all the fisiometria1List
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(fisiometria1.getId().intValue()))
            .jsonPath("$.[*].ritmoCardiaco")
            .value(hasItem(DEFAULT_RITMO_CARDIACO))
            .jsonPath("$.[*].ritmoRespiratorio")
            .value(hasItem(DEFAULT_RITMO_RESPIRATORIO))
            .jsonPath("$.[*].oximetria")
            .value(hasItem(DEFAULT_OXIMETRIA))
            .jsonPath("$.[*].presionArterialSistolica")
            .value(hasItem(DEFAULT_PRESION_ARTERIAL_SISTOLICA))
            .jsonPath("$.[*].presionArterialDiastolica")
            .value(hasItem(DEFAULT_PRESION_ARTERIAL_DIASTOLICA))
            .jsonPath("$.[*].temperatura")
            .value(hasItem(DEFAULT_TEMPERATURA.doubleValue()))
            .jsonPath("$.[*].fechaRegistro")
            .value(hasItem(DEFAULT_FECHA_REGISTRO.toString()))
            .jsonPath("$.[*].fechaToma")
            .value(hasItem(DEFAULT_FECHA_TOMA.toString()));
    }

    @Test
    void getFisiometria1() {
        // Initialize the database
        fisiometria1Repository.save(fisiometria1).block();

        // Get the fisiometria1
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, fisiometria1.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(fisiometria1.getId().intValue()))
            .jsonPath("$.ritmoCardiaco")
            .value(is(DEFAULT_RITMO_CARDIACO))
            .jsonPath("$.ritmoRespiratorio")
            .value(is(DEFAULT_RITMO_RESPIRATORIO))
            .jsonPath("$.oximetria")
            .value(is(DEFAULT_OXIMETRIA))
            .jsonPath("$.presionArterialSistolica")
            .value(is(DEFAULT_PRESION_ARTERIAL_SISTOLICA))
            .jsonPath("$.presionArterialDiastolica")
            .value(is(DEFAULT_PRESION_ARTERIAL_DIASTOLICA))
            .jsonPath("$.temperatura")
            .value(is(DEFAULT_TEMPERATURA.doubleValue()))
            .jsonPath("$.fechaRegistro")
            .value(is(DEFAULT_FECHA_REGISTRO.toString()))
            .jsonPath("$.fechaToma")
            .value(is(DEFAULT_FECHA_TOMA.toString()));
    }

    @Test
    void getNonExistingFisiometria1() {
        // Get the fisiometria1
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewFisiometria1() throws Exception {
        // Initialize the database
        fisiometria1Repository.save(fisiometria1).block();

        int databaseSizeBeforeUpdate = fisiometria1Repository.findAll().collectList().block().size();

        // Update the fisiometria1
        Fisiometria1 updatedFisiometria1 = fisiometria1Repository.findById(fisiometria1.getId()).block();
        updatedFisiometria1
            .ritmoCardiaco(UPDATED_RITMO_CARDIACO)
            .ritmoRespiratorio(UPDATED_RITMO_RESPIRATORIO)
            .oximetria(UPDATED_OXIMETRIA)
            .presionArterialSistolica(UPDATED_PRESION_ARTERIAL_SISTOLICA)
            .presionArterialDiastolica(UPDATED_PRESION_ARTERIAL_DIASTOLICA)
            .temperatura(UPDATED_TEMPERATURA)
            .fechaRegistro(UPDATED_FECHA_REGISTRO)
            .fechaToma(UPDATED_FECHA_TOMA);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedFisiometria1.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedFisiometria1))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Fisiometria1 in the database
        List<Fisiometria1> fisiometria1List = fisiometria1Repository.findAll().collectList().block();
        assertThat(fisiometria1List).hasSize(databaseSizeBeforeUpdate);
        Fisiometria1 testFisiometria1 = fisiometria1List.get(fisiometria1List.size() - 1);
        assertThat(testFisiometria1.getRitmoCardiaco()).isEqualTo(UPDATED_RITMO_CARDIACO);
        assertThat(testFisiometria1.getRitmoRespiratorio()).isEqualTo(UPDATED_RITMO_RESPIRATORIO);
        assertThat(testFisiometria1.getOximetria()).isEqualTo(UPDATED_OXIMETRIA);
        assertThat(testFisiometria1.getPresionArterialSistolica()).isEqualTo(UPDATED_PRESION_ARTERIAL_SISTOLICA);
        assertThat(testFisiometria1.getPresionArterialDiastolica()).isEqualTo(UPDATED_PRESION_ARTERIAL_DIASTOLICA);
        assertThat(testFisiometria1.getTemperatura()).isEqualTo(UPDATED_TEMPERATURA);
        assertThat(testFisiometria1.getFechaRegistro()).isEqualTo(UPDATED_FECHA_REGISTRO);
        assertThat(testFisiometria1.getFechaToma()).isEqualTo(UPDATED_FECHA_TOMA);
    }

    @Test
    void putNonExistingFisiometria1() throws Exception {
        int databaseSizeBeforeUpdate = fisiometria1Repository.findAll().collectList().block().size();
        fisiometria1.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, fisiometria1.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(fisiometria1))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Fisiometria1 in the database
        List<Fisiometria1> fisiometria1List = fisiometria1Repository.findAll().collectList().block();
        assertThat(fisiometria1List).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchFisiometria1() throws Exception {
        int databaseSizeBeforeUpdate = fisiometria1Repository.findAll().collectList().block().size();
        fisiometria1.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(fisiometria1))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Fisiometria1 in the database
        List<Fisiometria1> fisiometria1List = fisiometria1Repository.findAll().collectList().block();
        assertThat(fisiometria1List).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamFisiometria1() throws Exception {
        int databaseSizeBeforeUpdate = fisiometria1Repository.findAll().collectList().block().size();
        fisiometria1.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(fisiometria1))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Fisiometria1 in the database
        List<Fisiometria1> fisiometria1List = fisiometria1Repository.findAll().collectList().block();
        assertThat(fisiometria1List).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateFisiometria1WithPatch() throws Exception {
        // Initialize the database
        fisiometria1Repository.save(fisiometria1).block();

        int databaseSizeBeforeUpdate = fisiometria1Repository.findAll().collectList().block().size();

        // Update the fisiometria1 using partial update
        Fisiometria1 partialUpdatedFisiometria1 = new Fisiometria1();
        partialUpdatedFisiometria1.setId(fisiometria1.getId());

        partialUpdatedFisiometria1
            .ritmoCardiaco(UPDATED_RITMO_CARDIACO)
            .ritmoRespiratorio(UPDATED_RITMO_RESPIRATORIO)
            .oximetria(UPDATED_OXIMETRIA)
            .fechaRegistro(UPDATED_FECHA_REGISTRO)
            .fechaToma(UPDATED_FECHA_TOMA);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedFisiometria1.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedFisiometria1))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Fisiometria1 in the database
        List<Fisiometria1> fisiometria1List = fisiometria1Repository.findAll().collectList().block();
        assertThat(fisiometria1List).hasSize(databaseSizeBeforeUpdate);
        Fisiometria1 testFisiometria1 = fisiometria1List.get(fisiometria1List.size() - 1);
        assertThat(testFisiometria1.getRitmoCardiaco()).isEqualTo(UPDATED_RITMO_CARDIACO);
        assertThat(testFisiometria1.getRitmoRespiratorio()).isEqualTo(UPDATED_RITMO_RESPIRATORIO);
        assertThat(testFisiometria1.getOximetria()).isEqualTo(UPDATED_OXIMETRIA);
        assertThat(testFisiometria1.getPresionArterialSistolica()).isEqualTo(DEFAULT_PRESION_ARTERIAL_SISTOLICA);
        assertThat(testFisiometria1.getPresionArterialDiastolica()).isEqualTo(DEFAULT_PRESION_ARTERIAL_DIASTOLICA);
        assertThat(testFisiometria1.getTemperatura()).isEqualTo(DEFAULT_TEMPERATURA);
        assertThat(testFisiometria1.getFechaRegistro()).isEqualTo(UPDATED_FECHA_REGISTRO);
        assertThat(testFisiometria1.getFechaToma()).isEqualTo(UPDATED_FECHA_TOMA);
    }

    @Test
    void fullUpdateFisiometria1WithPatch() throws Exception {
        // Initialize the database
        fisiometria1Repository.save(fisiometria1).block();

        int databaseSizeBeforeUpdate = fisiometria1Repository.findAll().collectList().block().size();

        // Update the fisiometria1 using partial update
        Fisiometria1 partialUpdatedFisiometria1 = new Fisiometria1();
        partialUpdatedFisiometria1.setId(fisiometria1.getId());

        partialUpdatedFisiometria1
            .ritmoCardiaco(UPDATED_RITMO_CARDIACO)
            .ritmoRespiratorio(UPDATED_RITMO_RESPIRATORIO)
            .oximetria(UPDATED_OXIMETRIA)
            .presionArterialSistolica(UPDATED_PRESION_ARTERIAL_SISTOLICA)
            .presionArterialDiastolica(UPDATED_PRESION_ARTERIAL_DIASTOLICA)
            .temperatura(UPDATED_TEMPERATURA)
            .fechaRegistro(UPDATED_FECHA_REGISTRO)
            .fechaToma(UPDATED_FECHA_TOMA);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedFisiometria1.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedFisiometria1))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Fisiometria1 in the database
        List<Fisiometria1> fisiometria1List = fisiometria1Repository.findAll().collectList().block();
        assertThat(fisiometria1List).hasSize(databaseSizeBeforeUpdate);
        Fisiometria1 testFisiometria1 = fisiometria1List.get(fisiometria1List.size() - 1);
        assertThat(testFisiometria1.getRitmoCardiaco()).isEqualTo(UPDATED_RITMO_CARDIACO);
        assertThat(testFisiometria1.getRitmoRespiratorio()).isEqualTo(UPDATED_RITMO_RESPIRATORIO);
        assertThat(testFisiometria1.getOximetria()).isEqualTo(UPDATED_OXIMETRIA);
        assertThat(testFisiometria1.getPresionArterialSistolica()).isEqualTo(UPDATED_PRESION_ARTERIAL_SISTOLICA);
        assertThat(testFisiometria1.getPresionArterialDiastolica()).isEqualTo(UPDATED_PRESION_ARTERIAL_DIASTOLICA);
        assertThat(testFisiometria1.getTemperatura()).isEqualTo(UPDATED_TEMPERATURA);
        assertThat(testFisiometria1.getFechaRegistro()).isEqualTo(UPDATED_FECHA_REGISTRO);
        assertThat(testFisiometria1.getFechaToma()).isEqualTo(UPDATED_FECHA_TOMA);
    }

    @Test
    void patchNonExistingFisiometria1() throws Exception {
        int databaseSizeBeforeUpdate = fisiometria1Repository.findAll().collectList().block().size();
        fisiometria1.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, fisiometria1.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(fisiometria1))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Fisiometria1 in the database
        List<Fisiometria1> fisiometria1List = fisiometria1Repository.findAll().collectList().block();
        assertThat(fisiometria1List).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchFisiometria1() throws Exception {
        int databaseSizeBeforeUpdate = fisiometria1Repository.findAll().collectList().block().size();
        fisiometria1.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(fisiometria1))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Fisiometria1 in the database
        List<Fisiometria1> fisiometria1List = fisiometria1Repository.findAll().collectList().block();
        assertThat(fisiometria1List).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamFisiometria1() throws Exception {
        int databaseSizeBeforeUpdate = fisiometria1Repository.findAll().collectList().block().size();
        fisiometria1.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(fisiometria1))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Fisiometria1 in the database
        List<Fisiometria1> fisiometria1List = fisiometria1Repository.findAll().collectList().block();
        assertThat(fisiometria1List).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteFisiometria1() {
        // Initialize the database
        fisiometria1Repository.save(fisiometria1).block();

        int databaseSizeBeforeDelete = fisiometria1Repository.findAll().collectList().block().size();

        // Delete the fisiometria1
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, fisiometria1.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Fisiometria1> fisiometria1List = fisiometria1Repository.findAll().collectList().block();
        assertThat(fisiometria1List).hasSize(databaseSizeBeforeDelete - 1);
    }
}
