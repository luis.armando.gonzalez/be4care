package com.be4tech.be4care.manilla.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.manilla.IntegrationTest;
import com.be4tech.be4care.manilla.domain.Oximetria;
import com.be4tech.be4care.manilla.repository.OximetriaRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link OximetriaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class OximetriaResourceIT {

    private static final Integer DEFAULT_OXIMETRIA = 1;
    private static final Integer UPDATED_OXIMETRIA = 2;

    private static final Instant DEFAULT_FECHA_REGISTRO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_REGISTRO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/oximetrias";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private OximetriaRepository oximetriaRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Oximetria oximetria;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Oximetria createEntity(EntityManager em) {
        Oximetria oximetria = new Oximetria().oximetria(DEFAULT_OXIMETRIA).fechaRegistro(DEFAULT_FECHA_REGISTRO);
        return oximetria;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Oximetria createUpdatedEntity(EntityManager em) {
        Oximetria oximetria = new Oximetria().oximetria(UPDATED_OXIMETRIA).fechaRegistro(UPDATED_FECHA_REGISTRO);
        return oximetria;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Oximetria.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        oximetria = createEntity(em);
    }

    @Test
    void createOximetria() throws Exception {
        int databaseSizeBeforeCreate = oximetriaRepository.findAll().collectList().block().size();
        // Create the Oximetria
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(oximetria))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Oximetria in the database
        List<Oximetria> oximetriaList = oximetriaRepository.findAll().collectList().block();
        assertThat(oximetriaList).hasSize(databaseSizeBeforeCreate + 1);
        Oximetria testOximetria = oximetriaList.get(oximetriaList.size() - 1);
        assertThat(testOximetria.getOximetria()).isEqualTo(DEFAULT_OXIMETRIA);
        assertThat(testOximetria.getFechaRegistro()).isEqualTo(DEFAULT_FECHA_REGISTRO);
    }

    @Test
    void createOximetriaWithExistingId() throws Exception {
        // Create the Oximetria with an existing ID
        oximetria.setId(1L);

        int databaseSizeBeforeCreate = oximetriaRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(oximetria))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Oximetria in the database
        List<Oximetria> oximetriaList = oximetriaRepository.findAll().collectList().block();
        assertThat(oximetriaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllOximetrias() {
        // Initialize the database
        oximetriaRepository.save(oximetria).block();

        // Get all the oximetriaList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(oximetria.getId().intValue()))
            .jsonPath("$.[*].oximetria")
            .value(hasItem(DEFAULT_OXIMETRIA))
            .jsonPath("$.[*].fechaRegistro")
            .value(hasItem(DEFAULT_FECHA_REGISTRO.toString()));
    }

    @Test
    void getOximetria() {
        // Initialize the database
        oximetriaRepository.save(oximetria).block();

        // Get the oximetria
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, oximetria.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(oximetria.getId().intValue()))
            .jsonPath("$.oximetria")
            .value(is(DEFAULT_OXIMETRIA))
            .jsonPath("$.fechaRegistro")
            .value(is(DEFAULT_FECHA_REGISTRO.toString()));
    }

    @Test
    void getNonExistingOximetria() {
        // Get the oximetria
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewOximetria() throws Exception {
        // Initialize the database
        oximetriaRepository.save(oximetria).block();

        int databaseSizeBeforeUpdate = oximetriaRepository.findAll().collectList().block().size();

        // Update the oximetria
        Oximetria updatedOximetria = oximetriaRepository.findById(oximetria.getId()).block();
        updatedOximetria.oximetria(UPDATED_OXIMETRIA).fechaRegistro(UPDATED_FECHA_REGISTRO);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedOximetria.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedOximetria))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Oximetria in the database
        List<Oximetria> oximetriaList = oximetriaRepository.findAll().collectList().block();
        assertThat(oximetriaList).hasSize(databaseSizeBeforeUpdate);
        Oximetria testOximetria = oximetriaList.get(oximetriaList.size() - 1);
        assertThat(testOximetria.getOximetria()).isEqualTo(UPDATED_OXIMETRIA);
        assertThat(testOximetria.getFechaRegistro()).isEqualTo(UPDATED_FECHA_REGISTRO);
    }

    @Test
    void putNonExistingOximetria() throws Exception {
        int databaseSizeBeforeUpdate = oximetriaRepository.findAll().collectList().block().size();
        oximetria.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, oximetria.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(oximetria))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Oximetria in the database
        List<Oximetria> oximetriaList = oximetriaRepository.findAll().collectList().block();
        assertThat(oximetriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchOximetria() throws Exception {
        int databaseSizeBeforeUpdate = oximetriaRepository.findAll().collectList().block().size();
        oximetria.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(oximetria))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Oximetria in the database
        List<Oximetria> oximetriaList = oximetriaRepository.findAll().collectList().block();
        assertThat(oximetriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamOximetria() throws Exception {
        int databaseSizeBeforeUpdate = oximetriaRepository.findAll().collectList().block().size();
        oximetria.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(oximetria))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Oximetria in the database
        List<Oximetria> oximetriaList = oximetriaRepository.findAll().collectList().block();
        assertThat(oximetriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateOximetriaWithPatch() throws Exception {
        // Initialize the database
        oximetriaRepository.save(oximetria).block();

        int databaseSizeBeforeUpdate = oximetriaRepository.findAll().collectList().block().size();

        // Update the oximetria using partial update
        Oximetria partialUpdatedOximetria = new Oximetria();
        partialUpdatedOximetria.setId(oximetria.getId());

        partialUpdatedOximetria.oximetria(UPDATED_OXIMETRIA);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedOximetria.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedOximetria))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Oximetria in the database
        List<Oximetria> oximetriaList = oximetriaRepository.findAll().collectList().block();
        assertThat(oximetriaList).hasSize(databaseSizeBeforeUpdate);
        Oximetria testOximetria = oximetriaList.get(oximetriaList.size() - 1);
        assertThat(testOximetria.getOximetria()).isEqualTo(UPDATED_OXIMETRIA);
        assertThat(testOximetria.getFechaRegistro()).isEqualTo(DEFAULT_FECHA_REGISTRO);
    }

    @Test
    void fullUpdateOximetriaWithPatch() throws Exception {
        // Initialize the database
        oximetriaRepository.save(oximetria).block();

        int databaseSizeBeforeUpdate = oximetriaRepository.findAll().collectList().block().size();

        // Update the oximetria using partial update
        Oximetria partialUpdatedOximetria = new Oximetria();
        partialUpdatedOximetria.setId(oximetria.getId());

        partialUpdatedOximetria.oximetria(UPDATED_OXIMETRIA).fechaRegistro(UPDATED_FECHA_REGISTRO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedOximetria.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedOximetria))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Oximetria in the database
        List<Oximetria> oximetriaList = oximetriaRepository.findAll().collectList().block();
        assertThat(oximetriaList).hasSize(databaseSizeBeforeUpdate);
        Oximetria testOximetria = oximetriaList.get(oximetriaList.size() - 1);
        assertThat(testOximetria.getOximetria()).isEqualTo(UPDATED_OXIMETRIA);
        assertThat(testOximetria.getFechaRegistro()).isEqualTo(UPDATED_FECHA_REGISTRO);
    }

    @Test
    void patchNonExistingOximetria() throws Exception {
        int databaseSizeBeforeUpdate = oximetriaRepository.findAll().collectList().block().size();
        oximetria.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, oximetria.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(oximetria))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Oximetria in the database
        List<Oximetria> oximetriaList = oximetriaRepository.findAll().collectList().block();
        assertThat(oximetriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchOximetria() throws Exception {
        int databaseSizeBeforeUpdate = oximetriaRepository.findAll().collectList().block().size();
        oximetria.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(oximetria))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Oximetria in the database
        List<Oximetria> oximetriaList = oximetriaRepository.findAll().collectList().block();
        assertThat(oximetriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamOximetria() throws Exception {
        int databaseSizeBeforeUpdate = oximetriaRepository.findAll().collectList().block().size();
        oximetria.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(oximetria))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Oximetria in the database
        List<Oximetria> oximetriaList = oximetriaRepository.findAll().collectList().block();
        assertThat(oximetriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteOximetria() {
        // Initialize the database
        oximetriaRepository.save(oximetria).block();

        int databaseSizeBeforeDelete = oximetriaRepository.findAll().collectList().block().size();

        // Delete the oximetria
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, oximetria.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Oximetria> oximetriaList = oximetriaRepository.findAll().collectList().block();
        assertThat(oximetriaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
