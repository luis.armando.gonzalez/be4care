package com.be4tech.be4care.manilla.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.manilla.IntegrationTest;
import com.be4tech.be4care.manilla.domain.FrecuenciaCardiaca;
import com.be4tech.be4care.manilla.repository.FrecuenciaCardiacaRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link FrecuenciaCardiacaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class FrecuenciaCardiacaResourceIT {

    private static final Integer DEFAULT_FRECUENCIA_CARDIACA = 1;
    private static final Integer UPDATED_FRECUENCIA_CARDIACA = 2;

    private static final Instant DEFAULT_FECHA_REGISTRO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_REGISTRO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/frecuencia-cardiacas";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private FrecuenciaCardiacaRepository frecuenciaCardiacaRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private FrecuenciaCardiaca frecuenciaCardiaca;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FrecuenciaCardiaca createEntity(EntityManager em) {
        FrecuenciaCardiaca frecuenciaCardiaca = new FrecuenciaCardiaca()
            .frecuenciaCardiaca(DEFAULT_FRECUENCIA_CARDIACA)
            .fechaRegistro(DEFAULT_FECHA_REGISTRO);
        return frecuenciaCardiaca;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FrecuenciaCardiaca createUpdatedEntity(EntityManager em) {
        FrecuenciaCardiaca frecuenciaCardiaca = new FrecuenciaCardiaca()
            .frecuenciaCardiaca(UPDATED_FRECUENCIA_CARDIACA)
            .fechaRegistro(UPDATED_FECHA_REGISTRO);
        return frecuenciaCardiaca;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(FrecuenciaCardiaca.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        frecuenciaCardiaca = createEntity(em);
    }

    @Test
    void createFrecuenciaCardiaca() throws Exception {
        int databaseSizeBeforeCreate = frecuenciaCardiacaRepository.findAll().collectList().block().size();
        // Create the FrecuenciaCardiaca
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(frecuenciaCardiaca))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the FrecuenciaCardiaca in the database
        List<FrecuenciaCardiaca> frecuenciaCardiacaList = frecuenciaCardiacaRepository.findAll().collectList().block();
        assertThat(frecuenciaCardiacaList).hasSize(databaseSizeBeforeCreate + 1);
        FrecuenciaCardiaca testFrecuenciaCardiaca = frecuenciaCardiacaList.get(frecuenciaCardiacaList.size() - 1);
        assertThat(testFrecuenciaCardiaca.getFrecuenciaCardiaca()).isEqualTo(DEFAULT_FRECUENCIA_CARDIACA);
        assertThat(testFrecuenciaCardiaca.getFechaRegistro()).isEqualTo(DEFAULT_FECHA_REGISTRO);
    }

    @Test
    void createFrecuenciaCardiacaWithExistingId() throws Exception {
        // Create the FrecuenciaCardiaca with an existing ID
        frecuenciaCardiaca.setId(1L);

        int databaseSizeBeforeCreate = frecuenciaCardiacaRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(frecuenciaCardiaca))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the FrecuenciaCardiaca in the database
        List<FrecuenciaCardiaca> frecuenciaCardiacaList = frecuenciaCardiacaRepository.findAll().collectList().block();
        assertThat(frecuenciaCardiacaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllFrecuenciaCardiacas() {
        // Initialize the database
        frecuenciaCardiacaRepository.save(frecuenciaCardiaca).block();

        // Get all the frecuenciaCardiacaList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(frecuenciaCardiaca.getId().intValue()))
            .jsonPath("$.[*].frecuenciaCardiaca")
            .value(hasItem(DEFAULT_FRECUENCIA_CARDIACA))
            .jsonPath("$.[*].fechaRegistro")
            .value(hasItem(DEFAULT_FECHA_REGISTRO.toString()));
    }

    @Test
    void getFrecuenciaCardiaca() {
        // Initialize the database
        frecuenciaCardiacaRepository.save(frecuenciaCardiaca).block();

        // Get the frecuenciaCardiaca
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, frecuenciaCardiaca.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(frecuenciaCardiaca.getId().intValue()))
            .jsonPath("$.frecuenciaCardiaca")
            .value(is(DEFAULT_FRECUENCIA_CARDIACA))
            .jsonPath("$.fechaRegistro")
            .value(is(DEFAULT_FECHA_REGISTRO.toString()));
    }

    @Test
    void getNonExistingFrecuenciaCardiaca() {
        // Get the frecuenciaCardiaca
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewFrecuenciaCardiaca() throws Exception {
        // Initialize the database
        frecuenciaCardiacaRepository.save(frecuenciaCardiaca).block();

        int databaseSizeBeforeUpdate = frecuenciaCardiacaRepository.findAll().collectList().block().size();

        // Update the frecuenciaCardiaca
        FrecuenciaCardiaca updatedFrecuenciaCardiaca = frecuenciaCardiacaRepository.findById(frecuenciaCardiaca.getId()).block();
        updatedFrecuenciaCardiaca.frecuenciaCardiaca(UPDATED_FRECUENCIA_CARDIACA).fechaRegistro(UPDATED_FECHA_REGISTRO);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedFrecuenciaCardiaca.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedFrecuenciaCardiaca))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the FrecuenciaCardiaca in the database
        List<FrecuenciaCardiaca> frecuenciaCardiacaList = frecuenciaCardiacaRepository.findAll().collectList().block();
        assertThat(frecuenciaCardiacaList).hasSize(databaseSizeBeforeUpdate);
        FrecuenciaCardiaca testFrecuenciaCardiaca = frecuenciaCardiacaList.get(frecuenciaCardiacaList.size() - 1);
        assertThat(testFrecuenciaCardiaca.getFrecuenciaCardiaca()).isEqualTo(UPDATED_FRECUENCIA_CARDIACA);
        assertThat(testFrecuenciaCardiaca.getFechaRegistro()).isEqualTo(UPDATED_FECHA_REGISTRO);
    }

    @Test
    void putNonExistingFrecuenciaCardiaca() throws Exception {
        int databaseSizeBeforeUpdate = frecuenciaCardiacaRepository.findAll().collectList().block().size();
        frecuenciaCardiaca.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, frecuenciaCardiaca.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(frecuenciaCardiaca))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the FrecuenciaCardiaca in the database
        List<FrecuenciaCardiaca> frecuenciaCardiacaList = frecuenciaCardiacaRepository.findAll().collectList().block();
        assertThat(frecuenciaCardiacaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchFrecuenciaCardiaca() throws Exception {
        int databaseSizeBeforeUpdate = frecuenciaCardiacaRepository.findAll().collectList().block().size();
        frecuenciaCardiaca.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(frecuenciaCardiaca))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the FrecuenciaCardiaca in the database
        List<FrecuenciaCardiaca> frecuenciaCardiacaList = frecuenciaCardiacaRepository.findAll().collectList().block();
        assertThat(frecuenciaCardiacaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamFrecuenciaCardiaca() throws Exception {
        int databaseSizeBeforeUpdate = frecuenciaCardiacaRepository.findAll().collectList().block().size();
        frecuenciaCardiaca.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(frecuenciaCardiaca))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the FrecuenciaCardiaca in the database
        List<FrecuenciaCardiaca> frecuenciaCardiacaList = frecuenciaCardiacaRepository.findAll().collectList().block();
        assertThat(frecuenciaCardiacaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateFrecuenciaCardiacaWithPatch() throws Exception {
        // Initialize the database
        frecuenciaCardiacaRepository.save(frecuenciaCardiaca).block();

        int databaseSizeBeforeUpdate = frecuenciaCardiacaRepository.findAll().collectList().block().size();

        // Update the frecuenciaCardiaca using partial update
        FrecuenciaCardiaca partialUpdatedFrecuenciaCardiaca = new FrecuenciaCardiaca();
        partialUpdatedFrecuenciaCardiaca.setId(frecuenciaCardiaca.getId());

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedFrecuenciaCardiaca.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedFrecuenciaCardiaca))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the FrecuenciaCardiaca in the database
        List<FrecuenciaCardiaca> frecuenciaCardiacaList = frecuenciaCardiacaRepository.findAll().collectList().block();
        assertThat(frecuenciaCardiacaList).hasSize(databaseSizeBeforeUpdate);
        FrecuenciaCardiaca testFrecuenciaCardiaca = frecuenciaCardiacaList.get(frecuenciaCardiacaList.size() - 1);
        assertThat(testFrecuenciaCardiaca.getFrecuenciaCardiaca()).isEqualTo(DEFAULT_FRECUENCIA_CARDIACA);
        assertThat(testFrecuenciaCardiaca.getFechaRegistro()).isEqualTo(DEFAULT_FECHA_REGISTRO);
    }

    @Test
    void fullUpdateFrecuenciaCardiacaWithPatch() throws Exception {
        // Initialize the database
        frecuenciaCardiacaRepository.save(frecuenciaCardiaca).block();

        int databaseSizeBeforeUpdate = frecuenciaCardiacaRepository.findAll().collectList().block().size();

        // Update the frecuenciaCardiaca using partial update
        FrecuenciaCardiaca partialUpdatedFrecuenciaCardiaca = new FrecuenciaCardiaca();
        partialUpdatedFrecuenciaCardiaca.setId(frecuenciaCardiaca.getId());

        partialUpdatedFrecuenciaCardiaca.frecuenciaCardiaca(UPDATED_FRECUENCIA_CARDIACA).fechaRegistro(UPDATED_FECHA_REGISTRO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedFrecuenciaCardiaca.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedFrecuenciaCardiaca))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the FrecuenciaCardiaca in the database
        List<FrecuenciaCardiaca> frecuenciaCardiacaList = frecuenciaCardiacaRepository.findAll().collectList().block();
        assertThat(frecuenciaCardiacaList).hasSize(databaseSizeBeforeUpdate);
        FrecuenciaCardiaca testFrecuenciaCardiaca = frecuenciaCardiacaList.get(frecuenciaCardiacaList.size() - 1);
        assertThat(testFrecuenciaCardiaca.getFrecuenciaCardiaca()).isEqualTo(UPDATED_FRECUENCIA_CARDIACA);
        assertThat(testFrecuenciaCardiaca.getFechaRegistro()).isEqualTo(UPDATED_FECHA_REGISTRO);
    }

    @Test
    void patchNonExistingFrecuenciaCardiaca() throws Exception {
        int databaseSizeBeforeUpdate = frecuenciaCardiacaRepository.findAll().collectList().block().size();
        frecuenciaCardiaca.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, frecuenciaCardiaca.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(frecuenciaCardiaca))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the FrecuenciaCardiaca in the database
        List<FrecuenciaCardiaca> frecuenciaCardiacaList = frecuenciaCardiacaRepository.findAll().collectList().block();
        assertThat(frecuenciaCardiacaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchFrecuenciaCardiaca() throws Exception {
        int databaseSizeBeforeUpdate = frecuenciaCardiacaRepository.findAll().collectList().block().size();
        frecuenciaCardiaca.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(frecuenciaCardiaca))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the FrecuenciaCardiaca in the database
        List<FrecuenciaCardiaca> frecuenciaCardiacaList = frecuenciaCardiacaRepository.findAll().collectList().block();
        assertThat(frecuenciaCardiacaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamFrecuenciaCardiaca() throws Exception {
        int databaseSizeBeforeUpdate = frecuenciaCardiacaRepository.findAll().collectList().block().size();
        frecuenciaCardiaca.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(frecuenciaCardiaca))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the FrecuenciaCardiaca in the database
        List<FrecuenciaCardiaca> frecuenciaCardiacaList = frecuenciaCardiacaRepository.findAll().collectList().block();
        assertThat(frecuenciaCardiacaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteFrecuenciaCardiaca() {
        // Initialize the database
        frecuenciaCardiacaRepository.save(frecuenciaCardiaca).block();

        int databaseSizeBeforeDelete = frecuenciaCardiacaRepository.findAll().collectList().block().size();

        // Delete the frecuenciaCardiaca
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, frecuenciaCardiaca.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<FrecuenciaCardiaca> frecuenciaCardiacaList = frecuenciaCardiacaRepository.findAll().collectList().block();
        assertThat(frecuenciaCardiacaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
