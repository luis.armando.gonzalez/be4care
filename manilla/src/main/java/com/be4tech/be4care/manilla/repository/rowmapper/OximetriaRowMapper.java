package com.be4tech.be4care.manilla.repository.rowmapper;

import com.be4tech.be4care.manilla.domain.Oximetria;
import com.be4tech.be4care.manilla.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Oximetria}, with proper type conversions.
 */
@Service
public class OximetriaRowMapper implements BiFunction<Row, String, Oximetria> {

    private final ColumnConverter converter;

    public OximetriaRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Oximetria} stored in the database.
     */
    @Override
    public Oximetria apply(Row row, String prefix) {
        Oximetria entity = new Oximetria();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setOximetria(converter.fromRow(row, prefix + "_oximetria", Integer.class));
        entity.setFechaRegistro(converter.fromRow(row, prefix + "_fecha_registro", Instant.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        return entity;
    }
}
