package com.be4tech.be4care.manilla.domain;

import java.io.Serializable;
import java.time.Instant;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Programa.
 */
@Table("programa")
public class Programa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("calorias_actividad")
    private Integer caloriasActividad;

    @Column("pasos_actividad")
    private Integer pasosActividad;

    @Column("fecha_registro")
    private Instant fechaRegistro;

    @Transient
    private User user;

    @Column("user_id")
    private String userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Programa id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCaloriasActividad() {
        return this.caloriasActividad;
    }

    public Programa caloriasActividad(Integer caloriasActividad) {
        this.setCaloriasActividad(caloriasActividad);
        return this;
    }

    public void setCaloriasActividad(Integer caloriasActividad) {
        this.caloriasActividad = caloriasActividad;
    }

    public Integer getPasosActividad() {
        return this.pasosActividad;
    }

    public Programa pasosActividad(Integer pasosActividad) {
        this.setPasosActividad(pasosActividad);
        return this;
    }

    public void setPasosActividad(Integer pasosActividad) {
        this.pasosActividad = pasosActividad;
    }

    public Instant getFechaRegistro() {
        return this.fechaRegistro;
    }

    public Programa fechaRegistro(Instant fechaRegistro) {
        this.setFechaRegistro(fechaRegistro);
        return this;
    }

    public void setFechaRegistro(Instant fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user != null ? user.getId() : null;
    }

    public Programa user(User user) {
        this.setUser(user);
        return this;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String user) {
        this.userId = user;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Programa)) {
            return false;
        }
        return id != null && id.equals(((Programa) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Programa{" +
            "id=" + getId() +
            ", caloriasActividad=" + getCaloriasActividad() +
            ", pasosActividad=" + getPasosActividad() +
            ", fechaRegistro='" + getFechaRegistro() + "'" +
            "}";
    }
}
