package com.be4tech.be4care.manilla.repository.rowmapper;

import com.be4tech.be4care.manilla.domain.Encuesta;
import com.be4tech.be4care.manilla.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Encuesta}, with proper type conversions.
 */
@Service
public class EncuestaRowMapper implements BiFunction<Row, String, Encuesta> {

    private final ColumnConverter converter;

    public EncuestaRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Encuesta} stored in the database.
     */
    @Override
    public Encuesta apply(Row row, String prefix) {
        Encuesta entity = new Encuesta();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setFecha(converter.fromRow(row, prefix + "_fecha", Instant.class));
        entity.setDebilidad(converter.fromRow(row, prefix + "_debilidad", Boolean.class));
        entity.setCefalea(converter.fromRow(row, prefix + "_cefalea", Boolean.class));
        entity.setCalambres(converter.fromRow(row, prefix + "_calambres", Boolean.class));
        entity.setNauseas(converter.fromRow(row, prefix + "_nauseas", Boolean.class));
        entity.setVomito(converter.fromRow(row, prefix + "_vomito", Boolean.class));
        entity.setMareo(converter.fromRow(row, prefix + "_mareo", Boolean.class));
        entity.setNinguna(converter.fromRow(row, prefix + "_ninguna", Boolean.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        return entity;
    }
}
