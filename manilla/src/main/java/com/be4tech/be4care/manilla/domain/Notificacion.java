package com.be4tech.be4care.manilla.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.time.ZonedDateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Notificacion.
 */
@Table("notificacion")
public class Notificacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("fecha_inicio")
    private Instant fechaInicio;

    @Column("fecha_actualizacion")
    private ZonedDateTime fechaActualizacion;

    @Column("estado")
    private Integer estado;

    @Column("tipo_notificacion")
    private Integer tipoNotificacion;

    @Transient
    @JsonIgnoreProperties(value = { "user" }, allowSetters = true)
    private TokenDisp token;

    @Column("token_id")
    private Long tokenId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Notificacion id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getFechaInicio() {
        return this.fechaInicio;
    }

    public Notificacion fechaInicio(Instant fechaInicio) {
        this.setFechaInicio(fechaInicio);
        return this;
    }

    public void setFechaInicio(Instant fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public ZonedDateTime getFechaActualizacion() {
        return this.fechaActualizacion;
    }

    public Notificacion fechaActualizacion(ZonedDateTime fechaActualizacion) {
        this.setFechaActualizacion(fechaActualizacion);
        return this;
    }

    public void setFechaActualizacion(ZonedDateTime fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Integer getEstado() {
        return this.estado;
    }

    public Notificacion estado(Integer estado) {
        this.setEstado(estado);
        return this;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getTipoNotificacion() {
        return this.tipoNotificacion;
    }

    public Notificacion tipoNotificacion(Integer tipoNotificacion) {
        this.setTipoNotificacion(tipoNotificacion);
        return this;
    }

    public void setTipoNotificacion(Integer tipoNotificacion) {
        this.tipoNotificacion = tipoNotificacion;
    }

    public TokenDisp getToken() {
        return this.token;
    }

    public void setToken(TokenDisp tokenDisp) {
        this.token = tokenDisp;
        this.tokenId = tokenDisp != null ? tokenDisp.getId() : null;
    }

    public Notificacion token(TokenDisp tokenDisp) {
        this.setToken(tokenDisp);
        return this;
    }

    public Long getTokenId() {
        return this.tokenId;
    }

    public void setTokenId(Long tokenDisp) {
        this.tokenId = tokenDisp;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Notificacion)) {
            return false;
        }
        return id != null && id.equals(((Notificacion) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Notificacion{" +
            "id=" + getId() +
            ", fechaInicio='" + getFechaInicio() + "'" +
            ", fechaActualizacion='" + getFechaActualizacion() + "'" +
            ", estado=" + getEstado() +
            ", tipoNotificacion=" + getTipoNotificacion() +
            "}";
    }
}
