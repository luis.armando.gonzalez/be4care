/**
 * View Models used by Spring MVC REST controllers.
 */
package com.be4tech.be4care.manilla.web.rest.vm;
