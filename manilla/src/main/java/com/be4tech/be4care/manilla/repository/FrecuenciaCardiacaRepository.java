package com.be4tech.be4care.manilla.repository;

import com.be4tech.be4care.manilla.domain.FrecuenciaCardiaca;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the FrecuenciaCardiaca entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FrecuenciaCardiacaRepository extends R2dbcRepository<FrecuenciaCardiaca, Long>, FrecuenciaCardiacaRepositoryInternal {
    Flux<FrecuenciaCardiaca> findAllBy(Pageable pageable);

    @Query("SELECT * FROM frecuencia_cardiaca entity WHERE entity.user_id = :id")
    Flux<FrecuenciaCardiaca> findByUser(Long id);

    @Query("SELECT * FROM frecuencia_cardiaca entity WHERE entity.user_id IS NULL")
    Flux<FrecuenciaCardiaca> findAllWhereUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<FrecuenciaCardiaca> findAll();

    @Override
    Mono<FrecuenciaCardiaca> findById(Long id);

    @Override
    <S extends FrecuenciaCardiaca> Mono<S> save(S entity);
}

interface FrecuenciaCardiacaRepositoryInternal {
    <S extends FrecuenciaCardiaca> Mono<S> insert(S entity);
    <S extends FrecuenciaCardiaca> Mono<S> save(S entity);
    Mono<Integer> update(FrecuenciaCardiaca entity);

    Flux<FrecuenciaCardiaca> findAll();
    Mono<FrecuenciaCardiaca> findById(Long id);
    Flux<FrecuenciaCardiaca> findAllBy(Pageable pageable);
    Flux<FrecuenciaCardiaca> findAllBy(Pageable pageable, Criteria criteria);
}
