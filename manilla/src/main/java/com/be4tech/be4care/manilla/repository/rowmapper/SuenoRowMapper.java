package com.be4tech.be4care.manilla.repository.rowmapper;

import com.be4tech.be4care.manilla.domain.Sueno;
import com.be4tech.be4care.manilla.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Sueno}, with proper type conversions.
 */
@Service
public class SuenoRowMapper implements BiFunction<Row, String, Sueno> {

    private final ColumnConverter converter;

    public SuenoRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Sueno} stored in the database.
     */
    @Override
    public Sueno apply(Row row, String prefix) {
        Sueno entity = new Sueno();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setSuperficial(converter.fromRow(row, prefix + "_superficial", Integer.class));
        entity.setProfundo(converter.fromRow(row, prefix + "_profundo", Integer.class));
        entity.setDespierto(converter.fromRow(row, prefix + "_despierto", Integer.class));
        entity.setTimeInstant(converter.fromRow(row, prefix + "_time_instant", Instant.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        return entity;
    }
}
