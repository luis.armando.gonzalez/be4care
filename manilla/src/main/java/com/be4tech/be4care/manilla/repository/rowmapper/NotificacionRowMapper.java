package com.be4tech.be4care.manilla.repository.rowmapper;

import com.be4tech.be4care.manilla.domain.Notificacion;
import com.be4tech.be4care.manilla.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Notificacion}, with proper type conversions.
 */
@Service
public class NotificacionRowMapper implements BiFunction<Row, String, Notificacion> {

    private final ColumnConverter converter;

    public NotificacionRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Notificacion} stored in the database.
     */
    @Override
    public Notificacion apply(Row row, String prefix) {
        Notificacion entity = new Notificacion();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setFechaInicio(converter.fromRow(row, prefix + "_fecha_inicio", Instant.class));
        entity.setFechaActualizacion(converter.fromRow(row, prefix + "_fecha_actualizacion", ZonedDateTime.class));
        entity.setEstado(converter.fromRow(row, prefix + "_estado", Integer.class));
        entity.setTipoNotificacion(converter.fromRow(row, prefix + "_tipo_notificacion", Integer.class));
        entity.setTokenId(converter.fromRow(row, prefix + "_token_id", Long.class));
        return entity;
    }
}
