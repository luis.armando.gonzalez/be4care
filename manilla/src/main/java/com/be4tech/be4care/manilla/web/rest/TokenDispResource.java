package com.be4tech.be4care.manilla.web.rest;

import com.be4tech.be4care.manilla.domain.TokenDisp;
import com.be4tech.be4care.manilla.repository.TokenDispRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.manilla.domain.TokenDisp}.
 */
@RestController
@RequestMapping("/api")
public class TokenDispResource {

    private final Logger log = LoggerFactory.getLogger(TokenDispResource.class);

    private static final String ENTITY_NAME = "manillaTokenDisp";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TokenDispRepository tokenDispRepository;

    private final UserRepository userRepository;

    public TokenDispResource(TokenDispRepository tokenDispRepository, UserRepository userRepository) {
        this.tokenDispRepository = tokenDispRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /token-disps} : Create a new tokenDisp.
     *
     * @param tokenDisp the tokenDisp to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tokenDisp, or with status {@code 400 (Bad Request)} if the tokenDisp has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/token-disps")
    public Mono<ResponseEntity<TokenDisp>> createTokenDisp(@RequestBody TokenDisp tokenDisp) throws URISyntaxException {
        log.debug("REST request to save TokenDisp : {}", tokenDisp);
        if (tokenDisp.getId() != null) {
            throw new BadRequestAlertException("A new tokenDisp cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (tokenDisp.getUser() != null) {
            // Save user in case it's new and only exists in gateway
            userRepository.save(tokenDisp.getUser());
        }
        return tokenDispRepository
            .save(tokenDisp)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/token-disps/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /token-disps/:id} : Updates an existing tokenDisp.
     *
     * @param id the id of the tokenDisp to save.
     * @param tokenDisp the tokenDisp to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tokenDisp,
     * or with status {@code 400 (Bad Request)} if the tokenDisp is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tokenDisp couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/token-disps/{id}")
    public Mono<ResponseEntity<TokenDisp>> updateTokenDisp(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody TokenDisp tokenDisp
    ) throws URISyntaxException {
        log.debug("REST request to update TokenDisp : {}, {}", id, tokenDisp);
        if (tokenDisp.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, tokenDisp.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return tokenDispRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (tokenDisp.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(tokenDisp.getUser());
                }
                return tokenDispRepository
                    .save(tokenDisp)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /token-disps/:id} : Partial updates given fields of an existing tokenDisp, field will ignore if it is null
     *
     * @param id the id of the tokenDisp to save.
     * @param tokenDisp the tokenDisp to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tokenDisp,
     * or with status {@code 400 (Bad Request)} if the tokenDisp is not valid,
     * or with status {@code 404 (Not Found)} if the tokenDisp is not found,
     * or with status {@code 500 (Internal Server Error)} if the tokenDisp couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/token-disps/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<TokenDisp>> partialUpdateTokenDisp(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody TokenDisp tokenDisp
    ) throws URISyntaxException {
        log.debug("REST request to partial update TokenDisp partially : {}, {}", id, tokenDisp);
        if (tokenDisp.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, tokenDisp.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return tokenDispRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (tokenDisp.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(tokenDisp.getUser());
                }

                Mono<TokenDisp> result = tokenDispRepository
                    .findById(tokenDisp.getId())
                    .map(existingTokenDisp -> {
                        if (tokenDisp.getTokenConexion() != null) {
                            existingTokenDisp.setTokenConexion(tokenDisp.getTokenConexion());
                        }
                        if (tokenDisp.getActivo() != null) {
                            existingTokenDisp.setActivo(tokenDisp.getActivo());
                        }
                        if (tokenDisp.getFechaInicio() != null) {
                            existingTokenDisp.setFechaInicio(tokenDisp.getFechaInicio());
                        }
                        if (tokenDisp.getFechaFin() != null) {
                            existingTokenDisp.setFechaFin(tokenDisp.getFechaFin());
                        }

                        return existingTokenDisp;
                    })
                    .flatMap(tokenDispRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /token-disps} : get all the tokenDisps.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tokenDisps in body.
     */
    @GetMapping("/token-disps")
    public Mono<ResponseEntity<List<TokenDisp>>> getAllTokenDisps(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of TokenDisps");
        return tokenDispRepository
            .count()
            .zipWith(tokenDispRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /token-disps/:id} : get the "id" tokenDisp.
     *
     * @param id the id of the tokenDisp to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tokenDisp, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/token-disps/{id}")
    public Mono<ResponseEntity<TokenDisp>> getTokenDisp(@PathVariable Long id) {
        log.debug("REST request to get TokenDisp : {}", id);
        Mono<TokenDisp> tokenDisp = tokenDispRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(tokenDisp);
    }

    /**
     * {@code DELETE  /token-disps/:id} : delete the "id" tokenDisp.
     *
     * @param id the id of the tokenDisp to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/token-disps/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteTokenDisp(@PathVariable Long id) {
        log.debug("REST request to delete TokenDisp : {}", id);
        return tokenDispRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
