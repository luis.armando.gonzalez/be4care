package com.be4tech.be4care.manilla.repository;

import com.be4tech.be4care.manilla.domain.Dispositivo;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Dispositivo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DispositivoRepository extends R2dbcRepository<Dispositivo, Long>, DispositivoRepositoryInternal {
    Flux<Dispositivo> findAllBy(Pageable pageable);

    @Query("SELECT * FROM dispositivo entity WHERE entity.user_id = :id")
    Flux<Dispositivo> findByUser(Long id);

    @Query("SELECT * FROM dispositivo entity WHERE entity.user_id IS NULL")
    Flux<Dispositivo> findAllWhereUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Dispositivo> findAll();

    @Override
    Mono<Dispositivo> findById(Long id);

    @Override
    <S extends Dispositivo> Mono<S> save(S entity);
}

interface DispositivoRepositoryInternal {
    <S extends Dispositivo> Mono<S> insert(S entity);
    <S extends Dispositivo> Mono<S> save(S entity);
    Mono<Integer> update(Dispositivo entity);

    Flux<Dispositivo> findAll();
    Mono<Dispositivo> findById(Long id);
    Flux<Dispositivo> findAllBy(Pageable pageable);
    Flux<Dispositivo> findAllBy(Pageable pageable, Criteria criteria);
}
