package com.be4tech.be4care.manilla.web.rest;

import com.be4tech.be4care.manilla.domain.Oximetria;
import com.be4tech.be4care.manilla.repository.OximetriaRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.manilla.domain.Oximetria}.
 */
@RestController
@RequestMapping("/api")
public class OximetriaResource {

    private final Logger log = LoggerFactory.getLogger(OximetriaResource.class);

    private static final String ENTITY_NAME = "manillaOximetria";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OximetriaRepository oximetriaRepository;

    private final UserRepository userRepository;

    public OximetriaResource(OximetriaRepository oximetriaRepository, UserRepository userRepository) {
        this.oximetriaRepository = oximetriaRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /oximetrias} : Create a new oximetria.
     *
     * @param oximetria the oximetria to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new oximetria, or with status {@code 400 (Bad Request)} if the oximetria has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/oximetrias")
    public Mono<ResponseEntity<Oximetria>> createOximetria(@RequestBody Oximetria oximetria) throws URISyntaxException {
        log.debug("REST request to save Oximetria : {}", oximetria);
        if (oximetria.getId() != null) {
            throw new BadRequestAlertException("A new oximetria cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (oximetria.getUser() != null) {
            // Save user in case it's new and only exists in gateway
            userRepository.save(oximetria.getUser());
        }
        return oximetriaRepository
            .save(oximetria)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/oximetrias/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /oximetrias/:id} : Updates an existing oximetria.
     *
     * @param id the id of the oximetria to save.
     * @param oximetria the oximetria to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated oximetria,
     * or with status {@code 400 (Bad Request)} if the oximetria is not valid,
     * or with status {@code 500 (Internal Server Error)} if the oximetria couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/oximetrias/{id}")
    public Mono<ResponseEntity<Oximetria>> updateOximetria(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Oximetria oximetria
    ) throws URISyntaxException {
        log.debug("REST request to update Oximetria : {}, {}", id, oximetria);
        if (oximetria.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, oximetria.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return oximetriaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (oximetria.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(oximetria.getUser());
                }
                return oximetriaRepository
                    .save(oximetria)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /oximetrias/:id} : Partial updates given fields of an existing oximetria, field will ignore if it is null
     *
     * @param id the id of the oximetria to save.
     * @param oximetria the oximetria to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated oximetria,
     * or with status {@code 400 (Bad Request)} if the oximetria is not valid,
     * or with status {@code 404 (Not Found)} if the oximetria is not found,
     * or with status {@code 500 (Internal Server Error)} if the oximetria couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/oximetrias/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Oximetria>> partialUpdateOximetria(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Oximetria oximetria
    ) throws URISyntaxException {
        log.debug("REST request to partial update Oximetria partially : {}, {}", id, oximetria);
        if (oximetria.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, oximetria.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return oximetriaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (oximetria.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(oximetria.getUser());
                }

                Mono<Oximetria> result = oximetriaRepository
                    .findById(oximetria.getId())
                    .map(existingOximetria -> {
                        if (oximetria.getOximetria() != null) {
                            existingOximetria.setOximetria(oximetria.getOximetria());
                        }
                        if (oximetria.getFechaRegistro() != null) {
                            existingOximetria.setFechaRegistro(oximetria.getFechaRegistro());
                        }

                        return existingOximetria;
                    })
                    .flatMap(oximetriaRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /oximetrias} : get all the oximetrias.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of oximetrias in body.
     */
    @GetMapping("/oximetrias")
    public Mono<ResponseEntity<List<Oximetria>>> getAllOximetrias(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Oximetrias");
        return oximetriaRepository
            .count()
            .zipWith(oximetriaRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /oximetrias/:id} : get the "id" oximetria.
     *
     * @param id the id of the oximetria to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the oximetria, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/oximetrias/{id}")
    public Mono<ResponseEntity<Oximetria>> getOximetria(@PathVariable Long id) {
        log.debug("REST request to get Oximetria : {}", id);
        Mono<Oximetria> oximetria = oximetriaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(oximetria);
    }

    /**
     * {@code DELETE  /oximetrias/:id} : delete the "id" oximetria.
     *
     * @param id the id of the oximetria to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/oximetrias/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteOximetria(@PathVariable Long id) {
        log.debug("REST request to delete Oximetria : {}", id);
        return oximetriaRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
