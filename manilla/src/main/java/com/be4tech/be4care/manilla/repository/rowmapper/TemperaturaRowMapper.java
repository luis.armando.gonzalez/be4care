package com.be4tech.be4care.manilla.repository.rowmapper;

import com.be4tech.be4care.manilla.domain.Temperatura;
import com.be4tech.be4care.manilla.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Temperatura}, with proper type conversions.
 */
@Service
public class TemperaturaRowMapper implements BiFunction<Row, String, Temperatura> {

    private final ColumnConverter converter;

    public TemperaturaRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Temperatura} stored in the database.
     */
    @Override
    public Temperatura apply(Row row, String prefix) {
        Temperatura entity = new Temperatura();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setTemperatura(converter.fromRow(row, prefix + "_temperatura", Float.class));
        entity.setFechaRegistro(converter.fromRow(row, prefix + "_fecha_registro", Instant.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        return entity;
    }
}
