package com.be4tech.be4care.manilla.repository.rowmapper;

import com.be4tech.be4care.manilla.domain.TokenDisp;
import com.be4tech.be4care.manilla.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link TokenDisp}, with proper type conversions.
 */
@Service
public class TokenDispRowMapper implements BiFunction<Row, String, TokenDisp> {

    private final ColumnConverter converter;

    public TokenDispRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link TokenDisp} stored in the database.
     */
    @Override
    public TokenDisp apply(Row row, String prefix) {
        TokenDisp entity = new TokenDisp();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setTokenConexion(converter.fromRow(row, prefix + "_token_conexion", String.class));
        entity.setActivo(converter.fromRow(row, prefix + "_activo", Boolean.class));
        entity.setFechaInicio(converter.fromRow(row, prefix + "_fecha_inicio", Instant.class));
        entity.setFechaFin(converter.fromRow(row, prefix + "_fecha_fin", ZonedDateTime.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        return entity;
    }
}
