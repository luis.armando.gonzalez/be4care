package com.be4tech.be4care.manilla.domain;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Dispositivo.
 */
@Table("dispositivo")
public class Dispositivo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("dispositivo")
    private String dispositivo;

    @Column("mac")
    private String mac;

    @Column("conectado")
    private Boolean conectado;

    @Transient
    private User user;

    @Column("user_id")
    private String userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Dispositivo id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDispositivo() {
        return this.dispositivo;
    }

    public Dispositivo dispositivo(String dispositivo) {
        this.setDispositivo(dispositivo);
        return this;
    }

    public void setDispositivo(String dispositivo) {
        this.dispositivo = dispositivo;
    }

    public String getMac() {
        return this.mac;
    }

    public Dispositivo mac(String mac) {
        this.setMac(mac);
        return this;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public Boolean getConectado() {
        return this.conectado;
    }

    public Dispositivo conectado(Boolean conectado) {
        this.setConectado(conectado);
        return this;
    }

    public void setConectado(Boolean conectado) {
        this.conectado = conectado;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user != null ? user.getId() : null;
    }

    public Dispositivo user(User user) {
        this.setUser(user);
        return this;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String user) {
        this.userId = user;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Dispositivo)) {
            return false;
        }
        return id != null && id.equals(((Dispositivo) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Dispositivo{" +
            "id=" + getId() +
            ", dispositivo='" + getDispositivo() + "'" +
            ", mac='" + getMac() + "'" +
            ", conectado='" + getConectado() + "'" +
            "}";
    }
}
