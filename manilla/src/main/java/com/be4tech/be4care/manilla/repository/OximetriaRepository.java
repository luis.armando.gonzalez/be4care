package com.be4tech.be4care.manilla.repository;

import com.be4tech.be4care.manilla.domain.Oximetria;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Oximetria entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OximetriaRepository extends R2dbcRepository<Oximetria, Long>, OximetriaRepositoryInternal {
    Flux<Oximetria> findAllBy(Pageable pageable);

    @Query("SELECT * FROM oximetria entity WHERE entity.user_id = :id")
    Flux<Oximetria> findByUser(Long id);

    @Query("SELECT * FROM oximetria entity WHERE entity.user_id IS NULL")
    Flux<Oximetria> findAllWhereUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Oximetria> findAll();

    @Override
    Mono<Oximetria> findById(Long id);

    @Override
    <S extends Oximetria> Mono<S> save(S entity);
}

interface OximetriaRepositoryInternal {
    <S extends Oximetria> Mono<S> insert(S entity);
    <S extends Oximetria> Mono<S> save(S entity);
    Mono<Integer> update(Oximetria entity);

    Flux<Oximetria> findAll();
    Mono<Oximetria> findById(Long id);
    Flux<Oximetria> findAllBy(Pageable pageable);
    Flux<Oximetria> findAllBy(Pageable pageable, Criteria criteria);
}
