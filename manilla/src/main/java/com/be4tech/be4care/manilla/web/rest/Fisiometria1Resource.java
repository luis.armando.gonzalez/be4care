package com.be4tech.be4care.manilla.web.rest;

import com.be4tech.be4care.manilla.domain.Fisiometria1;
import com.be4tech.be4care.manilla.repository.Fisiometria1Repository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.manilla.domain.Fisiometria1}.
 */
@RestController
@RequestMapping("/api")
public class Fisiometria1Resource {

    private final Logger log = LoggerFactory.getLogger(Fisiometria1Resource.class);

    private static final String ENTITY_NAME = "manillaFisiometria1";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final Fisiometria1Repository fisiometria1Repository;

    private final UserRepository userRepository;

    public Fisiometria1Resource(Fisiometria1Repository fisiometria1Repository, UserRepository userRepository) {
        this.fisiometria1Repository = fisiometria1Repository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /fisiometria-1-s} : Create a new fisiometria1.
     *
     * @param fisiometria1 the fisiometria1 to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fisiometria1, or with status {@code 400 (Bad Request)} if the fisiometria1 has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fisiometria-1-s")
    public Mono<ResponseEntity<Fisiometria1>> createFisiometria1(@RequestBody Fisiometria1 fisiometria1) throws URISyntaxException {
        log.debug("REST request to save Fisiometria1 : {}", fisiometria1);
        if (fisiometria1.getId() != null) {
            throw new BadRequestAlertException("A new fisiometria1 cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (fisiometria1.getUser() != null) {
            // Save user in case it's new and only exists in gateway
            userRepository.save(fisiometria1.getUser());
        }
        return fisiometria1Repository
            .save(fisiometria1)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/fisiometria-1-s/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /fisiometria-1-s/:id} : Updates an existing fisiometria1.
     *
     * @param id the id of the fisiometria1 to save.
     * @param fisiometria1 the fisiometria1 to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fisiometria1,
     * or with status {@code 400 (Bad Request)} if the fisiometria1 is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fisiometria1 couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fisiometria-1-s/{id}")
    public Mono<ResponseEntity<Fisiometria1>> updateFisiometria1(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Fisiometria1 fisiometria1
    ) throws URISyntaxException {
        log.debug("REST request to update Fisiometria1 : {}, {}", id, fisiometria1);
        if (fisiometria1.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, fisiometria1.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return fisiometria1Repository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (fisiometria1.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(fisiometria1.getUser());
                }
                return fisiometria1Repository
                    .save(fisiometria1)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /fisiometria-1-s/:id} : Partial updates given fields of an existing fisiometria1, field will ignore if it is null
     *
     * @param id the id of the fisiometria1 to save.
     * @param fisiometria1 the fisiometria1 to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fisiometria1,
     * or with status {@code 400 (Bad Request)} if the fisiometria1 is not valid,
     * or with status {@code 404 (Not Found)} if the fisiometria1 is not found,
     * or with status {@code 500 (Internal Server Error)} if the fisiometria1 couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/fisiometria-1-s/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Fisiometria1>> partialUpdateFisiometria1(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Fisiometria1 fisiometria1
    ) throws URISyntaxException {
        log.debug("REST request to partial update Fisiometria1 partially : {}, {}", id, fisiometria1);
        if (fisiometria1.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, fisiometria1.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return fisiometria1Repository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (fisiometria1.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(fisiometria1.getUser());
                }

                Mono<Fisiometria1> result = fisiometria1Repository
                    .findById(fisiometria1.getId())
                    .map(existingFisiometria1 -> {
                        if (fisiometria1.getRitmoCardiaco() != null) {
                            existingFisiometria1.setRitmoCardiaco(fisiometria1.getRitmoCardiaco());
                        }
                        if (fisiometria1.getRitmoRespiratorio() != null) {
                            existingFisiometria1.setRitmoRespiratorio(fisiometria1.getRitmoRespiratorio());
                        }
                        if (fisiometria1.getOximetria() != null) {
                            existingFisiometria1.setOximetria(fisiometria1.getOximetria());
                        }
                        if (fisiometria1.getPresionArterialSistolica() != null) {
                            existingFisiometria1.setPresionArterialSistolica(fisiometria1.getPresionArterialSistolica());
                        }
                        if (fisiometria1.getPresionArterialDiastolica() != null) {
                            existingFisiometria1.setPresionArterialDiastolica(fisiometria1.getPresionArterialDiastolica());
                        }
                        if (fisiometria1.getTemperatura() != null) {
                            existingFisiometria1.setTemperatura(fisiometria1.getTemperatura());
                        }
                        if (fisiometria1.getFechaRegistro() != null) {
                            existingFisiometria1.setFechaRegistro(fisiometria1.getFechaRegistro());
                        }
                        if (fisiometria1.getFechaToma() != null) {
                            existingFisiometria1.setFechaToma(fisiometria1.getFechaToma());
                        }

                        return existingFisiometria1;
                    })
                    .flatMap(fisiometria1Repository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /fisiometria-1-s} : get all the fisiometria1s.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fisiometria1s in body.
     */
    @GetMapping("/fisiometria-1-s")
    public Mono<ResponseEntity<List<Fisiometria1>>> getAllFisiometria1s(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Fisiometria1s");
        return fisiometria1Repository
            .count()
            .zipWith(fisiometria1Repository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /fisiometria-1-s/:id} : get the "id" fisiometria1.
     *
     * @param id the id of the fisiometria1 to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fisiometria1, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fisiometria-1-s/{id}")
    public Mono<ResponseEntity<Fisiometria1>> getFisiometria1(@PathVariable Long id) {
        log.debug("REST request to get Fisiometria1 : {}", id);
        Mono<Fisiometria1> fisiometria1 = fisiometria1Repository.findById(id);
        return ResponseUtil.wrapOrNotFound(fisiometria1);
    }

    /**
     * {@code DELETE  /fisiometria-1-s/:id} : delete the "id" fisiometria1.
     *
     * @param id the id of the fisiometria1 to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fisiometria-1-s/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteFisiometria1(@PathVariable Long id) {
        log.debug("REST request to delete Fisiometria1 : {}", id);
        return fisiometria1Repository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
