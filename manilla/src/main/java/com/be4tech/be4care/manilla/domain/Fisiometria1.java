package com.be4tech.be4care.manilla.domain;

import java.io.Serializable;
import java.time.Instant;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Fisiometria1.
 */
@Table("fisiometria_1")
public class Fisiometria1 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("ritmo_cardiaco")
    private Integer ritmoCardiaco;

    @Column("ritmo_respiratorio")
    private Integer ritmoRespiratorio;

    @Column("oximetria")
    private Integer oximetria;

    @Column("presion_arterial_sistolica")
    private Integer presionArterialSistolica;

    @Column("presion_arterial_diastolica")
    private Integer presionArterialDiastolica;

    @Column("temperatura")
    private Float temperatura;

    @Column("fecha_registro")
    private Instant fechaRegistro;

    @Column("fecha_toma")
    private Instant fechaToma;

    @Transient
    private User user;

    @Column("user_id")
    private String userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Fisiometria1 id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRitmoCardiaco() {
        return this.ritmoCardiaco;
    }

    public Fisiometria1 ritmoCardiaco(Integer ritmoCardiaco) {
        this.setRitmoCardiaco(ritmoCardiaco);
        return this;
    }

    public void setRitmoCardiaco(Integer ritmoCardiaco) {
        this.ritmoCardiaco = ritmoCardiaco;
    }

    public Integer getRitmoRespiratorio() {
        return this.ritmoRespiratorio;
    }

    public Fisiometria1 ritmoRespiratorio(Integer ritmoRespiratorio) {
        this.setRitmoRespiratorio(ritmoRespiratorio);
        return this;
    }

    public void setRitmoRespiratorio(Integer ritmoRespiratorio) {
        this.ritmoRespiratorio = ritmoRespiratorio;
    }

    public Integer getOximetria() {
        return this.oximetria;
    }

    public Fisiometria1 oximetria(Integer oximetria) {
        this.setOximetria(oximetria);
        return this;
    }

    public void setOximetria(Integer oximetria) {
        this.oximetria = oximetria;
    }

    public Integer getPresionArterialSistolica() {
        return this.presionArterialSistolica;
    }

    public Fisiometria1 presionArterialSistolica(Integer presionArterialSistolica) {
        this.setPresionArterialSistolica(presionArterialSistolica);
        return this;
    }

    public void setPresionArterialSistolica(Integer presionArterialSistolica) {
        this.presionArterialSistolica = presionArterialSistolica;
    }

    public Integer getPresionArterialDiastolica() {
        return this.presionArterialDiastolica;
    }

    public Fisiometria1 presionArterialDiastolica(Integer presionArterialDiastolica) {
        this.setPresionArterialDiastolica(presionArterialDiastolica);
        return this;
    }

    public void setPresionArterialDiastolica(Integer presionArterialDiastolica) {
        this.presionArterialDiastolica = presionArterialDiastolica;
    }

    public Float getTemperatura() {
        return this.temperatura;
    }

    public Fisiometria1 temperatura(Float temperatura) {
        this.setTemperatura(temperatura);
        return this;
    }

    public void setTemperatura(Float temperatura) {
        this.temperatura = temperatura;
    }

    public Instant getFechaRegistro() {
        return this.fechaRegistro;
    }

    public Fisiometria1 fechaRegistro(Instant fechaRegistro) {
        this.setFechaRegistro(fechaRegistro);
        return this;
    }

    public void setFechaRegistro(Instant fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Instant getFechaToma() {
        return this.fechaToma;
    }

    public Fisiometria1 fechaToma(Instant fechaToma) {
        this.setFechaToma(fechaToma);
        return this;
    }

    public void setFechaToma(Instant fechaToma) {
        this.fechaToma = fechaToma;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user != null ? user.getId() : null;
    }

    public Fisiometria1 user(User user) {
        this.setUser(user);
        return this;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String user) {
        this.userId = user;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Fisiometria1)) {
            return false;
        }
        return id != null && id.equals(((Fisiometria1) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Fisiometria1{" +
            "id=" + getId() +
            ", ritmoCardiaco=" + getRitmoCardiaco() +
            ", ritmoRespiratorio=" + getRitmoRespiratorio() +
            ", oximetria=" + getOximetria() +
            ", presionArterialSistolica=" + getPresionArterialSistolica() +
            ", presionArterialDiastolica=" + getPresionArterialDiastolica() +
            ", temperatura=" + getTemperatura() +
            ", fechaRegistro='" + getFechaRegistro() + "'" +
            ", fechaToma='" + getFechaToma() + "'" +
            "}";
    }
}
