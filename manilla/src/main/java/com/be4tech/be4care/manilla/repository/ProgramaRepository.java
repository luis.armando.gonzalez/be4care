package com.be4tech.be4care.manilla.repository;

import com.be4tech.be4care.manilla.domain.Programa;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Programa entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramaRepository extends R2dbcRepository<Programa, Long>, ProgramaRepositoryInternal {
    Flux<Programa> findAllBy(Pageable pageable);

    @Query("SELECT * FROM programa entity WHERE entity.user_id = :id")
    Flux<Programa> findByUser(Long id);

    @Query("SELECT * FROM programa entity WHERE entity.user_id IS NULL")
    Flux<Programa> findAllWhereUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Programa> findAll();

    @Override
    Mono<Programa> findById(Long id);

    @Override
    <S extends Programa> Mono<S> save(S entity);
}

interface ProgramaRepositoryInternal {
    <S extends Programa> Mono<S> insert(S entity);
    <S extends Programa> Mono<S> save(S entity);
    Mono<Integer> update(Programa entity);

    Flux<Programa> findAll();
    Mono<Programa> findById(Long id);
    Flux<Programa> findAllBy(Pageable pageable);
    Flux<Programa> findAllBy(Pageable pageable, Criteria criteria);
}
