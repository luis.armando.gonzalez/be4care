package com.be4tech.be4care.manilla.repository.rowmapper;

import com.be4tech.be4care.manilla.domain.PresionSanguinea;
import com.be4tech.be4care.manilla.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link PresionSanguinea}, with proper type conversions.
 */
@Service
public class PresionSanguineaRowMapper implements BiFunction<Row, String, PresionSanguinea> {

    private final ColumnConverter converter;

    public PresionSanguineaRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link PresionSanguinea} stored in the database.
     */
    @Override
    public PresionSanguinea apply(Row row, String prefix) {
        PresionSanguinea entity = new PresionSanguinea();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setPresionSanguineaSistolica(converter.fromRow(row, prefix + "_presion_sanguinea_sistolica", Integer.class));
        entity.setPresionSanguineaDiastolica(converter.fromRow(row, prefix + "_presion_sanguinea_diastolica", Integer.class));
        entity.setFechaRegistro(converter.fromRow(row, prefix + "_fecha_registro", Instant.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        return entity;
    }
}
