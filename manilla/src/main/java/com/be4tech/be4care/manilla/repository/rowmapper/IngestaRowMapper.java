package com.be4tech.be4care.manilla.repository.rowmapper;

import com.be4tech.be4care.manilla.domain.Ingesta;
import com.be4tech.be4care.manilla.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Ingesta}, with proper type conversions.
 */
@Service
public class IngestaRowMapper implements BiFunction<Row, String, Ingesta> {

    private final ColumnConverter converter;

    public IngestaRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Ingesta} stored in the database.
     */
    @Override
    public Ingesta apply(Row row, String prefix) {
        Ingesta entity = new Ingesta();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setTipo(converter.fromRow(row, prefix + "_tipo", String.class));
        entity.setConsumoCalorias(converter.fromRow(row, prefix + "_consumo_calorias", Integer.class));
        entity.setFechaRegistro(converter.fromRow(row, prefix + "_fecha_registro", Instant.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        return entity;
    }
}
