package com.be4tech.be4care.manilla.repository;

import com.be4tech.be4care.manilla.domain.Ingesta;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Ingesta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IngestaRepository extends R2dbcRepository<Ingesta, Long>, IngestaRepositoryInternal {
    Flux<Ingesta> findAllBy(Pageable pageable);

    @Query("SELECT * FROM ingesta entity WHERE entity.user_id = :id")
    Flux<Ingesta> findByUser(Long id);

    @Query("SELECT * FROM ingesta entity WHERE entity.user_id IS NULL")
    Flux<Ingesta> findAllWhereUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Ingesta> findAll();

    @Override
    Mono<Ingesta> findById(Long id);

    @Override
    <S extends Ingesta> Mono<S> save(S entity);
}

interface IngestaRepositoryInternal {
    <S extends Ingesta> Mono<S> insert(S entity);
    <S extends Ingesta> Mono<S> save(S entity);
    Mono<Integer> update(Ingesta entity);

    Flux<Ingesta> findAll();
    Mono<Ingesta> findById(Long id);
    Flux<Ingesta> findAllBy(Pageable pageable);
    Flux<Ingesta> findAllBy(Pageable pageable, Criteria criteria);
}
