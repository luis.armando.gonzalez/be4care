package com.be4tech.be4care.manilla.web.rest;

import com.be4tech.be4care.manilla.domain.PresionSanguinea;
import com.be4tech.be4care.manilla.repository.PresionSanguineaRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.manilla.domain.PresionSanguinea}.
 */
@RestController
@RequestMapping("/api")
public class PresionSanguineaResource {

    private final Logger log = LoggerFactory.getLogger(PresionSanguineaResource.class);

    private static final String ENTITY_NAME = "manillaPresionSanguinea";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PresionSanguineaRepository presionSanguineaRepository;

    private final UserRepository userRepository;

    public PresionSanguineaResource(PresionSanguineaRepository presionSanguineaRepository, UserRepository userRepository) {
        this.presionSanguineaRepository = presionSanguineaRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /presion-sanguineas} : Create a new presionSanguinea.
     *
     * @param presionSanguinea the presionSanguinea to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new presionSanguinea, or with status {@code 400 (Bad Request)} if the presionSanguinea has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/presion-sanguineas")
    public Mono<ResponseEntity<PresionSanguinea>> createPresionSanguinea(@RequestBody PresionSanguinea presionSanguinea)
        throws URISyntaxException {
        log.debug("REST request to save PresionSanguinea : {}", presionSanguinea);
        if (presionSanguinea.getId() != null) {
            throw new BadRequestAlertException("A new presionSanguinea cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (presionSanguinea.getUser() != null) {
            // Save user in case it's new and only exists in gateway
            userRepository.save(presionSanguinea.getUser());
        }
        return presionSanguineaRepository
            .save(presionSanguinea)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/presion-sanguineas/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /presion-sanguineas/:id} : Updates an existing presionSanguinea.
     *
     * @param id the id of the presionSanguinea to save.
     * @param presionSanguinea the presionSanguinea to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated presionSanguinea,
     * or with status {@code 400 (Bad Request)} if the presionSanguinea is not valid,
     * or with status {@code 500 (Internal Server Error)} if the presionSanguinea couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/presion-sanguineas/{id}")
    public Mono<ResponseEntity<PresionSanguinea>> updatePresionSanguinea(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PresionSanguinea presionSanguinea
    ) throws URISyntaxException {
        log.debug("REST request to update PresionSanguinea : {}, {}", id, presionSanguinea);
        if (presionSanguinea.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, presionSanguinea.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return presionSanguineaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (presionSanguinea.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(presionSanguinea.getUser());
                }
                return presionSanguineaRepository
                    .save(presionSanguinea)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /presion-sanguineas/:id} : Partial updates given fields of an existing presionSanguinea, field will ignore if it is null
     *
     * @param id the id of the presionSanguinea to save.
     * @param presionSanguinea the presionSanguinea to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated presionSanguinea,
     * or with status {@code 400 (Bad Request)} if the presionSanguinea is not valid,
     * or with status {@code 404 (Not Found)} if the presionSanguinea is not found,
     * or with status {@code 500 (Internal Server Error)} if the presionSanguinea couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/presion-sanguineas/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<PresionSanguinea>> partialUpdatePresionSanguinea(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PresionSanguinea presionSanguinea
    ) throws URISyntaxException {
        log.debug("REST request to partial update PresionSanguinea partially : {}, {}", id, presionSanguinea);
        if (presionSanguinea.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, presionSanguinea.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return presionSanguineaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (presionSanguinea.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(presionSanguinea.getUser());
                }

                Mono<PresionSanguinea> result = presionSanguineaRepository
                    .findById(presionSanguinea.getId())
                    .map(existingPresionSanguinea -> {
                        if (presionSanguinea.getPresionSanguineaSistolica() != null) {
                            existingPresionSanguinea.setPresionSanguineaSistolica(presionSanguinea.getPresionSanguineaSistolica());
                        }
                        if (presionSanguinea.getPresionSanguineaDiastolica() != null) {
                            existingPresionSanguinea.setPresionSanguineaDiastolica(presionSanguinea.getPresionSanguineaDiastolica());
                        }
                        if (presionSanguinea.getFechaRegistro() != null) {
                            existingPresionSanguinea.setFechaRegistro(presionSanguinea.getFechaRegistro());
                        }

                        return existingPresionSanguinea;
                    })
                    .flatMap(presionSanguineaRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /presion-sanguineas} : get all the presionSanguineas.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of presionSanguineas in body.
     */
    @GetMapping("/presion-sanguineas")
    public Mono<ResponseEntity<List<PresionSanguinea>>> getAllPresionSanguineas(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of PresionSanguineas");
        return presionSanguineaRepository
            .count()
            .zipWith(presionSanguineaRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /presion-sanguineas/:id} : get the "id" presionSanguinea.
     *
     * @param id the id of the presionSanguinea to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the presionSanguinea, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/presion-sanguineas/{id}")
    public Mono<ResponseEntity<PresionSanguinea>> getPresionSanguinea(@PathVariable Long id) {
        log.debug("REST request to get PresionSanguinea : {}", id);
        Mono<PresionSanguinea> presionSanguinea = presionSanguineaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(presionSanguinea);
    }

    /**
     * {@code DELETE  /presion-sanguineas/:id} : delete the "id" presionSanguinea.
     *
     * @param id the id of the presionSanguinea to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/presion-sanguineas/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deletePresionSanguinea(@PathVariable Long id) {
        log.debug("REST request to delete PresionSanguinea : {}", id);
        return presionSanguineaRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
