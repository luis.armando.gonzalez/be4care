package com.be4tech.be4care.manilla.repository.rowmapper;

import com.be4tech.be4care.manilla.domain.FrecuenciaCardiaca;
import com.be4tech.be4care.manilla.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link FrecuenciaCardiaca}, with proper type conversions.
 */
@Service
public class FrecuenciaCardiacaRowMapper implements BiFunction<Row, String, FrecuenciaCardiaca> {

    private final ColumnConverter converter;

    public FrecuenciaCardiacaRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link FrecuenciaCardiaca} stored in the database.
     */
    @Override
    public FrecuenciaCardiaca apply(Row row, String prefix) {
        FrecuenciaCardiaca entity = new FrecuenciaCardiaca();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setFrecuenciaCardiaca(converter.fromRow(row, prefix + "_frecuencia_cardiaca", Integer.class));
        entity.setFechaRegistro(converter.fromRow(row, prefix + "_fecha_registro", Instant.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        return entity;
    }
}
