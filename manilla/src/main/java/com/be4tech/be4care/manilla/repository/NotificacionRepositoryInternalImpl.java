package com.be4tech.be4care.manilla.repository;

import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

import com.be4tech.be4care.manilla.domain.Notificacion;
import com.be4tech.be4care.manilla.repository.rowmapper.NotificacionRowMapper;
import com.be4tech.be4care.manilla.repository.rowmapper.TokenDispRowMapper;
import com.be4tech.be4care.manilla.service.EntityManager;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoinCondition;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive custom repository implementation for the Notificacion entity.
 */
@SuppressWarnings("unused")
class NotificacionRepositoryInternalImpl implements NotificacionRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final TokenDispRowMapper tokendispMapper;
    private final NotificacionRowMapper notificacionMapper;

    private static final Table entityTable = Table.aliased("notificacion", EntityManager.ENTITY_ALIAS);
    private static final Table tokenTable = Table.aliased("token_disp", "token");

    public NotificacionRepositoryInternalImpl(
        R2dbcEntityTemplate template,
        EntityManager entityManager,
        TokenDispRowMapper tokendispMapper,
        NotificacionRowMapper notificacionMapper
    ) {
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.tokendispMapper = tokendispMapper;
        this.notificacionMapper = notificacionMapper;
    }

    @Override
    public Flux<Notificacion> findAllBy(Pageable pageable) {
        return findAllBy(pageable, null);
    }

    @Override
    public Flux<Notificacion> findAllBy(Pageable pageable, Criteria criteria) {
        return createQuery(pageable, criteria).all();
    }

    RowsFetchSpec<Notificacion> createQuery(Pageable pageable, Criteria criteria) {
        List<Expression> columns = NotificacionSqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        columns.addAll(TokenDispSqlHelper.getColumns(tokenTable, "token"));
        SelectFromAndJoinCondition selectFrom = Select
            .builder()
            .select(columns)
            .from(entityTable)
            .leftOuterJoin(tokenTable)
            .on(Column.create("token_id", entityTable))
            .equals(Column.create("id", tokenTable));

        String select = entityManager.createSelect(selectFrom, Notificacion.class, pageable, criteria);
        String alias = entityTable.getReferenceName().getReference();
        String selectWhere = Optional
            .ofNullable(criteria)
            .map(crit ->
                new StringBuilder(select)
                    .append(" ")
                    .append("WHERE")
                    .append(" ")
                    .append(alias)
                    .append(".")
                    .append(crit.toString())
                    .toString()
            )
            .orElse(select); // TODO remove once https://github.com/spring-projects/spring-data-jdbc/issues/907 will be fixed
        return db.sql(selectWhere).map(this::process);
    }

    @Override
    public Flux<Notificacion> findAll() {
        return findAllBy(null, null);
    }

    @Override
    public Mono<Notificacion> findById(Long id) {
        return createQuery(null, where("id").is(id)).one();
    }

    private Notificacion process(Row row, RowMetadata metadata) {
        Notificacion entity = notificacionMapper.apply(row, "e");
        entity.setToken(tokendispMapper.apply(row, "token"));
        return entity;
    }

    @Override
    public <S extends Notificacion> Mono<S> insert(S entity) {
        return entityManager.insert(entity);
    }

    @Override
    public <S extends Notificacion> Mono<S> save(S entity) {
        if (entity.getId() == null) {
            return insert(entity);
        } else {
            return update(entity)
                .map(numberOfUpdates -> {
                    if (numberOfUpdates.intValue() <= 0) {
                        throw new IllegalStateException("Unable to update Notificacion with id = " + entity.getId());
                    }
                    return entity;
                });
        }
    }

    @Override
    public Mono<Integer> update(Notificacion entity) {
        //fixme is this the proper way?
        return r2dbcEntityTemplate.update(entity).thenReturn(1);
    }
}
