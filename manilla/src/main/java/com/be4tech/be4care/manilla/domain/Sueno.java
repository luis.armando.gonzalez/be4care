package com.be4tech.be4care.manilla.domain;

import java.io.Serializable;
import java.time.Instant;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Sueno.
 */
@Table("sueno")
public class Sueno implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("superficial")
    private Integer superficial;

    @Column("profundo")
    private Integer profundo;

    @Column("despierto")
    private Integer despierto;

    @Column("time_instant")
    private Instant timeInstant;

    @Transient
    private User user;

    @Column("user_id")
    private String userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Sueno id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSuperficial() {
        return this.superficial;
    }

    public Sueno superficial(Integer superficial) {
        this.setSuperficial(superficial);
        return this;
    }

    public void setSuperficial(Integer superficial) {
        this.superficial = superficial;
    }

    public Integer getProfundo() {
        return this.profundo;
    }

    public Sueno profundo(Integer profundo) {
        this.setProfundo(profundo);
        return this;
    }

    public void setProfundo(Integer profundo) {
        this.profundo = profundo;
    }

    public Integer getDespierto() {
        return this.despierto;
    }

    public Sueno despierto(Integer despierto) {
        this.setDespierto(despierto);
        return this;
    }

    public void setDespierto(Integer despierto) {
        this.despierto = despierto;
    }

    public Instant getTimeInstant() {
        return this.timeInstant;
    }

    public Sueno timeInstant(Instant timeInstant) {
        this.setTimeInstant(timeInstant);
        return this;
    }

    public void setTimeInstant(Instant timeInstant) {
        this.timeInstant = timeInstant;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user != null ? user.getId() : null;
    }

    public Sueno user(User user) {
        this.setUser(user);
        return this;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String user) {
        this.userId = user;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Sueno)) {
            return false;
        }
        return id != null && id.equals(((Sueno) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Sueno{" +
            "id=" + getId() +
            ", superficial=" + getSuperficial() +
            ", profundo=" + getProfundo() +
            ", despierto=" + getDespierto() +
            ", timeInstant='" + getTimeInstant() + "'" +
            "}";
    }
}
