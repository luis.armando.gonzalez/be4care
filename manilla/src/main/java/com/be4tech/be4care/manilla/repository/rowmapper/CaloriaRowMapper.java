package com.be4tech.be4care.manilla.repository.rowmapper;

import com.be4tech.be4care.manilla.domain.Caloria;
import com.be4tech.be4care.manilla.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Caloria}, with proper type conversions.
 */
@Service
public class CaloriaRowMapper implements BiFunction<Row, String, Caloria> {

    private final ColumnConverter converter;

    public CaloriaRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Caloria} stored in the database.
     */
    @Override
    public Caloria apply(Row row, String prefix) {
        Caloria entity = new Caloria();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setCaloriasActivas(converter.fromRow(row, prefix + "_calorias_activas", Integer.class));
        entity.setDescripcion(converter.fromRow(row, prefix + "_descripcion", String.class));
        entity.setFechaRegistro(converter.fromRow(row, prefix + "_fecha_registro", Instant.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        return entity;
    }
}
