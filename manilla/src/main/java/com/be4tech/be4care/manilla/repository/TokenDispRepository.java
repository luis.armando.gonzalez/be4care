package com.be4tech.be4care.manilla.repository;

import com.be4tech.be4care.manilla.domain.TokenDisp;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the TokenDisp entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TokenDispRepository extends R2dbcRepository<TokenDisp, Long>, TokenDispRepositoryInternal {
    Flux<TokenDisp> findAllBy(Pageable pageable);

    @Query("SELECT * FROM token_disp entity WHERE entity.user_id = :id")
    Flux<TokenDisp> findByUser(Long id);

    @Query("SELECT * FROM token_disp entity WHERE entity.user_id IS NULL")
    Flux<TokenDisp> findAllWhereUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<TokenDisp> findAll();

    @Override
    Mono<TokenDisp> findById(Long id);

    @Override
    <S extends TokenDisp> Mono<S> save(S entity);
}

interface TokenDispRepositoryInternal {
    <S extends TokenDisp> Mono<S> insert(S entity);
    <S extends TokenDisp> Mono<S> save(S entity);
    Mono<Integer> update(TokenDisp entity);

    Flux<TokenDisp> findAll();
    Mono<TokenDisp> findById(Long id);
    Flux<TokenDisp> findAllBy(Pageable pageable);
    Flux<TokenDisp> findAllBy(Pageable pageable, Criteria criteria);
}
