package com.be4tech.be4care.manilla.web.rest;

import com.be4tech.be4care.manilla.domain.FrecuenciaCardiaca;
import com.be4tech.be4care.manilla.repository.FrecuenciaCardiacaRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.manilla.domain.FrecuenciaCardiaca}.
 */
@RestController
@RequestMapping("/api")
public class FrecuenciaCardiacaResource {

    private final Logger log = LoggerFactory.getLogger(FrecuenciaCardiacaResource.class);

    private static final String ENTITY_NAME = "manillaFrecuenciaCardiaca";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FrecuenciaCardiacaRepository frecuenciaCardiacaRepository;

    private final UserRepository userRepository;

    public FrecuenciaCardiacaResource(FrecuenciaCardiacaRepository frecuenciaCardiacaRepository, UserRepository userRepository) {
        this.frecuenciaCardiacaRepository = frecuenciaCardiacaRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /frecuencia-cardiacas} : Create a new frecuenciaCardiaca.
     *
     * @param frecuenciaCardiaca the frecuenciaCardiaca to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new frecuenciaCardiaca, or with status {@code 400 (Bad Request)} if the frecuenciaCardiaca has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/frecuencia-cardiacas")
    public Mono<ResponseEntity<FrecuenciaCardiaca>> createFrecuenciaCardiaca(@RequestBody FrecuenciaCardiaca frecuenciaCardiaca)
        throws URISyntaxException {
        log.debug("REST request to save FrecuenciaCardiaca : {}", frecuenciaCardiaca);
        if (frecuenciaCardiaca.getId() != null) {
            throw new BadRequestAlertException("A new frecuenciaCardiaca cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (frecuenciaCardiaca.getUser() != null) {
            // Save user in case it's new and only exists in gateway
            userRepository.save(frecuenciaCardiaca.getUser());
        }
        return frecuenciaCardiacaRepository
            .save(frecuenciaCardiaca)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/frecuencia-cardiacas/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /frecuencia-cardiacas/:id} : Updates an existing frecuenciaCardiaca.
     *
     * @param id the id of the frecuenciaCardiaca to save.
     * @param frecuenciaCardiaca the frecuenciaCardiaca to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated frecuenciaCardiaca,
     * or with status {@code 400 (Bad Request)} if the frecuenciaCardiaca is not valid,
     * or with status {@code 500 (Internal Server Error)} if the frecuenciaCardiaca couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/frecuencia-cardiacas/{id}")
    public Mono<ResponseEntity<FrecuenciaCardiaca>> updateFrecuenciaCardiaca(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody FrecuenciaCardiaca frecuenciaCardiaca
    ) throws URISyntaxException {
        log.debug("REST request to update FrecuenciaCardiaca : {}, {}", id, frecuenciaCardiaca);
        if (frecuenciaCardiaca.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, frecuenciaCardiaca.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return frecuenciaCardiacaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (frecuenciaCardiaca.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(frecuenciaCardiaca.getUser());
                }
                return frecuenciaCardiacaRepository
                    .save(frecuenciaCardiaca)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /frecuencia-cardiacas/:id} : Partial updates given fields of an existing frecuenciaCardiaca, field will ignore if it is null
     *
     * @param id the id of the frecuenciaCardiaca to save.
     * @param frecuenciaCardiaca the frecuenciaCardiaca to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated frecuenciaCardiaca,
     * or with status {@code 400 (Bad Request)} if the frecuenciaCardiaca is not valid,
     * or with status {@code 404 (Not Found)} if the frecuenciaCardiaca is not found,
     * or with status {@code 500 (Internal Server Error)} if the frecuenciaCardiaca couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/frecuencia-cardiacas/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<FrecuenciaCardiaca>> partialUpdateFrecuenciaCardiaca(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody FrecuenciaCardiaca frecuenciaCardiaca
    ) throws URISyntaxException {
        log.debug("REST request to partial update FrecuenciaCardiaca partially : {}, {}", id, frecuenciaCardiaca);
        if (frecuenciaCardiaca.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, frecuenciaCardiaca.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return frecuenciaCardiacaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (frecuenciaCardiaca.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(frecuenciaCardiaca.getUser());
                }

                Mono<FrecuenciaCardiaca> result = frecuenciaCardiacaRepository
                    .findById(frecuenciaCardiaca.getId())
                    .map(existingFrecuenciaCardiaca -> {
                        if (frecuenciaCardiaca.getFrecuenciaCardiaca() != null) {
                            existingFrecuenciaCardiaca.setFrecuenciaCardiaca(frecuenciaCardiaca.getFrecuenciaCardiaca());
                        }
                        if (frecuenciaCardiaca.getFechaRegistro() != null) {
                            existingFrecuenciaCardiaca.setFechaRegistro(frecuenciaCardiaca.getFechaRegistro());
                        }

                        return existingFrecuenciaCardiaca;
                    })
                    .flatMap(frecuenciaCardiacaRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /frecuencia-cardiacas} : get all the frecuenciaCardiacas.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of frecuenciaCardiacas in body.
     */
    @GetMapping("/frecuencia-cardiacas")
    public Mono<ResponseEntity<List<FrecuenciaCardiaca>>> getAllFrecuenciaCardiacas(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of FrecuenciaCardiacas");
        return frecuenciaCardiacaRepository
            .count()
            .zipWith(frecuenciaCardiacaRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /frecuencia-cardiacas/:id} : get the "id" frecuenciaCardiaca.
     *
     * @param id the id of the frecuenciaCardiaca to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the frecuenciaCardiaca, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/frecuencia-cardiacas/{id}")
    public Mono<ResponseEntity<FrecuenciaCardiaca>> getFrecuenciaCardiaca(@PathVariable Long id) {
        log.debug("REST request to get FrecuenciaCardiaca : {}", id);
        Mono<FrecuenciaCardiaca> frecuenciaCardiaca = frecuenciaCardiacaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(frecuenciaCardiaca);
    }

    /**
     * {@code DELETE  /frecuencia-cardiacas/:id} : delete the "id" frecuenciaCardiaca.
     *
     * @param id the id of the frecuenciaCardiaca to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/frecuencia-cardiacas/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteFrecuenciaCardiaca(@PathVariable Long id) {
        log.debug("REST request to delete FrecuenciaCardiaca : {}", id);
        return frecuenciaCardiacaRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
