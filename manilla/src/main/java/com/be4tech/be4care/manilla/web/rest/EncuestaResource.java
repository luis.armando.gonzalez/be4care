package com.be4tech.be4care.manilla.web.rest;

import com.be4tech.be4care.manilla.domain.Encuesta;
import com.be4tech.be4care.manilla.repository.EncuestaRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.manilla.domain.Encuesta}.
 */
@RestController
@RequestMapping("/api")
public class EncuestaResource {

    private final Logger log = LoggerFactory.getLogger(EncuestaResource.class);

    private static final String ENTITY_NAME = "manillaEncuesta";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EncuestaRepository encuestaRepository;

    private final UserRepository userRepository;

    public EncuestaResource(EncuestaRepository encuestaRepository, UserRepository userRepository) {
        this.encuestaRepository = encuestaRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /encuestas} : Create a new encuesta.
     *
     * @param encuesta the encuesta to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new encuesta, or with status {@code 400 (Bad Request)} if the encuesta has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/encuestas")
    public Mono<ResponseEntity<Encuesta>> createEncuesta(@RequestBody Encuesta encuesta) throws URISyntaxException {
        log.debug("REST request to save Encuesta : {}", encuesta);
        if (encuesta.getId() != null) {
            throw new BadRequestAlertException("A new encuesta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (encuesta.getUser() != null) {
            // Save user in case it's new and only exists in gateway
            userRepository.save(encuesta.getUser());
        }
        return encuestaRepository
            .save(encuesta)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/encuestas/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /encuestas/:id} : Updates an existing encuesta.
     *
     * @param id the id of the encuesta to save.
     * @param encuesta the encuesta to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated encuesta,
     * or with status {@code 400 (Bad Request)} if the encuesta is not valid,
     * or with status {@code 500 (Internal Server Error)} if the encuesta couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/encuestas/{id}")
    public Mono<ResponseEntity<Encuesta>> updateEncuesta(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Encuesta encuesta
    ) throws URISyntaxException {
        log.debug("REST request to update Encuesta : {}, {}", id, encuesta);
        if (encuesta.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, encuesta.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return encuestaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (encuesta.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(encuesta.getUser());
                }
                return encuestaRepository
                    .save(encuesta)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /encuestas/:id} : Partial updates given fields of an existing encuesta, field will ignore if it is null
     *
     * @param id the id of the encuesta to save.
     * @param encuesta the encuesta to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated encuesta,
     * or with status {@code 400 (Bad Request)} if the encuesta is not valid,
     * or with status {@code 404 (Not Found)} if the encuesta is not found,
     * or with status {@code 500 (Internal Server Error)} if the encuesta couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/encuestas/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Encuesta>> partialUpdateEncuesta(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Encuesta encuesta
    ) throws URISyntaxException {
        log.debug("REST request to partial update Encuesta partially : {}, {}", id, encuesta);
        if (encuesta.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, encuesta.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return encuestaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (encuesta.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(encuesta.getUser());
                }

                Mono<Encuesta> result = encuestaRepository
                    .findById(encuesta.getId())
                    .map(existingEncuesta -> {
                        if (encuesta.getFecha() != null) {
                            existingEncuesta.setFecha(encuesta.getFecha());
                        }
                        if (encuesta.getDebilidad() != null) {
                            existingEncuesta.setDebilidad(encuesta.getDebilidad());
                        }
                        if (encuesta.getCefalea() != null) {
                            existingEncuesta.setCefalea(encuesta.getCefalea());
                        }
                        if (encuesta.getCalambres() != null) {
                            existingEncuesta.setCalambres(encuesta.getCalambres());
                        }
                        if (encuesta.getNauseas() != null) {
                            existingEncuesta.setNauseas(encuesta.getNauseas());
                        }
                        if (encuesta.getVomito() != null) {
                            existingEncuesta.setVomito(encuesta.getVomito());
                        }
                        if (encuesta.getMareo() != null) {
                            existingEncuesta.setMareo(encuesta.getMareo());
                        }
                        if (encuesta.getNinguna() != null) {
                            existingEncuesta.setNinguna(encuesta.getNinguna());
                        }

                        return existingEncuesta;
                    })
                    .flatMap(encuestaRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /encuestas} : get all the encuestas.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of encuestas in body.
     */
    @GetMapping("/encuestas")
    public Mono<ResponseEntity<List<Encuesta>>> getAllEncuestas(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Encuestas");
        return encuestaRepository
            .count()
            .zipWith(encuestaRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /encuestas/:id} : get the "id" encuesta.
     *
     * @param id the id of the encuesta to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the encuesta, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/encuestas/{id}")
    public Mono<ResponseEntity<Encuesta>> getEncuesta(@PathVariable Long id) {
        log.debug("REST request to get Encuesta : {}", id);
        Mono<Encuesta> encuesta = encuestaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(encuesta);
    }

    /**
     * {@code DELETE  /encuestas/:id} : delete the "id" encuesta.
     *
     * @param id the id of the encuesta to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/encuestas/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteEncuesta(@PathVariable Long id) {
        log.debug("REST request to delete Encuesta : {}", id);
        return encuestaRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
