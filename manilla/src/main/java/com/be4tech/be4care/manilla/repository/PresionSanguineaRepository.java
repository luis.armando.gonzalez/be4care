package com.be4tech.be4care.manilla.repository;

import com.be4tech.be4care.manilla.domain.PresionSanguinea;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the PresionSanguinea entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PresionSanguineaRepository extends R2dbcRepository<PresionSanguinea, Long>, PresionSanguineaRepositoryInternal {
    Flux<PresionSanguinea> findAllBy(Pageable pageable);

    @Query("SELECT * FROM presion_sanguinea entity WHERE entity.user_id = :id")
    Flux<PresionSanguinea> findByUser(Long id);

    @Query("SELECT * FROM presion_sanguinea entity WHERE entity.user_id IS NULL")
    Flux<PresionSanguinea> findAllWhereUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<PresionSanguinea> findAll();

    @Override
    Mono<PresionSanguinea> findById(Long id);

    @Override
    <S extends PresionSanguinea> Mono<S> save(S entity);
}

interface PresionSanguineaRepositoryInternal {
    <S extends PresionSanguinea> Mono<S> insert(S entity);
    <S extends PresionSanguinea> Mono<S> save(S entity);
    Mono<Integer> update(PresionSanguinea entity);

    Flux<PresionSanguinea> findAll();
    Mono<PresionSanguinea> findById(Long id);
    Flux<PresionSanguinea> findAllBy(Pageable pageable);
    Flux<PresionSanguinea> findAllBy(Pageable pageable, Criteria criteria);
}
