package com.be4tech.be4care.manilla.domain;

import java.io.Serializable;
import java.time.Instant;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A PresionSanguinea.
 */
@Table("presion_sanguinea")
public class PresionSanguinea implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("presion_sanguinea_sistolica")
    private Integer presionSanguineaSistolica;

    @Column("presion_sanguinea_diastolica")
    private Integer presionSanguineaDiastolica;

    @Column("fecha_registro")
    private Instant fechaRegistro;

    @Transient
    private User user;

    @Column("user_id")
    private String userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public PresionSanguinea id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPresionSanguineaSistolica() {
        return this.presionSanguineaSistolica;
    }

    public PresionSanguinea presionSanguineaSistolica(Integer presionSanguineaSistolica) {
        this.setPresionSanguineaSistolica(presionSanguineaSistolica);
        return this;
    }

    public void setPresionSanguineaSistolica(Integer presionSanguineaSistolica) {
        this.presionSanguineaSistolica = presionSanguineaSistolica;
    }

    public Integer getPresionSanguineaDiastolica() {
        return this.presionSanguineaDiastolica;
    }

    public PresionSanguinea presionSanguineaDiastolica(Integer presionSanguineaDiastolica) {
        this.setPresionSanguineaDiastolica(presionSanguineaDiastolica);
        return this;
    }

    public void setPresionSanguineaDiastolica(Integer presionSanguineaDiastolica) {
        this.presionSanguineaDiastolica = presionSanguineaDiastolica;
    }

    public Instant getFechaRegistro() {
        return this.fechaRegistro;
    }

    public PresionSanguinea fechaRegistro(Instant fechaRegistro) {
        this.setFechaRegistro(fechaRegistro);
        return this;
    }

    public void setFechaRegistro(Instant fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user != null ? user.getId() : null;
    }

    public PresionSanguinea user(User user) {
        this.setUser(user);
        return this;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String user) {
        this.userId = user;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PresionSanguinea)) {
            return false;
        }
        return id != null && id.equals(((PresionSanguinea) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PresionSanguinea{" +
            "id=" + getId() +
            ", presionSanguineaSistolica=" + getPresionSanguineaSistolica() +
            ", presionSanguineaDiastolica=" + getPresionSanguineaDiastolica() +
            ", fechaRegistro='" + getFechaRegistro() + "'" +
            "}";
    }
}
