package com.be4tech.be4care.manilla.repository;

import com.be4tech.be4care.manilla.domain.Caloria;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Caloria entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CaloriaRepository extends R2dbcRepository<Caloria, Long>, CaloriaRepositoryInternal {
    Flux<Caloria> findAllBy(Pageable pageable);

    @Query("SELECT * FROM caloria entity WHERE entity.user_id = :id")
    Flux<Caloria> findByUser(Long id);

    @Query("SELECT * FROM caloria entity WHERE entity.user_id IS NULL")
    Flux<Caloria> findAllWhereUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Caloria> findAll();

    @Override
    Mono<Caloria> findById(Long id);

    @Override
    <S extends Caloria> Mono<S> save(S entity);
}

interface CaloriaRepositoryInternal {
    <S extends Caloria> Mono<S> insert(S entity);
    <S extends Caloria> Mono<S> save(S entity);
    Mono<Integer> update(Caloria entity);

    Flux<Caloria> findAll();
    Mono<Caloria> findById(Long id);
    Flux<Caloria> findAllBy(Pageable pageable);
    Flux<Caloria> findAllBy(Pageable pageable, Criteria criteria);
}
