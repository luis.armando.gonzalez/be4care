package com.be4tech.be4care.manilla.web.rest;

import com.be4tech.be4care.manilla.domain.Pasos;
import com.be4tech.be4care.manilla.repository.PasosRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.manilla.domain.Pasos}.
 */
@RestController
@RequestMapping("/api")
public class PasosResource {

    private final Logger log = LoggerFactory.getLogger(PasosResource.class);

    private static final String ENTITY_NAME = "manillaPasos";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PasosRepository pasosRepository;

    private final UserRepository userRepository;

    public PasosResource(PasosRepository pasosRepository, UserRepository userRepository) {
        this.pasosRepository = pasosRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /pasos} : Create a new pasos.
     *
     * @param pasos the pasos to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new pasos, or with status {@code 400 (Bad Request)} if the pasos has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/pasos")
    public Mono<ResponseEntity<Pasos>> createPasos(@RequestBody Pasos pasos) throws URISyntaxException {
        log.debug("REST request to save Pasos : {}", pasos);
        if (pasos.getId() != null) {
            throw new BadRequestAlertException("A new pasos cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (pasos.getUser() != null) {
            // Save user in case it's new and only exists in gateway
            userRepository.save(pasos.getUser());
        }
        return pasosRepository
            .save(pasos)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/pasos/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /pasos/:id} : Updates an existing pasos.
     *
     * @param id the id of the pasos to save.
     * @param pasos the pasos to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pasos,
     * or with status {@code 400 (Bad Request)} if the pasos is not valid,
     * or with status {@code 500 (Internal Server Error)} if the pasos couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/pasos/{id}")
    public Mono<ResponseEntity<Pasos>> updatePasos(@PathVariable(value = "id", required = false) final Long id, @RequestBody Pasos pasos)
        throws URISyntaxException {
        log.debug("REST request to update Pasos : {}, {}", id, pasos);
        if (pasos.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, pasos.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return pasosRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (pasos.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(pasos.getUser());
                }
                return pasosRepository
                    .save(pasos)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /pasos/:id} : Partial updates given fields of an existing pasos, field will ignore if it is null
     *
     * @param id the id of the pasos to save.
     * @param pasos the pasos to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pasos,
     * or with status {@code 400 (Bad Request)} if the pasos is not valid,
     * or with status {@code 404 (Not Found)} if the pasos is not found,
     * or with status {@code 500 (Internal Server Error)} if the pasos couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/pasos/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Pasos>> partialUpdatePasos(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Pasos pasos
    ) throws URISyntaxException {
        log.debug("REST request to partial update Pasos partially : {}, {}", id, pasos);
        if (pasos.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, pasos.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return pasosRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (pasos.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(pasos.getUser());
                }

                Mono<Pasos> result = pasosRepository
                    .findById(pasos.getId())
                    .map(existingPasos -> {
                        if (pasos.getNroPasos() != null) {
                            existingPasos.setNroPasos(pasos.getNroPasos());
                        }
                        if (pasos.getTimeInstant() != null) {
                            existingPasos.setTimeInstant(pasos.getTimeInstant());
                        }

                        return existingPasos;
                    })
                    .flatMap(pasosRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /pasos} : get all the pasos.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pasos in body.
     */
    @GetMapping("/pasos")
    public Mono<ResponseEntity<List<Pasos>>> getAllPasos(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Pasos");
        return pasosRepository
            .count()
            .zipWith(pasosRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /pasos/:id} : get the "id" pasos.
     *
     * @param id the id of the pasos to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the pasos, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/pasos/{id}")
    public Mono<ResponseEntity<Pasos>> getPasos(@PathVariable Long id) {
        log.debug("REST request to get Pasos : {}", id);
        Mono<Pasos> pasos = pasosRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(pasos);
    }

    /**
     * {@code DELETE  /pasos/:id} : delete the "id" pasos.
     *
     * @param id the id of the pasos to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/pasos/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deletePasos(@PathVariable Long id) {
        log.debug("REST request to delete Pasos : {}", id);
        return pasosRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
