package com.be4tech.be4care.manilla.repository.rowmapper;

import com.be4tech.be4care.manilla.domain.Pasos;
import com.be4tech.be4care.manilla.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Pasos}, with proper type conversions.
 */
@Service
public class PasosRowMapper implements BiFunction<Row, String, Pasos> {

    private final ColumnConverter converter;

    public PasosRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Pasos} stored in the database.
     */
    @Override
    public Pasos apply(Row row, String prefix) {
        Pasos entity = new Pasos();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setNroPasos(converter.fromRow(row, prefix + "_nro_pasos", Integer.class));
        entity.setTimeInstant(converter.fromRow(row, prefix + "_time_instant", Instant.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        return entity;
    }
}
