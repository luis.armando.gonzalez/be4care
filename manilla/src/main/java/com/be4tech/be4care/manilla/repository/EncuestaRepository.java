package com.be4tech.be4care.manilla.repository;

import com.be4tech.be4care.manilla.domain.Encuesta;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Encuesta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EncuestaRepository extends R2dbcRepository<Encuesta, Long>, EncuestaRepositoryInternal {
    Flux<Encuesta> findAllBy(Pageable pageable);

    @Query("SELECT * FROM encuesta entity WHERE entity.user_id = :id")
    Flux<Encuesta> findByUser(Long id);

    @Query("SELECT * FROM encuesta entity WHERE entity.user_id IS NULL")
    Flux<Encuesta> findAllWhereUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Encuesta> findAll();

    @Override
    Mono<Encuesta> findById(Long id);

    @Override
    <S extends Encuesta> Mono<S> save(S entity);
}

interface EncuestaRepositoryInternal {
    <S extends Encuesta> Mono<S> insert(S entity);
    <S extends Encuesta> Mono<S> save(S entity);
    Mono<Integer> update(Encuesta entity);

    Flux<Encuesta> findAll();
    Mono<Encuesta> findById(Long id);
    Flux<Encuesta> findAllBy(Pageable pageable);
    Flux<Encuesta> findAllBy(Pageable pageable, Criteria criteria);
}
