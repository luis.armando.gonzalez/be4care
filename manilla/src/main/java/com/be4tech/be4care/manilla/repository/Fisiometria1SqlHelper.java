package com.be4tech.be4care.manilla.repository;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Table;

public class Fisiometria1SqlHelper {

    public static List<Expression> getColumns(Table table, String columnPrefix) {
        List<Expression> columns = new ArrayList<>();
        columns.add(Column.aliased("id", table, columnPrefix + "_id"));
        columns.add(Column.aliased("ritmo_cardiaco", table, columnPrefix + "_ritmo_cardiaco"));
        columns.add(Column.aliased("ritmo_respiratorio", table, columnPrefix + "_ritmo_respiratorio"));
        columns.add(Column.aliased("oximetria", table, columnPrefix + "_oximetria"));
        columns.add(Column.aliased("presion_arterial_sistolica", table, columnPrefix + "_presion_arterial_sistolica"));
        columns.add(Column.aliased("presion_arterial_diastolica", table, columnPrefix + "_presion_arterial_diastolica"));
        columns.add(Column.aliased("temperatura", table, columnPrefix + "_temperatura"));
        columns.add(Column.aliased("fecha_registro", table, columnPrefix + "_fecha_registro"));
        columns.add(Column.aliased("fecha_toma", table, columnPrefix + "_fecha_toma"));

        columns.add(Column.aliased("user_id", table, columnPrefix + "_user_id"));
        return columns;
    }
}
