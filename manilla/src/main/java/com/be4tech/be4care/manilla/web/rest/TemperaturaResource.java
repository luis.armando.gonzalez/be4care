package com.be4tech.be4care.manilla.web.rest;

import com.be4tech.be4care.manilla.domain.Temperatura;
import com.be4tech.be4care.manilla.repository.TemperaturaRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.manilla.domain.Temperatura}.
 */
@RestController
@RequestMapping("/api")
public class TemperaturaResource {

    private final Logger log = LoggerFactory.getLogger(TemperaturaResource.class);

    private static final String ENTITY_NAME = "manillaTemperatura";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TemperaturaRepository temperaturaRepository;

    private final UserRepository userRepository;

    public TemperaturaResource(TemperaturaRepository temperaturaRepository, UserRepository userRepository) {
        this.temperaturaRepository = temperaturaRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /temperaturas} : Create a new temperatura.
     *
     * @param temperatura the temperatura to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new temperatura, or with status {@code 400 (Bad Request)} if the temperatura has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/temperaturas")
    public Mono<ResponseEntity<Temperatura>> createTemperatura(@RequestBody Temperatura temperatura) throws URISyntaxException {
        log.debug("REST request to save Temperatura : {}", temperatura);
        if (temperatura.getId() != null) {
            throw new BadRequestAlertException("A new temperatura cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (temperatura.getUser() != null) {
            // Save user in case it's new and only exists in gateway
            userRepository.save(temperatura.getUser());
        }
        return temperaturaRepository
            .save(temperatura)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/temperaturas/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /temperaturas/:id} : Updates an existing temperatura.
     *
     * @param id the id of the temperatura to save.
     * @param temperatura the temperatura to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated temperatura,
     * or with status {@code 400 (Bad Request)} if the temperatura is not valid,
     * or with status {@code 500 (Internal Server Error)} if the temperatura couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/temperaturas/{id}")
    public Mono<ResponseEntity<Temperatura>> updateTemperatura(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Temperatura temperatura
    ) throws URISyntaxException {
        log.debug("REST request to update Temperatura : {}, {}", id, temperatura);
        if (temperatura.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, temperatura.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return temperaturaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (temperatura.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(temperatura.getUser());
                }
                return temperaturaRepository
                    .save(temperatura)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /temperaturas/:id} : Partial updates given fields of an existing temperatura, field will ignore if it is null
     *
     * @param id the id of the temperatura to save.
     * @param temperatura the temperatura to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated temperatura,
     * or with status {@code 400 (Bad Request)} if the temperatura is not valid,
     * or with status {@code 404 (Not Found)} if the temperatura is not found,
     * or with status {@code 500 (Internal Server Error)} if the temperatura couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/temperaturas/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Temperatura>> partialUpdateTemperatura(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Temperatura temperatura
    ) throws URISyntaxException {
        log.debug("REST request to partial update Temperatura partially : {}, {}", id, temperatura);
        if (temperatura.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, temperatura.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return temperaturaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (temperatura.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(temperatura.getUser());
                }

                Mono<Temperatura> result = temperaturaRepository
                    .findById(temperatura.getId())
                    .map(existingTemperatura -> {
                        if (temperatura.getTemperatura() != null) {
                            existingTemperatura.setTemperatura(temperatura.getTemperatura());
                        }
                        if (temperatura.getFechaRegistro() != null) {
                            existingTemperatura.setFechaRegistro(temperatura.getFechaRegistro());
                        }

                        return existingTemperatura;
                    })
                    .flatMap(temperaturaRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /temperaturas} : get all the temperaturas.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of temperaturas in body.
     */
    @GetMapping("/temperaturas")
    public Mono<ResponseEntity<List<Temperatura>>> getAllTemperaturas(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Temperaturas");
        return temperaturaRepository
            .count()
            .zipWith(temperaturaRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /temperaturas/:id} : get the "id" temperatura.
     *
     * @param id the id of the temperatura to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the temperatura, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/temperaturas/{id}")
    public Mono<ResponseEntity<Temperatura>> getTemperatura(@PathVariable Long id) {
        log.debug("REST request to get Temperatura : {}", id);
        Mono<Temperatura> temperatura = temperaturaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(temperatura);
    }

    /**
     * {@code DELETE  /temperaturas/:id} : delete the "id" temperatura.
     *
     * @param id the id of the temperatura to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/temperaturas/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteTemperatura(@PathVariable Long id) {
        log.debug("REST request to delete Temperatura : {}", id);
        return temperaturaRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
