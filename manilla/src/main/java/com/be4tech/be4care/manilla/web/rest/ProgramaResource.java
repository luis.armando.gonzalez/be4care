package com.be4tech.be4care.manilla.web.rest;

import com.be4tech.be4care.manilla.domain.Programa;
import com.be4tech.be4care.manilla.repository.ProgramaRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.manilla.domain.Programa}.
 */
@RestController
@RequestMapping("/api")
public class ProgramaResource {

    private final Logger log = LoggerFactory.getLogger(ProgramaResource.class);

    private static final String ENTITY_NAME = "manillaPrograma";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramaRepository programaRepository;

    private final UserRepository userRepository;

    public ProgramaResource(ProgramaRepository programaRepository, UserRepository userRepository) {
        this.programaRepository = programaRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /programas} : Create a new programa.
     *
     * @param programa the programa to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programa, or with status {@code 400 (Bad Request)} if the programa has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/programas")
    public Mono<ResponseEntity<Programa>> createPrograma(@RequestBody Programa programa) throws URISyntaxException {
        log.debug("REST request to save Programa : {}", programa);
        if (programa.getId() != null) {
            throw new BadRequestAlertException("A new programa cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (programa.getUser() != null) {
            // Save user in case it's new and only exists in gateway
            userRepository.save(programa.getUser());
        }
        return programaRepository
            .save(programa)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/programas/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /programas/:id} : Updates an existing programa.
     *
     * @param id the id of the programa to save.
     * @param programa the programa to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programa,
     * or with status {@code 400 (Bad Request)} if the programa is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programa couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/programas/{id}")
    public Mono<ResponseEntity<Programa>> updatePrograma(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Programa programa
    ) throws URISyntaxException {
        log.debug("REST request to update Programa : {}, {}", id, programa);
        if (programa.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, programa.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return programaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (programa.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(programa.getUser());
                }
                return programaRepository
                    .save(programa)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /programas/:id} : Partial updates given fields of an existing programa, field will ignore if it is null
     *
     * @param id the id of the programa to save.
     * @param programa the programa to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programa,
     * or with status {@code 400 (Bad Request)} if the programa is not valid,
     * or with status {@code 404 (Not Found)} if the programa is not found,
     * or with status {@code 500 (Internal Server Error)} if the programa couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/programas/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Programa>> partialUpdatePrograma(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Programa programa
    ) throws URISyntaxException {
        log.debug("REST request to partial update Programa partially : {}, {}", id, programa);
        if (programa.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, programa.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return programaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (programa.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(programa.getUser());
                }

                Mono<Programa> result = programaRepository
                    .findById(programa.getId())
                    .map(existingPrograma -> {
                        if (programa.getCaloriasActividad() != null) {
                            existingPrograma.setCaloriasActividad(programa.getCaloriasActividad());
                        }
                        if (programa.getPasosActividad() != null) {
                            existingPrograma.setPasosActividad(programa.getPasosActividad());
                        }
                        if (programa.getFechaRegistro() != null) {
                            existingPrograma.setFechaRegistro(programa.getFechaRegistro());
                        }

                        return existingPrograma;
                    })
                    .flatMap(programaRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /programas} : get all the programas.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programas in body.
     */
    @GetMapping("/programas")
    public Mono<ResponseEntity<List<Programa>>> getAllProgramas(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Programas");
        return programaRepository
            .count()
            .zipWith(programaRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /programas/:id} : get the "id" programa.
     *
     * @param id the id of the programa to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programa, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/programas/{id}")
    public Mono<ResponseEntity<Programa>> getPrograma(@PathVariable Long id) {
        log.debug("REST request to get Programa : {}", id);
        Mono<Programa> programa = programaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(programa);
    }

    /**
     * {@code DELETE  /programas/:id} : delete the "id" programa.
     *
     * @param id the id of the programa to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/programas/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deletePrograma(@PathVariable Long id) {
        log.debug("REST request to delete Programa : {}", id);
        return programaRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
