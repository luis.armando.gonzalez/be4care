package com.be4tech.be4care.manilla.web.rest;

import com.be4tech.be4care.manilla.domain.Dispositivo;
import com.be4tech.be4care.manilla.repository.DispositivoRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.manilla.domain.Dispositivo}.
 */
@RestController
@RequestMapping("/api")
public class DispositivoResource {

    private final Logger log = LoggerFactory.getLogger(DispositivoResource.class);

    private static final String ENTITY_NAME = "manillaDispositivo";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DispositivoRepository dispositivoRepository;

    private final UserRepository userRepository;

    public DispositivoResource(DispositivoRepository dispositivoRepository, UserRepository userRepository) {
        this.dispositivoRepository = dispositivoRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /dispositivos} : Create a new dispositivo.
     *
     * @param dispositivo the dispositivo to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dispositivo, or with status {@code 400 (Bad Request)} if the dispositivo has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/dispositivos")
    public Mono<ResponseEntity<Dispositivo>> createDispositivo(@RequestBody Dispositivo dispositivo) throws URISyntaxException {
        log.debug("REST request to save Dispositivo : {}", dispositivo);
        if (dispositivo.getId() != null) {
            throw new BadRequestAlertException("A new dispositivo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (dispositivo.getUser() != null) {
            // Save user in case it's new and only exists in gateway
            userRepository.save(dispositivo.getUser());
        }
        return dispositivoRepository
            .save(dispositivo)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/dispositivos/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /dispositivos/:id} : Updates an existing dispositivo.
     *
     * @param id the id of the dispositivo to save.
     * @param dispositivo the dispositivo to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dispositivo,
     * or with status {@code 400 (Bad Request)} if the dispositivo is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dispositivo couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/dispositivos/{id}")
    public Mono<ResponseEntity<Dispositivo>> updateDispositivo(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Dispositivo dispositivo
    ) throws URISyntaxException {
        log.debug("REST request to update Dispositivo : {}, {}", id, dispositivo);
        if (dispositivo.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dispositivo.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return dispositivoRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (dispositivo.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(dispositivo.getUser());
                }
                return dispositivoRepository
                    .save(dispositivo)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /dispositivos/:id} : Partial updates given fields of an existing dispositivo, field will ignore if it is null
     *
     * @param id the id of the dispositivo to save.
     * @param dispositivo the dispositivo to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dispositivo,
     * or with status {@code 400 (Bad Request)} if the dispositivo is not valid,
     * or with status {@code 404 (Not Found)} if the dispositivo is not found,
     * or with status {@code 500 (Internal Server Error)} if the dispositivo couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/dispositivos/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Dispositivo>> partialUpdateDispositivo(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Dispositivo dispositivo
    ) throws URISyntaxException {
        log.debug("REST request to partial update Dispositivo partially : {}, {}", id, dispositivo);
        if (dispositivo.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dispositivo.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return dispositivoRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (dispositivo.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(dispositivo.getUser());
                }

                Mono<Dispositivo> result = dispositivoRepository
                    .findById(dispositivo.getId())
                    .map(existingDispositivo -> {
                        if (dispositivo.getDispositivo() != null) {
                            existingDispositivo.setDispositivo(dispositivo.getDispositivo());
                        }
                        if (dispositivo.getMac() != null) {
                            existingDispositivo.setMac(dispositivo.getMac());
                        }
                        if (dispositivo.getConectado() != null) {
                            existingDispositivo.setConectado(dispositivo.getConectado());
                        }

                        return existingDispositivo;
                    })
                    .flatMap(dispositivoRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /dispositivos} : get all the dispositivos.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dispositivos in body.
     */
    @GetMapping("/dispositivos")
    public Mono<ResponseEntity<List<Dispositivo>>> getAllDispositivos(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Dispositivos");
        return dispositivoRepository
            .count()
            .zipWith(dispositivoRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /dispositivos/:id} : get the "id" dispositivo.
     *
     * @param id the id of the dispositivo to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dispositivo, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/dispositivos/{id}")
    public Mono<ResponseEntity<Dispositivo>> getDispositivo(@PathVariable Long id) {
        log.debug("REST request to get Dispositivo : {}", id);
        Mono<Dispositivo> dispositivo = dispositivoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(dispositivo);
    }

    /**
     * {@code DELETE  /dispositivos/:id} : delete the "id" dispositivo.
     *
     * @param id the id of the dispositivo to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/dispositivos/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteDispositivo(@PathVariable Long id) {
        log.debug("REST request to delete Dispositivo : {}", id);
        return dispositivoRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
