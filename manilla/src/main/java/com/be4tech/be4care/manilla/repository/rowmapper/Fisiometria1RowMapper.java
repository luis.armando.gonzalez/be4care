package com.be4tech.be4care.manilla.repository.rowmapper;

import com.be4tech.be4care.manilla.domain.Fisiometria1;
import com.be4tech.be4care.manilla.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Fisiometria1}, with proper type conversions.
 */
@Service
public class Fisiometria1RowMapper implements BiFunction<Row, String, Fisiometria1> {

    private final ColumnConverter converter;

    public Fisiometria1RowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Fisiometria1} stored in the database.
     */
    @Override
    public Fisiometria1 apply(Row row, String prefix) {
        Fisiometria1 entity = new Fisiometria1();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setRitmoCardiaco(converter.fromRow(row, prefix + "_ritmo_cardiaco", Integer.class));
        entity.setRitmoRespiratorio(converter.fromRow(row, prefix + "_ritmo_respiratorio", Integer.class));
        entity.setOximetria(converter.fromRow(row, prefix + "_oximetria", Integer.class));
        entity.setPresionArterialSistolica(converter.fromRow(row, prefix + "_presion_arterial_sistolica", Integer.class));
        entity.setPresionArterialDiastolica(converter.fromRow(row, prefix + "_presion_arterial_diastolica", Integer.class));
        entity.setTemperatura(converter.fromRow(row, prefix + "_temperatura", Float.class));
        entity.setFechaRegistro(converter.fromRow(row, prefix + "_fecha_registro", Instant.class));
        entity.setFechaToma(converter.fromRow(row, prefix + "_fecha_toma", Instant.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        return entity;
    }
}
