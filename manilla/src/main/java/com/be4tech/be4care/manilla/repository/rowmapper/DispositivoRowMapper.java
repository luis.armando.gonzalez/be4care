package com.be4tech.be4care.manilla.repository.rowmapper;

import com.be4tech.be4care.manilla.domain.Dispositivo;
import com.be4tech.be4care.manilla.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Dispositivo}, with proper type conversions.
 */
@Service
public class DispositivoRowMapper implements BiFunction<Row, String, Dispositivo> {

    private final ColumnConverter converter;

    public DispositivoRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Dispositivo} stored in the database.
     */
    @Override
    public Dispositivo apply(Row row, String prefix) {
        Dispositivo entity = new Dispositivo();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setDispositivo(converter.fromRow(row, prefix + "_dispositivo", String.class));
        entity.setMac(converter.fromRow(row, prefix + "_mac", String.class));
        entity.setConectado(converter.fromRow(row, prefix + "_conectado", Boolean.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        return entity;
    }
}
