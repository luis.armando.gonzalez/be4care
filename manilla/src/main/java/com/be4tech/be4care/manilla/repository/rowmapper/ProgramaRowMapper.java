package com.be4tech.be4care.manilla.repository.rowmapper;

import com.be4tech.be4care.manilla.domain.Programa;
import com.be4tech.be4care.manilla.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Programa}, with proper type conversions.
 */
@Service
public class ProgramaRowMapper implements BiFunction<Row, String, Programa> {

    private final ColumnConverter converter;

    public ProgramaRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Programa} stored in the database.
     */
    @Override
    public Programa apply(Row row, String prefix) {
        Programa entity = new Programa();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setCaloriasActividad(converter.fromRow(row, prefix + "_calorias_actividad", Integer.class));
        entity.setPasosActividad(converter.fromRow(row, prefix + "_pasos_actividad", Integer.class));
        entity.setFechaRegistro(converter.fromRow(row, prefix + "_fecha_registro", Instant.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        return entity;
    }
}
