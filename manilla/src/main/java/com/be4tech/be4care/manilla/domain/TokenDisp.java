package com.be4tech.be4care.manilla.domain;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZonedDateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A TokenDisp.
 */
@Table("token_disp")
public class TokenDisp implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("token_conexion")
    private String tokenConexion;

    @Column("activo")
    private Boolean activo;

    @Column("fecha_inicio")
    private Instant fechaInicio;

    @Column("fecha_fin")
    private ZonedDateTime fechaFin;

    @Transient
    private User user;

    @Column("user_id")
    private String userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public TokenDisp id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTokenConexion() {
        return this.tokenConexion;
    }

    public TokenDisp tokenConexion(String tokenConexion) {
        this.setTokenConexion(tokenConexion);
        return this;
    }

    public void setTokenConexion(String tokenConexion) {
        this.tokenConexion = tokenConexion;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public TokenDisp activo(Boolean activo) {
        this.setActivo(activo);
        return this;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Instant getFechaInicio() {
        return this.fechaInicio;
    }

    public TokenDisp fechaInicio(Instant fechaInicio) {
        this.setFechaInicio(fechaInicio);
        return this;
    }

    public void setFechaInicio(Instant fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public ZonedDateTime getFechaFin() {
        return this.fechaFin;
    }

    public TokenDisp fechaFin(ZonedDateTime fechaFin) {
        this.setFechaFin(fechaFin);
        return this;
    }

    public void setFechaFin(ZonedDateTime fechaFin) {
        this.fechaFin = fechaFin;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user != null ? user.getId() : null;
    }

    public TokenDisp user(User user) {
        this.setUser(user);
        return this;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String user) {
        this.userId = user;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TokenDisp)) {
            return false;
        }
        return id != null && id.equals(((TokenDisp) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TokenDisp{" +
            "id=" + getId() +
            ", tokenConexion='" + getTokenConexion() + "'" +
            ", activo='" + getActivo() + "'" +
            ", fechaInicio='" + getFechaInicio() + "'" +
            ", fechaFin='" + getFechaFin() + "'" +
            "}";
    }
}
