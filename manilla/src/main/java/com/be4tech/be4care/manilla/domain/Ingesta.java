package com.be4tech.be4care.manilla.domain;

import java.io.Serializable;
import java.time.Instant;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Ingesta.
 */
@Table("ingesta")
public class Ingesta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("tipo")
    private String tipo;

    @Column("consumo_calorias")
    private Integer consumoCalorias;

    @Column("fecha_registro")
    private Instant fechaRegistro;

    @Transient
    private User user;

    @Column("user_id")
    private String userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Ingesta id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipo() {
        return this.tipo;
    }

    public Ingesta tipo(String tipo) {
        this.setTipo(tipo);
        return this;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getConsumoCalorias() {
        return this.consumoCalorias;
    }

    public Ingesta consumoCalorias(Integer consumoCalorias) {
        this.setConsumoCalorias(consumoCalorias);
        return this;
    }

    public void setConsumoCalorias(Integer consumoCalorias) {
        this.consumoCalorias = consumoCalorias;
    }

    public Instant getFechaRegistro() {
        return this.fechaRegistro;
    }

    public Ingesta fechaRegistro(Instant fechaRegistro) {
        this.setFechaRegistro(fechaRegistro);
        return this;
    }

    public void setFechaRegistro(Instant fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user != null ? user.getId() : null;
    }

    public Ingesta user(User user) {
        this.setUser(user);
        return this;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String user) {
        this.userId = user;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Ingesta)) {
            return false;
        }
        return id != null && id.equals(((Ingesta) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Ingesta{" +
            "id=" + getId() +
            ", tipo='" + getTipo() + "'" +
            ", consumoCalorias=" + getConsumoCalorias() +
            ", fechaRegistro='" + getFechaRegistro() + "'" +
            "}";
    }
}
