package com.be4tech.be4care.manilla.repository;

import com.be4tech.be4care.manilla.domain.Notificacion;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Notificacion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NotificacionRepository extends R2dbcRepository<Notificacion, Long>, NotificacionRepositoryInternal {
    Flux<Notificacion> findAllBy(Pageable pageable);

    @Query("SELECT * FROM notificacion entity WHERE entity.token_id = :id")
    Flux<Notificacion> findByToken(Long id);

    @Query("SELECT * FROM notificacion entity WHERE entity.token_id IS NULL")
    Flux<Notificacion> findAllWhereTokenIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Notificacion> findAll();

    @Override
    Mono<Notificacion> findById(Long id);

    @Override
    <S extends Notificacion> Mono<S> save(S entity);
}

interface NotificacionRepositoryInternal {
    <S extends Notificacion> Mono<S> insert(S entity);
    <S extends Notificacion> Mono<S> save(S entity);
    Mono<Integer> update(Notificacion entity);

    Flux<Notificacion> findAll();
    Mono<Notificacion> findById(Long id);
    Flux<Notificacion> findAllBy(Pageable pageable);
    Flux<Notificacion> findAllBy(Pageable pageable, Criteria criteria);
}
