package com.be4tech.be4care.manilla.repository;

import com.be4tech.be4care.manilla.domain.Temperatura;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Temperatura entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TemperaturaRepository extends R2dbcRepository<Temperatura, Long>, TemperaturaRepositoryInternal {
    Flux<Temperatura> findAllBy(Pageable pageable);

    @Query("SELECT * FROM temperatura entity WHERE entity.user_id = :id")
    Flux<Temperatura> findByUser(Long id);

    @Query("SELECT * FROM temperatura entity WHERE entity.user_id IS NULL")
    Flux<Temperatura> findAllWhereUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Temperatura> findAll();

    @Override
    Mono<Temperatura> findById(Long id);

    @Override
    <S extends Temperatura> Mono<S> save(S entity);
}

interface TemperaturaRepositoryInternal {
    <S extends Temperatura> Mono<S> insert(S entity);
    <S extends Temperatura> Mono<S> save(S entity);
    Mono<Integer> update(Temperatura entity);

    Flux<Temperatura> findAll();
    Mono<Temperatura> findById(Long id);
    Flux<Temperatura> findAllBy(Pageable pageable);
    Flux<Temperatura> findAllBy(Pageable pageable, Criteria criteria);
}
