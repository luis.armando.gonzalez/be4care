package com.be4tech.be4care.manilla.repository;

import com.be4tech.be4care.manilla.domain.Pasos;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Pasos entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PasosRepository extends R2dbcRepository<Pasos, Long>, PasosRepositoryInternal {
    Flux<Pasos> findAllBy(Pageable pageable);

    @Query("SELECT * FROM pasos entity WHERE entity.user_id = :id")
    Flux<Pasos> findByUser(Long id);

    @Query("SELECT * FROM pasos entity WHERE entity.user_id IS NULL")
    Flux<Pasos> findAllWhereUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Pasos> findAll();

    @Override
    Mono<Pasos> findById(Long id);

    @Override
    <S extends Pasos> Mono<S> save(S entity);
}

interface PasosRepositoryInternal {
    <S extends Pasos> Mono<S> insert(S entity);
    <S extends Pasos> Mono<S> save(S entity);
    Mono<Integer> update(Pasos entity);

    Flux<Pasos> findAll();
    Mono<Pasos> findById(Long id);
    Flux<Pasos> findAllBy(Pageable pageable);
    Flux<Pasos> findAllBy(Pageable pageable, Criteria criteria);
}
