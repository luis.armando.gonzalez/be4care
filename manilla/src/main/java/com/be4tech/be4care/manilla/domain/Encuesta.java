package com.be4tech.be4care.manilla.domain;

import java.io.Serializable;
import java.time.Instant;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Encuesta.
 */
@Table("encuesta")
public class Encuesta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("fecha")
    private Instant fecha;

    @Column("debilidad")
    private Boolean debilidad;

    @Column("cefalea")
    private Boolean cefalea;

    @Column("calambres")
    private Boolean calambres;

    @Column("nauseas")
    private Boolean nauseas;

    @Column("vomito")
    private Boolean vomito;

    @Column("mareo")
    private Boolean mareo;

    @Column("ninguna")
    private Boolean ninguna;

    @Transient
    private User user;

    @Column("user_id")
    private String userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Encuesta id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getFecha() {
        return this.fecha;
    }

    public Encuesta fecha(Instant fecha) {
        this.setFecha(fecha);
        return this;
    }

    public void setFecha(Instant fecha) {
        this.fecha = fecha;
    }

    public Boolean getDebilidad() {
        return this.debilidad;
    }

    public Encuesta debilidad(Boolean debilidad) {
        this.setDebilidad(debilidad);
        return this;
    }

    public void setDebilidad(Boolean debilidad) {
        this.debilidad = debilidad;
    }

    public Boolean getCefalea() {
        return this.cefalea;
    }

    public Encuesta cefalea(Boolean cefalea) {
        this.setCefalea(cefalea);
        return this;
    }

    public void setCefalea(Boolean cefalea) {
        this.cefalea = cefalea;
    }

    public Boolean getCalambres() {
        return this.calambres;
    }

    public Encuesta calambres(Boolean calambres) {
        this.setCalambres(calambres);
        return this;
    }

    public void setCalambres(Boolean calambres) {
        this.calambres = calambres;
    }

    public Boolean getNauseas() {
        return this.nauseas;
    }

    public Encuesta nauseas(Boolean nauseas) {
        this.setNauseas(nauseas);
        return this;
    }

    public void setNauseas(Boolean nauseas) {
        this.nauseas = nauseas;
    }

    public Boolean getVomito() {
        return this.vomito;
    }

    public Encuesta vomito(Boolean vomito) {
        this.setVomito(vomito);
        return this;
    }

    public void setVomito(Boolean vomito) {
        this.vomito = vomito;
    }

    public Boolean getMareo() {
        return this.mareo;
    }

    public Encuesta mareo(Boolean mareo) {
        this.setMareo(mareo);
        return this;
    }

    public void setMareo(Boolean mareo) {
        this.mareo = mareo;
    }

    public Boolean getNinguna() {
        return this.ninguna;
    }

    public Encuesta ninguna(Boolean ninguna) {
        this.setNinguna(ninguna);
        return this;
    }

    public void setNinguna(Boolean ninguna) {
        this.ninguna = ninguna;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user != null ? user.getId() : null;
    }

    public Encuesta user(User user) {
        this.setUser(user);
        return this;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String user) {
        this.userId = user;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Encuesta)) {
            return false;
        }
        return id != null && id.equals(((Encuesta) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Encuesta{" +
            "id=" + getId() +
            ", fecha='" + getFecha() + "'" +
            ", debilidad='" + getDebilidad() + "'" +
            ", cefalea='" + getCefalea() + "'" +
            ", calambres='" + getCalambres() + "'" +
            ", nauseas='" + getNauseas() + "'" +
            ", vomito='" + getVomito() + "'" +
            ", mareo='" + getMareo() + "'" +
            ", ninguna='" + getNinguna() + "'" +
            "}";
    }
}
