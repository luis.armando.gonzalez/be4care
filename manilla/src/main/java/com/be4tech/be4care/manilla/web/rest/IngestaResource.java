package com.be4tech.be4care.manilla.web.rest;

import com.be4tech.be4care.manilla.domain.Ingesta;
import com.be4tech.be4care.manilla.repository.IngestaRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.manilla.domain.Ingesta}.
 */
@RestController
@RequestMapping("/api")
public class IngestaResource {

    private final Logger log = LoggerFactory.getLogger(IngestaResource.class);

    private static final String ENTITY_NAME = "manillaIngesta";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IngestaRepository ingestaRepository;

    private final UserRepository userRepository;

    public IngestaResource(IngestaRepository ingestaRepository, UserRepository userRepository) {
        this.ingestaRepository = ingestaRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /ingestas} : Create a new ingesta.
     *
     * @param ingesta the ingesta to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new ingesta, or with status {@code 400 (Bad Request)} if the ingesta has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ingestas")
    public Mono<ResponseEntity<Ingesta>> createIngesta(@RequestBody Ingesta ingesta) throws URISyntaxException {
        log.debug("REST request to save Ingesta : {}", ingesta);
        if (ingesta.getId() != null) {
            throw new BadRequestAlertException("A new ingesta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (ingesta.getUser() != null) {
            // Save user in case it's new and only exists in gateway
            userRepository.save(ingesta.getUser());
        }
        return ingestaRepository
            .save(ingesta)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/ingestas/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /ingestas/:id} : Updates an existing ingesta.
     *
     * @param id the id of the ingesta to save.
     * @param ingesta the ingesta to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ingesta,
     * or with status {@code 400 (Bad Request)} if the ingesta is not valid,
     * or with status {@code 500 (Internal Server Error)} if the ingesta couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ingestas/{id}")
    public Mono<ResponseEntity<Ingesta>> updateIngesta(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Ingesta ingesta
    ) throws URISyntaxException {
        log.debug("REST request to update Ingesta : {}, {}", id, ingesta);
        if (ingesta.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, ingesta.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return ingestaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (ingesta.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(ingesta.getUser());
                }
                return ingestaRepository
                    .save(ingesta)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /ingestas/:id} : Partial updates given fields of an existing ingesta, field will ignore if it is null
     *
     * @param id the id of the ingesta to save.
     * @param ingesta the ingesta to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ingesta,
     * or with status {@code 400 (Bad Request)} if the ingesta is not valid,
     * or with status {@code 404 (Not Found)} if the ingesta is not found,
     * or with status {@code 500 (Internal Server Error)} if the ingesta couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/ingestas/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Ingesta>> partialUpdateIngesta(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Ingesta ingesta
    ) throws URISyntaxException {
        log.debug("REST request to partial update Ingesta partially : {}, {}", id, ingesta);
        if (ingesta.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, ingesta.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return ingestaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (ingesta.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(ingesta.getUser());
                }

                Mono<Ingesta> result = ingestaRepository
                    .findById(ingesta.getId())
                    .map(existingIngesta -> {
                        if (ingesta.getTipo() != null) {
                            existingIngesta.setTipo(ingesta.getTipo());
                        }
                        if (ingesta.getConsumoCalorias() != null) {
                            existingIngesta.setConsumoCalorias(ingesta.getConsumoCalorias());
                        }
                        if (ingesta.getFechaRegistro() != null) {
                            existingIngesta.setFechaRegistro(ingesta.getFechaRegistro());
                        }

                        return existingIngesta;
                    })
                    .flatMap(ingestaRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /ingestas} : get all the ingestas.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of ingestas in body.
     */
    @GetMapping("/ingestas")
    public Mono<ResponseEntity<List<Ingesta>>> getAllIngestas(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Ingestas");
        return ingestaRepository
            .count()
            .zipWith(ingestaRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /ingestas/:id} : get the "id" ingesta.
     *
     * @param id the id of the ingesta to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the ingesta, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ingestas/{id}")
    public Mono<ResponseEntity<Ingesta>> getIngesta(@PathVariable Long id) {
        log.debug("REST request to get Ingesta : {}", id);
        Mono<Ingesta> ingesta = ingestaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(ingesta);
    }

    /**
     * {@code DELETE  /ingestas/:id} : delete the "id" ingesta.
     *
     * @param id the id of the ingesta to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ingestas/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteIngesta(@PathVariable Long id) {
        log.debug("REST request to delete Ingesta : {}", id);
        return ingestaRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
