package com.be4tech.be4care.manilla.repository;

import com.be4tech.be4care.manilla.domain.Fisiometria1;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Fisiometria1 entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Fisiometria1Repository extends R2dbcRepository<Fisiometria1, Long>, Fisiometria1RepositoryInternal {
    Flux<Fisiometria1> findAllBy(Pageable pageable);

    @Query("SELECT * FROM fisiometria_1 entity WHERE entity.user_id = :id")
    Flux<Fisiometria1> findByUser(Long id);

    @Query("SELECT * FROM fisiometria_1 entity WHERE entity.user_id IS NULL")
    Flux<Fisiometria1> findAllWhereUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Fisiometria1> findAll();

    @Override
    Mono<Fisiometria1> findById(Long id);

    @Override
    <S extends Fisiometria1> Mono<S> save(S entity);
}

interface Fisiometria1RepositoryInternal {
    <S extends Fisiometria1> Mono<S> insert(S entity);
    <S extends Fisiometria1> Mono<S> save(S entity);
    Mono<Integer> update(Fisiometria1 entity);

    Flux<Fisiometria1> findAll();
    Mono<Fisiometria1> findById(Long id);
    Flux<Fisiometria1> findAllBy(Pageable pageable);
    Flux<Fisiometria1> findAllBy(Pageable pageable, Criteria criteria);
}
