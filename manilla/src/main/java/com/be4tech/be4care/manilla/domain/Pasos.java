package com.be4tech.be4care.manilla.domain;

import java.io.Serializable;
import java.time.Instant;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Pasos.
 */
@Table("pasos")
public class Pasos implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("nro_pasos")
    private Integer nroPasos;

    @Column("time_instant")
    private Instant timeInstant;

    @Transient
    private User user;

    @Column("user_id")
    private String userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Pasos id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNroPasos() {
        return this.nroPasos;
    }

    public Pasos nroPasos(Integer nroPasos) {
        this.setNroPasos(nroPasos);
        return this;
    }

    public void setNroPasos(Integer nroPasos) {
        this.nroPasos = nroPasos;
    }

    public Instant getTimeInstant() {
        return this.timeInstant;
    }

    public Pasos timeInstant(Instant timeInstant) {
        this.setTimeInstant(timeInstant);
        return this;
    }

    public void setTimeInstant(Instant timeInstant) {
        this.timeInstant = timeInstant;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user != null ? user.getId() : null;
    }

    public Pasos user(User user) {
        this.setUser(user);
        return this;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String user) {
        this.userId = user;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Pasos)) {
            return false;
        }
        return id != null && id.equals(((Pasos) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Pasos{" +
            "id=" + getId() +
            ", nroPasos=" + getNroPasos() +
            ", timeInstant='" + getTimeInstant() + "'" +
            "}";
    }
}
