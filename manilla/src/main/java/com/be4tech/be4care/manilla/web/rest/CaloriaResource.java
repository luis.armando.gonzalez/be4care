package com.be4tech.be4care.manilla.web.rest;

import com.be4tech.be4care.manilla.domain.Caloria;
import com.be4tech.be4care.manilla.repository.CaloriaRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.manilla.domain.Caloria}.
 */
@RestController
@RequestMapping("/api")
public class CaloriaResource {

    private final Logger log = LoggerFactory.getLogger(CaloriaResource.class);

    private static final String ENTITY_NAME = "manillaCaloria";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CaloriaRepository caloriaRepository;

    private final UserRepository userRepository;

    public CaloriaResource(CaloriaRepository caloriaRepository, UserRepository userRepository) {
        this.caloriaRepository = caloriaRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /calorias} : Create a new caloria.
     *
     * @param caloria the caloria to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new caloria, or with status {@code 400 (Bad Request)} if the caloria has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/calorias")
    public Mono<ResponseEntity<Caloria>> createCaloria(@RequestBody Caloria caloria) throws URISyntaxException {
        log.debug("REST request to save Caloria : {}", caloria);
        if (caloria.getId() != null) {
            throw new BadRequestAlertException("A new caloria cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (caloria.getUser() != null) {
            // Save user in case it's new and only exists in gateway
            userRepository.save(caloria.getUser());
        }
        return caloriaRepository
            .save(caloria)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/calorias/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /calorias/:id} : Updates an existing caloria.
     *
     * @param id the id of the caloria to save.
     * @param caloria the caloria to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated caloria,
     * or with status {@code 400 (Bad Request)} if the caloria is not valid,
     * or with status {@code 500 (Internal Server Error)} if the caloria couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/calorias/{id}")
    public Mono<ResponseEntity<Caloria>> updateCaloria(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Caloria caloria
    ) throws URISyntaxException {
        log.debug("REST request to update Caloria : {}, {}", id, caloria);
        if (caloria.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, caloria.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return caloriaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (caloria.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(caloria.getUser());
                }
                return caloriaRepository
                    .save(caloria)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /calorias/:id} : Partial updates given fields of an existing caloria, field will ignore if it is null
     *
     * @param id the id of the caloria to save.
     * @param caloria the caloria to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated caloria,
     * or with status {@code 400 (Bad Request)} if the caloria is not valid,
     * or with status {@code 404 (Not Found)} if the caloria is not found,
     * or with status {@code 500 (Internal Server Error)} if the caloria couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/calorias/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Caloria>> partialUpdateCaloria(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Caloria caloria
    ) throws URISyntaxException {
        log.debug("REST request to partial update Caloria partially : {}, {}", id, caloria);
        if (caloria.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, caloria.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return caloriaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (caloria.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(caloria.getUser());
                }

                Mono<Caloria> result = caloriaRepository
                    .findById(caloria.getId())
                    .map(existingCaloria -> {
                        if (caloria.getCaloriasActivas() != null) {
                            existingCaloria.setCaloriasActivas(caloria.getCaloriasActivas());
                        }
                        if (caloria.getDescripcion() != null) {
                            existingCaloria.setDescripcion(caloria.getDescripcion());
                        }
                        if (caloria.getFechaRegistro() != null) {
                            existingCaloria.setFechaRegistro(caloria.getFechaRegistro());
                        }

                        return existingCaloria;
                    })
                    .flatMap(caloriaRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /calorias} : get all the calorias.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of calorias in body.
     */
    @GetMapping("/calorias")
    public Mono<ResponseEntity<List<Caloria>>> getAllCalorias(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Calorias");
        return caloriaRepository
            .count()
            .zipWith(caloriaRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /calorias/:id} : get the "id" caloria.
     *
     * @param id the id of the caloria to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the caloria, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/calorias/{id}")
    public Mono<ResponseEntity<Caloria>> getCaloria(@PathVariable Long id) {
        log.debug("REST request to get Caloria : {}", id);
        Mono<Caloria> caloria = caloriaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(caloria);
    }

    /**
     * {@code DELETE  /calorias/:id} : delete the "id" caloria.
     *
     * @param id the id of the caloria to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/calorias/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteCaloria(@PathVariable Long id) {
        log.debug("REST request to delete Caloria : {}", id);
        return caloriaRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
