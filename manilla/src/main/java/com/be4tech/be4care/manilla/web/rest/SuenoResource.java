package com.be4tech.be4care.manilla.web.rest;

import com.be4tech.be4care.manilla.domain.Sueno;
import com.be4tech.be4care.manilla.repository.SuenoRepository;
import com.be4tech.be4care.manilla.repository.UserRepository;
import com.be4tech.be4care.manilla.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.manilla.domain.Sueno}.
 */
@RestController
@RequestMapping("/api")
public class SuenoResource {

    private final Logger log = LoggerFactory.getLogger(SuenoResource.class);

    private static final String ENTITY_NAME = "manillaSueno";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SuenoRepository suenoRepository;

    private final UserRepository userRepository;

    public SuenoResource(SuenoRepository suenoRepository, UserRepository userRepository) {
        this.suenoRepository = suenoRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /suenos} : Create a new sueno.
     *
     * @param sueno the sueno to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new sueno, or with status {@code 400 (Bad Request)} if the sueno has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/suenos")
    public Mono<ResponseEntity<Sueno>> createSueno(@RequestBody Sueno sueno) throws URISyntaxException {
        log.debug("REST request to save Sueno : {}", sueno);
        if (sueno.getId() != null) {
            throw new BadRequestAlertException("A new sueno cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (sueno.getUser() != null) {
            // Save user in case it's new and only exists in gateway
            userRepository.save(sueno.getUser());
        }
        return suenoRepository
            .save(sueno)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/suenos/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /suenos/:id} : Updates an existing sueno.
     *
     * @param id the id of the sueno to save.
     * @param sueno the sueno to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sueno,
     * or with status {@code 400 (Bad Request)} if the sueno is not valid,
     * or with status {@code 500 (Internal Server Error)} if the sueno couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/suenos/{id}")
    public Mono<ResponseEntity<Sueno>> updateSueno(@PathVariable(value = "id", required = false) final Long id, @RequestBody Sueno sueno)
        throws URISyntaxException {
        log.debug("REST request to update Sueno : {}, {}", id, sueno);
        if (sueno.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, sueno.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return suenoRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (sueno.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(sueno.getUser());
                }
                return suenoRepository
                    .save(sueno)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /suenos/:id} : Partial updates given fields of an existing sueno, field will ignore if it is null
     *
     * @param id the id of the sueno to save.
     * @param sueno the sueno to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sueno,
     * or with status {@code 400 (Bad Request)} if the sueno is not valid,
     * or with status {@code 404 (Not Found)} if the sueno is not found,
     * or with status {@code 500 (Internal Server Error)} if the sueno couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/suenos/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Sueno>> partialUpdateSueno(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Sueno sueno
    ) throws URISyntaxException {
        log.debug("REST request to partial update Sueno partially : {}, {}", id, sueno);
        if (sueno.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, sueno.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return suenoRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (sueno.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(sueno.getUser());
                }

                Mono<Sueno> result = suenoRepository
                    .findById(sueno.getId())
                    .map(existingSueno -> {
                        if (sueno.getSuperficial() != null) {
                            existingSueno.setSuperficial(sueno.getSuperficial());
                        }
                        if (sueno.getProfundo() != null) {
                            existingSueno.setProfundo(sueno.getProfundo());
                        }
                        if (sueno.getDespierto() != null) {
                            existingSueno.setDespierto(sueno.getDespierto());
                        }
                        if (sueno.getTimeInstant() != null) {
                            existingSueno.setTimeInstant(sueno.getTimeInstant());
                        }

                        return existingSueno;
                    })
                    .flatMap(suenoRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /suenos} : get all the suenos.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of suenos in body.
     */
    @GetMapping("/suenos")
    public Mono<ResponseEntity<List<Sueno>>> getAllSuenos(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Suenos");
        return suenoRepository
            .count()
            .zipWith(suenoRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /suenos/:id} : get the "id" sueno.
     *
     * @param id the id of the sueno to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the sueno, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/suenos/{id}")
    public Mono<ResponseEntity<Sueno>> getSueno(@PathVariable Long id) {
        log.debug("REST request to get Sueno : {}", id);
        Mono<Sueno> sueno = suenoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(sueno);
    }

    /**
     * {@code DELETE  /suenos/:id} : delete the "id" sueno.
     *
     * @param id the id of the sueno to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/suenos/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteSueno(@PathVariable Long id) {
        log.debug("REST request to delete Sueno : {}", id);
        return suenoRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
