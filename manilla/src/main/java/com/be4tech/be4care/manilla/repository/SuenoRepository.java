package com.be4tech.be4care.manilla.repository;

import com.be4tech.be4care.manilla.domain.Sueno;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Sueno entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SuenoRepository extends R2dbcRepository<Sueno, Long>, SuenoRepositoryInternal {
    Flux<Sueno> findAllBy(Pageable pageable);

    @Query("SELECT * FROM sueno entity WHERE entity.user_id = :id")
    Flux<Sueno> findByUser(Long id);

    @Query("SELECT * FROM sueno entity WHERE entity.user_id IS NULL")
    Flux<Sueno> findAllWhereUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Sueno> findAll();

    @Override
    Mono<Sueno> findById(Long id);

    @Override
    <S extends Sueno> Mono<S> save(S entity);
}

interface SuenoRepositoryInternal {
    <S extends Sueno> Mono<S> insert(S entity);
    <S extends Sueno> Mono<S> save(S entity);
    Mono<Integer> update(Sueno entity);

    Flux<Sueno> findAll();
    Mono<Sueno> findById(Long id);
    Flux<Sueno> findAllBy(Pageable pageable);
    Flux<Sueno> findAllBy(Pageable pageable, Criteria criteria);
}
