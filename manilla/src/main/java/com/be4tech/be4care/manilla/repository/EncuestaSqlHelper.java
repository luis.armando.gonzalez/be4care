package com.be4tech.be4care.manilla.repository;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Table;

public class EncuestaSqlHelper {

    public static List<Expression> getColumns(Table table, String columnPrefix) {
        List<Expression> columns = new ArrayList<>();
        columns.add(Column.aliased("id", table, columnPrefix + "_id"));
        columns.add(Column.aliased("fecha", table, columnPrefix + "_fecha"));
        columns.add(Column.aliased("debilidad", table, columnPrefix + "_debilidad"));
        columns.add(Column.aliased("cefalea", table, columnPrefix + "_cefalea"));
        columns.add(Column.aliased("calambres", table, columnPrefix + "_calambres"));
        columns.add(Column.aliased("nauseas", table, columnPrefix + "_nauseas"));
        columns.add(Column.aliased("vomito", table, columnPrefix + "_vomito"));
        columns.add(Column.aliased("mareo", table, columnPrefix + "_mareo"));
        columns.add(Column.aliased("ninguna", table, columnPrefix + "_ninguna"));

        columns.add(Column.aliased("user_id", table, columnPrefix + "_user_id"));
        return columns;
    }
}
