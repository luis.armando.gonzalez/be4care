/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import Fisiometria1DetailComponent from '@/entities/manilla/fisiometria-1/fisiometria-1-details.vue';
import Fisiometria1Class from '@/entities/manilla/fisiometria-1/fisiometria-1-details.component';
import Fisiometria1Service from '@/entities/manilla/fisiometria-1/fisiometria-1.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Fisiometria1 Management Detail Component', () => {
    let wrapper: Wrapper<Fisiometria1Class>;
    let comp: Fisiometria1Class;
    let fisiometria1ServiceStub: SinonStubbedInstance<Fisiometria1Service>;

    beforeEach(() => {
      fisiometria1ServiceStub = sinon.createStubInstance<Fisiometria1Service>(Fisiometria1Service);

      wrapper = shallowMount<Fisiometria1Class>(Fisiometria1DetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { fisiometria1Service: () => fisiometria1ServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundFisiometria1 = { id: 123 };
        fisiometria1ServiceStub.find.resolves(foundFisiometria1);

        // WHEN
        comp.retrieveFisiometria1(123);
        await comp.$nextTick();

        // THEN
        expect(comp.fisiometria1).toBe(foundFisiometria1);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundFisiometria1 = { id: 123 };
        fisiometria1ServiceStub.find.resolves(foundFisiometria1);

        // WHEN
        comp.beforeRouteEnter({ params: { fisiometria1Id: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.fisiometria1).toBe(foundFisiometria1);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
