/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import CaloriaDetailComponent from '@/entities/manilla/caloria/caloria-details.vue';
import CaloriaClass from '@/entities/manilla/caloria/caloria-details.component';
import CaloriaService from '@/entities/manilla/caloria/caloria.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Caloria Management Detail Component', () => {
    let wrapper: Wrapper<CaloriaClass>;
    let comp: CaloriaClass;
    let caloriaServiceStub: SinonStubbedInstance<CaloriaService>;

    beforeEach(() => {
      caloriaServiceStub = sinon.createStubInstance<CaloriaService>(CaloriaService);

      wrapper = shallowMount<CaloriaClass>(CaloriaDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { caloriaService: () => caloriaServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundCaloria = { id: 123 };
        caloriaServiceStub.find.resolves(foundCaloria);

        // WHEN
        comp.retrieveCaloria(123);
        await comp.$nextTick();

        // THEN
        expect(comp.caloria).toBe(foundCaloria);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundCaloria = { id: 123 };
        caloriaServiceStub.find.resolves(foundCaloria);

        // WHEN
        comp.beforeRouteEnter({ params: { caloriaId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.caloria).toBe(foundCaloria);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
