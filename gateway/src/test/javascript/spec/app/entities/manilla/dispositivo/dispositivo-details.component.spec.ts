/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import DispositivoDetailComponent from '@/entities/manilla/dispositivo/dispositivo-details.vue';
import DispositivoClass from '@/entities/manilla/dispositivo/dispositivo-details.component';
import DispositivoService from '@/entities/manilla/dispositivo/dispositivo.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Dispositivo Management Detail Component', () => {
    let wrapper: Wrapper<DispositivoClass>;
    let comp: DispositivoClass;
    let dispositivoServiceStub: SinonStubbedInstance<DispositivoService>;

    beforeEach(() => {
      dispositivoServiceStub = sinon.createStubInstance<DispositivoService>(DispositivoService);

      wrapper = shallowMount<DispositivoClass>(DispositivoDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { dispositivoService: () => dispositivoServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundDispositivo = { id: 123 };
        dispositivoServiceStub.find.resolves(foundDispositivo);

        // WHEN
        comp.retrieveDispositivo(123);
        await comp.$nextTick();

        // THEN
        expect(comp.dispositivo).toBe(foundDispositivo);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundDispositivo = { id: 123 };
        dispositivoServiceStub.find.resolves(foundDispositivo);

        // WHEN
        comp.beforeRouteEnter({ params: { dispositivoId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.dispositivo).toBe(foundDispositivo);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
