/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import * as config from '@/shared/config/config';
import NotificacionUpdateComponent from '@/entities/manilla/notificacion/notificacion-update.vue';
import NotificacionClass from '@/entities/manilla/notificacion/notificacion-update.component';
import NotificacionService from '@/entities/manilla/notificacion/notificacion.service';

import TokenDispService from '@/entities/manilla/token-disp/token-disp.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Notificacion Management Update Component', () => {
    let wrapper: Wrapper<NotificacionClass>;
    let comp: NotificacionClass;
    let notificacionServiceStub: SinonStubbedInstance<NotificacionService>;

    beforeEach(() => {
      notificacionServiceStub = sinon.createStubInstance<NotificacionService>(NotificacionService);

      wrapper = shallowMount<NotificacionClass>(NotificacionUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          notificacionService: () => notificacionServiceStub,
          alertService: () => new AlertService(),

          tokenDispService: () => new TokenDispService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('load', () => {
      it('Should convert date from string', () => {
        // GIVEN
        const date = new Date('2019-10-15T11:42:02Z');

        // WHEN
        const convertedDate = comp.convertDateTimeFromServer(date);

        // THEN
        expect(convertedDate).toEqual(dayjs(date).format(DATE_TIME_LONG_FORMAT));
      });

      it('Should not convert date if date is not present', () => {
        expect(comp.convertDateTimeFromServer(null)).toBeNull();
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.notificacion = entity;
        notificacionServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(notificacionServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.notificacion = entity;
        notificacionServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(notificacionServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundNotificacion = { id: 123 };
        notificacionServiceStub.find.resolves(foundNotificacion);
        notificacionServiceStub.retrieve.resolves([foundNotificacion]);

        // WHEN
        comp.beforeRouteEnter({ params: { notificacionId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.notificacion).toBe(foundNotificacion);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
