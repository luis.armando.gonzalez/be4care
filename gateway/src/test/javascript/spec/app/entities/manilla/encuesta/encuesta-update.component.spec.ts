/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import * as config from '@/shared/config/config';
import EncuestaUpdateComponent from '@/entities/manilla/encuesta/encuesta-update.vue';
import EncuestaClass from '@/entities/manilla/encuesta/encuesta-update.component';
import EncuestaService from '@/entities/manilla/encuesta/encuesta.service';

import UserOAuth2Service from '@/entities/user/user.oauth2.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Encuesta Management Update Component', () => {
    let wrapper: Wrapper<EncuestaClass>;
    let comp: EncuestaClass;
    let encuestaServiceStub: SinonStubbedInstance<EncuestaService>;

    beforeEach(() => {
      encuestaServiceStub = sinon.createStubInstance<EncuestaService>(EncuestaService);

      wrapper = shallowMount<EncuestaClass>(EncuestaUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          encuestaService: () => encuestaServiceStub,
          alertService: () => new AlertService(),

          userOAuth2Service: () => new UserOAuth2Service(),
        },
      });
      comp = wrapper.vm;
    });

    describe('load', () => {
      it('Should convert date from string', () => {
        // GIVEN
        const date = new Date('2019-10-15T11:42:02Z');

        // WHEN
        const convertedDate = comp.convertDateTimeFromServer(date);

        // THEN
        expect(convertedDate).toEqual(dayjs(date).format(DATE_TIME_LONG_FORMAT));
      });

      it('Should not convert date if date is not present', () => {
        expect(comp.convertDateTimeFromServer(null)).toBeNull();
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.encuesta = entity;
        encuestaServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(encuestaServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.encuesta = entity;
        encuestaServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(encuestaServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundEncuesta = { id: 123 };
        encuestaServiceStub.find.resolves(foundEncuesta);
        encuestaServiceStub.retrieve.resolves([foundEncuesta]);

        // WHEN
        comp.beforeRouteEnter({ params: { encuestaId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.encuesta).toBe(foundEncuesta);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
