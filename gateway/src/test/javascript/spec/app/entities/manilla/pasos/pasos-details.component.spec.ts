/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import PasosDetailComponent from '@/entities/manilla/pasos/pasos-details.vue';
import PasosClass from '@/entities/manilla/pasos/pasos-details.component';
import PasosService from '@/entities/manilla/pasos/pasos.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Pasos Management Detail Component', () => {
    let wrapper: Wrapper<PasosClass>;
    let comp: PasosClass;
    let pasosServiceStub: SinonStubbedInstance<PasosService>;

    beforeEach(() => {
      pasosServiceStub = sinon.createStubInstance<PasosService>(PasosService);

      wrapper = shallowMount<PasosClass>(PasosDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { pasosService: () => pasosServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundPasos = { id: 123 };
        pasosServiceStub.find.resolves(foundPasos);

        // WHEN
        comp.retrievePasos(123);
        await comp.$nextTick();

        // THEN
        expect(comp.pasos).toBe(foundPasos);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundPasos = { id: 123 };
        pasosServiceStub.find.resolves(foundPasos);

        // WHEN
        comp.beforeRouteEnter({ params: { pasosId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.pasos).toBe(foundPasos);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
