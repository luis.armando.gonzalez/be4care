/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import IngestaComponent from '@/entities/manilla/ingesta/ingesta.vue';
import IngestaClass from '@/entities/manilla/ingesta/ingesta.component';
import IngestaService from '@/entities/manilla/ingesta/ingesta.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Ingesta Management Component', () => {
    let wrapper: Wrapper<IngestaClass>;
    let comp: IngestaClass;
    let ingestaServiceStub: SinonStubbedInstance<IngestaService>;

    beforeEach(() => {
      ingestaServiceStub = sinon.createStubInstance<IngestaService>(IngestaService);
      ingestaServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<IngestaClass>(IngestaComponent, {
        store,
        i18n,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          ingestaService: () => ingestaServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      ingestaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllIngestas();
      await comp.$nextTick();

      // THEN
      expect(ingestaServiceStub.retrieve.called).toBeTruthy();
      expect(comp.ingestas[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      ingestaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(ingestaServiceStub.retrieve.called).toBeTruthy();
      expect(comp.ingestas[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      ingestaServiceStub.retrieve.reset();
      ingestaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(ingestaServiceStub.retrieve.callCount).toEqual(2);
      expect(comp.page).toEqual(1);
      expect(comp.ingestas[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      ingestaServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeIngesta();
      await comp.$nextTick();

      // THEN
      expect(ingestaServiceStub.delete.called).toBeTruthy();
      expect(ingestaServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});
