/* tslint:disable max-line-length */
import axios from 'axios';
import sinon from 'sinon';
import dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from '@/shared/date/filters';
import Fisiometria1Service from '@/entities/manilla/fisiometria-1/fisiometria-1.service';
import { Fisiometria1 } from '@/shared/model/manilla/fisiometria-1.model';

const error = {
  response: {
    status: null,
    data: {
      type: null,
    },
  },
};

const axiosStub = {
  get: sinon.stub(axios, 'get'),
  post: sinon.stub(axios, 'post'),
  put: sinon.stub(axios, 'put'),
  patch: sinon.stub(axios, 'patch'),
  delete: sinon.stub(axios, 'delete'),
};

describe('Service Tests', () => {
  describe('Fisiometria1 Service', () => {
    let service: Fisiometria1Service;
    let elemDefault;
    let currentDate: Date;

    beforeEach(() => {
      service = new Fisiometria1Service();
      currentDate = new Date();
      elemDefault = new Fisiometria1(123, 0, 0, 0, 0, 0, 0, currentDate, currentDate);
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign(
          {
            fechaRegistro: dayjs(currentDate).format(DATE_TIME_FORMAT),
            fechaToma: dayjs(currentDate).format(DATE_TIME_FORMAT),
          },
          elemDefault
        );
        axiosStub.get.resolves({ data: returnedFromService });

        return service.find(123).then(res => {
          expect(res).toMatchObject(elemDefault);
        });
      });

      it('should not find an element', async () => {
        axiosStub.get.rejects(error);
        return service
          .find(123)
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should create a Fisiometria1', async () => {
        const returnedFromService = Object.assign(
          {
            id: 123,
            fechaRegistro: dayjs(currentDate).format(DATE_TIME_FORMAT),
            fechaToma: dayjs(currentDate).format(DATE_TIME_FORMAT),
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            fechaRegistro: currentDate,
            fechaToma: currentDate,
          },
          returnedFromService
        );

        axiosStub.post.resolves({ data: returnedFromService });
        return service.create({}).then(res => {
          expect(res).toMatchObject(expected);
        });
      });

      it('should not create a Fisiometria1', async () => {
        axiosStub.post.rejects(error);

        return service
          .create({})
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should update a Fisiometria1', async () => {
        const returnedFromService = Object.assign(
          {
            ritmoCardiaco: 1,
            ritmoRespiratorio: 1,
            oximetria: 1,
            presionArterialSistolica: 1,
            presionArterialDiastolica: 1,
            temperatura: 1,
            fechaRegistro: dayjs(currentDate).format(DATE_TIME_FORMAT),
            fechaToma: dayjs(currentDate).format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            fechaRegistro: currentDate,
            fechaToma: currentDate,
          },
          returnedFromService
        );
        axiosStub.put.resolves({ data: returnedFromService });

        return service.update(expected).then(res => {
          expect(res).toMatchObject(expected);
        });
      });

      it('should not update a Fisiometria1', async () => {
        axiosStub.put.rejects(error);

        return service
          .update({})
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should partial update a Fisiometria1', async () => {
        const patchObject = Object.assign(
          {
            ritmoCardiaco: 1,
            presionArterialSistolica: 1,
            temperatura: 1,
            fechaToma: dayjs(currentDate).format(DATE_TIME_FORMAT),
          },
          new Fisiometria1()
        );
        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            fechaRegistro: currentDate,
            fechaToma: currentDate,
          },
          returnedFromService
        );
        axiosStub.patch.resolves({ data: returnedFromService });

        return service.partialUpdate(patchObject).then(res => {
          expect(res).toMatchObject(expected);
        });
      });

      it('should not partial update a Fisiometria1', async () => {
        axiosStub.patch.rejects(error);

        return service
          .partialUpdate({})
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should return a list of Fisiometria1', async () => {
        const returnedFromService = Object.assign(
          {
            ritmoCardiaco: 1,
            ritmoRespiratorio: 1,
            oximetria: 1,
            presionArterialSistolica: 1,
            presionArterialDiastolica: 1,
            temperatura: 1,
            fechaRegistro: dayjs(currentDate).format(DATE_TIME_FORMAT),
            fechaToma: dayjs(currentDate).format(DATE_TIME_FORMAT),
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            fechaRegistro: currentDate,
            fechaToma: currentDate,
          },
          returnedFromService
        );
        axiosStub.get.resolves([returnedFromService]);
        return service.retrieve({ sort: {}, page: 0, size: 10 }).then(res => {
          expect(res).toContainEqual(expected);
        });
      });

      it('should not return a list of Fisiometria1', async () => {
        axiosStub.get.rejects(error);

        return service
          .retrieve()
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should delete a Fisiometria1', async () => {
        axiosStub.delete.resolves({ ok: true });
        return service.delete(123).then(res => {
          expect(res.ok).toBeTruthy();
        });
      });

      it('should not delete a Fisiometria1', async () => {
        axiosStub.delete.rejects(error);

        return service
          .delete(123)
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });
    });
  });
});
