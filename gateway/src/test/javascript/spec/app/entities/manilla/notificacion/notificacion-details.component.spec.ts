/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import NotificacionDetailComponent from '@/entities/manilla/notificacion/notificacion-details.vue';
import NotificacionClass from '@/entities/manilla/notificacion/notificacion-details.component';
import NotificacionService from '@/entities/manilla/notificacion/notificacion.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Notificacion Management Detail Component', () => {
    let wrapper: Wrapper<NotificacionClass>;
    let comp: NotificacionClass;
    let notificacionServiceStub: SinonStubbedInstance<NotificacionService>;

    beforeEach(() => {
      notificacionServiceStub = sinon.createStubInstance<NotificacionService>(NotificacionService);

      wrapper = shallowMount<NotificacionClass>(NotificacionDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { notificacionService: () => notificacionServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundNotificacion = { id: 123 };
        notificacionServiceStub.find.resolves(foundNotificacion);

        // WHEN
        comp.retrieveNotificacion(123);
        await comp.$nextTick();

        // THEN
        expect(comp.notificacion).toBe(foundNotificacion);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundNotificacion = { id: 123 };
        notificacionServiceStub.find.resolves(foundNotificacion);

        // WHEN
        comp.beforeRouteEnter({ params: { notificacionId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.notificacion).toBe(foundNotificacion);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
