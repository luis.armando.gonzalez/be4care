/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import FrecuenciaCardiacaDetailComponent from '@/entities/manilla/frecuencia-cardiaca/frecuencia-cardiaca-details.vue';
import FrecuenciaCardiacaClass from '@/entities/manilla/frecuencia-cardiaca/frecuencia-cardiaca-details.component';
import FrecuenciaCardiacaService from '@/entities/manilla/frecuencia-cardiaca/frecuencia-cardiaca.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('FrecuenciaCardiaca Management Detail Component', () => {
    let wrapper: Wrapper<FrecuenciaCardiacaClass>;
    let comp: FrecuenciaCardiacaClass;
    let frecuenciaCardiacaServiceStub: SinonStubbedInstance<FrecuenciaCardiacaService>;

    beforeEach(() => {
      frecuenciaCardiacaServiceStub = sinon.createStubInstance<FrecuenciaCardiacaService>(FrecuenciaCardiacaService);

      wrapper = shallowMount<FrecuenciaCardiacaClass>(FrecuenciaCardiacaDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { frecuenciaCardiacaService: () => frecuenciaCardiacaServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundFrecuenciaCardiaca = { id: 123 };
        frecuenciaCardiacaServiceStub.find.resolves(foundFrecuenciaCardiaca);

        // WHEN
        comp.retrieveFrecuenciaCardiaca(123);
        await comp.$nextTick();

        // THEN
        expect(comp.frecuenciaCardiaca).toBe(foundFrecuenciaCardiaca);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundFrecuenciaCardiaca = { id: 123 };
        frecuenciaCardiacaServiceStub.find.resolves(foundFrecuenciaCardiaca);

        // WHEN
        comp.beforeRouteEnter({ params: { frecuenciaCardiacaId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.frecuenciaCardiaca).toBe(foundFrecuenciaCardiaca);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
