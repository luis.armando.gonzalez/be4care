/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import TemperaturaDetailComponent from '@/entities/manilla/temperatura/temperatura-details.vue';
import TemperaturaClass from '@/entities/manilla/temperatura/temperatura-details.component';
import TemperaturaService from '@/entities/manilla/temperatura/temperatura.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Temperatura Management Detail Component', () => {
    let wrapper: Wrapper<TemperaturaClass>;
    let comp: TemperaturaClass;
    let temperaturaServiceStub: SinonStubbedInstance<TemperaturaService>;

    beforeEach(() => {
      temperaturaServiceStub = sinon.createStubInstance<TemperaturaService>(TemperaturaService);

      wrapper = shallowMount<TemperaturaClass>(TemperaturaDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { temperaturaService: () => temperaturaServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundTemperatura = { id: 123 };
        temperaturaServiceStub.find.resolves(foundTemperatura);

        // WHEN
        comp.retrieveTemperatura(123);
        await comp.$nextTick();

        // THEN
        expect(comp.temperatura).toBe(foundTemperatura);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundTemperatura = { id: 123 };
        temperaturaServiceStub.find.resolves(foundTemperatura);

        // WHEN
        comp.beforeRouteEnter({ params: { temperaturaId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.temperatura).toBe(foundTemperatura);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
