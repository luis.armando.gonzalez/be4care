/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import IngestaDetailComponent from '@/entities/manilla/ingesta/ingesta-details.vue';
import IngestaClass from '@/entities/manilla/ingesta/ingesta-details.component';
import IngestaService from '@/entities/manilla/ingesta/ingesta.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Ingesta Management Detail Component', () => {
    let wrapper: Wrapper<IngestaClass>;
    let comp: IngestaClass;
    let ingestaServiceStub: SinonStubbedInstance<IngestaService>;

    beforeEach(() => {
      ingestaServiceStub = sinon.createStubInstance<IngestaService>(IngestaService);

      wrapper = shallowMount<IngestaClass>(IngestaDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { ingestaService: () => ingestaServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundIngesta = { id: 123 };
        ingestaServiceStub.find.resolves(foundIngesta);

        // WHEN
        comp.retrieveIngesta(123);
        await comp.$nextTick();

        // THEN
        expect(comp.ingesta).toBe(foundIngesta);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundIngesta = { id: 123 };
        ingestaServiceStub.find.resolves(foundIngesta);

        // WHEN
        comp.beforeRouteEnter({ params: { ingestaId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.ingesta).toBe(foundIngesta);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
