/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import SuenoDetailComponent from '@/entities/manilla/sueno/sueno-details.vue';
import SuenoClass from '@/entities/manilla/sueno/sueno-details.component';
import SuenoService from '@/entities/manilla/sueno/sueno.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Sueno Management Detail Component', () => {
    let wrapper: Wrapper<SuenoClass>;
    let comp: SuenoClass;
    let suenoServiceStub: SinonStubbedInstance<SuenoService>;

    beforeEach(() => {
      suenoServiceStub = sinon.createStubInstance<SuenoService>(SuenoService);

      wrapper = shallowMount<SuenoClass>(SuenoDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { suenoService: () => suenoServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundSueno = { id: 123 };
        suenoServiceStub.find.resolves(foundSueno);

        // WHEN
        comp.retrieveSueno(123);
        await comp.$nextTick();

        // THEN
        expect(comp.sueno).toBe(foundSueno);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundSueno = { id: 123 };
        suenoServiceStub.find.resolves(foundSueno);

        // WHEN
        comp.beforeRouteEnter({ params: { suenoId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.sueno).toBe(foundSueno);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
