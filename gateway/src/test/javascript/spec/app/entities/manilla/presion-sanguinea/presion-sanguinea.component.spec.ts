/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import PresionSanguineaComponent from '@/entities/manilla/presion-sanguinea/presion-sanguinea.vue';
import PresionSanguineaClass from '@/entities/manilla/presion-sanguinea/presion-sanguinea.component';
import PresionSanguineaService from '@/entities/manilla/presion-sanguinea/presion-sanguinea.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('PresionSanguinea Management Component', () => {
    let wrapper: Wrapper<PresionSanguineaClass>;
    let comp: PresionSanguineaClass;
    let presionSanguineaServiceStub: SinonStubbedInstance<PresionSanguineaService>;

    beforeEach(() => {
      presionSanguineaServiceStub = sinon.createStubInstance<PresionSanguineaService>(PresionSanguineaService);
      presionSanguineaServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<PresionSanguineaClass>(PresionSanguineaComponent, {
        store,
        i18n,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          presionSanguineaService: () => presionSanguineaServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      presionSanguineaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllPresionSanguineas();
      await comp.$nextTick();

      // THEN
      expect(presionSanguineaServiceStub.retrieve.called).toBeTruthy();
      expect(comp.presionSanguineas[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      presionSanguineaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(presionSanguineaServiceStub.retrieve.called).toBeTruthy();
      expect(comp.presionSanguineas[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      presionSanguineaServiceStub.retrieve.reset();
      presionSanguineaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(presionSanguineaServiceStub.retrieve.callCount).toEqual(2);
      expect(comp.page).toEqual(1);
      expect(comp.presionSanguineas[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      presionSanguineaServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removePresionSanguinea();
      await comp.$nextTick();

      // THEN
      expect(presionSanguineaServiceStub.delete.called).toBeTruthy();
      expect(presionSanguineaServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});
