/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import OximetriaComponent from '@/entities/manilla/oximetria/oximetria.vue';
import OximetriaClass from '@/entities/manilla/oximetria/oximetria.component';
import OximetriaService from '@/entities/manilla/oximetria/oximetria.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Oximetria Management Component', () => {
    let wrapper: Wrapper<OximetriaClass>;
    let comp: OximetriaClass;
    let oximetriaServiceStub: SinonStubbedInstance<OximetriaService>;

    beforeEach(() => {
      oximetriaServiceStub = sinon.createStubInstance<OximetriaService>(OximetriaService);
      oximetriaServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<OximetriaClass>(OximetriaComponent, {
        store,
        i18n,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          oximetriaService: () => oximetriaServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      oximetriaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllOximetrias();
      await comp.$nextTick();

      // THEN
      expect(oximetriaServiceStub.retrieve.called).toBeTruthy();
      expect(comp.oximetrias[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      oximetriaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(oximetriaServiceStub.retrieve.called).toBeTruthy();
      expect(comp.oximetrias[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      oximetriaServiceStub.retrieve.reset();
      oximetriaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(oximetriaServiceStub.retrieve.callCount).toEqual(2);
      expect(comp.page).toEqual(1);
      expect(comp.oximetrias[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      oximetriaServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeOximetria();
      await comp.$nextTick();

      // THEN
      expect(oximetriaServiceStub.delete.called).toBeTruthy();
      expect(oximetriaServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});
