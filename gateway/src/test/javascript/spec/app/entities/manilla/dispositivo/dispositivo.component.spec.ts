/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import DispositivoComponent from '@/entities/manilla/dispositivo/dispositivo.vue';
import DispositivoClass from '@/entities/manilla/dispositivo/dispositivo.component';
import DispositivoService from '@/entities/manilla/dispositivo/dispositivo.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Dispositivo Management Component', () => {
    let wrapper: Wrapper<DispositivoClass>;
    let comp: DispositivoClass;
    let dispositivoServiceStub: SinonStubbedInstance<DispositivoService>;

    beforeEach(() => {
      dispositivoServiceStub = sinon.createStubInstance<DispositivoService>(DispositivoService);
      dispositivoServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<DispositivoClass>(DispositivoComponent, {
        store,
        i18n,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          dispositivoService: () => dispositivoServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      dispositivoServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllDispositivos();
      await comp.$nextTick();

      // THEN
      expect(dispositivoServiceStub.retrieve.called).toBeTruthy();
      expect(comp.dispositivos[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      dispositivoServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(dispositivoServiceStub.retrieve.called).toBeTruthy();
      expect(comp.dispositivos[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      dispositivoServiceStub.retrieve.reset();
      dispositivoServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(dispositivoServiceStub.retrieve.callCount).toEqual(2);
      expect(comp.page).toEqual(1);
      expect(comp.dispositivos[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      dispositivoServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeDispositivo();
      await comp.$nextTick();

      // THEN
      expect(dispositivoServiceStub.delete.called).toBeTruthy();
      expect(dispositivoServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});
