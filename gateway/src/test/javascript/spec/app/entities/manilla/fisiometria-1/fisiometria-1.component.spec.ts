/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import Fisiometria1Component from '@/entities/manilla/fisiometria-1/fisiometria-1.vue';
import Fisiometria1Class from '@/entities/manilla/fisiometria-1/fisiometria-1.component';
import Fisiometria1Service from '@/entities/manilla/fisiometria-1/fisiometria-1.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Fisiometria1 Management Component', () => {
    let wrapper: Wrapper<Fisiometria1Class>;
    let comp: Fisiometria1Class;
    let fisiometria1ServiceStub: SinonStubbedInstance<Fisiometria1Service>;

    beforeEach(() => {
      fisiometria1ServiceStub = sinon.createStubInstance<Fisiometria1Service>(Fisiometria1Service);
      fisiometria1ServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<Fisiometria1Class>(Fisiometria1Component, {
        store,
        i18n,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          fisiometria1Service: () => fisiometria1ServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      fisiometria1ServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllFisiometria1s();
      await comp.$nextTick();

      // THEN
      expect(fisiometria1ServiceStub.retrieve.called).toBeTruthy();
      expect(comp.fisiometria1s[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      fisiometria1ServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(fisiometria1ServiceStub.retrieve.called).toBeTruthy();
      expect(comp.fisiometria1s[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      fisiometria1ServiceStub.retrieve.reset();
      fisiometria1ServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(fisiometria1ServiceStub.retrieve.callCount).toEqual(2);
      expect(comp.page).toEqual(1);
      expect(comp.fisiometria1s[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      fisiometria1ServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeFisiometria1();
      await comp.$nextTick();

      // THEN
      expect(fisiometria1ServiceStub.delete.called).toBeTruthy();
      expect(fisiometria1ServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});
