/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import OximetriaDetailComponent from '@/entities/manilla/oximetria/oximetria-details.vue';
import OximetriaClass from '@/entities/manilla/oximetria/oximetria-details.component';
import OximetriaService from '@/entities/manilla/oximetria/oximetria.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Oximetria Management Detail Component', () => {
    let wrapper: Wrapper<OximetriaClass>;
    let comp: OximetriaClass;
    let oximetriaServiceStub: SinonStubbedInstance<OximetriaService>;

    beforeEach(() => {
      oximetriaServiceStub = sinon.createStubInstance<OximetriaService>(OximetriaService);

      wrapper = shallowMount<OximetriaClass>(OximetriaDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { oximetriaService: () => oximetriaServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundOximetria = { id: 123 };
        oximetriaServiceStub.find.resolves(foundOximetria);

        // WHEN
        comp.retrieveOximetria(123);
        await comp.$nextTick();

        // THEN
        expect(comp.oximetria).toBe(foundOximetria);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundOximetria = { id: 123 };
        oximetriaServiceStub.find.resolves(foundOximetria);

        // WHEN
        comp.beforeRouteEnter({ params: { oximetriaId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.oximetria).toBe(foundOximetria);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
