/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import TemperaturaComponent from '@/entities/manilla/temperatura/temperatura.vue';
import TemperaturaClass from '@/entities/manilla/temperatura/temperatura.component';
import TemperaturaService from '@/entities/manilla/temperatura/temperatura.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Temperatura Management Component', () => {
    let wrapper: Wrapper<TemperaturaClass>;
    let comp: TemperaturaClass;
    let temperaturaServiceStub: SinonStubbedInstance<TemperaturaService>;

    beforeEach(() => {
      temperaturaServiceStub = sinon.createStubInstance<TemperaturaService>(TemperaturaService);
      temperaturaServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<TemperaturaClass>(TemperaturaComponent, {
        store,
        i18n,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          temperaturaService: () => temperaturaServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      temperaturaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllTemperaturas();
      await comp.$nextTick();

      // THEN
      expect(temperaturaServiceStub.retrieve.called).toBeTruthy();
      expect(comp.temperaturas[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      temperaturaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(temperaturaServiceStub.retrieve.called).toBeTruthy();
      expect(comp.temperaturas[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      temperaturaServiceStub.retrieve.reset();
      temperaturaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(temperaturaServiceStub.retrieve.callCount).toEqual(2);
      expect(comp.page).toEqual(1);
      expect(comp.temperaturas[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      temperaturaServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeTemperatura();
      await comp.$nextTick();

      // THEN
      expect(temperaturaServiceStub.delete.called).toBeTruthy();
      expect(temperaturaServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});
