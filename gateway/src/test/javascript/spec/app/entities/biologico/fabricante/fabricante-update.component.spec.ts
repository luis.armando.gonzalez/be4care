/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import * as config from '@/shared/config/config';
import FabricanteUpdateComponent from '@/entities/biologico/fabricante/fabricante-update.vue';
import FabricanteClass from '@/entities/biologico/fabricante/fabricante-update.component';
import FabricanteService from '@/entities/biologico/fabricante/fabricante.service';

import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Fabricante Management Update Component', () => {
    let wrapper: Wrapper<FabricanteClass>;
    let comp: FabricanteClass;
    let fabricanteServiceStub: SinonStubbedInstance<FabricanteService>;

    beforeEach(() => {
      fabricanteServiceStub = sinon.createStubInstance<FabricanteService>(FabricanteService);

      wrapper = shallowMount<FabricanteClass>(FabricanteUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          fabricanteService: () => fabricanteServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.fabricante = entity;
        fabricanteServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(fabricanteServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.fabricante = entity;
        fabricanteServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(fabricanteServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundFabricante = { id: 123 };
        fabricanteServiceStub.find.resolves(foundFabricante);
        fabricanteServiceStub.retrieve.resolves([foundFabricante]);

        // WHEN
        comp.beforeRouteEnter({ params: { fabricanteId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.fabricante).toBe(foundFabricante);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
