/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import CicloActividadDetailComponent from '@/entities/biologico/ciclo-actividad/ciclo-actividad-details.vue';
import CicloActividadClass from '@/entities/biologico/ciclo-actividad/ciclo-actividad-details.component';
import CicloActividadService from '@/entities/biologico/ciclo-actividad/ciclo-actividad.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('CicloActividad Management Detail Component', () => {
    let wrapper: Wrapper<CicloActividadClass>;
    let comp: CicloActividadClass;
    let cicloActividadServiceStub: SinonStubbedInstance<CicloActividadService>;

    beforeEach(() => {
      cicloActividadServiceStub = sinon.createStubInstance<CicloActividadService>(CicloActividadService);

      wrapper = shallowMount<CicloActividadClass>(CicloActividadDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { cicloActividadService: () => cicloActividadServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundCicloActividad = { id: 123 };
        cicloActividadServiceStub.find.resolves(foundCicloActividad);

        // WHEN
        comp.retrieveCicloActividad(123);
        await comp.$nextTick();

        // THEN
        expect(comp.cicloActividad).toBe(foundCicloActividad);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundCicloActividad = { id: 123 };
        cicloActividadServiceStub.find.resolves(foundCicloActividad);

        // WHEN
        comp.beforeRouteEnter({ params: { cicloActividadId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.cicloActividad).toBe(foundCicloActividad);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
