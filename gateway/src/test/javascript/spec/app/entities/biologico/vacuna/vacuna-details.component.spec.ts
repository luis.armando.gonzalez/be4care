/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import VacunaDetailComponent from '@/entities/biologico/vacuna/vacuna-details.vue';
import VacunaClass from '@/entities/biologico/vacuna/vacuna-details.component';
import VacunaService from '@/entities/biologico/vacuna/vacuna.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Vacuna Management Detail Component', () => {
    let wrapper: Wrapper<VacunaClass>;
    let comp: VacunaClass;
    let vacunaServiceStub: SinonStubbedInstance<VacunaService>;

    beforeEach(() => {
      vacunaServiceStub = sinon.createStubInstance<VacunaService>(VacunaService);

      wrapper = shallowMount<VacunaClass>(VacunaDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { vacunaService: () => vacunaServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundVacuna = { id: 123 };
        vacunaServiceStub.find.resolves(foundVacuna);

        // WHEN
        comp.retrieveVacuna(123);
        await comp.$nextTick();

        // THEN
        expect(comp.vacuna).toBe(foundVacuna);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundVacuna = { id: 123 };
        vacunaServiceStub.find.resolves(foundVacuna);

        // WHEN
        comp.beforeRouteEnter({ params: { vacunaId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.vacuna).toBe(foundVacuna);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
