/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import FabricanteDetailComponent from '@/entities/biologico/fabricante/fabricante-details.vue';
import FabricanteClass from '@/entities/biologico/fabricante/fabricante-details.component';
import FabricanteService from '@/entities/biologico/fabricante/fabricante.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Fabricante Management Detail Component', () => {
    let wrapper: Wrapper<FabricanteClass>;
    let comp: FabricanteClass;
    let fabricanteServiceStub: SinonStubbedInstance<FabricanteService>;

    beforeEach(() => {
      fabricanteServiceStub = sinon.createStubInstance<FabricanteService>(FabricanteService);

      wrapper = shallowMount<FabricanteClass>(FabricanteDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { fabricanteService: () => fabricanteServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundFabricante = { id: 123 };
        fabricanteServiceStub.find.resolves(foundFabricante);

        // WHEN
        comp.retrieveFabricante(123);
        await comp.$nextTick();

        // THEN
        expect(comp.fabricante).toBe(foundFabricante);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundFabricante = { id: 123 };
        fabricanteServiceStub.find.resolves(foundFabricante);

        // WHEN
        comp.beforeRouteEnter({ params: { fabricanteId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.fabricante).toBe(foundFabricante);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
