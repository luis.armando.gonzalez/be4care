/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import BiologicoDetailComponent from '@/entities/biologico/biologico/biologico-details.vue';
import BiologicoClass from '@/entities/biologico/biologico/biologico-details.component';
import BiologicoService from '@/entities/biologico/biologico/biologico.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Biologico Management Detail Component', () => {
    let wrapper: Wrapper<BiologicoClass>;
    let comp: BiologicoClass;
    let biologicoServiceStub: SinonStubbedInstance<BiologicoService>;

    beforeEach(() => {
      biologicoServiceStub = sinon.createStubInstance<BiologicoService>(BiologicoService);

      wrapper = shallowMount<BiologicoClass>(BiologicoDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { biologicoService: () => biologicoServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundBiologico = { id: 123 };
        biologicoServiceStub.find.resolves(foundBiologico);

        // WHEN
        comp.retrieveBiologico(123);
        await comp.$nextTick();

        // THEN
        expect(comp.biologico).toBe(foundBiologico);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundBiologico = { id: 123 };
        biologicoServiceStub.find.resolves(foundBiologico);

        // WHEN
        comp.beforeRouteEnter({ params: { biologicoId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.biologico).toBe(foundBiologico);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
