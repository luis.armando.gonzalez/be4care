/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import HistorialActividadDetailComponent from '@/entities/biologico/historial-actividad/historial-actividad-details.vue';
import HistorialActividadClass from '@/entities/biologico/historial-actividad/historial-actividad-details.component';
import HistorialActividadService from '@/entities/biologico/historial-actividad/historial-actividad.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('HistorialActividad Management Detail Component', () => {
    let wrapper: Wrapper<HistorialActividadClass>;
    let comp: HistorialActividadClass;
    let historialActividadServiceStub: SinonStubbedInstance<HistorialActividadService>;

    beforeEach(() => {
      historialActividadServiceStub = sinon.createStubInstance<HistorialActividadService>(HistorialActividadService);

      wrapper = shallowMount<HistorialActividadClass>(HistorialActividadDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { historialActividadService: () => historialActividadServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundHistorialActividad = { id: 123 };
        historialActividadServiceStub.find.resolves(foundHistorialActividad);

        // WHEN
        comp.retrieveHistorialActividad(123);
        await comp.$nextTick();

        // THEN
        expect(comp.historialActividad).toBe(foundHistorialActividad);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundHistorialActividad = { id: 123 };
        historialActividadServiceStub.find.resolves(foundHistorialActividad);

        // WHEN
        comp.beforeRouteEnter({ params: { historialActividadId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.historialActividad).toBe(foundHistorialActividad);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
