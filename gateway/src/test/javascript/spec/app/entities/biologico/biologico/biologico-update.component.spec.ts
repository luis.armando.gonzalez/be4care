/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import * as config from '@/shared/config/config';
import BiologicoUpdateComponent from '@/entities/biologico/biologico/biologico-update.vue';
import BiologicoClass from '@/entities/biologico/biologico/biologico-update.component';
import BiologicoService from '@/entities/biologico/biologico/biologico.service';

import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Biologico Management Update Component', () => {
    let wrapper: Wrapper<BiologicoClass>;
    let comp: BiologicoClass;
    let biologicoServiceStub: SinonStubbedInstance<BiologicoService>;

    beforeEach(() => {
      biologicoServiceStub = sinon.createStubInstance<BiologicoService>(BiologicoService);

      wrapper = shallowMount<BiologicoClass>(BiologicoUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          biologicoService: () => biologicoServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.biologico = entity;
        biologicoServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(biologicoServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.biologico = entity;
        biologicoServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(biologicoServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundBiologico = { id: 123 };
        biologicoServiceStub.find.resolves(foundBiologico);
        biologicoServiceStub.retrieve.resolves([foundBiologico]);

        // WHEN
        comp.beforeRouteEnter({ params: { biologicoId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.biologico).toBe(foundBiologico);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
