/* tslint:disable max-line-length */
import axios from 'axios';
import sinon from 'sinon';
import dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from '@/shared/date/filters';
import Fisiometria2Service from '@/entities/diadema/fisiometria-2/fisiometria-2.service';
import { Fisiometria2 } from '@/shared/model/diadema/fisiometria-2.model';

const error = {
  response: {
    status: null,
    data: {
      type: null,
    },
  },
};

const axiosStub = {
  get: sinon.stub(axios, 'get'),
  post: sinon.stub(axios, 'post'),
  put: sinon.stub(axios, 'put'),
  patch: sinon.stub(axios, 'patch'),
  delete: sinon.stub(axios, 'delete'),
};

describe('Service Tests', () => {
  describe('Fisiometria2 Service', () => {
    let service: Fisiometria2Service;
    let elemDefault;
    let currentDate: Date;

    beforeEach(() => {
      service = new Fisiometria2Service();
      currentDate = new Date();
      elemDefault = new Fisiometria2(123, currentDate, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign(
          {
            timeInstant: dayjs(currentDate).format(DATE_TIME_FORMAT),
          },
          elemDefault
        );
        axiosStub.get.resolves({ data: returnedFromService });

        return service.find(123).then(res => {
          expect(res).toMatchObject(elemDefault);
        });
      });

      it('should not find an element', async () => {
        axiosStub.get.rejects(error);
        return service
          .find(123)
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should create a Fisiometria2', async () => {
        const returnedFromService = Object.assign(
          {
            id: 123,
            timeInstant: dayjs(currentDate).format(DATE_TIME_FORMAT),
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            timeInstant: currentDate,
          },
          returnedFromService
        );

        axiosStub.post.resolves({ data: returnedFromService });
        return service.create({}).then(res => {
          expect(res).toMatchObject(expected);
        });
      });

      it('should not create a Fisiometria2', async () => {
        axiosStub.post.rejects(error);

        return service
          .create({})
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should update a Fisiometria2', async () => {
        const returnedFromService = Object.assign(
          {
            timeInstant: dayjs(currentDate).format(DATE_TIME_FORMAT),
            meditacion: 1,
            attention: 1,
            delta: 1,
            theta: 1,
            lowAlpha: 1,
            highAlpha: 1,
            lowBeta: 1,
            highBeta: 1,
            lowGamma: 1,
            midGamma: 1,
            pulso: 1,
            respiracion: 1,
            acelerometro: 1,
            estadoFuncional: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            timeInstant: currentDate,
          },
          returnedFromService
        );
        axiosStub.put.resolves({ data: returnedFromService });

        return service.update(expected).then(res => {
          expect(res).toMatchObject(expected);
        });
      });

      it('should not update a Fisiometria2', async () => {
        axiosStub.put.rejects(error);

        return service
          .update({})
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should partial update a Fisiometria2', async () => {
        const patchObject = Object.assign(
          {
            meditacion: 1,
            lowAlpha: 1,
            highAlpha: 1,
            highBeta: 1,
            lowGamma: 1,
            respiracion: 1,
          },
          new Fisiometria2()
        );
        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            timeInstant: currentDate,
          },
          returnedFromService
        );
        axiosStub.patch.resolves({ data: returnedFromService });

        return service.partialUpdate(patchObject).then(res => {
          expect(res).toMatchObject(expected);
        });
      });

      it('should not partial update a Fisiometria2', async () => {
        axiosStub.patch.rejects(error);

        return service
          .partialUpdate({})
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should return a list of Fisiometria2', async () => {
        const returnedFromService = Object.assign(
          {
            timeInstant: dayjs(currentDate).format(DATE_TIME_FORMAT),
            meditacion: 1,
            attention: 1,
            delta: 1,
            theta: 1,
            lowAlpha: 1,
            highAlpha: 1,
            lowBeta: 1,
            highBeta: 1,
            lowGamma: 1,
            midGamma: 1,
            pulso: 1,
            respiracion: 1,
            acelerometro: 1,
            estadoFuncional: 1,
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            timeInstant: currentDate,
          },
          returnedFromService
        );
        axiosStub.get.resolves([returnedFromService]);
        return service.retrieve({ sort: {}, page: 0, size: 10 }).then(res => {
          expect(res).toContainEqual(expected);
        });
      });

      it('should not return a list of Fisiometria2', async () => {
        axiosStub.get.rejects(error);

        return service
          .retrieve()
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should delete a Fisiometria2', async () => {
        axiosStub.delete.resolves({ ok: true });
        return service.delete(123).then(res => {
          expect(res.ok).toBeTruthy();
        });
      });

      it('should not delete a Fisiometria2', async () => {
        axiosStub.delete.rejects(error);

        return service
          .delete(123)
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });
    });
  });
});
