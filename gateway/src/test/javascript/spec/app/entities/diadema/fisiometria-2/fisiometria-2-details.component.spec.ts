/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import Fisiometria2DetailComponent from '@/entities/diadema/fisiometria-2/fisiometria-2-details.vue';
import Fisiometria2Class from '@/entities/diadema/fisiometria-2/fisiometria-2-details.component';
import Fisiometria2Service from '@/entities/diadema/fisiometria-2/fisiometria-2.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Fisiometria2 Management Detail Component', () => {
    let wrapper: Wrapper<Fisiometria2Class>;
    let comp: Fisiometria2Class;
    let fisiometria2ServiceStub: SinonStubbedInstance<Fisiometria2Service>;

    beforeEach(() => {
      fisiometria2ServiceStub = sinon.createStubInstance<Fisiometria2Service>(Fisiometria2Service);

      wrapper = shallowMount<Fisiometria2Class>(Fisiometria2DetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { fisiometria2Service: () => fisiometria2ServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundFisiometria2 = { id: 123 };
        fisiometria2ServiceStub.find.resolves(foundFisiometria2);

        // WHEN
        comp.retrieveFisiometria2(123);
        await comp.$nextTick();

        // THEN
        expect(comp.fisiometria2).toBe(foundFisiometria2);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundFisiometria2 = { id: 123 };
        fisiometria2ServiceStub.find.resolves(foundFisiometria2);

        // WHEN
        comp.beforeRouteEnter({ params: { fisiometria2Id: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.fisiometria2).toBe(foundFisiometria2);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
