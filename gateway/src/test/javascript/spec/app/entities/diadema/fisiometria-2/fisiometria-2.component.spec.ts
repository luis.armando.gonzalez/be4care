/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import Fisiometria2Component from '@/entities/diadema/fisiometria-2/fisiometria-2.vue';
import Fisiometria2Class from '@/entities/diadema/fisiometria-2/fisiometria-2.component';
import Fisiometria2Service from '@/entities/diadema/fisiometria-2/fisiometria-2.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Fisiometria2 Management Component', () => {
    let wrapper: Wrapper<Fisiometria2Class>;
    let comp: Fisiometria2Class;
    let fisiometria2ServiceStub: SinonStubbedInstance<Fisiometria2Service>;

    beforeEach(() => {
      fisiometria2ServiceStub = sinon.createStubInstance<Fisiometria2Service>(Fisiometria2Service);
      fisiometria2ServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<Fisiometria2Class>(Fisiometria2Component, {
        store,
        i18n,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          fisiometria2Service: () => fisiometria2ServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      fisiometria2ServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllFisiometria2s();
      await comp.$nextTick();

      // THEN
      expect(fisiometria2ServiceStub.retrieve.called).toBeTruthy();
      expect(comp.fisiometria2s[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      fisiometria2ServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(fisiometria2ServiceStub.retrieve.called).toBeTruthy();
      expect(comp.fisiometria2s[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      fisiometria2ServiceStub.retrieve.reset();
      fisiometria2ServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(fisiometria2ServiceStub.retrieve.callCount).toEqual(2);
      expect(comp.page).toEqual(1);
      expect(comp.fisiometria2s[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      fisiometria2ServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeFisiometria2();
      await comp.$nextTick();

      // THEN
      expect(fisiometria2ServiceStub.delete.called).toBeTruthy();
      expect(fisiometria2ServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});
