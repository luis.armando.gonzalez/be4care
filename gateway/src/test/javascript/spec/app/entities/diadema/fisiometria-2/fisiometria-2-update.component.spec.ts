/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import * as config from '@/shared/config/config';
import Fisiometria2UpdateComponent from '@/entities/diadema/fisiometria-2/fisiometria-2-update.vue';
import Fisiometria2Class from '@/entities/diadema/fisiometria-2/fisiometria-2-update.component';
import Fisiometria2Service from '@/entities/diadema/fisiometria-2/fisiometria-2.service';

import UserOAuth2Service from '@/entities/user/user.oauth2.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Fisiometria2 Management Update Component', () => {
    let wrapper: Wrapper<Fisiometria2Class>;
    let comp: Fisiometria2Class;
    let fisiometria2ServiceStub: SinonStubbedInstance<Fisiometria2Service>;

    beforeEach(() => {
      fisiometria2ServiceStub = sinon.createStubInstance<Fisiometria2Service>(Fisiometria2Service);

      wrapper = shallowMount<Fisiometria2Class>(Fisiometria2UpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          fisiometria2Service: () => fisiometria2ServiceStub,
          alertService: () => new AlertService(),

          userOAuth2Service: () => new UserOAuth2Service(),
        },
      });
      comp = wrapper.vm;
    });

    describe('load', () => {
      it('Should convert date from string', () => {
        // GIVEN
        const date = new Date('2019-10-15T11:42:02Z');

        // WHEN
        const convertedDate = comp.convertDateTimeFromServer(date);

        // THEN
        expect(convertedDate).toEqual(dayjs(date).format(DATE_TIME_LONG_FORMAT));
      });

      it('Should not convert date if date is not present', () => {
        expect(comp.convertDateTimeFromServer(null)).toBeNull();
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.fisiometria2 = entity;
        fisiometria2ServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(fisiometria2ServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.fisiometria2 = entity;
        fisiometria2ServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(fisiometria2ServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundFisiometria2 = { id: 123 };
        fisiometria2ServiceStub.find.resolves(foundFisiometria2);
        fisiometria2ServiceStub.retrieve.resolves([foundFisiometria2]);

        // WHEN
        comp.beforeRouteEnter({ params: { fisiometria2Id: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.fisiometria2).toBe(foundFisiometria2);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
