/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import PreguntaDetailComponent from '@/entities/pacientes/pregunta/pregunta-details.vue';
import PreguntaClass from '@/entities/pacientes/pregunta/pregunta-details.component';
import PreguntaService from '@/entities/pacientes/pregunta/pregunta.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Pregunta Management Detail Component', () => {
    let wrapper: Wrapper<PreguntaClass>;
    let comp: PreguntaClass;
    let preguntaServiceStub: SinonStubbedInstance<PreguntaService>;

    beforeEach(() => {
      preguntaServiceStub = sinon.createStubInstance<PreguntaService>(PreguntaService);

      wrapper = shallowMount<PreguntaClass>(PreguntaDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { preguntaService: () => preguntaServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundPregunta = { id: 123 };
        preguntaServiceStub.find.resolves(foundPregunta);

        // WHEN
        comp.retrievePregunta(123);
        await comp.$nextTick();

        // THEN
        expect(comp.pregunta).toBe(foundPregunta);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundPregunta = { id: 123 };
        preguntaServiceStub.find.resolves(foundPregunta);

        // WHEN
        comp.beforeRouteEnter({ params: { preguntaId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.pregunta).toBe(foundPregunta);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
