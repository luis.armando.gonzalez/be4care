/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import * as config from '@/shared/config/config';
import FarmaceuticaUpdateComponent from '@/entities/pacientes/farmaceutica/farmaceutica-update.vue';
import FarmaceuticaClass from '@/entities/pacientes/farmaceutica/farmaceutica-update.component';
import FarmaceuticaService from '@/entities/pacientes/farmaceutica/farmaceutica.service';

import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Farmaceutica Management Update Component', () => {
    let wrapper: Wrapper<FarmaceuticaClass>;
    let comp: FarmaceuticaClass;
    let farmaceuticaServiceStub: SinonStubbedInstance<FarmaceuticaService>;

    beforeEach(() => {
      farmaceuticaServiceStub = sinon.createStubInstance<FarmaceuticaService>(FarmaceuticaService);

      wrapper = shallowMount<FarmaceuticaClass>(FarmaceuticaUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          farmaceuticaService: () => farmaceuticaServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.farmaceutica = entity;
        farmaceuticaServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(farmaceuticaServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.farmaceutica = entity;
        farmaceuticaServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(farmaceuticaServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundFarmaceutica = { id: 123 };
        farmaceuticaServiceStub.find.resolves(foundFarmaceutica);
        farmaceuticaServiceStub.retrieve.resolves([foundFarmaceutica]);

        // WHEN
        comp.beforeRouteEnter({ params: { farmaceuticaId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.farmaceutica).toBe(foundFarmaceutica);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
