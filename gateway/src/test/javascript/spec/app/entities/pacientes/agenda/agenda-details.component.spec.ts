/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import AgendaDetailComponent from '@/entities/pacientes/agenda/agenda-details.vue';
import AgendaClass from '@/entities/pacientes/agenda/agenda-details.component';
import AgendaService from '@/entities/pacientes/agenda/agenda.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Agenda Management Detail Component', () => {
    let wrapper: Wrapper<AgendaClass>;
    let comp: AgendaClass;
    let agendaServiceStub: SinonStubbedInstance<AgendaService>;

    beforeEach(() => {
      agendaServiceStub = sinon.createStubInstance<AgendaService>(AgendaService);

      wrapper = shallowMount<AgendaClass>(AgendaDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { agendaService: () => agendaServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundAgenda = { id: 123 };
        agendaServiceStub.find.resolves(foundAgenda);

        // WHEN
        comp.retrieveAgenda(123);
        await comp.$nextTick();

        // THEN
        expect(comp.agenda).toBe(foundAgenda);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundAgenda = { id: 123 };
        agendaServiceStub.find.resolves(foundAgenda);

        // WHEN
        comp.beforeRouteEnter({ params: { agendaId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.agenda).toBe(foundAgenda);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
