/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import CuestionarioEstadoDetailComponent from '@/entities/pacientes/cuestionario-estado/cuestionario-estado-details.vue';
import CuestionarioEstadoClass from '@/entities/pacientes/cuestionario-estado/cuestionario-estado-details.component';
import CuestionarioEstadoService from '@/entities/pacientes/cuestionario-estado/cuestionario-estado.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('CuestionarioEstado Management Detail Component', () => {
    let wrapper: Wrapper<CuestionarioEstadoClass>;
    let comp: CuestionarioEstadoClass;
    let cuestionarioEstadoServiceStub: SinonStubbedInstance<CuestionarioEstadoService>;

    beforeEach(() => {
      cuestionarioEstadoServiceStub = sinon.createStubInstance<CuestionarioEstadoService>(CuestionarioEstadoService);

      wrapper = shallowMount<CuestionarioEstadoClass>(CuestionarioEstadoDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { cuestionarioEstadoService: () => cuestionarioEstadoServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundCuestionarioEstado = { id: 123 };
        cuestionarioEstadoServiceStub.find.resolves(foundCuestionarioEstado);

        // WHEN
        comp.retrieveCuestionarioEstado(123);
        await comp.$nextTick();

        // THEN
        expect(comp.cuestionarioEstado).toBe(foundCuestionarioEstado);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundCuestionarioEstado = { id: 123 };
        cuestionarioEstadoServiceStub.find.resolves(foundCuestionarioEstado);

        // WHEN
        comp.beforeRouteEnter({ params: { cuestionarioEstadoId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.cuestionarioEstado).toBe(foundCuestionarioEstado);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
