/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import MedicamentoComponent from '@/entities/pacientes/medicamento/medicamento.vue';
import MedicamentoClass from '@/entities/pacientes/medicamento/medicamento.component';
import MedicamentoService from '@/entities/pacientes/medicamento/medicamento.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Medicamento Management Component', () => {
    let wrapper: Wrapper<MedicamentoClass>;
    let comp: MedicamentoClass;
    let medicamentoServiceStub: SinonStubbedInstance<MedicamentoService>;

    beforeEach(() => {
      medicamentoServiceStub = sinon.createStubInstance<MedicamentoService>(MedicamentoService);
      medicamentoServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<MedicamentoClass>(MedicamentoComponent, {
        store,
        i18n,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          medicamentoService: () => medicamentoServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      medicamentoServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllMedicamentos();
      await comp.$nextTick();

      // THEN
      expect(medicamentoServiceStub.retrieve.called).toBeTruthy();
      expect(comp.medicamentos[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      medicamentoServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(medicamentoServiceStub.retrieve.called).toBeTruthy();
      expect(comp.medicamentos[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      medicamentoServiceStub.retrieve.reset();
      medicamentoServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(medicamentoServiceStub.retrieve.callCount).toEqual(2);
      expect(comp.page).toEqual(1);
      expect(comp.medicamentos[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      medicamentoServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeMedicamento();
      await comp.$nextTick();

      // THEN
      expect(medicamentoServiceStub.delete.called).toBeTruthy();
      expect(medicamentoServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});
