/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import FarmaceuticaComponent from '@/entities/pacientes/farmaceutica/farmaceutica.vue';
import FarmaceuticaClass from '@/entities/pacientes/farmaceutica/farmaceutica.component';
import FarmaceuticaService from '@/entities/pacientes/farmaceutica/farmaceutica.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Farmaceutica Management Component', () => {
    let wrapper: Wrapper<FarmaceuticaClass>;
    let comp: FarmaceuticaClass;
    let farmaceuticaServiceStub: SinonStubbedInstance<FarmaceuticaService>;

    beforeEach(() => {
      farmaceuticaServiceStub = sinon.createStubInstance<FarmaceuticaService>(FarmaceuticaService);
      farmaceuticaServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<FarmaceuticaClass>(FarmaceuticaComponent, {
        store,
        i18n,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          farmaceuticaService: () => farmaceuticaServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      farmaceuticaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllFarmaceuticas();
      await comp.$nextTick();

      // THEN
      expect(farmaceuticaServiceStub.retrieve.called).toBeTruthy();
      expect(comp.farmaceuticas[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      farmaceuticaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(farmaceuticaServiceStub.retrieve.called).toBeTruthy();
      expect(comp.farmaceuticas[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      farmaceuticaServiceStub.retrieve.reset();
      farmaceuticaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(farmaceuticaServiceStub.retrieve.callCount).toEqual(2);
      expect(comp.page).toEqual(1);
      expect(comp.farmaceuticas[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      farmaceuticaServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeFarmaceutica();
      await comp.$nextTick();

      // THEN
      expect(farmaceuticaServiceStub.delete.called).toBeTruthy();
      expect(farmaceuticaServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});
