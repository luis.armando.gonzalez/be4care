/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import MedicamentoDetailComponent from '@/entities/pacientes/medicamento/medicamento-details.vue';
import MedicamentoClass from '@/entities/pacientes/medicamento/medicamento-details.component';
import MedicamentoService from '@/entities/pacientes/medicamento/medicamento.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Medicamento Management Detail Component', () => {
    let wrapper: Wrapper<MedicamentoClass>;
    let comp: MedicamentoClass;
    let medicamentoServiceStub: SinonStubbedInstance<MedicamentoService>;

    beforeEach(() => {
      medicamentoServiceStub = sinon.createStubInstance<MedicamentoService>(MedicamentoService);

      wrapper = shallowMount<MedicamentoClass>(MedicamentoDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { medicamentoService: () => medicamentoServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundMedicamento = { id: 123 };
        medicamentoServiceStub.find.resolves(foundMedicamento);

        // WHEN
        comp.retrieveMedicamento(123);
        await comp.$nextTick();

        // THEN
        expect(comp.medicamento).toBe(foundMedicamento);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundMedicamento = { id: 123 };
        medicamentoServiceStub.find.resolves(foundMedicamento);

        // WHEN
        comp.beforeRouteEnter({ params: { medicamentoId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.medicamento).toBe(foundMedicamento);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
