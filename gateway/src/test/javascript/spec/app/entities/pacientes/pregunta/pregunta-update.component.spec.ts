/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import * as config from '@/shared/config/config';
import PreguntaUpdateComponent from '@/entities/pacientes/pregunta/pregunta-update.vue';
import PreguntaClass from '@/entities/pacientes/pregunta/pregunta-update.component';
import PreguntaService from '@/entities/pacientes/pregunta/pregunta.service';

import CondicionService from '@/entities/pacientes/condicion/condicion.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Pregunta Management Update Component', () => {
    let wrapper: Wrapper<PreguntaClass>;
    let comp: PreguntaClass;
    let preguntaServiceStub: SinonStubbedInstance<PreguntaService>;

    beforeEach(() => {
      preguntaServiceStub = sinon.createStubInstance<PreguntaService>(PreguntaService);

      wrapper = shallowMount<PreguntaClass>(PreguntaUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          preguntaService: () => preguntaServiceStub,
          alertService: () => new AlertService(),

          condicionService: () => new CondicionService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.pregunta = entity;
        preguntaServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(preguntaServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.pregunta = entity;
        preguntaServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(preguntaServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundPregunta = { id: 123 };
        preguntaServiceStub.find.resolves(foundPregunta);
        preguntaServiceStub.retrieve.resolves([foundPregunta]);

        // WHEN
        comp.beforeRouteEnter({ params: { preguntaId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.pregunta).toBe(foundPregunta);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
