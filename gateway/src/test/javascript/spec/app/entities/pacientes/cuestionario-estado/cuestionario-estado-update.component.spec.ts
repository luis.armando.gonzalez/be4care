/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import * as config from '@/shared/config/config';
import CuestionarioEstadoUpdateComponent from '@/entities/pacientes/cuestionario-estado/cuestionario-estado-update.vue';
import CuestionarioEstadoClass from '@/entities/pacientes/cuestionario-estado/cuestionario-estado-update.component';
import CuestionarioEstadoService from '@/entities/pacientes/cuestionario-estado/cuestionario-estado.service';

import PreguntaService from '@/entities/pacientes/pregunta/pregunta.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('CuestionarioEstado Management Update Component', () => {
    let wrapper: Wrapper<CuestionarioEstadoClass>;
    let comp: CuestionarioEstadoClass;
    let cuestionarioEstadoServiceStub: SinonStubbedInstance<CuestionarioEstadoService>;

    beforeEach(() => {
      cuestionarioEstadoServiceStub = sinon.createStubInstance<CuestionarioEstadoService>(CuestionarioEstadoService);

      wrapper = shallowMount<CuestionarioEstadoClass>(CuestionarioEstadoUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          cuestionarioEstadoService: () => cuestionarioEstadoServiceStub,
          alertService: () => new AlertService(),

          preguntaService: () => new PreguntaService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.cuestionarioEstado = entity;
        cuestionarioEstadoServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(cuestionarioEstadoServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.cuestionarioEstado = entity;
        cuestionarioEstadoServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(cuestionarioEstadoServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundCuestionarioEstado = { id: 123 };
        cuestionarioEstadoServiceStub.find.resolves(foundCuestionarioEstado);
        cuestionarioEstadoServiceStub.retrieve.resolves([foundCuestionarioEstado]);

        // WHEN
        comp.beforeRouteEnter({ params: { cuestionarioEstadoId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.cuestionarioEstado).toBe(foundCuestionarioEstado);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
