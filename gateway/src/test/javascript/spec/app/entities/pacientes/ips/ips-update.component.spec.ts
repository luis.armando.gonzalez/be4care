/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import * as config from '@/shared/config/config';
import IPSUpdateComponent from '@/entities/pacientes/ips/ips-update.vue';
import IPSClass from '@/entities/pacientes/ips/ips-update.component';
import IPSService from '@/entities/pacientes/ips/ips.service';

import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('IPS Management Update Component', () => {
    let wrapper: Wrapper<IPSClass>;
    let comp: IPSClass;
    let iPSServiceStub: SinonStubbedInstance<IPSService>;

    beforeEach(() => {
      iPSServiceStub = sinon.createStubInstance<IPSService>(IPSService);

      wrapper = shallowMount<IPSClass>(IPSUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          iPSService: () => iPSServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.iPS = entity;
        iPSServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(iPSServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.iPS = entity;
        iPSServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(iPSServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundIPS = { id: 123 };
        iPSServiceStub.find.resolves(foundIPS);
        iPSServiceStub.retrieve.resolves([foundIPS]);

        // WHEN
        comp.beforeRouteEnter({ params: { iPSId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.iPS).toBe(foundIPS);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
