/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import FarmaceuticaDetailComponent from '@/entities/pacientes/farmaceutica/farmaceutica-details.vue';
import FarmaceuticaClass from '@/entities/pacientes/farmaceutica/farmaceutica-details.component';
import FarmaceuticaService from '@/entities/pacientes/farmaceutica/farmaceutica.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Farmaceutica Management Detail Component', () => {
    let wrapper: Wrapper<FarmaceuticaClass>;
    let comp: FarmaceuticaClass;
    let farmaceuticaServiceStub: SinonStubbedInstance<FarmaceuticaService>;

    beforeEach(() => {
      farmaceuticaServiceStub = sinon.createStubInstance<FarmaceuticaService>(FarmaceuticaService);

      wrapper = shallowMount<FarmaceuticaClass>(FarmaceuticaDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { farmaceuticaService: () => farmaceuticaServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundFarmaceutica = { id: 123 };
        farmaceuticaServiceStub.find.resolves(foundFarmaceutica);

        // WHEN
        comp.retrieveFarmaceutica(123);
        await comp.$nextTick();

        // THEN
        expect(comp.farmaceutica).toBe(foundFarmaceutica);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundFarmaceutica = { id: 123 };
        farmaceuticaServiceStub.find.resolves(foundFarmaceutica);

        // WHEN
        comp.beforeRouteEnter({ params: { farmaceuticaId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.farmaceutica).toBe(foundFarmaceutica);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
