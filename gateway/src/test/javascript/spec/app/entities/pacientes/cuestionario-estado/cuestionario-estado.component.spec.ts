/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import CuestionarioEstadoComponent from '@/entities/pacientes/cuestionario-estado/cuestionario-estado.vue';
import CuestionarioEstadoClass from '@/entities/pacientes/cuestionario-estado/cuestionario-estado.component';
import CuestionarioEstadoService from '@/entities/pacientes/cuestionario-estado/cuestionario-estado.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('CuestionarioEstado Management Component', () => {
    let wrapper: Wrapper<CuestionarioEstadoClass>;
    let comp: CuestionarioEstadoClass;
    let cuestionarioEstadoServiceStub: SinonStubbedInstance<CuestionarioEstadoService>;

    beforeEach(() => {
      cuestionarioEstadoServiceStub = sinon.createStubInstance<CuestionarioEstadoService>(CuestionarioEstadoService);
      cuestionarioEstadoServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<CuestionarioEstadoClass>(CuestionarioEstadoComponent, {
        store,
        i18n,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          cuestionarioEstadoService: () => cuestionarioEstadoServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      cuestionarioEstadoServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllCuestionarioEstados();
      await comp.$nextTick();

      // THEN
      expect(cuestionarioEstadoServiceStub.retrieve.called).toBeTruthy();
      expect(comp.cuestionarioEstados[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      cuestionarioEstadoServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(cuestionarioEstadoServiceStub.retrieve.called).toBeTruthy();
      expect(comp.cuestionarioEstados[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      cuestionarioEstadoServiceStub.retrieve.reset();
      cuestionarioEstadoServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(cuestionarioEstadoServiceStub.retrieve.callCount).toEqual(2);
      expect(comp.page).toEqual(1);
      expect(comp.cuestionarioEstados[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      cuestionarioEstadoServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeCuestionarioEstado();
      await comp.$nextTick();

      // THEN
      expect(cuestionarioEstadoServiceStub.delete.called).toBeTruthy();
      expect(cuestionarioEstadoServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});
