/* tslint:disable max-line-length */
import axios from 'axios';
import sinon from 'sinon';
import dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from '@/shared/date/filters';
import AlarmaService from '@/entities/pacientes/alarma/alarma.service';
import { Alarma } from '@/shared/model/pacientes/alarma.model';

const error = {
  response: {
    status: null,
    data: {
      type: null,
    },
  },
};

const axiosStub = {
  get: sinon.stub(axios, 'get'),
  post: sinon.stub(axios, 'post'),
  put: sinon.stub(axios, 'put'),
  patch: sinon.stub(axios, 'patch'),
  delete: sinon.stub(axios, 'delete'),
};

describe('Service Tests', () => {
  describe('Alarma Service', () => {
    let service: AlarmaService;
    let elemDefault;
    let currentDate: Date;

    beforeEach(() => {
      service = new AlarmaService();
      currentDate = new Date();
      elemDefault = new Alarma(123, currentDate, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', false, 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign(
          {
            timeInstant: dayjs(currentDate).format(DATE_TIME_FORMAT),
          },
          elemDefault
        );
        axiosStub.get.resolves({ data: returnedFromService });

        return service.find(123).then(res => {
          expect(res).toMatchObject(elemDefault);
        });
      });

      it('should not find an element', async () => {
        axiosStub.get.rejects(error);
        return service
          .find(123)
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should create a Alarma', async () => {
        const returnedFromService = Object.assign(
          {
            id: 123,
            timeInstant: dayjs(currentDate).format(DATE_TIME_FORMAT),
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            timeInstant: currentDate,
          },
          returnedFromService
        );

        axiosStub.post.resolves({ data: returnedFromService });
        return service.create({}).then(res => {
          expect(res).toMatchObject(expected);
        });
      });

      it('should not create a Alarma', async () => {
        axiosStub.post.rejects(error);

        return service
          .create({})
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should update a Alarma', async () => {
        const returnedFromService = Object.assign(
          {
            timeInstant: dayjs(currentDate).format(DATE_TIME_FORMAT),
            descripcion: 'BBBBBB',
            procedimiento: 'BBBBBB',
            titulo: 'BBBBBB',
            verificar: true,
            observaciones: 'BBBBBB',
            prioridad: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            timeInstant: currentDate,
          },
          returnedFromService
        );
        axiosStub.put.resolves({ data: returnedFromService });

        return service.update(expected).then(res => {
          expect(res).toMatchObject(expected);
        });
      });

      it('should not update a Alarma', async () => {
        axiosStub.put.rejects(error);

        return service
          .update({})
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should partial update a Alarma', async () => {
        const patchObject = Object.assign(
          {
            timeInstant: dayjs(currentDate).format(DATE_TIME_FORMAT),
            titulo: 'BBBBBB',
            prioridad: 'BBBBBB',
          },
          new Alarma()
        );
        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            timeInstant: currentDate,
          },
          returnedFromService
        );
        axiosStub.patch.resolves({ data: returnedFromService });

        return service.partialUpdate(patchObject).then(res => {
          expect(res).toMatchObject(expected);
        });
      });

      it('should not partial update a Alarma', async () => {
        axiosStub.patch.rejects(error);

        return service
          .partialUpdate({})
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should return a list of Alarma', async () => {
        const returnedFromService = Object.assign(
          {
            timeInstant: dayjs(currentDate).format(DATE_TIME_FORMAT),
            descripcion: 'BBBBBB',
            procedimiento: 'BBBBBB',
            titulo: 'BBBBBB',
            verificar: true,
            observaciones: 'BBBBBB',
            prioridad: 'BBBBBB',
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            timeInstant: currentDate,
          },
          returnedFromService
        );
        axiosStub.get.resolves([returnedFromService]);
        return service.retrieve({ sort: {}, page: 0, size: 10 }).then(res => {
          expect(res).toContainEqual(expected);
        });
      });

      it('should not return a list of Alarma', async () => {
        axiosStub.get.rejects(error);

        return service
          .retrieve()
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should delete a Alarma', async () => {
        axiosStub.delete.resolves({ ok: true });
        return service.delete(123).then(res => {
          expect(res.ok).toBeTruthy();
        });
      });

      it('should not delete a Alarma', async () => {
        axiosStub.delete.rejects(error);

        return service
          .delete(123)
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });
    });
  });
});
