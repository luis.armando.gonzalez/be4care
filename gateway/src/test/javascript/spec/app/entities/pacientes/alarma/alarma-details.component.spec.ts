/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import AlarmaDetailComponent from '@/entities/pacientes/alarma/alarma-details.vue';
import AlarmaClass from '@/entities/pacientes/alarma/alarma-details.component';
import AlarmaService from '@/entities/pacientes/alarma/alarma.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Alarma Management Detail Component', () => {
    let wrapper: Wrapper<AlarmaClass>;
    let comp: AlarmaClass;
    let alarmaServiceStub: SinonStubbedInstance<AlarmaService>;

    beforeEach(() => {
      alarmaServiceStub = sinon.createStubInstance<AlarmaService>(AlarmaService);

      wrapper = shallowMount<AlarmaClass>(AlarmaDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { alarmaService: () => alarmaServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundAlarma = { id: 123 };
        alarmaServiceStub.find.resolves(foundAlarma);

        // WHEN
        comp.retrieveAlarma(123);
        await comp.$nextTick();

        // THEN
        expect(comp.alarma).toBe(foundAlarma);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundAlarma = { id: 123 };
        alarmaServiceStub.find.resolves(foundAlarma);

        // WHEN
        comp.beforeRouteEnter({ params: { alarmaId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.alarma).toBe(foundAlarma);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
