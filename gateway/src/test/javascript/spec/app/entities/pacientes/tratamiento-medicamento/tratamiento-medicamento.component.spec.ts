/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import TratamientoMedicamentoComponent from '@/entities/pacientes/tratamiento-medicamento/tratamiento-medicamento.vue';
import TratamientoMedicamentoClass from '@/entities/pacientes/tratamiento-medicamento/tratamiento-medicamento.component';
import TratamientoMedicamentoService from '@/entities/pacientes/tratamiento-medicamento/tratamiento-medicamento.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('TratamientoMedicamento Management Component', () => {
    let wrapper: Wrapper<TratamientoMedicamentoClass>;
    let comp: TratamientoMedicamentoClass;
    let tratamientoMedicamentoServiceStub: SinonStubbedInstance<TratamientoMedicamentoService>;

    beforeEach(() => {
      tratamientoMedicamentoServiceStub = sinon.createStubInstance<TratamientoMedicamentoService>(TratamientoMedicamentoService);
      tratamientoMedicamentoServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<TratamientoMedicamentoClass>(TratamientoMedicamentoComponent, {
        store,
        i18n,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          tratamientoMedicamentoService: () => tratamientoMedicamentoServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      tratamientoMedicamentoServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllTratamientoMedicamentos();
      await comp.$nextTick();

      // THEN
      expect(tratamientoMedicamentoServiceStub.retrieve.called).toBeTruthy();
      expect(comp.tratamientoMedicamentos[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      tratamientoMedicamentoServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(tratamientoMedicamentoServiceStub.retrieve.called).toBeTruthy();
      expect(comp.tratamientoMedicamentos[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      tratamientoMedicamentoServiceStub.retrieve.reset();
      tratamientoMedicamentoServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(tratamientoMedicamentoServiceStub.retrieve.callCount).toEqual(2);
      expect(comp.page).toEqual(1);
      expect(comp.tratamientoMedicamentos[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      tratamientoMedicamentoServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeTratamientoMedicamento();
      await comp.$nextTick();

      // THEN
      expect(tratamientoMedicamentoServiceStub.delete.called).toBeTruthy();
      expect(tratamientoMedicamentoServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});
