/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import IPSDetailComponent from '@/entities/pacientes/ips/ips-details.vue';
import IPSClass from '@/entities/pacientes/ips/ips-details.component';
import IPSService from '@/entities/pacientes/ips/ips.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('IPS Management Detail Component', () => {
    let wrapper: Wrapper<IPSClass>;
    let comp: IPSClass;
    let iPSServiceStub: SinonStubbedInstance<IPSService>;

    beforeEach(() => {
      iPSServiceStub = sinon.createStubInstance<IPSService>(IPSService);

      wrapper = shallowMount<IPSClass>(IPSDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { iPSService: () => iPSServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundIPS = { id: 123 };
        iPSServiceStub.find.resolves(foundIPS);

        // WHEN
        comp.retrieveIPS(123);
        await comp.$nextTick();

        // THEN
        expect(comp.iPS).toBe(foundIPS);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundIPS = { id: 123 };
        iPSServiceStub.find.resolves(foundIPS);

        // WHEN
        comp.beforeRouteEnter({ params: { iPSId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.iPS).toBe(foundIPS);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
