/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import * as config from '@/shared/config/config';
import CondicionUpdateComponent from '@/entities/pacientes/condicion/condicion-update.vue';
import CondicionClass from '@/entities/pacientes/condicion/condicion-update.component';
import CondicionService from '@/entities/pacientes/condicion/condicion.service';

import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Condicion Management Update Component', () => {
    let wrapper: Wrapper<CondicionClass>;
    let comp: CondicionClass;
    let condicionServiceStub: SinonStubbedInstance<CondicionService>;

    beforeEach(() => {
      condicionServiceStub = sinon.createStubInstance<CondicionService>(CondicionService);

      wrapper = shallowMount<CondicionClass>(CondicionUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          condicionService: () => condicionServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.condicion = entity;
        condicionServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(condicionServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.condicion = entity;
        condicionServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(condicionServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundCondicion = { id: 123 };
        condicionServiceStub.find.resolves(foundCondicion);
        condicionServiceStub.retrieve.resolves([foundCondicion]);

        // WHEN
        comp.beforeRouteEnter({ params: { condicionId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.condicion).toBe(foundCondicion);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
