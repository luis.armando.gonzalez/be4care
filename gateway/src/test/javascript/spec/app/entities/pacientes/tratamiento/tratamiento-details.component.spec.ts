/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import TratamientoDetailComponent from '@/entities/pacientes/tratamiento/tratamiento-details.vue';
import TratamientoClass from '@/entities/pacientes/tratamiento/tratamiento-details.component';
import TratamientoService from '@/entities/pacientes/tratamiento/tratamiento.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Tratamiento Management Detail Component', () => {
    let wrapper: Wrapper<TratamientoClass>;
    let comp: TratamientoClass;
    let tratamientoServiceStub: SinonStubbedInstance<TratamientoService>;

    beforeEach(() => {
      tratamientoServiceStub = sinon.createStubInstance<TratamientoService>(TratamientoService);

      wrapper = shallowMount<TratamientoClass>(TratamientoDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { tratamientoService: () => tratamientoServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundTratamiento = { id: 123 };
        tratamientoServiceStub.find.resolves(foundTratamiento);

        // WHEN
        comp.retrieveTratamiento(123);
        await comp.$nextTick();

        // THEN
        expect(comp.tratamiento).toBe(foundTratamiento);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundTratamiento = { id: 123 };
        tratamientoServiceStub.find.resolves(foundTratamiento);

        // WHEN
        comp.beforeRouteEnter({ params: { tratamientoId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.tratamiento).toBe(foundTratamiento);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
