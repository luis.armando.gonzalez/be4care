/* tslint:disable max-line-length */
import axios from 'axios';
import sinon from 'sinon';

import PacienteService from '@/entities/pacientes/paciente/paciente.service';
import { Paciente } from '@/shared/model/pacientes/paciente.model';
import { Identificaciont } from '@/shared/model/enumerations/identificaciont.model';
import { Sexop } from '@/shared/model/enumerations/sexop.model';
import { Grupoetnicop } from '@/shared/model/enumerations/grupoetnicop.model';
import { Estratop } from '@/shared/model/enumerations/estratop.model';

const error = {
  response: {
    status: null,
    data: {
      type: null,
    },
  },
};

const axiosStub = {
  get: sinon.stub(axios, 'get'),
  post: sinon.stub(axios, 'post'),
  put: sinon.stub(axios, 'put'),
  patch: sinon.stub(axios, 'patch'),
  delete: sinon.stub(axios, 'delete'),
};

describe('Service Tests', () => {
  describe('Paciente Service', () => {
    let service: PacienteService;
    let elemDefault;

    beforeEach(() => {
      service = new PacienteService();
      elemDefault = new Paciente(
        123,
        'AAAAAAA',
        Identificaciont.CEDULA,
        0,
        0,
        Sexop.FEMENINO,
        0,
        'AAAAAAA',
        0,
        0,
        Grupoetnicop.Afrocolombiano,
        Estratop.Estrato_1,
        0,
        0,
        0,
        0,
        'AAAAAAA',
        0,
        0,
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign({}, elemDefault);
        axiosStub.get.resolves({ data: returnedFromService });

        return service.find(123).then(res => {
          expect(res).toMatchObject(elemDefault);
        });
      });

      it('should not find an element', async () => {
        axiosStub.get.rejects(error);
        return service
          .find(123)
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should create a Paciente', async () => {
        const returnedFromService = Object.assign(
          {
            id: 123,
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);

        axiosStub.post.resolves({ data: returnedFromService });
        return service.create({}).then(res => {
          expect(res).toMatchObject(expected);
        });
      });

      it('should not create a Paciente', async () => {
        axiosStub.post.rejects(error);

        return service
          .create({})
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should update a Paciente', async () => {
        const returnedFromService = Object.assign(
          {
            nombre: 'BBBBBB',
            tipoIdentificacion: 'BBBBBB',
            identificacion: 1,
            edad: 1,
            sexo: 'BBBBBB',
            telefono: 1,
            direccion: 'BBBBBB',
            pesoKG: 1,
            estaturaCM: 1,
            grupoEtnico: 'BBBBBB',
            estratoSocioeconomico: 'BBBBBB',
            oximetriaReferencia: 1,
            ritmoCardiacoReferencia: 1,
            presionSistolicaReferencia: 1,
            presionDistolicaReferencia: 1,
            comentarios: 'BBBBBB',
            pasosReferencia: 1,
            caloriasReferencia: 1,
            metaReferencia: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);
        axiosStub.put.resolves({ data: returnedFromService });

        return service.update(expected).then(res => {
          expect(res).toMatchObject(expected);
        });
      });

      it('should not update a Paciente', async () => {
        axiosStub.put.rejects(error);

        return service
          .update({})
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should partial update a Paciente', async () => {
        const patchObject = Object.assign(
          {
            nombre: 'BBBBBB',
            identificacion: 1,
            edad: 1,
            direccion: 'BBBBBB',
            estratoSocioeconomico: 'BBBBBB',
            oximetriaReferencia: 1,
            presionDistolicaReferencia: 1,
            pasosReferencia: 1,
          },
          new Paciente()
        );
        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);
        axiosStub.patch.resolves({ data: returnedFromService });

        return service.partialUpdate(patchObject).then(res => {
          expect(res).toMatchObject(expected);
        });
      });

      it('should not partial update a Paciente', async () => {
        axiosStub.patch.rejects(error);

        return service
          .partialUpdate({})
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should return a list of Paciente', async () => {
        const returnedFromService = Object.assign(
          {
            nombre: 'BBBBBB',
            tipoIdentificacion: 'BBBBBB',
            identificacion: 1,
            edad: 1,
            sexo: 'BBBBBB',
            telefono: 1,
            direccion: 'BBBBBB',
            pesoKG: 1,
            estaturaCM: 1,
            grupoEtnico: 'BBBBBB',
            estratoSocioeconomico: 'BBBBBB',
            oximetriaReferencia: 1,
            ritmoCardiacoReferencia: 1,
            presionSistolicaReferencia: 1,
            presionDistolicaReferencia: 1,
            comentarios: 'BBBBBB',
            pasosReferencia: 1,
            caloriasReferencia: 1,
            metaReferencia: 'BBBBBB',
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        axiosStub.get.resolves([returnedFromService]);
        return service.retrieve({ sort: {}, page: 0, size: 10 }).then(res => {
          expect(res).toContainEqual(expected);
        });
      });

      it('should not return a list of Paciente', async () => {
        axiosStub.get.rejects(error);

        return service
          .retrieve()
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should delete a Paciente', async () => {
        axiosStub.delete.resolves({ ok: true });
        return service.delete(123).then(res => {
          expect(res.ok).toBeTruthy();
        });
      });

      it('should not delete a Paciente', async () => {
        axiosStub.delete.rejects(error);

        return service
          .delete(123)
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });
    });
  });
});
