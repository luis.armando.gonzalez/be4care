/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import CondicionDetailComponent from '@/entities/pacientes/condicion/condicion-details.vue';
import CondicionClass from '@/entities/pacientes/condicion/condicion-details.component';
import CondicionService from '@/entities/pacientes/condicion/condicion.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Condicion Management Detail Component', () => {
    let wrapper: Wrapper<CondicionClass>;
    let comp: CondicionClass;
    let condicionServiceStub: SinonStubbedInstance<CondicionService>;

    beforeEach(() => {
      condicionServiceStub = sinon.createStubInstance<CondicionService>(CondicionService);

      wrapper = shallowMount<CondicionClass>(CondicionDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { condicionService: () => condicionServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundCondicion = { id: 123 };
        condicionServiceStub.find.resolves(foundCondicion);

        // WHEN
        comp.retrieveCondicion(123);
        await comp.$nextTick();

        // THEN
        expect(comp.condicion).toBe(foundCondicion);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundCondicion = { id: 123 };
        condicionServiceStub.find.resolves(foundCondicion);

        // WHEN
        comp.beforeRouteEnter({ params: { condicionId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.condicion).toBe(foundCondicion);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
