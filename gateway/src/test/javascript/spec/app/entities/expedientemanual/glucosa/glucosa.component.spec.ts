/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import GlucosaComponent from '@/entities/expedientemanual/glucosa/glucosa.vue';
import GlucosaClass from '@/entities/expedientemanual/glucosa/glucosa.component';
import GlucosaService from '@/entities/expedientemanual/glucosa/glucosa.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Glucosa Management Component', () => {
    let wrapper: Wrapper<GlucosaClass>;
    let comp: GlucosaClass;
    let glucosaServiceStub: SinonStubbedInstance<GlucosaService>;

    beforeEach(() => {
      glucosaServiceStub = sinon.createStubInstance<GlucosaService>(GlucosaService);
      glucosaServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<GlucosaClass>(GlucosaComponent, {
        store,
        i18n,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          glucosaService: () => glucosaServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      glucosaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllGlucosas();
      await comp.$nextTick();

      // THEN
      expect(glucosaServiceStub.retrieve.called).toBeTruthy();
      expect(comp.glucosas[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      glucosaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(glucosaServiceStub.retrieve.called).toBeTruthy();
      expect(comp.glucosas[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      glucosaServiceStub.retrieve.reset();
      glucosaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(glucosaServiceStub.retrieve.callCount).toEqual(2);
      expect(comp.page).toEqual(1);
      expect(comp.glucosas[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      glucosaServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeGlucosa();
      await comp.$nextTick();

      // THEN
      expect(glucosaServiceStub.delete.called).toBeTruthy();
      expect(glucosaServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});
