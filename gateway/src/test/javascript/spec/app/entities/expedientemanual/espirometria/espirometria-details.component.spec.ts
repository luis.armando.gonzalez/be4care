/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import EspirometriaDetailComponent from '@/entities/expedientemanual/espirometria/espirometria-details.vue';
import EspirometriaClass from '@/entities/expedientemanual/espirometria/espirometria-details.component';
import EspirometriaService from '@/entities/expedientemanual/espirometria/espirometria.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Espirometria Management Detail Component', () => {
    let wrapper: Wrapper<EspirometriaClass>;
    let comp: EspirometriaClass;
    let espirometriaServiceStub: SinonStubbedInstance<EspirometriaService>;

    beforeEach(() => {
      espirometriaServiceStub = sinon.createStubInstance<EspirometriaService>(EspirometriaService);

      wrapper = shallowMount<EspirometriaClass>(EspirometriaDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { espirometriaService: () => espirometriaServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundEspirometria = { id: 123 };
        espirometriaServiceStub.find.resolves(foundEspirometria);

        // WHEN
        comp.retrieveEspirometria(123);
        await comp.$nextTick();

        // THEN
        expect(comp.espirometria).toBe(foundEspirometria);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundEspirometria = { id: 123 };
        espirometriaServiceStub.find.resolves(foundEspirometria);

        // WHEN
        comp.beforeRouteEnter({ params: { espirometriaId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.espirometria).toBe(foundEspirometria);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
