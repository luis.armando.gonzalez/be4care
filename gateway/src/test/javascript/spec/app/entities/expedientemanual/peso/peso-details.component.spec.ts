/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import PesoDetailComponent from '@/entities/expedientemanual/peso/peso-details.vue';
import PesoClass from '@/entities/expedientemanual/peso/peso-details.component';
import PesoService from '@/entities/expedientemanual/peso/peso.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Peso Management Detail Component', () => {
    let wrapper: Wrapper<PesoClass>;
    let comp: PesoClass;
    let pesoServiceStub: SinonStubbedInstance<PesoService>;

    beforeEach(() => {
      pesoServiceStub = sinon.createStubInstance<PesoService>(PesoService);

      wrapper = shallowMount<PesoClass>(PesoDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { pesoService: () => pesoServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundPeso = { id: 123 };
        pesoServiceStub.find.resolves(foundPeso);

        // WHEN
        comp.retrievePeso(123);
        await comp.$nextTick();

        // THEN
        expect(comp.peso).toBe(foundPeso);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundPeso = { id: 123 };
        pesoServiceStub.find.resolves(foundPeso);

        // WHEN
        comp.beforeRouteEnter({ params: { pesoId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.peso).toBe(foundPeso);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
