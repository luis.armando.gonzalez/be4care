/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import EspirometriaComponent from '@/entities/expedientemanual/espirometria/espirometria.vue';
import EspirometriaClass from '@/entities/expedientemanual/espirometria/espirometria.component';
import EspirometriaService from '@/entities/expedientemanual/espirometria/espirometria.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Espirometria Management Component', () => {
    let wrapper: Wrapper<EspirometriaClass>;
    let comp: EspirometriaClass;
    let espirometriaServiceStub: SinonStubbedInstance<EspirometriaService>;

    beforeEach(() => {
      espirometriaServiceStub = sinon.createStubInstance<EspirometriaService>(EspirometriaService);
      espirometriaServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<EspirometriaClass>(EspirometriaComponent, {
        store,
        i18n,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          espirometriaService: () => espirometriaServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      espirometriaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllEspirometrias();
      await comp.$nextTick();

      // THEN
      expect(espirometriaServiceStub.retrieve.called).toBeTruthy();
      expect(comp.espirometrias[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      espirometriaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(espirometriaServiceStub.retrieve.called).toBeTruthy();
      expect(comp.espirometrias[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      espirometriaServiceStub.retrieve.reset();
      espirometriaServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(espirometriaServiceStub.retrieve.callCount).toEqual(2);
      expect(comp.page).toEqual(1);
      expect(comp.espirometrias[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      espirometriaServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeEspirometria();
      await comp.$nextTick();

      // THEN
      expect(espirometriaServiceStub.delete.called).toBeTruthy();
      expect(espirometriaServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});
