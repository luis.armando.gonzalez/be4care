/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import GlucosaDetailComponent from '@/entities/expedientemanual/glucosa/glucosa-details.vue';
import GlucosaClass from '@/entities/expedientemanual/glucosa/glucosa-details.component';
import GlucosaService from '@/entities/expedientemanual/glucosa/glucosa.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Glucosa Management Detail Component', () => {
    let wrapper: Wrapper<GlucosaClass>;
    let comp: GlucosaClass;
    let glucosaServiceStub: SinonStubbedInstance<GlucosaService>;

    beforeEach(() => {
      glucosaServiceStub = sinon.createStubInstance<GlucosaService>(GlucosaService);

      wrapper = shallowMount<GlucosaClass>(GlucosaDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { glucosaService: () => glucosaServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundGlucosa = { id: 123 };
        glucosaServiceStub.find.resolves(foundGlucosa);

        // WHEN
        comp.retrieveGlucosa(123);
        await comp.$nextTick();

        // THEN
        expect(comp.glucosa).toBe(foundGlucosa);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundGlucosa = { id: 123 };
        glucosaServiceStub.find.resolves(foundGlucosa);

        // WHEN
        comp.beforeRouteEnter({ params: { glucosaId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.glucosa).toBe(foundGlucosa);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
