import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Ciudad e2e test', () => {
  const ciudadPageUrl = '/ciudad';
  const ciudadPageUrlPattern = new RegExp('/ciudad(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const ciudadSample = { ciudad: 'Dollar Madera payment' };

  let ciudad: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/pacientes/api/ciudads').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/pacientes/api/ciudads+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/pacientes/api/ciudads').as('postEntityRequest');
    cy.intercept('DELETE', '/services/pacientes/api/ciudads/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (ciudad) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/pacientes/api/ciudads/${ciudad.id}`,
      }).then(() => {
        ciudad = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Ciudads menu should load Ciudads page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('ciudad');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Ciudad').should('exist');
    cy.url().should('match', ciudadPageUrlPattern);
  });

  describe('Ciudad page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(ciudadPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Ciudad page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/ciudad/new$'));
        cy.getEntityCreateUpdateHeading('Ciudad');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', ciudadPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/pacientes/api/ciudads',
          body: ciudadSample,
        }).then(({ body }) => {
          ciudad = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/pacientes/api/ciudads+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [ciudad],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(ciudadPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Ciudad page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('ciudad');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', ciudadPageUrlPattern);
      });

      it('edit button click should load edit Ciudad page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Ciudad');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', ciudadPageUrlPattern);
      });

      it('last delete button click should delete instance of Ciudad', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('ciudad').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', ciudadPageUrlPattern);

        ciudad = undefined;
      });
    });
  });

  describe('new Ciudad page', () => {
    beforeEach(() => {
      cy.visit(`${ciudadPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Ciudad');
    });

    it('should create an instance of Ciudad', () => {
      cy.get(`[data-cy="ciudad"]`).type('Algodón monitor').should('have.value', 'Algodón monitor');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        ciudad = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', ciudadPageUrlPattern);
    });
  });
});
