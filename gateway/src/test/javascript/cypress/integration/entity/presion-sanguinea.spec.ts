import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('PresionSanguinea e2e test', () => {
  const presionSanguineaPageUrl = '/presion-sanguinea';
  const presionSanguineaPageUrlPattern = new RegExp('/presion-sanguinea(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const presionSanguineaSample = { presionSanguineaSistolica: 52213 };

  let presionSanguinea: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/manilla/api/presion-sanguineas').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/manilla/api/presion-sanguineas+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/manilla/api/presion-sanguineas').as('postEntityRequest');
    cy.intercept('DELETE', '/services/manilla/api/presion-sanguineas/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (presionSanguinea) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/manilla/api/presion-sanguineas/${presionSanguinea.id}`,
      }).then(() => {
        presionSanguinea = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('PresionSanguineas menu should load PresionSanguineas page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('presion-sanguinea');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('PresionSanguinea').should('exist');
    cy.url().should('match', presionSanguineaPageUrlPattern);
  });

  describe('PresionSanguinea page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(presionSanguineaPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create PresionSanguinea page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/presion-sanguinea/new$'));
        cy.getEntityCreateUpdateHeading('PresionSanguinea');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', presionSanguineaPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/manilla/api/presion-sanguineas',
          body: presionSanguineaSample,
        }).then(({ body }) => {
          presionSanguinea = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/manilla/api/presion-sanguineas+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [presionSanguinea],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(presionSanguineaPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details PresionSanguinea page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('presionSanguinea');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', presionSanguineaPageUrlPattern);
      });

      it('edit button click should load edit PresionSanguinea page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('PresionSanguinea');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', presionSanguineaPageUrlPattern);
      });

      it('last delete button click should delete instance of PresionSanguinea', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('presionSanguinea').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', presionSanguineaPageUrlPattern);

        presionSanguinea = undefined;
      });
    });
  });

  describe('new PresionSanguinea page', () => {
    beforeEach(() => {
      cy.visit(`${presionSanguineaPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('PresionSanguinea');
    });

    it('should create an instance of PresionSanguinea', () => {
      cy.get(`[data-cy="presionSanguineaSistolica"]`).type('98504').should('have.value', '98504');

      cy.get(`[data-cy="presionSanguineaDiastolica"]`).type('44349').should('have.value', '44349');

      cy.get(`[data-cy="fechaRegistro"]`).type('2021-11-10T23:03').should('have.value', '2021-11-10T23:03');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        presionSanguinea = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', presionSanguineaPageUrlPattern);
    });
  });
});
