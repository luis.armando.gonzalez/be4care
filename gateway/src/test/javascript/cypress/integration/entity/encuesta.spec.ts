import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Encuesta e2e test', () => {
  const encuestaPageUrl = '/encuesta';
  const encuestaPageUrlPattern = new RegExp('/encuesta(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const encuestaSample = { fecha: '2021-11-11T05:44:43.549Z' };

  let encuesta: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/manilla/api/encuestas').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/manilla/api/encuestas+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/manilla/api/encuestas').as('postEntityRequest');
    cy.intercept('DELETE', '/services/manilla/api/encuestas/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (encuesta) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/manilla/api/encuestas/${encuesta.id}`,
      }).then(() => {
        encuesta = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Encuestas menu should load Encuestas page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('encuesta');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Encuesta').should('exist');
    cy.url().should('match', encuestaPageUrlPattern);
  });

  describe('Encuesta page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(encuestaPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Encuesta page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/encuesta/new$'));
        cy.getEntityCreateUpdateHeading('Encuesta');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', encuestaPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/manilla/api/encuestas',
          body: encuestaSample,
        }).then(({ body }) => {
          encuesta = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/manilla/api/encuestas+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [encuesta],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(encuestaPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Encuesta page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('encuesta');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', encuestaPageUrlPattern);
      });

      it('edit button click should load edit Encuesta page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Encuesta');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', encuestaPageUrlPattern);
      });

      it('last delete button click should delete instance of Encuesta', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('encuesta').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', encuestaPageUrlPattern);

        encuesta = undefined;
      });
    });
  });

  describe('new Encuesta page', () => {
    beforeEach(() => {
      cy.visit(`${encuestaPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Encuesta');
    });

    it('should create an instance of Encuesta', () => {
      cy.get(`[data-cy="fecha"]`).type('2021-11-11T15:42').should('have.value', '2021-11-11T15:42');

      cy.get(`[data-cy="debilidad"]`).should('not.be.checked');
      cy.get(`[data-cy="debilidad"]`).click().should('be.checked');

      cy.get(`[data-cy="cefalea"]`).should('not.be.checked');
      cy.get(`[data-cy="cefalea"]`).click().should('be.checked');

      cy.get(`[data-cy="calambres"]`).should('not.be.checked');
      cy.get(`[data-cy="calambres"]`).click().should('be.checked');

      cy.get(`[data-cy="nauseas"]`).should('not.be.checked');
      cy.get(`[data-cy="nauseas"]`).click().should('be.checked');

      cy.get(`[data-cy="vomito"]`).should('not.be.checked');
      cy.get(`[data-cy="vomito"]`).click().should('be.checked');

      cy.get(`[data-cy="mareo"]`).should('not.be.checked');
      cy.get(`[data-cy="mareo"]`).click().should('be.checked');

      cy.get(`[data-cy="ninguna"]`).should('not.be.checked');
      cy.get(`[data-cy="ninguna"]`).click().should('be.checked');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        encuesta = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', encuestaPageUrlPattern);
    });
  });
});
