import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Programa e2e test', () => {
  const programaPageUrl = '/programa';
  const programaPageUrlPattern = new RegExp('/programa(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const programaSample = { caloriasActividad: 98117 };

  let programa: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/manilla/api/programas').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/manilla/api/programas+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/manilla/api/programas').as('postEntityRequest');
    cy.intercept('DELETE', '/services/manilla/api/programas/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (programa) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/manilla/api/programas/${programa.id}`,
      }).then(() => {
        programa = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Programas menu should load Programas page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('programa');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Programa').should('exist');
    cy.url().should('match', programaPageUrlPattern);
  });

  describe('Programa page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(programaPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Programa page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/programa/new$'));
        cy.getEntityCreateUpdateHeading('Programa');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', programaPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/manilla/api/programas',
          body: programaSample,
        }).then(({ body }) => {
          programa = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/manilla/api/programas+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [programa],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(programaPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Programa page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('programa');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', programaPageUrlPattern);
      });

      it('edit button click should load edit Programa page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Programa');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', programaPageUrlPattern);
      });

      it('last delete button click should delete instance of Programa', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('programa').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', programaPageUrlPattern);

        programa = undefined;
      });
    });
  });

  describe('new Programa page', () => {
    beforeEach(() => {
      cy.visit(`${programaPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Programa');
    });

    it('should create an instance of Programa', () => {
      cy.get(`[data-cy="caloriasActividad"]`).type('89642').should('have.value', '89642');

      cy.get(`[data-cy="pasosActividad"]`).type('30291').should('have.value', '30291');

      cy.get(`[data-cy="fechaRegistro"]`).type('2021-11-11T13:14').should('have.value', '2021-11-11T13:14');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        programa = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', programaPageUrlPattern);
    });
  });
});
