import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('CuestionarioEstado e2e test', () => {
  const cuestionarioEstadoPageUrl = '/cuestionario-estado';
  const cuestionarioEstadoPageUrlPattern = new RegExp('/cuestionario-estado(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const cuestionarioEstadoSample = { valor: 3210 };

  let cuestionarioEstado: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/pacientes/api/cuestionario-estados').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/pacientes/api/cuestionario-estados+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/pacientes/api/cuestionario-estados').as('postEntityRequest');
    cy.intercept('DELETE', '/services/pacientes/api/cuestionario-estados/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (cuestionarioEstado) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/pacientes/api/cuestionario-estados/${cuestionarioEstado.id}`,
      }).then(() => {
        cuestionarioEstado = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('CuestionarioEstados menu should load CuestionarioEstados page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('cuestionario-estado');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('CuestionarioEstado').should('exist');
    cy.url().should('match', cuestionarioEstadoPageUrlPattern);
  });

  describe('CuestionarioEstado page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(cuestionarioEstadoPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create CuestionarioEstado page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/cuestionario-estado/new$'));
        cy.getEntityCreateUpdateHeading('CuestionarioEstado');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', cuestionarioEstadoPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/pacientes/api/cuestionario-estados',
          body: cuestionarioEstadoSample,
        }).then(({ body }) => {
          cuestionarioEstado = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/pacientes/api/cuestionario-estados+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [cuestionarioEstado],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(cuestionarioEstadoPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details CuestionarioEstado page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('cuestionarioEstado');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', cuestionarioEstadoPageUrlPattern);
      });

      it('edit button click should load edit CuestionarioEstado page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('CuestionarioEstado');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', cuestionarioEstadoPageUrlPattern);
      });

      it('last delete button click should delete instance of CuestionarioEstado', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('cuestionarioEstado').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', cuestionarioEstadoPageUrlPattern);

        cuestionarioEstado = undefined;
      });
    });
  });

  describe('new CuestionarioEstado page', () => {
    beforeEach(() => {
      cy.visit(`${cuestionarioEstadoPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('CuestionarioEstado');
    });

    it('should create an instance of CuestionarioEstado', () => {
      cy.get(`[data-cy="valor"]`).type('56299').should('have.value', '56299');

      cy.get(`[data-cy="valoracion"]`).type('firmware whiteboard').should('have.value', 'firmware whiteboard');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        cuestionarioEstado = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', cuestionarioEstadoPageUrlPattern);
    });
  });
});
