import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Biologico e2e test', () => {
  const biologicoPageUrl = '/biologico';
  const biologicoPageUrlPattern = new RegExp('/biologico(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const biologicoSample = { tipoBiologico: 'payment' };

  let biologico: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/biologico/api/biologicos').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/biologico/api/biologicos+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/biologico/api/biologicos').as('postEntityRequest');
    cy.intercept('DELETE', '/services/biologico/api/biologicos/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (biologico) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/biologico/api/biologicos/${biologico.id}`,
      }).then(() => {
        biologico = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Biologicos menu should load Biologicos page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('biologico');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Biologico').should('exist');
    cy.url().should('match', biologicoPageUrlPattern);
  });

  describe('Biologico page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(biologicoPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Biologico page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/biologico/new$'));
        cy.getEntityCreateUpdateHeading('Biologico');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', biologicoPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/biologico/api/biologicos',
          body: biologicoSample,
        }).then(({ body }) => {
          biologico = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/biologico/api/biologicos+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [biologico],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(biologicoPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Biologico page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('biologico');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', biologicoPageUrlPattern);
      });

      it('edit button click should load edit Biologico page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Biologico');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', biologicoPageUrlPattern);
      });

      it('last delete button click should delete instance of Biologico', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('biologico').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', biologicoPageUrlPattern);

        biologico = undefined;
      });
    });
  });

  describe('new Biologico page', () => {
    beforeEach(() => {
      cy.visit(`${biologicoPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Biologico');
    });

    it('should create an instance of Biologico', () => {
      cy.get(`[data-cy="tipoBiologico"]`).type('Bedfordshire').should('have.value', 'Bedfordshire');

      cy.get(`[data-cy="detalleBiologico"]`).type('distributed Valenciana').should('have.value', 'distributed Valenciana');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        biologico = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', biologicoPageUrlPattern);
    });
  });
});
