import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Espirometria e2e test', () => {
  const espirometriaPageUrl = '/espirometria';
  const espirometriaPageUrlPattern = new RegExp('/espirometria(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const espirometriaSample = { fev1T: 86459 };

  let espirometria: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/expedientemanual/api/espirometrias').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/expedientemanual/api/espirometrias+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/expedientemanual/api/espirometrias').as('postEntityRequest');
    cy.intercept('DELETE', '/services/expedientemanual/api/espirometrias/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (espirometria) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/expedientemanual/api/espirometrias/${espirometria.id}`,
      }).then(() => {
        espirometria = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Espirometrias menu should load Espirometrias page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('espirometria');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Espirometria').should('exist');
    cy.url().should('match', espirometriaPageUrlPattern);
  });

  describe('Espirometria page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(espirometriaPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Espirometria page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/espirometria/new$'));
        cy.getEntityCreateUpdateHeading('Espirometria');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', espirometriaPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/expedientemanual/api/espirometrias',
          body: espirometriaSample,
        }).then(({ body }) => {
          espirometria = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/expedientemanual/api/espirometrias+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [espirometria],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(espirometriaPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Espirometria page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('espirometria');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', espirometriaPageUrlPattern);
      });

      it('edit button click should load edit Espirometria page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Espirometria');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', espirometriaPageUrlPattern);
      });

      it('last delete button click should delete instance of Espirometria', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('espirometria').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', espirometriaPageUrlPattern);

        espirometria = undefined;
      });
    });
  });

  describe('new Espirometria page', () => {
    beforeEach(() => {
      cy.visit(`${espirometriaPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Espirometria');
    });

    it('should create an instance of Espirometria', () => {
      cy.get(`[data-cy="fev1T"]`).type('85371').should('have.value', '85371');

      cy.get(`[data-cy="fev1R"]`).type('28420').should('have.value', '28420');

      cy.get(`[data-cy="fvcT"]`).type('79234').should('have.value', '79234');

      cy.get(`[data-cy="fvcR"]`).type('56872').should('have.value', '56872');

      cy.get(`[data-cy="indiceTP"]`).type('40229').should('have.value', '40229');

      cy.get(`[data-cy="fechaToma"]`).type('2021-11-11T13:10').should('have.value', '2021-11-11T13:10');

      cy.get(`[data-cy="resultado"]`).type('Violeta').should('have.value', 'Violeta');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        espirometria = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', espirometriaPageUrlPattern);
    });
  });
});
