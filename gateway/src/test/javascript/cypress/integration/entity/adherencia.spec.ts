import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Adherencia e2e test', () => {
  const adherenciaPageUrl = '/adherencia';
  const adherenciaPageUrlPattern = new RegExp('/adherencia(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const adherenciaSample = { horaToma: '2021-11-11T07:44:03.798Z' };

  let adherencia: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/pacientes/api/adherencias').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/pacientes/api/adherencias+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/pacientes/api/adherencias').as('postEntityRequest');
    cy.intercept('DELETE', '/services/pacientes/api/adherencias/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (adherencia) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/pacientes/api/adherencias/${adherencia.id}`,
      }).then(() => {
        adherencia = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Adherencias menu should load Adherencias page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('adherencia');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Adherencia').should('exist');
    cy.url().should('match', adherenciaPageUrlPattern);
  });

  describe('Adherencia page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(adherenciaPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Adherencia page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/adherencia/new$'));
        cy.getEntityCreateUpdateHeading('Adherencia');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', adherenciaPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/pacientes/api/adherencias',
          body: adherenciaSample,
        }).then(({ body }) => {
          adherencia = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/pacientes/api/adherencias+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [adherencia],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(adherenciaPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Adherencia page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('adherencia');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', adherenciaPageUrlPattern);
      });

      it('edit button click should load edit Adherencia page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Adherencia');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', adherenciaPageUrlPattern);
      });

      it('last delete button click should delete instance of Adherencia', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('adherencia').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', adherenciaPageUrlPattern);

        adherencia = undefined;
      });
    });
  });

  describe('new Adherencia page', () => {
    beforeEach(() => {
      cy.visit(`${adherenciaPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Adherencia');
    });

    it('should create an instance of Adherencia', () => {
      cy.get(`[data-cy="horaToma"]`).type('2021-11-10T21:40').should('have.value', '2021-11-10T21:40');

      cy.get(`[data-cy="respuesta"]`).should('not.be.checked');
      cy.get(`[data-cy="respuesta"]`).click().should('be.checked');

      cy.get(`[data-cy="valor"]`).type('5563').should('have.value', '5563');

      cy.get(`[data-cy="comentario"]`).type('cutting-edge Interno').should('have.value', 'cutting-edge Interno');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        adherencia = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', adherenciaPageUrlPattern);
    });
  });
});
