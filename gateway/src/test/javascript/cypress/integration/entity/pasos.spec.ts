import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Pasos e2e test', () => {
  const pasosPageUrl = '/pasos';
  const pasosPageUrlPattern = new RegExp('/pasos(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const pasosSample = { nroPasos: 83382 };

  let pasos: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/manilla/api/pasos').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/manilla/api/pasos+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/manilla/api/pasos').as('postEntityRequest');
    cy.intercept('DELETE', '/services/manilla/api/pasos/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (pasos) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/manilla/api/pasos/${pasos.id}`,
      }).then(() => {
        pasos = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Pasos menu should load Pasos page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('pasos');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Pasos').should('exist');
    cy.url().should('match', pasosPageUrlPattern);
  });

  describe('Pasos page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(pasosPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Pasos page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/pasos/new$'));
        cy.getEntityCreateUpdateHeading('Pasos');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', pasosPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/manilla/api/pasos',
          body: pasosSample,
        }).then(({ body }) => {
          pasos = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/manilla/api/pasos+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [pasos],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(pasosPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Pasos page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('pasos');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', pasosPageUrlPattern);
      });

      it('edit button click should load edit Pasos page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Pasos');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', pasosPageUrlPattern);
      });

      it('last delete button click should delete instance of Pasos', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('pasos').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', pasosPageUrlPattern);

        pasos = undefined;
      });
    });
  });

  describe('new Pasos page', () => {
    beforeEach(() => {
      cy.visit(`${pasosPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Pasos');
    });

    it('should create an instance of Pasos', () => {
      cy.get(`[data-cy="nroPasos"]`).type('89563').should('have.value', '89563');

      cy.get(`[data-cy="timeInstant"]`).type('2021-11-11T14:02').should('have.value', '2021-11-11T14:02');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        pasos = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', pasosPageUrlPattern);
    });
  });
});
