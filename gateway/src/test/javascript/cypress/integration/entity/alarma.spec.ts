import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Alarma e2e test', () => {
  const alarmaPageUrl = '/alarma';
  const alarmaPageUrlPattern = new RegExp('/alarma(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const alarmaSample = { timeInstant: '2021-11-11T06:34:23.644Z' };

  let alarma: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/pacientes/api/alarmas').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/pacientes/api/alarmas+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/pacientes/api/alarmas').as('postEntityRequest');
    cy.intercept('DELETE', '/services/pacientes/api/alarmas/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (alarma) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/pacientes/api/alarmas/${alarma.id}`,
      }).then(() => {
        alarma = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Alarmas menu should load Alarmas page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('alarma');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Alarma').should('exist');
    cy.url().should('match', alarmaPageUrlPattern);
  });

  describe('Alarma page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(alarmaPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Alarma page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/alarma/new$'));
        cy.getEntityCreateUpdateHeading('Alarma');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', alarmaPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/pacientes/api/alarmas',
          body: alarmaSample,
        }).then(({ body }) => {
          alarma = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/pacientes/api/alarmas+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [alarma],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(alarmaPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Alarma page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('alarma');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', alarmaPageUrlPattern);
      });

      it('edit button click should load edit Alarma page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Alarma');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', alarmaPageUrlPattern);
      });

      it('last delete button click should delete instance of Alarma', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('alarma').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', alarmaPageUrlPattern);

        alarma = undefined;
      });
    });
  });

  describe('new Alarma page', () => {
    beforeEach(() => {
      cy.visit(`${alarmaPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Alarma');
    });

    it('should create an instance of Alarma', () => {
      cy.get(`[data-cy="timeInstant"]`).type('2021-11-11T10:23').should('have.value', '2021-11-11T10:23');

      cy.get(`[data-cy="descripcion"]`).type('Ejecutivo').should('have.value', 'Ejecutivo');

      cy.get(`[data-cy="procedimiento"]`).type('Madera Account').should('have.value', 'Madera Account');

      cy.get(`[data-cy="titulo"]`).type('Account Rioja').should('have.value', 'Account Rioja');

      cy.get(`[data-cy="verificar"]`).should('not.be.checked');
      cy.get(`[data-cy="verificar"]`).click().should('be.checked');

      cy.get(`[data-cy="observaciones"]`).type('bypassing').should('have.value', 'bypassing');

      cy.get(`[data-cy="prioridad"]`).type('jerarquía').should('have.value', 'jerarquía');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        alarma = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', alarmaPageUrlPattern);
    });
  });
});
