import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Peso e2e test', () => {
  const pesoPageUrl = '/peso';
  const pesoPageUrlPattern = new RegExp('/peso(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const pesoSample = { pesoKG: 83604 };

  let peso: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/expedientemanual/api/pesos').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/expedientemanual/api/pesos+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/expedientemanual/api/pesos').as('postEntityRequest');
    cy.intercept('DELETE', '/services/expedientemanual/api/pesos/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (peso) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/expedientemanual/api/pesos/${peso.id}`,
      }).then(() => {
        peso = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Pesos menu should load Pesos page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('peso');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Peso').should('exist');
    cy.url().should('match', pesoPageUrlPattern);
  });

  describe('Peso page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(pesoPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Peso page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/peso/new$'));
        cy.getEntityCreateUpdateHeading('Peso');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', pesoPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/expedientemanual/api/pesos',
          body: pesoSample,
        }).then(({ body }) => {
          peso = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/expedientemanual/api/pesos+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [peso],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(pesoPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Peso page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('peso');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', pesoPageUrlPattern);
      });

      it('edit button click should load edit Peso page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Peso');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', pesoPageUrlPattern);
      });

      it('last delete button click should delete instance of Peso', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('peso').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', pesoPageUrlPattern);

        peso = undefined;
      });
    });
  });

  describe('new Peso page', () => {
    beforeEach(() => {
      cy.visit(`${pesoPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Peso');
    });

    it('should create an instance of Peso', () => {
      cy.get(`[data-cy="pesoKG"]`).type('44998').should('have.value', '44998');

      cy.get(`[data-cy="descripcion"]`).type('Mejorado').should('have.value', 'Mejorado');

      cy.get(`[data-cy="fechaRegistro"]`).type('2021-11-10T23:43').should('have.value', '2021-11-10T23:43');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        peso = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', pesoPageUrlPattern);
    });
  });
});
