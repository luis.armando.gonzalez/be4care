import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('CicloActividad e2e test', () => {
  const cicloActividadPageUrl = '/ciclo-actividad';
  const cicloActividadPageUrlPattern = new RegExp('/ciclo-actividad(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const cicloActividadSample = { periodoCiclo: 22969 };

  let cicloActividad: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/biologico/api/ciclo-actividads').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/biologico/api/ciclo-actividads+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/biologico/api/ciclo-actividads').as('postEntityRequest');
    cy.intercept('DELETE', '/services/biologico/api/ciclo-actividads/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (cicloActividad) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/biologico/api/ciclo-actividads/${cicloActividad.id}`,
      }).then(() => {
        cicloActividad = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('CicloActividads menu should load CicloActividads page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('ciclo-actividad');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('CicloActividad').should('exist');
    cy.url().should('match', cicloActividadPageUrlPattern);
  });

  describe('CicloActividad page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(cicloActividadPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create CicloActividad page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/ciclo-actividad/new$'));
        cy.getEntityCreateUpdateHeading('CicloActividad');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', cicloActividadPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/biologico/api/ciclo-actividads',
          body: cicloActividadSample,
        }).then(({ body }) => {
          cicloActividad = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/biologico/api/ciclo-actividads+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [cicloActividad],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(cicloActividadPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details CicloActividad page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('cicloActividad');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', cicloActividadPageUrlPattern);
      });

      it('edit button click should load edit CicloActividad page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('CicloActividad');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', cicloActividadPageUrlPattern);
      });

      it('last delete button click should delete instance of CicloActividad', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('cicloActividad').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', cicloActividadPageUrlPattern);

        cicloActividad = undefined;
      });
    });
  });

  describe('new CicloActividad page', () => {
    beforeEach(() => {
      cy.visit(`${cicloActividadPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('CicloActividad');
    });

    it('should create an instance of CicloActividad', () => {
      cy.get(`[data-cy="periodoCiclo"]`).type('51692').should('have.value', '51692');

      cy.get(`[data-cy="cantidad"]`).type('97780').should('have.value', '97780');

      cy.get(`[data-cy="fechaInicioCiclo"]`).type('2021-11-11T07:25').should('have.value', '2021-11-11T07:25');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        cicloActividad = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', cicloActividadPageUrlPattern);
    });
  });
});
