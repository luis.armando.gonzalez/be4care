import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Fabricante e2e test', () => {
  const fabricantePageUrl = '/fabricante';
  const fabricantePageUrlPattern = new RegExp('/fabricante(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const fabricanteSample = { nombreFabricante: 'Li4vZmFrZS1kYXRhL2Jsb2IvaGlwc3Rlci50eHQ=' };

  let fabricante: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/biologico/api/fabricantes').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/biologico/api/fabricantes+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/biologico/api/fabricantes').as('postEntityRequest');
    cy.intercept('DELETE', '/services/biologico/api/fabricantes/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (fabricante) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/biologico/api/fabricantes/${fabricante.id}`,
      }).then(() => {
        fabricante = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Fabricantes menu should load Fabricantes page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('fabricante');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Fabricante').should('exist');
    cy.url().should('match', fabricantePageUrlPattern);
  });

  describe('Fabricante page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(fabricantePageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Fabricante page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/fabricante/new$'));
        cy.getEntityCreateUpdateHeading('Fabricante');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', fabricantePageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/biologico/api/fabricantes',
          body: fabricanteSample,
        }).then(({ body }) => {
          fabricante = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/biologico/api/fabricantes+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [fabricante],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(fabricantePageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Fabricante page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('fabricante');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', fabricantePageUrlPattern);
      });

      it('edit button click should load edit Fabricante page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Fabricante');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', fabricantePageUrlPattern);
      });

      it('last delete button click should delete instance of Fabricante', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('fabricante').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', fabricantePageUrlPattern);

        fabricante = undefined;
      });
    });
  });

  describe('new Fabricante page', () => {
    beforeEach(() => {
      cy.visit(`${fabricantePageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Fabricante');
    });

    it('should create an instance of Fabricante', () => {
      cy.get(`[data-cy="nombreFabricante"]`)
        .type('../fake-data/blob/hipster.txt')
        .invoke('val')
        .should('match', new RegExp('../fake-data/blob/hipster.txt'));

      cy.get(`[data-cy="productosFabricanet"]`)
        .type('../fake-data/blob/hipster.txt')
        .invoke('val')
        .should('match', new RegExp('../fake-data/blob/hipster.txt'));

      cy.get(`[data-cy="detalleFabricante"]`)
        .type('../fake-data/blob/hipster.txt')
        .invoke('val')
        .should('match', new RegExp('../fake-data/blob/hipster.txt'));

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        fabricante = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', fabricantePageUrlPattern);
    });
  });
});
