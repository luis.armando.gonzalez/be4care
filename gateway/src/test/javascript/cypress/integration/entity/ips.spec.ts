import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('IPS e2e test', () => {
  const iPSPageUrl = '/ips';
  const iPSPageUrlPattern = new RegExp('/ips(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const iPSSample = { nombre: 'Jamaica' };

  let iPS: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/pacientes/api/ips').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/pacientes/api/ips+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/pacientes/api/ips').as('postEntityRequest');
    cy.intercept('DELETE', '/services/pacientes/api/ips/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (iPS) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/pacientes/api/ips/${iPS.id}`,
      }).then(() => {
        iPS = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('IPS menu should load IPS page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('ips');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('IPS').should('exist');
    cy.url().should('match', iPSPageUrlPattern);
  });

  describe('IPS page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(iPSPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create IPS page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/ips/new$'));
        cy.getEntityCreateUpdateHeading('IPS');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', iPSPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/pacientes/api/ips',
          body: iPSSample,
        }).then(({ body }) => {
          iPS = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/pacientes/api/ips+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [iPS],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(iPSPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details IPS page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('iPS');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', iPSPageUrlPattern);
      });

      it('edit button click should load edit IPS page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('IPS');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', iPSPageUrlPattern);
      });

      it('last delete button click should delete instance of IPS', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('iPS').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', iPSPageUrlPattern);

        iPS = undefined;
      });
    });
  });

  describe('new IPS page', () => {
    beforeEach(() => {
      cy.visit(`${iPSPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('IPS');
    });

    it('should create an instance of IPS', () => {
      cy.get(`[data-cy="nombre"]`).type('Corea RSS').should('have.value', 'Corea RSS');

      cy.get(`[data-cy="nit"]`).type('Representante').should('have.value', 'Representante');

      cy.get(`[data-cy="direccion"]`).type('Violeta Acero arquitectura').should('have.value', 'Violeta Acero arquitectura');

      cy.get(`[data-cy="telefono"]`).type('Diseñador').should('have.value', 'Diseñador');

      cy.get(`[data-cy="correoElectronico"]`).type('technologies Account').should('have.value', 'technologies Account');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        iPS = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', iPSPageUrlPattern);
    });
  });
});
