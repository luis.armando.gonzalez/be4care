import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Tratamiento e2e test', () => {
  const tratamientoPageUrl = '/tratamiento';
  const tratamientoPageUrlPattern = new RegExp('/tratamiento(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const tratamientoSample = { descripcionTratamiento: 'Li4vZmFrZS1kYXRhL2Jsb2IvaGlwc3Rlci50eHQ=' };

  let tratamiento: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/pacientes/api/tratamientos').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/pacientes/api/tratamientos+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/pacientes/api/tratamientos').as('postEntityRequest');
    cy.intercept('DELETE', '/services/pacientes/api/tratamientos/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (tratamiento) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/pacientes/api/tratamientos/${tratamiento.id}`,
      }).then(() => {
        tratamiento = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Tratamientos menu should load Tratamientos page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('tratamiento');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Tratamiento').should('exist');
    cy.url().should('match', tratamientoPageUrlPattern);
  });

  describe('Tratamiento page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(tratamientoPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Tratamiento page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/tratamiento/new$'));
        cy.getEntityCreateUpdateHeading('Tratamiento');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', tratamientoPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/pacientes/api/tratamientos',
          body: tratamientoSample,
        }).then(({ body }) => {
          tratamiento = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/pacientes/api/tratamientos+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [tratamiento],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(tratamientoPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Tratamiento page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('tratamiento');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', tratamientoPageUrlPattern);
      });

      it('edit button click should load edit Tratamiento page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Tratamiento');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', tratamientoPageUrlPattern);
      });

      it('last delete button click should delete instance of Tratamiento', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('tratamiento').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', tratamientoPageUrlPattern);

        tratamiento = undefined;
      });
    });
  });

  describe('new Tratamiento page', () => {
    beforeEach(() => {
      cy.visit(`${tratamientoPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Tratamiento');
    });

    it('should create an instance of Tratamiento', () => {
      cy.get(`[data-cy="descripcionTratamiento"]`)
        .type('../fake-data/blob/hipster.txt')
        .invoke('val')
        .should('match', new RegExp('../fake-data/blob/hipster.txt'));

      cy.get(`[data-cy="fechaInicio"]`).type('2021-11-11T01:58').should('have.value', '2021-11-11T01:58');

      cy.get(`[data-cy="fechaFin"]`).type('2021-11-11T09:43').should('have.value', '2021-11-11T09:43');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        tratamiento = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', tratamientoPageUrlPattern);
    });
  });
});
