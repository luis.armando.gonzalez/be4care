import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Dispositivo e2e test', () => {
  const dispositivoPageUrl = '/dispositivo';
  const dispositivoPageUrlPattern = new RegExp('/dispositivo(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const dispositivoSample = { dispositivo: 'magnetic direccional Loan' };

  let dispositivo: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/manilla/api/dispositivos').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/manilla/api/dispositivos+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/manilla/api/dispositivos').as('postEntityRequest');
    cy.intercept('DELETE', '/services/manilla/api/dispositivos/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (dispositivo) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/manilla/api/dispositivos/${dispositivo.id}`,
      }).then(() => {
        dispositivo = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Dispositivos menu should load Dispositivos page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('dispositivo');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Dispositivo').should('exist');
    cy.url().should('match', dispositivoPageUrlPattern);
  });

  describe('Dispositivo page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(dispositivoPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Dispositivo page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/dispositivo/new$'));
        cy.getEntityCreateUpdateHeading('Dispositivo');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', dispositivoPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/manilla/api/dispositivos',
          body: dispositivoSample,
        }).then(({ body }) => {
          dispositivo = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/manilla/api/dispositivos+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [dispositivo],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(dispositivoPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Dispositivo page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('dispositivo');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', dispositivoPageUrlPattern);
      });

      it('edit button click should load edit Dispositivo page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Dispositivo');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', dispositivoPageUrlPattern);
      });

      it('last delete button click should delete instance of Dispositivo', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('dispositivo').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', dispositivoPageUrlPattern);

        dispositivo = undefined;
      });
    });
  });

  describe('new Dispositivo page', () => {
    beforeEach(() => {
      cy.visit(`${dispositivoPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Dispositivo');
    });

    it('should create an instance of Dispositivo', () => {
      cy.get(`[data-cy="dispositivo"]`).type('hacking Corporativo medición').should('have.value', 'hacking Corporativo medición');

      cy.get(`[data-cy="mac"]`).type('Parque COM Galicia').should('have.value', 'Parque COM Galicia');

      cy.get(`[data-cy="conectado"]`).should('not.be.checked');
      cy.get(`[data-cy="conectado"]`).click().should('be.checked');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        dispositivo = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', dispositivoPageUrlPattern);
    });
  });
});
