import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Medicamento e2e test', () => {
  const medicamentoPageUrl = '/medicamento';
  const medicamentoPageUrlPattern = new RegExp('/medicamento(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const medicamentoSample = { nombre: 'COM parsing world-class' };

  let medicamento: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/pacientes/api/medicamentos').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/pacientes/api/medicamentos+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/pacientes/api/medicamentos').as('postEntityRequest');
    cy.intercept('DELETE', '/services/pacientes/api/medicamentos/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (medicamento) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/pacientes/api/medicamentos/${medicamento.id}`,
      }).then(() => {
        medicamento = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Medicamentos menu should load Medicamentos page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('medicamento');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Medicamento').should('exist');
    cy.url().should('match', medicamentoPageUrlPattern);
  });

  describe('Medicamento page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(medicamentoPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Medicamento page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/medicamento/new$'));
        cy.getEntityCreateUpdateHeading('Medicamento');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', medicamentoPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/pacientes/api/medicamentos',
          body: medicamentoSample,
        }).then(({ body }) => {
          medicamento = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/pacientes/api/medicamentos+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [medicamento],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(medicamentoPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Medicamento page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('medicamento');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', medicamentoPageUrlPattern);
      });

      it('edit button click should load edit Medicamento page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Medicamento');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', medicamentoPageUrlPattern);
      });

      it('last delete button click should delete instance of Medicamento', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('medicamento').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', medicamentoPageUrlPattern);

        medicamento = undefined;
      });
    });
  });

  describe('new Medicamento page', () => {
    beforeEach(() => {
      cy.visit(`${medicamentoPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Medicamento');
    });

    it('should create an instance of Medicamento', () => {
      cy.get(`[data-cy="nombre"]`).type('navigating parse').should('have.value', 'navigating parse');

      cy.get(`[data-cy="descripcion"]`)
        .type('../fake-data/blob/hipster.txt')
        .invoke('val')
        .should('match', new RegExp('../fake-data/blob/hipster.txt'));

      cy.get(`[data-cy="fechaIngreso"]`).type('2021-11-11T12:55').should('have.value', '2021-11-11T12:55');

      cy.get(`[data-cy="presentacion"]`).select('Suspension');

      cy.get(`[data-cy="generico"]`)
        .type('../fake-data/blob/hipster.txt')
        .invoke('val')
        .should('match', new RegExp('../fake-data/blob/hipster.txt'));

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        medicamento = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', medicamentoPageUrlPattern);
    });
  });
});
