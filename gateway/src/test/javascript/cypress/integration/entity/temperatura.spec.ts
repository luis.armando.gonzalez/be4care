import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Temperatura e2e test', () => {
  const temperaturaPageUrl = '/temperatura';
  const temperaturaPageUrlPattern = new RegExp('/temperatura(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const temperaturaSample = { temperatura: 90741 };

  let temperatura: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/manilla/api/temperaturas').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/manilla/api/temperaturas+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/manilla/api/temperaturas').as('postEntityRequest');
    cy.intercept('DELETE', '/services/manilla/api/temperaturas/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (temperatura) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/manilla/api/temperaturas/${temperatura.id}`,
      }).then(() => {
        temperatura = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Temperaturas menu should load Temperaturas page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('temperatura');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Temperatura').should('exist');
    cy.url().should('match', temperaturaPageUrlPattern);
  });

  describe('Temperatura page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(temperaturaPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Temperatura page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/temperatura/new$'));
        cy.getEntityCreateUpdateHeading('Temperatura');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', temperaturaPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/manilla/api/temperaturas',
          body: temperaturaSample,
        }).then(({ body }) => {
          temperatura = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/manilla/api/temperaturas+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [temperatura],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(temperaturaPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Temperatura page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('temperatura');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', temperaturaPageUrlPattern);
      });

      it('edit button click should load edit Temperatura page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Temperatura');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', temperaturaPageUrlPattern);
      });

      it('last delete button click should delete instance of Temperatura', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('temperatura').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', temperaturaPageUrlPattern);

        temperatura = undefined;
      });
    });
  });

  describe('new Temperatura page', () => {
    beforeEach(() => {
      cy.visit(`${temperaturaPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Temperatura');
    });

    it('should create an instance of Temperatura', () => {
      cy.get(`[data-cy="temperatura"]`).type('83574').should('have.value', '83574');

      cy.get(`[data-cy="fechaRegistro"]`).type('2021-11-11T04:02').should('have.value', '2021-11-11T04:02');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        temperatura = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', temperaturaPageUrlPattern);
    });
  });
});
