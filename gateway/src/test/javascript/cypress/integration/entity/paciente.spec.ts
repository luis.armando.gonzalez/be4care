import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Paciente e2e test', () => {
  const pacientePageUrl = '/paciente';
  const pacientePageUrlPattern = new RegExp('/paciente(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const pacienteSample = { nombre: 'invoice interface' };

  let paciente: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/pacientes/api/pacientes').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/pacientes/api/pacientes+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/pacientes/api/pacientes').as('postEntityRequest');
    cy.intercept('DELETE', '/services/pacientes/api/pacientes/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (paciente) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/pacientes/api/pacientes/${paciente.id}`,
      }).then(() => {
        paciente = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Pacientes menu should load Pacientes page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('paciente');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Paciente').should('exist');
    cy.url().should('match', pacientePageUrlPattern);
  });

  describe('Paciente page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(pacientePageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Paciente page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/paciente/new$'));
        cy.getEntityCreateUpdateHeading('Paciente');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', pacientePageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/pacientes/api/pacientes',
          body: pacienteSample,
        }).then(({ body }) => {
          paciente = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/pacientes/api/pacientes+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [paciente],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(pacientePageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Paciente page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('paciente');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', pacientePageUrlPattern);
      });

      it('edit button click should load edit Paciente page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Paciente');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', pacientePageUrlPattern);
      });

      it('last delete button click should delete instance of Paciente', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('paciente').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', pacientePageUrlPattern);

        paciente = undefined;
      });
    });
  });

  describe('new Paciente page', () => {
    beforeEach(() => {
      cy.visit(`${pacientePageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Paciente');
    });

    it('should create an instance of Paciente', () => {
      cy.get(`[data-cy="nombre"]`).type('bypassing synthesizing Mesa').should('have.value', 'bypassing synthesizing Mesa');

      cy.get(`[data-cy="tipoIdentificacion"]`).select('IDENTIDAD');

      cy.get(`[data-cy="identificacion"]`).type('52288').should('have.value', '52288');

      cy.get(`[data-cy="edad"]`).type('39923').should('have.value', '39923');

      cy.get(`[data-cy="sexo"]`).select('FEMENINO');

      cy.get(`[data-cy="telefono"]`).type('12037').should('have.value', '12037');

      cy.get(`[data-cy="direccion"]`).type('Cambridgeshire Pescado').should('have.value', 'Cambridgeshire Pescado');

      cy.get(`[data-cy="pesoKG"]`).type('57499').should('have.value', '57499');

      cy.get(`[data-cy="estaturaCM"]`).type('52933').should('have.value', '52933');

      cy.get(`[data-cy="grupoEtnico"]`).select('Indigena');

      cy.get(`[data-cy="estratoSocioeconomico"]`).select('Estrato_3');

      cy.get(`[data-cy="oximetriaReferencia"]`).type('26286').should('have.value', '26286');

      cy.get(`[data-cy="ritmoCardiacoReferencia"]`).type('42323').should('have.value', '42323');

      cy.get(`[data-cy="presionSistolicaReferencia"]`).type('33381').should('have.value', '33381');

      cy.get(`[data-cy="presionDistolicaReferencia"]`).type('33820').should('have.value', '33820');

      cy.get(`[data-cy="comentarios"]`)
        .type('../fake-data/blob/hipster.txt')
        .invoke('val')
        .should('match', new RegExp('../fake-data/blob/hipster.txt'));

      cy.get(`[data-cy="pasosReferencia"]`).type('50159').should('have.value', '50159');

      cy.get(`[data-cy="caloriasReferencia"]`).type('29490').should('have.value', '29490');

      cy.get(`[data-cy="metaReferencia"]`).type('Guapa').should('have.value', 'Guapa');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        paciente = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', pacientePageUrlPattern);
    });
  });
});
