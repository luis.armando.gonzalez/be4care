import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Vacuna e2e test', () => {
  const vacunaPageUrl = '/vacuna';
  const vacunaPageUrlPattern = new RegExp('/vacuna(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const vacunaSample = { nombreVacuna: 'Coordinador Franc Buckinghamshire' };

  let vacuna: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/biologico/api/vacunas').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/biologico/api/vacunas+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/biologico/api/vacunas').as('postEntityRequest');
    cy.intercept('DELETE', '/services/biologico/api/vacunas/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (vacuna) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/biologico/api/vacunas/${vacuna.id}`,
      }).then(() => {
        vacuna = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Vacunas menu should load Vacunas page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('vacuna');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Vacuna').should('exist');
    cy.url().should('match', vacunaPageUrlPattern);
  });

  describe('Vacuna page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(vacunaPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Vacuna page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/vacuna/new$'));
        cy.getEntityCreateUpdateHeading('Vacuna');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', vacunaPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/biologico/api/vacunas',
          body: vacunaSample,
        }).then(({ body }) => {
          vacuna = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/biologico/api/vacunas+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [vacuna],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(vacunaPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Vacuna page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('vacuna');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', vacunaPageUrlPattern);
      });

      it('edit button click should load edit Vacuna page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Vacuna');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', vacunaPageUrlPattern);
      });

      it('last delete button click should delete instance of Vacuna', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('vacuna').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', vacunaPageUrlPattern);

        vacuna = undefined;
      });
    });
  });

  describe('new Vacuna page', () => {
    beforeEach(() => {
      cy.visit(`${vacunaPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Vacuna');
    });

    it('should create an instance of Vacuna', () => {
      cy.get(`[data-cy="nombreVacuna"]`).type('Metical').should('have.value', 'Metical');

      cy.get(`[data-cy="tipoVacuna"]`).type('partnerships synthesize').should('have.value', 'partnerships synthesize');

      cy.get(`[data-cy="descripcionVacuna"]`).type('Aragón Pakistan').should('have.value', 'Aragón Pakistan');

      cy.get(`[data-cy="fechaIngreso"]`).type('2021-11-11T10:17').should('have.value', '2021-11-11T10:17');

      cy.get(`[data-cy="loteVacuna"]`).type('viral Bricolaje').should('have.value', 'viral Bricolaje');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        vacuna = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', vacunaPageUrlPattern);
    });
  });
});
