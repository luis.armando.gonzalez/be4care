import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Agenda e2e test', () => {
  const agendaPageUrl = '/agenda';
  const agendaPageUrlPattern = new RegExp('/agenda(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const agendaSample = { horaMedicamento: 75046 };

  let agenda: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/pacientes/api/agenda').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/pacientes/api/agenda+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/pacientes/api/agenda').as('postEntityRequest');
    cy.intercept('DELETE', '/services/pacientes/api/agenda/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (agenda) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/pacientes/api/agenda/${agenda.id}`,
      }).then(() => {
        agenda = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Agenda menu should load Agenda page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('agenda');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Agenda').should('exist');
    cy.url().should('match', agendaPageUrlPattern);
  });

  describe('Agenda page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(agendaPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Agenda page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/agenda/new$'));
        cy.getEntityCreateUpdateHeading('Agenda');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', agendaPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/pacientes/api/agenda',
          body: agendaSample,
        }).then(({ body }) => {
          agenda = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/pacientes/api/agenda+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [agenda],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(agendaPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Agenda page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('agenda');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', agendaPageUrlPattern);
      });

      it('edit button click should load edit Agenda page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Agenda');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', agendaPageUrlPattern);
      });

      it('last delete button click should delete instance of Agenda', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('agenda').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', agendaPageUrlPattern);

        agenda = undefined;
      });
    });
  });

  describe('new Agenda page', () => {
    beforeEach(() => {
      cy.visit(`${agendaPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Agenda');
    });

    it('should create an instance of Agenda', () => {
      cy.get(`[data-cy="horaMedicamento"]`).type('59622').should('have.value', '59622');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        agenda = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', agendaPageUrlPattern);
    });
  });
});
