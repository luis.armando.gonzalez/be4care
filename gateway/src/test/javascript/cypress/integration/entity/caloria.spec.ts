import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Caloria e2e test', () => {
  const caloriaPageUrl = '/caloria';
  const caloriaPageUrlPattern = new RegExp('/caloria(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const caloriaSample = { caloriasActivas: 38702 };

  let caloria: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/manilla/api/calorias').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/manilla/api/calorias+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/manilla/api/calorias').as('postEntityRequest');
    cy.intercept('DELETE', '/services/manilla/api/calorias/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (caloria) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/manilla/api/calorias/${caloria.id}`,
      }).then(() => {
        caloria = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Calorias menu should load Calorias page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('caloria');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Caloria').should('exist');
    cy.url().should('match', caloriaPageUrlPattern);
  });

  describe('Caloria page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(caloriaPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Caloria page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/caloria/new$'));
        cy.getEntityCreateUpdateHeading('Caloria');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', caloriaPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/manilla/api/calorias',
          body: caloriaSample,
        }).then(({ body }) => {
          caloria = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/manilla/api/calorias+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [caloria],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(caloriaPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Caloria page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('caloria');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', caloriaPageUrlPattern);
      });

      it('edit button click should load edit Caloria page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Caloria');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', caloriaPageUrlPattern);
      });

      it('last delete button click should delete instance of Caloria', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('caloria').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', caloriaPageUrlPattern);

        caloria = undefined;
      });
    });
  });

  describe('new Caloria page', () => {
    beforeEach(() => {
      cy.visit(`${caloriaPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Caloria');
    });

    it('should create an instance of Caloria', () => {
      cy.get(`[data-cy="caloriasActivas"]`).type('34457').should('have.value', '34457');

      cy.get(`[data-cy="descripcion"]`).type('Account').should('have.value', 'Account');

      cy.get(`[data-cy="fechaRegistro"]`).type('2021-11-11T07:19').should('have.value', '2021-11-11T07:19');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        caloria = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', caloriaPageUrlPattern);
    });
  });
});
