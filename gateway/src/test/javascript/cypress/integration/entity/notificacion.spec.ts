import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Notificacion e2e test', () => {
  const notificacionPageUrl = '/notificacion';
  const notificacionPageUrlPattern = new RegExp('/notificacion(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const notificacionSample = { fechaInicio: '2021-11-10T23:12:57.821Z' };

  let notificacion: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/manilla/api/notificacions').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/manilla/api/notificacions+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/manilla/api/notificacions').as('postEntityRequest');
    cy.intercept('DELETE', '/services/manilla/api/notificacions/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (notificacion) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/manilla/api/notificacions/${notificacion.id}`,
      }).then(() => {
        notificacion = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Notificacions menu should load Notificacions page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('notificacion');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Notificacion').should('exist');
    cy.url().should('match', notificacionPageUrlPattern);
  });

  describe('Notificacion page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(notificacionPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Notificacion page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/notificacion/new$'));
        cy.getEntityCreateUpdateHeading('Notificacion');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', notificacionPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/manilla/api/notificacions',
          body: notificacionSample,
        }).then(({ body }) => {
          notificacion = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/manilla/api/notificacions+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [notificacion],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(notificacionPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Notificacion page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('notificacion');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', notificacionPageUrlPattern);
      });

      it('edit button click should load edit Notificacion page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Notificacion');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', notificacionPageUrlPattern);
      });

      it('last delete button click should delete instance of Notificacion', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('notificacion').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', notificacionPageUrlPattern);

        notificacion = undefined;
      });
    });
  });

  describe('new Notificacion page', () => {
    beforeEach(() => {
      cy.visit(`${notificacionPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Notificacion');
    });

    it('should create an instance of Notificacion', () => {
      cy.get(`[data-cy="fechaInicio"]`).type('2021-11-10T21:26').should('have.value', '2021-11-10T21:26');

      cy.get(`[data-cy="fechaActualizacion"]`).type('2021-11-11T12:22').should('have.value', '2021-11-11T12:22');

      cy.get(`[data-cy="estado"]`).type('31552').should('have.value', '31552');

      cy.get(`[data-cy="tipoNotificacion"]`).type('58764').should('have.value', '58764');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        notificacion = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', notificacionPageUrlPattern);
    });
  });
});
