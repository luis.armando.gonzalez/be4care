import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Fisiometria2 e2e test', () => {
  const fisiometria2PageUrl = '/fisiometria-2';
  const fisiometria2PageUrlPattern = new RegExp('/fisiometria-2(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const fisiometria2Sample = { timeInstant: '2021-11-11T10:45:52.040Z' };

  let fisiometria2: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/diadema/api/fisiometria-2-s').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/diadema/api/fisiometria-2-s+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/diadema/api/fisiometria-2-s').as('postEntityRequest');
    cy.intercept('DELETE', '/services/diadema/api/fisiometria-2-s/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (fisiometria2) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/diadema/api/fisiometria-2-s/${fisiometria2.id}`,
      }).then(() => {
        fisiometria2 = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Fisiometria2s menu should load Fisiometria2s page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('fisiometria-2');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Fisiometria2').should('exist');
    cy.url().should('match', fisiometria2PageUrlPattern);
  });

  describe('Fisiometria2 page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(fisiometria2PageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Fisiometria2 page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/fisiometria-2/new$'));
        cy.getEntityCreateUpdateHeading('Fisiometria2');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', fisiometria2PageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/diadema/api/fisiometria-2-s',
          body: fisiometria2Sample,
        }).then(({ body }) => {
          fisiometria2 = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/diadema/api/fisiometria-2-s+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [fisiometria2],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(fisiometria2PageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Fisiometria2 page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('fisiometria2');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', fisiometria2PageUrlPattern);
      });

      it('edit button click should load edit Fisiometria2 page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Fisiometria2');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', fisiometria2PageUrlPattern);
      });

      it('last delete button click should delete instance of Fisiometria2', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('fisiometria2').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', fisiometria2PageUrlPattern);

        fisiometria2 = undefined;
      });
    });
  });

  describe('new Fisiometria2 page', () => {
    beforeEach(() => {
      cy.visit(`${fisiometria2PageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Fisiometria2');
    });

    it('should create an instance of Fisiometria2', () => {
      cy.get(`[data-cy="timeInstant"]`).type('2021-11-10T22:05').should('have.value', '2021-11-10T22:05');

      cy.get(`[data-cy="meditacion"]`).type('8175').should('have.value', '8175');

      cy.get(`[data-cy="attention"]`).type('38964').should('have.value', '38964');

      cy.get(`[data-cy="delta"]`).type('82552').should('have.value', '82552');

      cy.get(`[data-cy="theta"]`).type('52648').should('have.value', '52648');

      cy.get(`[data-cy="lowAlpha"]`).type('7120').should('have.value', '7120');

      cy.get(`[data-cy="highAlpha"]`).type('43712').should('have.value', '43712');

      cy.get(`[data-cy="lowBeta"]`).type('3838').should('have.value', '3838');

      cy.get(`[data-cy="highBeta"]`).type('14069').should('have.value', '14069');

      cy.get(`[data-cy="lowGamma"]`).type('54992').should('have.value', '54992');

      cy.get(`[data-cy="midGamma"]`).type('63496').should('have.value', '63496');

      cy.get(`[data-cy="pulso"]`).type('15329').should('have.value', '15329');

      cy.get(`[data-cy="respiracion"]`).type('93462').should('have.value', '93462');

      cy.get(`[data-cy="acelerometro"]`).type('90357').should('have.value', '90357');

      cy.get(`[data-cy="estadoFuncional"]`).type('19549').should('have.value', '19549');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        fisiometria2 = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', fisiometria2PageUrlPattern);
    });
  });
});
