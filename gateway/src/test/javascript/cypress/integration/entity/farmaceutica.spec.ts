import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Farmaceutica e2e test', () => {
  const farmaceuticaPageUrl = '/farmaceutica';
  const farmaceuticaPageUrlPattern = new RegExp('/farmaceutica(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const farmaceuticaSample = { nombre: 'Travesía Manat' };

  let farmaceutica: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/pacientes/api/farmaceuticas').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/pacientes/api/farmaceuticas+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/pacientes/api/farmaceuticas').as('postEntityRequest');
    cy.intercept('DELETE', '/services/pacientes/api/farmaceuticas/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (farmaceutica) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/pacientes/api/farmaceuticas/${farmaceutica.id}`,
      }).then(() => {
        farmaceutica = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Farmaceuticas menu should load Farmaceuticas page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('farmaceutica');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Farmaceutica').should('exist');
    cy.url().should('match', farmaceuticaPageUrlPattern);
  });

  describe('Farmaceutica page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(farmaceuticaPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Farmaceutica page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/farmaceutica/new$'));
        cy.getEntityCreateUpdateHeading('Farmaceutica');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', farmaceuticaPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/pacientes/api/farmaceuticas',
          body: farmaceuticaSample,
        }).then(({ body }) => {
          farmaceutica = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/pacientes/api/farmaceuticas+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [farmaceutica],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(farmaceuticaPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Farmaceutica page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('farmaceutica');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', farmaceuticaPageUrlPattern);
      });

      it('edit button click should load edit Farmaceutica page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Farmaceutica');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', farmaceuticaPageUrlPattern);
      });

      it('last delete button click should delete instance of Farmaceutica', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('farmaceutica').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', farmaceuticaPageUrlPattern);

        farmaceutica = undefined;
      });
    });
  });

  describe('new Farmaceutica page', () => {
    beforeEach(() => {
      cy.visit(`${farmaceuticaPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Farmaceutica');
    });

    it('should create an instance of Farmaceutica', () => {
      cy.get(`[data-cy="nombre"]`).type('FTP').should('have.value', 'FTP');

      cy.get(`[data-cy="direccion"]`).type('program Marca modular').should('have.value', 'program Marca modular');

      cy.get(`[data-cy="propietario"]`).type('Director Marketing Plástico').should('have.value', 'Director Marketing Plástico');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        farmaceutica = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', farmaceuticaPageUrlPattern);
    });
  });
});
