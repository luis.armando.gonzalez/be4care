import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Glucosa e2e test', () => {
  const glucosaPageUrl = '/glucosa';
  const glucosaPageUrlPattern = new RegExp('/glucosa(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const glucosaSample = { glucosaUser: 67222 };

  let glucosa: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/expedientemanual/api/glucosas').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/expedientemanual/api/glucosas+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/expedientemanual/api/glucosas').as('postEntityRequest');
    cy.intercept('DELETE', '/services/expedientemanual/api/glucosas/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (glucosa) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/expedientemanual/api/glucosas/${glucosa.id}`,
      }).then(() => {
        glucosa = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Glucosas menu should load Glucosas page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('glucosa');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Glucosa').should('exist');
    cy.url().should('match', glucosaPageUrlPattern);
  });

  describe('Glucosa page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(glucosaPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Glucosa page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/glucosa/new$'));
        cy.getEntityCreateUpdateHeading('Glucosa');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', glucosaPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/expedientemanual/api/glucosas',
          body: glucosaSample,
        }).then(({ body }) => {
          glucosa = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/expedientemanual/api/glucosas+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [glucosa],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(glucosaPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Glucosa page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('glucosa');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', glucosaPageUrlPattern);
      });

      it('edit button click should load edit Glucosa page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Glucosa');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', glucosaPageUrlPattern);
      });

      it('last delete button click should delete instance of Glucosa', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('glucosa').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', glucosaPageUrlPattern);

        glucosa = undefined;
      });
    });
  });

  describe('new Glucosa page', () => {
    beforeEach(() => {
      cy.visit(`${glucosaPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Glucosa');
    });

    it('should create an instance of Glucosa', () => {
      cy.get(`[data-cy="glucosaUser"]`).type('88479').should('have.value', '88479');

      cy.get(`[data-cy="fechaRegistro"]`).type('2021-11-11T07:39').should('have.value', '2021-11-11T07:39');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        glucosa = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', glucosaPageUrlPattern);
    });
  });
});
