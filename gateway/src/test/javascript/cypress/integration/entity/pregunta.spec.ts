import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Pregunta e2e test', () => {
  const preguntaPageUrl = '/pregunta';
  const preguntaPageUrlPattern = new RegExp('/pregunta(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const preguntaSample = { pregunta: 'División' };

  let pregunta: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/pacientes/api/preguntas').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/pacientes/api/preguntas+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/pacientes/api/preguntas').as('postEntityRequest');
    cy.intercept('DELETE', '/services/pacientes/api/preguntas/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (pregunta) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/pacientes/api/preguntas/${pregunta.id}`,
      }).then(() => {
        pregunta = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Preguntas menu should load Preguntas page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('pregunta');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Pregunta').should('exist');
    cy.url().should('match', preguntaPageUrlPattern);
  });

  describe('Pregunta page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(preguntaPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Pregunta page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/pregunta/new$'));
        cy.getEntityCreateUpdateHeading('Pregunta');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', preguntaPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/pacientes/api/preguntas',
          body: preguntaSample,
        }).then(({ body }) => {
          pregunta = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/pacientes/api/preguntas+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [pregunta],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(preguntaPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Pregunta page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('pregunta');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', preguntaPageUrlPattern);
      });

      it('edit button click should load edit Pregunta page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Pregunta');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', preguntaPageUrlPattern);
      });

      it('last delete button click should delete instance of Pregunta', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('pregunta').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', preguntaPageUrlPattern);

        pregunta = undefined;
      });
    });
  });

  describe('new Pregunta page', () => {
    beforeEach(() => {
      cy.visit(`${preguntaPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Pregunta');
    });

    it('should create an instance of Pregunta', () => {
      cy.get(`[data-cy="pregunta"]`).type('Ensalada XML grow').should('have.value', 'Ensalada XML grow');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        pregunta = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', preguntaPageUrlPattern);
    });
  });
});
