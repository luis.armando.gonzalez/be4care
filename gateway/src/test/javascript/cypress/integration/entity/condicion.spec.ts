import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Condicion e2e test', () => {
  const condicionPageUrl = '/condicion';
  const condicionPageUrlPattern = new RegExp('/condicion(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const condicionSample = { condicion: 'enfoque Burundi Negro' };

  let condicion: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/pacientes/api/condicions').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/pacientes/api/condicions+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/pacientes/api/condicions').as('postEntityRequest');
    cy.intercept('DELETE', '/services/pacientes/api/condicions/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (condicion) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/pacientes/api/condicions/${condicion.id}`,
      }).then(() => {
        condicion = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Condicions menu should load Condicions page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('condicion');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Condicion').should('exist');
    cy.url().should('match', condicionPageUrlPattern);
  });

  describe('Condicion page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(condicionPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Condicion page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/condicion/new$'));
        cy.getEntityCreateUpdateHeading('Condicion');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', condicionPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/pacientes/api/condicions',
          body: condicionSample,
        }).then(({ body }) => {
          condicion = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/pacientes/api/condicions+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [condicion],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(condicionPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Condicion page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('condicion');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', condicionPageUrlPattern);
      });

      it('edit button click should load edit Condicion page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Condicion');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', condicionPageUrlPattern);
      });

      it('last delete button click should delete instance of Condicion', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('condicion').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', condicionPageUrlPattern);

        condicion = undefined;
      });
    });
  });

  describe('new Condicion page', () => {
    beforeEach(() => {
      cy.visit(`${condicionPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Condicion');
    });

    it('should create an instance of Condicion', () => {
      cy.get(`[data-cy="condicion"]`).type('aprovechar').should('have.value', 'aprovechar');

      cy.get(`[data-cy="descripcion"]`)
        .type('../fake-data/blob/hipster.txt')
        .invoke('val')
        .should('match', new RegExp('../fake-data/blob/hipster.txt'));

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        condicion = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', condicionPageUrlPattern);
    });
  });
});
