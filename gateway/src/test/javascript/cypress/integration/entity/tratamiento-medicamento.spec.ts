import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('TratamientoMedicamento e2e test', () => {
  const tratamientoMedicamentoPageUrl = '/tratamiento-medicamento';
  const tratamientoMedicamentoPageUrlPattern = new RegExp('/tratamiento-medicamento(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const tratamientoMedicamentoSample = { dosis: 'Metal Director circuit' };

  let tratamientoMedicamento: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/pacientes/api/tratamiento-medicamentos').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/pacientes/api/tratamiento-medicamentos+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/pacientes/api/tratamiento-medicamentos').as('postEntityRequest');
    cy.intercept('DELETE', '/services/pacientes/api/tratamiento-medicamentos/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (tratamientoMedicamento) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/pacientes/api/tratamiento-medicamentos/${tratamientoMedicamento.id}`,
      }).then(() => {
        tratamientoMedicamento = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('TratamientoMedicamentos menu should load TratamientoMedicamentos page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('tratamiento-medicamento');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('TratamientoMedicamento').should('exist');
    cy.url().should('match', tratamientoMedicamentoPageUrlPattern);
  });

  describe('TratamientoMedicamento page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(tratamientoMedicamentoPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create TratamientoMedicamento page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/tratamiento-medicamento/new$'));
        cy.getEntityCreateUpdateHeading('TratamientoMedicamento');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', tratamientoMedicamentoPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/pacientes/api/tratamiento-medicamentos',
          body: tratamientoMedicamentoSample,
        }).then(({ body }) => {
          tratamientoMedicamento = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/pacientes/api/tratamiento-medicamentos+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [tratamientoMedicamento],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(tratamientoMedicamentoPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details TratamientoMedicamento page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('tratamientoMedicamento');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', tratamientoMedicamentoPageUrlPattern);
      });

      it('edit button click should load edit TratamientoMedicamento page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('TratamientoMedicamento');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', tratamientoMedicamentoPageUrlPattern);
      });

      it('last delete button click should delete instance of TratamientoMedicamento', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('tratamientoMedicamento').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', tratamientoMedicamentoPageUrlPattern);

        tratamientoMedicamento = undefined;
      });
    });
  });

  describe('new TratamientoMedicamento page', () => {
    beforeEach(() => {
      cy.visit(`${tratamientoMedicamentoPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('TratamientoMedicamento');
    });

    it('should create an instance of TratamientoMedicamento', () => {
      cy.get(`[data-cy="dosis"]`).type('Avenida Genérico cross-media').should('have.value', 'Avenida Genérico cross-media');

      cy.get(`[data-cy="intensidad"]`).type('Interfaz Argelia').should('have.value', 'Interfaz Argelia');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        tratamientoMedicamento = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', tratamientoMedicamentoPageUrlPattern);
    });
  });
});
