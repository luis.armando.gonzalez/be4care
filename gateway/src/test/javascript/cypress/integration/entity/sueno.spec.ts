import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Sueno e2e test', () => {
  const suenoPageUrl = '/sueno';
  const suenoPageUrlPattern = new RegExp('/sueno(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const suenoSample = { superficial: 56119 };

  let sueno: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/manilla/api/suenos').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/manilla/api/suenos+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/manilla/api/suenos').as('postEntityRequest');
    cy.intercept('DELETE', '/services/manilla/api/suenos/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (sueno) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/manilla/api/suenos/${sueno.id}`,
      }).then(() => {
        sueno = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Suenos menu should load Suenos page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('sueno');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Sueno').should('exist');
    cy.url().should('match', suenoPageUrlPattern);
  });

  describe('Sueno page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(suenoPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Sueno page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/sueno/new$'));
        cy.getEntityCreateUpdateHeading('Sueno');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', suenoPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/manilla/api/suenos',
          body: suenoSample,
        }).then(({ body }) => {
          sueno = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/manilla/api/suenos+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [sueno],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(suenoPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Sueno page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('sueno');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', suenoPageUrlPattern);
      });

      it('edit button click should load edit Sueno page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Sueno');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', suenoPageUrlPattern);
      });

      it('last delete button click should delete instance of Sueno', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('sueno').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', suenoPageUrlPattern);

        sueno = undefined;
      });
    });
  });

  describe('new Sueno page', () => {
    beforeEach(() => {
      cy.visit(`${suenoPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Sueno');
    });

    it('should create an instance of Sueno', () => {
      cy.get(`[data-cy="superficial"]`).type('75426').should('have.value', '75426');

      cy.get(`[data-cy="profundo"]`).type('13124').should('have.value', '13124');

      cy.get(`[data-cy="despierto"]`).type('36406').should('have.value', '36406');

      cy.get(`[data-cy="timeInstant"]`).type('2021-11-11T16:00').should('have.value', '2021-11-11T16:00');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        sueno = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', suenoPageUrlPattern);
    });
  });
});
