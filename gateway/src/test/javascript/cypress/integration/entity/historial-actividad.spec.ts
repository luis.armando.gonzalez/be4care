import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('HistorialActividad e2e test', () => {
  const historialActividadPageUrl = '/historial-actividad';
  const historialActividadPageUrlPattern = new RegExp('/historial-actividad(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const historialActividadSample = { fechaHistorial: '2021-11-11T13:33:11.318Z' };

  let historialActividad: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/biologico/api/historial-actividads').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/biologico/api/historial-actividads+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/biologico/api/historial-actividads').as('postEntityRequest');
    cy.intercept('DELETE', '/services/biologico/api/historial-actividads/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (historialActividad) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/biologico/api/historial-actividads/${historialActividad.id}`,
      }).then(() => {
        historialActividad = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('HistorialActividads menu should load HistorialActividads page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('historial-actividad');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('HistorialActividad').should('exist');
    cy.url().should('match', historialActividadPageUrlPattern);
  });

  describe('HistorialActividad page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(historialActividadPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create HistorialActividad page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/historial-actividad/new$'));
        cy.getEntityCreateUpdateHeading('HistorialActividad');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', historialActividadPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/biologico/api/historial-actividads',
          body: historialActividadSample,
        }).then(({ body }) => {
          historialActividad = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/biologico/api/historial-actividads+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [historialActividad],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(historialActividadPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details HistorialActividad page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('historialActividad');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', historialActividadPageUrlPattern);
      });

      it('edit button click should load edit HistorialActividad page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('HistorialActividad');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', historialActividadPageUrlPattern);
      });

      it('last delete button click should delete instance of HistorialActividad', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('historialActividad').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', historialActividadPageUrlPattern);

        historialActividad = undefined;
      });
    });
  });

  describe('new HistorialActividad page', () => {
    beforeEach(() => {
      cy.visit(`${historialActividadPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('HistorialActividad');
    });

    it('should create an instance of HistorialActividad', () => {
      cy.get(`[data-cy="fechaHistorial"]`).type('2021-11-10T23:45').should('have.value', '2021-11-10T23:45');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        historialActividad = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', historialActividadPageUrlPattern);
    });
  });
});
