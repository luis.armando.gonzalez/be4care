import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Pais e2e test', () => {
  const paisPageUrl = '/pais';
  const paisPageUrlPattern = new RegExp('/pais(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const paisSample = { pais: 'Coordinador disintermediate' };

  let pais: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/pacientes/api/pais').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/pacientes/api/pais+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/pacientes/api/pais').as('postEntityRequest');
    cy.intercept('DELETE', '/services/pacientes/api/pais/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (pais) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/pacientes/api/pais/${pais.id}`,
      }).then(() => {
        pais = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Pais menu should load Pais page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('pais');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Pais').should('exist');
    cy.url().should('match', paisPageUrlPattern);
  });

  describe('Pais page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(paisPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Pais page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/pais/new$'));
        cy.getEntityCreateUpdateHeading('Pais');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', paisPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/pacientes/api/pais',
          body: paisSample,
        }).then(({ body }) => {
          pais = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/pacientes/api/pais+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [pais],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(paisPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Pais page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('pais');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', paisPageUrlPattern);
      });

      it('edit button click should load edit Pais page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Pais');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', paisPageUrlPattern);
      });

      it('last delete button click should delete instance of Pais', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('pais').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', paisPageUrlPattern);

        pais = undefined;
      });
    });
  });

  describe('new Pais page', () => {
    beforeEach(() => {
      cy.visit(`${paisPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Pais');
    });

    it('should create an instance of Pais', () => {
      cy.get(`[data-cy="pais"]`).type('Corporativo Sorprendente quantify').should('have.value', 'Corporativo Sorprendente quantify');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        pais = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', paisPageUrlPattern);
    });
  });
});
