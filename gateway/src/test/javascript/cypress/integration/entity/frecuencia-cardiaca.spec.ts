import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('FrecuenciaCardiaca e2e test', () => {
  const frecuenciaCardiacaPageUrl = '/frecuencia-cardiaca';
  const frecuenciaCardiacaPageUrlPattern = new RegExp('/frecuencia-cardiaca(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const frecuenciaCardiacaSample = { frecuenciaCardiaca: 46930 };

  let frecuenciaCardiaca: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/manilla/api/frecuencia-cardiacas').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/manilla/api/frecuencia-cardiacas+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/manilla/api/frecuencia-cardiacas').as('postEntityRequest');
    cy.intercept('DELETE', '/services/manilla/api/frecuencia-cardiacas/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (frecuenciaCardiaca) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/manilla/api/frecuencia-cardiacas/${frecuenciaCardiaca.id}`,
      }).then(() => {
        frecuenciaCardiaca = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('FrecuenciaCardiacas menu should load FrecuenciaCardiacas page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('frecuencia-cardiaca');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('FrecuenciaCardiaca').should('exist');
    cy.url().should('match', frecuenciaCardiacaPageUrlPattern);
  });

  describe('FrecuenciaCardiaca page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(frecuenciaCardiacaPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create FrecuenciaCardiaca page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/frecuencia-cardiaca/new$'));
        cy.getEntityCreateUpdateHeading('FrecuenciaCardiaca');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', frecuenciaCardiacaPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/manilla/api/frecuencia-cardiacas',
          body: frecuenciaCardiacaSample,
        }).then(({ body }) => {
          frecuenciaCardiaca = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/manilla/api/frecuencia-cardiacas+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [frecuenciaCardiaca],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(frecuenciaCardiacaPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details FrecuenciaCardiaca page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('frecuenciaCardiaca');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', frecuenciaCardiacaPageUrlPattern);
      });

      it('edit button click should load edit FrecuenciaCardiaca page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('FrecuenciaCardiaca');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', frecuenciaCardiacaPageUrlPattern);
      });

      it('last delete button click should delete instance of FrecuenciaCardiaca', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('frecuenciaCardiaca').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', frecuenciaCardiacaPageUrlPattern);

        frecuenciaCardiaca = undefined;
      });
    });
  });

  describe('new FrecuenciaCardiaca page', () => {
    beforeEach(() => {
      cy.visit(`${frecuenciaCardiacaPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('FrecuenciaCardiaca');
    });

    it('should create an instance of FrecuenciaCardiaca', () => {
      cy.get(`[data-cy="frecuenciaCardiaca"]`).type('10155').should('have.value', '10155');

      cy.get(`[data-cy="fechaRegistro"]`).type('2021-11-11T07:11').should('have.value', '2021-11-11T07:11');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        frecuenciaCardiaca = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', frecuenciaCardiacaPageUrlPattern);
    });
  });
});
