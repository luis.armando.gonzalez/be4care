import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Oximetria e2e test', () => {
  const oximetriaPageUrl = '/oximetria';
  const oximetriaPageUrlPattern = new RegExp('/oximetria(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const oximetriaSample = { oximetria: 68916 };

  let oximetria: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/manilla/api/oximetrias').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/manilla/api/oximetrias+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/manilla/api/oximetrias').as('postEntityRequest');
    cy.intercept('DELETE', '/services/manilla/api/oximetrias/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (oximetria) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/manilla/api/oximetrias/${oximetria.id}`,
      }).then(() => {
        oximetria = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Oximetrias menu should load Oximetrias page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('oximetria');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Oximetria').should('exist');
    cy.url().should('match', oximetriaPageUrlPattern);
  });

  describe('Oximetria page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(oximetriaPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Oximetria page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/oximetria/new$'));
        cy.getEntityCreateUpdateHeading('Oximetria');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', oximetriaPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/manilla/api/oximetrias',
          body: oximetriaSample,
        }).then(({ body }) => {
          oximetria = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/manilla/api/oximetrias+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [oximetria],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(oximetriaPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Oximetria page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('oximetria');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', oximetriaPageUrlPattern);
      });

      it('edit button click should load edit Oximetria page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Oximetria');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', oximetriaPageUrlPattern);
      });

      it('last delete button click should delete instance of Oximetria', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('oximetria').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', oximetriaPageUrlPattern);

        oximetria = undefined;
      });
    });
  });

  describe('new Oximetria page', () => {
    beforeEach(() => {
      cy.visit(`${oximetriaPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Oximetria');
    });

    it('should create an instance of Oximetria', () => {
      cy.get(`[data-cy="oximetria"]`).type('94294').should('have.value', '94294');

      cy.get(`[data-cy="fechaRegistro"]`).type('2021-11-11T04:08').should('have.value', '2021-11-11T04:08');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        oximetria = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', oximetriaPageUrlPattern);
    });
  });
});
