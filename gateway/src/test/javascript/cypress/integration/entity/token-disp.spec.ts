import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('TokenDisp e2e test', () => {
  const tokenDispPageUrl = '/token-disp';
  const tokenDispPageUrlPattern = new RegExp('/token-disp(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const tokenDispSample = { tokenConexion: 'Granito' };

  let tokenDisp: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/manilla/api/token-disps').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/manilla/api/token-disps+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/manilla/api/token-disps').as('postEntityRequest');
    cy.intercept('DELETE', '/services/manilla/api/token-disps/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (tokenDisp) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/manilla/api/token-disps/${tokenDisp.id}`,
      }).then(() => {
        tokenDisp = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('TokenDisps menu should load TokenDisps page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('token-disp');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('TokenDisp').should('exist');
    cy.url().should('match', tokenDispPageUrlPattern);
  });

  describe('TokenDisp page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(tokenDispPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create TokenDisp page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/token-disp/new$'));
        cy.getEntityCreateUpdateHeading('TokenDisp');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', tokenDispPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/manilla/api/token-disps',
          body: tokenDispSample,
        }).then(({ body }) => {
          tokenDisp = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/manilla/api/token-disps+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [tokenDisp],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(tokenDispPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details TokenDisp page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('tokenDisp');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', tokenDispPageUrlPattern);
      });

      it('edit button click should load edit TokenDisp page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('TokenDisp');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', tokenDispPageUrlPattern);
      });

      it('last delete button click should delete instance of TokenDisp', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('tokenDisp').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', tokenDispPageUrlPattern);

        tokenDisp = undefined;
      });
    });
  });

  describe('new TokenDisp page', () => {
    beforeEach(() => {
      cy.visit(`${tokenDispPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('TokenDisp');
    });

    it('should create an instance of TokenDisp', () => {
      cy.get(`[data-cy="tokenConexion"]`).type('Suecia Guantes').should('have.value', 'Suecia Guantes');

      cy.get(`[data-cy="activo"]`).should('not.be.checked');
      cy.get(`[data-cy="activo"]`).click().should('be.checked');

      cy.get(`[data-cy="fechaInicio"]`).type('2021-11-11T03:33').should('have.value', '2021-11-11T03:33');

      cy.get(`[data-cy="fechaFin"]`).type('2021-11-10T18:12').should('have.value', '2021-11-10T18:12');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        tokenDisp = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', tokenDispPageUrlPattern);
    });
  });
});
