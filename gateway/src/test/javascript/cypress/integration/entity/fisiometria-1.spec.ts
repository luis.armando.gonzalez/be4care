import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Fisiometria1 e2e test', () => {
  const fisiometria1PageUrl = '/fisiometria-1';
  const fisiometria1PageUrlPattern = new RegExp('/fisiometria-1(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const fisiometria1Sample = { ritmoCardiaco: 58018 };

  let fisiometria1: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/manilla/api/fisiometria-1-s').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/manilla/api/fisiometria-1-s+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/manilla/api/fisiometria-1-s').as('postEntityRequest');
    cy.intercept('DELETE', '/services/manilla/api/fisiometria-1-s/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (fisiometria1) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/manilla/api/fisiometria-1-s/${fisiometria1.id}`,
      }).then(() => {
        fisiometria1 = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Fisiometria1s menu should load Fisiometria1s page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('fisiometria-1');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Fisiometria1').should('exist');
    cy.url().should('match', fisiometria1PageUrlPattern);
  });

  describe('Fisiometria1 page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(fisiometria1PageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Fisiometria1 page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/fisiometria-1/new$'));
        cy.getEntityCreateUpdateHeading('Fisiometria1');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', fisiometria1PageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/manilla/api/fisiometria-1-s',
          body: fisiometria1Sample,
        }).then(({ body }) => {
          fisiometria1 = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/manilla/api/fisiometria-1-s+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [fisiometria1],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(fisiometria1PageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Fisiometria1 page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('fisiometria1');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', fisiometria1PageUrlPattern);
      });

      it('edit button click should load edit Fisiometria1 page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Fisiometria1');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', fisiometria1PageUrlPattern);
      });

      it('last delete button click should delete instance of Fisiometria1', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('fisiometria1').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', fisiometria1PageUrlPattern);

        fisiometria1 = undefined;
      });
    });
  });

  describe('new Fisiometria1 page', () => {
    beforeEach(() => {
      cy.visit(`${fisiometria1PageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Fisiometria1');
    });

    it('should create an instance of Fisiometria1', () => {
      cy.get(`[data-cy="ritmoCardiaco"]`).type('40876').should('have.value', '40876');

      cy.get(`[data-cy="ritmoRespiratorio"]`).type('35110').should('have.value', '35110');

      cy.get(`[data-cy="oximetria"]`).type('70764').should('have.value', '70764');

      cy.get(`[data-cy="presionArterialSistolica"]`).type('29184').should('have.value', '29184');

      cy.get(`[data-cy="presionArterialDiastolica"]`).type('77757').should('have.value', '77757');

      cy.get(`[data-cy="temperatura"]`).type('52639').should('have.value', '52639');

      cy.get(`[data-cy="fechaRegistro"]`).type('2021-11-11T07:44').should('have.value', '2021-11-11T07:44');

      cy.get(`[data-cy="fechaToma"]`).type('2021-11-11T13:47').should('have.value', '2021-11-11T13:47');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        fisiometria1 = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', fisiometria1PageUrlPattern);
    });
  });
});
