import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Ingesta e2e test', () => {
  const ingestaPageUrl = '/ingesta';
  const ingestaPageUrlPattern = new RegExp('/ingesta(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const ingestaSample = { tipo: 'Iceland JBOD withdrawal' };

  let ingesta: any;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, username, password);
    });
    cy.intercept('GET', '/services/manilla/api/ingestas').as('entitiesRequest');
    cy.visit('');
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('XSRF-TOKEN', 'JSESSIONID');
  });

  beforeEach(() => {
    cy.intercept('GET', '/services/manilla/api/ingestas+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/services/manilla/api/ingestas').as('postEntityRequest');
    cy.intercept('DELETE', '/services/manilla/api/ingestas/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (ingesta) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/services/manilla/api/ingestas/${ingesta.id}`,
      }).then(() => {
        ingesta = undefined;
      });
    }
  });

  afterEach(() => {
    cy.oauthLogout();
    cy.clearCache();
  });

  it('Ingestas menu should load Ingestas page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('ingesta');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Ingesta').should('exist');
    cy.url().should('match', ingestaPageUrlPattern);
  });

  describe('Ingesta page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(ingestaPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Ingesta page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/ingesta/new$'));
        cy.getEntityCreateUpdateHeading('Ingesta');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', ingestaPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/services/manilla/api/ingestas',
          body: ingestaSample,
        }).then(({ body }) => {
          ingesta = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/services/manilla/api/ingestas+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [ingesta],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(ingestaPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Ingesta page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('ingesta');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', ingestaPageUrlPattern);
      });

      it('edit button click should load edit Ingesta page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Ingesta');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', ingestaPageUrlPattern);
      });

      it('last delete button click should delete instance of Ingesta', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('ingesta').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', ingestaPageUrlPattern);

        ingesta = undefined;
      });
    });
  });

  describe('new Ingesta page', () => {
    beforeEach(() => {
      cy.visit(`${ingestaPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Ingesta');
    });

    it('should create an instance of Ingesta', () => {
      cy.get(`[data-cy="tipo"]`).type('pixel Agente distributed').should('have.value', 'pixel Agente distributed');

      cy.get(`[data-cy="consumoCalorias"]`).type('41168').should('have.value', '41168');

      cy.get(`[data-cy="fechaRegistro"]`).type('2021-11-11T04:53').should('have.value', '2021-11-11T04:53');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        ingesta = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', ingestaPageUrlPattern);
    });
  });
});
