// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.common with an alias.
import Vue from 'vue';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import App from './app.vue';
import Vue2Filters from 'vue2-filters';
import { ToastPlugin } from 'bootstrap-vue';
import router from './router';
import * as config from './shared/config/config';
import * as bootstrapVueConfig from './shared/config/config-bootstrap-vue';
import JhiItemCountComponent from './shared/jhi-item-count.vue';
import JhiSortIndicatorComponent from './shared/sort/jhi-sort-indicator.vue';
import InfiniteLoading from 'vue-infinite-loading';
import HealthService from './admin/health/health.service';
import MetricsService from './admin/metrics/metrics.service';
import LogsService from './admin/logs/logs.service';
import ConfigurationService from '@/admin/configuration/configuration.service';
import LoginService from './account/login.service';
import AccountService from './account/account.service';
import AlertService from './shared/alert/alert.service';

import '../content/scss/vendor.scss';
import TranslationService from '@/locale/translation.service';

import GatewayService from '@/admin/gateway/gateway.service';

import UserOAuth2Service from '@/entities/user/user.oauth2.service';
/* tslint:disable */

import AdherenciaService from '@/entities/pacientes/adherencia/adherencia.service';
import AgendaService from '@/entities/pacientes/agenda/agenda.service';
import AlarmaService from '@/entities/pacientes/alarma/alarma.service';
import BiologicoService from '@/entities/biologico/biologico/biologico.service';
import CaloriaService from '@/entities/manilla/caloria/caloria.service';
import CicloActividadService from '@/entities/biologico/ciclo-actividad/ciclo-actividad.service';
import CiudadService from '@/entities/pacientes/ciudad/ciudad.service';
import CondicionService from '@/entities/pacientes/condicion/condicion.service';
import CuestionarioEstadoService from '@/entities/pacientes/cuestionario-estado/cuestionario-estado.service';
import DispositivoService from '@/entities/manilla/dispositivo/dispositivo.service';
import EncuestaService from '@/entities/manilla/encuesta/encuesta.service';
import EspirometriaService from '@/entities/expedientemanual/espirometria/espirometria.service';
import FabricanteService from '@/entities/biologico/fabricante/fabricante.service';
import FarmaceuticaService from '@/entities/pacientes/farmaceutica/farmaceutica.service';
import Fisiometria1Service from '@/entities/manilla/fisiometria-1/fisiometria-1.service';
import Fisiometria2Service from '@/entities/diadema/fisiometria-2/fisiometria-2.service';
import FrecuenciaCardiacaService from '@/entities/manilla/frecuencia-cardiaca/frecuencia-cardiaca.service';
import GlucosaService from '@/entities/expedientemanual/glucosa/glucosa.service';
import HistorialActividadService from '@/entities/biologico/historial-actividad/historial-actividad.service';
import IngestaService from '@/entities/manilla/ingesta/ingesta.service';
import IPSService from '@/entities/pacientes/ips/ips.service';
import MedicamentoService from '@/entities/pacientes/medicamento/medicamento.service';
import NotificacionService from '@/entities/manilla/notificacion/notificacion.service';
import OximetriaService from '@/entities/manilla/oximetria/oximetria.service';
import PacienteService from '@/entities/pacientes/paciente/paciente.service';
import PaisService from '@/entities/pacientes/pais/pais.service';
import PasosService from '@/entities/manilla/pasos/pasos.service';
import PesoService from '@/entities/expedientemanual/peso/peso.service';
import PreguntaService from '@/entities/pacientes/pregunta/pregunta.service';
import PresionSanguineaService from '@/entities/manilla/presion-sanguinea/presion-sanguinea.service';
import ProgramaService from '@/entities/manilla/programa/programa.service';
import SuenoService from '@/entities/manilla/sueno/sueno.service';
import TemperaturaService from '@/entities/manilla/temperatura/temperatura.service';
import TokenDispService from '@/entities/manilla/token-disp/token-disp.service';
import TratamientoService from '@/entities/pacientes/tratamiento/tratamiento.service';
import TratamientoMedicamentoService from '@/entities/pacientes/tratamiento-medicamento/tratamiento-medicamento.service';
import VacunaService from '@/entities/biologico/vacuna/vacuna.service';
// jhipster-needle-add-entity-service-to-main-import - JHipster will import entities services here

/* tslint:enable */
Vue.config.productionTip = false;
config.initVueApp(Vue);
config.initFortAwesome(Vue);
bootstrapVueConfig.initBootstrapVue(Vue);
Vue.use(Vue2Filters);
Vue.use(ToastPlugin);
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('jhi-item-count', JhiItemCountComponent);
Vue.component('jhi-sort-indicator', JhiSortIndicatorComponent);
Vue.component('infinite-loading', InfiniteLoading);
const i18n = config.initI18N(Vue);
const store = config.initVueXStore(Vue);

const translationService = new TranslationService(store, i18n);
const loginService = new LoginService();
const accountService = new AccountService(store, translationService, (<any>Vue).cookie, router);

router.beforeEach((to, from, next) => {
  if (!to.matched.length) {
    next('/not-found');
  }

  if (to.meta && to.meta.authorities && to.meta.authorities.length > 0) {
    accountService.hasAnyAuthorityAndCheckAuth(to.meta.authorities).then(value => {
      if (!value) {
        sessionStorage.setItem('requested-url', to.fullPath);
        next('/forbidden');
      } else {
        next();
      }
    });
  } else {
    // no authorities, so just proceed
    next();
  }
});

/* tslint:disable */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>',
  router,
  provide: {
    loginService: () => loginService,

    gatewayService: () => new GatewayService(),
    healthService: () => new HealthService(),
    configurationService: () => new ConfigurationService(),
    logsService: () => new LogsService(),
    metricsService: () => new MetricsService(),

    userOAuth2Service: () => new UserOAuth2Service(),
    translationService: () => translationService,
    adherenciaService: () => new AdherenciaService(),
    agendaService: () => new AgendaService(),
    alarmaService: () => new AlarmaService(),
    biologicoService: () => new BiologicoService(),
    caloriaService: () => new CaloriaService(),
    cicloActividadService: () => new CicloActividadService(),
    ciudadService: () => new CiudadService(),
    condicionService: () => new CondicionService(),
    cuestionarioEstadoService: () => new CuestionarioEstadoService(),
    dispositivoService: () => new DispositivoService(),
    encuestaService: () => new EncuestaService(),
    espirometriaService: () => new EspirometriaService(),
    fabricanteService: () => new FabricanteService(),
    farmaceuticaService: () => new FarmaceuticaService(),
    fisiometria1Service: () => new Fisiometria1Service(),
    fisiometria2Service: () => new Fisiometria2Service(),
    frecuenciaCardiacaService: () => new FrecuenciaCardiacaService(),
    glucosaService: () => new GlucosaService(),
    historialActividadService: () => new HistorialActividadService(),
    ingestaService: () => new IngestaService(),
    iPSService: () => new IPSService(),
    medicamentoService: () => new MedicamentoService(),
    notificacionService: () => new NotificacionService(),
    oximetriaService: () => new OximetriaService(),
    pacienteService: () => new PacienteService(),
    paisService: () => new PaisService(),
    pasosService: () => new PasosService(),
    pesoService: () => new PesoService(),
    preguntaService: () => new PreguntaService(),
    presionSanguineaService: () => new PresionSanguineaService(),
    programaService: () => new ProgramaService(),
    suenoService: () => new SuenoService(),
    temperaturaService: () => new TemperaturaService(),
    tokenDispService: () => new TokenDispService(),
    tratamientoService: () => new TratamientoService(),
    tratamientoMedicamentoService: () => new TratamientoMedicamentoService(),
    vacunaService: () => new VacunaService(),
    // jhipster-needle-add-entity-service-to-main - JHipster will import entities services here
    accountService: () => accountService,
    alertService: () => new AlertService(),
  },
  i18n,
  store,
});
