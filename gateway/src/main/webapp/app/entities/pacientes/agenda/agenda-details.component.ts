import { Component, Vue, Inject } from 'vue-property-decorator';

import { IAgenda } from '@/shared/model/pacientes/agenda.model';
import AgendaService from './agenda.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class AgendaDetails extends Vue {
  @Inject('agendaService') private agendaService: () => AgendaService;
  @Inject('alertService') private alertService: () => AlertService;

  public agenda: IAgenda = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.agendaId) {
        vm.retrieveAgenda(to.params.agendaId);
      }
    });
  }

  public retrieveAgenda(agendaId) {
    this.agendaService()
      .find(agendaId)
      .then(res => {
        this.agenda = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
