import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import MedicamentoService from '@/entities/pacientes/medicamento/medicamento.service';
import { IMedicamento } from '@/shared/model/pacientes/medicamento.model';

import { IAgenda, Agenda } from '@/shared/model/pacientes/agenda.model';
import AgendaService from './agenda.service';

const validations: any = {
  agenda: {
    horaMedicamento: {},
  },
};

@Component({
  validations,
})
export default class AgendaUpdate extends Vue {
  @Inject('agendaService') private agendaService: () => AgendaService;
  @Inject('alertService') private alertService: () => AlertService;

  public agenda: IAgenda = new Agenda();

  @Inject('medicamentoService') private medicamentoService: () => MedicamentoService;

  public medicamentos: IMedicamento[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.agendaId) {
        vm.retrieveAgenda(to.params.agendaId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.agenda.id) {
      this.agendaService()
        .update(this.agenda)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesAgenda.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.agendaService()
        .create(this.agenda)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesAgenda.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveAgenda(agendaId): void {
    this.agendaService()
      .find(agendaId)
      .then(res => {
        this.agenda = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.medicamentoService()
      .retrieve()
      .then(res => {
        this.medicamentos = res.data;
      });
  }
}
