import { Component, Vue, Inject } from 'vue-property-decorator';

import { IPregunta } from '@/shared/model/pacientes/pregunta.model';
import PreguntaService from './pregunta.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class PreguntaDetails extends Vue {
  @Inject('preguntaService') private preguntaService: () => PreguntaService;
  @Inject('alertService') private alertService: () => AlertService;

  public pregunta: IPregunta = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.preguntaId) {
        vm.retrievePregunta(to.params.preguntaId);
      }
    });
  }

  public retrievePregunta(preguntaId) {
    this.preguntaService()
      .find(preguntaId)
      .then(res => {
        this.pregunta = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
