import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import AlertService from '@/shared/alert/alert.service';

import { ICondicion, Condicion } from '@/shared/model/pacientes/condicion.model';
import CondicionService from './condicion.service';

const validations: any = {
  condicion: {
    condicion: {},
    descripcion: {},
  },
};

@Component({
  validations,
})
export default class CondicionUpdate extends mixins(JhiDataUtils) {
  @Inject('condicionService') private condicionService: () => CondicionService;
  @Inject('alertService') private alertService: () => AlertService;

  public condicion: ICondicion = new Condicion();
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.condicionId) {
        vm.retrieveCondicion(to.params.condicionId);
      }
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.condicion.id) {
      this.condicionService()
        .update(this.condicion)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesCondicion.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.condicionService()
        .create(this.condicion)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesCondicion.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveCondicion(condicionId): void {
    this.condicionService()
      .find(condicionId)
      .then(res => {
        this.condicion = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
