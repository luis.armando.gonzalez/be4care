import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { ICondicion } from '@/shared/model/pacientes/condicion.model';
import CondicionService from './condicion.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class CondicionDetails extends mixins(JhiDataUtils) {
  @Inject('condicionService') private condicionService: () => CondicionService;
  @Inject('alertService') private alertService: () => AlertService;

  public condicion: ICondicion = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.condicionId) {
        vm.retrieveCondicion(to.params.condicionId);
      }
    });
  }

  public retrieveCondicion(condicionId) {
    this.condicionService()
      .find(condicionId)
      .then(res => {
        this.condicion = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
