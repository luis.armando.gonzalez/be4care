import { Component, Vue, Inject } from 'vue-property-decorator';

import { IFarmaceutica } from '@/shared/model/pacientes/farmaceutica.model';
import FarmaceuticaService from './farmaceutica.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class FarmaceuticaDetails extends Vue {
  @Inject('farmaceuticaService') private farmaceuticaService: () => FarmaceuticaService;
  @Inject('alertService') private alertService: () => AlertService;

  public farmaceutica: IFarmaceutica = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.farmaceuticaId) {
        vm.retrieveFarmaceutica(to.params.farmaceuticaId);
      }
    });
  }

  public retrieveFarmaceutica(farmaceuticaId) {
    this.farmaceuticaService()
      .find(farmaceuticaId)
      .then(res => {
        this.farmaceutica = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
