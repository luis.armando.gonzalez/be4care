import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import { IFarmaceutica, Farmaceutica } from '@/shared/model/pacientes/farmaceutica.model';
import FarmaceuticaService from './farmaceutica.service';

const validations: any = {
  farmaceutica: {
    nombre: {},
    direccion: {},
    propietario: {},
  },
};

@Component({
  validations,
})
export default class FarmaceuticaUpdate extends Vue {
  @Inject('farmaceuticaService') private farmaceuticaService: () => FarmaceuticaService;
  @Inject('alertService') private alertService: () => AlertService;

  public farmaceutica: IFarmaceutica = new Farmaceutica();
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.farmaceuticaId) {
        vm.retrieveFarmaceutica(to.params.farmaceuticaId);
      }
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.farmaceutica.id) {
      this.farmaceuticaService()
        .update(this.farmaceutica)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesFarmaceutica.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.farmaceuticaService()
        .create(this.farmaceutica)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesFarmaceutica.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveFarmaceutica(farmaceuticaId): void {
    this.farmaceuticaService()
      .find(farmaceuticaId)
      .then(res => {
        this.farmaceutica = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
