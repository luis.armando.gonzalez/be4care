import { Component, Vue, Inject } from 'vue-property-decorator';

import { IAlarma } from '@/shared/model/pacientes/alarma.model';
import AlarmaService from './alarma.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class AlarmaDetails extends Vue {
  @Inject('alarmaService') private alarmaService: () => AlarmaService;
  @Inject('alertService') private alertService: () => AlertService;

  public alarma: IAlarma = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.alarmaId) {
        vm.retrieveAlarma(to.params.alarmaId);
      }
    });
  }

  public retrieveAlarma(alarmaId) {
    this.alarmaService()
      .find(alarmaId)
      .then(res => {
        this.alarma = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
