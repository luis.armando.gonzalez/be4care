import { Component, Vue, Inject } from 'vue-property-decorator';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import UserOAuth2Service from '@/entities/user/user.oauth2.service';

import { IAlarma, Alarma } from '@/shared/model/pacientes/alarma.model';
import AlarmaService from './alarma.service';

const validations: any = {
  alarma: {
    timeInstant: {},
    descripcion: {},
    procedimiento: {},
    titulo: {},
    verificar: {},
    observaciones: {},
    prioridad: {},
  },
};

@Component({
  validations,
})
export default class AlarmaUpdate extends Vue {
  @Inject('alarmaService') private alarmaService: () => AlarmaService;
  @Inject('alertService') private alertService: () => AlertService;

  public alarma: IAlarma = new Alarma();

  @Inject('userOAuth2Service') private userOAuth2Service: () => UserOAuth2Service;

  public users: Array<any> = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.alarmaId) {
        vm.retrieveAlarma(to.params.alarmaId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.alarma.id) {
      this.alarmaService()
        .update(this.alarma)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesAlarma.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.alarmaService()
        .create(this.alarma)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesAlarma.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.alarma[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.alarma[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.alarma[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.alarma[field] = null;
    }
  }

  public retrieveAlarma(alarmaId): void {
    this.alarmaService()
      .find(alarmaId)
      .then(res => {
        res.timeInstant = new Date(res.timeInstant);
        this.alarma = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.userOAuth2Service()
      .retrieve()
      .then(res => {
        this.users = res.data;
      });
  }
}
