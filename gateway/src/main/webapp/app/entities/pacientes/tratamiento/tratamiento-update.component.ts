import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import { ITratamiento, Tratamiento } from '@/shared/model/pacientes/tratamiento.model';
import TratamientoService from './tratamiento.service';

const validations: any = {
  tratamiento: {
    descripcionTratamiento: {},
    fechaInicio: {},
    fechaFin: {},
  },
};

@Component({
  validations,
})
export default class TratamientoUpdate extends mixins(JhiDataUtils) {
  @Inject('tratamientoService') private tratamientoService: () => TratamientoService;
  @Inject('alertService') private alertService: () => AlertService;

  public tratamiento: ITratamiento = new Tratamiento();
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.tratamientoId) {
        vm.retrieveTratamiento(to.params.tratamientoId);
      }
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.tratamiento.id) {
      this.tratamientoService()
        .update(this.tratamiento)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesTratamiento.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.tratamientoService()
        .create(this.tratamiento)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesTratamiento.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.tratamiento[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.tratamiento[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.tratamiento[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.tratamiento[field] = null;
    }
  }

  public retrieveTratamiento(tratamientoId): void {
    this.tratamientoService()
      .find(tratamientoId)
      .then(res => {
        res.fechaInicio = new Date(res.fechaInicio);
        res.fechaFin = new Date(res.fechaFin);
        this.tratamiento = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
