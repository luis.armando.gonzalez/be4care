import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { ITratamiento } from '@/shared/model/pacientes/tratamiento.model';
import TratamientoService from './tratamiento.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class TratamientoDetails extends mixins(JhiDataUtils) {
  @Inject('tratamientoService') private tratamientoService: () => TratamientoService;
  @Inject('alertService') private alertService: () => AlertService;

  public tratamiento: ITratamiento = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.tratamientoId) {
        vm.retrieveTratamiento(to.params.tratamientoId);
      }
    });
  }

  public retrieveTratamiento(tratamientoId) {
    this.tratamientoService()
      .find(tratamientoId)
      .then(res => {
        this.tratamiento = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
