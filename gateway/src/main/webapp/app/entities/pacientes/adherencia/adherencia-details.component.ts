import { Component, Vue, Inject } from 'vue-property-decorator';

import { IAdherencia } from '@/shared/model/pacientes/adherencia.model';
import AdherenciaService from './adherencia.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class AdherenciaDetails extends Vue {
  @Inject('adherenciaService') private adherenciaService: () => AdherenciaService;
  @Inject('alertService') private alertService: () => AlertService;

  public adherencia: IAdherencia = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.adherenciaId) {
        vm.retrieveAdherencia(to.params.adherenciaId);
      }
    });
  }

  public retrieveAdherencia(adherenciaId) {
    this.adherenciaService()
      .find(adherenciaId)
      .then(res => {
        this.adherencia = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
