import { Component, Vue, Inject } from 'vue-property-decorator';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import MedicamentoService from '@/entities/pacientes/medicamento/medicamento.service';
import { IMedicamento } from '@/shared/model/pacientes/medicamento.model';

import PacienteService from '@/entities/pacientes/paciente/paciente.service';
import { IPaciente } from '@/shared/model/pacientes/paciente.model';

import { IAdherencia, Adherencia } from '@/shared/model/pacientes/adherencia.model';
import AdherenciaService from './adherencia.service';

const validations: any = {
  adherencia: {
    horaToma: {},
    respuesta: {},
    valor: {},
    comentario: {},
  },
};

@Component({
  validations,
})
export default class AdherenciaUpdate extends Vue {
  @Inject('adherenciaService') private adherenciaService: () => AdherenciaService;
  @Inject('alertService') private alertService: () => AlertService;

  public adherencia: IAdherencia = new Adherencia();

  @Inject('medicamentoService') private medicamentoService: () => MedicamentoService;

  public medicamentos: IMedicamento[] = [];

  @Inject('pacienteService') private pacienteService: () => PacienteService;

  public pacientes: IPaciente[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.adherenciaId) {
        vm.retrieveAdherencia(to.params.adherenciaId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.adherencia.id) {
      this.adherenciaService()
        .update(this.adherencia)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesAdherencia.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.adherenciaService()
        .create(this.adherencia)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesAdherencia.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.adherencia[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.adherencia[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.adherencia[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.adherencia[field] = null;
    }
  }

  public retrieveAdherencia(adherenciaId): void {
    this.adherenciaService()
      .find(adherenciaId)
      .then(res => {
        res.horaToma = new Date(res.horaToma);
        this.adherencia = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.medicamentoService()
      .retrieve()
      .then(res => {
        this.medicamentos = res.data;
      });
    this.pacienteService()
      .retrieve()
      .then(res => {
        this.pacientes = res.data;
      });
  }
}
