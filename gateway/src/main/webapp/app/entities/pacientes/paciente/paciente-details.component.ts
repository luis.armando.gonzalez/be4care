import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { IPaciente } from '@/shared/model/pacientes/paciente.model';
import PacienteService from './paciente.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class PacienteDetails extends mixins(JhiDataUtils) {
  @Inject('pacienteService') private pacienteService: () => PacienteService;
  @Inject('alertService') private alertService: () => AlertService;

  public paciente: IPaciente = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.pacienteId) {
        vm.retrievePaciente(to.params.pacienteId);
      }
    });
  }

  public retrievePaciente(pacienteId) {
    this.pacienteService()
      .find(pacienteId)
      .then(res => {
        this.paciente = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
