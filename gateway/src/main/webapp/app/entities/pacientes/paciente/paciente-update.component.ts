import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import AlertService from '@/shared/alert/alert.service';

import UserOAuth2Service from '@/entities/user/user.oauth2.service';

import CiudadService from '@/entities/pacientes/ciudad/ciudad.service';
import { ICiudad } from '@/shared/model/pacientes/ciudad.model';

import CondicionService from '@/entities/pacientes/condicion/condicion.service';
import { ICondicion } from '@/shared/model/pacientes/condicion.model';

import IPSService from '@/entities/pacientes/ips/ips.service';
import { IIPS } from '@/shared/model/pacientes/ips.model';

import TratamientoService from '@/entities/pacientes/tratamiento/tratamiento.service';
import { ITratamiento } from '@/shared/model/pacientes/tratamiento.model';

import FarmaceuticaService from '@/entities/pacientes/farmaceutica/farmaceutica.service';
import { IFarmaceutica } from '@/shared/model/pacientes/farmaceutica.model';

import { IPaciente, Paciente } from '@/shared/model/pacientes/paciente.model';
import PacienteService from './paciente.service';
import { Identificaciont } from '@/shared/model/enumerations/identificaciont.model';
import { Sexop } from '@/shared/model/enumerations/sexop.model';
import { Grupoetnicop } from '@/shared/model/enumerations/grupoetnicop.model';
import { Estratop } from '@/shared/model/enumerations/estratop.model';

const validations: any = {
  paciente: {
    nombre: {},
    tipoIdentificacion: {},
    identificacion: {},
    edad: {},
    sexo: {},
    telefono: {},
    direccion: {},
    pesoKG: {},
    estaturaCM: {},
    grupoEtnico: {},
    estratoSocioeconomico: {},
    oximetriaReferencia: {},
    ritmoCardiacoReferencia: {},
    presionSistolicaReferencia: {},
    presionDistolicaReferencia: {},
    comentarios: {},
    pasosReferencia: {},
    caloriasReferencia: {},
    metaReferencia: {},
  },
};

@Component({
  validations,
})
export default class PacienteUpdate extends mixins(JhiDataUtils) {
  @Inject('pacienteService') private pacienteService: () => PacienteService;
  @Inject('alertService') private alertService: () => AlertService;

  public paciente: IPaciente = new Paciente();

  @Inject('userOAuth2Service') private userOAuth2Service: () => UserOAuth2Service;

  public users: Array<any> = [];

  @Inject('ciudadService') private ciudadService: () => CiudadService;

  public ciudads: ICiudad[] = [];

  @Inject('condicionService') private condicionService: () => CondicionService;

  public condicions: ICondicion[] = [];

  @Inject('iPSService') private iPSService: () => IPSService;

  public iPS: IIPS[] = [];

  @Inject('tratamientoService') private tratamientoService: () => TratamientoService;

  public tratamientos: ITratamiento[] = [];

  @Inject('farmaceuticaService') private farmaceuticaService: () => FarmaceuticaService;

  public farmaceuticas: IFarmaceutica[] = [];
  public identificaciontValues: string[] = Object.keys(Identificaciont);
  public sexopValues: string[] = Object.keys(Sexop);
  public grupoetnicopValues: string[] = Object.keys(Grupoetnicop);
  public estratopValues: string[] = Object.keys(Estratop);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.pacienteId) {
        vm.retrievePaciente(to.params.pacienteId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.paciente.id) {
      this.pacienteService()
        .update(this.paciente)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesPaciente.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.pacienteService()
        .create(this.paciente)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesPaciente.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrievePaciente(pacienteId): void {
    this.pacienteService()
      .find(pacienteId)
      .then(res => {
        this.paciente = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.userOAuth2Service()
      .retrieve()
      .then(res => {
        this.users = res.data;
      });
    this.ciudadService()
      .retrieve()
      .then(res => {
        this.ciudads = res.data;
      });
    this.condicionService()
      .retrieve()
      .then(res => {
        this.condicions = res.data;
      });
    this.iPSService()
      .retrieve()
      .then(res => {
        this.iPS = res.data;
      });
    this.tratamientoService()
      .retrieve()
      .then(res => {
        this.tratamientos = res.data;
      });
    this.farmaceuticaService()
      .retrieve()
      .then(res => {
        this.farmaceuticas = res.data;
      });
  }
}
