import { Component, Vue, Inject } from 'vue-property-decorator';

import { IIPS } from '@/shared/model/pacientes/ips.model';
import IPSService from './ips.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class IPSDetails extends Vue {
  @Inject('iPSService') private iPSService: () => IPSService;
  @Inject('alertService') private alertService: () => AlertService;

  public iPS: IIPS = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.iPSId) {
        vm.retrieveIPS(to.params.iPSId);
      }
    });
  }

  public retrieveIPS(iPSId) {
    this.iPSService()
      .find(iPSId)
      .then(res => {
        this.iPS = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
