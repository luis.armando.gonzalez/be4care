import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import { IIPS, IPS } from '@/shared/model/pacientes/ips.model';
import IPSService from './ips.service';

const validations: any = {
  iPS: {
    nombre: {},
    nit: {},
    direccion: {},
    telefono: {},
    correoElectronico: {},
  },
};

@Component({
  validations,
})
export default class IPSUpdate extends Vue {
  @Inject('iPSService') private iPSService: () => IPSService;
  @Inject('alertService') private alertService: () => AlertService;

  public iPS: IIPS = new IPS();
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.iPSId) {
        vm.retrieveIPS(to.params.iPSId);
      }
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.iPS.id) {
      this.iPSService()
        .update(this.iPS)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesIPs.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.iPSService()
        .create(this.iPS)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesIPs.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveIPS(iPSId): void {
    this.iPSService()
      .find(iPSId)
      .then(res => {
        this.iPS = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
