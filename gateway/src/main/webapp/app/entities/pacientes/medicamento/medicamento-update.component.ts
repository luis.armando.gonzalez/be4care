import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import { IMedicamento, Medicamento } from '@/shared/model/pacientes/medicamento.model';
import MedicamentoService from './medicamento.service';
import { Presentacion } from '@/shared/model/enumerations/presentacion.model';

const validations: any = {
  medicamento: {
    nombre: {},
    descripcion: {},
    fechaIngreso: {},
    presentacion: {},
    generico: {},
  },
};

@Component({
  validations,
})
export default class MedicamentoUpdate extends mixins(JhiDataUtils) {
  @Inject('medicamentoService') private medicamentoService: () => MedicamentoService;
  @Inject('alertService') private alertService: () => AlertService;

  public medicamento: IMedicamento = new Medicamento();
  public presentacionValues: string[] = Object.keys(Presentacion);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.medicamentoId) {
        vm.retrieveMedicamento(to.params.medicamentoId);
      }
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.medicamento.id) {
      this.medicamentoService()
        .update(this.medicamento)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesMedicamento.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.medicamentoService()
        .create(this.medicamento)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesMedicamento.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.medicamento[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.medicamento[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.medicamento[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.medicamento[field] = null;
    }
  }

  public retrieveMedicamento(medicamentoId): void {
    this.medicamentoService()
      .find(medicamentoId)
      .then(res => {
        res.fechaIngreso = new Date(res.fechaIngreso);
        this.medicamento = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
