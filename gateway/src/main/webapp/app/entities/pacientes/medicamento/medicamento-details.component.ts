import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { IMedicamento } from '@/shared/model/pacientes/medicamento.model';
import MedicamentoService from './medicamento.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class MedicamentoDetails extends mixins(JhiDataUtils) {
  @Inject('medicamentoService') private medicamentoService: () => MedicamentoService;
  @Inject('alertService') private alertService: () => AlertService;

  public medicamento: IMedicamento = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.medicamentoId) {
        vm.retrieveMedicamento(to.params.medicamentoId);
      }
    });
  }

  public retrieveMedicamento(medicamentoId) {
    this.medicamentoService()
      .find(medicamentoId)
      .then(res => {
        this.medicamento = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
