import { Component, Vue, Inject } from 'vue-property-decorator';

import { ITratamientoMedicamento } from '@/shared/model/pacientes/tratamiento-medicamento.model';
import TratamientoMedicamentoService from './tratamiento-medicamento.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class TratamientoMedicamentoDetails extends Vue {
  @Inject('tratamientoMedicamentoService') private tratamientoMedicamentoService: () => TratamientoMedicamentoService;
  @Inject('alertService') private alertService: () => AlertService;

  public tratamientoMedicamento: ITratamientoMedicamento = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.tratamientoMedicamentoId) {
        vm.retrieveTratamientoMedicamento(to.params.tratamientoMedicamentoId);
      }
    });
  }

  public retrieveTratamientoMedicamento(tratamientoMedicamentoId) {
    this.tratamientoMedicamentoService()
      .find(tratamientoMedicamentoId)
      .then(res => {
        this.tratamientoMedicamento = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
