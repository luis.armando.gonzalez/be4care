import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import TratamientoService from '@/entities/pacientes/tratamiento/tratamiento.service';
import { ITratamiento } from '@/shared/model/pacientes/tratamiento.model';

import MedicamentoService from '@/entities/pacientes/medicamento/medicamento.service';
import { IMedicamento } from '@/shared/model/pacientes/medicamento.model';

import { ITratamientoMedicamento, TratamientoMedicamento } from '@/shared/model/pacientes/tratamiento-medicamento.model';
import TratamientoMedicamentoService from './tratamiento-medicamento.service';

const validations: any = {
  tratamientoMedicamento: {
    dosis: {},
    intensidad: {},
  },
};

@Component({
  validations,
})
export default class TratamientoMedicamentoUpdate extends Vue {
  @Inject('tratamientoMedicamentoService') private tratamientoMedicamentoService: () => TratamientoMedicamentoService;
  @Inject('alertService') private alertService: () => AlertService;

  public tratamientoMedicamento: ITratamientoMedicamento = new TratamientoMedicamento();

  @Inject('tratamientoService') private tratamientoService: () => TratamientoService;

  public tratamientos: ITratamiento[] = [];

  @Inject('medicamentoService') private medicamentoService: () => MedicamentoService;

  public medicamentos: IMedicamento[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.tratamientoMedicamentoId) {
        vm.retrieveTratamientoMedicamento(to.params.tratamientoMedicamentoId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.tratamientoMedicamento.id) {
      this.tratamientoMedicamentoService()
        .update(this.tratamientoMedicamento)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesTratamientoMedicamento.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.tratamientoMedicamentoService()
        .create(this.tratamientoMedicamento)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesTratamientoMedicamento.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveTratamientoMedicamento(tratamientoMedicamentoId): void {
    this.tratamientoMedicamentoService()
      .find(tratamientoMedicamentoId)
      .then(res => {
        this.tratamientoMedicamento = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.tratamientoService()
      .retrieve()
      .then(res => {
        this.tratamientos = res.data;
      });
    this.medicamentoService()
      .retrieve()
      .then(res => {
        this.medicamentos = res.data;
      });
  }
}
