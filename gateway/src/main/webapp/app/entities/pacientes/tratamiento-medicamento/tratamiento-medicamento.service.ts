import axios from 'axios';

import buildPaginationQueryOpts from '@/shared/sort/sorts';

import { ITratamientoMedicamento } from '@/shared/model/pacientes/tratamiento-medicamento.model';

const baseApiUrl = 'services/pacientes/api/tratamiento-medicamentos';

export default class TratamientoMedicamentoService {
  public find(id: number): Promise<ITratamientoMedicamento> {
    return new Promise<ITratamientoMedicamento>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public retrieve(paginationQuery?: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(baseApiUrl + `?${buildPaginationQueryOpts(paginationQuery)}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public delete(id: number): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .delete(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public create(entity: ITratamientoMedicamento): Promise<ITratamientoMedicamento> {
    return new Promise<ITratamientoMedicamento>((resolve, reject) => {
      axios
        .post(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public update(entity: ITratamientoMedicamento): Promise<ITratamientoMedicamento> {
    return new Promise<ITratamientoMedicamento>((resolve, reject) => {
      axios
        .put(`${baseApiUrl}/${entity.id}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public partialUpdate(entity: ITratamientoMedicamento): Promise<ITratamientoMedicamento> {
    return new Promise<ITratamientoMedicamento>((resolve, reject) => {
      axios
        .patch(`${baseApiUrl}/${entity.id}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}
