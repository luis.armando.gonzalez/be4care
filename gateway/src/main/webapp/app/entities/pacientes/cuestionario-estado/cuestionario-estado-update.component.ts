import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import PreguntaService from '@/entities/pacientes/pregunta/pregunta.service';
import { IPregunta } from '@/shared/model/pacientes/pregunta.model';

import { ICuestionarioEstado, CuestionarioEstado } from '@/shared/model/pacientes/cuestionario-estado.model';
import CuestionarioEstadoService from './cuestionario-estado.service';

const validations: any = {
  cuestionarioEstado: {
    valor: {},
    valoracion: {},
  },
};

@Component({
  validations,
})
export default class CuestionarioEstadoUpdate extends Vue {
  @Inject('cuestionarioEstadoService') private cuestionarioEstadoService: () => CuestionarioEstadoService;
  @Inject('alertService') private alertService: () => AlertService;

  public cuestionarioEstado: ICuestionarioEstado = new CuestionarioEstado();

  @Inject('preguntaService') private preguntaService: () => PreguntaService;

  public preguntas: IPregunta[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.cuestionarioEstadoId) {
        vm.retrieveCuestionarioEstado(to.params.cuestionarioEstadoId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.cuestionarioEstado.id) {
      this.cuestionarioEstadoService()
        .update(this.cuestionarioEstado)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesCuestionarioEstado.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.cuestionarioEstadoService()
        .create(this.cuestionarioEstado)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('pacientesApp.pacientesCuestionarioEstado.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveCuestionarioEstado(cuestionarioEstadoId): void {
    this.cuestionarioEstadoService()
      .find(cuestionarioEstadoId)
      .then(res => {
        this.cuestionarioEstado = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.preguntaService()
      .retrieve()
      .then(res => {
        this.preguntas = res.data;
      });
  }
}
