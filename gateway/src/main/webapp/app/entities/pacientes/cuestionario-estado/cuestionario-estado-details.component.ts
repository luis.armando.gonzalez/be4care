import { Component, Vue, Inject } from 'vue-property-decorator';

import { ICuestionarioEstado } from '@/shared/model/pacientes/cuestionario-estado.model';
import CuestionarioEstadoService from './cuestionario-estado.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class CuestionarioEstadoDetails extends Vue {
  @Inject('cuestionarioEstadoService') private cuestionarioEstadoService: () => CuestionarioEstadoService;
  @Inject('alertService') private alertService: () => AlertService;

  public cuestionarioEstado: ICuestionarioEstado = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.cuestionarioEstadoId) {
        vm.retrieveCuestionarioEstado(to.params.cuestionarioEstadoId);
      }
    });
  }

  public retrieveCuestionarioEstado(cuestionarioEstadoId) {
    this.cuestionarioEstadoService()
      .find(cuestionarioEstadoId)
      .then(res => {
        this.cuestionarioEstado = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
