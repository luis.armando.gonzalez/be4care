import { Component, Vue, Inject } from 'vue-property-decorator';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import UserOAuth2Service from '@/entities/user/user.oauth2.service';

import { IFisiometria2, Fisiometria2 } from '@/shared/model/diadema/fisiometria-2.model';
import Fisiometria2Service from './fisiometria-2.service';

const validations: any = {
  fisiometria2: {
    timeInstant: {},
    meditacion: {},
    attention: {},
    delta: {},
    theta: {},
    lowAlpha: {},
    highAlpha: {},
    lowBeta: {},
    highBeta: {},
    lowGamma: {},
    midGamma: {},
    pulso: {},
    respiracion: {},
    acelerometro: {},
    estadoFuncional: {},
  },
};

@Component({
  validations,
})
export default class Fisiometria2Update extends Vue {
  @Inject('fisiometria2Service') private fisiometria2Service: () => Fisiometria2Service;
  @Inject('alertService') private alertService: () => AlertService;

  public fisiometria2: IFisiometria2 = new Fisiometria2();

  @Inject('userOAuth2Service') private userOAuth2Service: () => UserOAuth2Service;

  public users: Array<any> = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.fisiometria2Id) {
        vm.retrieveFisiometria2(to.params.fisiometria2Id);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.fisiometria2.id) {
      this.fisiometria2Service()
        .update(this.fisiometria2)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('diademaApp.diademaFisiometria2.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.fisiometria2Service()
        .create(this.fisiometria2)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('diademaApp.diademaFisiometria2.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.fisiometria2[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.fisiometria2[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.fisiometria2[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.fisiometria2[field] = null;
    }
  }

  public retrieveFisiometria2(fisiometria2Id): void {
    this.fisiometria2Service()
      .find(fisiometria2Id)
      .then(res => {
        res.timeInstant = new Date(res.timeInstant);
        this.fisiometria2 = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.userOAuth2Service()
      .retrieve()
      .then(res => {
        this.users = res.data;
      });
  }
}
