import { Component, Vue, Inject } from 'vue-property-decorator';

import { IFisiometria2 } from '@/shared/model/diadema/fisiometria-2.model';
import Fisiometria2Service from './fisiometria-2.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class Fisiometria2Details extends Vue {
  @Inject('fisiometria2Service') private fisiometria2Service: () => Fisiometria2Service;
  @Inject('alertService') private alertService: () => AlertService;

  public fisiometria2: IFisiometria2 = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.fisiometria2Id) {
        vm.retrieveFisiometria2(to.params.fisiometria2Id);
      }
    });
  }

  public retrieveFisiometria2(fisiometria2Id) {
    this.fisiometria2Service()
      .find(fisiometria2Id)
      .then(res => {
        this.fisiometria2 = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
