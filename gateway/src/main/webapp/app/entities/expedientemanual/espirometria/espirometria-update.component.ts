import { Component, Vue, Inject } from 'vue-property-decorator';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import UserOAuth2Service from '@/entities/user/user.oauth2.service';

import { IEspirometria, Espirometria } from '@/shared/model/expedientemanual/espirometria.model';
import EspirometriaService from './espirometria.service';

const validations: any = {
  espirometria: {
    fev1T: {},
    fev1R: {},
    fvcT: {},
    fvcR: {},
    indiceTP: {},
    fechaToma: {},
    resultado: {},
  },
};

@Component({
  validations,
})
export default class EspirometriaUpdate extends Vue {
  @Inject('espirometriaService') private espirometriaService: () => EspirometriaService;
  @Inject('alertService') private alertService: () => AlertService;

  public espirometria: IEspirometria = new Espirometria();

  @Inject('userOAuth2Service') private userOAuth2Service: () => UserOAuth2Service;

  public users: Array<any> = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.espirometriaId) {
        vm.retrieveEspirometria(to.params.espirometriaId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.espirometria.id) {
      this.espirometriaService()
        .update(this.espirometria)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('expedientemanualApp.expedientemanualEspirometria.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.espirometriaService()
        .create(this.espirometria)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('expedientemanualApp.expedientemanualEspirometria.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.espirometria[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.espirometria[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.espirometria[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.espirometria[field] = null;
    }
  }

  public retrieveEspirometria(espirometriaId): void {
    this.espirometriaService()
      .find(espirometriaId)
      .then(res => {
        res.fechaToma = new Date(res.fechaToma);
        this.espirometria = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.userOAuth2Service()
      .retrieve()
      .then(res => {
        this.users = res.data;
      });
  }
}
