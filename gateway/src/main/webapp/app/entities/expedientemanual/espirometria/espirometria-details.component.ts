import { Component, Vue, Inject } from 'vue-property-decorator';

import { IEspirometria } from '@/shared/model/expedientemanual/espirometria.model';
import EspirometriaService from './espirometria.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class EspirometriaDetails extends Vue {
  @Inject('espirometriaService') private espirometriaService: () => EspirometriaService;
  @Inject('alertService') private alertService: () => AlertService;

  public espirometria: IEspirometria = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.espirometriaId) {
        vm.retrieveEspirometria(to.params.espirometriaId);
      }
    });
  }

  public retrieveEspirometria(espirometriaId) {
    this.espirometriaService()
      .find(espirometriaId)
      .then(res => {
        this.espirometria = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
