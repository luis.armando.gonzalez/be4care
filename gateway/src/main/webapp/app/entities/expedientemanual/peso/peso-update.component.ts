import { Component, Vue, Inject } from 'vue-property-decorator';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import UserOAuth2Service from '@/entities/user/user.oauth2.service';

import { IPeso, Peso } from '@/shared/model/expedientemanual/peso.model';
import PesoService from './peso.service';

const validations: any = {
  peso: {
    pesoKG: {},
    descripcion: {},
    fechaRegistro: {},
  },
};

@Component({
  validations,
})
export default class PesoUpdate extends Vue {
  @Inject('pesoService') private pesoService: () => PesoService;
  @Inject('alertService') private alertService: () => AlertService;

  public peso: IPeso = new Peso();

  @Inject('userOAuth2Service') private userOAuth2Service: () => UserOAuth2Service;

  public users: Array<any> = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.pesoId) {
        vm.retrievePeso(to.params.pesoId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.peso.id) {
      this.pesoService()
        .update(this.peso)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('expedientemanualApp.expedientemanualPeso.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.pesoService()
        .create(this.peso)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('expedientemanualApp.expedientemanualPeso.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.peso[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.peso[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.peso[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.peso[field] = null;
    }
  }

  public retrievePeso(pesoId): void {
    this.pesoService()
      .find(pesoId)
      .then(res => {
        res.fechaRegistro = new Date(res.fechaRegistro);
        this.peso = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.userOAuth2Service()
      .retrieve()
      .then(res => {
        this.users = res.data;
      });
  }
}
