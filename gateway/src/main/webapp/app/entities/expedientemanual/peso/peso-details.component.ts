import { Component, Vue, Inject } from 'vue-property-decorator';

import { IPeso } from '@/shared/model/expedientemanual/peso.model';
import PesoService from './peso.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class PesoDetails extends Vue {
  @Inject('pesoService') private pesoService: () => PesoService;
  @Inject('alertService') private alertService: () => AlertService;

  public peso: IPeso = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.pesoId) {
        vm.retrievePeso(to.params.pesoId);
      }
    });
  }

  public retrievePeso(pesoId) {
    this.pesoService()
      .find(pesoId)
      .then(res => {
        this.peso = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
