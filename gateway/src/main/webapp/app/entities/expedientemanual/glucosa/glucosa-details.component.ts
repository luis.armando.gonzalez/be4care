import { Component, Vue, Inject } from 'vue-property-decorator';

import { IGlucosa } from '@/shared/model/expedientemanual/glucosa.model';
import GlucosaService from './glucosa.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class GlucosaDetails extends Vue {
  @Inject('glucosaService') private glucosaService: () => GlucosaService;
  @Inject('alertService') private alertService: () => AlertService;

  public glucosa: IGlucosa = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.glucosaId) {
        vm.retrieveGlucosa(to.params.glucosaId);
      }
    });
  }

  public retrieveGlucosa(glucosaId) {
    this.glucosaService()
      .find(glucosaId)
      .then(res => {
        this.glucosa = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
