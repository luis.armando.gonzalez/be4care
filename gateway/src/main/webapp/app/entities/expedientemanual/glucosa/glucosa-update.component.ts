import { Component, Vue, Inject } from 'vue-property-decorator';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import UserOAuth2Service from '@/entities/user/user.oauth2.service';

import { IGlucosa, Glucosa } from '@/shared/model/expedientemanual/glucosa.model';
import GlucosaService from './glucosa.service';

const validations: any = {
  glucosa: {
    glucosaUser: {},
    fechaRegistro: {},
  },
};

@Component({
  validations,
})
export default class GlucosaUpdate extends Vue {
  @Inject('glucosaService') private glucosaService: () => GlucosaService;
  @Inject('alertService') private alertService: () => AlertService;

  public glucosa: IGlucosa = new Glucosa();

  @Inject('userOAuth2Service') private userOAuth2Service: () => UserOAuth2Service;

  public users: Array<any> = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.glucosaId) {
        vm.retrieveGlucosa(to.params.glucosaId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.glucosa.id) {
      this.glucosaService()
        .update(this.glucosa)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('expedientemanualApp.expedientemanualGlucosa.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.glucosaService()
        .create(this.glucosa)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('expedientemanualApp.expedientemanualGlucosa.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.glucosa[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.glucosa[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.glucosa[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.glucosa[field] = null;
    }
  }

  public retrieveGlucosa(glucosaId): void {
    this.glucosaService()
      .find(glucosaId)
      .then(res => {
        res.fechaRegistro = new Date(res.fechaRegistro);
        this.glucosa = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.userOAuth2Service()
      .retrieve()
      .then(res => {
        this.users = res.data;
      });
  }
}
