import { Component, Vue, Inject } from 'vue-property-decorator';

import { ISueno } from '@/shared/model/manilla/sueno.model';
import SuenoService from './sueno.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class SuenoDetails extends Vue {
  @Inject('suenoService') private suenoService: () => SuenoService;
  @Inject('alertService') private alertService: () => AlertService;

  public sueno: ISueno = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.suenoId) {
        vm.retrieveSueno(to.params.suenoId);
      }
    });
  }

  public retrieveSueno(suenoId) {
    this.suenoService()
      .find(suenoId)
      .then(res => {
        this.sueno = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
