import { Component, Vue, Inject } from 'vue-property-decorator';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import TokenDispService from '@/entities/manilla/token-disp/token-disp.service';
import { ITokenDisp } from '@/shared/model/manilla/token-disp.model';

import { INotificacion, Notificacion } from '@/shared/model/manilla/notificacion.model';
import NotificacionService from './notificacion.service';

const validations: any = {
  notificacion: {
    fechaInicio: {},
    fechaActualizacion: {},
    estado: {},
    tipoNotificacion: {},
  },
};

@Component({
  validations,
})
export default class NotificacionUpdate extends Vue {
  @Inject('notificacionService') private notificacionService: () => NotificacionService;
  @Inject('alertService') private alertService: () => AlertService;

  public notificacion: INotificacion = new Notificacion();

  @Inject('tokenDispService') private tokenDispService: () => TokenDispService;

  public tokenDisps: ITokenDisp[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.notificacionId) {
        vm.retrieveNotificacion(to.params.notificacionId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.notificacion.id) {
      this.notificacionService()
        .update(this.notificacion)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('manillaApp.manillaNotificacion.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.notificacionService()
        .create(this.notificacion)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('manillaApp.manillaNotificacion.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.notificacion[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.notificacion[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.notificacion[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.notificacion[field] = null;
    }
  }

  public retrieveNotificacion(notificacionId): void {
    this.notificacionService()
      .find(notificacionId)
      .then(res => {
        res.fechaInicio = new Date(res.fechaInicio);
        res.fechaActualizacion = new Date(res.fechaActualizacion);
        this.notificacion = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.tokenDispService()
      .retrieve()
      .then(res => {
        this.tokenDisps = res.data;
      });
  }
}
