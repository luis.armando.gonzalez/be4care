import { Component, Vue, Inject } from 'vue-property-decorator';

import { INotificacion } from '@/shared/model/manilla/notificacion.model';
import NotificacionService from './notificacion.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class NotificacionDetails extends Vue {
  @Inject('notificacionService') private notificacionService: () => NotificacionService;
  @Inject('alertService') private alertService: () => AlertService;

  public notificacion: INotificacion = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.notificacionId) {
        vm.retrieveNotificacion(to.params.notificacionId);
      }
    });
  }

  public retrieveNotificacion(notificacionId) {
    this.notificacionService()
      .find(notificacionId)
      .then(res => {
        this.notificacion = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
