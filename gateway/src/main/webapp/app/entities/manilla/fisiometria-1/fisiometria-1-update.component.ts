import { Component, Vue, Inject } from 'vue-property-decorator';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import UserOAuth2Service from '@/entities/user/user.oauth2.service';

import { IFisiometria1, Fisiometria1 } from '@/shared/model/manilla/fisiometria-1.model';
import Fisiometria1Service from './fisiometria-1.service';

const validations: any = {
  fisiometria1: {
    ritmoCardiaco: {},
    ritmoRespiratorio: {},
    oximetria: {},
    presionArterialSistolica: {},
    presionArterialDiastolica: {},
    temperatura: {},
    fechaRegistro: {},
    fechaToma: {},
  },
};

@Component({
  validations,
})
export default class Fisiometria1Update extends Vue {
  @Inject('fisiometria1Service') private fisiometria1Service: () => Fisiometria1Service;
  @Inject('alertService') private alertService: () => AlertService;

  public fisiometria1: IFisiometria1 = new Fisiometria1();

  @Inject('userOAuth2Service') private userOAuth2Service: () => UserOAuth2Service;

  public users: Array<any> = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.fisiometria1Id) {
        vm.retrieveFisiometria1(to.params.fisiometria1Id);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.fisiometria1.id) {
      this.fisiometria1Service()
        .update(this.fisiometria1)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('manillaApp.manillaFisiometria1.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.fisiometria1Service()
        .create(this.fisiometria1)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('manillaApp.manillaFisiometria1.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.fisiometria1[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.fisiometria1[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.fisiometria1[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.fisiometria1[field] = null;
    }
  }

  public retrieveFisiometria1(fisiometria1Id): void {
    this.fisiometria1Service()
      .find(fisiometria1Id)
      .then(res => {
        res.fechaRegistro = new Date(res.fechaRegistro);
        res.fechaToma = new Date(res.fechaToma);
        this.fisiometria1 = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.userOAuth2Service()
      .retrieve()
      .then(res => {
        this.users = res.data;
      });
  }
}
