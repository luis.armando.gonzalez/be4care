import { Component, Vue, Inject } from 'vue-property-decorator';

import { IFisiometria1 } from '@/shared/model/manilla/fisiometria-1.model';
import Fisiometria1Service from './fisiometria-1.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class Fisiometria1Details extends Vue {
  @Inject('fisiometria1Service') private fisiometria1Service: () => Fisiometria1Service;
  @Inject('alertService') private alertService: () => AlertService;

  public fisiometria1: IFisiometria1 = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.fisiometria1Id) {
        vm.retrieveFisiometria1(to.params.fisiometria1Id);
      }
    });
  }

  public retrieveFisiometria1(fisiometria1Id) {
    this.fisiometria1Service()
      .find(fisiometria1Id)
      .then(res => {
        this.fisiometria1 = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
