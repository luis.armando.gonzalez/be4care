import { Component, Vue, Inject } from 'vue-property-decorator';

import { ITemperatura } from '@/shared/model/manilla/temperatura.model';
import TemperaturaService from './temperatura.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class TemperaturaDetails extends Vue {
  @Inject('temperaturaService') private temperaturaService: () => TemperaturaService;
  @Inject('alertService') private alertService: () => AlertService;

  public temperatura: ITemperatura = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.temperaturaId) {
        vm.retrieveTemperatura(to.params.temperaturaId);
      }
    });
  }

  public retrieveTemperatura(temperaturaId) {
    this.temperaturaService()
      .find(temperaturaId)
      .then(res => {
        this.temperatura = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
