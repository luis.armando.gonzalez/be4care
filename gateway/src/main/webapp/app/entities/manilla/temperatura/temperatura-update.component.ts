import { Component, Vue, Inject } from 'vue-property-decorator';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import UserOAuth2Service from '@/entities/user/user.oauth2.service';

import { ITemperatura, Temperatura } from '@/shared/model/manilla/temperatura.model';
import TemperaturaService from './temperatura.service';

const validations: any = {
  temperatura: {
    temperatura: {},
    fechaRegistro: {},
  },
};

@Component({
  validations,
})
export default class TemperaturaUpdate extends Vue {
  @Inject('temperaturaService') private temperaturaService: () => TemperaturaService;
  @Inject('alertService') private alertService: () => AlertService;

  public temperatura: ITemperatura = new Temperatura();

  @Inject('userOAuth2Service') private userOAuth2Service: () => UserOAuth2Service;

  public users: Array<any> = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.temperaturaId) {
        vm.retrieveTemperatura(to.params.temperaturaId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.temperatura.id) {
      this.temperaturaService()
        .update(this.temperatura)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('manillaApp.manillaTemperatura.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.temperaturaService()
        .create(this.temperatura)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('manillaApp.manillaTemperatura.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.temperatura[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.temperatura[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.temperatura[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.temperatura[field] = null;
    }
  }

  public retrieveTemperatura(temperaturaId): void {
    this.temperaturaService()
      .find(temperaturaId)
      .then(res => {
        res.fechaRegistro = new Date(res.fechaRegistro);
        this.temperatura = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.userOAuth2Service()
      .retrieve()
      .then(res => {
        this.users = res.data;
      });
  }
}
