import { Component, Vue, Inject } from 'vue-property-decorator';

import { ICaloria } from '@/shared/model/manilla/caloria.model';
import CaloriaService from './caloria.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class CaloriaDetails extends Vue {
  @Inject('caloriaService') private caloriaService: () => CaloriaService;
  @Inject('alertService') private alertService: () => AlertService;

  public caloria: ICaloria = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.caloriaId) {
        vm.retrieveCaloria(to.params.caloriaId);
      }
    });
  }

  public retrieveCaloria(caloriaId) {
    this.caloriaService()
      .find(caloriaId)
      .then(res => {
        this.caloria = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
