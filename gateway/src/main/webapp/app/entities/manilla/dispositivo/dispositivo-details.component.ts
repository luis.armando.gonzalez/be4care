import { Component, Vue, Inject } from 'vue-property-decorator';

import { IDispositivo } from '@/shared/model/manilla/dispositivo.model';
import DispositivoService from './dispositivo.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class DispositivoDetails extends Vue {
  @Inject('dispositivoService') private dispositivoService: () => DispositivoService;
  @Inject('alertService') private alertService: () => AlertService;

  public dispositivo: IDispositivo = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.dispositivoId) {
        vm.retrieveDispositivo(to.params.dispositivoId);
      }
    });
  }

  public retrieveDispositivo(dispositivoId) {
    this.dispositivoService()
      .find(dispositivoId)
      .then(res => {
        this.dispositivo = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
