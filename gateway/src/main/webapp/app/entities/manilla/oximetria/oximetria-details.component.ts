import { Component, Vue, Inject } from 'vue-property-decorator';

import { IOximetria } from '@/shared/model/manilla/oximetria.model';
import OximetriaService from './oximetria.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class OximetriaDetails extends Vue {
  @Inject('oximetriaService') private oximetriaService: () => OximetriaService;
  @Inject('alertService') private alertService: () => AlertService;

  public oximetria: IOximetria = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.oximetriaId) {
        vm.retrieveOximetria(to.params.oximetriaId);
      }
    });
  }

  public retrieveOximetria(oximetriaId) {
    this.oximetriaService()
      .find(oximetriaId)
      .then(res => {
        this.oximetria = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
