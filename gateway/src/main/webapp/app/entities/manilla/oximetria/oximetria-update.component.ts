import { Component, Vue, Inject } from 'vue-property-decorator';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import UserOAuth2Service from '@/entities/user/user.oauth2.service';

import { IOximetria, Oximetria } from '@/shared/model/manilla/oximetria.model';
import OximetriaService from './oximetria.service';

const validations: any = {
  oximetria: {
    oximetria: {},
    fechaRegistro: {},
  },
};

@Component({
  validations,
})
export default class OximetriaUpdate extends Vue {
  @Inject('oximetriaService') private oximetriaService: () => OximetriaService;
  @Inject('alertService') private alertService: () => AlertService;

  public oximetria: IOximetria = new Oximetria();

  @Inject('userOAuth2Service') private userOAuth2Service: () => UserOAuth2Service;

  public users: Array<any> = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.oximetriaId) {
        vm.retrieveOximetria(to.params.oximetriaId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.oximetria.id) {
      this.oximetriaService()
        .update(this.oximetria)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('manillaApp.manillaOximetria.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.oximetriaService()
        .create(this.oximetria)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('manillaApp.manillaOximetria.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.oximetria[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.oximetria[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.oximetria[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.oximetria[field] = null;
    }
  }

  public retrieveOximetria(oximetriaId): void {
    this.oximetriaService()
      .find(oximetriaId)
      .then(res => {
        res.fechaRegistro = new Date(res.fechaRegistro);
        this.oximetria = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.userOAuth2Service()
      .retrieve()
      .then(res => {
        this.users = res.data;
      });
  }
}
