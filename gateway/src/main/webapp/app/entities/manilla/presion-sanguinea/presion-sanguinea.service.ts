import axios from 'axios';

import buildPaginationQueryOpts from '@/shared/sort/sorts';

import { IPresionSanguinea } from '@/shared/model/manilla/presion-sanguinea.model';

const baseApiUrl = 'services/manilla/api/presion-sanguineas';

export default class PresionSanguineaService {
  public find(id: number): Promise<IPresionSanguinea> {
    return new Promise<IPresionSanguinea>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public retrieve(paginationQuery?: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(baseApiUrl + `?${buildPaginationQueryOpts(paginationQuery)}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public delete(id: number): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .delete(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public create(entity: IPresionSanguinea): Promise<IPresionSanguinea> {
    return new Promise<IPresionSanguinea>((resolve, reject) => {
      axios
        .post(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public update(entity: IPresionSanguinea): Promise<IPresionSanguinea> {
    return new Promise<IPresionSanguinea>((resolve, reject) => {
      axios
        .put(`${baseApiUrl}/${entity.id}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public partialUpdate(entity: IPresionSanguinea): Promise<IPresionSanguinea> {
    return new Promise<IPresionSanguinea>((resolve, reject) => {
      axios
        .patch(`${baseApiUrl}/${entity.id}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}
