import { Component, Vue, Inject } from 'vue-property-decorator';

import { IPresionSanguinea } from '@/shared/model/manilla/presion-sanguinea.model';
import PresionSanguineaService from './presion-sanguinea.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class PresionSanguineaDetails extends Vue {
  @Inject('presionSanguineaService') private presionSanguineaService: () => PresionSanguineaService;
  @Inject('alertService') private alertService: () => AlertService;

  public presionSanguinea: IPresionSanguinea = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.presionSanguineaId) {
        vm.retrievePresionSanguinea(to.params.presionSanguineaId);
      }
    });
  }

  public retrievePresionSanguinea(presionSanguineaId) {
    this.presionSanguineaService()
      .find(presionSanguineaId)
      .then(res => {
        this.presionSanguinea = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
