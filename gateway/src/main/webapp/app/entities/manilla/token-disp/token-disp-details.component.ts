import { Component, Vue, Inject } from 'vue-property-decorator';

import { ITokenDisp } from '@/shared/model/manilla/token-disp.model';
import TokenDispService from './token-disp.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class TokenDispDetails extends Vue {
  @Inject('tokenDispService') private tokenDispService: () => TokenDispService;
  @Inject('alertService') private alertService: () => AlertService;

  public tokenDisp: ITokenDisp = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.tokenDispId) {
        vm.retrieveTokenDisp(to.params.tokenDispId);
      }
    });
  }

  public retrieveTokenDisp(tokenDispId) {
    this.tokenDispService()
      .find(tokenDispId)
      .then(res => {
        this.tokenDisp = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
