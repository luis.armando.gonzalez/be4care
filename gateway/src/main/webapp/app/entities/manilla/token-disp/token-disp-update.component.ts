import { Component, Vue, Inject } from 'vue-property-decorator';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import UserOAuth2Service from '@/entities/user/user.oauth2.service';

import { ITokenDisp, TokenDisp } from '@/shared/model/manilla/token-disp.model';
import TokenDispService from './token-disp.service';

const validations: any = {
  tokenDisp: {
    tokenConexion: {},
    activo: {},
    fechaInicio: {},
    fechaFin: {},
  },
};

@Component({
  validations,
})
export default class TokenDispUpdate extends Vue {
  @Inject('tokenDispService') private tokenDispService: () => TokenDispService;
  @Inject('alertService') private alertService: () => AlertService;

  public tokenDisp: ITokenDisp = new TokenDisp();

  @Inject('userOAuth2Service') private userOAuth2Service: () => UserOAuth2Service;

  public users: Array<any> = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.tokenDispId) {
        vm.retrieveTokenDisp(to.params.tokenDispId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.tokenDisp.id) {
      this.tokenDispService()
        .update(this.tokenDisp)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('manillaApp.manillaTokenDisp.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.tokenDispService()
        .create(this.tokenDisp)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('manillaApp.manillaTokenDisp.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.tokenDisp[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.tokenDisp[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.tokenDisp[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.tokenDisp[field] = null;
    }
  }

  public retrieveTokenDisp(tokenDispId): void {
    this.tokenDispService()
      .find(tokenDispId)
      .then(res => {
        res.fechaInicio = new Date(res.fechaInicio);
        res.fechaFin = new Date(res.fechaFin);
        this.tokenDisp = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.userOAuth2Service()
      .retrieve()
      .then(res => {
        this.users = res.data;
      });
  }
}
