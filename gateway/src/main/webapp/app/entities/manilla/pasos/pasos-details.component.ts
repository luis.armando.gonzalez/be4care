import { Component, Vue, Inject } from 'vue-property-decorator';

import { IPasos } from '@/shared/model/manilla/pasos.model';
import PasosService from './pasos.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class PasosDetails extends Vue {
  @Inject('pasosService') private pasosService: () => PasosService;
  @Inject('alertService') private alertService: () => AlertService;

  public pasos: IPasos = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.pasosId) {
        vm.retrievePasos(to.params.pasosId);
      }
    });
  }

  public retrievePasos(pasosId) {
    this.pasosService()
      .find(pasosId)
      .then(res => {
        this.pasos = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
