import { Component, Vue, Inject } from 'vue-property-decorator';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import UserOAuth2Service from '@/entities/user/user.oauth2.service';

import { IPasos, Pasos } from '@/shared/model/manilla/pasos.model';
import PasosService from './pasos.service';

const validations: any = {
  pasos: {
    nroPasos: {},
    timeInstant: {},
  },
};

@Component({
  validations,
})
export default class PasosUpdate extends Vue {
  @Inject('pasosService') private pasosService: () => PasosService;
  @Inject('alertService') private alertService: () => AlertService;

  public pasos: IPasos = new Pasos();

  @Inject('userOAuth2Service') private userOAuth2Service: () => UserOAuth2Service;

  public users: Array<any> = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.pasosId) {
        vm.retrievePasos(to.params.pasosId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.pasos.id) {
      this.pasosService()
        .update(this.pasos)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('manillaApp.manillaPasos.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.pasosService()
        .create(this.pasos)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('manillaApp.manillaPasos.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.pasos[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.pasos[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.pasos[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.pasos[field] = null;
    }
  }

  public retrievePasos(pasosId): void {
    this.pasosService()
      .find(pasosId)
      .then(res => {
        res.timeInstant = new Date(res.timeInstant);
        this.pasos = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.userOAuth2Service()
      .retrieve()
      .then(res => {
        this.users = res.data;
      });
  }
}
