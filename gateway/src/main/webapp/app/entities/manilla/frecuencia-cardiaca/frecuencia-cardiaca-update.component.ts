import { Component, Vue, Inject } from 'vue-property-decorator';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import UserOAuth2Service from '@/entities/user/user.oauth2.service';

import { IFrecuenciaCardiaca, FrecuenciaCardiaca } from '@/shared/model/manilla/frecuencia-cardiaca.model';
import FrecuenciaCardiacaService from './frecuencia-cardiaca.service';

const validations: any = {
  frecuenciaCardiaca: {
    frecuenciaCardiaca: {},
    fechaRegistro: {},
  },
};

@Component({
  validations,
})
export default class FrecuenciaCardiacaUpdate extends Vue {
  @Inject('frecuenciaCardiacaService') private frecuenciaCardiacaService: () => FrecuenciaCardiacaService;
  @Inject('alertService') private alertService: () => AlertService;

  public frecuenciaCardiaca: IFrecuenciaCardiaca = new FrecuenciaCardiaca();

  @Inject('userOAuth2Service') private userOAuth2Service: () => UserOAuth2Service;

  public users: Array<any> = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.frecuenciaCardiacaId) {
        vm.retrieveFrecuenciaCardiaca(to.params.frecuenciaCardiacaId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.frecuenciaCardiaca.id) {
      this.frecuenciaCardiacaService()
        .update(this.frecuenciaCardiaca)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('manillaApp.manillaFrecuenciaCardiaca.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.frecuenciaCardiacaService()
        .create(this.frecuenciaCardiaca)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('manillaApp.manillaFrecuenciaCardiaca.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.frecuenciaCardiaca[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.frecuenciaCardiaca[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.frecuenciaCardiaca[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.frecuenciaCardiaca[field] = null;
    }
  }

  public retrieveFrecuenciaCardiaca(frecuenciaCardiacaId): void {
    this.frecuenciaCardiacaService()
      .find(frecuenciaCardiacaId)
      .then(res => {
        res.fechaRegistro = new Date(res.fechaRegistro);
        this.frecuenciaCardiaca = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.userOAuth2Service()
      .retrieve()
      .then(res => {
        this.users = res.data;
      });
  }
}
