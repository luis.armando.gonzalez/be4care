import { Component, Vue, Inject } from 'vue-property-decorator';

import { IFrecuenciaCardiaca } from '@/shared/model/manilla/frecuencia-cardiaca.model';
import FrecuenciaCardiacaService from './frecuencia-cardiaca.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class FrecuenciaCardiacaDetails extends Vue {
  @Inject('frecuenciaCardiacaService') private frecuenciaCardiacaService: () => FrecuenciaCardiacaService;
  @Inject('alertService') private alertService: () => AlertService;

  public frecuenciaCardiaca: IFrecuenciaCardiaca = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.frecuenciaCardiacaId) {
        vm.retrieveFrecuenciaCardiaca(to.params.frecuenciaCardiacaId);
      }
    });
  }

  public retrieveFrecuenciaCardiaca(frecuenciaCardiacaId) {
    this.frecuenciaCardiacaService()
      .find(frecuenciaCardiacaId)
      .then(res => {
        this.frecuenciaCardiaca = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
