import { Component, Vue, Inject } from 'vue-property-decorator';

import { IIngesta } from '@/shared/model/manilla/ingesta.model';
import IngestaService from './ingesta.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class IngestaDetails extends Vue {
  @Inject('ingestaService') private ingestaService: () => IngestaService;
  @Inject('alertService') private alertService: () => AlertService;

  public ingesta: IIngesta = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.ingestaId) {
        vm.retrieveIngesta(to.params.ingestaId);
      }
    });
  }

  public retrieveIngesta(ingestaId) {
    this.ingestaService()
      .find(ingestaId)
      .then(res => {
        this.ingesta = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
