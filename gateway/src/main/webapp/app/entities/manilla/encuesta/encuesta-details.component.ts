import { Component, Vue, Inject } from 'vue-property-decorator';

import { IEncuesta } from '@/shared/model/manilla/encuesta.model';
import EncuestaService from './encuesta.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class EncuestaDetails extends Vue {
  @Inject('encuestaService') private encuestaService: () => EncuestaService;
  @Inject('alertService') private alertService: () => AlertService;

  public encuesta: IEncuesta = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.encuestaId) {
        vm.retrieveEncuesta(to.params.encuestaId);
      }
    });
  }

  public retrieveEncuesta(encuestaId) {
    this.encuestaService()
      .find(encuestaId)
      .then(res => {
        this.encuesta = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
