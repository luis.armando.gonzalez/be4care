import { Component, Vue, Inject } from 'vue-property-decorator';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import UserOAuth2Service from '@/entities/user/user.oauth2.service';

import { IEncuesta, Encuesta } from '@/shared/model/manilla/encuesta.model';
import EncuestaService from './encuesta.service';

const validations: any = {
  encuesta: {
    fecha: {},
    debilidad: {},
    cefalea: {},
    calambres: {},
    nauseas: {},
    vomito: {},
    mareo: {},
    ninguna: {},
  },
};

@Component({
  validations,
})
export default class EncuestaUpdate extends Vue {
  @Inject('encuestaService') private encuestaService: () => EncuestaService;
  @Inject('alertService') private alertService: () => AlertService;

  public encuesta: IEncuesta = new Encuesta();

  @Inject('userOAuth2Service') private userOAuth2Service: () => UserOAuth2Service;

  public users: Array<any> = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.encuestaId) {
        vm.retrieveEncuesta(to.params.encuestaId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.encuesta.id) {
      this.encuestaService()
        .update(this.encuesta)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('manillaApp.manillaEncuesta.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.encuestaService()
        .create(this.encuesta)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('manillaApp.manillaEncuesta.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.encuesta[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.encuesta[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.encuesta[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.encuesta[field] = null;
    }
  }

  public retrieveEncuesta(encuestaId): void {
    this.encuestaService()
      .find(encuestaId)
      .then(res => {
        res.fecha = new Date(res.fecha);
        this.encuesta = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.userOAuth2Service()
      .retrieve()
      .then(res => {
        this.users = res.data;
      });
  }
}
