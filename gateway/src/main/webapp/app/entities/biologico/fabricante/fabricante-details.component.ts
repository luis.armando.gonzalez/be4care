import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { IFabricante } from '@/shared/model/biologico/fabricante.model';
import FabricanteService from './fabricante.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class FabricanteDetails extends mixins(JhiDataUtils) {
  @Inject('fabricanteService') private fabricanteService: () => FabricanteService;
  @Inject('alertService') private alertService: () => AlertService;

  public fabricante: IFabricante = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.fabricanteId) {
        vm.retrieveFabricante(to.params.fabricanteId);
      }
    });
  }

  public retrieveFabricante(fabricanteId) {
    this.fabricanteService()
      .find(fabricanteId)
      .then(res => {
        this.fabricante = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
