import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import AlertService from '@/shared/alert/alert.service';

import { IFabricante, Fabricante } from '@/shared/model/biologico/fabricante.model';
import FabricanteService from './fabricante.service';

const validations: any = {
  fabricante: {
    nombreFabricante: {},
    productosFabricanet: {},
    detalleFabricante: {},
  },
};

@Component({
  validations,
})
export default class FabricanteUpdate extends mixins(JhiDataUtils) {
  @Inject('fabricanteService') private fabricanteService: () => FabricanteService;
  @Inject('alertService') private alertService: () => AlertService;

  public fabricante: IFabricante = new Fabricante();
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.fabricanteId) {
        vm.retrieveFabricante(to.params.fabricanteId);
      }
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.fabricante.id) {
      this.fabricanteService()
        .update(this.fabricante)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('biologicoApp.biologicoFabricante.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.fabricanteService()
        .create(this.fabricante)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('biologicoApp.biologicoFabricante.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveFabricante(fabricanteId): void {
    this.fabricanteService()
      .find(fabricanteId)
      .then(res => {
        this.fabricante = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
