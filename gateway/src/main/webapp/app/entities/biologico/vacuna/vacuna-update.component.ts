import { Component, Vue, Inject } from 'vue-property-decorator';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import FabricanteService from '@/entities/biologico/fabricante/fabricante.service';
import { IFabricante } from '@/shared/model/biologico/fabricante.model';

import { IVacuna, Vacuna } from '@/shared/model/biologico/vacuna.model';
import VacunaService from './vacuna.service';

const validations: any = {
  vacuna: {
    nombreVacuna: {},
    tipoVacuna: {},
    descripcionVacuna: {},
    fechaIngreso: {},
    loteVacuna: {},
  },
};

@Component({
  validations,
})
export default class VacunaUpdate extends Vue {
  @Inject('vacunaService') private vacunaService: () => VacunaService;
  @Inject('alertService') private alertService: () => AlertService;

  public vacuna: IVacuna = new Vacuna();

  @Inject('fabricanteService') private fabricanteService: () => FabricanteService;

  public fabricantes: IFabricante[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.vacunaId) {
        vm.retrieveVacuna(to.params.vacunaId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.vacuna.id) {
      this.vacunaService()
        .update(this.vacuna)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('biologicoApp.biologicoVacuna.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.vacunaService()
        .create(this.vacuna)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('biologicoApp.biologicoVacuna.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.vacuna[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.vacuna[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.vacuna[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.vacuna[field] = null;
    }
  }

  public retrieveVacuna(vacunaId): void {
    this.vacunaService()
      .find(vacunaId)
      .then(res => {
        res.fechaIngreso = new Date(res.fechaIngreso);
        this.vacuna = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.fabricanteService()
      .retrieve()
      .then(res => {
        this.fabricantes = res.data;
      });
  }
}
