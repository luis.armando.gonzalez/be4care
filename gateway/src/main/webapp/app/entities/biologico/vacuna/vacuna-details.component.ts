import { Component, Vue, Inject } from 'vue-property-decorator';

import { IVacuna } from '@/shared/model/biologico/vacuna.model';
import VacunaService from './vacuna.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class VacunaDetails extends Vue {
  @Inject('vacunaService') private vacunaService: () => VacunaService;
  @Inject('alertService') private alertService: () => AlertService;

  public vacuna: IVacuna = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.vacunaId) {
        vm.retrieveVacuna(to.params.vacunaId);
      }
    });
  }

  public retrieveVacuna(vacunaId) {
    this.vacunaService()
      .find(vacunaId)
      .then(res => {
        this.vacuna = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
