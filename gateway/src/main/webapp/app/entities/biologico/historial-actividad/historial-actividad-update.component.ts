import { Component, Vue, Inject } from 'vue-property-decorator';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import CicloActividadService from '@/entities/biologico/ciclo-actividad/ciclo-actividad.service';
import { ICicloActividad } from '@/shared/model/biologico/ciclo-actividad.model';

import { IHistorialActividad, HistorialActividad } from '@/shared/model/biologico/historial-actividad.model';
import HistorialActividadService from './historial-actividad.service';

const validations: any = {
  historialActividad: {
    fechaHistorial: {},
  },
};

@Component({
  validations,
})
export default class HistorialActividadUpdate extends Vue {
  @Inject('historialActividadService') private historialActividadService: () => HistorialActividadService;
  @Inject('alertService') private alertService: () => AlertService;

  public historialActividad: IHistorialActividad = new HistorialActividad();

  @Inject('cicloActividadService') private cicloActividadService: () => CicloActividadService;

  public cicloActividads: ICicloActividad[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.historialActividadId) {
        vm.retrieveHistorialActividad(to.params.historialActividadId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.historialActividad.id) {
      this.historialActividadService()
        .update(this.historialActividad)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('biologicoApp.biologicoHistorialActividad.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.historialActividadService()
        .create(this.historialActividad)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('biologicoApp.biologicoHistorialActividad.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.historialActividad[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.historialActividad[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.historialActividad[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.historialActividad[field] = null;
    }
  }

  public retrieveHistorialActividad(historialActividadId): void {
    this.historialActividadService()
      .find(historialActividadId)
      .then(res => {
        res.fechaHistorial = new Date(res.fechaHistorial);
        this.historialActividad = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.cicloActividadService()
      .retrieve()
      .then(res => {
        this.cicloActividads = res.data;
      });
  }
}
