import { Component, Vue, Inject } from 'vue-property-decorator';

import { IHistorialActividad } from '@/shared/model/biologico/historial-actividad.model';
import HistorialActividadService from './historial-actividad.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class HistorialActividadDetails extends Vue {
  @Inject('historialActividadService') private historialActividadService: () => HistorialActividadService;
  @Inject('alertService') private alertService: () => AlertService;

  public historialActividad: IHistorialActividad = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.historialActividadId) {
        vm.retrieveHistorialActividad(to.params.historialActividadId);
      }
    });
  }

  public retrieveHistorialActividad(historialActividadId) {
    this.historialActividadService()
      .find(historialActividadId)
      .then(res => {
        this.historialActividad = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
