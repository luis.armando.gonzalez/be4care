import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import { IBiologico, Biologico } from '@/shared/model/biologico/biologico.model';
import BiologicoService from './biologico.service';

const validations: any = {
  biologico: {
    tipoBiologico: {},
    detalleBiologico: {},
  },
};

@Component({
  validations,
})
export default class BiologicoUpdate extends Vue {
  @Inject('biologicoService') private biologicoService: () => BiologicoService;
  @Inject('alertService') private alertService: () => AlertService;

  public biologico: IBiologico = new Biologico();
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.biologicoId) {
        vm.retrieveBiologico(to.params.biologicoId);
      }
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.biologico.id) {
      this.biologicoService()
        .update(this.biologico)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('biologicoApp.biologicoBiologico.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.biologicoService()
        .create(this.biologico)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('biologicoApp.biologicoBiologico.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveBiologico(biologicoId): void {
    this.biologicoService()
      .find(biologicoId)
      .then(res => {
        this.biologico = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
