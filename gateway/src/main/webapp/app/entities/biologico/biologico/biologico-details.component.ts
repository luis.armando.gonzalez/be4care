import { Component, Vue, Inject } from 'vue-property-decorator';

import { IBiologico } from '@/shared/model/biologico/biologico.model';
import BiologicoService from './biologico.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class BiologicoDetails extends Vue {
  @Inject('biologicoService') private biologicoService: () => BiologicoService;
  @Inject('alertService') private alertService: () => AlertService;

  public biologico: IBiologico = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.biologicoId) {
        vm.retrieveBiologico(to.params.biologicoId);
      }
    });
  }

  public retrieveBiologico(biologicoId) {
    this.biologicoService()
      .find(biologicoId)
      .then(res => {
        this.biologico = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
