import { Component, Vue, Inject } from 'vue-property-decorator';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import BiologicoService from '@/entities/biologico/biologico/biologico.service';
import { IBiologico } from '@/shared/model/biologico/biologico.model';

import VacunaService from '@/entities/biologico/vacuna/vacuna.service';
import { IVacuna } from '@/shared/model/biologico/vacuna.model';

import UserOAuth2Service from '@/entities/user/user.oauth2.service';

import { ICicloActividad, CicloActividad } from '@/shared/model/biologico/ciclo-actividad.model';
import CicloActividadService from './ciclo-actividad.service';

const validations: any = {
  cicloActividad: {
    periodoCiclo: {},
    cantidad: {},
    fechaInicioCiclo: {},
  },
};

@Component({
  validations,
})
export default class CicloActividadUpdate extends Vue {
  @Inject('cicloActividadService') private cicloActividadService: () => CicloActividadService;
  @Inject('alertService') private alertService: () => AlertService;

  public cicloActividad: ICicloActividad = new CicloActividad();

  @Inject('biologicoService') private biologicoService: () => BiologicoService;

  public biologicos: IBiologico[] = [];

  @Inject('vacunaService') private vacunaService: () => VacunaService;

  public vacunas: IVacuna[] = [];

  @Inject('userOAuth2Service') private userOAuth2Service: () => UserOAuth2Service;

  public users: Array<any> = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.cicloActividadId) {
        vm.retrieveCicloActividad(to.params.cicloActividadId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.cicloActividad.id) {
      this.cicloActividadService()
        .update(this.cicloActividad)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('biologicoApp.biologicoCicloActividad.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.cicloActividadService()
        .create(this.cicloActividad)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('biologicoApp.biologicoCicloActividad.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.cicloActividad[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.cicloActividad[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.cicloActividad[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.cicloActividad[field] = null;
    }
  }

  public retrieveCicloActividad(cicloActividadId): void {
    this.cicloActividadService()
      .find(cicloActividadId)
      .then(res => {
        res.fechaInicioCiclo = new Date(res.fechaInicioCiclo);
        this.cicloActividad = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.biologicoService()
      .retrieve()
      .then(res => {
        this.biologicos = res.data;
      });
    this.vacunaService()
      .retrieve()
      .then(res => {
        this.vacunas = res.data;
      });
    this.userOAuth2Service()
      .retrieve()
      .then(res => {
        this.users = res.data;
      });
  }
}
