import { Component, Vue, Inject } from 'vue-property-decorator';

import { ICicloActividad } from '@/shared/model/biologico/ciclo-actividad.model';
import CicloActividadService from './ciclo-actividad.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class CicloActividadDetails extends Vue {
  @Inject('cicloActividadService') private cicloActividadService: () => CicloActividadService;
  @Inject('alertService') private alertService: () => AlertService;

  public cicloActividad: ICicloActividad = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.cicloActividadId) {
        vm.retrieveCicloActividad(to.params.cicloActividadId);
      }
    });
  }

  public retrieveCicloActividad(cicloActividadId) {
    this.cicloActividadService()
      .find(cicloActividadId)
      .then(res => {
        this.cicloActividad = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
