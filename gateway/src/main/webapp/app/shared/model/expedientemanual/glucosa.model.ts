import { IUser } from '@/shared/model/user.model';

export interface IGlucosa {
  id?: number;
  glucosaUser?: number | null;
  fechaRegistro?: Date | null;
  user?: IUser | null;
}

export class Glucosa implements IGlucosa {
  constructor(public id?: number, public glucosaUser?: number | null, public fechaRegistro?: Date | null, public user?: IUser | null) {}
}
