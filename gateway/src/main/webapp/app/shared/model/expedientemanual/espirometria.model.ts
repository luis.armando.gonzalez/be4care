import { IUser } from '@/shared/model/user.model';

export interface IEspirometria {
  id?: number;
  fev1T?: number | null;
  fev1R?: number | null;
  fvcT?: number | null;
  fvcR?: number | null;
  indiceTP?: number | null;
  fechaToma?: Date | null;
  resultado?: string | null;
  user?: IUser | null;
}

export class Espirometria implements IEspirometria {
  constructor(
    public id?: number,
    public fev1T?: number | null,
    public fev1R?: number | null,
    public fvcT?: number | null,
    public fvcR?: number | null,
    public indiceTP?: number | null,
    public fechaToma?: Date | null,
    public resultado?: string | null,
    public user?: IUser | null
  ) {}
}
