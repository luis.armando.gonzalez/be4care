import { IUser } from '@/shared/model/user.model';

export interface IPeso {
  id?: number;
  pesoKG?: number | null;
  descripcion?: string | null;
  fechaRegistro?: Date | null;
  user?: IUser | null;
}

export class Peso implements IPeso {
  constructor(
    public id?: number,
    public pesoKG?: number | null,
    public descripcion?: string | null,
    public fechaRegistro?: Date | null,
    public user?: IUser | null
  ) {}
}
