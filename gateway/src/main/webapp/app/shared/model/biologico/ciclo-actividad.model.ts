import { IBiologico } from '@/shared/model/biologico/biologico.model';
import { IVacuna } from '@/shared/model/biologico/vacuna.model';
import { IUser } from '@/shared/model/user.model';

export interface ICicloActividad {
  id?: number;
  periodoCiclo?: number | null;
  cantidad?: number | null;
  fechaInicioCiclo?: Date | null;
  biologico?: IBiologico | null;
  vacuna?: IVacuna | null;
  user?: IUser | null;
}

export class CicloActividad implements ICicloActividad {
  constructor(
    public id?: number,
    public periodoCiclo?: number | null,
    public cantidad?: number | null,
    public fechaInicioCiclo?: Date | null,
    public biologico?: IBiologico | null,
    public vacuna?: IVacuna | null,
    public user?: IUser | null
  ) {}
}
