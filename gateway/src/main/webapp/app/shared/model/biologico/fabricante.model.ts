export interface IFabricante {
  id?: number;
  nombreFabricante?: string | null;
  productosFabricanet?: string | null;
  detalleFabricante?: string | null;
}

export class Fabricante implements IFabricante {
  constructor(
    public id?: number,
    public nombreFabricante?: string | null,
    public productosFabricanet?: string | null,
    public detalleFabricante?: string | null
  ) {}
}
