export interface IBiologico {
  id?: number;
  tipoBiologico?: string | null;
  detalleBiologico?: string | null;
}

export class Biologico implements IBiologico {
  constructor(public id?: number, public tipoBiologico?: string | null, public detalleBiologico?: string | null) {}
}
