import { ICicloActividad } from '@/shared/model/biologico/ciclo-actividad.model';

export interface IHistorialActividad {
  id?: number;
  fechaHistorial?: Date | null;
  cicloActividad?: ICicloActividad | null;
}

export class HistorialActividad implements IHistorialActividad {
  constructor(public id?: number, public fechaHistorial?: Date | null, public cicloActividad?: ICicloActividad | null) {}
}
