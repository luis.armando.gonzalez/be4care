import { IFabricante } from '@/shared/model/biologico/fabricante.model';

export interface IVacuna {
  id?: number;
  nombreVacuna?: string | null;
  tipoVacuna?: string | null;
  descripcionVacuna?: string | null;
  fechaIngreso?: Date | null;
  loteVacuna?: string | null;
  fabricante?: IFabricante | null;
}

export class Vacuna implements IVacuna {
  constructor(
    public id?: number,
    public nombreVacuna?: string | null,
    public tipoVacuna?: string | null,
    public descripcionVacuna?: string | null,
    public fechaIngreso?: Date | null,
    public loteVacuna?: string | null,
    public fabricante?: IFabricante | null
  ) {}
}
