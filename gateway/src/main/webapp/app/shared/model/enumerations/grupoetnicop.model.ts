export enum Grupoetnicop {
  Afrocolombiano = 'Afrocolombiano',

  Palenquero = 'Palenquero',

  Raizal = 'Raizal',

  Indigena = 'Indigena',

  RomGitano = 'RomGitano',

  Mestizo = 'Mestizo',

  Ninguno = 'Ninguno',

  Otro = 'Otro',
}
