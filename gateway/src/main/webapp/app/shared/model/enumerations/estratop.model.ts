export enum Estratop {
  Estrato_1 = 'Estrato_1',

  Estrato_2 = 'Estrato_2',

  Estrato_3 = 'Estrato_3',

  Estrato_4 = 'Estrato_4',

  Estrato_5 = 'Estrato_5',

  Estrato_especial = 'Estrato_especial',
}
