export enum Sexop {
  FEMENINO = 'FEMENINO',

  MASCULINO = 'MASCULINO',

  OTRO = 'OTRO',
}
