export enum Presentacion {
  Comprimido = 'Comprimido',

  Gragea = 'Gragea',

  Capsula = 'Capsula',

  Polvo = 'Polvo',

  Pildora = 'Pildora',

  Unguento = 'Unguento',

  Pomada = 'Pomada',

  Crema = 'Crema',

  Pasta = 'Pasta',

  Solucion = 'Solucion',

  Jarabe = 'Jarabe',

  Suspension = 'Suspension',

  Emulsion = 'Emulsion',

  Locion = 'Locion',
}
