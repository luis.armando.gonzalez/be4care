import { IUser } from '@/shared/model/user.model';

export interface IFisiometria2 {
  id?: number;
  timeInstant?: Date | null;
  meditacion?: number | null;
  attention?: number | null;
  delta?: number | null;
  theta?: number | null;
  lowAlpha?: number | null;
  highAlpha?: number | null;
  lowBeta?: number | null;
  highBeta?: number | null;
  lowGamma?: number | null;
  midGamma?: number | null;
  pulso?: number | null;
  respiracion?: number | null;
  acelerometro?: number | null;
  estadoFuncional?: number | null;
  user?: IUser | null;
}

export class Fisiometria2 implements IFisiometria2 {
  constructor(
    public id?: number,
    public timeInstant?: Date | null,
    public meditacion?: number | null,
    public attention?: number | null,
    public delta?: number | null,
    public theta?: number | null,
    public lowAlpha?: number | null,
    public highAlpha?: number | null,
    public lowBeta?: number | null,
    public highBeta?: number | null,
    public lowGamma?: number | null,
    public midGamma?: number | null,
    public pulso?: number | null,
    public respiracion?: number | null,
    public acelerometro?: number | null,
    public estadoFuncional?: number | null,
    public user?: IUser | null
  ) {}
}
