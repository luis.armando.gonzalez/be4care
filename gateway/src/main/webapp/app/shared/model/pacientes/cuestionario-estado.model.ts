import { IPregunta } from '@/shared/model/pacientes/pregunta.model';

export interface ICuestionarioEstado {
  id?: number;
  valor?: number | null;
  valoracion?: string | null;
  pregunta?: IPregunta | null;
}

export class CuestionarioEstado implements ICuestionarioEstado {
  constructor(public id?: number, public valor?: number | null, public valoracion?: string | null, public pregunta?: IPregunta | null) {}
}
