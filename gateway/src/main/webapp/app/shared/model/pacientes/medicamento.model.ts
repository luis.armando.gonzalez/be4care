import { Presentacion } from '@/shared/model/enumerations/presentacion.model';
export interface IMedicamento {
  id?: number;
  nombre?: string | null;
  descripcion?: string | null;
  fechaIngreso?: Date | null;
  presentacion?: Presentacion | null;
  generico?: string | null;
}

export class Medicamento implements IMedicamento {
  constructor(
    public id?: number,
    public nombre?: string | null,
    public descripcion?: string | null,
    public fechaIngreso?: Date | null,
    public presentacion?: Presentacion | null,
    public generico?: string | null
  ) {}
}
