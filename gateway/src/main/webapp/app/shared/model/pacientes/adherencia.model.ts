import { IMedicamento } from '@/shared/model/pacientes/medicamento.model';
import { IPaciente } from '@/shared/model/pacientes/paciente.model';

export interface IAdherencia {
  id?: number;
  horaToma?: Date | null;
  respuesta?: boolean | null;
  valor?: number | null;
  comentario?: string | null;
  medicamento?: IMedicamento | null;
  paciente?: IPaciente | null;
}

export class Adherencia implements IAdherencia {
  constructor(
    public id?: number,
    public horaToma?: Date | null,
    public respuesta?: boolean | null,
    public valor?: number | null,
    public comentario?: string | null,
    public medicamento?: IMedicamento | null,
    public paciente?: IPaciente | null
  ) {
    this.respuesta = this.respuesta ?? false;
  }
}
