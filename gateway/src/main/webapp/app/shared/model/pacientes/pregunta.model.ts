import { ICondicion } from '@/shared/model/pacientes/condicion.model';

export interface IPregunta {
  id?: number;
  pregunta?: string | null;
  condicion?: ICondicion | null;
}

export class Pregunta implements IPregunta {
  constructor(public id?: number, public pregunta?: string | null, public condicion?: ICondicion | null) {}
}
