export interface ITratamiento {
  id?: number;
  descripcionTratamiento?: string | null;
  fechaInicio?: Date | null;
  fechaFin?: Date | null;
}

export class Tratamiento implements ITratamiento {
  constructor(
    public id?: number,
    public descripcionTratamiento?: string | null,
    public fechaInicio?: Date | null,
    public fechaFin?: Date | null
  ) {}
}
