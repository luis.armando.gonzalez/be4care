import { ITratamiento } from '@/shared/model/pacientes/tratamiento.model';
import { IMedicamento } from '@/shared/model/pacientes/medicamento.model';

export interface ITratamientoMedicamento {
  id?: number;
  dosis?: string | null;
  intensidad?: string | null;
  tratamiento?: ITratamiento | null;
  medicamento?: IMedicamento | null;
}

export class TratamientoMedicamento implements ITratamientoMedicamento {
  constructor(
    public id?: number,
    public dosis?: string | null,
    public intensidad?: string | null,
    public tratamiento?: ITratamiento | null,
    public medicamento?: IMedicamento | null
  ) {}
}
