export interface IPais {
  id?: number;
  pais?: string | null;
}

export class Pais implements IPais {
  constructor(public id?: number, public pais?: string | null) {}
}
