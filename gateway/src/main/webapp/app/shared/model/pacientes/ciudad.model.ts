import { IPais } from '@/shared/model/pacientes/pais.model';

export interface ICiudad {
  id?: number;
  ciudad?: string | null;
  pais?: IPais | null;
}

export class Ciudad implements ICiudad {
  constructor(public id?: number, public ciudad?: string | null, public pais?: IPais | null) {}
}
