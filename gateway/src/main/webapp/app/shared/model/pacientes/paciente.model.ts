import { IUser } from '@/shared/model/user.model';
import { ICiudad } from '@/shared/model/pacientes/ciudad.model';
import { ICondicion } from '@/shared/model/pacientes/condicion.model';
import { IIPS } from '@/shared/model/pacientes/ips.model';
import { ITratamiento } from '@/shared/model/pacientes/tratamiento.model';
import { IFarmaceutica } from '@/shared/model/pacientes/farmaceutica.model';

import { Identificaciont } from '@/shared/model/enumerations/identificaciont.model';
import { Sexop } from '@/shared/model/enumerations/sexop.model';
import { Grupoetnicop } from '@/shared/model/enumerations/grupoetnicop.model';
import { Estratop } from '@/shared/model/enumerations/estratop.model';
export interface IPaciente {
  id?: number;
  nombre?: string | null;
  tipoIdentificacion?: Identificaciont | null;
  identificacion?: number | null;
  edad?: number | null;
  sexo?: Sexop | null;
  telefono?: number | null;
  direccion?: string | null;
  pesoKG?: number | null;
  estaturaCM?: number | null;
  grupoEtnico?: Grupoetnicop | null;
  estratoSocioeconomico?: Estratop | null;
  oximetriaReferencia?: number | null;
  ritmoCardiacoReferencia?: number | null;
  presionSistolicaReferencia?: number | null;
  presionDistolicaReferencia?: number | null;
  comentarios?: string | null;
  pasosReferencia?: number | null;
  caloriasReferencia?: number | null;
  metaReferencia?: string | null;
  user?: IUser | null;
  ciudad?: ICiudad | null;
  condicion?: ICondicion | null;
  ips?: IIPS | null;
  tratamiento?: ITratamiento | null;
  farmaceutica?: IFarmaceutica | null;
}

export class Paciente implements IPaciente {
  constructor(
    public id?: number,
    public nombre?: string | null,
    public tipoIdentificacion?: Identificaciont | null,
    public identificacion?: number | null,
    public edad?: number | null,
    public sexo?: Sexop | null,
    public telefono?: number | null,
    public direccion?: string | null,
    public pesoKG?: number | null,
    public estaturaCM?: number | null,
    public grupoEtnico?: Grupoetnicop | null,
    public estratoSocioeconomico?: Estratop | null,
    public oximetriaReferencia?: number | null,
    public ritmoCardiacoReferencia?: number | null,
    public presionSistolicaReferencia?: number | null,
    public presionDistolicaReferencia?: number | null,
    public comentarios?: string | null,
    public pasosReferencia?: number | null,
    public caloriasReferencia?: number | null,
    public metaReferencia?: string | null,
    public user?: IUser | null,
    public ciudad?: ICiudad | null,
    public condicion?: ICondicion | null,
    public ips?: IIPS | null,
    public tratamiento?: ITratamiento | null,
    public farmaceutica?: IFarmaceutica | null
  ) {}
}
