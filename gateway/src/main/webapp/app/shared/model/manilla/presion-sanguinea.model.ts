import { IUser } from '@/shared/model/user.model';

export interface IPresionSanguinea {
  id?: number;
  presionSanguineaSistolica?: number | null;
  presionSanguineaDiastolica?: number | null;
  fechaRegistro?: Date | null;
  user?: IUser | null;
}

export class PresionSanguinea implements IPresionSanguinea {
  constructor(
    public id?: number,
    public presionSanguineaSistolica?: number | null,
    public presionSanguineaDiastolica?: number | null,
    public fechaRegistro?: Date | null,
    public user?: IUser | null
  ) {}
}
