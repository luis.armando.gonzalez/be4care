import { IUser } from '@/shared/model/user.model';

export interface ITokenDisp {
  id?: number;
  tokenConexion?: string | null;
  activo?: boolean | null;
  fechaInicio?: Date | null;
  fechaFin?: Date | null;
  user?: IUser | null;
}

export class TokenDisp implements ITokenDisp {
  constructor(
    public id?: number,
    public tokenConexion?: string | null,
    public activo?: boolean | null,
    public fechaInicio?: Date | null,
    public fechaFin?: Date | null,
    public user?: IUser | null
  ) {
    this.activo = this.activo ?? false;
  }
}
