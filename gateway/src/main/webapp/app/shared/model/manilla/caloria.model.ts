import { IUser } from '@/shared/model/user.model';

export interface ICaloria {
  id?: number;
  caloriasActivas?: number | null;
  descripcion?: string | null;
  fechaRegistro?: Date | null;
  user?: IUser | null;
}

export class Caloria implements ICaloria {
  constructor(
    public id?: number,
    public caloriasActivas?: number | null,
    public descripcion?: string | null,
    public fechaRegistro?: Date | null,
    public user?: IUser | null
  ) {}
}
