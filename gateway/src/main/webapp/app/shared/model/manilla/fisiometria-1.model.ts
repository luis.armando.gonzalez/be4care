import { IUser } from '@/shared/model/user.model';

export interface IFisiometria1 {
  id?: number;
  ritmoCardiaco?: number | null;
  ritmoRespiratorio?: number | null;
  oximetria?: number | null;
  presionArterialSistolica?: number | null;
  presionArterialDiastolica?: number | null;
  temperatura?: number | null;
  fechaRegistro?: Date | null;
  fechaToma?: Date | null;
  user?: IUser | null;
}

export class Fisiometria1 implements IFisiometria1 {
  constructor(
    public id?: number,
    public ritmoCardiaco?: number | null,
    public ritmoRespiratorio?: number | null,
    public oximetria?: number | null,
    public presionArterialSistolica?: number | null,
    public presionArterialDiastolica?: number | null,
    public temperatura?: number | null,
    public fechaRegistro?: Date | null,
    public fechaToma?: Date | null,
    public user?: IUser | null
  ) {}
}
