import { IUser } from '@/shared/model/user.model';

export interface ISueno {
  id?: number;
  superficial?: number | null;
  profundo?: number | null;
  despierto?: number | null;
  timeInstant?: Date | null;
  user?: IUser | null;
}

export class Sueno implements ISueno {
  constructor(
    public id?: number,
    public superficial?: number | null,
    public profundo?: number | null,
    public despierto?: number | null,
    public timeInstant?: Date | null,
    public user?: IUser | null
  ) {}
}
