import { IUser } from '@/shared/model/user.model';

export interface IOximetria {
  id?: number;
  oximetria?: number | null;
  fechaRegistro?: Date | null;
  user?: IUser | null;
}

export class Oximetria implements IOximetria {
  constructor(public id?: number, public oximetria?: number | null, public fechaRegistro?: Date | null, public user?: IUser | null) {}
}
