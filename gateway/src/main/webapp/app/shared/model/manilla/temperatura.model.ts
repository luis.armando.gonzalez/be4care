import { IUser } from '@/shared/model/user.model';

export interface ITemperatura {
  id?: number;
  temperatura?: number | null;
  fechaRegistro?: Date | null;
  user?: IUser | null;
}

export class Temperatura implements ITemperatura {
  constructor(public id?: number, public temperatura?: number | null, public fechaRegistro?: Date | null, public user?: IUser | null) {}
}
