import { IUser } from '@/shared/model/user.model';

export interface IFrecuenciaCardiaca {
  id?: number;
  frecuenciaCardiaca?: number | null;
  fechaRegistro?: Date | null;
  user?: IUser | null;
}

export class FrecuenciaCardiaca implements IFrecuenciaCardiaca {
  constructor(
    public id?: number,
    public frecuenciaCardiaca?: number | null,
    public fechaRegistro?: Date | null,
    public user?: IUser | null
  ) {}
}
