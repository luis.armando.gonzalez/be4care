import { IUser } from '@/shared/model/user.model';

export interface IPrograma {
  id?: number;
  caloriasActividad?: number | null;
  pasosActividad?: number | null;
  fechaRegistro?: Date | null;
  user?: IUser | null;
}

export class Programa implements IPrograma {
  constructor(
    public id?: number,
    public caloriasActividad?: number | null,
    public pasosActividad?: number | null,
    public fechaRegistro?: Date | null,
    public user?: IUser | null
  ) {}
}
