import { ITokenDisp } from '@/shared/model/manilla/token-disp.model';

export interface INotificacion {
  id?: number;
  fechaInicio?: Date | null;
  fechaActualizacion?: Date | null;
  estado?: number | null;
  tipoNotificacion?: number | null;
  token?: ITokenDisp | null;
}

export class Notificacion implements INotificacion {
  constructor(
    public id?: number,
    public fechaInicio?: Date | null,
    public fechaActualizacion?: Date | null,
    public estado?: number | null,
    public tipoNotificacion?: number | null,
    public token?: ITokenDisp | null
  ) {}
}
