import { IUser } from '@/shared/model/user.model';

export interface IIngesta {
  id?: number;
  tipo?: string | null;
  consumoCalorias?: number | null;
  fechaRegistro?: Date | null;
  user?: IUser | null;
}

export class Ingesta implements IIngesta {
  constructor(
    public id?: number,
    public tipo?: string | null,
    public consumoCalorias?: number | null,
    public fechaRegistro?: Date | null,
    public user?: IUser | null
  ) {}
}
