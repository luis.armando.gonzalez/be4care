import { IUser } from '@/shared/model/user.model';

export interface IPasos {
  id?: number;
  nroPasos?: number | null;
  timeInstant?: Date | null;
  user?: IUser | null;
}

export class Pasos implements IPasos {
  constructor(public id?: number, public nroPasos?: number | null, public timeInstant?: Date | null, public user?: IUser | null) {}
}
