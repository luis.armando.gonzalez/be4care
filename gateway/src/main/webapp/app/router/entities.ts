import { Authority } from '@/shared/security/authority';
/* tslint:disable */
// prettier-ignore

// prettier-ignore
const Adherencia = () => import('@/entities/pacientes/adherencia/adherencia.vue');
// prettier-ignore
const AdherenciaUpdate = () => import('@/entities/pacientes/adherencia/adherencia-update.vue');
// prettier-ignore
const AdherenciaDetails = () => import('@/entities/pacientes/adherencia/adherencia-details.vue');
// prettier-ignore
const Agenda = () => import('@/entities/pacientes/agenda/agenda.vue');
// prettier-ignore
const AgendaUpdate = () => import('@/entities/pacientes/agenda/agenda-update.vue');
// prettier-ignore
const AgendaDetails = () => import('@/entities/pacientes/agenda/agenda-details.vue');
// prettier-ignore
const Alarma = () => import('@/entities/pacientes/alarma/alarma.vue');
// prettier-ignore
const AlarmaUpdate = () => import('@/entities/pacientes/alarma/alarma-update.vue');
// prettier-ignore
const AlarmaDetails = () => import('@/entities/pacientes/alarma/alarma-details.vue');
// prettier-ignore
const Biologico = () => import('@/entities/biologico/biologico/biologico.vue');
// prettier-ignore
const BiologicoUpdate = () => import('@/entities/biologico/biologico/biologico-update.vue');
// prettier-ignore
const BiologicoDetails = () => import('@/entities/biologico/biologico/biologico-details.vue');
// prettier-ignore
const Caloria = () => import('@/entities/manilla/caloria/caloria.vue');
// prettier-ignore
const CaloriaUpdate = () => import('@/entities/manilla/caloria/caloria-update.vue');
// prettier-ignore
const CaloriaDetails = () => import('@/entities/manilla/caloria/caloria-details.vue');
// prettier-ignore
const CicloActividad = () => import('@/entities/biologico/ciclo-actividad/ciclo-actividad.vue');
// prettier-ignore
const CicloActividadUpdate = () => import('@/entities/biologico/ciclo-actividad/ciclo-actividad-update.vue');
// prettier-ignore
const CicloActividadDetails = () => import('@/entities/biologico/ciclo-actividad/ciclo-actividad-details.vue');
// prettier-ignore
const Ciudad = () => import('@/entities/pacientes/ciudad/ciudad.vue');
// prettier-ignore
const CiudadUpdate = () => import('@/entities/pacientes/ciudad/ciudad-update.vue');
// prettier-ignore
const CiudadDetails = () => import('@/entities/pacientes/ciudad/ciudad-details.vue');
// prettier-ignore
const Condicion = () => import('@/entities/pacientes/condicion/condicion.vue');
// prettier-ignore
const CondicionUpdate = () => import('@/entities/pacientes/condicion/condicion-update.vue');
// prettier-ignore
const CondicionDetails = () => import('@/entities/pacientes/condicion/condicion-details.vue');
// prettier-ignore
const CuestionarioEstado = () => import('@/entities/pacientes/cuestionario-estado/cuestionario-estado.vue');
// prettier-ignore
const CuestionarioEstadoUpdate = () => import('@/entities/pacientes/cuestionario-estado/cuestionario-estado-update.vue');
// prettier-ignore
const CuestionarioEstadoDetails = () => import('@/entities/pacientes/cuestionario-estado/cuestionario-estado-details.vue');
// prettier-ignore
const Dispositivo = () => import('@/entities/manilla/dispositivo/dispositivo.vue');
// prettier-ignore
const DispositivoUpdate = () => import('@/entities/manilla/dispositivo/dispositivo-update.vue');
// prettier-ignore
const DispositivoDetails = () => import('@/entities/manilla/dispositivo/dispositivo-details.vue');
// prettier-ignore
const Encuesta = () => import('@/entities/manilla/encuesta/encuesta.vue');
// prettier-ignore
const EncuestaUpdate = () => import('@/entities/manilla/encuesta/encuesta-update.vue');
// prettier-ignore
const EncuestaDetails = () => import('@/entities/manilla/encuesta/encuesta-details.vue');
// prettier-ignore
const Espirometria = () => import('@/entities/expedientemanual/espirometria/espirometria.vue');
// prettier-ignore
const EspirometriaUpdate = () => import('@/entities/expedientemanual/espirometria/espirometria-update.vue');
// prettier-ignore
const EspirometriaDetails = () => import('@/entities/expedientemanual/espirometria/espirometria-details.vue');
// prettier-ignore
const Fabricante = () => import('@/entities/biologico/fabricante/fabricante.vue');
// prettier-ignore
const FabricanteUpdate = () => import('@/entities/biologico/fabricante/fabricante-update.vue');
// prettier-ignore
const FabricanteDetails = () => import('@/entities/biologico/fabricante/fabricante-details.vue');
// prettier-ignore
const Farmaceutica = () => import('@/entities/pacientes/farmaceutica/farmaceutica.vue');
// prettier-ignore
const FarmaceuticaUpdate = () => import('@/entities/pacientes/farmaceutica/farmaceutica-update.vue');
// prettier-ignore
const FarmaceuticaDetails = () => import('@/entities/pacientes/farmaceutica/farmaceutica-details.vue');
// prettier-ignore
const Fisiometria1 = () => import('@/entities/manilla/fisiometria-1/fisiometria-1.vue');
// prettier-ignore
const Fisiometria1Update = () => import('@/entities/manilla/fisiometria-1/fisiometria-1-update.vue');
// prettier-ignore
const Fisiometria1Details = () => import('@/entities/manilla/fisiometria-1/fisiometria-1-details.vue');
// prettier-ignore
const Fisiometria2 = () => import('@/entities/diadema/fisiometria-2/fisiometria-2.vue');
// prettier-ignore
const Fisiometria2Update = () => import('@/entities/diadema/fisiometria-2/fisiometria-2-update.vue');
// prettier-ignore
const Fisiometria2Details = () => import('@/entities/diadema/fisiometria-2/fisiometria-2-details.vue');
// prettier-ignore
const FrecuenciaCardiaca = () => import('@/entities/manilla/frecuencia-cardiaca/frecuencia-cardiaca.vue');
// prettier-ignore
const FrecuenciaCardiacaUpdate = () => import('@/entities/manilla/frecuencia-cardiaca/frecuencia-cardiaca-update.vue');
// prettier-ignore
const FrecuenciaCardiacaDetails = () => import('@/entities/manilla/frecuencia-cardiaca/frecuencia-cardiaca-details.vue');
// prettier-ignore
const Glucosa = () => import('@/entities/expedientemanual/glucosa/glucosa.vue');
// prettier-ignore
const GlucosaUpdate = () => import('@/entities/expedientemanual/glucosa/glucosa-update.vue');
// prettier-ignore
const GlucosaDetails = () => import('@/entities/expedientemanual/glucosa/glucosa-details.vue');
// prettier-ignore
const HistorialActividad = () => import('@/entities/biologico/historial-actividad/historial-actividad.vue');
// prettier-ignore
const HistorialActividadUpdate = () => import('@/entities/biologico/historial-actividad/historial-actividad-update.vue');
// prettier-ignore
const HistorialActividadDetails = () => import('@/entities/biologico/historial-actividad/historial-actividad-details.vue');
// prettier-ignore
const Ingesta = () => import('@/entities/manilla/ingesta/ingesta.vue');
// prettier-ignore
const IngestaUpdate = () => import('@/entities/manilla/ingesta/ingesta-update.vue');
// prettier-ignore
const IngestaDetails = () => import('@/entities/manilla/ingesta/ingesta-details.vue');
// prettier-ignore
const IPS = () => import('@/entities/pacientes/ips/ips.vue');
// prettier-ignore
const IPSUpdate = () => import('@/entities/pacientes/ips/ips-update.vue');
// prettier-ignore
const IPSDetails = () => import('@/entities/pacientes/ips/ips-details.vue');
// prettier-ignore
const Medicamento = () => import('@/entities/pacientes/medicamento/medicamento.vue');
// prettier-ignore
const MedicamentoUpdate = () => import('@/entities/pacientes/medicamento/medicamento-update.vue');
// prettier-ignore
const MedicamentoDetails = () => import('@/entities/pacientes/medicamento/medicamento-details.vue');
// prettier-ignore
const Notificacion = () => import('@/entities/manilla/notificacion/notificacion.vue');
// prettier-ignore
const NotificacionUpdate = () => import('@/entities/manilla/notificacion/notificacion-update.vue');
// prettier-ignore
const NotificacionDetails = () => import('@/entities/manilla/notificacion/notificacion-details.vue');
// prettier-ignore
const Oximetria = () => import('@/entities/manilla/oximetria/oximetria.vue');
// prettier-ignore
const OximetriaUpdate = () => import('@/entities/manilla/oximetria/oximetria-update.vue');
// prettier-ignore
const OximetriaDetails = () => import('@/entities/manilla/oximetria/oximetria-details.vue');
// prettier-ignore
const Paciente = () => import('@/entities/pacientes/paciente/paciente.vue');
// prettier-ignore
const PacienteUpdate = () => import('@/entities/pacientes/paciente/paciente-update.vue');
// prettier-ignore
const PacienteDetails = () => import('@/entities/pacientes/paciente/paciente-details.vue');
// prettier-ignore
const Pais = () => import('@/entities/pacientes/pais/pais.vue');
// prettier-ignore
const PaisUpdate = () => import('@/entities/pacientes/pais/pais-update.vue');
// prettier-ignore
const PaisDetails = () => import('@/entities/pacientes/pais/pais-details.vue');
// prettier-ignore
const Pasos = () => import('@/entities/manilla/pasos/pasos.vue');
// prettier-ignore
const PasosUpdate = () => import('@/entities/manilla/pasos/pasos-update.vue');
// prettier-ignore
const PasosDetails = () => import('@/entities/manilla/pasos/pasos-details.vue');
// prettier-ignore
const Peso = () => import('@/entities/expedientemanual/peso/peso.vue');
// prettier-ignore
const PesoUpdate = () => import('@/entities/expedientemanual/peso/peso-update.vue');
// prettier-ignore
const PesoDetails = () => import('@/entities/expedientemanual/peso/peso-details.vue');
// prettier-ignore
const Pregunta = () => import('@/entities/pacientes/pregunta/pregunta.vue');
// prettier-ignore
const PreguntaUpdate = () => import('@/entities/pacientes/pregunta/pregunta-update.vue');
// prettier-ignore
const PreguntaDetails = () => import('@/entities/pacientes/pregunta/pregunta-details.vue');
// prettier-ignore
const PresionSanguinea = () => import('@/entities/manilla/presion-sanguinea/presion-sanguinea.vue');
// prettier-ignore
const PresionSanguineaUpdate = () => import('@/entities/manilla/presion-sanguinea/presion-sanguinea-update.vue');
// prettier-ignore
const PresionSanguineaDetails = () => import('@/entities/manilla/presion-sanguinea/presion-sanguinea-details.vue');
// prettier-ignore
const Programa = () => import('@/entities/manilla/programa/programa.vue');
// prettier-ignore
const ProgramaUpdate = () => import('@/entities/manilla/programa/programa-update.vue');
// prettier-ignore
const ProgramaDetails = () => import('@/entities/manilla/programa/programa-details.vue');
// prettier-ignore
const Sueno = () => import('@/entities/manilla/sueno/sueno.vue');
// prettier-ignore
const SuenoUpdate = () => import('@/entities/manilla/sueno/sueno-update.vue');
// prettier-ignore
const SuenoDetails = () => import('@/entities/manilla/sueno/sueno-details.vue');
// prettier-ignore
const Temperatura = () => import('@/entities/manilla/temperatura/temperatura.vue');
// prettier-ignore
const TemperaturaUpdate = () => import('@/entities/manilla/temperatura/temperatura-update.vue');
// prettier-ignore
const TemperaturaDetails = () => import('@/entities/manilla/temperatura/temperatura-details.vue');
// prettier-ignore
const TokenDisp = () => import('@/entities/manilla/token-disp/token-disp.vue');
// prettier-ignore
const TokenDispUpdate = () => import('@/entities/manilla/token-disp/token-disp-update.vue');
// prettier-ignore
const TokenDispDetails = () => import('@/entities/manilla/token-disp/token-disp-details.vue');
// prettier-ignore
const Tratamiento = () => import('@/entities/pacientes/tratamiento/tratamiento.vue');
// prettier-ignore
const TratamientoUpdate = () => import('@/entities/pacientes/tratamiento/tratamiento-update.vue');
// prettier-ignore
const TratamientoDetails = () => import('@/entities/pacientes/tratamiento/tratamiento-details.vue');
// prettier-ignore
const TratamientoMedicamento = () => import('@/entities/pacientes/tratamiento-medicamento/tratamiento-medicamento.vue');
// prettier-ignore
const TratamientoMedicamentoUpdate = () => import('@/entities/pacientes/tratamiento-medicamento/tratamiento-medicamento-update.vue');
// prettier-ignore
const TratamientoMedicamentoDetails = () => import('@/entities/pacientes/tratamiento-medicamento/tratamiento-medicamento-details.vue');
// prettier-ignore
const Vacuna = () => import('@/entities/biologico/vacuna/vacuna.vue');
// prettier-ignore
const VacunaUpdate = () => import('@/entities/biologico/vacuna/vacuna-update.vue');
// prettier-ignore
const VacunaDetails = () => import('@/entities/biologico/vacuna/vacuna-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

export default [
  {
    path: '/adherencia',
    name: 'Adherencia',
    component: Adherencia,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/adherencia/new',
    name: 'AdherenciaCreate',
    component: AdherenciaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/adherencia/:adherenciaId/edit',
    name: 'AdherenciaEdit',
    component: AdherenciaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/adherencia/:adherenciaId/view',
    name: 'AdherenciaView',
    component: AdherenciaDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/agenda',
    name: 'Agenda',
    component: Agenda,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/agenda/new',
    name: 'AgendaCreate',
    component: AgendaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/agenda/:agendaId/edit',
    name: 'AgendaEdit',
    component: AgendaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/agenda/:agendaId/view',
    name: 'AgendaView',
    component: AgendaDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/alarma',
    name: 'Alarma',
    component: Alarma,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/alarma/new',
    name: 'AlarmaCreate',
    component: AlarmaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/alarma/:alarmaId/edit',
    name: 'AlarmaEdit',
    component: AlarmaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/alarma/:alarmaId/view',
    name: 'AlarmaView',
    component: AlarmaDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/biologico',
    name: 'Biologico',
    component: Biologico,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/biologico/new',
    name: 'BiologicoCreate',
    component: BiologicoUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/biologico/:biologicoId/edit',
    name: 'BiologicoEdit',
    component: BiologicoUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/biologico/:biologicoId/view',
    name: 'BiologicoView',
    component: BiologicoDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/caloria',
    name: 'Caloria',
    component: Caloria,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/caloria/new',
    name: 'CaloriaCreate',
    component: CaloriaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/caloria/:caloriaId/edit',
    name: 'CaloriaEdit',
    component: CaloriaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/caloria/:caloriaId/view',
    name: 'CaloriaView',
    component: CaloriaDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/ciclo-actividad',
    name: 'CicloActividad',
    component: CicloActividad,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/ciclo-actividad/new',
    name: 'CicloActividadCreate',
    component: CicloActividadUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/ciclo-actividad/:cicloActividadId/edit',
    name: 'CicloActividadEdit',
    component: CicloActividadUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/ciclo-actividad/:cicloActividadId/view',
    name: 'CicloActividadView',
    component: CicloActividadDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/ciudad',
    name: 'Ciudad',
    component: Ciudad,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/ciudad/new',
    name: 'CiudadCreate',
    component: CiudadUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/ciudad/:ciudadId/edit',
    name: 'CiudadEdit',
    component: CiudadUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/ciudad/:ciudadId/view',
    name: 'CiudadView',
    component: CiudadDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/condicion',
    name: 'Condicion',
    component: Condicion,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/condicion/new',
    name: 'CondicionCreate',
    component: CondicionUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/condicion/:condicionId/edit',
    name: 'CondicionEdit',
    component: CondicionUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/condicion/:condicionId/view',
    name: 'CondicionView',
    component: CondicionDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/cuestionario-estado',
    name: 'CuestionarioEstado',
    component: CuestionarioEstado,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/cuestionario-estado/new',
    name: 'CuestionarioEstadoCreate',
    component: CuestionarioEstadoUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/cuestionario-estado/:cuestionarioEstadoId/edit',
    name: 'CuestionarioEstadoEdit',
    component: CuestionarioEstadoUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/cuestionario-estado/:cuestionarioEstadoId/view',
    name: 'CuestionarioEstadoView',
    component: CuestionarioEstadoDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/dispositivo',
    name: 'Dispositivo',
    component: Dispositivo,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/dispositivo/new',
    name: 'DispositivoCreate',
    component: DispositivoUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/dispositivo/:dispositivoId/edit',
    name: 'DispositivoEdit',
    component: DispositivoUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/dispositivo/:dispositivoId/view',
    name: 'DispositivoView',
    component: DispositivoDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/encuesta',
    name: 'Encuesta',
    component: Encuesta,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/encuesta/new',
    name: 'EncuestaCreate',
    component: EncuestaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/encuesta/:encuestaId/edit',
    name: 'EncuestaEdit',
    component: EncuestaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/encuesta/:encuestaId/view',
    name: 'EncuestaView',
    component: EncuestaDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/espirometria',
    name: 'Espirometria',
    component: Espirometria,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/espirometria/new',
    name: 'EspirometriaCreate',
    component: EspirometriaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/espirometria/:espirometriaId/edit',
    name: 'EspirometriaEdit',
    component: EspirometriaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/espirometria/:espirometriaId/view',
    name: 'EspirometriaView',
    component: EspirometriaDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/fabricante',
    name: 'Fabricante',
    component: Fabricante,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/fabricante/new',
    name: 'FabricanteCreate',
    component: FabricanteUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/fabricante/:fabricanteId/edit',
    name: 'FabricanteEdit',
    component: FabricanteUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/fabricante/:fabricanteId/view',
    name: 'FabricanteView',
    component: FabricanteDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/farmaceutica',
    name: 'Farmaceutica',
    component: Farmaceutica,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/farmaceutica/new',
    name: 'FarmaceuticaCreate',
    component: FarmaceuticaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/farmaceutica/:farmaceuticaId/edit',
    name: 'FarmaceuticaEdit',
    component: FarmaceuticaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/farmaceutica/:farmaceuticaId/view',
    name: 'FarmaceuticaView',
    component: FarmaceuticaDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/fisiometria-1',
    name: 'Fisiometria1',
    component: Fisiometria1,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/fisiometria-1/new',
    name: 'Fisiometria1Create',
    component: Fisiometria1Update,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/fisiometria-1/:fisiometria1Id/edit',
    name: 'Fisiometria1Edit',
    component: Fisiometria1Update,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/fisiometria-1/:fisiometria1Id/view',
    name: 'Fisiometria1View',
    component: Fisiometria1Details,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/fisiometria-2',
    name: 'Fisiometria2',
    component: Fisiometria2,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/fisiometria-2/new',
    name: 'Fisiometria2Create',
    component: Fisiometria2Update,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/fisiometria-2/:fisiometria2Id/edit',
    name: 'Fisiometria2Edit',
    component: Fisiometria2Update,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/fisiometria-2/:fisiometria2Id/view',
    name: 'Fisiometria2View',
    component: Fisiometria2Details,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/frecuencia-cardiaca',
    name: 'FrecuenciaCardiaca',
    component: FrecuenciaCardiaca,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/frecuencia-cardiaca/new',
    name: 'FrecuenciaCardiacaCreate',
    component: FrecuenciaCardiacaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/frecuencia-cardiaca/:frecuenciaCardiacaId/edit',
    name: 'FrecuenciaCardiacaEdit',
    component: FrecuenciaCardiacaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/frecuencia-cardiaca/:frecuenciaCardiacaId/view',
    name: 'FrecuenciaCardiacaView',
    component: FrecuenciaCardiacaDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/glucosa',
    name: 'Glucosa',
    component: Glucosa,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/glucosa/new',
    name: 'GlucosaCreate',
    component: GlucosaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/glucosa/:glucosaId/edit',
    name: 'GlucosaEdit',
    component: GlucosaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/glucosa/:glucosaId/view',
    name: 'GlucosaView',
    component: GlucosaDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/historial-actividad',
    name: 'HistorialActividad',
    component: HistorialActividad,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/historial-actividad/new',
    name: 'HistorialActividadCreate',
    component: HistorialActividadUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/historial-actividad/:historialActividadId/edit',
    name: 'HistorialActividadEdit',
    component: HistorialActividadUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/historial-actividad/:historialActividadId/view',
    name: 'HistorialActividadView',
    component: HistorialActividadDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/ingesta',
    name: 'Ingesta',
    component: Ingesta,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/ingesta/new',
    name: 'IngestaCreate',
    component: IngestaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/ingesta/:ingestaId/edit',
    name: 'IngestaEdit',
    component: IngestaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/ingesta/:ingestaId/view',
    name: 'IngestaView',
    component: IngestaDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/ips',
    name: 'IPS',
    component: IPS,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/ips/new',
    name: 'IPSCreate',
    component: IPSUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/ips/:iPSId/edit',
    name: 'IPSEdit',
    component: IPSUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/ips/:iPSId/view',
    name: 'IPSView',
    component: IPSDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/medicamento',
    name: 'Medicamento',
    component: Medicamento,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/medicamento/new',
    name: 'MedicamentoCreate',
    component: MedicamentoUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/medicamento/:medicamentoId/edit',
    name: 'MedicamentoEdit',
    component: MedicamentoUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/medicamento/:medicamentoId/view',
    name: 'MedicamentoView',
    component: MedicamentoDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/notificacion',
    name: 'Notificacion',
    component: Notificacion,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/notificacion/new',
    name: 'NotificacionCreate',
    component: NotificacionUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/notificacion/:notificacionId/edit',
    name: 'NotificacionEdit',
    component: NotificacionUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/notificacion/:notificacionId/view',
    name: 'NotificacionView',
    component: NotificacionDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/oximetria',
    name: 'Oximetria',
    component: Oximetria,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/oximetria/new',
    name: 'OximetriaCreate',
    component: OximetriaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/oximetria/:oximetriaId/edit',
    name: 'OximetriaEdit',
    component: OximetriaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/oximetria/:oximetriaId/view',
    name: 'OximetriaView',
    component: OximetriaDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/paciente',
    name: 'Paciente',
    component: Paciente,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/paciente/new',
    name: 'PacienteCreate',
    component: PacienteUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/paciente/:pacienteId/edit',
    name: 'PacienteEdit',
    component: PacienteUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/paciente/:pacienteId/view',
    name: 'PacienteView',
    component: PacienteDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/pais',
    name: 'Pais',
    component: Pais,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/pais/new',
    name: 'PaisCreate',
    component: PaisUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/pais/:paisId/edit',
    name: 'PaisEdit',
    component: PaisUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/pais/:paisId/view',
    name: 'PaisView',
    component: PaisDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/pasos',
    name: 'Pasos',
    component: Pasos,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/pasos/new',
    name: 'PasosCreate',
    component: PasosUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/pasos/:pasosId/edit',
    name: 'PasosEdit',
    component: PasosUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/pasos/:pasosId/view',
    name: 'PasosView',
    component: PasosDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/peso',
    name: 'Peso',
    component: Peso,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/peso/new',
    name: 'PesoCreate',
    component: PesoUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/peso/:pesoId/edit',
    name: 'PesoEdit',
    component: PesoUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/peso/:pesoId/view',
    name: 'PesoView',
    component: PesoDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/pregunta',
    name: 'Pregunta',
    component: Pregunta,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/pregunta/new',
    name: 'PreguntaCreate',
    component: PreguntaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/pregunta/:preguntaId/edit',
    name: 'PreguntaEdit',
    component: PreguntaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/pregunta/:preguntaId/view',
    name: 'PreguntaView',
    component: PreguntaDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/presion-sanguinea',
    name: 'PresionSanguinea',
    component: PresionSanguinea,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/presion-sanguinea/new',
    name: 'PresionSanguineaCreate',
    component: PresionSanguineaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/presion-sanguinea/:presionSanguineaId/edit',
    name: 'PresionSanguineaEdit',
    component: PresionSanguineaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/presion-sanguinea/:presionSanguineaId/view',
    name: 'PresionSanguineaView',
    component: PresionSanguineaDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/programa',
    name: 'Programa',
    component: Programa,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/programa/new',
    name: 'ProgramaCreate',
    component: ProgramaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/programa/:programaId/edit',
    name: 'ProgramaEdit',
    component: ProgramaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/programa/:programaId/view',
    name: 'ProgramaView',
    component: ProgramaDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/sueno',
    name: 'Sueno',
    component: Sueno,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/sueno/new',
    name: 'SuenoCreate',
    component: SuenoUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/sueno/:suenoId/edit',
    name: 'SuenoEdit',
    component: SuenoUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/sueno/:suenoId/view',
    name: 'SuenoView',
    component: SuenoDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/temperatura',
    name: 'Temperatura',
    component: Temperatura,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/temperatura/new',
    name: 'TemperaturaCreate',
    component: TemperaturaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/temperatura/:temperaturaId/edit',
    name: 'TemperaturaEdit',
    component: TemperaturaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/temperatura/:temperaturaId/view',
    name: 'TemperaturaView',
    component: TemperaturaDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/token-disp',
    name: 'TokenDisp',
    component: TokenDisp,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/token-disp/new',
    name: 'TokenDispCreate',
    component: TokenDispUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/token-disp/:tokenDispId/edit',
    name: 'TokenDispEdit',
    component: TokenDispUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/token-disp/:tokenDispId/view',
    name: 'TokenDispView',
    component: TokenDispDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/tratamiento',
    name: 'Tratamiento',
    component: Tratamiento,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/tratamiento/new',
    name: 'TratamientoCreate',
    component: TratamientoUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/tratamiento/:tratamientoId/edit',
    name: 'TratamientoEdit',
    component: TratamientoUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/tratamiento/:tratamientoId/view',
    name: 'TratamientoView',
    component: TratamientoDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/tratamiento-medicamento',
    name: 'TratamientoMedicamento',
    component: TratamientoMedicamento,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/tratamiento-medicamento/new',
    name: 'TratamientoMedicamentoCreate',
    component: TratamientoMedicamentoUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/tratamiento-medicamento/:tratamientoMedicamentoId/edit',
    name: 'TratamientoMedicamentoEdit',
    component: TratamientoMedicamentoUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/tratamiento-medicamento/:tratamientoMedicamentoId/view',
    name: 'TratamientoMedicamentoView',
    component: TratamientoMedicamentoDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/vacuna',
    name: 'Vacuna',
    component: Vacuna,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/vacuna/new',
    name: 'VacunaCreate',
    component: VacunaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/vacuna/:vacunaId/edit',
    name: 'VacunaEdit',
    component: VacunaUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/vacuna/:vacunaId/view',
    name: 'VacunaView',
    component: VacunaDetails,
    meta: { authorities: [Authority.USER] },
  },
  // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
];
