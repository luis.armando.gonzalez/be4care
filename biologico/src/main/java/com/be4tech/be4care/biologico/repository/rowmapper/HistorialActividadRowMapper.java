package com.be4tech.be4care.biologico.repository.rowmapper;

import com.be4tech.be4care.biologico.domain.HistorialActividad;
import com.be4tech.be4care.biologico.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link HistorialActividad}, with proper type conversions.
 */
@Service
public class HistorialActividadRowMapper implements BiFunction<Row, String, HistorialActividad> {

    private final ColumnConverter converter;

    public HistorialActividadRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link HistorialActividad} stored in the database.
     */
    @Override
    public HistorialActividad apply(Row row, String prefix) {
        HistorialActividad entity = new HistorialActividad();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setFechaHistorial(converter.fromRow(row, prefix + "_fecha_historial", Instant.class));
        entity.setCicloActividadId(converter.fromRow(row, prefix + "_ciclo_actividad_id", Long.class));
        return entity;
    }
}
