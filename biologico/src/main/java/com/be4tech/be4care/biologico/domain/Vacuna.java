package com.be4tech.be4care.biologico.domain;

import java.io.Serializable;
import java.time.Instant;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Vacuna.
 */
@Table("vacuna")
public class Vacuna implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("nombre_vacuna")
    private String nombreVacuna;

    @Column("tipo_vacuna")
    private String tipoVacuna;

    @Column("descripcion_vacuna")
    private String descripcionVacuna;

    @Column("fecha_ingreso")
    private Instant fechaIngreso;

    @Column("lote_vacuna")
    private String loteVacuna;

    @Transient
    private Fabricante fabricante;

    @Column("fabricante_id")
    private Long fabricanteId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Vacuna id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreVacuna() {
        return this.nombreVacuna;
    }

    public Vacuna nombreVacuna(String nombreVacuna) {
        this.setNombreVacuna(nombreVacuna);
        return this;
    }

    public void setNombreVacuna(String nombreVacuna) {
        this.nombreVacuna = nombreVacuna;
    }

    public String getTipoVacuna() {
        return this.tipoVacuna;
    }

    public Vacuna tipoVacuna(String tipoVacuna) {
        this.setTipoVacuna(tipoVacuna);
        return this;
    }

    public void setTipoVacuna(String tipoVacuna) {
        this.tipoVacuna = tipoVacuna;
    }

    public String getDescripcionVacuna() {
        return this.descripcionVacuna;
    }

    public Vacuna descripcionVacuna(String descripcionVacuna) {
        this.setDescripcionVacuna(descripcionVacuna);
        return this;
    }

    public void setDescripcionVacuna(String descripcionVacuna) {
        this.descripcionVacuna = descripcionVacuna;
    }

    public Instant getFechaIngreso() {
        return this.fechaIngreso;
    }

    public Vacuna fechaIngreso(Instant fechaIngreso) {
        this.setFechaIngreso(fechaIngreso);
        return this;
    }

    public void setFechaIngreso(Instant fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getLoteVacuna() {
        return this.loteVacuna;
    }

    public Vacuna loteVacuna(String loteVacuna) {
        this.setLoteVacuna(loteVacuna);
        return this;
    }

    public void setLoteVacuna(String loteVacuna) {
        this.loteVacuna = loteVacuna;
    }

    public Fabricante getFabricante() {
        return this.fabricante;
    }

    public void setFabricante(Fabricante fabricante) {
        this.fabricante = fabricante;
        this.fabricanteId = fabricante != null ? fabricante.getId() : null;
    }

    public Vacuna fabricante(Fabricante fabricante) {
        this.setFabricante(fabricante);
        return this;
    }

    public Long getFabricanteId() {
        return this.fabricanteId;
    }

    public void setFabricanteId(Long fabricante) {
        this.fabricanteId = fabricante;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Vacuna)) {
            return false;
        }
        return id != null && id.equals(((Vacuna) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Vacuna{" +
            "id=" + getId() +
            ", nombreVacuna='" + getNombreVacuna() + "'" +
            ", tipoVacuna='" + getTipoVacuna() + "'" +
            ", descripcionVacuna='" + getDescripcionVacuna() + "'" +
            ", fechaIngreso='" + getFechaIngreso() + "'" +
            ", loteVacuna='" + getLoteVacuna() + "'" +
            "}";
    }
}
