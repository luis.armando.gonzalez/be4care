package com.be4tech.be4care.biologico.web.rest;

import com.be4tech.be4care.biologico.domain.Biologico;
import com.be4tech.be4care.biologico.repository.BiologicoRepository;
import com.be4tech.be4care.biologico.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.biologico.domain.Biologico}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class BiologicoResource {

    private final Logger log = LoggerFactory.getLogger(BiologicoResource.class);

    private static final String ENTITY_NAME = "biologicoBiologico";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BiologicoRepository biologicoRepository;

    public BiologicoResource(BiologicoRepository biologicoRepository) {
        this.biologicoRepository = biologicoRepository;
    }

    /**
     * {@code POST  /biologicos} : Create a new biologico.
     *
     * @param biologico the biologico to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new biologico, or with status {@code 400 (Bad Request)} if the biologico has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/biologicos")
    public Mono<ResponseEntity<Biologico>> createBiologico(@RequestBody Biologico biologico) throws URISyntaxException {
        log.debug("REST request to save Biologico : {}", biologico);
        if (biologico.getId() != null) {
            throw new BadRequestAlertException("A new biologico cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return biologicoRepository
            .save(biologico)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/biologicos/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /biologicos/:id} : Updates an existing biologico.
     *
     * @param id the id of the biologico to save.
     * @param biologico the biologico to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated biologico,
     * or with status {@code 400 (Bad Request)} if the biologico is not valid,
     * or with status {@code 500 (Internal Server Error)} if the biologico couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/biologicos/{id}")
    public Mono<ResponseEntity<Biologico>> updateBiologico(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Biologico biologico
    ) throws URISyntaxException {
        log.debug("REST request to update Biologico : {}, {}", id, biologico);
        if (biologico.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, biologico.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return biologicoRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                return biologicoRepository
                    .save(biologico)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /biologicos/:id} : Partial updates given fields of an existing biologico, field will ignore if it is null
     *
     * @param id the id of the biologico to save.
     * @param biologico the biologico to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated biologico,
     * or with status {@code 400 (Bad Request)} if the biologico is not valid,
     * or with status {@code 404 (Not Found)} if the biologico is not found,
     * or with status {@code 500 (Internal Server Error)} if the biologico couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/biologicos/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Biologico>> partialUpdateBiologico(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Biologico biologico
    ) throws URISyntaxException {
        log.debug("REST request to partial update Biologico partially : {}, {}", id, biologico);
        if (biologico.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, biologico.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return biologicoRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                Mono<Biologico> result = biologicoRepository
                    .findById(biologico.getId())
                    .map(existingBiologico -> {
                        if (biologico.getTipoBiologico() != null) {
                            existingBiologico.setTipoBiologico(biologico.getTipoBiologico());
                        }
                        if (biologico.getDetalleBiologico() != null) {
                            existingBiologico.setDetalleBiologico(biologico.getDetalleBiologico());
                        }

                        return existingBiologico;
                    })
                    .flatMap(biologicoRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /biologicos} : get all the biologicos.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of biologicos in body.
     */
    @GetMapping("/biologicos")
    public Mono<ResponseEntity<List<Biologico>>> getAllBiologicos(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Biologicos");
        return biologicoRepository
            .count()
            .zipWith(biologicoRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /biologicos/:id} : get the "id" biologico.
     *
     * @param id the id of the biologico to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the biologico, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/biologicos/{id}")
    public Mono<ResponseEntity<Biologico>> getBiologico(@PathVariable Long id) {
        log.debug("REST request to get Biologico : {}", id);
        Mono<Biologico> biologico = biologicoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(biologico);
    }

    /**
     * {@code DELETE  /biologicos/:id} : delete the "id" biologico.
     *
     * @param id the id of the biologico to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/biologicos/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteBiologico(@PathVariable Long id) {
        log.debug("REST request to delete Biologico : {}", id);
        return biologicoRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
