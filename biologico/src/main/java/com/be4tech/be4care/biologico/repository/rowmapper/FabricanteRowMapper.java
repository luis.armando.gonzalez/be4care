package com.be4tech.be4care.biologico.repository.rowmapper;

import com.be4tech.be4care.biologico.domain.Fabricante;
import com.be4tech.be4care.biologico.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Fabricante}, with proper type conversions.
 */
@Service
public class FabricanteRowMapper implements BiFunction<Row, String, Fabricante> {

    private final ColumnConverter converter;

    public FabricanteRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Fabricante} stored in the database.
     */
    @Override
    public Fabricante apply(Row row, String prefix) {
        Fabricante entity = new Fabricante();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setNombreFabricante(converter.fromRow(row, prefix + "_nombre_fabricante", String.class));
        entity.setProductosFabricanet(converter.fromRow(row, prefix + "_productos_fabricanet", String.class));
        entity.setDetalleFabricante(converter.fromRow(row, prefix + "_detalle_fabricante", String.class));
        return entity;
    }
}
