package com.be4tech.be4care.biologico.repository;

import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

import com.be4tech.be4care.biologico.domain.HistorialActividad;
import com.be4tech.be4care.biologico.repository.rowmapper.CicloActividadRowMapper;
import com.be4tech.be4care.biologico.repository.rowmapper.HistorialActividadRowMapper;
import com.be4tech.be4care.biologico.service.EntityManager;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoinCondition;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive custom repository implementation for the HistorialActividad entity.
 */
@SuppressWarnings("unused")
class HistorialActividadRepositoryInternalImpl implements HistorialActividadRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final CicloActividadRowMapper cicloactividadMapper;
    private final HistorialActividadRowMapper historialactividadMapper;

    private static final Table entityTable = Table.aliased("historial_actividad", EntityManager.ENTITY_ALIAS);
    private static final Table cicloActividadTable = Table.aliased("ciclo_actividad", "cicloActividad");

    public HistorialActividadRepositoryInternalImpl(
        R2dbcEntityTemplate template,
        EntityManager entityManager,
        CicloActividadRowMapper cicloactividadMapper,
        HistorialActividadRowMapper historialactividadMapper
    ) {
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.cicloactividadMapper = cicloactividadMapper;
        this.historialactividadMapper = historialactividadMapper;
    }

    @Override
    public Flux<HistorialActividad> findAllBy(Pageable pageable) {
        return findAllBy(pageable, null);
    }

    @Override
    public Flux<HistorialActividad> findAllBy(Pageable pageable, Criteria criteria) {
        return createQuery(pageable, criteria).all();
    }

    RowsFetchSpec<HistorialActividad> createQuery(Pageable pageable, Criteria criteria) {
        List<Expression> columns = HistorialActividadSqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        columns.addAll(CicloActividadSqlHelper.getColumns(cicloActividadTable, "cicloActividad"));
        SelectFromAndJoinCondition selectFrom = Select
            .builder()
            .select(columns)
            .from(entityTable)
            .leftOuterJoin(cicloActividadTable)
            .on(Column.create("ciclo_actividad_id", entityTable))
            .equals(Column.create("id", cicloActividadTable));

        String select = entityManager.createSelect(selectFrom, HistorialActividad.class, pageable, criteria);
        String alias = entityTable.getReferenceName().getReference();
        String selectWhere = Optional
            .ofNullable(criteria)
            .map(crit ->
                new StringBuilder(select)
                    .append(" ")
                    .append("WHERE")
                    .append(" ")
                    .append(alias)
                    .append(".")
                    .append(crit.toString())
                    .toString()
            )
            .orElse(select); // TODO remove once https://github.com/spring-projects/spring-data-jdbc/issues/907 will be fixed
        return db.sql(selectWhere).map(this::process);
    }

    @Override
    public Flux<HistorialActividad> findAll() {
        return findAllBy(null, null);
    }

    @Override
    public Mono<HistorialActividad> findById(Long id) {
        return createQuery(null, where("id").is(id)).one();
    }

    private HistorialActividad process(Row row, RowMetadata metadata) {
        HistorialActividad entity = historialactividadMapper.apply(row, "e");
        entity.setCicloActividad(cicloactividadMapper.apply(row, "cicloActividad"));
        return entity;
    }

    @Override
    public <S extends HistorialActividad> Mono<S> insert(S entity) {
        return entityManager.insert(entity);
    }

    @Override
    public <S extends HistorialActividad> Mono<S> save(S entity) {
        if (entity.getId() == null) {
            return insert(entity);
        } else {
            return update(entity)
                .map(numberOfUpdates -> {
                    if (numberOfUpdates.intValue() <= 0) {
                        throw new IllegalStateException("Unable to update HistorialActividad with id = " + entity.getId());
                    }
                    return entity;
                });
        }
    }

    @Override
    public Mono<Integer> update(HistorialActividad entity) {
        //fixme is this the proper way?
        return r2dbcEntityTemplate.update(entity).thenReturn(1);
    }
}
