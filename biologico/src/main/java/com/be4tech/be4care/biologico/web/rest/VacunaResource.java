package com.be4tech.be4care.biologico.web.rest;

import com.be4tech.be4care.biologico.domain.Vacuna;
import com.be4tech.be4care.biologico.repository.VacunaRepository;
import com.be4tech.be4care.biologico.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.biologico.domain.Vacuna}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class VacunaResource {

    private final Logger log = LoggerFactory.getLogger(VacunaResource.class);

    private static final String ENTITY_NAME = "biologicoVacuna";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VacunaRepository vacunaRepository;

    public VacunaResource(VacunaRepository vacunaRepository) {
        this.vacunaRepository = vacunaRepository;
    }

    /**
     * {@code POST  /vacunas} : Create a new vacuna.
     *
     * @param vacuna the vacuna to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new vacuna, or with status {@code 400 (Bad Request)} if the vacuna has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/vacunas")
    public Mono<ResponseEntity<Vacuna>> createVacuna(@RequestBody Vacuna vacuna) throws URISyntaxException {
        log.debug("REST request to save Vacuna : {}", vacuna);
        if (vacuna.getId() != null) {
            throw new BadRequestAlertException("A new vacuna cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return vacunaRepository
            .save(vacuna)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/vacunas/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /vacunas/:id} : Updates an existing vacuna.
     *
     * @param id the id of the vacuna to save.
     * @param vacuna the vacuna to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated vacuna,
     * or with status {@code 400 (Bad Request)} if the vacuna is not valid,
     * or with status {@code 500 (Internal Server Error)} if the vacuna couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/vacunas/{id}")
    public Mono<ResponseEntity<Vacuna>> updateVacuna(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Vacuna vacuna
    ) throws URISyntaxException {
        log.debug("REST request to update Vacuna : {}, {}", id, vacuna);
        if (vacuna.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, vacuna.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return vacunaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                return vacunaRepository
                    .save(vacuna)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /vacunas/:id} : Partial updates given fields of an existing vacuna, field will ignore if it is null
     *
     * @param id the id of the vacuna to save.
     * @param vacuna the vacuna to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated vacuna,
     * or with status {@code 400 (Bad Request)} if the vacuna is not valid,
     * or with status {@code 404 (Not Found)} if the vacuna is not found,
     * or with status {@code 500 (Internal Server Error)} if the vacuna couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/vacunas/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Vacuna>> partialUpdateVacuna(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Vacuna vacuna
    ) throws URISyntaxException {
        log.debug("REST request to partial update Vacuna partially : {}, {}", id, vacuna);
        if (vacuna.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, vacuna.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return vacunaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                Mono<Vacuna> result = vacunaRepository
                    .findById(vacuna.getId())
                    .map(existingVacuna -> {
                        if (vacuna.getNombreVacuna() != null) {
                            existingVacuna.setNombreVacuna(vacuna.getNombreVacuna());
                        }
                        if (vacuna.getTipoVacuna() != null) {
                            existingVacuna.setTipoVacuna(vacuna.getTipoVacuna());
                        }
                        if (vacuna.getDescripcionVacuna() != null) {
                            existingVacuna.setDescripcionVacuna(vacuna.getDescripcionVacuna());
                        }
                        if (vacuna.getFechaIngreso() != null) {
                            existingVacuna.setFechaIngreso(vacuna.getFechaIngreso());
                        }
                        if (vacuna.getLoteVacuna() != null) {
                            existingVacuna.setLoteVacuna(vacuna.getLoteVacuna());
                        }

                        return existingVacuna;
                    })
                    .flatMap(vacunaRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /vacunas} : get all the vacunas.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of vacunas in body.
     */
    @GetMapping("/vacunas")
    public Mono<ResponseEntity<List<Vacuna>>> getAllVacunas(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Vacunas");
        return vacunaRepository
            .count()
            .zipWith(vacunaRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /vacunas/:id} : get the "id" vacuna.
     *
     * @param id the id of the vacuna to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the vacuna, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/vacunas/{id}")
    public Mono<ResponseEntity<Vacuna>> getVacuna(@PathVariable Long id) {
        log.debug("REST request to get Vacuna : {}", id);
        Mono<Vacuna> vacuna = vacunaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(vacuna);
    }

    /**
     * {@code DELETE  /vacunas/:id} : delete the "id" vacuna.
     *
     * @param id the id of the vacuna to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/vacunas/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteVacuna(@PathVariable Long id) {
        log.debug("REST request to delete Vacuna : {}", id);
        return vacunaRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
