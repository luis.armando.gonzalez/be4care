package com.be4tech.be4care.biologico.repository;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Table;

public class CicloActividadSqlHelper {

    public static List<Expression> getColumns(Table table, String columnPrefix) {
        List<Expression> columns = new ArrayList<>();
        columns.add(Column.aliased("id", table, columnPrefix + "_id"));
        columns.add(Column.aliased("periodo_ciclo", table, columnPrefix + "_periodo_ciclo"));
        columns.add(Column.aliased("cantidad", table, columnPrefix + "_cantidad"));
        columns.add(Column.aliased("fecha_inicio_ciclo", table, columnPrefix + "_fecha_inicio_ciclo"));

        columns.add(Column.aliased("biologico_id", table, columnPrefix + "_biologico_id"));
        columns.add(Column.aliased("vacuna_id", table, columnPrefix + "_vacuna_id"));
        columns.add(Column.aliased("user_id", table, columnPrefix + "_user_id"));
        return columns;
    }
}
