package com.be4tech.be4care.biologico.repository;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Table;

public class FabricanteSqlHelper {

    public static List<Expression> getColumns(Table table, String columnPrefix) {
        List<Expression> columns = new ArrayList<>();
        columns.add(Column.aliased("id", table, columnPrefix + "_id"));
        columns.add(Column.aliased("nombre_fabricante", table, columnPrefix + "_nombre_fabricante"));
        columns.add(Column.aliased("productos_fabricanet", table, columnPrefix + "_productos_fabricanet"));
        columns.add(Column.aliased("detalle_fabricante", table, columnPrefix + "_detalle_fabricante"));

        return columns;
    }
}
