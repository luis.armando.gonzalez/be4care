package com.be4tech.be4care.biologico.repository;

import com.be4tech.be4care.biologico.domain.Fabricante;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Fabricante entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FabricanteRepository extends R2dbcRepository<Fabricante, Long>, FabricanteRepositoryInternal {
    Flux<Fabricante> findAllBy(Pageable pageable);

    // just to avoid having unambigous methods
    @Override
    Flux<Fabricante> findAll();

    @Override
    Mono<Fabricante> findById(Long id);

    @Override
    <S extends Fabricante> Mono<S> save(S entity);
}

interface FabricanteRepositoryInternal {
    <S extends Fabricante> Mono<S> insert(S entity);
    <S extends Fabricante> Mono<S> save(S entity);
    Mono<Integer> update(Fabricante entity);

    Flux<Fabricante> findAll();
    Mono<Fabricante> findById(Long id);
    Flux<Fabricante> findAllBy(Pageable pageable);
    Flux<Fabricante> findAllBy(Pageable pageable, Criteria criteria);
}
