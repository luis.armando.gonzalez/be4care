package com.be4tech.be4care.biologico.repository;

import com.be4tech.be4care.biologico.domain.Biologico;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Biologico entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BiologicoRepository extends R2dbcRepository<Biologico, Long>, BiologicoRepositoryInternal {
    Flux<Biologico> findAllBy(Pageable pageable);

    // just to avoid having unambigous methods
    @Override
    Flux<Biologico> findAll();

    @Override
    Mono<Biologico> findById(Long id);

    @Override
    <S extends Biologico> Mono<S> save(S entity);
}

interface BiologicoRepositoryInternal {
    <S extends Biologico> Mono<S> insert(S entity);
    <S extends Biologico> Mono<S> save(S entity);
    Mono<Integer> update(Biologico entity);

    Flux<Biologico> findAll();
    Mono<Biologico> findById(Long id);
    Flux<Biologico> findAllBy(Pageable pageable);
    Flux<Biologico> findAllBy(Pageable pageable, Criteria criteria);
}
