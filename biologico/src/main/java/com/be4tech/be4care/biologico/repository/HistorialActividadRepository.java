package com.be4tech.be4care.biologico.repository;

import com.be4tech.be4care.biologico.domain.HistorialActividad;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the HistorialActividad entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HistorialActividadRepository extends R2dbcRepository<HistorialActividad, Long>, HistorialActividadRepositoryInternal {
    Flux<HistorialActividad> findAllBy(Pageable pageable);

    @Query("SELECT * FROM historial_actividad entity WHERE entity.ciclo_actividad_id = :id")
    Flux<HistorialActividad> findByCicloActividad(Long id);

    @Query("SELECT * FROM historial_actividad entity WHERE entity.ciclo_actividad_id IS NULL")
    Flux<HistorialActividad> findAllWhereCicloActividadIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<HistorialActividad> findAll();

    @Override
    Mono<HistorialActividad> findById(Long id);

    @Override
    <S extends HistorialActividad> Mono<S> save(S entity);
}

interface HistorialActividadRepositoryInternal {
    <S extends HistorialActividad> Mono<S> insert(S entity);
    <S extends HistorialActividad> Mono<S> save(S entity);
    Mono<Integer> update(HistorialActividad entity);

    Flux<HistorialActividad> findAll();
    Mono<HistorialActividad> findById(Long id);
    Flux<HistorialActividad> findAllBy(Pageable pageable);
    Flux<HistorialActividad> findAllBy(Pageable pageable, Criteria criteria);
}
