package com.be4tech.be4care.biologico.repository.rowmapper;

import com.be4tech.be4care.biologico.domain.Vacuna;
import com.be4tech.be4care.biologico.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Vacuna}, with proper type conversions.
 */
@Service
public class VacunaRowMapper implements BiFunction<Row, String, Vacuna> {

    private final ColumnConverter converter;

    public VacunaRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Vacuna} stored in the database.
     */
    @Override
    public Vacuna apply(Row row, String prefix) {
        Vacuna entity = new Vacuna();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setNombreVacuna(converter.fromRow(row, prefix + "_nombre_vacuna", String.class));
        entity.setTipoVacuna(converter.fromRow(row, prefix + "_tipo_vacuna", String.class));
        entity.setDescripcionVacuna(converter.fromRow(row, prefix + "_descripcion_vacuna", String.class));
        entity.setFechaIngreso(converter.fromRow(row, prefix + "_fecha_ingreso", Instant.class));
        entity.setLoteVacuna(converter.fromRow(row, prefix + "_lote_vacuna", String.class));
        entity.setFabricanteId(converter.fromRow(row, prefix + "_fabricante_id", Long.class));
        return entity;
    }
}
