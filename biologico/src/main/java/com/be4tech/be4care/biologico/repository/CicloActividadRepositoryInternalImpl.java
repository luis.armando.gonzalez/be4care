package com.be4tech.be4care.biologico.repository;

import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

import com.be4tech.be4care.biologico.domain.CicloActividad;
import com.be4tech.be4care.biologico.repository.rowmapper.BiologicoRowMapper;
import com.be4tech.be4care.biologico.repository.rowmapper.CicloActividadRowMapper;
import com.be4tech.be4care.biologico.repository.rowmapper.UserRowMapper;
import com.be4tech.be4care.biologico.repository.rowmapper.VacunaRowMapper;
import com.be4tech.be4care.biologico.service.EntityManager;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoinCondition;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive custom repository implementation for the CicloActividad entity.
 */
@SuppressWarnings("unused")
class CicloActividadRepositoryInternalImpl implements CicloActividadRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final BiologicoRowMapper biologicoMapper;
    private final VacunaRowMapper vacunaMapper;
    private final UserRowMapper userMapper;
    private final CicloActividadRowMapper cicloactividadMapper;

    private static final Table entityTable = Table.aliased("ciclo_actividad", EntityManager.ENTITY_ALIAS);
    private static final Table biologicoTable = Table.aliased("biologico", "biologico");
    private static final Table vacunaTable = Table.aliased("vacuna", "vacuna");
    private static final Table userTable = Table.aliased("jhi_user", "e_user");

    public CicloActividadRepositoryInternalImpl(
        R2dbcEntityTemplate template,
        EntityManager entityManager,
        BiologicoRowMapper biologicoMapper,
        VacunaRowMapper vacunaMapper,
        UserRowMapper userMapper,
        CicloActividadRowMapper cicloactividadMapper
    ) {
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.biologicoMapper = biologicoMapper;
        this.vacunaMapper = vacunaMapper;
        this.userMapper = userMapper;
        this.cicloactividadMapper = cicloactividadMapper;
    }

    @Override
    public Flux<CicloActividad> findAllBy(Pageable pageable) {
        return findAllBy(pageable, null);
    }

    @Override
    public Flux<CicloActividad> findAllBy(Pageable pageable, Criteria criteria) {
        return createQuery(pageable, criteria).all();
    }

    RowsFetchSpec<CicloActividad> createQuery(Pageable pageable, Criteria criteria) {
        List<Expression> columns = CicloActividadSqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        columns.addAll(BiologicoSqlHelper.getColumns(biologicoTable, "biologico"));
        columns.addAll(VacunaSqlHelper.getColumns(vacunaTable, "vacuna"));
        columns.addAll(UserSqlHelper.getColumns(userTable, "user"));
        SelectFromAndJoinCondition selectFrom = Select
            .builder()
            .select(columns)
            .from(entityTable)
            .leftOuterJoin(biologicoTable)
            .on(Column.create("biologico_id", entityTable))
            .equals(Column.create("id", biologicoTable))
            .leftOuterJoin(vacunaTable)
            .on(Column.create("vacuna_id", entityTable))
            .equals(Column.create("id", vacunaTable))
            .leftOuterJoin(userTable)
            .on(Column.create("user_id", entityTable))
            .equals(Column.create("id", userTable));

        String select = entityManager.createSelect(selectFrom, CicloActividad.class, pageable, criteria);
        String alias = entityTable.getReferenceName().getReference();
        String selectWhere = Optional
            .ofNullable(criteria)
            .map(crit ->
                new StringBuilder(select)
                    .append(" ")
                    .append("WHERE")
                    .append(" ")
                    .append(alias)
                    .append(".")
                    .append(crit.toString())
                    .toString()
            )
            .orElse(select); // TODO remove once https://github.com/spring-projects/spring-data-jdbc/issues/907 will be fixed
        return db.sql(selectWhere).map(this::process);
    }

    @Override
    public Flux<CicloActividad> findAll() {
        return findAllBy(null, null);
    }

    @Override
    public Mono<CicloActividad> findById(Long id) {
        return createQuery(null, where("id").is(id)).one();
    }

    private CicloActividad process(Row row, RowMetadata metadata) {
        CicloActividad entity = cicloactividadMapper.apply(row, "e");
        entity.setBiologico(biologicoMapper.apply(row, "biologico"));
        entity.setVacuna(vacunaMapper.apply(row, "vacuna"));
        entity.setUser(userMapper.apply(row, "user"));
        return entity;
    }

    @Override
    public <S extends CicloActividad> Mono<S> insert(S entity) {
        return entityManager.insert(entity);
    }

    @Override
    public <S extends CicloActividad> Mono<S> save(S entity) {
        if (entity.getId() == null) {
            return insert(entity);
        } else {
            return update(entity)
                .map(numberOfUpdates -> {
                    if (numberOfUpdates.intValue() <= 0) {
                        throw new IllegalStateException("Unable to update CicloActividad with id = " + entity.getId());
                    }
                    return entity;
                });
        }
    }

    @Override
    public Mono<Integer> update(CicloActividad entity) {
        //fixme is this the proper way?
        return r2dbcEntityTemplate.update(entity).thenReturn(1);
    }
}
