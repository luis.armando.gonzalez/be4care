package com.be4tech.be4care.biologico.repository;

import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

import com.be4tech.be4care.biologico.domain.Vacuna;
import com.be4tech.be4care.biologico.repository.rowmapper.FabricanteRowMapper;
import com.be4tech.be4care.biologico.repository.rowmapper.VacunaRowMapper;
import com.be4tech.be4care.biologico.service.EntityManager;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoinCondition;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive custom repository implementation for the Vacuna entity.
 */
@SuppressWarnings("unused")
class VacunaRepositoryInternalImpl implements VacunaRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final FabricanteRowMapper fabricanteMapper;
    private final VacunaRowMapper vacunaMapper;

    private static final Table entityTable = Table.aliased("vacuna", EntityManager.ENTITY_ALIAS);
    private static final Table fabricanteTable = Table.aliased("fabricante", "fabricante");

    public VacunaRepositoryInternalImpl(
        R2dbcEntityTemplate template,
        EntityManager entityManager,
        FabricanteRowMapper fabricanteMapper,
        VacunaRowMapper vacunaMapper
    ) {
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.fabricanteMapper = fabricanteMapper;
        this.vacunaMapper = vacunaMapper;
    }

    @Override
    public Flux<Vacuna> findAllBy(Pageable pageable) {
        return findAllBy(pageable, null);
    }

    @Override
    public Flux<Vacuna> findAllBy(Pageable pageable, Criteria criteria) {
        return createQuery(pageable, criteria).all();
    }

    RowsFetchSpec<Vacuna> createQuery(Pageable pageable, Criteria criteria) {
        List<Expression> columns = VacunaSqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        columns.addAll(FabricanteSqlHelper.getColumns(fabricanteTable, "fabricante"));
        SelectFromAndJoinCondition selectFrom = Select
            .builder()
            .select(columns)
            .from(entityTable)
            .leftOuterJoin(fabricanteTable)
            .on(Column.create("fabricante_id", entityTable))
            .equals(Column.create("id", fabricanteTable));

        String select = entityManager.createSelect(selectFrom, Vacuna.class, pageable, criteria);
        String alias = entityTable.getReferenceName().getReference();
        String selectWhere = Optional
            .ofNullable(criteria)
            .map(crit ->
                new StringBuilder(select)
                    .append(" ")
                    .append("WHERE")
                    .append(" ")
                    .append(alias)
                    .append(".")
                    .append(crit.toString())
                    .toString()
            )
            .orElse(select); // TODO remove once https://github.com/spring-projects/spring-data-jdbc/issues/907 will be fixed
        return db.sql(selectWhere).map(this::process);
    }

    @Override
    public Flux<Vacuna> findAll() {
        return findAllBy(null, null);
    }

    @Override
    public Mono<Vacuna> findById(Long id) {
        return createQuery(null, where("id").is(id)).one();
    }

    private Vacuna process(Row row, RowMetadata metadata) {
        Vacuna entity = vacunaMapper.apply(row, "e");
        entity.setFabricante(fabricanteMapper.apply(row, "fabricante"));
        return entity;
    }

    @Override
    public <S extends Vacuna> Mono<S> insert(S entity) {
        return entityManager.insert(entity);
    }

    @Override
    public <S extends Vacuna> Mono<S> save(S entity) {
        if (entity.getId() == null) {
            return insert(entity);
        } else {
            return update(entity)
                .map(numberOfUpdates -> {
                    if (numberOfUpdates.intValue() <= 0) {
                        throw new IllegalStateException("Unable to update Vacuna with id = " + entity.getId());
                    }
                    return entity;
                });
        }
    }

    @Override
    public Mono<Integer> update(Vacuna entity) {
        //fixme is this the proper way?
        return r2dbcEntityTemplate.update(entity).thenReturn(1);
    }
}
