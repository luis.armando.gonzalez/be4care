package com.be4tech.be4care.biologico.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A CicloActividad.
 */
@Table("ciclo_actividad")
public class CicloActividad implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("periodo_ciclo")
    private Integer periodoCiclo;

    @Column("cantidad")
    private Integer cantidad;

    @Column("fecha_inicio_ciclo")
    private Instant fechaInicioCiclo;

    @Transient
    private Biologico biologico;

    @Transient
    @JsonIgnoreProperties(value = { "fabricante" }, allowSetters = true)
    private Vacuna vacuna;

    @Transient
    private User user;

    @Column("biologico_id")
    private Long biologicoId;

    @Column("vacuna_id")
    private Long vacunaId;

    @Column("user_id")
    private String userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public CicloActividad id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPeriodoCiclo() {
        return this.periodoCiclo;
    }

    public CicloActividad periodoCiclo(Integer periodoCiclo) {
        this.setPeriodoCiclo(periodoCiclo);
        return this;
    }

    public void setPeriodoCiclo(Integer periodoCiclo) {
        this.periodoCiclo = periodoCiclo;
    }

    public Integer getCantidad() {
        return this.cantidad;
    }

    public CicloActividad cantidad(Integer cantidad) {
        this.setCantidad(cantidad);
        return this;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Instant getFechaInicioCiclo() {
        return this.fechaInicioCiclo;
    }

    public CicloActividad fechaInicioCiclo(Instant fechaInicioCiclo) {
        this.setFechaInicioCiclo(fechaInicioCiclo);
        return this;
    }

    public void setFechaInicioCiclo(Instant fechaInicioCiclo) {
        this.fechaInicioCiclo = fechaInicioCiclo;
    }

    public Biologico getBiologico() {
        return this.biologico;
    }

    public void setBiologico(Biologico biologico) {
        this.biologico = biologico;
        this.biologicoId = biologico != null ? biologico.getId() : null;
    }

    public CicloActividad biologico(Biologico biologico) {
        this.setBiologico(biologico);
        return this;
    }

    public Vacuna getVacuna() {
        return this.vacuna;
    }

    public void setVacuna(Vacuna vacuna) {
        this.vacuna = vacuna;
        this.vacunaId = vacuna != null ? vacuna.getId() : null;
    }

    public CicloActividad vacuna(Vacuna vacuna) {
        this.setVacuna(vacuna);
        return this;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user != null ? user.getId() : null;
    }

    public CicloActividad user(User user) {
        this.setUser(user);
        return this;
    }

    public Long getBiologicoId() {
        return this.biologicoId;
    }

    public void setBiologicoId(Long biologico) {
        this.biologicoId = biologico;
    }

    public Long getVacunaId() {
        return this.vacunaId;
    }

    public void setVacunaId(Long vacuna) {
        this.vacunaId = vacuna;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String user) {
        this.userId = user;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CicloActividad)) {
            return false;
        }
        return id != null && id.equals(((CicloActividad) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CicloActividad{" +
            "id=" + getId() +
            ", periodoCiclo=" + getPeriodoCiclo() +
            ", cantidad=" + getCantidad() +
            ", fechaInicioCiclo='" + getFechaInicioCiclo() + "'" +
            "}";
    }
}
