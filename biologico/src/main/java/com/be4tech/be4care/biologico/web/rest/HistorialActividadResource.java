package com.be4tech.be4care.biologico.web.rest;

import com.be4tech.be4care.biologico.domain.HistorialActividad;
import com.be4tech.be4care.biologico.repository.HistorialActividadRepository;
import com.be4tech.be4care.biologico.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.biologico.domain.HistorialActividad}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class HistorialActividadResource {

    private final Logger log = LoggerFactory.getLogger(HistorialActividadResource.class);

    private static final String ENTITY_NAME = "biologicoHistorialActividad";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HistorialActividadRepository historialActividadRepository;

    public HistorialActividadResource(HistorialActividadRepository historialActividadRepository) {
        this.historialActividadRepository = historialActividadRepository;
    }

    /**
     * {@code POST  /historial-actividads} : Create a new historialActividad.
     *
     * @param historialActividad the historialActividad to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new historialActividad, or with status {@code 400 (Bad Request)} if the historialActividad has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/historial-actividads")
    public Mono<ResponseEntity<HistorialActividad>> createHistorialActividad(@RequestBody HistorialActividad historialActividad)
        throws URISyntaxException {
        log.debug("REST request to save HistorialActividad : {}", historialActividad);
        if (historialActividad.getId() != null) {
            throw new BadRequestAlertException("A new historialActividad cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return historialActividadRepository
            .save(historialActividad)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/historial-actividads/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /historial-actividads/:id} : Updates an existing historialActividad.
     *
     * @param id the id of the historialActividad to save.
     * @param historialActividad the historialActividad to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated historialActividad,
     * or with status {@code 400 (Bad Request)} if the historialActividad is not valid,
     * or with status {@code 500 (Internal Server Error)} if the historialActividad couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/historial-actividads/{id}")
    public Mono<ResponseEntity<HistorialActividad>> updateHistorialActividad(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody HistorialActividad historialActividad
    ) throws URISyntaxException {
        log.debug("REST request to update HistorialActividad : {}, {}", id, historialActividad);
        if (historialActividad.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, historialActividad.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return historialActividadRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                return historialActividadRepository
                    .save(historialActividad)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /historial-actividads/:id} : Partial updates given fields of an existing historialActividad, field will ignore if it is null
     *
     * @param id the id of the historialActividad to save.
     * @param historialActividad the historialActividad to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated historialActividad,
     * or with status {@code 400 (Bad Request)} if the historialActividad is not valid,
     * or with status {@code 404 (Not Found)} if the historialActividad is not found,
     * or with status {@code 500 (Internal Server Error)} if the historialActividad couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/historial-actividads/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<HistorialActividad>> partialUpdateHistorialActividad(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody HistorialActividad historialActividad
    ) throws URISyntaxException {
        log.debug("REST request to partial update HistorialActividad partially : {}, {}", id, historialActividad);
        if (historialActividad.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, historialActividad.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return historialActividadRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                Mono<HistorialActividad> result = historialActividadRepository
                    .findById(historialActividad.getId())
                    .map(existingHistorialActividad -> {
                        if (historialActividad.getFechaHistorial() != null) {
                            existingHistorialActividad.setFechaHistorial(historialActividad.getFechaHistorial());
                        }

                        return existingHistorialActividad;
                    })
                    .flatMap(historialActividadRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /historial-actividads} : get all the historialActividads.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of historialActividads in body.
     */
    @GetMapping("/historial-actividads")
    public Mono<ResponseEntity<List<HistorialActividad>>> getAllHistorialActividads(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of HistorialActividads");
        return historialActividadRepository
            .count()
            .zipWith(historialActividadRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /historial-actividads/:id} : get the "id" historialActividad.
     *
     * @param id the id of the historialActividad to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the historialActividad, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/historial-actividads/{id}")
    public Mono<ResponseEntity<HistorialActividad>> getHistorialActividad(@PathVariable Long id) {
        log.debug("REST request to get HistorialActividad : {}", id);
        Mono<HistorialActividad> historialActividad = historialActividadRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(historialActividad);
    }

    /**
     * {@code DELETE  /historial-actividads/:id} : delete the "id" historialActividad.
     *
     * @param id the id of the historialActividad to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/historial-actividads/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteHistorialActividad(@PathVariable Long id) {
        log.debug("REST request to delete HistorialActividad : {}", id);
        return historialActividadRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
