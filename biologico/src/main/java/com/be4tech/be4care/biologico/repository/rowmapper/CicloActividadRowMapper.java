package com.be4tech.be4care.biologico.repository.rowmapper;

import com.be4tech.be4care.biologico.domain.CicloActividad;
import com.be4tech.be4care.biologico.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link CicloActividad}, with proper type conversions.
 */
@Service
public class CicloActividadRowMapper implements BiFunction<Row, String, CicloActividad> {

    private final ColumnConverter converter;

    public CicloActividadRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link CicloActividad} stored in the database.
     */
    @Override
    public CicloActividad apply(Row row, String prefix) {
        CicloActividad entity = new CicloActividad();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setPeriodoCiclo(converter.fromRow(row, prefix + "_periodo_ciclo", Integer.class));
        entity.setCantidad(converter.fromRow(row, prefix + "_cantidad", Integer.class));
        entity.setFechaInicioCiclo(converter.fromRow(row, prefix + "_fecha_inicio_ciclo", Instant.class));
        entity.setBiologicoId(converter.fromRow(row, prefix + "_biologico_id", Long.class));
        entity.setVacunaId(converter.fromRow(row, prefix + "_vacuna_id", Long.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        return entity;
    }
}
