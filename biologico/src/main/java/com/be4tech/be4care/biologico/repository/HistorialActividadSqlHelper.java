package com.be4tech.be4care.biologico.repository;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Table;

public class HistorialActividadSqlHelper {

    public static List<Expression> getColumns(Table table, String columnPrefix) {
        List<Expression> columns = new ArrayList<>();
        columns.add(Column.aliased("id", table, columnPrefix + "_id"));
        columns.add(Column.aliased("fecha_historial", table, columnPrefix + "_fecha_historial"));

        columns.add(Column.aliased("ciclo_actividad_id", table, columnPrefix + "_ciclo_actividad_id"));
        return columns;
    }
}
