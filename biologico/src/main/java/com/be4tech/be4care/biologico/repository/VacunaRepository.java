package com.be4tech.be4care.biologico.repository;

import com.be4tech.be4care.biologico.domain.Vacuna;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Vacuna entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VacunaRepository extends R2dbcRepository<Vacuna, Long>, VacunaRepositoryInternal {
    Flux<Vacuna> findAllBy(Pageable pageable);

    @Query("SELECT * FROM vacuna entity WHERE entity.fabricante_id = :id")
    Flux<Vacuna> findByFabricante(Long id);

    @Query("SELECT * FROM vacuna entity WHERE entity.fabricante_id IS NULL")
    Flux<Vacuna> findAllWhereFabricanteIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Vacuna> findAll();

    @Override
    Mono<Vacuna> findById(Long id);

    @Override
    <S extends Vacuna> Mono<S> save(S entity);
}

interface VacunaRepositoryInternal {
    <S extends Vacuna> Mono<S> insert(S entity);
    <S extends Vacuna> Mono<S> save(S entity);
    Mono<Integer> update(Vacuna entity);

    Flux<Vacuna> findAll();
    Mono<Vacuna> findById(Long id);
    Flux<Vacuna> findAllBy(Pageable pageable);
    Flux<Vacuna> findAllBy(Pageable pageable, Criteria criteria);
}
