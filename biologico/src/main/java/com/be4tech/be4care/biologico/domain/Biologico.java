package com.be4tech.be4care.biologico.domain;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Biologico.
 */
@Table("biologico")
public class Biologico implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("tipo_biologico")
    private String tipoBiologico;

    @Column("detalle_biologico")
    private String detalleBiologico;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Biologico id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoBiologico() {
        return this.tipoBiologico;
    }

    public Biologico tipoBiologico(String tipoBiologico) {
        this.setTipoBiologico(tipoBiologico);
        return this;
    }

    public void setTipoBiologico(String tipoBiologico) {
        this.tipoBiologico = tipoBiologico;
    }

    public String getDetalleBiologico() {
        return this.detalleBiologico;
    }

    public Biologico detalleBiologico(String detalleBiologico) {
        this.setDetalleBiologico(detalleBiologico);
        return this;
    }

    public void setDetalleBiologico(String detalleBiologico) {
        this.detalleBiologico = detalleBiologico;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Biologico)) {
            return false;
        }
        return id != null && id.equals(((Biologico) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Biologico{" +
            "id=" + getId() +
            ", tipoBiologico='" + getTipoBiologico() + "'" +
            ", detalleBiologico='" + getDetalleBiologico() + "'" +
            "}";
    }
}
