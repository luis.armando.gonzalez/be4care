package com.be4tech.be4care.biologico.web.rest;

import com.be4tech.be4care.biologico.domain.Fabricante;
import com.be4tech.be4care.biologico.repository.FabricanteRepository;
import com.be4tech.be4care.biologico.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.biologico.domain.Fabricante}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class FabricanteResource {

    private final Logger log = LoggerFactory.getLogger(FabricanteResource.class);

    private static final String ENTITY_NAME = "biologicoFabricante";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FabricanteRepository fabricanteRepository;

    public FabricanteResource(FabricanteRepository fabricanteRepository) {
        this.fabricanteRepository = fabricanteRepository;
    }

    /**
     * {@code POST  /fabricantes} : Create a new fabricante.
     *
     * @param fabricante the fabricante to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fabricante, or with status {@code 400 (Bad Request)} if the fabricante has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fabricantes")
    public Mono<ResponseEntity<Fabricante>> createFabricante(@RequestBody Fabricante fabricante) throws URISyntaxException {
        log.debug("REST request to save Fabricante : {}", fabricante);
        if (fabricante.getId() != null) {
            throw new BadRequestAlertException("A new fabricante cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return fabricanteRepository
            .save(fabricante)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/fabricantes/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /fabricantes/:id} : Updates an existing fabricante.
     *
     * @param id the id of the fabricante to save.
     * @param fabricante the fabricante to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fabricante,
     * or with status {@code 400 (Bad Request)} if the fabricante is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fabricante couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fabricantes/{id}")
    public Mono<ResponseEntity<Fabricante>> updateFabricante(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Fabricante fabricante
    ) throws URISyntaxException {
        log.debug("REST request to update Fabricante : {}, {}", id, fabricante);
        if (fabricante.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, fabricante.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return fabricanteRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                return fabricanteRepository
                    .save(fabricante)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /fabricantes/:id} : Partial updates given fields of an existing fabricante, field will ignore if it is null
     *
     * @param id the id of the fabricante to save.
     * @param fabricante the fabricante to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fabricante,
     * or with status {@code 400 (Bad Request)} if the fabricante is not valid,
     * or with status {@code 404 (Not Found)} if the fabricante is not found,
     * or with status {@code 500 (Internal Server Error)} if the fabricante couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/fabricantes/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Fabricante>> partialUpdateFabricante(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Fabricante fabricante
    ) throws URISyntaxException {
        log.debug("REST request to partial update Fabricante partially : {}, {}", id, fabricante);
        if (fabricante.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, fabricante.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return fabricanteRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                Mono<Fabricante> result = fabricanteRepository
                    .findById(fabricante.getId())
                    .map(existingFabricante -> {
                        if (fabricante.getNombreFabricante() != null) {
                            existingFabricante.setNombreFabricante(fabricante.getNombreFabricante());
                        }
                        if (fabricante.getProductosFabricanet() != null) {
                            existingFabricante.setProductosFabricanet(fabricante.getProductosFabricanet());
                        }
                        if (fabricante.getDetalleFabricante() != null) {
                            existingFabricante.setDetalleFabricante(fabricante.getDetalleFabricante());
                        }

                        return existingFabricante;
                    })
                    .flatMap(fabricanteRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /fabricantes} : get all the fabricantes.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fabricantes in body.
     */
    @GetMapping("/fabricantes")
    public Mono<ResponseEntity<List<Fabricante>>> getAllFabricantes(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Fabricantes");
        return fabricanteRepository
            .count()
            .zipWith(fabricanteRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /fabricantes/:id} : get the "id" fabricante.
     *
     * @param id the id of the fabricante to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fabricante, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fabricantes/{id}")
    public Mono<ResponseEntity<Fabricante>> getFabricante(@PathVariable Long id) {
        log.debug("REST request to get Fabricante : {}", id);
        Mono<Fabricante> fabricante = fabricanteRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(fabricante);
    }

    /**
     * {@code DELETE  /fabricantes/:id} : delete the "id" fabricante.
     *
     * @param id the id of the fabricante to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fabricantes/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteFabricante(@PathVariable Long id) {
        log.debug("REST request to delete Fabricante : {}", id);
        return fabricanteRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
