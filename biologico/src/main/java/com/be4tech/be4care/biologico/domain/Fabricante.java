package com.be4tech.be4care.biologico.domain;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Fabricante.
 */
@Table("fabricante")
public class Fabricante implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("nombre_fabricante")
    private String nombreFabricante;

    @Column("productos_fabricanet")
    private String productosFabricanet;

    @Column("detalle_fabricante")
    private String detalleFabricante;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Fabricante id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreFabricante() {
        return this.nombreFabricante;
    }

    public Fabricante nombreFabricante(String nombreFabricante) {
        this.setNombreFabricante(nombreFabricante);
        return this;
    }

    public void setNombreFabricante(String nombreFabricante) {
        this.nombreFabricante = nombreFabricante;
    }

    public String getProductosFabricanet() {
        return this.productosFabricanet;
    }

    public Fabricante productosFabricanet(String productosFabricanet) {
        this.setProductosFabricanet(productosFabricanet);
        return this;
    }

    public void setProductosFabricanet(String productosFabricanet) {
        this.productosFabricanet = productosFabricanet;
    }

    public String getDetalleFabricante() {
        return this.detalleFabricante;
    }

    public Fabricante detalleFabricante(String detalleFabricante) {
        this.setDetalleFabricante(detalleFabricante);
        return this;
    }

    public void setDetalleFabricante(String detalleFabricante) {
        this.detalleFabricante = detalleFabricante;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Fabricante)) {
            return false;
        }
        return id != null && id.equals(((Fabricante) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Fabricante{" +
            "id=" + getId() +
            ", nombreFabricante='" + getNombreFabricante() + "'" +
            ", productosFabricanet='" + getProductosFabricanet() + "'" +
            ", detalleFabricante='" + getDetalleFabricante() + "'" +
            "}";
    }
}
