package com.be4tech.be4care.biologico.web.rest;

import com.be4tech.be4care.biologico.domain.CicloActividad;
import com.be4tech.be4care.biologico.repository.CicloActividadRepository;
import com.be4tech.be4care.biologico.repository.UserRepository;
import com.be4tech.be4care.biologico.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.biologico.domain.CicloActividad}.
 */
@RestController
@RequestMapping("/api")
public class CicloActividadResource {

    private final Logger log = LoggerFactory.getLogger(CicloActividadResource.class);

    private static final String ENTITY_NAME = "biologicoCicloActividad";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CicloActividadRepository cicloActividadRepository;

    private final UserRepository userRepository;

    public CicloActividadResource(CicloActividadRepository cicloActividadRepository, UserRepository userRepository) {
        this.cicloActividadRepository = cicloActividadRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /ciclo-actividads} : Create a new cicloActividad.
     *
     * @param cicloActividad the cicloActividad to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cicloActividad, or with status {@code 400 (Bad Request)} if the cicloActividad has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ciclo-actividads")
    public Mono<ResponseEntity<CicloActividad>> createCicloActividad(@RequestBody CicloActividad cicloActividad) throws URISyntaxException {
        log.debug("REST request to save CicloActividad : {}", cicloActividad);
        if (cicloActividad.getId() != null) {
            throw new BadRequestAlertException("A new cicloActividad cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (cicloActividad.getUser() != null) {
            // Save user in case it's new and only exists in gateway
            userRepository.save(cicloActividad.getUser());
        }
        return cicloActividadRepository
            .save(cicloActividad)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/ciclo-actividads/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /ciclo-actividads/:id} : Updates an existing cicloActividad.
     *
     * @param id the id of the cicloActividad to save.
     * @param cicloActividad the cicloActividad to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cicloActividad,
     * or with status {@code 400 (Bad Request)} if the cicloActividad is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cicloActividad couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ciclo-actividads/{id}")
    public Mono<ResponseEntity<CicloActividad>> updateCicloActividad(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CicloActividad cicloActividad
    ) throws URISyntaxException {
        log.debug("REST request to update CicloActividad : {}, {}", id, cicloActividad);
        if (cicloActividad.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, cicloActividad.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return cicloActividadRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (cicloActividad.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(cicloActividad.getUser());
                }
                return cicloActividadRepository
                    .save(cicloActividad)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /ciclo-actividads/:id} : Partial updates given fields of an existing cicloActividad, field will ignore if it is null
     *
     * @param id the id of the cicloActividad to save.
     * @param cicloActividad the cicloActividad to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cicloActividad,
     * or with status {@code 400 (Bad Request)} if the cicloActividad is not valid,
     * or with status {@code 404 (Not Found)} if the cicloActividad is not found,
     * or with status {@code 500 (Internal Server Error)} if the cicloActividad couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/ciclo-actividads/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<CicloActividad>> partialUpdateCicloActividad(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CicloActividad cicloActividad
    ) throws URISyntaxException {
        log.debug("REST request to partial update CicloActividad partially : {}, {}", id, cicloActividad);
        if (cicloActividad.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, cicloActividad.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return cicloActividadRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (cicloActividad.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(cicloActividad.getUser());
                }

                Mono<CicloActividad> result = cicloActividadRepository
                    .findById(cicloActividad.getId())
                    .map(existingCicloActividad -> {
                        if (cicloActividad.getPeriodoCiclo() != null) {
                            existingCicloActividad.setPeriodoCiclo(cicloActividad.getPeriodoCiclo());
                        }
                        if (cicloActividad.getCantidad() != null) {
                            existingCicloActividad.setCantidad(cicloActividad.getCantidad());
                        }
                        if (cicloActividad.getFechaInicioCiclo() != null) {
                            existingCicloActividad.setFechaInicioCiclo(cicloActividad.getFechaInicioCiclo());
                        }

                        return existingCicloActividad;
                    })
                    .flatMap(cicloActividadRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /ciclo-actividads} : get all the cicloActividads.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cicloActividads in body.
     */
    @GetMapping("/ciclo-actividads")
    public Mono<ResponseEntity<List<CicloActividad>>> getAllCicloActividads(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of CicloActividads");
        return cicloActividadRepository
            .count()
            .zipWith(cicloActividadRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /ciclo-actividads/:id} : get the "id" cicloActividad.
     *
     * @param id the id of the cicloActividad to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cicloActividad, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ciclo-actividads/{id}")
    public Mono<ResponseEntity<CicloActividad>> getCicloActividad(@PathVariable Long id) {
        log.debug("REST request to get CicloActividad : {}", id);
        Mono<CicloActividad> cicloActividad = cicloActividadRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(cicloActividad);
    }

    /**
     * {@code DELETE  /ciclo-actividads/:id} : delete the "id" cicloActividad.
     *
     * @param id the id of the cicloActividad to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ciclo-actividads/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteCicloActividad(@PathVariable Long id) {
        log.debug("REST request to delete CicloActividad : {}", id);
        return cicloActividadRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
