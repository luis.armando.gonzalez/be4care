package com.be4tech.be4care.biologico.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A HistorialActividad.
 */
@Table("historial_actividad")
public class HistorialActividad implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("fecha_historial")
    private Instant fechaHistorial;

    @Transient
    @JsonIgnoreProperties(value = { "biologico", "vacuna", "user" }, allowSetters = true)
    private CicloActividad cicloActividad;

    @Column("ciclo_actividad_id")
    private Long cicloActividadId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public HistorialActividad id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getFechaHistorial() {
        return this.fechaHistorial;
    }

    public HistorialActividad fechaHistorial(Instant fechaHistorial) {
        this.setFechaHistorial(fechaHistorial);
        return this;
    }

    public void setFechaHistorial(Instant fechaHistorial) {
        this.fechaHistorial = fechaHistorial;
    }

    public CicloActividad getCicloActividad() {
        return this.cicloActividad;
    }

    public void setCicloActividad(CicloActividad cicloActividad) {
        this.cicloActividad = cicloActividad;
        this.cicloActividadId = cicloActividad != null ? cicloActividad.getId() : null;
    }

    public HistorialActividad cicloActividad(CicloActividad cicloActividad) {
        this.setCicloActividad(cicloActividad);
        return this;
    }

    public Long getCicloActividadId() {
        return this.cicloActividadId;
    }

    public void setCicloActividadId(Long cicloActividad) {
        this.cicloActividadId = cicloActividad;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HistorialActividad)) {
            return false;
        }
        return id != null && id.equals(((HistorialActividad) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HistorialActividad{" +
            "id=" + getId() +
            ", fechaHistorial='" + getFechaHistorial() + "'" +
            "}";
    }
}
