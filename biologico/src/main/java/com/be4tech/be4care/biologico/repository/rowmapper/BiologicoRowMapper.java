package com.be4tech.be4care.biologico.repository.rowmapper;

import com.be4tech.be4care.biologico.domain.Biologico;
import com.be4tech.be4care.biologico.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Biologico}, with proper type conversions.
 */
@Service
public class BiologicoRowMapper implements BiFunction<Row, String, Biologico> {

    private final ColumnConverter converter;

    public BiologicoRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Biologico} stored in the database.
     */
    @Override
    public Biologico apply(Row row, String prefix) {
        Biologico entity = new Biologico();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setTipoBiologico(converter.fromRow(row, prefix + "_tipo_biologico", String.class));
        entity.setDetalleBiologico(converter.fromRow(row, prefix + "_detalle_biologico", String.class));
        return entity;
    }
}
