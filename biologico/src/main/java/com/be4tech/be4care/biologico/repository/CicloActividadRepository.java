package com.be4tech.be4care.biologico.repository;

import com.be4tech.be4care.biologico.domain.CicloActividad;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the CicloActividad entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CicloActividadRepository extends R2dbcRepository<CicloActividad, Long>, CicloActividadRepositoryInternal {
    Flux<CicloActividad> findAllBy(Pageable pageable);

    @Query("SELECT * FROM ciclo_actividad entity WHERE entity.biologico_id = :id")
    Flux<CicloActividad> findByBiologico(Long id);

    @Query("SELECT * FROM ciclo_actividad entity WHERE entity.biologico_id IS NULL")
    Flux<CicloActividad> findAllWhereBiologicoIsNull();

    @Query("SELECT * FROM ciclo_actividad entity WHERE entity.vacuna_id = :id")
    Flux<CicloActividad> findByVacuna(Long id);

    @Query("SELECT * FROM ciclo_actividad entity WHERE entity.vacuna_id IS NULL")
    Flux<CicloActividad> findAllWhereVacunaIsNull();

    @Query("SELECT * FROM ciclo_actividad entity WHERE entity.user_id = :id")
    Flux<CicloActividad> findByUser(Long id);

    @Query("SELECT * FROM ciclo_actividad entity WHERE entity.user_id IS NULL")
    Flux<CicloActividad> findAllWhereUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<CicloActividad> findAll();

    @Override
    Mono<CicloActividad> findById(Long id);

    @Override
    <S extends CicloActividad> Mono<S> save(S entity);
}

interface CicloActividadRepositoryInternal {
    <S extends CicloActividad> Mono<S> insert(S entity);
    <S extends CicloActividad> Mono<S> save(S entity);
    Mono<Integer> update(CicloActividad entity);

    Flux<CicloActividad> findAll();
    Mono<CicloActividad> findById(Long id);
    Flux<CicloActividad> findAllBy(Pageable pageable);
    Flux<CicloActividad> findAllBy(Pageable pageable, Criteria criteria);
}
