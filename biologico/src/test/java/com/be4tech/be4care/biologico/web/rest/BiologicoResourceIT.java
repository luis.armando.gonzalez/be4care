package com.be4tech.be4care.biologico.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.biologico.IntegrationTest;
import com.be4tech.be4care.biologico.domain.Biologico;
import com.be4tech.be4care.biologico.repository.BiologicoRepository;
import com.be4tech.be4care.biologico.service.EntityManager;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link BiologicoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class BiologicoResourceIT {

    private static final String DEFAULT_TIPO_BIOLOGICO = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_BIOLOGICO = "BBBBBBBBBB";

    private static final String DEFAULT_DETALLE_BIOLOGICO = "AAAAAAAAAA";
    private static final String UPDATED_DETALLE_BIOLOGICO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/biologicos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private BiologicoRepository biologicoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Biologico biologico;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Biologico createEntity(EntityManager em) {
        Biologico biologico = new Biologico().tipoBiologico(DEFAULT_TIPO_BIOLOGICO).detalleBiologico(DEFAULT_DETALLE_BIOLOGICO);
        return biologico;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Biologico createUpdatedEntity(EntityManager em) {
        Biologico biologico = new Biologico().tipoBiologico(UPDATED_TIPO_BIOLOGICO).detalleBiologico(UPDATED_DETALLE_BIOLOGICO);
        return biologico;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Biologico.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        biologico = createEntity(em);
    }

    @Test
    void createBiologico() throws Exception {
        int databaseSizeBeforeCreate = biologicoRepository.findAll().collectList().block().size();
        // Create the Biologico
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(biologico))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Biologico in the database
        List<Biologico> biologicoList = biologicoRepository.findAll().collectList().block();
        assertThat(biologicoList).hasSize(databaseSizeBeforeCreate + 1);
        Biologico testBiologico = biologicoList.get(biologicoList.size() - 1);
        assertThat(testBiologico.getTipoBiologico()).isEqualTo(DEFAULT_TIPO_BIOLOGICO);
        assertThat(testBiologico.getDetalleBiologico()).isEqualTo(DEFAULT_DETALLE_BIOLOGICO);
    }

    @Test
    void createBiologicoWithExistingId() throws Exception {
        // Create the Biologico with an existing ID
        biologico.setId(1L);

        int databaseSizeBeforeCreate = biologicoRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(biologico))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Biologico in the database
        List<Biologico> biologicoList = biologicoRepository.findAll().collectList().block();
        assertThat(biologicoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllBiologicos() {
        // Initialize the database
        biologicoRepository.save(biologico).block();

        // Get all the biologicoList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(biologico.getId().intValue()))
            .jsonPath("$.[*].tipoBiologico")
            .value(hasItem(DEFAULT_TIPO_BIOLOGICO))
            .jsonPath("$.[*].detalleBiologico")
            .value(hasItem(DEFAULT_DETALLE_BIOLOGICO));
    }

    @Test
    void getBiologico() {
        // Initialize the database
        biologicoRepository.save(biologico).block();

        // Get the biologico
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, biologico.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(biologico.getId().intValue()))
            .jsonPath("$.tipoBiologico")
            .value(is(DEFAULT_TIPO_BIOLOGICO))
            .jsonPath("$.detalleBiologico")
            .value(is(DEFAULT_DETALLE_BIOLOGICO));
    }

    @Test
    void getNonExistingBiologico() {
        // Get the biologico
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewBiologico() throws Exception {
        // Initialize the database
        biologicoRepository.save(biologico).block();

        int databaseSizeBeforeUpdate = biologicoRepository.findAll().collectList().block().size();

        // Update the biologico
        Biologico updatedBiologico = biologicoRepository.findById(biologico.getId()).block();
        updatedBiologico.tipoBiologico(UPDATED_TIPO_BIOLOGICO).detalleBiologico(UPDATED_DETALLE_BIOLOGICO);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedBiologico.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedBiologico))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Biologico in the database
        List<Biologico> biologicoList = biologicoRepository.findAll().collectList().block();
        assertThat(biologicoList).hasSize(databaseSizeBeforeUpdate);
        Biologico testBiologico = biologicoList.get(biologicoList.size() - 1);
        assertThat(testBiologico.getTipoBiologico()).isEqualTo(UPDATED_TIPO_BIOLOGICO);
        assertThat(testBiologico.getDetalleBiologico()).isEqualTo(UPDATED_DETALLE_BIOLOGICO);
    }

    @Test
    void putNonExistingBiologico() throws Exception {
        int databaseSizeBeforeUpdate = biologicoRepository.findAll().collectList().block().size();
        biologico.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, biologico.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(biologico))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Biologico in the database
        List<Biologico> biologicoList = biologicoRepository.findAll().collectList().block();
        assertThat(biologicoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchBiologico() throws Exception {
        int databaseSizeBeforeUpdate = biologicoRepository.findAll().collectList().block().size();
        biologico.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(biologico))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Biologico in the database
        List<Biologico> biologicoList = biologicoRepository.findAll().collectList().block();
        assertThat(biologicoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamBiologico() throws Exception {
        int databaseSizeBeforeUpdate = biologicoRepository.findAll().collectList().block().size();
        biologico.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(biologico))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Biologico in the database
        List<Biologico> biologicoList = biologicoRepository.findAll().collectList().block();
        assertThat(biologicoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateBiologicoWithPatch() throws Exception {
        // Initialize the database
        biologicoRepository.save(biologico).block();

        int databaseSizeBeforeUpdate = biologicoRepository.findAll().collectList().block().size();

        // Update the biologico using partial update
        Biologico partialUpdatedBiologico = new Biologico();
        partialUpdatedBiologico.setId(biologico.getId());

        partialUpdatedBiologico.tipoBiologico(UPDATED_TIPO_BIOLOGICO).detalleBiologico(UPDATED_DETALLE_BIOLOGICO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedBiologico.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedBiologico))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Biologico in the database
        List<Biologico> biologicoList = biologicoRepository.findAll().collectList().block();
        assertThat(biologicoList).hasSize(databaseSizeBeforeUpdate);
        Biologico testBiologico = biologicoList.get(biologicoList.size() - 1);
        assertThat(testBiologico.getTipoBiologico()).isEqualTo(UPDATED_TIPO_BIOLOGICO);
        assertThat(testBiologico.getDetalleBiologico()).isEqualTo(UPDATED_DETALLE_BIOLOGICO);
    }

    @Test
    void fullUpdateBiologicoWithPatch() throws Exception {
        // Initialize the database
        biologicoRepository.save(biologico).block();

        int databaseSizeBeforeUpdate = biologicoRepository.findAll().collectList().block().size();

        // Update the biologico using partial update
        Biologico partialUpdatedBiologico = new Biologico();
        partialUpdatedBiologico.setId(biologico.getId());

        partialUpdatedBiologico.tipoBiologico(UPDATED_TIPO_BIOLOGICO).detalleBiologico(UPDATED_DETALLE_BIOLOGICO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedBiologico.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedBiologico))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Biologico in the database
        List<Biologico> biologicoList = biologicoRepository.findAll().collectList().block();
        assertThat(biologicoList).hasSize(databaseSizeBeforeUpdate);
        Biologico testBiologico = biologicoList.get(biologicoList.size() - 1);
        assertThat(testBiologico.getTipoBiologico()).isEqualTo(UPDATED_TIPO_BIOLOGICO);
        assertThat(testBiologico.getDetalleBiologico()).isEqualTo(UPDATED_DETALLE_BIOLOGICO);
    }

    @Test
    void patchNonExistingBiologico() throws Exception {
        int databaseSizeBeforeUpdate = biologicoRepository.findAll().collectList().block().size();
        biologico.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, biologico.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(biologico))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Biologico in the database
        List<Biologico> biologicoList = biologicoRepository.findAll().collectList().block();
        assertThat(biologicoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchBiologico() throws Exception {
        int databaseSizeBeforeUpdate = biologicoRepository.findAll().collectList().block().size();
        biologico.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(biologico))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Biologico in the database
        List<Biologico> biologicoList = biologicoRepository.findAll().collectList().block();
        assertThat(biologicoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamBiologico() throws Exception {
        int databaseSizeBeforeUpdate = biologicoRepository.findAll().collectList().block().size();
        biologico.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(biologico))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Biologico in the database
        List<Biologico> biologicoList = biologicoRepository.findAll().collectList().block();
        assertThat(biologicoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteBiologico() {
        // Initialize the database
        biologicoRepository.save(biologico).block();

        int databaseSizeBeforeDelete = biologicoRepository.findAll().collectList().block().size();

        // Delete the biologico
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, biologico.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Biologico> biologicoList = biologicoRepository.findAll().collectList().block();
        assertThat(biologicoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
