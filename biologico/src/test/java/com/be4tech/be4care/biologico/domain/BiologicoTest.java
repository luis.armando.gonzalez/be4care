package com.be4tech.be4care.biologico.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.be4tech.be4care.biologico.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class BiologicoTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Biologico.class);
        Biologico biologico1 = new Biologico();
        biologico1.setId(1L);
        Biologico biologico2 = new Biologico();
        biologico2.setId(biologico1.getId());
        assertThat(biologico1).isEqualTo(biologico2);
        biologico2.setId(2L);
        assertThat(biologico1).isNotEqualTo(biologico2);
        biologico1.setId(null);
        assertThat(biologico1).isNotEqualTo(biologico2);
    }
}
