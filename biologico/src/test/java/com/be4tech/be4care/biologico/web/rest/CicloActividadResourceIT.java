package com.be4tech.be4care.biologico.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.biologico.IntegrationTest;
import com.be4tech.be4care.biologico.domain.CicloActividad;
import com.be4tech.be4care.biologico.repository.CicloActividadRepository;
import com.be4tech.be4care.biologico.repository.UserRepository;
import com.be4tech.be4care.biologico.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link CicloActividadResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class CicloActividadResourceIT {

    private static final Integer DEFAULT_PERIODO_CICLO = 1;
    private static final Integer UPDATED_PERIODO_CICLO = 2;

    private static final Integer DEFAULT_CANTIDAD = 1;
    private static final Integer UPDATED_CANTIDAD = 2;

    private static final Instant DEFAULT_FECHA_INICIO_CICLO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_INICIO_CICLO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/ciclo-actividads";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CicloActividadRepository cicloActividadRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private CicloActividad cicloActividad;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CicloActividad createEntity(EntityManager em) {
        CicloActividad cicloActividad = new CicloActividad()
            .periodoCiclo(DEFAULT_PERIODO_CICLO)
            .cantidad(DEFAULT_CANTIDAD)
            .fechaInicioCiclo(DEFAULT_FECHA_INICIO_CICLO);
        return cicloActividad;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CicloActividad createUpdatedEntity(EntityManager em) {
        CicloActividad cicloActividad = new CicloActividad()
            .periodoCiclo(UPDATED_PERIODO_CICLO)
            .cantidad(UPDATED_CANTIDAD)
            .fechaInicioCiclo(UPDATED_FECHA_INICIO_CICLO);
        return cicloActividad;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(CicloActividad.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        cicloActividad = createEntity(em);
    }

    @Test
    void createCicloActividad() throws Exception {
        int databaseSizeBeforeCreate = cicloActividadRepository.findAll().collectList().block().size();
        // Create the CicloActividad
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(cicloActividad))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the CicloActividad in the database
        List<CicloActividad> cicloActividadList = cicloActividadRepository.findAll().collectList().block();
        assertThat(cicloActividadList).hasSize(databaseSizeBeforeCreate + 1);
        CicloActividad testCicloActividad = cicloActividadList.get(cicloActividadList.size() - 1);
        assertThat(testCicloActividad.getPeriodoCiclo()).isEqualTo(DEFAULT_PERIODO_CICLO);
        assertThat(testCicloActividad.getCantidad()).isEqualTo(DEFAULT_CANTIDAD);
        assertThat(testCicloActividad.getFechaInicioCiclo()).isEqualTo(DEFAULT_FECHA_INICIO_CICLO);
    }

    @Test
    void createCicloActividadWithExistingId() throws Exception {
        // Create the CicloActividad with an existing ID
        cicloActividad.setId(1L);

        int databaseSizeBeforeCreate = cicloActividadRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(cicloActividad))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the CicloActividad in the database
        List<CicloActividad> cicloActividadList = cicloActividadRepository.findAll().collectList().block();
        assertThat(cicloActividadList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllCicloActividads() {
        // Initialize the database
        cicloActividadRepository.save(cicloActividad).block();

        // Get all the cicloActividadList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(cicloActividad.getId().intValue()))
            .jsonPath("$.[*].periodoCiclo")
            .value(hasItem(DEFAULT_PERIODO_CICLO))
            .jsonPath("$.[*].cantidad")
            .value(hasItem(DEFAULT_CANTIDAD))
            .jsonPath("$.[*].fechaInicioCiclo")
            .value(hasItem(DEFAULT_FECHA_INICIO_CICLO.toString()));
    }

    @Test
    void getCicloActividad() {
        // Initialize the database
        cicloActividadRepository.save(cicloActividad).block();

        // Get the cicloActividad
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, cicloActividad.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(cicloActividad.getId().intValue()))
            .jsonPath("$.periodoCiclo")
            .value(is(DEFAULT_PERIODO_CICLO))
            .jsonPath("$.cantidad")
            .value(is(DEFAULT_CANTIDAD))
            .jsonPath("$.fechaInicioCiclo")
            .value(is(DEFAULT_FECHA_INICIO_CICLO.toString()));
    }

    @Test
    void getNonExistingCicloActividad() {
        // Get the cicloActividad
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewCicloActividad() throws Exception {
        // Initialize the database
        cicloActividadRepository.save(cicloActividad).block();

        int databaseSizeBeforeUpdate = cicloActividadRepository.findAll().collectList().block().size();

        // Update the cicloActividad
        CicloActividad updatedCicloActividad = cicloActividadRepository.findById(cicloActividad.getId()).block();
        updatedCicloActividad.periodoCiclo(UPDATED_PERIODO_CICLO).cantidad(UPDATED_CANTIDAD).fechaInicioCiclo(UPDATED_FECHA_INICIO_CICLO);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedCicloActividad.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedCicloActividad))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the CicloActividad in the database
        List<CicloActividad> cicloActividadList = cicloActividadRepository.findAll().collectList().block();
        assertThat(cicloActividadList).hasSize(databaseSizeBeforeUpdate);
        CicloActividad testCicloActividad = cicloActividadList.get(cicloActividadList.size() - 1);
        assertThat(testCicloActividad.getPeriodoCiclo()).isEqualTo(UPDATED_PERIODO_CICLO);
        assertThat(testCicloActividad.getCantidad()).isEqualTo(UPDATED_CANTIDAD);
        assertThat(testCicloActividad.getFechaInicioCiclo()).isEqualTo(UPDATED_FECHA_INICIO_CICLO);
    }

    @Test
    void putNonExistingCicloActividad() throws Exception {
        int databaseSizeBeforeUpdate = cicloActividadRepository.findAll().collectList().block().size();
        cicloActividad.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, cicloActividad.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(cicloActividad))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the CicloActividad in the database
        List<CicloActividad> cicloActividadList = cicloActividadRepository.findAll().collectList().block();
        assertThat(cicloActividadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchCicloActividad() throws Exception {
        int databaseSizeBeforeUpdate = cicloActividadRepository.findAll().collectList().block().size();
        cicloActividad.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(cicloActividad))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the CicloActividad in the database
        List<CicloActividad> cicloActividadList = cicloActividadRepository.findAll().collectList().block();
        assertThat(cicloActividadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamCicloActividad() throws Exception {
        int databaseSizeBeforeUpdate = cicloActividadRepository.findAll().collectList().block().size();
        cicloActividad.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(cicloActividad))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the CicloActividad in the database
        List<CicloActividad> cicloActividadList = cicloActividadRepository.findAll().collectList().block();
        assertThat(cicloActividadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateCicloActividadWithPatch() throws Exception {
        // Initialize the database
        cicloActividadRepository.save(cicloActividad).block();

        int databaseSizeBeforeUpdate = cicloActividadRepository.findAll().collectList().block().size();

        // Update the cicloActividad using partial update
        CicloActividad partialUpdatedCicloActividad = new CicloActividad();
        partialUpdatedCicloActividad.setId(cicloActividad.getId());

        partialUpdatedCicloActividad
            .periodoCiclo(UPDATED_PERIODO_CICLO)
            .cantidad(UPDATED_CANTIDAD)
            .fechaInicioCiclo(UPDATED_FECHA_INICIO_CICLO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedCicloActividad.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedCicloActividad))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the CicloActividad in the database
        List<CicloActividad> cicloActividadList = cicloActividadRepository.findAll().collectList().block();
        assertThat(cicloActividadList).hasSize(databaseSizeBeforeUpdate);
        CicloActividad testCicloActividad = cicloActividadList.get(cicloActividadList.size() - 1);
        assertThat(testCicloActividad.getPeriodoCiclo()).isEqualTo(UPDATED_PERIODO_CICLO);
        assertThat(testCicloActividad.getCantidad()).isEqualTo(UPDATED_CANTIDAD);
        assertThat(testCicloActividad.getFechaInicioCiclo()).isEqualTo(UPDATED_FECHA_INICIO_CICLO);
    }

    @Test
    void fullUpdateCicloActividadWithPatch() throws Exception {
        // Initialize the database
        cicloActividadRepository.save(cicloActividad).block();

        int databaseSizeBeforeUpdate = cicloActividadRepository.findAll().collectList().block().size();

        // Update the cicloActividad using partial update
        CicloActividad partialUpdatedCicloActividad = new CicloActividad();
        partialUpdatedCicloActividad.setId(cicloActividad.getId());

        partialUpdatedCicloActividad
            .periodoCiclo(UPDATED_PERIODO_CICLO)
            .cantidad(UPDATED_CANTIDAD)
            .fechaInicioCiclo(UPDATED_FECHA_INICIO_CICLO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedCicloActividad.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedCicloActividad))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the CicloActividad in the database
        List<CicloActividad> cicloActividadList = cicloActividadRepository.findAll().collectList().block();
        assertThat(cicloActividadList).hasSize(databaseSizeBeforeUpdate);
        CicloActividad testCicloActividad = cicloActividadList.get(cicloActividadList.size() - 1);
        assertThat(testCicloActividad.getPeriodoCiclo()).isEqualTo(UPDATED_PERIODO_CICLO);
        assertThat(testCicloActividad.getCantidad()).isEqualTo(UPDATED_CANTIDAD);
        assertThat(testCicloActividad.getFechaInicioCiclo()).isEqualTo(UPDATED_FECHA_INICIO_CICLO);
    }

    @Test
    void patchNonExistingCicloActividad() throws Exception {
        int databaseSizeBeforeUpdate = cicloActividadRepository.findAll().collectList().block().size();
        cicloActividad.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, cicloActividad.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(cicloActividad))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the CicloActividad in the database
        List<CicloActividad> cicloActividadList = cicloActividadRepository.findAll().collectList().block();
        assertThat(cicloActividadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchCicloActividad() throws Exception {
        int databaseSizeBeforeUpdate = cicloActividadRepository.findAll().collectList().block().size();
        cicloActividad.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(cicloActividad))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the CicloActividad in the database
        List<CicloActividad> cicloActividadList = cicloActividadRepository.findAll().collectList().block();
        assertThat(cicloActividadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamCicloActividad() throws Exception {
        int databaseSizeBeforeUpdate = cicloActividadRepository.findAll().collectList().block().size();
        cicloActividad.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(cicloActividad))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the CicloActividad in the database
        List<CicloActividad> cicloActividadList = cicloActividadRepository.findAll().collectList().block();
        assertThat(cicloActividadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteCicloActividad() {
        // Initialize the database
        cicloActividadRepository.save(cicloActividad).block();

        int databaseSizeBeforeDelete = cicloActividadRepository.findAll().collectList().block().size();

        // Delete the cicloActividad
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, cicloActividad.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<CicloActividad> cicloActividadList = cicloActividadRepository.findAll().collectList().block();
        assertThat(cicloActividadList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
