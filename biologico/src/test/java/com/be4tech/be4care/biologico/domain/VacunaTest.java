package com.be4tech.be4care.biologico.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.be4tech.be4care.biologico.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class VacunaTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Vacuna.class);
        Vacuna vacuna1 = new Vacuna();
        vacuna1.setId(1L);
        Vacuna vacuna2 = new Vacuna();
        vacuna2.setId(vacuna1.getId());
        assertThat(vacuna1).isEqualTo(vacuna2);
        vacuna2.setId(2L);
        assertThat(vacuna1).isNotEqualTo(vacuna2);
        vacuna1.setId(null);
        assertThat(vacuna1).isNotEqualTo(vacuna2);
    }
}
