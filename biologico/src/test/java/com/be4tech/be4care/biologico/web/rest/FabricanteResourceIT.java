package com.be4tech.be4care.biologico.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.biologico.IntegrationTest;
import com.be4tech.be4care.biologico.domain.Fabricante;
import com.be4tech.be4care.biologico.repository.FabricanteRepository;
import com.be4tech.be4care.biologico.service.EntityManager;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link FabricanteResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class FabricanteResourceIT {

    private static final String DEFAULT_NOMBRE_FABRICANTE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_FABRICANTE = "BBBBBBBBBB";

    private static final String DEFAULT_PRODUCTOS_FABRICANET = "AAAAAAAAAA";
    private static final String UPDATED_PRODUCTOS_FABRICANET = "BBBBBBBBBB";

    private static final String DEFAULT_DETALLE_FABRICANTE = "AAAAAAAAAA";
    private static final String UPDATED_DETALLE_FABRICANTE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/fabricantes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private FabricanteRepository fabricanteRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Fabricante fabricante;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fabricante createEntity(EntityManager em) {
        Fabricante fabricante = new Fabricante()
            .nombreFabricante(DEFAULT_NOMBRE_FABRICANTE)
            .productosFabricanet(DEFAULT_PRODUCTOS_FABRICANET)
            .detalleFabricante(DEFAULT_DETALLE_FABRICANTE);
        return fabricante;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fabricante createUpdatedEntity(EntityManager em) {
        Fabricante fabricante = new Fabricante()
            .nombreFabricante(UPDATED_NOMBRE_FABRICANTE)
            .productosFabricanet(UPDATED_PRODUCTOS_FABRICANET)
            .detalleFabricante(UPDATED_DETALLE_FABRICANTE);
        return fabricante;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Fabricante.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        fabricante = createEntity(em);
    }

    @Test
    void createFabricante() throws Exception {
        int databaseSizeBeforeCreate = fabricanteRepository.findAll().collectList().block().size();
        // Create the Fabricante
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(fabricante))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Fabricante in the database
        List<Fabricante> fabricanteList = fabricanteRepository.findAll().collectList().block();
        assertThat(fabricanteList).hasSize(databaseSizeBeforeCreate + 1);
        Fabricante testFabricante = fabricanteList.get(fabricanteList.size() - 1);
        assertThat(testFabricante.getNombreFabricante()).isEqualTo(DEFAULT_NOMBRE_FABRICANTE);
        assertThat(testFabricante.getProductosFabricanet()).isEqualTo(DEFAULT_PRODUCTOS_FABRICANET);
        assertThat(testFabricante.getDetalleFabricante()).isEqualTo(DEFAULT_DETALLE_FABRICANTE);
    }

    @Test
    void createFabricanteWithExistingId() throws Exception {
        // Create the Fabricante with an existing ID
        fabricante.setId(1L);

        int databaseSizeBeforeCreate = fabricanteRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(fabricante))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Fabricante in the database
        List<Fabricante> fabricanteList = fabricanteRepository.findAll().collectList().block();
        assertThat(fabricanteList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllFabricantes() {
        // Initialize the database
        fabricanteRepository.save(fabricante).block();

        // Get all the fabricanteList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(fabricante.getId().intValue()))
            .jsonPath("$.[*].nombreFabricante")
            .value(hasItem(DEFAULT_NOMBRE_FABRICANTE.toString()))
            .jsonPath("$.[*].productosFabricanet")
            .value(hasItem(DEFAULT_PRODUCTOS_FABRICANET.toString()))
            .jsonPath("$.[*].detalleFabricante")
            .value(hasItem(DEFAULT_DETALLE_FABRICANTE.toString()));
    }

    @Test
    void getFabricante() {
        // Initialize the database
        fabricanteRepository.save(fabricante).block();

        // Get the fabricante
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, fabricante.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(fabricante.getId().intValue()))
            .jsonPath("$.nombreFabricante")
            .value(is(DEFAULT_NOMBRE_FABRICANTE.toString()))
            .jsonPath("$.productosFabricanet")
            .value(is(DEFAULT_PRODUCTOS_FABRICANET.toString()))
            .jsonPath("$.detalleFabricante")
            .value(is(DEFAULT_DETALLE_FABRICANTE.toString()));
    }

    @Test
    void getNonExistingFabricante() {
        // Get the fabricante
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewFabricante() throws Exception {
        // Initialize the database
        fabricanteRepository.save(fabricante).block();

        int databaseSizeBeforeUpdate = fabricanteRepository.findAll().collectList().block().size();

        // Update the fabricante
        Fabricante updatedFabricante = fabricanteRepository.findById(fabricante.getId()).block();
        updatedFabricante
            .nombreFabricante(UPDATED_NOMBRE_FABRICANTE)
            .productosFabricanet(UPDATED_PRODUCTOS_FABRICANET)
            .detalleFabricante(UPDATED_DETALLE_FABRICANTE);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedFabricante.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedFabricante))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Fabricante in the database
        List<Fabricante> fabricanteList = fabricanteRepository.findAll().collectList().block();
        assertThat(fabricanteList).hasSize(databaseSizeBeforeUpdate);
        Fabricante testFabricante = fabricanteList.get(fabricanteList.size() - 1);
        assertThat(testFabricante.getNombreFabricante()).isEqualTo(UPDATED_NOMBRE_FABRICANTE);
        assertThat(testFabricante.getProductosFabricanet()).isEqualTo(UPDATED_PRODUCTOS_FABRICANET);
        assertThat(testFabricante.getDetalleFabricante()).isEqualTo(UPDATED_DETALLE_FABRICANTE);
    }

    @Test
    void putNonExistingFabricante() throws Exception {
        int databaseSizeBeforeUpdate = fabricanteRepository.findAll().collectList().block().size();
        fabricante.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, fabricante.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(fabricante))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Fabricante in the database
        List<Fabricante> fabricanteList = fabricanteRepository.findAll().collectList().block();
        assertThat(fabricanteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchFabricante() throws Exception {
        int databaseSizeBeforeUpdate = fabricanteRepository.findAll().collectList().block().size();
        fabricante.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(fabricante))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Fabricante in the database
        List<Fabricante> fabricanteList = fabricanteRepository.findAll().collectList().block();
        assertThat(fabricanteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamFabricante() throws Exception {
        int databaseSizeBeforeUpdate = fabricanteRepository.findAll().collectList().block().size();
        fabricante.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(fabricante))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Fabricante in the database
        List<Fabricante> fabricanteList = fabricanteRepository.findAll().collectList().block();
        assertThat(fabricanteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateFabricanteWithPatch() throws Exception {
        // Initialize the database
        fabricanteRepository.save(fabricante).block();

        int databaseSizeBeforeUpdate = fabricanteRepository.findAll().collectList().block().size();

        // Update the fabricante using partial update
        Fabricante partialUpdatedFabricante = new Fabricante();
        partialUpdatedFabricante.setId(fabricante.getId());

        partialUpdatedFabricante.nombreFabricante(UPDATED_NOMBRE_FABRICANTE);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedFabricante.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedFabricante))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Fabricante in the database
        List<Fabricante> fabricanteList = fabricanteRepository.findAll().collectList().block();
        assertThat(fabricanteList).hasSize(databaseSizeBeforeUpdate);
        Fabricante testFabricante = fabricanteList.get(fabricanteList.size() - 1);
        assertThat(testFabricante.getNombreFabricante()).isEqualTo(UPDATED_NOMBRE_FABRICANTE);
        assertThat(testFabricante.getProductosFabricanet()).isEqualTo(DEFAULT_PRODUCTOS_FABRICANET);
        assertThat(testFabricante.getDetalleFabricante()).isEqualTo(DEFAULT_DETALLE_FABRICANTE);
    }

    @Test
    void fullUpdateFabricanteWithPatch() throws Exception {
        // Initialize the database
        fabricanteRepository.save(fabricante).block();

        int databaseSizeBeforeUpdate = fabricanteRepository.findAll().collectList().block().size();

        // Update the fabricante using partial update
        Fabricante partialUpdatedFabricante = new Fabricante();
        partialUpdatedFabricante.setId(fabricante.getId());

        partialUpdatedFabricante
            .nombreFabricante(UPDATED_NOMBRE_FABRICANTE)
            .productosFabricanet(UPDATED_PRODUCTOS_FABRICANET)
            .detalleFabricante(UPDATED_DETALLE_FABRICANTE);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedFabricante.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedFabricante))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Fabricante in the database
        List<Fabricante> fabricanteList = fabricanteRepository.findAll().collectList().block();
        assertThat(fabricanteList).hasSize(databaseSizeBeforeUpdate);
        Fabricante testFabricante = fabricanteList.get(fabricanteList.size() - 1);
        assertThat(testFabricante.getNombreFabricante()).isEqualTo(UPDATED_NOMBRE_FABRICANTE);
        assertThat(testFabricante.getProductosFabricanet()).isEqualTo(UPDATED_PRODUCTOS_FABRICANET);
        assertThat(testFabricante.getDetalleFabricante()).isEqualTo(UPDATED_DETALLE_FABRICANTE);
    }

    @Test
    void patchNonExistingFabricante() throws Exception {
        int databaseSizeBeforeUpdate = fabricanteRepository.findAll().collectList().block().size();
        fabricante.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, fabricante.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(fabricante))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Fabricante in the database
        List<Fabricante> fabricanteList = fabricanteRepository.findAll().collectList().block();
        assertThat(fabricanteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchFabricante() throws Exception {
        int databaseSizeBeforeUpdate = fabricanteRepository.findAll().collectList().block().size();
        fabricante.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(fabricante))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Fabricante in the database
        List<Fabricante> fabricanteList = fabricanteRepository.findAll().collectList().block();
        assertThat(fabricanteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamFabricante() throws Exception {
        int databaseSizeBeforeUpdate = fabricanteRepository.findAll().collectList().block().size();
        fabricante.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(fabricante))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Fabricante in the database
        List<Fabricante> fabricanteList = fabricanteRepository.findAll().collectList().block();
        assertThat(fabricanteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteFabricante() {
        // Initialize the database
        fabricanteRepository.save(fabricante).block();

        int databaseSizeBeforeDelete = fabricanteRepository.findAll().collectList().block().size();

        // Delete the fabricante
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, fabricante.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Fabricante> fabricanteList = fabricanteRepository.findAll().collectList().block();
        assertThat(fabricanteList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
