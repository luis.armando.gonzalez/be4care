package com.be4tech.be4care.biologico.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.be4tech.be4care.biologico.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CicloActividadTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CicloActividad.class);
        CicloActividad cicloActividad1 = new CicloActividad();
        cicloActividad1.setId(1L);
        CicloActividad cicloActividad2 = new CicloActividad();
        cicloActividad2.setId(cicloActividad1.getId());
        assertThat(cicloActividad1).isEqualTo(cicloActividad2);
        cicloActividad2.setId(2L);
        assertThat(cicloActividad1).isNotEqualTo(cicloActividad2);
        cicloActividad1.setId(null);
        assertThat(cicloActividad1).isNotEqualTo(cicloActividad2);
    }
}
