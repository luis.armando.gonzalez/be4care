package com.be4tech.be4care.biologico.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.biologico.IntegrationTest;
import com.be4tech.be4care.biologico.domain.HistorialActividad;
import com.be4tech.be4care.biologico.repository.HistorialActividadRepository;
import com.be4tech.be4care.biologico.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link HistorialActividadResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class HistorialActividadResourceIT {

    private static final Instant DEFAULT_FECHA_HISTORIAL = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_HISTORIAL = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/historial-actividads";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private HistorialActividadRepository historialActividadRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private HistorialActividad historialActividad;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HistorialActividad createEntity(EntityManager em) {
        HistorialActividad historialActividad = new HistorialActividad().fechaHistorial(DEFAULT_FECHA_HISTORIAL);
        return historialActividad;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HistorialActividad createUpdatedEntity(EntityManager em) {
        HistorialActividad historialActividad = new HistorialActividad().fechaHistorial(UPDATED_FECHA_HISTORIAL);
        return historialActividad;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(HistorialActividad.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        historialActividad = createEntity(em);
    }

    @Test
    void createHistorialActividad() throws Exception {
        int databaseSizeBeforeCreate = historialActividadRepository.findAll().collectList().block().size();
        // Create the HistorialActividad
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(historialActividad))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the HistorialActividad in the database
        List<HistorialActividad> historialActividadList = historialActividadRepository.findAll().collectList().block();
        assertThat(historialActividadList).hasSize(databaseSizeBeforeCreate + 1);
        HistorialActividad testHistorialActividad = historialActividadList.get(historialActividadList.size() - 1);
        assertThat(testHistorialActividad.getFechaHistorial()).isEqualTo(DEFAULT_FECHA_HISTORIAL);
    }

    @Test
    void createHistorialActividadWithExistingId() throws Exception {
        // Create the HistorialActividad with an existing ID
        historialActividad.setId(1L);

        int databaseSizeBeforeCreate = historialActividadRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(historialActividad))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the HistorialActividad in the database
        List<HistorialActividad> historialActividadList = historialActividadRepository.findAll().collectList().block();
        assertThat(historialActividadList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllHistorialActividads() {
        // Initialize the database
        historialActividadRepository.save(historialActividad).block();

        // Get all the historialActividadList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(historialActividad.getId().intValue()))
            .jsonPath("$.[*].fechaHistorial")
            .value(hasItem(DEFAULT_FECHA_HISTORIAL.toString()));
    }

    @Test
    void getHistorialActividad() {
        // Initialize the database
        historialActividadRepository.save(historialActividad).block();

        // Get the historialActividad
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, historialActividad.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(historialActividad.getId().intValue()))
            .jsonPath("$.fechaHistorial")
            .value(is(DEFAULT_FECHA_HISTORIAL.toString()));
    }

    @Test
    void getNonExistingHistorialActividad() {
        // Get the historialActividad
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewHistorialActividad() throws Exception {
        // Initialize the database
        historialActividadRepository.save(historialActividad).block();

        int databaseSizeBeforeUpdate = historialActividadRepository.findAll().collectList().block().size();

        // Update the historialActividad
        HistorialActividad updatedHistorialActividad = historialActividadRepository.findById(historialActividad.getId()).block();
        updatedHistorialActividad.fechaHistorial(UPDATED_FECHA_HISTORIAL);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedHistorialActividad.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedHistorialActividad))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the HistorialActividad in the database
        List<HistorialActividad> historialActividadList = historialActividadRepository.findAll().collectList().block();
        assertThat(historialActividadList).hasSize(databaseSizeBeforeUpdate);
        HistorialActividad testHistorialActividad = historialActividadList.get(historialActividadList.size() - 1);
        assertThat(testHistorialActividad.getFechaHistorial()).isEqualTo(UPDATED_FECHA_HISTORIAL);
    }

    @Test
    void putNonExistingHistorialActividad() throws Exception {
        int databaseSizeBeforeUpdate = historialActividadRepository.findAll().collectList().block().size();
        historialActividad.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, historialActividad.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(historialActividad))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the HistorialActividad in the database
        List<HistorialActividad> historialActividadList = historialActividadRepository.findAll().collectList().block();
        assertThat(historialActividadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchHistorialActividad() throws Exception {
        int databaseSizeBeforeUpdate = historialActividadRepository.findAll().collectList().block().size();
        historialActividad.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(historialActividad))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the HistorialActividad in the database
        List<HistorialActividad> historialActividadList = historialActividadRepository.findAll().collectList().block();
        assertThat(historialActividadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamHistorialActividad() throws Exception {
        int databaseSizeBeforeUpdate = historialActividadRepository.findAll().collectList().block().size();
        historialActividad.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(historialActividad))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the HistorialActividad in the database
        List<HistorialActividad> historialActividadList = historialActividadRepository.findAll().collectList().block();
        assertThat(historialActividadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateHistorialActividadWithPatch() throws Exception {
        // Initialize the database
        historialActividadRepository.save(historialActividad).block();

        int databaseSizeBeforeUpdate = historialActividadRepository.findAll().collectList().block().size();

        // Update the historialActividad using partial update
        HistorialActividad partialUpdatedHistorialActividad = new HistorialActividad();
        partialUpdatedHistorialActividad.setId(historialActividad.getId());

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedHistorialActividad.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedHistorialActividad))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the HistorialActividad in the database
        List<HistorialActividad> historialActividadList = historialActividadRepository.findAll().collectList().block();
        assertThat(historialActividadList).hasSize(databaseSizeBeforeUpdate);
        HistorialActividad testHistorialActividad = historialActividadList.get(historialActividadList.size() - 1);
        assertThat(testHistorialActividad.getFechaHistorial()).isEqualTo(DEFAULT_FECHA_HISTORIAL);
    }

    @Test
    void fullUpdateHistorialActividadWithPatch() throws Exception {
        // Initialize the database
        historialActividadRepository.save(historialActividad).block();

        int databaseSizeBeforeUpdate = historialActividadRepository.findAll().collectList().block().size();

        // Update the historialActividad using partial update
        HistorialActividad partialUpdatedHistorialActividad = new HistorialActividad();
        partialUpdatedHistorialActividad.setId(historialActividad.getId());

        partialUpdatedHistorialActividad.fechaHistorial(UPDATED_FECHA_HISTORIAL);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedHistorialActividad.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedHistorialActividad))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the HistorialActividad in the database
        List<HistorialActividad> historialActividadList = historialActividadRepository.findAll().collectList().block();
        assertThat(historialActividadList).hasSize(databaseSizeBeforeUpdate);
        HistorialActividad testHistorialActividad = historialActividadList.get(historialActividadList.size() - 1);
        assertThat(testHistorialActividad.getFechaHistorial()).isEqualTo(UPDATED_FECHA_HISTORIAL);
    }

    @Test
    void patchNonExistingHistorialActividad() throws Exception {
        int databaseSizeBeforeUpdate = historialActividadRepository.findAll().collectList().block().size();
        historialActividad.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, historialActividad.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(historialActividad))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the HistorialActividad in the database
        List<HistorialActividad> historialActividadList = historialActividadRepository.findAll().collectList().block();
        assertThat(historialActividadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchHistorialActividad() throws Exception {
        int databaseSizeBeforeUpdate = historialActividadRepository.findAll().collectList().block().size();
        historialActividad.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(historialActividad))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the HistorialActividad in the database
        List<HistorialActividad> historialActividadList = historialActividadRepository.findAll().collectList().block();
        assertThat(historialActividadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamHistorialActividad() throws Exception {
        int databaseSizeBeforeUpdate = historialActividadRepository.findAll().collectList().block().size();
        historialActividad.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(historialActividad))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the HistorialActividad in the database
        List<HistorialActividad> historialActividadList = historialActividadRepository.findAll().collectList().block();
        assertThat(historialActividadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteHistorialActividad() {
        // Initialize the database
        historialActividadRepository.save(historialActividad).block();

        int databaseSizeBeforeDelete = historialActividadRepository.findAll().collectList().block().size();

        // Delete the historialActividad
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, historialActividad.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<HistorialActividad> historialActividadList = historialActividadRepository.findAll().collectList().block();
        assertThat(historialActividadList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
