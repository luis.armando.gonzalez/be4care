package com.be4tech.be4care.biologico.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.biologico.IntegrationTest;
import com.be4tech.be4care.biologico.domain.Vacuna;
import com.be4tech.be4care.biologico.repository.VacunaRepository;
import com.be4tech.be4care.biologico.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link VacunaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class VacunaResourceIT {

    private static final String DEFAULT_NOMBRE_VACUNA = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_VACUNA = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_VACUNA = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_VACUNA = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPCION_VACUNA = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPCION_VACUNA = "BBBBBBBBBB";

    private static final Instant DEFAULT_FECHA_INGRESO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_INGRESO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LOTE_VACUNA = "AAAAAAAAAA";
    private static final String UPDATED_LOTE_VACUNA = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/vacunas";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private VacunaRepository vacunaRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Vacuna vacuna;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Vacuna createEntity(EntityManager em) {
        Vacuna vacuna = new Vacuna()
            .nombreVacuna(DEFAULT_NOMBRE_VACUNA)
            .tipoVacuna(DEFAULT_TIPO_VACUNA)
            .descripcionVacuna(DEFAULT_DESCRIPCION_VACUNA)
            .fechaIngreso(DEFAULT_FECHA_INGRESO)
            .loteVacuna(DEFAULT_LOTE_VACUNA);
        return vacuna;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Vacuna createUpdatedEntity(EntityManager em) {
        Vacuna vacuna = new Vacuna()
            .nombreVacuna(UPDATED_NOMBRE_VACUNA)
            .tipoVacuna(UPDATED_TIPO_VACUNA)
            .descripcionVacuna(UPDATED_DESCRIPCION_VACUNA)
            .fechaIngreso(UPDATED_FECHA_INGRESO)
            .loteVacuna(UPDATED_LOTE_VACUNA);
        return vacuna;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Vacuna.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        vacuna = createEntity(em);
    }

    @Test
    void createVacuna() throws Exception {
        int databaseSizeBeforeCreate = vacunaRepository.findAll().collectList().block().size();
        // Create the Vacuna
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(vacuna))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Vacuna in the database
        List<Vacuna> vacunaList = vacunaRepository.findAll().collectList().block();
        assertThat(vacunaList).hasSize(databaseSizeBeforeCreate + 1);
        Vacuna testVacuna = vacunaList.get(vacunaList.size() - 1);
        assertThat(testVacuna.getNombreVacuna()).isEqualTo(DEFAULT_NOMBRE_VACUNA);
        assertThat(testVacuna.getTipoVacuna()).isEqualTo(DEFAULT_TIPO_VACUNA);
        assertThat(testVacuna.getDescripcionVacuna()).isEqualTo(DEFAULT_DESCRIPCION_VACUNA);
        assertThat(testVacuna.getFechaIngreso()).isEqualTo(DEFAULT_FECHA_INGRESO);
        assertThat(testVacuna.getLoteVacuna()).isEqualTo(DEFAULT_LOTE_VACUNA);
    }

    @Test
    void createVacunaWithExistingId() throws Exception {
        // Create the Vacuna with an existing ID
        vacuna.setId(1L);

        int databaseSizeBeforeCreate = vacunaRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(vacuna))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Vacuna in the database
        List<Vacuna> vacunaList = vacunaRepository.findAll().collectList().block();
        assertThat(vacunaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllVacunas() {
        // Initialize the database
        vacunaRepository.save(vacuna).block();

        // Get all the vacunaList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(vacuna.getId().intValue()))
            .jsonPath("$.[*].nombreVacuna")
            .value(hasItem(DEFAULT_NOMBRE_VACUNA))
            .jsonPath("$.[*].tipoVacuna")
            .value(hasItem(DEFAULT_TIPO_VACUNA))
            .jsonPath("$.[*].descripcionVacuna")
            .value(hasItem(DEFAULT_DESCRIPCION_VACUNA))
            .jsonPath("$.[*].fechaIngreso")
            .value(hasItem(DEFAULT_FECHA_INGRESO.toString()))
            .jsonPath("$.[*].loteVacuna")
            .value(hasItem(DEFAULT_LOTE_VACUNA));
    }

    @Test
    void getVacuna() {
        // Initialize the database
        vacunaRepository.save(vacuna).block();

        // Get the vacuna
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, vacuna.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(vacuna.getId().intValue()))
            .jsonPath("$.nombreVacuna")
            .value(is(DEFAULT_NOMBRE_VACUNA))
            .jsonPath("$.tipoVacuna")
            .value(is(DEFAULT_TIPO_VACUNA))
            .jsonPath("$.descripcionVacuna")
            .value(is(DEFAULT_DESCRIPCION_VACUNA))
            .jsonPath("$.fechaIngreso")
            .value(is(DEFAULT_FECHA_INGRESO.toString()))
            .jsonPath("$.loteVacuna")
            .value(is(DEFAULT_LOTE_VACUNA));
    }

    @Test
    void getNonExistingVacuna() {
        // Get the vacuna
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewVacuna() throws Exception {
        // Initialize the database
        vacunaRepository.save(vacuna).block();

        int databaseSizeBeforeUpdate = vacunaRepository.findAll().collectList().block().size();

        // Update the vacuna
        Vacuna updatedVacuna = vacunaRepository.findById(vacuna.getId()).block();
        updatedVacuna
            .nombreVacuna(UPDATED_NOMBRE_VACUNA)
            .tipoVacuna(UPDATED_TIPO_VACUNA)
            .descripcionVacuna(UPDATED_DESCRIPCION_VACUNA)
            .fechaIngreso(UPDATED_FECHA_INGRESO)
            .loteVacuna(UPDATED_LOTE_VACUNA);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedVacuna.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedVacuna))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Vacuna in the database
        List<Vacuna> vacunaList = vacunaRepository.findAll().collectList().block();
        assertThat(vacunaList).hasSize(databaseSizeBeforeUpdate);
        Vacuna testVacuna = vacunaList.get(vacunaList.size() - 1);
        assertThat(testVacuna.getNombreVacuna()).isEqualTo(UPDATED_NOMBRE_VACUNA);
        assertThat(testVacuna.getTipoVacuna()).isEqualTo(UPDATED_TIPO_VACUNA);
        assertThat(testVacuna.getDescripcionVacuna()).isEqualTo(UPDATED_DESCRIPCION_VACUNA);
        assertThat(testVacuna.getFechaIngreso()).isEqualTo(UPDATED_FECHA_INGRESO);
        assertThat(testVacuna.getLoteVacuna()).isEqualTo(UPDATED_LOTE_VACUNA);
    }

    @Test
    void putNonExistingVacuna() throws Exception {
        int databaseSizeBeforeUpdate = vacunaRepository.findAll().collectList().block().size();
        vacuna.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, vacuna.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(vacuna))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Vacuna in the database
        List<Vacuna> vacunaList = vacunaRepository.findAll().collectList().block();
        assertThat(vacunaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchVacuna() throws Exception {
        int databaseSizeBeforeUpdate = vacunaRepository.findAll().collectList().block().size();
        vacuna.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(vacuna))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Vacuna in the database
        List<Vacuna> vacunaList = vacunaRepository.findAll().collectList().block();
        assertThat(vacunaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamVacuna() throws Exception {
        int databaseSizeBeforeUpdate = vacunaRepository.findAll().collectList().block().size();
        vacuna.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(vacuna))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Vacuna in the database
        List<Vacuna> vacunaList = vacunaRepository.findAll().collectList().block();
        assertThat(vacunaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateVacunaWithPatch() throws Exception {
        // Initialize the database
        vacunaRepository.save(vacuna).block();

        int databaseSizeBeforeUpdate = vacunaRepository.findAll().collectList().block().size();

        // Update the vacuna using partial update
        Vacuna partialUpdatedVacuna = new Vacuna();
        partialUpdatedVacuna.setId(vacuna.getId());

        partialUpdatedVacuna
            .nombreVacuna(UPDATED_NOMBRE_VACUNA)
            .descripcionVacuna(UPDATED_DESCRIPCION_VACUNA)
            .fechaIngreso(UPDATED_FECHA_INGRESO)
            .loteVacuna(UPDATED_LOTE_VACUNA);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedVacuna.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedVacuna))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Vacuna in the database
        List<Vacuna> vacunaList = vacunaRepository.findAll().collectList().block();
        assertThat(vacunaList).hasSize(databaseSizeBeforeUpdate);
        Vacuna testVacuna = vacunaList.get(vacunaList.size() - 1);
        assertThat(testVacuna.getNombreVacuna()).isEqualTo(UPDATED_NOMBRE_VACUNA);
        assertThat(testVacuna.getTipoVacuna()).isEqualTo(DEFAULT_TIPO_VACUNA);
        assertThat(testVacuna.getDescripcionVacuna()).isEqualTo(UPDATED_DESCRIPCION_VACUNA);
        assertThat(testVacuna.getFechaIngreso()).isEqualTo(UPDATED_FECHA_INGRESO);
        assertThat(testVacuna.getLoteVacuna()).isEqualTo(UPDATED_LOTE_VACUNA);
    }

    @Test
    void fullUpdateVacunaWithPatch() throws Exception {
        // Initialize the database
        vacunaRepository.save(vacuna).block();

        int databaseSizeBeforeUpdate = vacunaRepository.findAll().collectList().block().size();

        // Update the vacuna using partial update
        Vacuna partialUpdatedVacuna = new Vacuna();
        partialUpdatedVacuna.setId(vacuna.getId());

        partialUpdatedVacuna
            .nombreVacuna(UPDATED_NOMBRE_VACUNA)
            .tipoVacuna(UPDATED_TIPO_VACUNA)
            .descripcionVacuna(UPDATED_DESCRIPCION_VACUNA)
            .fechaIngreso(UPDATED_FECHA_INGRESO)
            .loteVacuna(UPDATED_LOTE_VACUNA);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedVacuna.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedVacuna))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Vacuna in the database
        List<Vacuna> vacunaList = vacunaRepository.findAll().collectList().block();
        assertThat(vacunaList).hasSize(databaseSizeBeforeUpdate);
        Vacuna testVacuna = vacunaList.get(vacunaList.size() - 1);
        assertThat(testVacuna.getNombreVacuna()).isEqualTo(UPDATED_NOMBRE_VACUNA);
        assertThat(testVacuna.getTipoVacuna()).isEqualTo(UPDATED_TIPO_VACUNA);
        assertThat(testVacuna.getDescripcionVacuna()).isEqualTo(UPDATED_DESCRIPCION_VACUNA);
        assertThat(testVacuna.getFechaIngreso()).isEqualTo(UPDATED_FECHA_INGRESO);
        assertThat(testVacuna.getLoteVacuna()).isEqualTo(UPDATED_LOTE_VACUNA);
    }

    @Test
    void patchNonExistingVacuna() throws Exception {
        int databaseSizeBeforeUpdate = vacunaRepository.findAll().collectList().block().size();
        vacuna.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, vacuna.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(vacuna))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Vacuna in the database
        List<Vacuna> vacunaList = vacunaRepository.findAll().collectList().block();
        assertThat(vacunaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchVacuna() throws Exception {
        int databaseSizeBeforeUpdate = vacunaRepository.findAll().collectList().block().size();
        vacuna.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(vacuna))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Vacuna in the database
        List<Vacuna> vacunaList = vacunaRepository.findAll().collectList().block();
        assertThat(vacunaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamVacuna() throws Exception {
        int databaseSizeBeforeUpdate = vacunaRepository.findAll().collectList().block().size();
        vacuna.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(vacuna))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Vacuna in the database
        List<Vacuna> vacunaList = vacunaRepository.findAll().collectList().block();
        assertThat(vacunaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteVacuna() {
        // Initialize the database
        vacunaRepository.save(vacuna).block();

        int databaseSizeBeforeDelete = vacunaRepository.findAll().collectList().block().size();

        // Delete the vacuna
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, vacuna.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Vacuna> vacunaList = vacunaRepository.findAll().collectList().block();
        assertThat(vacunaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
