package com.be4tech.be4care.biologico.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.be4tech.be4care.biologico.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class HistorialActividadTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(HistorialActividad.class);
        HistorialActividad historialActividad1 = new HistorialActividad();
        historialActividad1.setId(1L);
        HistorialActividad historialActividad2 = new HistorialActividad();
        historialActividad2.setId(historialActividad1.getId());
        assertThat(historialActividad1).isEqualTo(historialActividad2);
        historialActividad2.setId(2L);
        assertThat(historialActividad1).isNotEqualTo(historialActividad2);
        historialActividad1.setId(null);
        assertThat(historialActividad1).isNotEqualTo(historialActividad2);
    }
}
