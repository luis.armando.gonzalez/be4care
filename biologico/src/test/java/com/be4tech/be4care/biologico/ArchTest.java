package com.be4tech.be4care.biologico;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.be4tech.be4care.biologico");

        noClasses()
            .that()
            .resideInAnyPackage("com.be4tech.be4care.biologico.service..")
            .or()
            .resideInAnyPackage("com.be4tech.be4care.biologico.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..com.be4tech.be4care.biologico.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
