package com.be4tech.be4care.expedientemanual.repository.rowmapper;

import com.be4tech.be4care.expedientemanual.domain.Espirometria;
import com.be4tech.be4care.expedientemanual.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Espirometria}, with proper type conversions.
 */
@Service
public class EspirometriaRowMapper implements BiFunction<Row, String, Espirometria> {

    private final ColumnConverter converter;

    public EspirometriaRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Espirometria} stored in the database.
     */
    @Override
    public Espirometria apply(Row row, String prefix) {
        Espirometria entity = new Espirometria();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setFev1T(converter.fromRow(row, prefix + "_fev_1_t", Integer.class));
        entity.setFev1R(converter.fromRow(row, prefix + "_fev_1_r", Integer.class));
        entity.setFvcT(converter.fromRow(row, prefix + "_fvc_t", Integer.class));
        entity.setFvcR(converter.fromRow(row, prefix + "_fvc_r", Integer.class));
        entity.setIndiceTP(converter.fromRow(row, prefix + "_indice_tp", Integer.class));
        entity.setFechaToma(converter.fromRow(row, prefix + "_fecha_toma", Instant.class));
        entity.setResultado(converter.fromRow(row, prefix + "_resultado", String.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        return entity;
    }
}
