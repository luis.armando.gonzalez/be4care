package com.be4tech.be4care.expedientemanual.repository;

import com.be4tech.be4care.expedientemanual.domain.Peso;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Peso entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PesoRepository extends R2dbcRepository<Peso, Long>, PesoRepositoryInternal {
    Flux<Peso> findAllBy(Pageable pageable);

    @Query("SELECT * FROM peso entity WHERE entity.user_id = :id")
    Flux<Peso> findByUser(Long id);

    @Query("SELECT * FROM peso entity WHERE entity.user_id IS NULL")
    Flux<Peso> findAllWhereUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Peso> findAll();

    @Override
    Mono<Peso> findById(Long id);

    @Override
    <S extends Peso> Mono<S> save(S entity);
}

interface PesoRepositoryInternal {
    <S extends Peso> Mono<S> insert(S entity);
    <S extends Peso> Mono<S> save(S entity);
    Mono<Integer> update(Peso entity);

    Flux<Peso> findAll();
    Mono<Peso> findById(Long id);
    Flux<Peso> findAllBy(Pageable pageable);
    Flux<Peso> findAllBy(Pageable pageable, Criteria criteria);
}
