package com.be4tech.be4care.expedientemanual.repository.rowmapper;

import com.be4tech.be4care.expedientemanual.domain.Peso;
import com.be4tech.be4care.expedientemanual.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Peso}, with proper type conversions.
 */
@Service
public class PesoRowMapper implements BiFunction<Row, String, Peso> {

    private final ColumnConverter converter;

    public PesoRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Peso} stored in the database.
     */
    @Override
    public Peso apply(Row row, String prefix) {
        Peso entity = new Peso();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setPesoKG(converter.fromRow(row, prefix + "_peso_kg", Integer.class));
        entity.setDescripcion(converter.fromRow(row, prefix + "_descripcion", String.class));
        entity.setFechaRegistro(converter.fromRow(row, prefix + "_fecha_registro", Instant.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        return entity;
    }
}
