package com.be4tech.be4care.expedientemanual.web.rest;

import com.be4tech.be4care.expedientemanual.domain.Peso;
import com.be4tech.be4care.expedientemanual.repository.PesoRepository;
import com.be4tech.be4care.expedientemanual.repository.UserRepository;
import com.be4tech.be4care.expedientemanual.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.expedientemanual.domain.Peso}.
 */
@RestController
@RequestMapping("/api")
public class PesoResource {

    private final Logger log = LoggerFactory.getLogger(PesoResource.class);

    private static final String ENTITY_NAME = "expedientemanualPeso";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PesoRepository pesoRepository;

    private final UserRepository userRepository;

    public PesoResource(PesoRepository pesoRepository, UserRepository userRepository) {
        this.pesoRepository = pesoRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /pesos} : Create a new peso.
     *
     * @param peso the peso to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new peso, or with status {@code 400 (Bad Request)} if the peso has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/pesos")
    public Mono<ResponseEntity<Peso>> createPeso(@RequestBody Peso peso) throws URISyntaxException {
        log.debug("REST request to save Peso : {}", peso);
        if (peso.getId() != null) {
            throw new BadRequestAlertException("A new peso cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (peso.getUser() != null) {
            // Save user in case it's new and only exists in gateway
            userRepository.save(peso.getUser());
        }
        return pesoRepository
            .save(peso)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/pesos/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /pesos/:id} : Updates an existing peso.
     *
     * @param id the id of the peso to save.
     * @param peso the peso to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated peso,
     * or with status {@code 400 (Bad Request)} if the peso is not valid,
     * or with status {@code 500 (Internal Server Error)} if the peso couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/pesos/{id}")
    public Mono<ResponseEntity<Peso>> updatePeso(@PathVariable(value = "id", required = false) final Long id, @RequestBody Peso peso)
        throws URISyntaxException {
        log.debug("REST request to update Peso : {}, {}", id, peso);
        if (peso.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, peso.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return pesoRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (peso.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(peso.getUser());
                }
                return pesoRepository
                    .save(peso)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /pesos/:id} : Partial updates given fields of an existing peso, field will ignore if it is null
     *
     * @param id the id of the peso to save.
     * @param peso the peso to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated peso,
     * or with status {@code 400 (Bad Request)} if the peso is not valid,
     * or with status {@code 404 (Not Found)} if the peso is not found,
     * or with status {@code 500 (Internal Server Error)} if the peso couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/pesos/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Peso>> partialUpdatePeso(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Peso peso
    ) throws URISyntaxException {
        log.debug("REST request to partial update Peso partially : {}, {}", id, peso);
        if (peso.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, peso.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return pesoRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (peso.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(peso.getUser());
                }

                Mono<Peso> result = pesoRepository
                    .findById(peso.getId())
                    .map(existingPeso -> {
                        if (peso.getPesoKG() != null) {
                            existingPeso.setPesoKG(peso.getPesoKG());
                        }
                        if (peso.getDescripcion() != null) {
                            existingPeso.setDescripcion(peso.getDescripcion());
                        }
                        if (peso.getFechaRegistro() != null) {
                            existingPeso.setFechaRegistro(peso.getFechaRegistro());
                        }

                        return existingPeso;
                    })
                    .flatMap(pesoRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /pesos} : get all the pesos.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pesos in body.
     */
    @GetMapping("/pesos")
    public Mono<ResponseEntity<List<Peso>>> getAllPesos(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Pesos");
        return pesoRepository
            .count()
            .zipWith(pesoRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /pesos/:id} : get the "id" peso.
     *
     * @param id the id of the peso to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the peso, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/pesos/{id}")
    public Mono<ResponseEntity<Peso>> getPeso(@PathVariable Long id) {
        log.debug("REST request to get Peso : {}", id);
        Mono<Peso> peso = pesoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(peso);
    }

    /**
     * {@code DELETE  /pesos/:id} : delete the "id" peso.
     *
     * @param id the id of the peso to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/pesos/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deletePeso(@PathVariable Long id) {
        log.debug("REST request to delete Peso : {}", id);
        return pesoRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
