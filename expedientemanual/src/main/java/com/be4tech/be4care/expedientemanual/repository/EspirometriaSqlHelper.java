package com.be4tech.be4care.expedientemanual.repository;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Table;

public class EspirometriaSqlHelper {

    public static List<Expression> getColumns(Table table, String columnPrefix) {
        List<Expression> columns = new ArrayList<>();
        columns.add(Column.aliased("id", table, columnPrefix + "_id"));
        columns.add(Column.aliased("fev_1_t", table, columnPrefix + "_fev_1_t"));
        columns.add(Column.aliased("fev_1_r", table, columnPrefix + "_fev_1_r"));
        columns.add(Column.aliased("fvc_t", table, columnPrefix + "_fvc_t"));
        columns.add(Column.aliased("fvc_r", table, columnPrefix + "_fvc_r"));
        columns.add(Column.aliased("indice_tp", table, columnPrefix + "_indice_tp"));
        columns.add(Column.aliased("fecha_toma", table, columnPrefix + "_fecha_toma"));
        columns.add(Column.aliased("resultado", table, columnPrefix + "_resultado"));

        columns.add(Column.aliased("user_id", table, columnPrefix + "_user_id"));
        return columns;
    }
}
