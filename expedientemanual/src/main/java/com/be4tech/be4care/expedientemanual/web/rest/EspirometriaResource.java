package com.be4tech.be4care.expedientemanual.web.rest;

import com.be4tech.be4care.expedientemanual.domain.Espirometria;
import com.be4tech.be4care.expedientemanual.repository.EspirometriaRepository;
import com.be4tech.be4care.expedientemanual.repository.UserRepository;
import com.be4tech.be4care.expedientemanual.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.expedientemanual.domain.Espirometria}.
 */
@RestController
@RequestMapping("/api")
public class EspirometriaResource {

    private final Logger log = LoggerFactory.getLogger(EspirometriaResource.class);

    private static final String ENTITY_NAME = "expedientemanualEspirometria";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EspirometriaRepository espirometriaRepository;

    private final UserRepository userRepository;

    public EspirometriaResource(EspirometriaRepository espirometriaRepository, UserRepository userRepository) {
        this.espirometriaRepository = espirometriaRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /espirometrias} : Create a new espirometria.
     *
     * @param espirometria the espirometria to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new espirometria, or with status {@code 400 (Bad Request)} if the espirometria has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/espirometrias")
    public Mono<ResponseEntity<Espirometria>> createEspirometria(@RequestBody Espirometria espirometria) throws URISyntaxException {
        log.debug("REST request to save Espirometria : {}", espirometria);
        if (espirometria.getId() != null) {
            throw new BadRequestAlertException("A new espirometria cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (espirometria.getUser() != null) {
            // Save user in case it's new and only exists in gateway
            userRepository.save(espirometria.getUser());
        }
        return espirometriaRepository
            .save(espirometria)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/espirometrias/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /espirometrias/:id} : Updates an existing espirometria.
     *
     * @param id the id of the espirometria to save.
     * @param espirometria the espirometria to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated espirometria,
     * or with status {@code 400 (Bad Request)} if the espirometria is not valid,
     * or with status {@code 500 (Internal Server Error)} if the espirometria couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/espirometrias/{id}")
    public Mono<ResponseEntity<Espirometria>> updateEspirometria(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Espirometria espirometria
    ) throws URISyntaxException {
        log.debug("REST request to update Espirometria : {}, {}", id, espirometria);
        if (espirometria.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, espirometria.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return espirometriaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (espirometria.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(espirometria.getUser());
                }
                return espirometriaRepository
                    .save(espirometria)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /espirometrias/:id} : Partial updates given fields of an existing espirometria, field will ignore if it is null
     *
     * @param id the id of the espirometria to save.
     * @param espirometria the espirometria to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated espirometria,
     * or with status {@code 400 (Bad Request)} if the espirometria is not valid,
     * or with status {@code 404 (Not Found)} if the espirometria is not found,
     * or with status {@code 500 (Internal Server Error)} if the espirometria couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/espirometrias/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Espirometria>> partialUpdateEspirometria(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Espirometria espirometria
    ) throws URISyntaxException {
        log.debug("REST request to partial update Espirometria partially : {}, {}", id, espirometria);
        if (espirometria.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, espirometria.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return espirometriaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (espirometria.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(espirometria.getUser());
                }

                Mono<Espirometria> result = espirometriaRepository
                    .findById(espirometria.getId())
                    .map(existingEspirometria -> {
                        if (espirometria.getFev1T() != null) {
                            existingEspirometria.setFev1T(espirometria.getFev1T());
                        }
                        if (espirometria.getFev1R() != null) {
                            existingEspirometria.setFev1R(espirometria.getFev1R());
                        }
                        if (espirometria.getFvcT() != null) {
                            existingEspirometria.setFvcT(espirometria.getFvcT());
                        }
                        if (espirometria.getFvcR() != null) {
                            existingEspirometria.setFvcR(espirometria.getFvcR());
                        }
                        if (espirometria.getIndiceTP() != null) {
                            existingEspirometria.setIndiceTP(espirometria.getIndiceTP());
                        }
                        if (espirometria.getFechaToma() != null) {
                            existingEspirometria.setFechaToma(espirometria.getFechaToma());
                        }
                        if (espirometria.getResultado() != null) {
                            existingEspirometria.setResultado(espirometria.getResultado());
                        }

                        return existingEspirometria;
                    })
                    .flatMap(espirometriaRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /espirometrias} : get all the espirometrias.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of espirometrias in body.
     */
    @GetMapping("/espirometrias")
    public Mono<ResponseEntity<List<Espirometria>>> getAllEspirometrias(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Espirometrias");
        return espirometriaRepository
            .count()
            .zipWith(espirometriaRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /espirometrias/:id} : get the "id" espirometria.
     *
     * @param id the id of the espirometria to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the espirometria, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/espirometrias/{id}")
    public Mono<ResponseEntity<Espirometria>> getEspirometria(@PathVariable Long id) {
        log.debug("REST request to get Espirometria : {}", id);
        Mono<Espirometria> espirometria = espirometriaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(espirometria);
    }

    /**
     * {@code DELETE  /espirometrias/:id} : delete the "id" espirometria.
     *
     * @param id the id of the espirometria to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/espirometrias/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteEspirometria(@PathVariable Long id) {
        log.debug("REST request to delete Espirometria : {}", id);
        return espirometriaRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
