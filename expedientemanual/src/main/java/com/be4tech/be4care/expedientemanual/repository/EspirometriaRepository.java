package com.be4tech.be4care.expedientemanual.repository;

import com.be4tech.be4care.expedientemanual.domain.Espirometria;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Espirometria entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EspirometriaRepository extends R2dbcRepository<Espirometria, Long>, EspirometriaRepositoryInternal {
    Flux<Espirometria> findAllBy(Pageable pageable);

    @Query("SELECT * FROM espirometria entity WHERE entity.user_id = :id")
    Flux<Espirometria> findByUser(Long id);

    @Query("SELECT * FROM espirometria entity WHERE entity.user_id IS NULL")
    Flux<Espirometria> findAllWhereUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Espirometria> findAll();

    @Override
    Mono<Espirometria> findById(Long id);

    @Override
    <S extends Espirometria> Mono<S> save(S entity);
}

interface EspirometriaRepositoryInternal {
    <S extends Espirometria> Mono<S> insert(S entity);
    <S extends Espirometria> Mono<S> save(S entity);
    Mono<Integer> update(Espirometria entity);

    Flux<Espirometria> findAll();
    Mono<Espirometria> findById(Long id);
    Flux<Espirometria> findAllBy(Pageable pageable);
    Flux<Espirometria> findAllBy(Pageable pageable, Criteria criteria);
}
