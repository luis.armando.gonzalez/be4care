package com.be4tech.be4care.expedientemanual.repository.rowmapper;

import com.be4tech.be4care.expedientemanual.domain.Glucosa;
import com.be4tech.be4care.expedientemanual.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Glucosa}, with proper type conversions.
 */
@Service
public class GlucosaRowMapper implements BiFunction<Row, String, Glucosa> {

    private final ColumnConverter converter;

    public GlucosaRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Glucosa} stored in the database.
     */
    @Override
    public Glucosa apply(Row row, String prefix) {
        Glucosa entity = new Glucosa();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setGlucosaUser(converter.fromRow(row, prefix + "_glucosa_user", Integer.class));
        entity.setFechaRegistro(converter.fromRow(row, prefix + "_fecha_registro", Instant.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        return entity;
    }
}
