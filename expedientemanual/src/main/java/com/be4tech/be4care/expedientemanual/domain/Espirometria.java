package com.be4tech.be4care.expedientemanual.domain;

import java.io.Serializable;
import java.time.Instant;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Espirometria.
 */
@Table("espirometria")
public class Espirometria implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("fev_1_t")
    private Integer fev1T;

    @Column("fev_1_r")
    private Integer fev1R;

    @Column("fvc_t")
    private Integer fvcT;

    @Column("fvc_r")
    private Integer fvcR;

    @Column("indice_tp")
    private Integer indiceTP;

    @Column("fecha_toma")
    private Instant fechaToma;

    @Column("resultado")
    private String resultado;

    @Transient
    private User user;

    @Column("user_id")
    private String userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Espirometria id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getFev1T() {
        return this.fev1T;
    }

    public Espirometria fev1T(Integer fev1T) {
        this.setFev1T(fev1T);
        return this;
    }

    public void setFev1T(Integer fev1T) {
        this.fev1T = fev1T;
    }

    public Integer getFev1R() {
        return this.fev1R;
    }

    public Espirometria fev1R(Integer fev1R) {
        this.setFev1R(fev1R);
        return this;
    }

    public void setFev1R(Integer fev1R) {
        this.fev1R = fev1R;
    }

    public Integer getFvcT() {
        return this.fvcT;
    }

    public Espirometria fvcT(Integer fvcT) {
        this.setFvcT(fvcT);
        return this;
    }

    public void setFvcT(Integer fvcT) {
        this.fvcT = fvcT;
    }

    public Integer getFvcR() {
        return this.fvcR;
    }

    public Espirometria fvcR(Integer fvcR) {
        this.setFvcR(fvcR);
        return this;
    }

    public void setFvcR(Integer fvcR) {
        this.fvcR = fvcR;
    }

    public Integer getIndiceTP() {
        return this.indiceTP;
    }

    public Espirometria indiceTP(Integer indiceTP) {
        this.setIndiceTP(indiceTP);
        return this;
    }

    public void setIndiceTP(Integer indiceTP) {
        this.indiceTP = indiceTP;
    }

    public Instant getFechaToma() {
        return this.fechaToma;
    }

    public Espirometria fechaToma(Instant fechaToma) {
        this.setFechaToma(fechaToma);
        return this;
    }

    public void setFechaToma(Instant fechaToma) {
        this.fechaToma = fechaToma;
    }

    public String getResultado() {
        return this.resultado;
    }

    public Espirometria resultado(String resultado) {
        this.setResultado(resultado);
        return this;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user != null ? user.getId() : null;
    }

    public Espirometria user(User user) {
        this.setUser(user);
        return this;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String user) {
        this.userId = user;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Espirometria)) {
            return false;
        }
        return id != null && id.equals(((Espirometria) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Espirometria{" +
            "id=" + getId() +
            ", fev1T=" + getFev1T() +
            ", fev1R=" + getFev1R() +
            ", fvcT=" + getFvcT() +
            ", fvcR=" + getFvcR() +
            ", indiceTP=" + getIndiceTP() +
            ", fechaToma='" + getFechaToma() + "'" +
            ", resultado='" + getResultado() + "'" +
            "}";
    }
}
