package com.be4tech.be4care.expedientemanual.repository;

import com.be4tech.be4care.expedientemanual.domain.Glucosa;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Glucosa entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GlucosaRepository extends R2dbcRepository<Glucosa, Long>, GlucosaRepositoryInternal {
    Flux<Glucosa> findAllBy(Pageable pageable);

    @Query("SELECT * FROM glucosa entity WHERE entity.user_id = :id")
    Flux<Glucosa> findByUser(Long id);

    @Query("SELECT * FROM glucosa entity WHERE entity.user_id IS NULL")
    Flux<Glucosa> findAllWhereUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Glucosa> findAll();

    @Override
    Mono<Glucosa> findById(Long id);

    @Override
    <S extends Glucosa> Mono<S> save(S entity);
}

interface GlucosaRepositoryInternal {
    <S extends Glucosa> Mono<S> insert(S entity);
    <S extends Glucosa> Mono<S> save(S entity);
    Mono<Integer> update(Glucosa entity);

    Flux<Glucosa> findAll();
    Mono<Glucosa> findById(Long id);
    Flux<Glucosa> findAllBy(Pageable pageable);
    Flux<Glucosa> findAllBy(Pageable pageable, Criteria criteria);
}
