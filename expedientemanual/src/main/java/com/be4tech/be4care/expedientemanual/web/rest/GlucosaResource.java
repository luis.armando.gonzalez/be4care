package com.be4tech.be4care.expedientemanual.web.rest;

import com.be4tech.be4care.expedientemanual.domain.Glucosa;
import com.be4tech.be4care.expedientemanual.repository.GlucosaRepository;
import com.be4tech.be4care.expedientemanual.repository.UserRepository;
import com.be4tech.be4care.expedientemanual.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.be4tech.be4care.expedientemanual.domain.Glucosa}.
 */
@RestController
@RequestMapping("/api")
public class GlucosaResource {

    private final Logger log = LoggerFactory.getLogger(GlucosaResource.class);

    private static final String ENTITY_NAME = "expedientemanualGlucosa";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GlucosaRepository glucosaRepository;

    private final UserRepository userRepository;

    public GlucosaResource(GlucosaRepository glucosaRepository, UserRepository userRepository) {
        this.glucosaRepository = glucosaRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /glucosas} : Create a new glucosa.
     *
     * @param glucosa the glucosa to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new glucosa, or with status {@code 400 (Bad Request)} if the glucosa has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/glucosas")
    public Mono<ResponseEntity<Glucosa>> createGlucosa(@RequestBody Glucosa glucosa) throws URISyntaxException {
        log.debug("REST request to save Glucosa : {}", glucosa);
        if (glucosa.getId() != null) {
            throw new BadRequestAlertException("A new glucosa cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (glucosa.getUser() != null) {
            // Save user in case it's new and only exists in gateway
            userRepository.save(glucosa.getUser());
        }
        return glucosaRepository
            .save(glucosa)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/glucosas/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /glucosas/:id} : Updates an existing glucosa.
     *
     * @param id the id of the glucosa to save.
     * @param glucosa the glucosa to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated glucosa,
     * or with status {@code 400 (Bad Request)} if the glucosa is not valid,
     * or with status {@code 500 (Internal Server Error)} if the glucosa couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/glucosas/{id}")
    public Mono<ResponseEntity<Glucosa>> updateGlucosa(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Glucosa glucosa
    ) throws URISyntaxException {
        log.debug("REST request to update Glucosa : {}, {}", id, glucosa);
        if (glucosa.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, glucosa.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return glucosaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (glucosa.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(glucosa.getUser());
                }
                return glucosaRepository
                    .save(glucosa)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /glucosas/:id} : Partial updates given fields of an existing glucosa, field will ignore if it is null
     *
     * @param id the id of the glucosa to save.
     * @param glucosa the glucosa to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated glucosa,
     * or with status {@code 400 (Bad Request)} if the glucosa is not valid,
     * or with status {@code 404 (Not Found)} if the glucosa is not found,
     * or with status {@code 500 (Internal Server Error)} if the glucosa couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/glucosas/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Glucosa>> partialUpdateGlucosa(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Glucosa glucosa
    ) throws URISyntaxException {
        log.debug("REST request to partial update Glucosa partially : {}, {}", id, glucosa);
        if (glucosa.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, glucosa.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return glucosaRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                if (glucosa.getUser() != null) {
                    // Save user in case it's new and only exists in gateway
                    userRepository.save(glucosa.getUser());
                }

                Mono<Glucosa> result = glucosaRepository
                    .findById(glucosa.getId())
                    .map(existingGlucosa -> {
                        if (glucosa.getGlucosaUser() != null) {
                            existingGlucosa.setGlucosaUser(glucosa.getGlucosaUser());
                        }
                        if (glucosa.getFechaRegistro() != null) {
                            existingGlucosa.setFechaRegistro(glucosa.getFechaRegistro());
                        }

                        return existingGlucosa;
                    })
                    .flatMap(glucosaRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /glucosas} : get all the glucosas.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of glucosas in body.
     */
    @GetMapping("/glucosas")
    public Mono<ResponseEntity<List<Glucosa>>> getAllGlucosas(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Glucosas");
        return glucosaRepository
            .count()
            .zipWith(glucosaRepository.findAllBy(pageable).collectList())
            .map(countWithEntities -> {
                return ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2());
            });
    }

    /**
     * {@code GET  /glucosas/:id} : get the "id" glucosa.
     *
     * @param id the id of the glucosa to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the glucosa, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/glucosas/{id}")
    public Mono<ResponseEntity<Glucosa>> getGlucosa(@PathVariable Long id) {
        log.debug("REST request to get Glucosa : {}", id);
        Mono<Glucosa> glucosa = glucosaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(glucosa);
    }

    /**
     * {@code DELETE  /glucosas/:id} : delete the "id" glucosa.
     *
     * @param id the id of the glucosa to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/glucosas/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteGlucosa(@PathVariable Long id) {
        log.debug("REST request to delete Glucosa : {}", id);
        return glucosaRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
