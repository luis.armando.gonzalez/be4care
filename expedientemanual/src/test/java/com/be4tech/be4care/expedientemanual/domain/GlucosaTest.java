package com.be4tech.be4care.expedientemanual.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.be4tech.be4care.expedientemanual.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class GlucosaTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Glucosa.class);
        Glucosa glucosa1 = new Glucosa();
        glucosa1.setId(1L);
        Glucosa glucosa2 = new Glucosa();
        glucosa2.setId(glucosa1.getId());
        assertThat(glucosa1).isEqualTo(glucosa2);
        glucosa2.setId(2L);
        assertThat(glucosa1).isNotEqualTo(glucosa2);
        glucosa1.setId(null);
        assertThat(glucosa1).isNotEqualTo(glucosa2);
    }
}
