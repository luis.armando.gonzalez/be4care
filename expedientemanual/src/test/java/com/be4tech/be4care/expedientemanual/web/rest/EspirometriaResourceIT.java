package com.be4tech.be4care.expedientemanual.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.expedientemanual.IntegrationTest;
import com.be4tech.be4care.expedientemanual.domain.Espirometria;
import com.be4tech.be4care.expedientemanual.repository.EspirometriaRepository;
import com.be4tech.be4care.expedientemanual.repository.UserRepository;
import com.be4tech.be4care.expedientemanual.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link EspirometriaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class EspirometriaResourceIT {

    private static final Integer DEFAULT_FEV_1_T = 1;
    private static final Integer UPDATED_FEV_1_T = 2;

    private static final Integer DEFAULT_FEV_1_R = 1;
    private static final Integer UPDATED_FEV_1_R = 2;

    private static final Integer DEFAULT_FVC_T = 1;
    private static final Integer UPDATED_FVC_T = 2;

    private static final Integer DEFAULT_FVC_R = 1;
    private static final Integer UPDATED_FVC_R = 2;

    private static final Integer DEFAULT_INDICE_TP = 1;
    private static final Integer UPDATED_INDICE_TP = 2;

    private static final Instant DEFAULT_FECHA_TOMA = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_TOMA = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_RESULTADO = "AAAAAAAAAA";
    private static final String UPDATED_RESULTADO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/espirometrias";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private EspirometriaRepository espirometriaRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Espirometria espirometria;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Espirometria createEntity(EntityManager em) {
        Espirometria espirometria = new Espirometria()
            .fev1T(DEFAULT_FEV_1_T)
            .fev1R(DEFAULT_FEV_1_R)
            .fvcT(DEFAULT_FVC_T)
            .fvcR(DEFAULT_FVC_R)
            .indiceTP(DEFAULT_INDICE_TP)
            .fechaToma(DEFAULT_FECHA_TOMA)
            .resultado(DEFAULT_RESULTADO);
        return espirometria;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Espirometria createUpdatedEntity(EntityManager em) {
        Espirometria espirometria = new Espirometria()
            .fev1T(UPDATED_FEV_1_T)
            .fev1R(UPDATED_FEV_1_R)
            .fvcT(UPDATED_FVC_T)
            .fvcR(UPDATED_FVC_R)
            .indiceTP(UPDATED_INDICE_TP)
            .fechaToma(UPDATED_FECHA_TOMA)
            .resultado(UPDATED_RESULTADO);
        return espirometria;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Espirometria.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        espirometria = createEntity(em);
    }

    @Test
    void createEspirometria() throws Exception {
        int databaseSizeBeforeCreate = espirometriaRepository.findAll().collectList().block().size();
        // Create the Espirometria
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(espirometria))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Espirometria in the database
        List<Espirometria> espirometriaList = espirometriaRepository.findAll().collectList().block();
        assertThat(espirometriaList).hasSize(databaseSizeBeforeCreate + 1);
        Espirometria testEspirometria = espirometriaList.get(espirometriaList.size() - 1);
        assertThat(testEspirometria.getFev1T()).isEqualTo(DEFAULT_FEV_1_T);
        assertThat(testEspirometria.getFev1R()).isEqualTo(DEFAULT_FEV_1_R);
        assertThat(testEspirometria.getFvcT()).isEqualTo(DEFAULT_FVC_T);
        assertThat(testEspirometria.getFvcR()).isEqualTo(DEFAULT_FVC_R);
        assertThat(testEspirometria.getIndiceTP()).isEqualTo(DEFAULT_INDICE_TP);
        assertThat(testEspirometria.getFechaToma()).isEqualTo(DEFAULT_FECHA_TOMA);
        assertThat(testEspirometria.getResultado()).isEqualTo(DEFAULT_RESULTADO);
    }

    @Test
    void createEspirometriaWithExistingId() throws Exception {
        // Create the Espirometria with an existing ID
        espirometria.setId(1L);

        int databaseSizeBeforeCreate = espirometriaRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(espirometria))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Espirometria in the database
        List<Espirometria> espirometriaList = espirometriaRepository.findAll().collectList().block();
        assertThat(espirometriaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllEspirometrias() {
        // Initialize the database
        espirometriaRepository.save(espirometria).block();

        // Get all the espirometriaList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(espirometria.getId().intValue()))
            .jsonPath("$.[*].fev1T")
            .value(hasItem(DEFAULT_FEV_1_T))
            .jsonPath("$.[*].fev1R")
            .value(hasItem(DEFAULT_FEV_1_R))
            .jsonPath("$.[*].fvcT")
            .value(hasItem(DEFAULT_FVC_T))
            .jsonPath("$.[*].fvcR")
            .value(hasItem(DEFAULT_FVC_R))
            .jsonPath("$.[*].indiceTP")
            .value(hasItem(DEFAULT_INDICE_TP))
            .jsonPath("$.[*].fechaToma")
            .value(hasItem(DEFAULT_FECHA_TOMA.toString()))
            .jsonPath("$.[*].resultado")
            .value(hasItem(DEFAULT_RESULTADO));
    }

    @Test
    void getEspirometria() {
        // Initialize the database
        espirometriaRepository.save(espirometria).block();

        // Get the espirometria
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, espirometria.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(espirometria.getId().intValue()))
            .jsonPath("$.fev1T")
            .value(is(DEFAULT_FEV_1_T))
            .jsonPath("$.fev1R")
            .value(is(DEFAULT_FEV_1_R))
            .jsonPath("$.fvcT")
            .value(is(DEFAULT_FVC_T))
            .jsonPath("$.fvcR")
            .value(is(DEFAULT_FVC_R))
            .jsonPath("$.indiceTP")
            .value(is(DEFAULT_INDICE_TP))
            .jsonPath("$.fechaToma")
            .value(is(DEFAULT_FECHA_TOMA.toString()))
            .jsonPath("$.resultado")
            .value(is(DEFAULT_RESULTADO));
    }

    @Test
    void getNonExistingEspirometria() {
        // Get the espirometria
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewEspirometria() throws Exception {
        // Initialize the database
        espirometriaRepository.save(espirometria).block();

        int databaseSizeBeforeUpdate = espirometriaRepository.findAll().collectList().block().size();

        // Update the espirometria
        Espirometria updatedEspirometria = espirometriaRepository.findById(espirometria.getId()).block();
        updatedEspirometria
            .fev1T(UPDATED_FEV_1_T)
            .fev1R(UPDATED_FEV_1_R)
            .fvcT(UPDATED_FVC_T)
            .fvcR(UPDATED_FVC_R)
            .indiceTP(UPDATED_INDICE_TP)
            .fechaToma(UPDATED_FECHA_TOMA)
            .resultado(UPDATED_RESULTADO);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedEspirometria.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedEspirometria))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Espirometria in the database
        List<Espirometria> espirometriaList = espirometriaRepository.findAll().collectList().block();
        assertThat(espirometriaList).hasSize(databaseSizeBeforeUpdate);
        Espirometria testEspirometria = espirometriaList.get(espirometriaList.size() - 1);
        assertThat(testEspirometria.getFev1T()).isEqualTo(UPDATED_FEV_1_T);
        assertThat(testEspirometria.getFev1R()).isEqualTo(UPDATED_FEV_1_R);
        assertThat(testEspirometria.getFvcT()).isEqualTo(UPDATED_FVC_T);
        assertThat(testEspirometria.getFvcR()).isEqualTo(UPDATED_FVC_R);
        assertThat(testEspirometria.getIndiceTP()).isEqualTo(UPDATED_INDICE_TP);
        assertThat(testEspirometria.getFechaToma()).isEqualTo(UPDATED_FECHA_TOMA);
        assertThat(testEspirometria.getResultado()).isEqualTo(UPDATED_RESULTADO);
    }

    @Test
    void putNonExistingEspirometria() throws Exception {
        int databaseSizeBeforeUpdate = espirometriaRepository.findAll().collectList().block().size();
        espirometria.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, espirometria.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(espirometria))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Espirometria in the database
        List<Espirometria> espirometriaList = espirometriaRepository.findAll().collectList().block();
        assertThat(espirometriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchEspirometria() throws Exception {
        int databaseSizeBeforeUpdate = espirometriaRepository.findAll().collectList().block().size();
        espirometria.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(espirometria))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Espirometria in the database
        List<Espirometria> espirometriaList = espirometriaRepository.findAll().collectList().block();
        assertThat(espirometriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamEspirometria() throws Exception {
        int databaseSizeBeforeUpdate = espirometriaRepository.findAll().collectList().block().size();
        espirometria.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(espirometria))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Espirometria in the database
        List<Espirometria> espirometriaList = espirometriaRepository.findAll().collectList().block();
        assertThat(espirometriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateEspirometriaWithPatch() throws Exception {
        // Initialize the database
        espirometriaRepository.save(espirometria).block();

        int databaseSizeBeforeUpdate = espirometriaRepository.findAll().collectList().block().size();

        // Update the espirometria using partial update
        Espirometria partialUpdatedEspirometria = new Espirometria();
        partialUpdatedEspirometria.setId(espirometria.getId());

        partialUpdatedEspirometria.fechaToma(UPDATED_FECHA_TOMA);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedEspirometria.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedEspirometria))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Espirometria in the database
        List<Espirometria> espirometriaList = espirometriaRepository.findAll().collectList().block();
        assertThat(espirometriaList).hasSize(databaseSizeBeforeUpdate);
        Espirometria testEspirometria = espirometriaList.get(espirometriaList.size() - 1);
        assertThat(testEspirometria.getFev1T()).isEqualTo(DEFAULT_FEV_1_T);
        assertThat(testEspirometria.getFev1R()).isEqualTo(DEFAULT_FEV_1_R);
        assertThat(testEspirometria.getFvcT()).isEqualTo(DEFAULT_FVC_T);
        assertThat(testEspirometria.getFvcR()).isEqualTo(DEFAULT_FVC_R);
        assertThat(testEspirometria.getIndiceTP()).isEqualTo(DEFAULT_INDICE_TP);
        assertThat(testEspirometria.getFechaToma()).isEqualTo(UPDATED_FECHA_TOMA);
        assertThat(testEspirometria.getResultado()).isEqualTo(DEFAULT_RESULTADO);
    }

    @Test
    void fullUpdateEspirometriaWithPatch() throws Exception {
        // Initialize the database
        espirometriaRepository.save(espirometria).block();

        int databaseSizeBeforeUpdate = espirometriaRepository.findAll().collectList().block().size();

        // Update the espirometria using partial update
        Espirometria partialUpdatedEspirometria = new Espirometria();
        partialUpdatedEspirometria.setId(espirometria.getId());

        partialUpdatedEspirometria
            .fev1T(UPDATED_FEV_1_T)
            .fev1R(UPDATED_FEV_1_R)
            .fvcT(UPDATED_FVC_T)
            .fvcR(UPDATED_FVC_R)
            .indiceTP(UPDATED_INDICE_TP)
            .fechaToma(UPDATED_FECHA_TOMA)
            .resultado(UPDATED_RESULTADO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedEspirometria.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedEspirometria))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Espirometria in the database
        List<Espirometria> espirometriaList = espirometriaRepository.findAll().collectList().block();
        assertThat(espirometriaList).hasSize(databaseSizeBeforeUpdate);
        Espirometria testEspirometria = espirometriaList.get(espirometriaList.size() - 1);
        assertThat(testEspirometria.getFev1T()).isEqualTo(UPDATED_FEV_1_T);
        assertThat(testEspirometria.getFev1R()).isEqualTo(UPDATED_FEV_1_R);
        assertThat(testEspirometria.getFvcT()).isEqualTo(UPDATED_FVC_T);
        assertThat(testEspirometria.getFvcR()).isEqualTo(UPDATED_FVC_R);
        assertThat(testEspirometria.getIndiceTP()).isEqualTo(UPDATED_INDICE_TP);
        assertThat(testEspirometria.getFechaToma()).isEqualTo(UPDATED_FECHA_TOMA);
        assertThat(testEspirometria.getResultado()).isEqualTo(UPDATED_RESULTADO);
    }

    @Test
    void patchNonExistingEspirometria() throws Exception {
        int databaseSizeBeforeUpdate = espirometriaRepository.findAll().collectList().block().size();
        espirometria.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, espirometria.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(espirometria))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Espirometria in the database
        List<Espirometria> espirometriaList = espirometriaRepository.findAll().collectList().block();
        assertThat(espirometriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchEspirometria() throws Exception {
        int databaseSizeBeforeUpdate = espirometriaRepository.findAll().collectList().block().size();
        espirometria.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(espirometria))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Espirometria in the database
        List<Espirometria> espirometriaList = espirometriaRepository.findAll().collectList().block();
        assertThat(espirometriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamEspirometria() throws Exception {
        int databaseSizeBeforeUpdate = espirometriaRepository.findAll().collectList().block().size();
        espirometria.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(espirometria))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Espirometria in the database
        List<Espirometria> espirometriaList = espirometriaRepository.findAll().collectList().block();
        assertThat(espirometriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteEspirometria() {
        // Initialize the database
        espirometriaRepository.save(espirometria).block();

        int databaseSizeBeforeDelete = espirometriaRepository.findAll().collectList().block().size();

        // Delete the espirometria
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, espirometria.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Espirometria> espirometriaList = espirometriaRepository.findAll().collectList().block();
        assertThat(espirometriaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
