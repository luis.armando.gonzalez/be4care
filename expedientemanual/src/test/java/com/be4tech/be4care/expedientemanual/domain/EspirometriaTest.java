package com.be4tech.be4care.expedientemanual.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.be4tech.be4care.expedientemanual.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class EspirometriaTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Espirometria.class);
        Espirometria espirometria1 = new Espirometria();
        espirometria1.setId(1L);
        Espirometria espirometria2 = new Espirometria();
        espirometria2.setId(espirometria1.getId());
        assertThat(espirometria1).isEqualTo(espirometria2);
        espirometria2.setId(2L);
        assertThat(espirometria1).isNotEqualTo(espirometria2);
        espirometria1.setId(null);
        assertThat(espirometria1).isNotEqualTo(espirometria2);
    }
}
