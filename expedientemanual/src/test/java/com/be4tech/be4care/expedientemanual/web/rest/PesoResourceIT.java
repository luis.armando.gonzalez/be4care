package com.be4tech.be4care.expedientemanual.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.expedientemanual.IntegrationTest;
import com.be4tech.be4care.expedientemanual.domain.Peso;
import com.be4tech.be4care.expedientemanual.repository.PesoRepository;
import com.be4tech.be4care.expedientemanual.repository.UserRepository;
import com.be4tech.be4care.expedientemanual.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link PesoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class PesoResourceIT {

    private static final Integer DEFAULT_PESO_KG = 1;
    private static final Integer UPDATED_PESO_KG = 2;

    private static final String DEFAULT_DESCRIPCION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPCION = "BBBBBBBBBB";

    private static final Instant DEFAULT_FECHA_REGISTRO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_REGISTRO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/pesos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PesoRepository pesoRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Peso peso;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Peso createEntity(EntityManager em) {
        Peso peso = new Peso().pesoKG(DEFAULT_PESO_KG).descripcion(DEFAULT_DESCRIPCION).fechaRegistro(DEFAULT_FECHA_REGISTRO);
        return peso;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Peso createUpdatedEntity(EntityManager em) {
        Peso peso = new Peso().pesoKG(UPDATED_PESO_KG).descripcion(UPDATED_DESCRIPCION).fechaRegistro(UPDATED_FECHA_REGISTRO);
        return peso;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Peso.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        peso = createEntity(em);
    }

    @Test
    void createPeso() throws Exception {
        int databaseSizeBeforeCreate = pesoRepository.findAll().collectList().block().size();
        // Create the Peso
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(peso))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Peso in the database
        List<Peso> pesoList = pesoRepository.findAll().collectList().block();
        assertThat(pesoList).hasSize(databaseSizeBeforeCreate + 1);
        Peso testPeso = pesoList.get(pesoList.size() - 1);
        assertThat(testPeso.getPesoKG()).isEqualTo(DEFAULT_PESO_KG);
        assertThat(testPeso.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
        assertThat(testPeso.getFechaRegistro()).isEqualTo(DEFAULT_FECHA_REGISTRO);
    }

    @Test
    void createPesoWithExistingId() throws Exception {
        // Create the Peso with an existing ID
        peso.setId(1L);

        int databaseSizeBeforeCreate = pesoRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(peso))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Peso in the database
        List<Peso> pesoList = pesoRepository.findAll().collectList().block();
        assertThat(pesoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllPesos() {
        // Initialize the database
        pesoRepository.save(peso).block();

        // Get all the pesoList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(peso.getId().intValue()))
            .jsonPath("$.[*].pesoKG")
            .value(hasItem(DEFAULT_PESO_KG))
            .jsonPath("$.[*].descripcion")
            .value(hasItem(DEFAULT_DESCRIPCION))
            .jsonPath("$.[*].fechaRegistro")
            .value(hasItem(DEFAULT_FECHA_REGISTRO.toString()));
    }

    @Test
    void getPeso() {
        // Initialize the database
        pesoRepository.save(peso).block();

        // Get the peso
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, peso.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(peso.getId().intValue()))
            .jsonPath("$.pesoKG")
            .value(is(DEFAULT_PESO_KG))
            .jsonPath("$.descripcion")
            .value(is(DEFAULT_DESCRIPCION))
            .jsonPath("$.fechaRegistro")
            .value(is(DEFAULT_FECHA_REGISTRO.toString()));
    }

    @Test
    void getNonExistingPeso() {
        // Get the peso
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewPeso() throws Exception {
        // Initialize the database
        pesoRepository.save(peso).block();

        int databaseSizeBeforeUpdate = pesoRepository.findAll().collectList().block().size();

        // Update the peso
        Peso updatedPeso = pesoRepository.findById(peso.getId()).block();
        updatedPeso.pesoKG(UPDATED_PESO_KG).descripcion(UPDATED_DESCRIPCION).fechaRegistro(UPDATED_FECHA_REGISTRO);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedPeso.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedPeso))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Peso in the database
        List<Peso> pesoList = pesoRepository.findAll().collectList().block();
        assertThat(pesoList).hasSize(databaseSizeBeforeUpdate);
        Peso testPeso = pesoList.get(pesoList.size() - 1);
        assertThat(testPeso.getPesoKG()).isEqualTo(UPDATED_PESO_KG);
        assertThat(testPeso.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testPeso.getFechaRegistro()).isEqualTo(UPDATED_FECHA_REGISTRO);
    }

    @Test
    void putNonExistingPeso() throws Exception {
        int databaseSizeBeforeUpdate = pesoRepository.findAll().collectList().block().size();
        peso.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, peso.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(peso))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Peso in the database
        List<Peso> pesoList = pesoRepository.findAll().collectList().block();
        assertThat(pesoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchPeso() throws Exception {
        int databaseSizeBeforeUpdate = pesoRepository.findAll().collectList().block().size();
        peso.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(peso))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Peso in the database
        List<Peso> pesoList = pesoRepository.findAll().collectList().block();
        assertThat(pesoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamPeso() throws Exception {
        int databaseSizeBeforeUpdate = pesoRepository.findAll().collectList().block().size();
        peso.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(peso))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Peso in the database
        List<Peso> pesoList = pesoRepository.findAll().collectList().block();
        assertThat(pesoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdatePesoWithPatch() throws Exception {
        // Initialize the database
        pesoRepository.save(peso).block();

        int databaseSizeBeforeUpdate = pesoRepository.findAll().collectList().block().size();

        // Update the peso using partial update
        Peso partialUpdatedPeso = new Peso();
        partialUpdatedPeso.setId(peso.getId());

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPeso.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPeso))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Peso in the database
        List<Peso> pesoList = pesoRepository.findAll().collectList().block();
        assertThat(pesoList).hasSize(databaseSizeBeforeUpdate);
        Peso testPeso = pesoList.get(pesoList.size() - 1);
        assertThat(testPeso.getPesoKG()).isEqualTo(DEFAULT_PESO_KG);
        assertThat(testPeso.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
        assertThat(testPeso.getFechaRegistro()).isEqualTo(DEFAULT_FECHA_REGISTRO);
    }

    @Test
    void fullUpdatePesoWithPatch() throws Exception {
        // Initialize the database
        pesoRepository.save(peso).block();

        int databaseSizeBeforeUpdate = pesoRepository.findAll().collectList().block().size();

        // Update the peso using partial update
        Peso partialUpdatedPeso = new Peso();
        partialUpdatedPeso.setId(peso.getId());

        partialUpdatedPeso.pesoKG(UPDATED_PESO_KG).descripcion(UPDATED_DESCRIPCION).fechaRegistro(UPDATED_FECHA_REGISTRO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPeso.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPeso))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Peso in the database
        List<Peso> pesoList = pesoRepository.findAll().collectList().block();
        assertThat(pesoList).hasSize(databaseSizeBeforeUpdate);
        Peso testPeso = pesoList.get(pesoList.size() - 1);
        assertThat(testPeso.getPesoKG()).isEqualTo(UPDATED_PESO_KG);
        assertThat(testPeso.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testPeso.getFechaRegistro()).isEqualTo(UPDATED_FECHA_REGISTRO);
    }

    @Test
    void patchNonExistingPeso() throws Exception {
        int databaseSizeBeforeUpdate = pesoRepository.findAll().collectList().block().size();
        peso.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, peso.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(peso))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Peso in the database
        List<Peso> pesoList = pesoRepository.findAll().collectList().block();
        assertThat(pesoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchPeso() throws Exception {
        int databaseSizeBeforeUpdate = pesoRepository.findAll().collectList().block().size();
        peso.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(peso))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Peso in the database
        List<Peso> pesoList = pesoRepository.findAll().collectList().block();
        assertThat(pesoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamPeso() throws Exception {
        int databaseSizeBeforeUpdate = pesoRepository.findAll().collectList().block().size();
        peso.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(peso))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Peso in the database
        List<Peso> pesoList = pesoRepository.findAll().collectList().block();
        assertThat(pesoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deletePeso() {
        // Initialize the database
        pesoRepository.save(peso).block();

        int databaseSizeBeforeDelete = pesoRepository.findAll().collectList().block().size();

        // Delete the peso
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, peso.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Peso> pesoList = pesoRepository.findAll().collectList().block();
        assertThat(pesoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
