package com.be4tech.be4care.expedientemanual.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.be4tech.be4care.expedientemanual.IntegrationTest;
import com.be4tech.be4care.expedientemanual.domain.Glucosa;
import com.be4tech.be4care.expedientemanual.repository.GlucosaRepository;
import com.be4tech.be4care.expedientemanual.repository.UserRepository;
import com.be4tech.be4care.expedientemanual.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link GlucosaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class GlucosaResourceIT {

    private static final Integer DEFAULT_GLUCOSA_USER = 1;
    private static final Integer UPDATED_GLUCOSA_USER = 2;

    private static final Instant DEFAULT_FECHA_REGISTRO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_REGISTRO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/glucosas";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private GlucosaRepository glucosaRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Glucosa glucosa;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Glucosa createEntity(EntityManager em) {
        Glucosa glucosa = new Glucosa().glucosaUser(DEFAULT_GLUCOSA_USER).fechaRegistro(DEFAULT_FECHA_REGISTRO);
        return glucosa;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Glucosa createUpdatedEntity(EntityManager em) {
        Glucosa glucosa = new Glucosa().glucosaUser(UPDATED_GLUCOSA_USER).fechaRegistro(UPDATED_FECHA_REGISTRO);
        return glucosa;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Glucosa.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        glucosa = createEntity(em);
    }

    @Test
    void createGlucosa() throws Exception {
        int databaseSizeBeforeCreate = glucosaRepository.findAll().collectList().block().size();
        // Create the Glucosa
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(glucosa))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Glucosa in the database
        List<Glucosa> glucosaList = glucosaRepository.findAll().collectList().block();
        assertThat(glucosaList).hasSize(databaseSizeBeforeCreate + 1);
        Glucosa testGlucosa = glucosaList.get(glucosaList.size() - 1);
        assertThat(testGlucosa.getGlucosaUser()).isEqualTo(DEFAULT_GLUCOSA_USER);
        assertThat(testGlucosa.getFechaRegistro()).isEqualTo(DEFAULT_FECHA_REGISTRO);
    }

    @Test
    void createGlucosaWithExistingId() throws Exception {
        // Create the Glucosa with an existing ID
        glucosa.setId(1L);

        int databaseSizeBeforeCreate = glucosaRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(glucosa))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Glucosa in the database
        List<Glucosa> glucosaList = glucosaRepository.findAll().collectList().block();
        assertThat(glucosaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllGlucosas() {
        // Initialize the database
        glucosaRepository.save(glucosa).block();

        // Get all the glucosaList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(glucosa.getId().intValue()))
            .jsonPath("$.[*].glucosaUser")
            .value(hasItem(DEFAULT_GLUCOSA_USER))
            .jsonPath("$.[*].fechaRegistro")
            .value(hasItem(DEFAULT_FECHA_REGISTRO.toString()));
    }

    @Test
    void getGlucosa() {
        // Initialize the database
        glucosaRepository.save(glucosa).block();

        // Get the glucosa
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, glucosa.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(glucosa.getId().intValue()))
            .jsonPath("$.glucosaUser")
            .value(is(DEFAULT_GLUCOSA_USER))
            .jsonPath("$.fechaRegistro")
            .value(is(DEFAULT_FECHA_REGISTRO.toString()));
    }

    @Test
    void getNonExistingGlucosa() {
        // Get the glucosa
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewGlucosa() throws Exception {
        // Initialize the database
        glucosaRepository.save(glucosa).block();

        int databaseSizeBeforeUpdate = glucosaRepository.findAll().collectList().block().size();

        // Update the glucosa
        Glucosa updatedGlucosa = glucosaRepository.findById(glucosa.getId()).block();
        updatedGlucosa.glucosaUser(UPDATED_GLUCOSA_USER).fechaRegistro(UPDATED_FECHA_REGISTRO);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedGlucosa.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedGlucosa))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Glucosa in the database
        List<Glucosa> glucosaList = glucosaRepository.findAll().collectList().block();
        assertThat(glucosaList).hasSize(databaseSizeBeforeUpdate);
        Glucosa testGlucosa = glucosaList.get(glucosaList.size() - 1);
        assertThat(testGlucosa.getGlucosaUser()).isEqualTo(UPDATED_GLUCOSA_USER);
        assertThat(testGlucosa.getFechaRegistro()).isEqualTo(UPDATED_FECHA_REGISTRO);
    }

    @Test
    void putNonExistingGlucosa() throws Exception {
        int databaseSizeBeforeUpdate = glucosaRepository.findAll().collectList().block().size();
        glucosa.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, glucosa.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(glucosa))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Glucosa in the database
        List<Glucosa> glucosaList = glucosaRepository.findAll().collectList().block();
        assertThat(glucosaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchGlucosa() throws Exception {
        int databaseSizeBeforeUpdate = glucosaRepository.findAll().collectList().block().size();
        glucosa.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(glucosa))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Glucosa in the database
        List<Glucosa> glucosaList = glucosaRepository.findAll().collectList().block();
        assertThat(glucosaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamGlucosa() throws Exception {
        int databaseSizeBeforeUpdate = glucosaRepository.findAll().collectList().block().size();
        glucosa.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(glucosa))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Glucosa in the database
        List<Glucosa> glucosaList = glucosaRepository.findAll().collectList().block();
        assertThat(glucosaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateGlucosaWithPatch() throws Exception {
        // Initialize the database
        glucosaRepository.save(glucosa).block();

        int databaseSizeBeforeUpdate = glucosaRepository.findAll().collectList().block().size();

        // Update the glucosa using partial update
        Glucosa partialUpdatedGlucosa = new Glucosa();
        partialUpdatedGlucosa.setId(glucosa.getId());

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedGlucosa.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedGlucosa))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Glucosa in the database
        List<Glucosa> glucosaList = glucosaRepository.findAll().collectList().block();
        assertThat(glucosaList).hasSize(databaseSizeBeforeUpdate);
        Glucosa testGlucosa = glucosaList.get(glucosaList.size() - 1);
        assertThat(testGlucosa.getGlucosaUser()).isEqualTo(DEFAULT_GLUCOSA_USER);
        assertThat(testGlucosa.getFechaRegistro()).isEqualTo(DEFAULT_FECHA_REGISTRO);
    }

    @Test
    void fullUpdateGlucosaWithPatch() throws Exception {
        // Initialize the database
        glucosaRepository.save(glucosa).block();

        int databaseSizeBeforeUpdate = glucosaRepository.findAll().collectList().block().size();

        // Update the glucosa using partial update
        Glucosa partialUpdatedGlucosa = new Glucosa();
        partialUpdatedGlucosa.setId(glucosa.getId());

        partialUpdatedGlucosa.glucosaUser(UPDATED_GLUCOSA_USER).fechaRegistro(UPDATED_FECHA_REGISTRO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedGlucosa.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedGlucosa))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Glucosa in the database
        List<Glucosa> glucosaList = glucosaRepository.findAll().collectList().block();
        assertThat(glucosaList).hasSize(databaseSizeBeforeUpdate);
        Glucosa testGlucosa = glucosaList.get(glucosaList.size() - 1);
        assertThat(testGlucosa.getGlucosaUser()).isEqualTo(UPDATED_GLUCOSA_USER);
        assertThat(testGlucosa.getFechaRegistro()).isEqualTo(UPDATED_FECHA_REGISTRO);
    }

    @Test
    void patchNonExistingGlucosa() throws Exception {
        int databaseSizeBeforeUpdate = glucosaRepository.findAll().collectList().block().size();
        glucosa.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, glucosa.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(glucosa))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Glucosa in the database
        List<Glucosa> glucosaList = glucosaRepository.findAll().collectList().block();
        assertThat(glucosaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchGlucosa() throws Exception {
        int databaseSizeBeforeUpdate = glucosaRepository.findAll().collectList().block().size();
        glucosa.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(glucosa))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Glucosa in the database
        List<Glucosa> glucosaList = glucosaRepository.findAll().collectList().block();
        assertThat(glucosaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamGlucosa() throws Exception {
        int databaseSizeBeforeUpdate = glucosaRepository.findAll().collectList().block().size();
        glucosa.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(glucosa))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Glucosa in the database
        List<Glucosa> glucosaList = glucosaRepository.findAll().collectList().block();
        assertThat(glucosaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteGlucosa() {
        // Initialize the database
        glucosaRepository.save(glucosa).block();

        int databaseSizeBeforeDelete = glucosaRepository.findAll().collectList().block().size();

        // Delete the glucosa
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, glucosa.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Glucosa> glucosaList = glucosaRepository.findAll().collectList().block();
        assertThat(glucosaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
