**To generate the missing Docker image(s), please run:**
  ./gradlew bootJar -Pprod jibDockerBuild in E:\be4care\gateway
  ./gradlew bootJar -Pprod jibDockerBuild in E:\be4care\manilla
  ./gradlew bootJar -Pprod jibDockerBuild in E:\be4care\pacientes
  ./gradlew bootJar -Pprod jibDockerBuild in E:\be4care\biologico
  ./gradlew bootJar -Pprod jibDockerBuild in E:\be4care\diadema
  ./gradlew bootJar -Pprod jibDockerBuild in E:\be4care\expedientemanual

**You can launch all your infrastructure by running :**
docker-compose up -d

**Kubernetes configuration generated, but no Jib cache found**
**If you forgot to generate the Docker image for this application, please run:**
**To generate the missing Docker image(s), please run:**
  ./gradlew bootJar -Pprod jibDockerBuild in E:\becaremicro\biologico
  ./gradlew bootJar -Pprod jibDockerBuild in E:\becaremicro\diadema
  ./gradlew bootJar -Pprod jibDockerBuild in E:\becaremicro\gateway
  ./gradlew bootJar -Pprod jibDockerBuild in E:\becaremicro\manilla
  ./gradlew bootJar -Pprod jibDockerBuild in E:\becaremicro\pacientes


**WARNING! You will need to push your image to a registry. If you have not done so, use the following commands to tag and push the images:**
  docker image tag biologico be4tech/biologico
  docker push be4tech/biologico
  docker image tag diadema be4tech/diadema
  docker push be4tech/diadema
  docker image tag gateway be4tech/gateway
  docker push be4tech/gateway
  docker image tag manilla be4tech/manilla
  docker push be4tech/manilla
  docker image tag pacientes be4tech/pacientes
  docker push be4tech/pacientes

**INFO! Alternatively, you can use Jib to build and push image directly to a remote registry:**
  ./gradlew bootJar -Pprod jib -Djib.to.image=be4tech/biologico in E:\be4care\biologico
  ./gradlew bootJar -Pprod jib -Djib.to.image=be4tech/diadema in E:\be4care\diadema
  ./gradlew bootJar -Pprod jib -Djib.to.image=be4tech/expedientemanual in E:\be4care\expedientemanual
  ./gradlew bootJar -Pprod jib -Djib.to.image=be4tech/gateway in E:\be4care\gateway
  ./gradlew bootJar -Pprod jib -Djib.to.image=be4tech/manilla in E:\be4care\manilla
  ./gradlew bootJar -Pprod jib -Djib.to.image=be4tech/pacientes in E:\be4care\pacientes

  **Update k8s/registry-k8s/application-configmap.yml to contain your OIDC settings from the .okta.env file the Okta CLI just created. The Spring Cloud Config server reads from this file and shares the values with the gateway and microservices.**
  **OKTA**
  spring:
      security:
        oauth2:
          client:
            provider:
              oidc:
                issuer-uri: https://dev-9747234.okta.com/oauth2/default
            registration:
              oidc:
                client-id: 0oa2fdpnlprNRSGgA5d7
                client-secret: pi7ssnYYpLCLbHk2F1QfY-yxLoK4pNQfha6ZrhAy

**KEYCLOAK**
    spring:
          security:
            oauth2:
              client:
                provider:
                  oidc:
                    issuer-uri: https://becare-key.herokuapp.com/auth/realms/jhipster
                registration:
                  oidc:
                    client-id: web_app
                    client-secret: web-app
                    scope: openid,profile,email

**To configure the JHipster Registry to use OIDC for authentication, modify k8s/registry-k8s/jhipster-registry.yml to enable the oauth2 profile.**

- name: SPRING_PROFILES_ACTIVE
  value: prod,k8s,oauth2

**Initiate minikube**
minikube start

**You can deploy all your apps by running the following kubectl command:**
  bash kubectl-apply.sh -f

**If you want to use kustomize configuration, then run the following command:**
  bash kubectl-apply.sh -k

**Use these commands to find your application's IP addresses:**
  kubectl get svc gateway -n becare

  **OR**

  k9s -n becare
  kdash -n becare


**You can see if everything starts up using the following command.**

kubectl get pods -n becare

**You can use the name of a pod with kubectl logs to tail its logs.**

kubectl logs <pod-name> --tail=-1 -n becare

**You can use port-forwarding to see the JHipster Registry.**

kubectl port-forward svc/jhipster-registry -n becare 8761

**Open a browser and navigate to http://localhost:8761. You’ll need to sign in with your Okta credentials.**

**Once all is green, use port-forwarding to see the gateway app.**

kubectl port-forward svc/gateway -n becare 8080

**Then, go to http://localhost:8080, and you should be able to add blogs, posts, tags, and products.**

**reiniciar los pods**

kubectl rollout restart deploy -n becare


**Iniciar gcloud**

gcloud services enable container.googleapis.com containerregistry.googleapis.com

gcloud init

**abrir el siguiente enlace:**

https://console.cloud.google.com/gcr/images/be4care-331016?authuser=1&project=be4care-329119

**Crear cluster kubernete**
gcloud container clusters create becare --zone us-central1-a --machine-type n1-standard-4 --enable-autorepair --enable-autoupgrade

**Subir contendores a GKE de Google**

gcloud auth application-default login

gcloud auth configure-docker us-central1-docker.pkg.dev

**obtener el id de proyecto**

gcloud projects list

**responde**
PROJECT_ID            NAME              PROJECT_NUMBER
be4care-331016        be4care           755833460952

**subir al repositorio gcr.io/be4care-329119 todos los contenedores del cluster becare**
gradlew bootJar -Pprod jib -Djib.to.image=gcr.io/be4care-331016/gateway

**en la carpeta becaremicro/gateway**

**en sus archivos, agregue como prefijo. Elimine el si lo especificó anteriormente. Por ejemplo:k8s/*/-deployment.ymlgcr.io/<your-project-id>imagePullPolicy**

containers:
  - name: gateway-app
    image: gcr.io/be4care-331016/gateway
    env:


**En el k8sdirectorio, aplique todos los descriptores de implementación para ejecutar todas sus imágenes.**

./kubectl-apply.sh -f

**If you make a mistake configuring JHipster Registry and need to deploy it, you can do so with the following command:**

kubectl apply -f registry-k8s/jhipster-registry.yml -n becare
kubectl rollout restart statefulset/jhipster-registry -n becare

**You’ll need to restart all your deployments if you changed any configuration settings that services need to retrieve.**

kubectl rollout restart deploy -n becare

**Once everything is up and running, get the external IP of your gateway.**

kubectl get svc gateway -n becare

**You’ll need to add the external IP address as a valid redirect to your Okta OIDC app. Run okta login, open the returned URL in your browser, and sign in to the Okta Admin Console. Go to the Applications section, find your application, and edit it.**

**Add the standard JHipster redirect URIs using the IP address. For example, http://34.71.48.244:8080/login/oauth2/code/oidc for the login redirect URI, and http://34.71.48.244:8080 for the logout redirect URI.**

**You can use the following command to set your gateway’s IP address as a variable you can curl.**

EXTERNAL_IP=$(kubectl get svc gateway -ojsonpath="{.status loadBalancer.ingress[0].ip}" -n becare)
curl $EXTERNAL_IP:8080


kubectl expose deployment keycloak --type=LoadBalancer --port 80 --target-port 8080

**Instalar PGAdmin**

**Agregar repositorio de Helm**
helm repo add cetic https://cetic.github.io/helm-charts
helm repo update

**Instale el gráfico de helm pgAdmin con un nombre de versión my-release:**

helm install my-release cetic/pgadmin

**Desplegar PGadmin**
minikube service pgadmin-service --url                                                                      ✔ 
🏃  Starting tunnel for service pgadmin-service.
|-----------|-----------------|-------------|------------------------|
| NAMESPACE |      NAME       | TARGET PORT |          URL           |
|-----------|-----------------|-------------|------------------------|
| default   | pgadmin-service |             | http://127.0.0.1:64299 |
|-----------|-----------------|-------------|------------------------|
http://127.0.0.1:64299

**alternativo dentro del mismo contexto**

helm install control cetic/pgadmin -n becare

minikube service control-pgadmin -n becare --url
* Starting tunnel for service control-pgadmin.
|-----------|-----------------|-------------|------------------------|
| NAMESPACE |      NAME       | TARGET PORT |          URL           |
|-----------|-----------------|-------------|------------------------|
| becare    | control-pgadmin |             | http://127.0.0.1:54897 |
|-----------|-----------------|-------------|------------------------|
http://127.0.0.1:54897
! Porque estás usando controlador Docker en windows, la terminal debe abrirse para ejecutarlo.


**Desplegar PGadmin**
minikube service pgadmin-service --url                                                                      ✔ 
🏃  Starting tunnel for service pgadmin-service.
|-----------|-----------------|-------------|------------------------|
| NAMESPACE |      NAME       | TARGET PORT |          URL           |
|-----------|-----------------|-------------|------------------------|
| default   | pgadmin-service |             | http://127.0.0.1:64299 |
|-----------|-----------------|-------------|------------------------|
http://127.0.0.1:64299


**Because you are using a Docker driver on darwin, the terminal needs to be open to run it.**

kubectl port-forward service/pgadmin-service 8080:80                      
Forwarding from 127.0.0.1:8080 -> 80
Forwarding from [::1]:8080 -> 80

**Eliminar el repositorio**

helm delete my-release

**obtener password base de datos**
export POSTGRES_PASSWORD=$(kubectl get secret --namespace becare gateway-postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode)

**Acceder a su puerta de enlace en Google Cloud**
**Una vez que todo esté en funcionamiento, obtenga la IP externa de su puerta de enlace.**

kubectl get svc gateway -n demo

**Deberá agregar la dirección IP externa como una redirección válida a su aplicación Okta OIDC. Ejecute okta login, abra la URL devuelta en su navegador e inicie sesión en la Consola de administración de Okta. Vaya a la sección Aplicaciones , busque su aplicación y edítela.**

Agregue los URI de redireccionamiento estándar de JHipster utilizando la dirección IP. Por ejemplo, http://34.71.48.244:8080/login/oauth2/code/oidc para el URI de redireccionamiento de inicio de sesión y http://34.71.48.244:8080 para el URI de redireccionamiento de cierre de sesión.

**Agregue HTTPS a su puerta de enlace reactiva**
**Siempre debes usar HTTPS. Es una de las formas más fáciles de proteger las cosas, especialmente con los certificados gratuitos que se ofrecen en estos días. Los documentos de equilibrio de carga externo de Ray Tsang fueron de gran ayuda para descubrir todos estos pasos.**

**Necesitará una IP estática para asignar su certificado TLS (el nombre oficial de HTTPS).**

gcloud compute addresses create gateway-ingress-ip --global

**Puede ejecutar el siguiente comando para asegurarse de que funcione.**

gcloud compute addresses describe gateway-ingress-ip --global --format='value(address)'

**Luego, crea un k8s/ingress.ymlarchivo:**

apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: gateway
  annotations:
    kubernetes.io/ingress.global-static-ip-name: "gateway-ingress-ip"
spec:
  rules:
  - http:
      paths:
      - path: /*
        pathType: ImplementationSpecific
        backend:
          service:
            name: gateway
            port:
              number: 8080

**Impleméntelo y asegúrese de que funcione.**

kubectl apply -f ingress.yml -n demo

# keep running this command displays an IP address
# (hint: up arrow recalls the last command)

kubectl get ingress gateway -n demo

# Para utilizar un certificado TLS, debe tener un nombre de dominio completo y configurarlo para que apunte a la dirección IP. Si usted no tiene un dominio real, puede utilizar nip.io .

# Para crear un certificado, cree un k8s/certificate.yml .

apiVersion: networking.gke.io/v1
kind: ManagedCertificate
metadata:
  name: gateway-certificate
spec:
  domains:
  # Replace the value with your domain name
  - https://34.117.121.172.nip.io

#  Agregue el certificado a ingress.yml:

...
metadata:
  name: gateway
  annotations:
    kubernetes.io/ingress.global-static-ip-name: "gateway-ingress-ip"
    networking.gke.io/managed-certificates: "gateway-certificate"
...

# Implemente ambos archivos:

kubectl apply -f certificate.yml -f ingress.yml -n becare

# Verifique el estado de su certificado hasta que se imprima Status: ACTIVE:

kubectl describe managedcertificate gateway-certificate -n becare

# Despliega la descripci{on deñ certificado}
Name:         gateway-certificate
Namespace:    becare
Labels:       <none>
Annotations:  <none>
API Version:  networking.gke.io/v1
Kind:         ManagedCertificate
Metadata:
  Creation Timestamp:  2021-11-16T15:59:35Z
  Generation:          4
  Managed Fields:
    API Version:  networking.gke.io/v1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
      f:spec:
        .:
        f:domains:
    Manager:      kubectl-client-side-apply
    Operation:    Update
    Time:         2021-11-16T15:59:35Z
    API Version:  networking.gke.io/v1
    Fields Type:  FieldsV1
    fieldsV1:
      f:status:
        .:
        f:certificateName:
        f:certificateStatus:
        f:domainStatus:
        f:expireTime:
    Manager:         managed-certificate-controller
    Operation:       Update
    Time:            2021-11-16T16:49:00Z
  Resource Version:  2132808
  UID:               b548a13c-519c-4639-a969-73b0c2716c17
Spec:
  Domains:
    34.117.121.172.nip.io
Status:
  Certificate Name:    mcrt-c826a541-d3a9-48e1-b54e-b2f19f1d3ea4
  Certificate Status:  Active
  Domain Status:
    Domain:     34.117.121.172.nip.io
    Status:     Active
  Expire Time:  2022-02-14T07:34:50.000-08:00
Events:         <none>

# Forzar HTTPS con Spring Security

# El soporte WebFlux de Spring Security facilita la redirección a HTTPS . Sin embargo, si redirige todas las solicitudes HTTPS, las verificaciones de estado de Kubernetes fallarán porque reciben un 302 en lugar de un 200.

# Abra SecurityConfiguration.java el proyecto de puerta de enlace y agregue el siguiente código al springSecurityFilterChain()método.

src / main / java /… /gateway/config/SecurityConfiguration.java
http.redirectToHttps(redirect -> redirect
    .httpsRedirectWhen(e -> e.getRequest().getHeaders().containsKey("X-Forwarded-Proto"))
);

# Reconstruya la imagen de Docker para el proyecto de puerta de enlace.

./gradlew bootJar -Pprod jib -Djib.to.image=gcr.io/becare/gateway

# Ejecute los siguientes comandos para iniciar un reinicio continuo de las instancias de puerta de enlace:

kubectl rollout restart deployment gateway -n becare
